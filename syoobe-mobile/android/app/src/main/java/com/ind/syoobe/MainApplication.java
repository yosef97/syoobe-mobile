package com.ind.syoobe;

import android.app.Application;
import android.content.Context;
import com.bugsnag.android.Bugsnag;
import com.bugsnag.android.BugsnagPackage;
import com.bugsnag.android.BugsnagReactNativePlugin;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import io.invertase.firebase.messaging.ReactNativeFirebaseMessagingPackage;
import io.invertase.firebase.app.ReactNativeFirebaseAppPackage;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import java.lang.reflect.InvocationTargetException;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.reactnativecommunity.clipboard.ClipboardPackage;
import com.adrianha.midtrans.ReactNativeMidtransPackage;
import com.reactnativecommunity.slider.ReactSliderPackage;
import com.adityahas.midtrans.RNMidtransPackage;
import com.reactcommunity.rndatetimepicker.RNDateTimePickerPackage;
import com.th3rdwave.safeareacontext.SafeAreaContextPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.reactnativecommunity.picker.RNCPickerPackage;
import com.actionsheet.ActionSheetPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage;
import com.reactcommunity.rnlocalize.RNLocalizePackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import cl.json.RNSharePackage;
import com.rumax.reactnative.pdfviewer.PDFViewPackage;
import com.rnfs.RNFSPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.imagepicker.ImagePickerPackage;
import com.facebook.react.ReactNativeHost;
import java.util.Arrays;
import java.util.List;
import com.facebook.FacebookSdk;
import com.facebook.CallbackManager;
import com.facebook.appevents.AppEventsLogger;


public class MainApplication extends Application implements ReactApplication {

  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

  protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new RNGoogleSigninPackage(),
            new BugsnagPackage(),
            new ReactNativePushNotificationPackage(),
            new ReactNativeFirebaseMessagingPackage(),
            new ReactNativeFirebaseAppPackage(),
            new FBSDKPackage(),
            new ClipboardPackage(),
            new ReactNativeMidtransPackage(),
            new ReactSliderPackage(),
            new RNMidtransPackage(),
            new RNDateTimePickerPackage(),
            new SafeAreaContextPackage(),
            new ReanimatedPackage(),
            new RNCPickerPackage(),
            new ActionSheetPackage(),
            new RNCWebViewPackage(),
            new RNGestureHandlerPackage(),
            new SplashScreenReactPackage(),
            new DocumentPickerPackage(),
            new RNLocalizePackage(),
            new NetInfoPackage(),
            new RNSharePackage(),
            new PDFViewPackage(),
            new RNFSPackage(),
            new RNFetchBlobPackage(),
            new RNDeviceInfo(),
            new PickerPackage(),
            new ImagePickerPackage(),
            new CrashyPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Bugsnag.start(this);
    SoLoader.init(this, /* native exopackage */ false);
    initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
  }
  /**
   * Loads Flipper in React Native templates. Call this in the onCreate method with something like
   * initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
   *
   * @param context
   * @param reactInstanceManager
   */
  private static void initializeFlipper(
      Context context, ReactInstanceManager reactInstanceManager) {
    if (BuildConfig.DEBUG) {
      try {
        /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
        Class<?> aClass = Class.forName("com.ind.syoobe.ReactNativeFlipper");
        aClass
            .getMethod("initializeFlipper", Context.class, ReactInstanceManager.class)
            .invoke(null, context, reactInstanceManager);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }
}
