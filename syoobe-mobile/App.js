import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducers from './src/reducers';
import ReduxThunk from 'redux-thunk';
// import { AppStackNavigator } from './src/components/Navigation';
import SplashScreen from 'react-native-splash-screen';
import NetInfo from '@react-native-community/netinfo';
import NoInternet from './src/components/NoInternet';
// import * as RNLocalize from 'react-native-localize';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {AppStackNavigator} from './src/components/routes';
import {AppState, StatusBar, AsyncStorage} from 'react-native';
import axios from 'axios';
import Bugsnag from '@bugsnag/react-native';
Bugsnag.start();

const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
    };
  }

  async componentDidMount() {
    await SplashScreen.hide();
    this.checkInterNetConnection();
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange = async (nextAppState) => {
    const userToken = await AsyncStorage.getItem('token');
    if (nextAppState === 'background') {
      this.updateUserStatus(0, userToken);
    }
    if (nextAppState === 'active') {
      this.updateUserStatus(1, userToken);
    }
  };

  updateUserStatus = (status, _token) => {
    let details = {status, _token};
    // console.log(details);
    let formBody = [];
    for (let property in details) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/update_user_status', formBody)
      // .then((res) => res.json())
      .then((response) => console.log('updateUserStatus', response.data))
      .catch((error) => {
        Bugsnag.notify(error);
        console.log('error apps', error);
      });
  };

  handleConnectivityChange = (isConnected) => {
    this.setState({isConnected});
  };

  checkInterNetConnection = () => {
    // NetInfo.addEventListener('connectionChange', this.handleConnectivityChange);
    NetInfo.addEventListener((state) => this.handleConnectivityChange);
  };

  render() {
    console.disableYellowBox = true;
    return (
      <Provider store={store}>
        <StatusBar backgroundColor="transparent" transcluent />
        {!this.state.isConnected ? (
          <NoInternet />
        ) : (
          <NavigationContainer>
            <AppStackNavigator />
          </NavigationContainer>
        )}
      </Provider>
    );
  }
}

export default App;
