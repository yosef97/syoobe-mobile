import {
  GET_ORDER_DETAILS,
  GET_ORDER_DETAILS_SUCCESS,
  GET_ORDER_DETAILS_FAIL,
  GET_SALES_LIST,
} from '../actions/types';

const INITIAL_STATE = {
  loading: false,
  data: null,
  payment_history: {},
  salesList: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_ORDER_DETAILS:
      return {...state, loading: true, data: null, payment_history: {}};
    case GET_ORDER_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload,
        payment_history: action.payload.payment_history,
      };
    case GET_ORDER_DETAILS_FAIL:
      return {...state, loading: false, data: null, payment_history: {}};
    case GET_SALES_LIST:
      return {...state, loading: false, salesList: action.payload};
    default:
      return state;
  }
};
