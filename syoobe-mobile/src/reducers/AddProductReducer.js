import {
  UPLOAD_IMAGES_GENERAL,
  UPLOAD_IMAGES_GENERAL_SUCCESS,
  UPLOAD_IMAGES_GENERAL_FAIL,
  DELETE_IMAGE_GENERAL,
  DELETE_IMAGE_GENERAL_SUCCESS,
  DELETE_IMAGE_GENERAL_FAIL,
  REMOVE_GENERAL_IMAGES,
  SAVE_PRODUCT,
  SAVE_PRODUCT_SUCCESS,
  SAVE_PRODUCT_FAIL,
  EDIT_PRODUCT_VIEW,
  EDIT_PRODUCT_VIEW_SUCCESS,
  EDIT_PRODUCT_VIEW_FAIL,
  EMPTY_PRODUCT_DATA,
  SET_MAIN_IMAGE,
  GET_PRODUCT_FEE,
  GET_PRODUCT_FEE_FAIL,
  GET_PRODUCT_FEE_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
  product_id: null,
  product_image: [],
  loading: false,
  indexToRemove: null,
  saveload: false,
  product_data: null,
  currentTab: 1,
  admincommphysical: [],
  admincommdigital: [],
  midtrans_fee: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPLOAD_IMAGES_GENERAL:
      return {...state, loading: true};
    case UPLOAD_IMAGES_GENERAL_SUCCESS:
      // state.product_image.unshift(action.payload.product_image[0]);
      // if(action.no_of_images > state.product_image.length){
      //     return{ ...state, loading: true};
      // } else {
      //     return{ ...state, ...state.product_image, loading: false};
      // }
      return {
        ...state,
        product_image: action.payload.product_image,
        loading: false,
      };
    case UPLOAD_IMAGES_GENERAL_FAIL:
      return {...state, loading: false};
    case DELETE_IMAGE_GENERAL:
      state.indexToRemove = null;
      return {...state, loading: true};
    case DELETE_IMAGE_GENERAL_SUCCESS:
      for (let i = 0; i < state.product_image.length; i++) {
        if (state.product_image[i].image_id == action.image_id) {
          state.indexToRemove = i;
          break;
        }
      }
      if (state.indexToRemove >= 0) {
        state.product_image.splice(state.indexToRemove, 1);
      }
      return {...state, ...state.product_image, loading: false};
    case DELETE_IMAGE_GENERAL_FAIL:
      return {...state, loading: false};
    case REMOVE_GENERAL_IMAGES:
      return {...state, product_image: []};
    case EMPTY_PRODUCT_DATA:
      return {...state, product_data: null, loading: false};
    case SAVE_PRODUCT:
      return {...state, saveload: true};
    case SAVE_PRODUCT_SUCCESS:
      return {
        ...state,
        saveload: false,
        product_id: action.payload.prod_id,
        currentTab: action.nextTab,
      };
    case SAVE_PRODUCT_FAIL:
      return {...state, saveload: false};
    case EDIT_PRODUCT_VIEW:
      return {...state, loading: true, product_data: null};
    case EDIT_PRODUCT_VIEW_SUCCESS:
      return {
        ...state,
        loading: false,
        product_image: action.payload.data.images,
        product_data: action.payload.data,
      };
    case EDIT_PRODUCT_VIEW_FAIL:
      return {...state, loading: false};
    case SET_MAIN_IMAGE:
      return {...state, product_image: action.payload.product_images_details};
    case GET_PRODUCT_FEE:
      return {...state, loading: true};
    case GET_PRODUCT_FEE_SUCCESS:
      console.log(action.payload);
      return {
        ...state,
        loading: false,
        admincommphysical: action.payload.admincommphysical,
        admincommdigital: action.payload.admincommdigital,
        midtrans_fee: action.payload.midtrans_fee,
        dcommission: action.payload.dcommission,
        pcommission: action.payload.pcommission,
        admin_commission: action.payload.admin_commission,
      };
    case GET_PRODUCT_FEE_FAIL:
      return {...state, loading: false};

    default:
      return state;
  }
};
