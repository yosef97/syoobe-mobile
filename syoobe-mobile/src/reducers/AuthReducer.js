import {
  LOGOUT_USER,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAIL,
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  SOCIAL_LOGIN,
  SOCIAL_LOGIN_SUCCESS,
  SOCIAL_LOGIN_FAIL,
  GOOGLE_LOGIN,
  GOOGLE_LOGIN_SUCCESS,
  GOOGLE_LOGIN_FAIL,
  LOGIN_USER_FAIL_VERIFY,
  SET_VERIFY_FALSE,
  SEND_VERIFY_OTP,
  SEND_VERIFY_OTP_SUCCESS,
  SEND_VERIFY_OTP_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  loggedInEmail: '',
  loggedInUserImage: '',
  loggedInUserName: '',
  name: '',
  email: '',
  username: '',
  password: '',
  user: null,
  registerError: '',
  loading: false,
  reVerify: false,
  reVerifyMsg: '',
  otpSentToNumber: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGOUT_USER:
      return {...state, loading: true, registerError: ''};
    case LOGOUT_USER_SUCCESS:
      return {...state, username: '', password: '', loading: false};
    case LOGOUT_USER_FAIL:
      return {...state, password: '', loading: false};
    case EMAIL_CHANGED:
      return {...state, username: action.payload};
    case PASSWORD_CHANGED:
      return {...state, password: action.payload};

    case SEND_VERIFY_OTP:
      return {...state, loading: true, reVerify: false};
    case SEND_VERIFY_OTP_SUCCESS:
      return {
        ...state,
        loading: false,
        otpSentToNumber: action.payload.user_phone,
      };
    case SEND_VERIFY_OTP_FAIL:
      return {...state, password: '', loading: false};
    case LOGIN_USER_FAIL_VERIFY:
      return {
        ...state,
        password: '',
        reVerify: true,
        loading: false,
        reVerifyMsg: action.payload,
      };
    case SET_VERIFY_FALSE:
      return {...state, reVerify: false, reVerifyMsg: ''};

    case LOGIN_USER:
      return {...state, loading: true};
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        loggedInEmail: action.payload.user_email,
        loggedInUserImage: action.payload.user_image_url,
        loggedInUserName: action.payload.user_name,
        user: action.payload,
        username: '',
        password: '',
        loading: false,
      };
    case LOGIN_USER_FAIL:
      return {...state, password: '', loading: false};

    case SOCIAL_LOGIN:
      return {...state, loading: true};
    case SOCIAL_LOGIN_SUCCESS:
      return {
        ...state,
        loggedInEmail: action.payload.user_email,
        loggedInUserImage: action.payload.user_image_url,
        loggedInUserName: action.payload.user_name,
        user: action.payload,
        username: '',
        password: '',
        loading: false,
      };
    case SOCIAL_LOGIN_FAIL:
      return {...state, password: '', loading: false};
    case GOOGLE_LOGIN:
      return {...state, loading: true};
    case GOOGLE_LOGIN_SUCCESS:
      return {
        ...state,
        loggedInEmail: action.payload.user_email,
        // loggedInPassword: action.payload.user_password,
        loggedInUserImage: action.payload.user_image_url,
        loggedInUserName: action.payload.user_name,
        user: action.payload,
        username: '',
        password: '',
        loading: false,
      };
    case GOOGLE_LOGIN_FAIL:
      return {...state, password: '', loading: false};
    default:
      return state;
  }
};
