import {
  GET_BANK_DETAILS,
  GET_BANK_DETAILS_SUCCESS,
  GET_BANK_DETAILS_FAIL,
  CHANGE_BANK_NAME,
  CHANGE_ACCOUNT_HOLDER_NAME,
  CHANGE_ACCOUNT_NUMBER,
  CHANGE_IFSC_CODE,
  CHANGE_PAYPAL_ID,
  CHANGE_BANK_ADDRESS,
  SAVE_BANK_DETAILS,
  SAVE_BANK_DETAILS_SUCCESS,
  SAVE_BANK_DETAILS_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  loading: false,
  bank_address: '',
  paypal_id: '',
  account_number: '',
  ifsc_code: '',
  account_holder_name: '',
  bank_name: '',
  bank_info: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_BANK_DETAILS:
      return {
        ...state,
        loading: true,
        error: '',
        bank_address: '',
        paypal_id: '',
        account_number: '',
        ifsc_code: '',
        account_holder_name: '',
        bank_name: '',
        bank_info: {},
      };
    case GET_BANK_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        bank_info: action.payload,
        bank_name: action.payload.ub_bank_name,
        account_holder_name: action.payload.ub_account_holder_name,
        account_number: action.payload.ub_account_number,
        ifsc_code: action.payload.ub_ifsc_swift_code,
        paypal_id: action.payload.ub_user_paypalid,
        bank_address: action.payload.ub_bank_address,
      };
    case GET_BANK_DETAILS_FAIL:
      return {...state, loading: false, error: 'Some error, Please try again'};
    case CHANGE_BANK_NAME:
      return {...state, bank_name: action.payload};
    case CHANGE_ACCOUNT_HOLDER_NAME:
      return {...state, account_holder_name: action.payload};
    case CHANGE_ACCOUNT_NUMBER:
      return {...state, account_number: action.payload};
    case CHANGE_IFSC_CODE:
      return {...state, ifsc_code: action.payload};
    case CHANGE_PAYPAL_ID:
      return {...state, paypal_id: action.payload};
    case CHANGE_BANK_ADDRESS:
      return {...state, bank_address: action.payload};
    case SAVE_BANK_DETAILS:
      return {...state, loading: true, error: ''};
    case SAVE_BANK_DETAILS_SUCCESS:
      return {...state, loading: false, error: ''};
    case SAVE_BANK_DETAILS_FAIL:
      return {...state, loading: false, error: 'some error occured'};
    default:
      return state;
  }
};
