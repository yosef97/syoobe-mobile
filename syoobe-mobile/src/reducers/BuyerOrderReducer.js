import {
  GET_BUYER_ORDER_HISTORY,
  GET_BUYER_ORDER_HISTORY_SUCCESS,
  GET_BUYER_ORDER_HISTORY_FAIL,
  GET_ORDER_STATUS_LIST,
  GET_ORDER_STATUS_LIST_SUCCESS,
  GET_ORDER_STATUS_LIST_FAIL,
  EMPTY_ORDER_HISTORY,
  EMPTY_DOWNLOAD_HISTORY,
  GET_BUYER_DOWNLOAD_HISTORY,
  GET_BUYER_DOWNLOAD_HISTORY_SUCCESS,
  GET_BUYER_DOWNLOAD_HISTORY_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  download_list: [],
  endOfDownloads: false,
  total_page_downloads: null,
  no_downloads: false,
  endOfRecords: false,
  current_page: 1,
  total_page: null,
  status_list: [],
  no_orders: false,
  scrollLoading: false,
  loading: false,
  order_list: [],
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // case GET_BUYER_DOWNLOAD_HISTORY:
    //     if(action.search == 'new_search' || action.search == 'clear'){
    //         state.download_list = [];
    //         state.endOfDownloads = false;
    //     }
    //     if(state.download_list.length < 1){
    //         return { ...state, no_downloads: false, scrollLoading: false, loading: true};
    //     } else {
    //         return { ...state, no_downloads: false, loading: false, scrollLoading: true};
    //     }
    // case GET_BUYER_DOWNLOAD_HISTORY_SUCCESS:
    //     action.payload.downloadlist.forEach((element) => {
    //         state.download_list.push(element);
    //     })
    //     if(state.download_list.length == action.payload.total_records){
    //         state.endOfDownloads = true;
    //     }
    //     return { ...state, current_page:action.current_page,
    //         no_downloads: false,
    //         total_page_downloads: action.payload.total_page,
    //         ...state.download_list,
    //         loading: false,
    //         scrollLoading: false
    //     };
    // case GET_BUYER_DOWNLOAD_HISTORY_FAIL:
    //     return { ...state, no_downloads: true, loading: false, download_list:[]};

    case GET_BUYER_DOWNLOAD_HISTORY:
      return {
        ...state,
        no_downloads: false,
        scrollLoading: false,
        loading: true,
      };
    case GET_BUYER_DOWNLOAD_HISTORY_SUCCESS:
      return {
        ...state,
        current_page: action.current_page,
        no_downloads: false,
        total_page_downloads: action.payload.total_page,
        download_list: action.payload.downloadlist,
        loading: false,
        scrollLoading: false,
      };
    case GET_BUYER_DOWNLOAD_HISTORY_FAIL:
      return {...state, no_downloads: true, loading: false, download_list: []};

    //=============================================================================//

    // case GET_BUYER_ORDER_HISTORY:
    //     if(action.search == 'new_search' || action.search == 'clear'){
    //         state.order_list = [];
    //         state.endOfRecords = false;
    //     }
    //     if(state.order_list.length < 1){
    //         return { ...state, no_orders: false, scrollLoading: false, loading: true};
    //     } else {
    //         return { ...state, no_orders: false, loading: false, scrollLoading: true};
    //     }
    // case GET_BUYER_ORDER_HISTORY_SUCCESS:
    //     action.payload.orderlist.forEach((element) => {
    //         state.order_list.push(element);
    //     });
    //     if(state.order_list.length == action.payload.total_records){
    //         state.endOfRecords = true;
    //     }
    //     return { ...state, current_page:action.current_page,
    //             no_orders: false,
    //             total_page: action.payload.total_page,
    //             ...state.order_list,
    //             loading: false,
    //             scrollLoading: false,
    //         };
    // case GET_BUYER_ORDER_HISTORY_FAIL:
    //     return { ...state, no_orders: true, scrollLoading: false, loading: false, order_list:[]};

    case GET_BUYER_ORDER_HISTORY:
      return {
        ...state,
        no_orders: false,
        scrollLoading: false,
        loading: true,
        order_list: [],
      };
    case GET_BUYER_ORDER_HISTORY_SUCCESS:
      return {
        ...state,
        current_page: action.current_page,
        no_orders: false,
        total_page: action.payload.total_page,
        order_list: action.payload.orderlist,
        loading: false,
        scrollLoading: false,
      };
    case GET_BUYER_ORDER_HISTORY_FAIL:
      return {
        ...state,
        no_orders: true,
        scrollLoading: false,
        loading: false,
        order_list: [],
      };

    //==================================================================================================//

    case GET_ORDER_STATUS_LIST:
      return {...state, loading: true};
    case GET_ORDER_STATUS_LIST_SUCCESS:
      var data = {status_id: 0, status_name: 'Select Status'};
      action.payload.data.unshift(data);
      return {...state, status_list: action.payload.data, loading: false};
    case GET_ORDER_STATUS_LIST_FAIL:
      return {...state, status_list: [], loading: false};
    case EMPTY_ORDER_HISTORY:
      return {...state, order_list: [], endOfRecords: false};
    case EMPTY_DOWNLOAD_HISTORY:
      return {...state, download_list: [], endOfDownloads: false};
    default:
      return state;
  }
};
