import {
  GET_CATEGORY_LIST,
  GET_CATEGORY_LIST_SUCCESS,
  GET_CATEGORY_LIST_FAIL,
  EXPAND_CATEGORY,
  GET_PRODUCT_LIST_BY_CATEGORY,
  GET_PRODUCT_LIST_BY_CATEGORY_SUCCESS,
  GET_PRODUCT_LIST_BY_CATEGORY_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  loading: true,
  brand_details: [],
  price_array: null,
  product_condition: [],
  category_details: [],
  categories: [],
  products: [],
  no_products: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_CATEGORY_LIST:
      return {...state, loading: true, error: '', categories: []};
    case GET_CATEGORY_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        categories: action.payload.category_details,
      };
    case GET_CATEGORY_LIST_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Some error, Please try again',
        categories: [],
      };
    case EXPAND_CATEGORY:
      return {...state, loading: false, error: '', categories: action.payload};
    case GET_PRODUCT_LIST_BY_CATEGORY:
      return {
        ...state,
        loading: true,
        no_products: false,
        brand_details: [],
        price_array: null,
        product_condition: [],
        category_details: [],
        products: [],
        error: '',
      };
    case GET_PRODUCT_LIST_BY_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        no_products: false,
        error: '',
        category_details: action.payload.category_details,
        products: action.payload.products,
        product_condition: action.payload.product_condition,
        price_array: action.payload.price_array,
        brand_details: action.payload.brand_details,
      };
    case GET_PRODUCT_LIST_BY_CATEGORY_FAIL:
      return {
        ...state,
        loading: false,
        no_products: true,
        price_array: null,
        product_condition: [],
        category_details: [],
        products: [],
        brand_details: [],
        error: 'Some error, Please try again',
      };
    default:
      return state;
  }
};
