import {
  GET_COUNTRIES,
  GET_COUNTRIES_SUCCESS,
  GET_COUNTRIES_FAIL,
  GET_STATES,
  GET_STATES_SUCCESS,
  GET_STATES_FAIL,
  GET_SHIPS_TO_COUNTRIES_SUCCESS,
  GET_SHIPS_TO_COUNTRIES_FAIL,
  GET_SHIPS_TO_COUNTRIES,
  GET_SHIPPING_COMPANY,
  GET_SHIPPING_COMPANY_SUCCESS,
  GET_SHIPPING_COMPANY_FAIL,
  GET_CARRIER_SERVICE,
  GET_CARRIER_SERVICE_SUCCESS,
  GET_CARRIER_SERVICE_FAIL,
  GET_SPECIFICATIONS,
  GET_SPECIFICATIONS_SUCCESS,
  GET_SPECIFICATIONS_FAIL,
  GET_PRODUCT_FILTERS_FAIL,
  GET_PRODUCT_FILTERS_SUCCESS,
  GET_PRODUCT_FILTERS,
  GET_RELATED_PRODUCTS,
  GET_RELATED_PRODUCTS_SUCCESS,
  GET_RELATED_PRODUCTS_FAIL,
  LOAD_OPTIONS,
  LOAD_OPTIONS_SUCCESS,
  LOAD_OPTIONS_FAIL,
  CREATE_OPTION_SUCCESS,
  CREATE_OPTION_FAIL,
  CREATE_OPTION,
  LOAD_COMPATIBILITY,
  LOAD_COMPATIBILITY_SUCCESS,
  LOAD_COMPATIBILITY_FAIL,
  LOAD_COMPATIBILITY_CONTENT,
  LOAD_COMPATIBILITY_CONTENT_SUCCESS,
  LOAD_COMPATIBILITY_CONTENT_FAIL,
  GET_PROVINCE,
  GET_PROVINCE_SUCCESS,
  GET_PROVINCE_FAIL,
  GET_PROVINCE_SELECTED,
  GET_CITY,
  GET_CITY_SUCCESS,
  GET_CITY_SELECTED,
  GET_DISTRICT_SELECTED,
  GET_CITY_FAIL,
  GET_DISTRICT_FAIL,
  GET_DISTRICT,
  GET_DISTRICT_SUCCESS,
  SET_COMPONENT_REDIRECT,
  GET_COUNTRY_SELECTED,
} from '../actions/types';

const INITIAL_STATE = {
  group_id: null,
  compatInnerLoading: false,
  compatInnerContent: [],
  compatData: [],
  optionCreated: false,
  createOptionsLoader: false,
  options: [],
  loading: false,
  related_products: [],
  product_filters: [],
  specification_details: [],
  carrier_service: [],
  companies: [],
  ships_to_countries: [],
  countries: [],
  selectedCountry: {},
  states: [],
  province: {
    provinces: [],
    selectedProvince: {},
  },
  city: {
    cities: [],
    selectedCity: {},
  },
  district: {
    districts: [],
    selectedDistrict: {},
  },
  component: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_COUNTRIES:
      return {...state, error: '', loading: true};
    case GET_COUNTRY_SELECTED:
      return {
        ...state,
        selectedCountry: action.payload,
        error: '',
        loading: false,
      };
    case GET_COUNTRIES_SUCCESS:
      return {...state, error: '', countries: action.payload, loading: false};
    case GET_COUNTRIES_FAIL:
      return {
        ...state,
        error: 'Terjadi kesalahan silahkan ulangi kembali',
        loading: false,
      };
    case GET_STATES:
      return {...state, error: ''};
    case GET_STATES_SUCCESS:
      return {...state, error: '', states: action.payload};
    case GET_STATES_FAIL:
      return {...state, error: 'Terjadi kesalahan silahkan ulangi kembali'};
    case GET_PROVINCE:
      return {...state, error: '', loading: true};
    case GET_PROVINCE_SUCCESS:
      return {
        ...state,
        error: '',
        province: {
          ...state.province,
          provinces: action.payload,
        },
        loading: false,
      };
    case GET_PROVINCE_FAIL:
      return {
        ...state,
        error: 'Terjadi kesalahan saat mengambil data',
        loading: false,
      };
    case GET_PROVINCE_SELECTED:
      return {
        ...state,
        error: '',
        province: {
          ...state.province,
          selectedProvince: action.payload,
        },
        loading: false,
      };
    case GET_CITY:
      return {...state, error: '', loading: true};
    case GET_CITY_SUCCESS:
      return {
        ...state,
        error: '',
        city: {
          ...state.city,
          cities: action.payload,
        },
        loading: false,
      };
    case GET_CITY_FAIL:
      return {
        ...state,
        error: 'Terjadi kesalahan saat mengambil data',
        loading: false,
      };
    case GET_CITY_SELECTED:
      return {
        ...state,
        error: '',
        city: {
          ...state.city,
          selectedCity: action.payload,
        },
        loading: false,
      };
    case GET_DISTRICT:
      return {...state, error: '', loading: true};
    case GET_DISTRICT_SUCCESS:
      return {
        ...state,
        error: '',
        district: {
          ...state.district,
          districts: action.payload,
        },
        loading: false,
      };
    case GET_DISTRICT_FAIL:
      return {
        ...state,
        error: 'Terjadi kesalahan saat mengambil data',
        loading: false,
      };
    case GET_DISTRICT_SELECTED:
      return {
        ...state,
        error: '',
        district: {
          ...state.district,
          selectedDistrict: action.payload,
        },
        loading: false,
      };
    case SET_COMPONENT_REDIRECT:
      return {...state, component: action.payload, error: '', loading: false};
    case GET_SHIPS_TO_COUNTRIES:
      return {...state, error: '', loading: true};
    case GET_SHIPS_TO_COUNTRIES_SUCCESS:
      return {
        ...state,
        error: '',
        ships_to_countries: action.payload,
        loading: false,
      };
    case GET_SHIPS_TO_COUNTRIES_FAIL:
      return {
        ...state,
        error: 'Terjadi kesalahan silahkan ulangi kembali',
        loading: false,
      };
    case GET_SHIPPING_COMPANY:
      return {...state, error: '', loading: true};
    case GET_SHIPPING_COMPANY_SUCCESS:
      return {...state, error: '', companies: action.payload, loading: false};
    case GET_SHIPPING_COMPANY_FAIL:
      return {
        ...state,
        error: 'Terjadi kesalahan silahkan ulangi kembali',
        loading: false,
      };
    case GET_CARRIER_SERVICE:
      return {...state, error: '', loading: true};
    case GET_CARRIER_SERVICE_SUCCESS:
      return {
        ...state,
        error: '',
        carrier_service: action.payload,
        loading: false,
      };
    case GET_CARRIER_SERVICE_FAIL:
      return {
        ...state,
        error: 'Terjadi kesalahan silahkan ulangi kembali',
        loading: false,
      };
    case GET_SPECIFICATIONS:
      return {...state, loading: true};
    case GET_SPECIFICATIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        specification_details: action.payload.specification_details,
      };
    case GET_SPECIFICATIONS_FAIL:
      return {...state, loading: false};
    case GET_PRODUCT_FILTERS:
      return {...state, loading: true};
    case GET_PRODUCT_FILTERS_SUCCESS:
      return {...state, loading: false, product_filters: action.payload.data};
    case GET_PRODUCT_FILTERS_FAIL:
      return {...state, loading: false};
    case GET_RELATED_PRODUCTS:
      return {...state, loading: true};
    case GET_RELATED_PRODUCTS_SUCCESS:
      return {...state, loading: false, related_products: action.payload.data};
    case GET_RELATED_PRODUCTS_FAIL:
      return {...state, loading: false};
    case CREATE_OPTION:
      return {...state, optionCreated: false, createOptionsLoader: true};
    case CREATE_OPTION_SUCCESS:
      return {...state, optionCreated: true, createOptionsLoader: false};
    case CREATE_OPTION_FAIL:
      return {...state, optionCreated: false, createOptionsLoader: false};
    case LOAD_OPTIONS:
      return {...state, optionCreated: false, loading: true};
    case LOAD_OPTIONS_SUCCESS:
      return {
        ...state,
        optionCreated: false,
        loading: false,
        options: action.payload.options,
      };
    case LOAD_OPTIONS_FAIL:
      return {...state, optionCreated: false, loading: false};
    case LOAD_COMPATIBILITY:
      return {...state, loading: true, compatData: []};
    case LOAD_COMPATIBILITY_SUCCESS:
      return {...state, loading: false, compatData: action.payload.data};
    case LOAD_COMPATIBILITY_FAIL:
      return {...state, loading: false};
    case LOAD_COMPATIBILITY_CONTENT:
      return {...state, compatInnerLoading: true};
    case LOAD_COMPATIBILITY_CONTENT_SUCCESS:
      return {
        ...state,
        compatInnerLoading: false,
        compatInnerContent: action.payload.data,
        group_id: action.payload.group_id,
      };
    case LOAD_COMPATIBILITY_CONTENT_FAIL:
      return {...state, compatInnerLoading: false};
    default:
      return state;
  }
};
