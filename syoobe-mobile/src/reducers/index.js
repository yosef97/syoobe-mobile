import {combineReducers} from 'redux';
import AuthReducers from './AuthReducer';
import HomeReducers from './HomeReducers';
import CartReducer from './CartReducer';
import CategoryReducer from './CategoryReducer';
import DashboardReducer from './DashboardReducer';
import FavouriteReducer from './FavouriteReducer';
import ProfileReducer from './ProfileReducer';
import Country_States_Reducer from './Country_States_Reducer';
import Bankreducer from './Bankreducer';
import ForgotPasswordReducer from './ForgotPasswordReducer';
import CreateShopReducer from './CreateShopReducer';
import SellerProductReducer from './SellerProductReducer';
import BuyerOrderReducer from './BuyerOrderReducer';
import OrderDetailReducer from './OrderDetailReducer';
import BrandsReducer from './BrandsReducer';
import ProcessingTimeReducer from './ProcessingTimeReducer';
import AddProductReducer from './AddProductReducer';
import WalletReducer from './WalletReducer';
import PaymentReducer from './PaymentReducer';
import RegistrationReducer from './RegistrationReducer';
import NotificationReducer from './NotificationReducer';
import ShippingReducer from './ShippingReducer';

export default combineReducers({
  auth: AuthReducers,
  reg: RegistrationReducer,
  home: HomeReducers,
  cart: CartReducer,
  category: CategoryReducer,
  dashboard: DashboardReducer,
  fav: FavouriteReducer,
  profile: ProfileReducer,
  country: Country_States_Reducer,
  bank: Bankreducer,
  forgotPassword: ForgotPasswordReducer,
  createShop: CreateShopReducer,
  sellerProducts: SellerProductReducer,
  buyerOrders: BuyerOrderReducer,
  orderDetails: OrderDetailReducer,
  brands: BrandsReducer,
  process: ProcessingTimeReducer,
  addProduct: AddProductReducer,
  wallet: WalletReducer,
  // message: MessageReducer,
  payment: PaymentReducer,
  notification: NotificationReducer,
  shipping: ShippingReducer,
});
