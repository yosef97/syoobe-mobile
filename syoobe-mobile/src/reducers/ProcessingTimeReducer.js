import {
  GET_PROCESSING_TIME,
  GET_PROCESSING_TIME_SUCCESS,
  GET_PROCESSING_TIME_FAIL,
} from '../actions/types';

const INITIAL_STATE = {processing_time: [], loading: true};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_PROCESSING_TIME:
      return {...state, processing_time: [], loading: true};
    case GET_PROCESSING_TIME_SUCCESS:
      return {
        ...state,
        processing_time: action.payload.processing_time,
        loading: false,
      };
    case GET_PROCESSING_TIME_FAIL:
      return {...state, processing_time: [], loading: false};
    default:
      return state;
  }
};
