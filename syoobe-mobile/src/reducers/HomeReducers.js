import {
  GET_HOME_PRODUCTS,
  GET_HOME_PRODUCTS_SUCCESS,
  GET_HOME_PRODUCTS_FAIL,
  GET_HOME_TOKEN_PRODUCTS,
  GET_HOME_PRODUCTS_TOKEN_SUCCESS,
  GET_HOME_PRODUCTS_TOKEN_FAIL,
  GET_RECENTLY_VIEWED_SUCCESS,
  GET_RECENTLY_VIEWED_FAIL,
  GET_RECENTLY_VIEWED,
  SHOW_SEARCH_FIELD,
  CLEAR_HOME_PRODUCTS,
  GET_HOME_PRODUCTS_FAIL_NO_INTERNET,
} from '../actions/types';

const INITIAL_STATE = {
  loading: true,
  homeProducts: [],
  recentlyViewed: [],
  recentLoading: true,
  showSearch: false,
  bannerImages: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_HOME_PRODUCTS:
      return {...state, loading: true, error: '', homeProducts: []};
    case GET_HOME_PRODUCTS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        homeProducts: action.payload,
        bannerImages: action.bannerImages,
      };
    case GET_HOME_PRODUCTS_FAIL:
      return {...state, loading: false, homeProducts: 'logout'};

    case GET_HOME_TOKEN_PRODUCTS:
      return {...state, loading: true, error: '', homeProducts: []};
    case GET_HOME_PRODUCTS_TOKEN_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        homeProducts: action.payload,
        bannerImages: action.bannerImages,
      };
    case GET_HOME_PRODUCTS_TOKEN_FAIL:
      return {...state, loading: false, homeProducts: 'logout'};

    case GET_HOME_PRODUCTS_FAIL_NO_INTERNET:
      return {...state, loading: false, homeProducts: []};
    case CLEAR_HOME_PRODUCTS:
      return {...state, loading: false, homeProducts: [], recentlyViewed: []};
    case GET_RECENTLY_VIEWED:
      return {...state, recentLoading: true};
    case GET_RECENTLY_VIEWED_SUCCESS:
      return {...state, recentLoading: false, recentlyViewed: action.payload};
    case GET_RECENTLY_VIEWED_FAIL:
      return {...state, recentLoading: false};
    case SHOW_SEARCH_FIELD:
      return {...state, showSearch: !state.showSearch};
    default:
      return state;
  }
};
