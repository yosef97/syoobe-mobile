import { act } from 'react-test-renderer';
import {
  ADD_TO_CART,
  ADD_TO_CART_SUCCESS,
  ADD_TO_CART_FAIL,
  VIEW_CART,
  VIEW_CART_SUCCESS,
  VIEW_CART_FAIL,
  DELETE_CART_ITEM,
  DELETE_CART_ITEM_SUCCESS,
  DELETE_CART_ITEM_FAIL,
  EDIT_CART_ITEM,
  EDIT_CART_ITEM_SUCCESS,
  EDIT_CART_ITEM_FAIL,
  APPLY_COUPON_CART,
  APPLY_COUPON_CART_SUCCESS,
  APPLY_COUPON_CART_FAIL,
  REMOVE_COUPON_CART,
  REMOVE_COUPON_CART_SUCCESS,
  REMOVE_COUPON_CART_FAIL,
  APPLY_REWARD_POINT,
  APPLY_REWARD_POINT_SUCCESS,
  APPLY_REWARD_POINT_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  cart_error: 'unchecked',
  reward_point_message: null,
  cart_max_rewards_points: null,
  reward_data: null,
  available_reward_point: null,
  price_currency: null,
  net_payable_amount: null,
  coupon_message: null,
  isCouponApplied: false,
  coupon_value: null,
  coupon_name: null,
  addToCartLoading: false,
  loading: false,
  response: null,
  message: null,
  cart_count: null,
  tax_data: null,
  cart_total_price: null,
  cart_items: [],
  editCartLoading: false,
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return {
        ...state,
        cart_error: 'unchecked',
        loading: true,
        addToCartLoading: true,
      };
    case ADD_TO_CART_SUCCESS:
      return {
        ...state,
        loading: false,
        cart_error: false,
        // message: action.payload.msg,
        addToCartLoading: false,
      };
    case ADD_TO_CART_FAIL:
      return {
        ...state,
        loading: false,
        cart_error: true,
        addToCartLoading: false,
        // message: 'Some error occured, Please try again'
      };
    case VIEW_CART:
      return {...state, loading: true, reward_data: null};
    case 'CHANGE_PAYABLE_AMOUNT':
      console.log(action.payload.amount);
      return {
        ...state,
        loading: false,
        net_payable_amount: action.payload,
      };
    case VIEW_CART_SUCCESS:
      if (action.payload.coupon_data != null) {
        state.isCouponApplied = true;
        state.coupon_name = action.payload.coupon_data.name;
        state.coupon_value = action.payload.coupon_data.value;
      } else {
        state.isCouponApplied = false;
      }
      // if(action.payload.reward_data != null){
      //     state.reward_data = action.payload.reward_data.value
      // }
      return {
        ...state,
        message: null,
        loading: false,
        tax_data: action.payload.tax_data,
        cart_count: action.payload.cart_count,
        cart_total_price: action.payload.new_cart_product_price_total,
        net_payable_amount: action.payload.net_payable_amount,
        cart_items: action.payload.cart,
        price_currency: action.payload.price_currency,
        available_reward_point: action.payload.available_reward_point,
        cart_max_rewards_points: action.payload.cart_max_rewards_points,
        reward_data:
          action.payload.reward_data != null
            ? action.payload.reward_data.value
            : null,
        reward_point_message: null,
      };
    case VIEW_CART_FAIL:
      return {...state, cart_count: null, tax_data: null, loading: false};
    case DELETE_CART_ITEM:
      return {...state, loading: true};
    case DELETE_CART_ITEM_SUCCESS:
      return {
        ...state,
        loading: false,
        tax_data: action.payload.tax_data,
        cart_count: action.payload.cart_count,
        cart_items: action.payload.cart,
        cart_total_price: action.payload.cart_prod_price,
        net_payable_amount: action.payload.net_payable_amount,
        cart_max_rewards_points: action.payload.cart_max_rewards_points,
        reward_data:
          action.payload.reward_data != null
            ? action.payload.reward_data.value
            : null,
        message: action.payload.msg,
      };
    case DELETE_CART_ITEM_FAIL:
      return {
        ...state,
        loading: false,
        // message: 'Some error occured, Please try again'
      };

    // Edit cart
    case EDIT_CART_ITEM:
      return {...state, editCartLoading: true};
    case EDIT_CART_ITEM_SUCCESS:
      // const cart_total_price = (action.payload.cart_product_price_total).substring(2);
      return {
        ...state,
        editCartLoading: false,
        tax_data: action.payload.tax_data,
        cart_items: action.payload.cart,
        net_payable_amount: action.payload.cart_product_price_total,
        cart_max_rewards_points: action.payload.cart_max_rewards_points,
        reward_data:
          action.payload.reward_data != null
            ? action.payload.reward_data.value
            : null,
        cart_total_price: action.payload.subTotal,
      };
    case EDIT_CART_ITEM_FAIL:
      return {
        ...state,
        editCartLoading: false,
        // message: 'Some error occured, Please try again'
      };
    case APPLY_COUPON_CART:
      return {...state, loading: true, coupon_message: null};
    case APPLY_COUPON_CART_SUCCESS:
      return {
        ...state,
        loading: false,
        isCouponApplied: true,
        coupon_name: action.payload.cart_prod_price_deatsils.coupon,
        coupon_value: action.payload.cart_prod_price_deatsils.coupon_value,
        net_payable_amount:
          action.payload.cart_prod_price_deatsils.net_total_after_discount,
        cart_total_price:
          action.payload.cart_prod_price_deatsils.net_total_without_discount,
        coupon_message: action.payload.msg,
      };
    case APPLY_COUPON_CART_FAIL:
      return {...state, loading: false};
    case REMOVE_COUPON_CART:
      return {...state, loading: true};
    case REMOVE_COUPON_CART_SUCCESS:
      return {
        ...state,
        loading: false,
        isCouponApplied: false,
        coupon_value: null,
        coupon_name: null,
        net_payable_amount: action.payload.data.net_total_without_discount,
        coupon_message: null,
      };
    case REMOVE_COUPON_CART_FAIL:
      return {...state, loading: false};
    case APPLY_REWARD_POINT:
      return {...state, loading: true, reward_point_message: null};
    case APPLY_REWARD_POINT_SUCCESS:
      return {
        ...state,
        loading: false,
        reward_data: action.payload.product_details_with_price.reward_point,
        net_payable_amount:
          action.payload.product_details_with_price
            .net_total_after_discount_after_reward,
        cart_total_price:
          action.payload.product_details_with_price
            .net_total_without_discount_without_reward,
        reward_point_message: 'Poin Hadiah berhasil digunakan',
      };
    case APPLY_REWARD_POINT_FAIL:
      return {...state, loading: false, reward_point_message: action.payload};
    default:
      return state;
  }
};
