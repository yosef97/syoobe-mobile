import {
  REGISTER_USER,
  NAME_CHANGED,
  USERNAME_CHANGED,
  REG_PASSWORD_CHANGED,
  REG_EMAIL_CHANGED,
  REGISTRATION_SUCCESS,
  REGISTRATION_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  name: '',
  email: '',
  username: '',
  password: '',
  user: null,
  loading: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case NAME_CHANGED:
      return {...state, name: action.payload};
    case REG_EMAIL_CHANGED:
      return {...state, email: action.payload};
    case REG_PASSWORD_CHANGED:
      return {...state, password: action.payload};
    case USERNAME_CHANGED:
      return {...state, username: action.payload};
    case REGISTER_USER:
      return {...state, loading: true};
    case REGISTRATION_SUCCESS:
      return {
        ...state,
        user: action.payload,
        username: '',
        password: '',
        name: '',
        email: '',
        loading: false,
      };
    case REGISTRATION_FAIL:
      return {...state, username: '', name: '', email: '', loading: false};
    default:
      return state;
  }
};
