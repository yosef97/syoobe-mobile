import {
  GET_DASHBOARD,
  GET_DASHBOARD_SUCCESS,
  GET_DASHBOARD_FAIL,
} from '../actions/types';

const INITIAL_STATE = {loading: true, buyerData: null, sellerData: null};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_DASHBOARD:
      return {...state, loading: true, error: ''};
    case GET_DASHBOARD_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        buyerData: action.payload.buyer,
        sellerData: action.payload.seller,
      };
    case GET_DASHBOARD_FAIL:
      return {...state, loading: false, error: action.payload};
    default:
      return state;
  }
};
