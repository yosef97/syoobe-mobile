import {
  FORGOT_PASSWORD_CHANGED,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAIL,
} from '../actions/types';

const INITIAL_STATE = {username: '', error: '', loading: false};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FORGOT_PASSWORD_CHANGED:
      return {...state, username: action.payload};
    case FORGOT_PASSWORD:
      return {...state, loading: true};
    case FORGOT_PASSWORD_SUCCESS:
      return {...state, loading: false};
    case FORGOT_PASSWORD_FAIL:
      return {...state, loading: false};
    default:
      return state;
  }
};
