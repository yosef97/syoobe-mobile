import {
  CHANGE_SHOP_NAME,
  CHANGE_URL_KEYWORDS,
  SHOP_DESCRIPTION,
  SHOP_CITY,
  SHOP_ANNOUNCEMENT,
  MESSAGE_TO_BUYER,
  UPDATE_LOGO,
  UPDATE_BANNER,
  CLOSE_LOGO,
  CLOSE_BANNER,
  DISPLAY_STATUS,
  SHOP_IS_PROD_TAX,
  WELCOME_MESSAGE,
  PAYMENT_POLICY,
  DELIVERY_POLICY,
  REFUND_POLICY,
  ADDITIONAL_INFORMATION,
  SELLER_INFORMATION,
  SHOPING_NPWP,
  META_TAG_TITLE,
  META_TAG_KEYWORDS,
  META_TAG_DESCRIPTION,
  SET_COUNTRY,
  SET_STATE,
  SAVE_SHOP,
  SAVE_SHOP_SUCCESS,
  SAVE_SHOP_FAIL,
  SKIP_STEP_2,
  SKIP_STEP_3,
  GET_SHOP_DETAILS,
  GET_SHOP_DETAILS_SUCCESS,
  GET_SHOP_DETAILS_FAIL,
  NO_SHOP_DETAILS_FOR_USER,
  SHOP_STATE_NAME,
  SHOP_DISTRICT_ID,
  SHOP_DISTRICT_NAME,
  SHOP_CITY_ID,
} from '../actions/types';

const INITIAL_STATE = {
  shop_id: '',
  message: '',
  country_id: '',
  state_id: '',
  meta_tag_description: '',
  meta_tag_keywords: '',
  meta_tag_title: '',
  seller_info: '',
  shop_npwp: '',
  additional_info: '',
  refund_policy: '',
  delivery_policy: '',
  payment_policy: '',
  welcome_message: '',
  shop_country: '',
  shop_state: '',
  shop_vendor_display_status: true,
  is_prod_tax: false,
  banner: null,
  logo: null,
  loading: true,
  message_to_buyer: '',
  announcement: '',
  flag: null,
  flag2: null,
  shop_city: '',
  description: '',
  shop_name: '',
  url_keywords: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_SHOP_DETAILS:
      return {
        ...state,
        country_id: '',
        meta_tag_description: '',
        meta_tag_keywords: '',
        state_id: '',
        meta_tag_title: '',
        seller_info: '',
        shop_npwp: '',
        additional_info: '',
        refund_policy: '',
        delivery_policy: '',
        payment_policy: '',
        welcome_message: '',
        shop_country: '',
        shop_state: '',
        shop_state_name: '',
        shop_district_id: '',
        shop_district_name: '',
        shop_city_id: '',
        shop_city: '',
        shop_vendor_display_status: true,
        is_prod_tax: false,
        banner: null,
        logo: null,
        message_to_buyer: '',
        announcement: '',
        description: '',
        shop_name: '',
        url_keywords: '',
        loading: true,
      };
    case GET_SHOP_DETAILS_SUCCESS:
      var flag;
      if (action.payload.shop_info.display_status === 1) {
        flag = true;
      } else if (action.payload.shop_info.display_status === 0) {
        flag = false;
      }

      var flag2;
      if (action.payload.shop_info.shop_is_prod_tax === 1) {
        flag2 = true;
      } else if (action.payload.shop_info.shop_is_prod_tax === 0) {
        flag2 = false;
      }

      return {
        ...state,
        shop_id: action.payload.shop_info.shop_id,
        url_keywords: action.payload.shop_info.url_keyword,
        shop_name: action.payload.shop_info.shop_name,
        description: action.payload.shop_info.description,
        shop_city: action.payload.shop_info.shop_city,
        announcement: action.payload.shop_info.announcement,
        message_to_buyer: action.payload.shop_info.message_to_buyers,
        shop_vendor_display_status: flag,
        is_prod_tax: flag2,
        banner: action.payload.shop_info.banner,
        logo: action.payload.shop_info.logo,
        country_id: action.payload.shop_info.country,
        state_id: action.payload.shop_info.state,
        welcome_message: action.payload.shop_info.welcome_message,
        payment_policy: action.payload.shop_info.payment_policy,
        delivery_policy: action.payload.shop_info.delivery_policy,
        refund_policy: action.payload.shop_info.refund_policy,
        additional_info: action.payload.shop_info.additional_info,
        seller_info: action.payload.shop_info.seller_info,
        shop_npwp: action.payload.shop_info.shop_npwp,
        meta_tag_title: action.payload.shop_info.meta_tag_title,
        meta_tag_keywords: action.payload.shop_info.meta_tag_keywords,
        meta_tag_description: action.payload.shop_info.meta_tag_description,
        shop: action.payload.shop_info.shops,
        loading: false,
      };
    case GET_SHOP_DETAILS_FAIL:
      return {...state, loading: false, message: action.payload.msg};
    // case NO_SHOP_DETAILS_FOR_USER:
    //     return{ ...state, loading: false, message: action.payload.msg};
    case CHANGE_SHOP_NAME:
      return {...state, shop_name: action.payload};
    case SET_COUNTRY:
      return {...state, country_id: action.payload};
    case SET_STATE:
      return {...state, state_id: action.payload};
    case CHANGE_URL_KEYWORDS:
      return {...state, url_keywords: action.payload};
    case SHOP_DESCRIPTION:
      return {...state, description: action.payload};
    case SHOP_CITY:
      return {...state, shop_city: action.payload};
    case SHOP_ANNOUNCEMENT:
      return {...state, announcement: action.payload};
    case MESSAGE_TO_BUYER:
      return {...state, message_to_buyer: action.payload};
    case UPDATE_LOGO:
      return {...state, logo: action.payload};
    case UPDATE_BANNER:
      return {...state, banner: action.payload};
    case CLOSE_LOGO:
      return {...state, logo: null};
    case CLOSE_BANNER:
      return {...state, banner: null};
    case DISPLAY_STATUS:
      return {
        ...state,
        shop_vendor_display_status: !state.shop_vendor_display_status,
      };
    case SHOP_IS_PROD_TAX: {
      return {
        ...state,
        is_prod_tax: !state.is_prod_tax,
      };
    }
    case WELCOME_MESSAGE:
      return {...state, welcome_message: action.payload};
    case PAYMENT_POLICY:
      return {...state, payment_policy: action.payload};
    case DELIVERY_POLICY:
      return {...state, delivery_policy: action.payload};
    case REFUND_POLICY:
      return {...state, refund_policy: action.payload};
    case ADDITIONAL_INFORMATION:
      return {...state, additional_info: action.payload};
    case SELLER_INFORMATION:
      return {...state, seller_info: action.payload};
    case SHOPING_NPWP:
      return {...state, shop_npwp: action.payload};
    case META_TAG_TITLE:
      return {...state, meta_tag_title: action.payload};
    case META_TAG_KEYWORDS:
      return {...state, meta_tag_keywords: action.payload};
    case META_TAG_DESCRIPTION:
      return {...state, meta_tag_description: action.payload};
    case SHOP_STATE_NAME:
      return {...state, shop_state_name: action.payload};
    case SHOP_DISTRICT_ID:
      return {...state, shop_district_id: action.payload};
    case SHOP_DISTRICT_NAME:
      return {...state, shop_district_name: action.payload};
    case SHOP_CITY_ID:
      return {...state, shop_city_id: action.payload};
    case SAVE_SHOP:
      return {...state, loading: true};
    case SAVE_SHOP_SUCCESS:
      return {
        ...state,
        country_id: '',
        meta_tag_description: '',
        meta_tag_keywords: '',
        state_id: '',
        meta_tag_title: '',
        seller_info: '',
        shop_npwp: '',
        additional_info: '',
        refund_policy: '',
        delivery_policy: '',
        payment_policy: '',
        welcome_message: '',
        shop_country: '',
        shop_state: '',
        shop_vendor_display_status: true,
        is_prod_tax: false,
        banner: null,
        logo: null,
        loading: false,
        message_to_buyer: '',
        announcement: '',
        shop_city: '',
        description: '',
        shop_name: '',
        url_keywords: '',
      };
    case SAVE_SHOP_FAIL:
      return {...state, loading: false, message: action.payload.msg};
    case SKIP_STEP_2:
      return {
        ...state,
        banner: null,
        logo: null,
        message_to_buyer: '',
        announcement: '',
        shop_city: '',
        description: '',
        country_id: '',
        state_id: '',
      };
    case SKIP_STEP_3:
      return {
        ...state,
        seller_info: '',
        shop_npwp: '',
        additional_info: '',
        refund_policy: '',
        delivery_policy: '',
        payment_policy: '',
        welcome_message: '',
      };
    default:
      return state;
  }
};
