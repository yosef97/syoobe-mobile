import {
  SET_FAVOURITE,
  SET_FAVOURITE_SUCCESS,
  SET_FAVOURITE_FAIL,
  SET_UNFAVOURITE,
  SET_UNFAVOURITE_SUCCESS,
  SET_UNFAVOURITE_FAIL,
  GET_FAVOURITES,
  GET_FAVOURITES_SUCCESS,
  GET_FAVOURITES_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  loading: true,
  digital_favourites: [],
  physical_favourites: [],
  listname: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_FAVOURITE:
      return {...state, loading: true, error: ''};
    case SET_FAVOURITE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        favoriteIndex: action.payload,
      };
    case SET_FAVOURITE_FAIL:
      return {...state, loading: false, error: 'Some error, Please try again'};
    case SET_UNFAVOURITE:
      return {...state, loading: true, error: ''};
    case SET_UNFAVOURITE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        unfavoriteIndex: action.payload,
      };
    case SET_UNFAVOURITE_FAIL:
      return {...state, loading: false, error: 'Some error, Please try again'};
    case GET_FAVOURITES:
      return {
        ...state,
        loading: true,
        digital_favourites: [],
        physical_favourites: [],
      };
    case GET_FAVOURITES_SUCCESS:
      action.payload.data.forEach((item) => {
        if (item.prod_requires_shipping == 1) {
          state.physical_favourites.push(item);
        } else {
          state.digital_favourites.push(item);
        }
      });
      return {
        ...state,
        loading: false,
        listname: action.payload.list_name,
        ...state.digital_favourites,
        ...state.physical_favourites,
      };
    case GET_FAVOURITES_FAIL:
      return {...state, loading: false};
    default:
      return state;
  }
};
