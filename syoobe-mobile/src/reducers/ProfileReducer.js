import {
  GET_PROFILE_DETAILS,
  GET_PROFILE_DETAILS_SUCCESS,
  GET_PROFILE_DETAILS_FAIL,
  UPLOAD_PROFILE_PICTURE,
  UPLOAD_PROFILE_PICTURE_SUCCESS,
  UPLOAD_PROFILE_PICTURE_FAIL,
  PERSONAL_NAME_CHANGED,
  PERSONAL_PHONE_CHANGED,
  PERSONAL_CITY_CHANGED,
  UPDATE_PROFILE,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_FAIL,
  CURRENT_PASSWORD,
  NEW_PASSWORD,
  CONFIRM_NEW_PASSWORD,
  SAVE_CHANGE_PASSWORD,
  SAVE_CHANGE_PASSWORD_SUCCESS,
  SAVE_CHANGE_PASSWORD_FAIL,
  NEW_EMAIL,
  CONFIRM_NEW_EMAIL,
  CURRENT_PASSWORD_CHANGE_EMAIL,
  SAVE_CHANGE_EMAIL,
  SAVE_CHANGE_EMAIL_SUCCESS,
  SAVE_CHANGE_EMAIL_FAIL,
  SELLER_FULL_NAME,
  SELLER_ADDRESS_LINE_1,
  SELLER_ADDRESS_LINE_2,
  SELLER_CITY,
  SELLER_ZIP_CODE,
  SELLER_PHONE_NUMBER,
  GET_ADDRESS_INFO,
  GET_ADDRESS_INFO_SUCCESS,
  GET_ADDRESS_INFO_FAIL,
  SAVE_ADDRESS_INFO,
  SAVE_ADDRESS_INFO_SUCCESS,
  SAVE_ADDRESS_INFO_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  updated: false,
  sellerInfo: {},
  country_id: '',
  state_id: '',
  seller_phone_number: '',
  seller_zip_code: '',
  seller_city: '',
  seller_address2: '',
  seller_address1: '',
  seller_full_name: '',
  status: null,
  current_password_change_email: '',
  confirm_new_email: '',
  new_email: '',
  confirm_new_password: '',
  new_password: '',
  current_password: '',
  city: '',
  phone: '',
  name: '',
  user_email: '',
  username: '',
  loading: false,
  profile_details: {},
  user_image_url: null,
  user_id: null,
  ura_name: null,
  ura_address_line_1: null,
  ura_address_line_2: null,
  ura_district_id: null,
  ura_district: null,
  ura_state: null,
  ura_state_name: null,
  ura_city: null,
  ura_city_id: null,
  ura_zip: null,
  ura_phone: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_PROFILE_DETAILS:
      return {
        ...state,
        loading: true,
        error: '',
        profile_details: {},
      };
    case GET_PROFILE_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        profile_details: action.payload,
        user_image_url: action.payload.user_image_url,
        name: action.payload.name,
        city: action.payload.city_town,
        phone: action.payload.phone,
        user_email: action.payload.user_email,
        username: action.payload.user_username,
      };
    case GET_PROFILE_DETAILS_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Some error, Please try again',
        profile_details: {},
      };
    case UPLOAD_PROFILE_PICTURE:
      return {...state, loading: true, error: ''};
    case UPLOAD_PROFILE_PICTURE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        user_image_url: action.payload.user_image_url,
      };
    case UPLOAD_PROFILE_PICTURE_FAIL:
      return {...state, loading: false, error: 'Some error, Please try again'};
    case PERSONAL_NAME_CHANGED:
      return {...state, name: action.payload};
    case PERSONAL_PHONE_CHANGED:
      return {...state, phone: action.payload};
    case PERSONAL_CITY_CHANGED:
      return {...state, city: action.payload};
    case UPDATE_PROFILE:
      return {...state, loading: true, error: '', updated: false};
    case UPDATE_PROFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        updated: true,
        error: '',
        status: action.payload,
      };
    case UPDATE_PROFILE_FAIL:
      return {
        ...state,
        loading: false,
        updated: false,
        error: 'Some error, Please try again',
      };
    case CURRENT_PASSWORD:
      return {...state, current_password: action.payload};
    case NEW_PASSWORD:
      return {...state, new_password: action.payload};
    case CONFIRM_NEW_PASSWORD:
      return {...state, confirm_new_password: action.payload};
    case SAVE_CHANGE_PASSWORD:
      return {...state, loading: true};
    case SAVE_CHANGE_PASSWORD_SUCCESS:
      return {...state, loading: false};
    case SAVE_CHANGE_PASSWORD_FAIL:
      return {...state, loading: false};
    case NEW_EMAIL:
      return {...state, new_email: action.payload};
    case CONFIRM_NEW_EMAIL:
      return {...state, confirm_new_email: action.payload};
    case CURRENT_PASSWORD_CHANGE_EMAIL:
      return {...state, current_password_change_email: action.payload};
    case SAVE_CHANGE_EMAIL:
      return {...state, loading: true};
    case SAVE_CHANGE_EMAIL_SUCCESS:
      return {...state, loading: false};
    case SAVE_CHANGE_EMAIL_FAIL:
      return {...state, loading: false};
    case SELLER_FULL_NAME:
      return {...state, seller_full_name: action.payload};
    case SELLER_ADDRESS_LINE_1:
      return {...state, seller_address1: action.payload};
    case SELLER_ADDRESS_LINE_2:
      return {...state, seller_address2: action.payload};
    case SELLER_CITY:
      return {...state, seller_city: action.payload};
    case SELLER_ZIP_CODE:
      return {...state, seller_zip_code: action.payload};
    case SELLER_PHONE_NUMBER:
      return {...state, seller_phone_number: action.payload};
    case GET_ADDRESS_INFO:
      return {
        ...state,
        loading: true,
        sellerInfo: {},
      };
    case GET_ADDRESS_INFO_SUCCESS:
      return {
        ...state,
        loading: false,
        sellerInfo: action.payload,
        user_id: action.payload,
        ura_name: action.payload.ura_name,
        ura_address_line_1: action.payload.ura_address_line_1,
        ura_address_line_2: action.payload.ura_address_line_2,
        ura_district_id: action.payload.ura_district_id,
        ura_district: action.payload.ura_district,
        ura_state: action.payload.ura_state,
        ura_state_name: action.payload.ura_state_name,
        ura_city: action.payload.ura_city,
        ura_city_id: action.payload.ura_city_id,
        ura_zip: action.payload.ura_zip,
        ura_phone: action.payload.ura_phone,
      };
    case GET_ADDRESS_INFO_FAIL:
      return {
        ...state,
        loading: false,
        sellerInfo: {},
      };
    case SAVE_ADDRESS_INFO:
      return {...state, loading: true};
    case SAVE_ADDRESS_INFO_SUCCESS:
      return {
        ...state,
        loading: false,
        sellerInfo: {},
      };
    case SAVE_ADDRESS_INFO_FAIL:
      return {...state, loading: false};
    default:
      return state;
  }
};
