import {
  REGISTER_USER,
  NAME_CHANGED,
  USERNAME_CHANGED,
  REG_PASSWORD_CHANGED,
  REG_EMAIL_CHANGED,
  REGISTRATION_SUCCESS,
  REGISTRATION_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  username: '',
  password: '',
  name: '',
  email: '',
  user: null,
  error: '',
  loading: false,
  registered: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case NAME_CHANGED:
      return {...state, name: action.payload};
    case REG_EMAIL_CHANGED:
      return {...state, email: action.payload};
    case REG_PASSWORD_CHANGED:
      return {...state, password: action.payload};
    case USERNAME_CHANGED:
      return {...state, username: action.payload};
    case REGISTER_USER:
      return {...state, loading: true, error: ''};
    case REGISTRATION_SUCCESS:
      return {
        ...state,
        user: action.payload,
        username: '',
        password: '',
        name: '',
        email: '',
        loading: false,
        error: '',
        registered: true,
      };
    case REGISTRATION_FAIL:
      return {
        ...state,
        error: 'Registration Failed.',
        username: '',
        password: '',
        name: '',
        email: '',
        loading: false,
        registered: false,
      };
    default:
      return state;
  }
};
