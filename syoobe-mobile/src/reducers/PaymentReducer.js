import {
  GET_ADDRESS_PAYMENT,
  GET_ADDRESS_PAYMENT_SUCCESS,
  GET_ADDRESS_PAYMENT_FAIL,
  SELECT_ADDRESS_PAYMENT,
  SELECT_ADDRESS_PAYMENT_SUCCESS,
  SELECT_ADDRESS_PAYMENT_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  product_type: null,
  loading: true,
  changeDisplayBill: null,
  changeDisplayShip: null,
  billing_address: null,
  shipping_address: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_ADDRESS_PAYMENT:
      return {...state, loading: true};
    case GET_ADDRESS_PAYMENT_SUCCESS:
      state.billing_address = action.payload.billing_address_details;
      state.shipping_address = action.payload.shipping_address_details;
      return {
        ...state,
        loading: false,
        product_type: action.payload.product_type,
        changeDisplayBill: action.payload.change_billing_address_btn_display,
        changeDisplayShip: action.payload.change_shipping_address_btn_display,
      };
    case GET_ADDRESS_PAYMENT_FAIL:
      return {...state, loading: false};
    case SELECT_ADDRESS_PAYMENT:
      return {...state, loading: true};
    case SELECT_ADDRESS_PAYMENT_SUCCESS:
      state.billing_address = action.payload.billing_address_details;
      state.shipping_address = action.payload.shipping_address_details;
      return {...state, loading: false};
    case SELECT_ADDRESS_PAYMENT_FAIL:
      return {...state, loading: false};
    default:
      return state;
  }
};
