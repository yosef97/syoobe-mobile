import {
  FETCH_WALLET_DETAILS,
  FETCH_WALLET_DETAILS_SUCCESS,
  FETCH_WALLET_DETAILS_FAIL,
  EMPTY_WALLET_DETAILS,
} from '../actions/types';

const INITIAL_STATE = {
  no_transactions: false,
  endOfRecords: false,
  scrollLoading: false,
  total_pages: null,
  symbol: null,
  balance: null,
  no_of_transactions: null,
  transactions: [],
  loading: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case EMPTY_WALLET_DETAILS:
      return {...state, transactions: [], loading: false, scrollLoading: false};

    case FETCH_WALLET_DETAILS:
      return {
        ...state,
        transactions: [],
        no_transactions: false,
        scrollLoading: false,
        loading: true,
        endOfRecords: false,
      };
    case FETCH_WALLET_DETAILS_SUCCESS:
      return {
        ...state,
        balance: action.payload.total_balance,
        no_of_transactions: action.payload.total_record,
        symbol: action.payload.currency_symbol,
        transactions: action.payload.transaction_details,
        total_pages: action.payload.total_page,
        loading: false,
        scrollLoading: false,
      };
    case FETCH_WALLET_DETAILS_FAIL:
      return {...state, transactions: [], loading: false, scrollLoading: false};
    default:
      return state;
  }
};
