import * as actionTypes from '../actions/types';

const INITIAL_STATE = {notifications: [], unreadNotificationCount: 0};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionTypes.NOTIFICATIONS:
      return {
        ...state,
        notifications: action.payload.filter((notEl) => true).reverse(),
      };
    case actionTypes.UNREAD_NOTIFICATIONS:
      return {
        ...state,
        unreadNotificationCount: action.payload,
      };
    case actionTypes.EMPTY_NOTIFICATIONS:
      return {
        ...state,
        notifications: [],
        unreadNotificationCount: 0,
      };
    default:
      return state;
  }
};
