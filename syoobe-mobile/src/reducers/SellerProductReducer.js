import {
  GET_SELLER_PAUSED_ITEMS,
  GET_SELLER_PAUSED_ITEMS_SUCCESS,
  GET_SELLER_PAUSED_ITEMS_FAIL,
  GET_SELLER_ACTIVE_ITEMS,
  GET_SELLER_ACTIVE_ITEMS_SUCCESS,
  GET_SELLER_ACTIVE_ITEMS_FAIL,
  CHANGE_SELLER_PRODUCT_STATUS,
  CHANGE_SELLER_PRODUCT_STATUS_SUCCESS,
  CHANGE_SELLER_PRODUCT_STATUS_FAIL,
  DELETE_SELLER_PRODUCT,
  DELETE_SELLER_PRODUCT_SUCCESS,
  DELETE_SELLER_PRODUCT_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
  loading: false,
  no_active: false,
  no_paused: false,
  active_products: [],
  paused_products: [],
  upgrade_limit_info: null,
  can_add_product: 0,
  can_upgrade_limit: 0,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_SELLER_PAUSED_ITEMS:
      return {...state, loading: true, no_paused: false};
    case GET_SELLER_PAUSED_ITEMS_SUCCESS:
      return {
        ...state,
        loading: false,
        no_paused: false,
        paused_products: action.payload.paused_product,
      };
    case GET_SELLER_PAUSED_ITEMS_FAIL:
      return {...state, loading: false, no_paused: true};
    case GET_SELLER_ACTIVE_ITEMS:
      return {...state, loading: true, no_active: false};
    case GET_SELLER_ACTIVE_ITEMS_SUCCESS:
      return {
        ...state,
        loading: false,
        no_active: false,
        active_products: action.payload.active_product,
        upgrade_limit_info: action.payload.upgrade_limit_info,
        can_add_product: action.payload.can_add_product,
        can_upgrade_limit: action.payload.can_upgrade_limit,
      };
    case GET_SELLER_ACTIVE_ITEMS_FAIL:
      return {...state, loading: false, no_active: true};
    case CHANGE_SELLER_PRODUCT_STATUS:
      return {...state, loading: true};
    case CHANGE_SELLER_PRODUCT_STATUS_SUCCESS:
      if (action.status == 'active') {
        state.active_products.forEach((item, index) => {
          if (item.product_id == action.payload.product_id) {
            state.active_products.splice(index, 1);
          }
        });
        if (state.active_products.length < 1) {
          return {
            ...state,
            no_active: true,
            loading: false,
            ...state.active_products,
            ...state.paused_products,
          };
        } else {
          return {
            ...state,
            loading: false,
            ...state.active_products,
            ...state.paused_products,
          };
        }
      } else if (action.status == 'pause') {
        state.paused_products.forEach((item, index) => {
          if (item.product_id == action.payload.product_id) {
            state.paused_products.splice(index, 1);
          }
        });
        if (state.paused_products.length < 1) {
          return {
            ...state,
            no_paused: true,
            loading: false,
            ...state.active_products,
            ...state.paused_products,
          };
        } else {
          return {
            ...state,
            loading: false,
            ...state.active_products,
            ...state.paused_products,
          };
        }
      }
    case CHANGE_SELLER_PRODUCT_STATUS_FAIL:
      return {...state, loading: false};

    case DELETE_SELLER_PRODUCT:
      return {...state, loading: true};
    case DELETE_SELLER_PRODUCT_SUCCESS:
      if (action.status == 'active') {
        state.active_products.forEach((item, index) => {
          if (item.product_id == action.payload.product_id) {
            state.active_products.splice(index, 1);
          }
        });
        if (state.active_products.length < 1) {
          return {
            ...state,
            no_active: true,
            loading: false,
            ...state.active_products,
            ...state.paused_products,
          };
        } else {
          return {
            ...state,
            loading: false,
            ...state.active_products,
            ...state.paused_products,
          };
        }
      } else if (action.status == 'pause') {
        state.paused_products.forEach((item, index) => {
          if (item.product_id == action.payload.product_id) {
            state.paused_products.splice(index, 1);
          }
        });
        if (state.paused_products.length < 1) {
          return {
            ...state,
            no_paused: true,
            loading: false,
            ...state.active_products,
            ...state.paused_products,
          };
        } else {
          return {
            ...state,
            loading: false,
            ...state.active_products,
            ...state.paused_products,
          };
        }
      }
    case DELETE_SELLER_PRODUCT_FAIL:
      return {...state, loading: false};
    default:
      return state;
  }
};
