import {
  SET_COST,
  SALES_TAX,
  SET_SELECTED_SHIPPING_OPTION_SERVICE,
  SET_SHIPPING_COMPANY,
  SET_SHIPPING_OPTIONS,
  SHIPPING_COMPANY_SELECTED,
  TOTAL_TAX,
  TOTAL_PRICE,
  DELIVERY_CHARGE,
  SHIPPING_SUMMARY,
  DATA_TO_SEND,
} from '../actions/types';

const INITIAL_STATE = {
  shippingSummary: [],
  dataToSend: [],
  shippingCompanies: [],
  shippingOptions: [],
  selectedShippingOptionService: [],
  total_price: 0,
  shippingCompanySelected: [],
  sales_tax: 0,
  total_tax: 0,
  total_cost: 0,
};

export default (state = INITIAL_STATE, {type, payload}) => {
  switch (type) {
    case SET_SHIPPING_OPTIONS:
      return {...state, shippingOptions: payload};
    case SHIPPING_COMPANY_SELECTED:
      return {...state, shippingCompanySelected: payload};
    case SET_SHIPPING_COMPANY:
      return {...state, shippingCompanies: payload};
    case SET_SELECTED_SHIPPING_OPTION_SERVICE:
      return {...state, selectedShippingOptionService: payload};

    case SET_COST:
      return {...state, total_cost: payload};
    case DATA_TO_SEND:
      return {...state, dataToSend: payload};
    case DELIVERY_CHARGE:
      return {...state, deliveryCharge: payload};
    case SHIPPING_SUMMARY:
      return {...state, shippingSummary: payload};
    case SALES_TAX:
      return {...state, sales_tax: payload};
    case TOTAL_TAX:
      return {...state, total_tax: payload};
    case TOTAL_PRICE:
      return {...state, total_price: payload};
    default:
      return state;
  }
};
