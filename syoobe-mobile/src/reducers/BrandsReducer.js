import {
  GET_BRANDS,
  GET_BRANDS_SUCCESS,
  GET_BRANDS_FAIL,
} from '../actions/types';

const INITIAL_STATE = {brands: [], loading: true};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_BRANDS:
      return {...state, brands: [], loading: true};
    case GET_BRANDS_SUCCESS:
      var data = {id: 0, name: 'Select Brand'};
      action.payload.brand.unshift(data);
      return {...state, brands: action.payload.brand, loading: false};
    case GET_BRANDS_FAIL:
      return {...state, brands: [], loading: false};
    default:
      return state;
  }
};
