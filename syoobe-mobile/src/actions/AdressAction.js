import Axios from 'axios';
import {
  GET_CITY,
  GET_CITY_FAIL,
  GET_CITY_SELECTED,
  GET_CITY_SUCCESS,
  GET_COUNTRIES,
  GET_COUNTRIES_FAIL,
  GET_COUNTRIES_SUCCESS,
  GET_COUNTRY_SELECTED,
  GET_DISTRICT,
  GET_DISTRICT_FAIL,
  GET_DISTRICT_SELECTED,
  GET_DISTRICT_SUCCESS,
  GET_PROVINCE,
  GET_PROVINCE_FAIL,
  GET_PROVINCE_SELECTED,
  GET_PROVINCE_SUCCESS,
  SET_COMPONENT_REDIRECT,
} from './types';
import {env} from '../../appconfig';

const rajaOngkirHeader = {headers: {key: env.SHIPPING.api_key}};

// Start Province
export const getProvince = () => (dispatch) => {
  dispatch({type: GET_PROVINCE});
  Axios.get(`${env.SHIPPING.url}/province`, rajaOngkirHeader)
    .then((res) => getProvince_Success(res.data, dispatch))
    .catch((err) => getProvince_Fail(err, dispatch));
};

export const setSelectedProvince = (
  response,
  navigation,
  reset = false,
  component = 'City',
) => (dispatch) => {
  dispatch({type: GET_PROVINCE_SELECTED, payload: response});
  if (!reset) {
    dispatch({type: GET_CITY_SELECTED, payload: {}});
    dispatch({type: GET_CITY_SUCCESS, payload: []});
    dispatch({type: GET_DISTRICT_SELECTED, payload: {}});
    dispatch({type: GET_DISTRICT_SUCCESS, payload: []});
    navigation.navigate(component, {province_id: response.province_id});
  }
};

const getProvince_Success = (response, dispatch) => {
  // console.log(response)
  dispatch({
    type: GET_PROVINCE_SUCCESS,
    payload: response.rajaongkir.results,
  });
};

const getProvince_Fail = (error, dispatch) => {
  dispatch({
    type: GET_PROVINCE_FAIL,
    payload: error,
  });
};

// Start City

export const getCity = (province_id) => (dispatch) => {
  dispatch({type: GET_CITY});
  Axios.get(
    `${env.SHIPPING.url}/city?province=${province_id}`,
    rajaOngkirHeader,
  )
    .then((res) => getCity_Success(res.data, dispatch))
    .catch((err) => getCity_Fail(err, dispatch));
};

export const setSelectedCity = (
  response,
  navigation,
  reset = false,
  component = 'District',
) => (dispatch) => {
  // console.log(component);
  dispatch({type: GET_CITY_SELECTED, payload: response});
  if (!reset) {
    dispatch({type: GET_DISTRICT_SELECTED, payload: {}});
    dispatch({type: GET_DISTRICT_SUCCESS, payload: []});
    navigation.navigate(component === 'PersonalInfo' ? component : 'District', {
      city_id: response.city_id,
    });
  }
};

const getCity_Success = (response, dispatch) => {
  dispatch({
    type: GET_CITY_SUCCESS,
    payload: response.rajaongkir.results,
  });
};

const getCity_Fail = (error, dispatch) => {
  dispatch({
    type: GET_CITY_FAIL,
    payload: error,
  });
};

// District
export const getDistrict = (city_id) => (dispatch) => {
  dispatch({type: GET_DISTRICT});
  Axios.get(`${env.SHIPPING.url}/subdistrict?city=${city_id}`, rajaOngkirHeader)
    .then((res) => getDistrict_Success(res.data, dispatch))
    .catch((err) => getDistrict_Fail(err, dispatch));
};

export const setSelectedDistrict = (
  response,
  navigation,
  reset = false,
  component,
) => (dispatch) => {
  dispatch({type: GET_DISTRICT_SELECTED, payload: response});
  if (!reset) {
    navigation.navigate(component ? component : 'AddOrEditAddress', {
      typeHeader: 'Alamat Pengembalian',
    });
  }
};

const getDistrict_Success = (response, dispatch) => {
  dispatch({
    type: GET_DISTRICT_SUCCESS,
    payload: response.rajaongkir.results,
  });
};

const getDistrict_Fail = (error, dispatch) => {
  dispatch({
    type: GET_DISTRICT_FAIL,
    payload: error,
  });
};

// Get country
export const setSelectedCountry = (response, navigation, redirect = true) => (
  dispatch,
) => {
  dispatch({type: GET_COUNTRY_SELECTED, payload: response});
  dispatch({type: GET_PROVINCE_SELECTED, payload: {}});
  if (redirect) {
    navigation.navigate(response.id == 106 ? 'Province' : 'State', {
      country_id: response.id,
    });
  }
};

export const setSelectedStates = (response, navigation, redirect = true) => (
  dispatch,
) => {
  dispatch({type: GET_PROVINCE_SELECTED, payload: response});
  dispatch({type: GET_CITY_SELECTED, payload: {}});
  dispatch({type: GET_CITY_SUCCESS, payload: []});
  if (redirect) {
    navigation.navigate('PersonalInfo');
  }
};

// Reset Adress
export const resetAdress = () => (dispatch) => {
  dispatch({type: GET_PROVINCE_SELECTED, payload: {}});
  dispatch({type: GET_PROVINCE_SUCCESS, payload: []});
  dispatch({type: GET_CITY_SELECTED, payload: {}});
  dispatch({type: GET_CITY_SUCCESS, payload: []});
  dispatch({type: GET_DISTRICT_SELECTED, payload: {}});
  dispatch({type: GET_DISTRICT_SUCCESS, payload: []});
};

export const setComponentRedirect = (component) => (dispatch) => {
  dispatch({type: SET_COMPONENT_REDIRECT, payload: component});
};
