import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGOUT_USER,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAIL,
  REGISTER_USER,
  NAME_CHANGED,
  USERNAME_CHANGED,
  REG_PASSWORD_CHANGED,
  REG_EMAIL_CHANGED,
  REGISTRATION_SUCCESS,
  REGISTRATION_FAIL,
  GET_HOME_PRODUCTS,
  GET_HOME_PRODUCTS_SUCCESS,
  GET_HOME_PRODUCTS_FAIL,
  GET_HOME_TOKEN_PRODUCTS,
  GET_HOME_PRODUCTS_TOKEN_SUCCESS,
  GET_HOME_PRODUCTS_TOKEN_FAIL,
  ADD_TO_CART,
  ADD_TO_CART_SUCCESS,
  ADD_TO_CART_FAIL,
  VIEW_CART,
  VIEW_CART_SUCCESS,
  VIEW_CART_FAIL,
  DELETE_CART_ITEM,
  DELETE_CART_ITEM_SUCCESS,
  DELETE_CART_ITEM_FAIL,
  EDIT_CART_ITEM,
  EDIT_CART_ITEM_SUCCESS,
  EDIT_CART_ITEM_FAIL,
  GET_CATEGORY_LIST,
  GET_CATEGORY_LIST_SUCCESS,
  GET_CATEGORY_LIST_FAIL,
  EXPAND_CATEGORY,
  GET_PRODUCT_LIST_BY_CATEGORY,
  GET_PRODUCT_LIST_BY_CATEGORY_SUCCESS,
  GET_PRODUCT_LIST_BY_CATEGORY_FAIL,
  GET_DASHBOARD,
  GET_DASHBOARD_SUCCESS,
  GET_DASHBOARD_FAIL,
  SET_FAVOURITE,
  SET_FAVOURITE_SUCCESS,
  SET_FAVOURITE_FAIL,
  SET_UNFAVOURITE,
  SET_UNFAVOURITE_SUCCESS,
  SET_UNFAVOURITE_FAIL,
  // RECENTLY_VIEWED,
  GET_PROFILE_DETAILS,
  GET_PROFILE_DETAILS_SUCCESS,
  GET_PROFILE_DETAILS_FAIL,
  UPLOAD_PROFILE_PICTURE,
  UPLOAD_PROFILE_PICTURE_SUCCESS,
  UPLOAD_PROFILE_PICTURE_FAIL,
  PERSONAL_NAME_CHANGED,
  PERSONAL_PHONE_CHANGED,
  PERSONAL_CITY_CHANGED,
  GET_STATES,
  GET_STATES_SUCCESS,
  GET_STATES_FAIL,
  GET_COUNTRIES,
  GET_COUNTRIES_SUCCESS,
  GET_COUNTRIES_FAIL,
  UPDATE_PROFILE,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_FAIL,
  CURRENT_PASSWORD,
  NEW_PASSWORD,
  CONFIRM_NEW_PASSWORD,
  SAVE_CHANGE_PASSWORD,
  SAVE_CHANGE_PASSWORD_SUCCESS,
  SAVE_CHANGE_PASSWORD_FAIL,
  SAVE_CHANGE_EMAIL,
  SAVE_CHANGE_EMAIL_SUCCESS,
  SAVE_CHANGE_EMAIL_FAIL,
  NEW_EMAIL,
  CONFIRM_NEW_EMAIL,
  CURRENT_PASSWORD_CHANGE_EMAIL,
  GET_BANK_DETAILS,
  GET_BANK_DETAILS_FAIL,
  GET_BANK_DETAILS_SUCCESS,
  CHANGE_BANK_NAME,
  CHANGE_ACCOUNT_HOLDER_NAME,
  CHANGE_ACCOUNT_NUMBER,
  CHANGE_IFSC_CODE,
  CHANGE_PAYPAL_ID,
  CHANGE_BANK_ADDRESS,
  SAVE_BANK_DETAILS,
  SAVE_BANK_DETAILS_SUCCESS,
  SAVE_BANK_DETAILS_FAIL,
  FORGOT_PASSWORD_CHANGED,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAIL,
  CHANGE_SHOP_NAME,
  CHANGE_URL_KEYWORDS,
  SHOP_DESCRIPTION,
  SHOP_CITY,
  SHOP_ANNOUNCEMENT,
  MESSAGE_TO_BUYER,
  UPDATE_LOGO,
  UPDATE_BANNER,
  CLOSE_LOGO,
  CLOSE_BANNER,
  DISPLAY_STATUS,
  SHOP_IS_PROD_TAX,
  WELCOME_MESSAGE,
  PAYMENT_POLICY,
  DELIVERY_POLICY,
  REFUND_POLICY,
  ADDITIONAL_INFORMATION,
  SELLER_INFORMATION,
  SHOPING_NPWP,
  META_TAG_TITLE,
  META_TAG_KEYWORDS,
  META_TAG_DESCRIPTION,
  SAVE_SHOP,
  SAVE_SHOP_SUCCESS,
  SAVE_SHOP_FAIL,
  SET_COUNTRY,
  SET_STATE,
  SKIP_STEP_2,
  SKIP_STEP_3,
  GET_SHOP_DETAILS,
  GET_SHOP_DETAILS_SUCCESS,
  GET_SHOP_DETAILS_FAIL,
  GET_FAVOURITES,
  GET_FAVOURITES_SUCCESS,
  GET_FAVOURITES_FAIL,
  GET_ADDRESS_INFO,
  GET_ADDRESS_INFO_SUCCESS,
  GET_ADDRESS_INFO_FAIL,
  SELLER_FULL_NAME,
  SELLER_ADDRESS_LINE_1,
  SELLER_ADDRESS_LINE_2,
  SELLER_CITY,
  SELLER_ZIP_CODE,
  SELLER_PHONE_NUMBER,
  SAVE_ADDRESS_INFO,
  SAVE_ADDRESS_INFO_SUCCESS,
  SAVE_ADDRESS_INFO_FAIL,
  GET_SELLER_ACTIVE_ITEMS,
  GET_SELLER_ACTIVE_ITEMS_SUCCESS,
  GET_SELLER_ACTIVE_ITEMS_FAIL,
  GET_SELLER_PAUSED_ITEMS,
  GET_SELLER_PAUSED_ITEMS_SUCCESS,
  GET_SELLER_PAUSED_ITEMS_FAIL,
  CHANGE_SELLER_PRODUCT_STATUS,
  CHANGE_SELLER_PRODUCT_STATUS_SUCCESS,
  CHANGE_SELLER_PRODUCT_STATUS_FAIL,
  DELETE_SELLER_PRODUCT,
  DELETE_SELLER_PRODUCT_SUCCESS,
  DELETE_SELLER_PRODUCT_FAIL,
  GET_BUYER_ORDER_HISTORY,
  GET_BUYER_ORDER_HISTORY_SUCCESS,
  GET_BUYER_ORDER_HISTORY_FAIL,
  GET_ORDER_STATUS_LIST,
  GET_ORDER_STATUS_LIST_SUCCESS,
  GET_ORDER_STATUS_LIST_FAIL,
  EMPTY_ORDER_HISTORY,
  GET_BUYER_DOWNLOAD_HISTORY,
  GET_BUYER_DOWNLOAD_HISTORY_SUCCESS,
  GET_BUYER_DOWNLOAD_HISTORY_FAIL,
  EMPTY_DOWNLOAD_HISTORY,
  GET_ORDER_DETAILS,
  GET_ORDER_DETAILS_SUCCESS,
  GET_ORDER_DETAILS_FAIL,
  GET_BRANDS,
  GET_BRANDS_SUCCESS,
  GET_BRANDS_FAIL,
  GET_PROCESSING_TIME,
  GET_PROCESSING_TIME_SUCCESS,
  GET_PROCESSING_TIME_FAIL,
  GET_SHIPS_TO_COUNTRIES,
  GET_SHIPS_TO_COUNTRIES_SUCCESS,
  GET_SHIPS_TO_COUNTRIES_FAIL,
  GET_SHIPPING_COMPANY,
  GET_SHIPPING_COMPANY_SUCCESS,
  GET_SHIPPING_COMPANY_FAIL,
  GET_CARRIER_SERVICE_SUCCESS,
  GET_CARRIER_SERVICE_FAIL,
  GET_CARRIER_SERVICE,
  SAVE_PRODUCT,
  SAVE_PRODUCT_SUCCESS,
  SAVE_PRODUCT_FAIL,
  UPLOAD_IMAGES_GENERAL_SUCCESS,
  UPLOAD_IMAGES_GENERAL_FAIL,
  UPLOAD_IMAGES_GENERAL,
  DELETE_IMAGE_GENERAL,
  DELETE_IMAGE_GENERAL_SUCCESS,
  DELETE_IMAGE_GENERAL_FAIL,
  REMOVE_GENERAL_IMAGES,
  GET_SPECIFICATIONS_FAIL,
  GET_SPECIFICATIONS_SUCCESS,
  GET_SPECIFICATIONS,
  GET_PRODUCT_FILTERS,
  GET_PRODUCT_FILTERS_SUCCESS,
  GET_PRODUCT_FILTERS_FAIL,
  GET_RELATED_PRODUCTS_SUCCESS,
  GET_RELATED_PRODUCTS_FAIL,
  GET_RELATED_PRODUCTS,
  CREATE_OPTION,
  CREATE_OPTION_SUCCESS,
  CREATE_OPTION_FAIL,
  LOAD_OPTIONS,
  LOAD_OPTIONS_SUCCESS,
  LOAD_OPTIONS_FAIL,
  LOAD_COMPATIBILITY,
  LOAD_COMPATIBILITY_SUCCESS,
  LOAD_COMPATIBILITY_FAIL,
  LOAD_COMPATIBILITY_CONTENT,
  LOAD_COMPATIBILITY_CONTENT_SUCCESS,
  LOAD_COMPATIBILITY_CONTENT_FAIL,
  LOAD_COMPATIBILITY_CONTENT_DROPDOWN,
  LOAD_COMPATIBILITY_CONTENT_DROPDOWN_SUCCESS,
  LOAD_COMPATIBILITY_CONTENT_DROPDOWN_FAIL,
  GET_RECENTLY_VIEWED,
  GET_RECENTLY_VIEWED_SUCCESS,
  GET_RECENTLY_VIEWED_FAIL,
  SHOW_SEARCH_FIELD,
  CLEAR_HOME_PRODUCTS,
  SOCIAL_LOGIN,
  SOCIAL_LOGIN_SUCCESS,
  SOCIAL_LOGIN_FAIL,
  FETCH_WALLET_DETAILS,
  FETCH_WALLET_DETAILS_SUCCESS,
  FETCH_WALLET_DETAILS_FAIL,
  FETCH_MESSAGES,
  FETCH_MESSAGES_SUCCESS,
  FETCH_MESSAGES_FAIL,
  CLEAR_MESSAGES,
  APPLY_COUPON_CART,
  APPLY_COUPON_CART_SUCCESS,
  APPLY_COUPON_CART_FAIL,
  REMOVE_COUPON_CART,
  REMOVE_COUPON_CART_SUCCESS,
  REMOVE_COUPON_CART_FAIL,
  APPLY_REWARD_POINT,
  APPLY_REWARD_POINT_SUCCESS,
  APPLY_REWARD_POINT_FAIL,
  GET_ADDRESS_PAYMENT,
  GET_ADDRESS_PAYMENT_SUCCESS,
  GET_ADDRESS_PAYMENT_FAIL,
  SELECT_ADDRESS_PAYMENT,
  SELECT_ADDRESS_PAYMENT_SUCCESS,
  SELECT_ADDRESS_PAYMENT_FAIL,
  EMPTY_WALLET_DETAILS,
  EDIT_PRODUCT_VIEW,
  EDIT_PRODUCT_VIEW_SUCCESS,
  EDIT_PRODUCT_VIEW_FAIL,
  EMPTY_PRODUCT_DATA,
  GET_HOME_PRODUCTS_FAIL_NO_INTERNET,
  GOOGLE_LOGIN,
  GOOGLE_LOGIN_FAIL,
  GOOGLE_LOGIN_SUCCESS,
  LOGIN_USER_FAIL_VERIFY,
  SET_VERIFY_FALSE,
  SET_MAIN_IMAGE,
  EMPTY_NOTIFICATIONS,
  NOTIFICATIONS,
  UNREAD_NOTIFICATIONS,
  SEND_VERIFY_OTP,
  SEND_VERIFY_OTP_SUCCESS,
  SEND_VERIFY_OTP_FAIL,
  GET_PRODUCT_FEE,
  GET_PRODUCT_FEE_SUCCESS,
  GET_PRODUCT_FEE_FAIL,
  GET_SALES_LIST,
  SHOP_DISTRICT_ID,
  SHOP_DISTRICT_NAME,
  SHOP_STATE_NAME,
  SHOP_CITY_ID,
} from './types';
import {Alert, AsyncStorage} from 'react-native';
import {showToast} from '../helpers/toastMessage';
import Axios from 'axios';
import Bugsnag from '@bugsnag/react-native';
const queryString = require('query-string');

export const emptyNotifications = () => {
  return (dispatch) => {
    dispatch({
      type: EMPTY_NOTIFICATIONS,
    });
  };
};

export const storeNotifications = (data) => {
  return (dispatch) => {
    dispatch({
      type: NOTIFICATIONS,
      payload: data,
    });
  };
};

export const storeUnreadNotifications = (count) => {
  return (dispatch) => {
    dispatch({
      type: UNREAD_NOTIFICATIONS,
      payload: count,
    });
  };
};

export const setMainImageDone = (response) => {
  return (dispatch) => {
    dispatch({
      type: SET_MAIN_IMAGE,
      payload: response,
    });
  };
};

export const editProductView = (details, navigation) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: EDIT_PRODUCT_VIEW});
    fetch('https://syoobe.co.id/api/viewEditProduct', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => editProductViewSuccess(response, dispatch))
      .catch(() =>
        editProductViewFail('Terjadi kesalahan', dispatch, navigation),
      );
  };
};

const editProductViewSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: EDIT_PRODUCT_VIEW_SUCCESS,
      payload: response,
    });
  } else {
    editProductViewFail(response.msg, dispatch);
  }
};

export const editProductViewFail = (error, dispatch, navigation) => {
  console.log(error);
  dispatch({
    type: EDIT_PRODUCT_VIEW_FAIL,
    payload: error,
  });
  Alert.alert('Gagal', error, [{text: 'OK', onPress: () => navigation.pop()}]);
};

export const selectAddressPayment = (type, details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  let url = '';
  if (type == 'shipping') {
    url = 'selectshippingAddress';
  } else if (type == 'billing') {
    url = 'selectBillingAddress';
  }
  return (dispatch) => {
    dispatch({type: SELECT_ADDRESS_PAYMENT});
    fetch('https://syoobe.co.id/api/' + url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => selectAddressPaymentSuccess(response, dispatch))
      .catch(() => selectAddressPaymentFail('Terjadi kesalahan', dispatch));
  };
};

const selectAddressPaymentSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: SELECT_ADDRESS_PAYMENT_SUCCESS,
      payload: response,
    });
  } else {
    selectAddressPaymentFail(response.msg, dispatch);
  }
};

export const selectAddressPaymentFail = (error, dispatch) => {
  dispatch({
    type: SELECT_ADDRESS_PAYMENT_FAIL,
    payload: error,
  });
};

export const getAddressPayment = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_ADDRESS_PAYMENT});
    fetch('https://syoobe.co.id/api/cartExistingAddress', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getAddressPaymentSuccess(response, dispatch))
      .catch(() => getAddressPaymentFail('Terjadi kesalahan', dispatch));
  };
};

const getAddressPaymentSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: GET_ADDRESS_PAYMENT_SUCCESS,
      payload: response,
    });
  } else {
    getAddressPaymentFail(response.msg, dispatch);
  }
};

export const getAddressPaymentFail = (error, dispatch) => {
  dispatch({
    type: GET_ADDRESS_PAYMENT_FAIL,
    payload: error,
  });
};

export const applyReward = (details) => {
  console.log('details', details);
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: APPLY_REWARD_POINT});
    fetch('https://syoobe.co.id/api/applyRewardPoint', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => applyRewardSuccess(response, dispatch))
      .catch((error) =>
        // console.log('err',error)
        applyRewardFail('Terjadi kesalahan', dispatch),
      );
  };
};

const applyRewardSuccess = (response, dispatch) => {
  // console.log('reward',JSON.parse(response._bodyText));
  if (response.status === 1) {
    dispatch({
      type: APPLY_REWARD_POINT_SUCCESS,
      payload: response,
    });
  } else {
    applyRewardFail(response.msg, dispatch);
  }
};

export const applyRewardFail = (error, dispatch) => {
  dispatch({
    type: APPLY_REWARD_POINT_FAIL,
    payload: error,
  });
};

export const removeCoupon = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: REMOVE_COUPON_CART});
    fetch('https://syoobe.co.id/api/removeCouponAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => removeCouponSuccess(response, dispatch))
      .catch(() => removeCouponFail('Terjadi kesalahan', dispatch));
  };
};

const removeCouponSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: REMOVE_COUPON_CART_SUCCESS,
      payload: response,
    });
  } else {
    removeCouponFail(response.msg, dispatch);
  }
};

export const removeCouponFail = (error, dispatch) => {
  dispatch({
    type: REMOVE_COUPON_CART_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const applyCoupon = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: APPLY_COUPON_CART});
    fetch('https://syoobe.co.id/api/cart_apply_coupon', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => applyCouponSuccess(response, dispatch))
      .catch(() => applyCouponFail('Terjadi kesalahan', dispatch));
  };
};

const applyCouponSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: APPLY_COUPON_CART_SUCCESS,
      payload: response,
    });
  } else {
    applyCouponFail(response.msg, dispatch);
  }
};

export const applyCouponFail = (error, dispatch) => {
  dispatch({
    type: APPLY_COUPON_CART_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const clearMessages = () => {
  return {
    type: CLEAR_MESSAGES,
  };
};

export const getMessages = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: FETCH_MESSAGES});
    fetch('https://syoobe.co.id/api/sellerBuyerMessageListing', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getMessagesSuccess(response, dispatch))
      .catch(() => getMessagesFail('Terjadi kesalahan', dispatch));
  };
};

const getMessagesSuccess = (response, dispatch) => {
  console.log(response);
  if (response.status === 1) {
    dispatch({
      type: FETCH_MESSAGES_SUCCESS,
      payload: response,
    });
  } else {
    getMessagesFail(response.msg, dispatch);
  }
};

export const getMessagesFail = (error, dispatch) => {
  console.log(error);
  dispatch({
    type: FETCH_MESSAGES_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const getWalletDetails = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: FETCH_WALLET_DETAILS});
    fetch('https://syoobe.co.id/api/walletDetailsListing', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getWalletDetailsSuccess(response, dispatch))
      .catch(() => getWalletDetailsFail('Terjadi kesalahan', dispatch));
  };
};

export const getRewardPoint = (details) => () => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return new Promise((resolve, reject) => {
    return fetch('https://syoobe.co.id/api/rewardPointListingAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => resolve(response))
      .catch((err) => reject(err));
  });
};

const getWalletDetailsSuccess = (response, dispatch) => {
  // console.log('getWalletDetailsSuccess', JSON.stringify(response));
  if (response.status === 1) {
    dispatch({
      type: FETCH_WALLET_DETAILS_SUCCESS,
      payload: response,
    });
  } else if (response.status === 0) {
    dispatch({
      type: FETCH_WALLET_DETAILS_SUCCESS,
      payload: response,
    });
  } else {
    getWalletDetailsFail(response.msg, dispatch);
  }
};

export const getWalletDetailsFail = (error, dispatch) => {
  dispatch({
    type: FETCH_WALLET_DETAILS_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const emptyWalletDetails = () => {
  return {
    type: EMPTY_WALLET_DETAILS,
  };
};

export const googleLogin = (navigation, details) => {
  console.log('googleLogin', details);
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GOOGLE_LOGIN});
    fetch('https://syoobe.co.id/api/googleLogin', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => googleLoginSuccess(navigation, response, dispatch))
      .catch((error) => googleLoginFail(error, dispatch));
  };
};

const googleLoginSuccess = (navigation, response, dispatch) => {
  console.log('google login', response);
  if (response.status === 1) {
    AsyncStorage.setItem('emailStore', response.user_email);
    AsyncStorage.setItem('passwordStore', response.user_password);
    AsyncStorage.setItem('needToDecode', 'true');
    _storeData(response.token);
    _storeUserId(response.user_id);
    if (response.shop_id != 0) {
      _storeShopId(response.shop_id);
    }
    dispatch({
      type: GOOGLE_LOGIN_SUCCESS,
      payload: response,
    });
    dispatch({
      type: CLEAR_HOME_PRODUCTS,
    });
    // const resetAction = StackActions.reset({
    //   index: 0,
    //   actions: [CommonActions.],
    // });
    navigation.navigate('HomeDrawer', {screen: 'Home'});
    // navigation.navigate('homeDrawer');
    showToast({
      message: 'Logged in',
    });
  } else {
    googleLoginFail('Invalid username or password', dispatch);
  }
};

export const googleLoginFail = (error, dispatch) => {
  dispatch({
    type: GOOGLE_LOGIN_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const fbsocialLogin = (navigation, details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: SOCIAL_LOGIN});
    fetch('https://syoobe.co.id/api/facebookLogin', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => fbsocialLoginSuccess(navigation, response, dispatch))
      .catch((error) => fbsocialLoginFail(error, dispatch));
  };
};

const fbsocialLoginSuccess = (navigation, response, dispatch) => {
  if (response.status === 1) {
    _storeData(response.token);
    _storeUserId(response.user_id);
    if (response.shop_id != 0) {
      _storeShopId(response.shop_id);
    }
    dispatch({
      type: SOCIAL_LOGIN_SUCCESS,
      payload: response,
    });
    dispatch({
      type: CLEAR_HOME_PRODUCTS,
    });
    navigation.navigate('HomeDrawer', {screen: 'Home'});
    // navigation.navigate('homeDrawer');
    showToast({
      message: 'Logged in',
    });
  } else {
    fbsocialLoginFail('Invalid username or password', dispatch);
  }
};

export const fbsocialLoginFail = (error, dispatch) => {
  dispatch({
    type: SOCIAL_LOGIN_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

// ==========================================Get Search========================================

export const getSearchResults = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_PRODUCT_LIST_BY_CATEGORY});
    fetch('https://syoobe.co.id/api/dashboardSearch', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getSearchResultsSuccess(response, dispatch))
      .catch(() => getSearchResultsFail('Terjadi kesalahan', dispatch));
  };
};

const getSearchResultsSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: GET_PRODUCT_LIST_BY_CATEGORY_SUCCESS,
      payload: response,
    });
    showToast({
      message: response.total_records + ' records found',
    });
  } else {
    getSearchResultsFail('No Records found', dispatch);
  }
};

export const getSearchResultsFail = (error, dispatch) => {
  dispatch({
    type: GET_PRODUCT_LIST_BY_CATEGORY_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const showSearchField = () => {
  return {
    type: SHOW_SEARCH_FIELD,
  };
};

export const fetchRecentlyViewed = (details) => {
  console.log(details);
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_RECENTLY_VIEWED});
    Axios.post('https://syoobe.co.id/api/recentlyViewedProductList', details)
      // .then((res) => res.json())
      .then((response) => fetchRecentlyViewedSuccess(response.data, dispatch))
      .catch(() =>
        fetchRecentlyViewedFail(
          'Oops some error occured.. Please try agian!',
          dispatch,
        ),
      );
  };
};

const fetchRecentlyViewedSuccess = (response, dispatch) => {
  console.log(response);
  if (response.status == 1) {
    dispatch({
      type: GET_RECENTLY_VIEWED_SUCCESS,
      payload: response.data,
    });
  } else {
    fetchRecentlyViewedFail(response.msg, dispatch);
  }
};

const fetchRecentlyViewedFail = (error, dispatch) => {
  dispatch({
    type: GET_RECENTLY_VIEWED_FAIL,
    payload: error,
  });
};

export const loadCompatibilityContentDropdown = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: LOAD_COMPATIBILITY_CONTENT_DROPDOWN});
    fetch('https://syoobe.co.id/api/attributeDetails', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        loadCompatibilityContentDropdownSuccess(response, dispatch),
      )
      .catch(() =>
        loadCompatibilityContentDropdownFail(
          'Oops some error occured.. Please try agian!',
          dispatch,
        ),
      );
  };
};

const loadCompatibilityContentDropdownSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: LOAD_COMPATIBILITY_CONTENT_DROPDOWN_SUCCESS,
      payload: response,
    });
  } else {
    loadCompatibilityContentDropdownFail(response.msg, dispatch);
  }
};

const loadCompatibilityContentDropdownFail = (error, dispatch) => {
  dispatch({
    type: LOAD_COMPATIBILITY_CONTENT_DROPDOWN_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const loadCompatibilityContent = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: LOAD_COMPATIBILITY_CONTENT});
    fetch('https://syoobe.co.id/api/individuallyGroupAttribute', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => loadCompatibilityContentSuccess(response, dispatch))
      .catch(() =>
        loadCompatibilityContentFail(
          'Oops some error occured.. Please try agian!',
          dispatch,
        ),
      );
  };
};

const loadCompatibilityContentSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: LOAD_COMPATIBILITY_CONTENT_SUCCESS,
      payload: response,
    });
  } else {
    loadCompatibilityContentFail(response.msg, dispatch);
  }
};

const loadCompatibilityContentFail = (error, dispatch) => {
  dispatch({
    type: LOAD_COMPATIBILITY_CONTENT_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const loadCompatibilities = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: LOAD_COMPATIBILITY});
    fetch('https://syoobe.co.id/api/compatibilityGroupListing', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => loadCompatibilitiesSuccess(response, dispatch))
      .catch(() =>
        loadCompatibilitiesFail(
          'Oops some error occured.. Please try agian!',
          dispatch,
        ),
      );
  };
};

const loadCompatibilitiesSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: LOAD_COMPATIBILITY_SUCCESS,
      payload: response,
    });
  } else {
    loadCompatibilitiesFail(response.msg, dispatch);
  }
};

const loadCompatibilitiesFail = (error, dispatch) => {
  dispatch({
    type: LOAD_COMPATIBILITY_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const loadOptions = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: LOAD_OPTIONS});
    fetch('https://syoobe.co.id/api/optionAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => loadOptionsSuccess(response, dispatch))
      .catch(() =>
        loadOptionsFail(
          'Oops some error occured.. Please try agian!',
          dispatch,
        ),
      );
  };
};

const loadOptionsSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: LOAD_OPTIONS_SUCCESS,
      payload: response,
    });
  } else {
    loadOptionsFail(response.msg, dispatch);
  }
};

const loadOptionsFail = (error, dispatch) => {
  dispatch({
    type: LOAD_OPTIONS_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

//=============================================Create-Option====================================

export const createNewOption = (details) => {
  var formBody = [];
  for (var property in details) {
    if (property == 'option_values') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: CREATE_OPTION});
    fetch('https://syoobe.co.id/api/createUpdateOption', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => createNewOptionSuccess(response, dispatch))
      .catch((error) =>
        createNewOptionFail(
          'Oops some error occured.. Please try agian!',
          dispatch,
        ),
      );
  };
};

const createNewOptionSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: CREATE_OPTION_SUCCESS,
      payload: response,
    });
    showToast({
      message: 'New Option Created!',
    });
  } else {
    createNewOptionFail(response.msg, dispatch);
  }
};

const createNewOptionFail = (error, dispatch) => {
  dispatch({
    type: CREATE_OPTION_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

//=========================================addProductReleatedProduct=========================

export const getRelatedProducts = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_RELATED_PRODUCTS});
    fetch('https://syoobe.co.id/api/addProductReleatedProduct', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getRelatedProductsSuccess(response, dispatch))
      .catch((error) => getRelatedProductsFail(error, dispatch));
  };
};

const getRelatedProductsSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_RELATED_PRODUCTS_SUCCESS,
      payload: response,
    });
  } else {
    getRelatedProductsFail(response, dispatch);
  }
};

const getRelatedProductsFail = (error, dispatch) => {
  dispatch({
    type: GET_RELATED_PRODUCTS_FAIL,
    payload: error,
  });
};

//===========================================================================

export const getProdcutFilters = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_PRODUCT_FILTERS});
    fetch('https://syoobe.co.id/api/addProductProductFilters', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getProdcutFiltersSuccess(response, dispatch))
      .catch((error) => getProdcutFiltersFail(error, dispatch));
  };
};

const getProdcutFiltersSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_PRODUCT_FILTERS_SUCCESS,
      payload: response,
    });
  } else {
    getProdcutFiltersFail(response, dispatch);
  }
};

const getProdcutFiltersFail = (error, dispatch) => {
  dispatch({
    type: GET_PRODUCT_FILTERS_FAIL,
    payload: error,
  });
};

//==========================================specifications tab==================================

export const getSpecifications = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_SPECIFICATIONS});
    fetch('https://syoobe.co.id/api/specificationAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getSpecificationsSuccess(response, dispatch))
      .catch((error) => getSpecificationsFail(error, dispatch));
  };
};

const getSpecificationsSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_SPECIFICATIONS_SUCCESS,
      payload: response,
    });
  } else {
    getSpecificationsFail(response, dispatch);
  }
};

const getSpecificationsFail = (error, dispatch) => {
  dispatch({
    type: GET_SPECIFICATIONS_FAIL,
    payload: error,
  });
};

//===========================================================================

export const removeGeneralImages = () => {
  return {
    type: REMOVE_GENERAL_IMAGES,
  };
};

export const emptyProductData = () => {
  return {
    type: EMPTY_PRODUCT_DATA,
  };
};

export const delete_image_general = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: DELETE_IMAGE_GENERAL});
    fetch('https://syoobe.co.id/api/imageDelete', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        delete_image_general_success(response, dispatch, details.image_id),
      )
      .catch((error) =>
        delete_image_general_fail(
          'Oops something went wrong! Please try again',
          dispatch,
        ),
      );
  };
};

const delete_image_general_success = (response, dispatch, id) => {
  if (response.status == 1) {
    dispatch({
      type: DELETE_IMAGE_GENERAL_SUCCESS,
      payload: response,
      image_id: id,
    });
    showToast({
      message: 'Image removed!',
    });
  } else {
    delete_image_general_fail(response, dispatch);
  }
};

const delete_image_general_fail = (error, dispatch) => {
  dispatch({
    type: DELETE_IMAGE_GENERAL_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

//========================================upload===========================================

export const upload_images_general = (details, imageLength) => {
  var formBody = [];
  for (var property in details) {
    if (property == 'prod_image') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  }
  formBody = formBody.join('&');
  // let formData = new FormData();
  // formData.append(formBody);
  // console.log('de',formData);
  return (dispatch) => {
    dispatch({type: UPLOAD_IMAGES_GENERAL});
    fetch('https://syoobe.co.id/api/uploadProductImagesAPI', {
      method: 'POST',
      // headers: {
      //     Accept: 'application/json',
      //     'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      // },
      body: JSON.stringify(details),
    })
      .then((res) => res.json())
      .then((response) =>
        upload_images_generalSuccess(response, dispatch, imageLength),
      )
      .catch((err) =>
        upload_images_generalFail(
          'Oops something went wrong! Please try again',
          dispatch,
          err,
        ),
      );
  };
};

const upload_images_generalSuccess = (response, dispatch, imageLength) => {
  if (response.status == 1) {
    dispatch({
      type: UPLOAD_IMAGES_GENERAL_SUCCESS,
      payload: response,
      no_of_images: imageLength,
    });
  } else {
    upload_images_generalFail(response.msg, dispatch);
  }
};

const upload_images_generalFail = (error, dispatch, err) => {
  dispatch({
    type: UPLOAD_IMAGES_GENERAL_FAIL,
    payload: error,
  });
};

export const saveProduct = (details, navigation, task, currentTab) => {
  var formBody = [];
  // console.log('ini details', JSON.stringify (details));
  for (var property in details) {
    if (property == 'product_images') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else if (property == 'product_option') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        if (
          details[property][i].value == undefined ||
          details[property][i].value == null
        ) {
          var clonedObj = Object.assign({
            type: details[property][i].type,
            option_id: details[property][i].option_id,
            required: details[property][i].required,
            product_option_value: details[property][i].product_option_value,
          });
        } else {
          var clonedObj = Object.assign({
            type: details[property][i].type,
            option_id: details[property][i].option_id,
            required: details[property][i].required,
            value: details[property][i].value,
            product_option_value: details[property][i].product_option_value,
          });
        }
        array.push(clonedObj);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else if (property == 'product_shipping') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else if (property == 'product_download') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else if (property == 'product_attribute') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else if (property == 'product_filter') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        var obj = {product_filter_id: details[property][i]};
        array.push(obj);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else if (property == 'product_related') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        var obj = {product_related_id: details[property][i]};
        array.push(obj);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else if (property == 'product_discount') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else if (property == 'product_special') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else if (property == 'product_compatibility') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  }
  // console.log('ini respon formBody', JSON.stringify(formBody));
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: SAVE_PRODUCT});
    fetch('https://syoobe.co.id/api/addProduct', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        saveProductSuccess(response, dispatch, navigation, task, currentTab),
      )
      .catch((e) => saveProductFail(e, dispatch));
  };
};

const saveProductSuccess = (
  response,
  dispatch,
  navigation,
  task,
  currentTab,
) => {
  console.log('add product', JSON.stringify(response));
  if (response.status === 1) {
    dispatch({
      type: SAVE_PRODUCT_SUCCESS,
      payload: response,
      nextTab: task != 'exit' ? currentTab + 1 : null,
    });
    showToast({
      message: response.msg,
    });
    if (task == 'exit') {
      navigation.replace('HomeDrawer', {screen: 'SellerMyProductList'});
    }
  } else if (response.status === 2) {
    dispatch({
      type: SAVE_PRODUCT_FAIL,
    });

    Alert.alert('Informasi', response.msg, [
      {
        text: 'OK',
        onPress: () => {
          navigation.replace('HomeDrawer', {screen: 'EditProfile'});
        },
      },
    ]);
  } else if (response.status === 3) {
    dispatch({
      type: SAVE_PRODUCT_FAIL,
    });
    Alert.alert('Informasi', response.msg, [
      {
        text: 'OK',
        onPress: () => {
          navigation.replace('HomeDrawer', {screen: 'CreatShopStepOne'});
        },
      },
    ]);
  } else if (response.status === 4) {
    dispatch({
      type: SAVE_PRODUCT_FAIL,
    });
    Alert.alert('Informasi', response.msg, [
      {
        text: 'OK',
        onPress: () => {
          navigation.replace('ApplySellingLimit');
        },
      },
    ]);
  } else {
    saveProductFail(response.msg, dispatch);
  }
};

const saveProductFail = (error, dispatch) => {
  console.log(error);
  dispatch({
    type: SAVE_PRODUCT_FAIL,
    payload: error,
  });
  Alert.alert('Gagal', error, [{text: 'OK'}]);
};

export const getProcessingTime = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_PROCESSING_TIME});
    fetch('https://syoobe.co.id/api/processingTime', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getProcessingTimeSuccess(response, dispatch))
      .catch((error) => getProcessingTimeFail(error, dispatch));
  };
};

const getProcessingTimeSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_PROCESSING_TIME_SUCCESS,
      payload: response,
    });
  } else {
    getProcessingTimeFail(response, dispatch);
  }
};

const getProcessingTimeFail = (error, dispatch) => {
  dispatch({
    type: GET_PROCESSING_TIME_FAIL,
    payload: error,
  });
};

export const getBrands = () => {
  return (dispatch) => {
    dispatch({type: GET_BRANDS});
    fetch('https://syoobe.co.id/api/brandsAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
    })
      .then((res) => res.json())
      .then((response) => getBrandsSuccess(response, dispatch))
      .catch((error) => getBrandsFail(error, dispatch));
  };
};

const getBrandsSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_BRANDS_SUCCESS,
      payload: response,
    });
  } else {
    getBrandsFail(response, dispatch);
  }
};

const getBrandsFail = (error, dispatch) => {
  console.log('error', error);
  dispatch({
    type: GET_BRANDS_FAIL,
    payload: error,
  });
};

//==================================================================================

export const getOrderDetails = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_ORDER_DETAILS});
    Axios.post('https://syoobe.co.id/api/viewOrder', formBody)
      // .then((res) => res.json())
      .then((response) => getOrderDetailsSuccess(response.data, dispatch))
      .catch((error) => getOrderDetailsFail(error, dispatch));
  };
};

const getOrderDetailsSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_ORDER_DETAILS_SUCCESS,
      payload: response.data,
    });
  } else {
    getOrderDetailsFail(response, dispatch);
  }
};

const getOrderDetailsFail = (error, dispatch) => {
  console.log(error);
  dispatch({
    type: GET_ORDER_DETAILS_FAIL,
    payload: error,
  });
};

// =====================================================================================================

export const getBuyerDownloads = (details, searchType) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({
      type: GET_BUYER_DOWNLOAD_HISTORY,
      search: searchType,
    });
    fetch('https://syoobe.co.id/api/buyerMyDownloads', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        getBuyerDownloadsSuccess(details.page, response, dispatch),
      )
      .catch((error) => getBuyerDownloadsFail(error, dispatch));
  };
};

const getBuyerDownloadsSuccess = (page, response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_BUYER_DOWNLOAD_HISTORY_SUCCESS,
      payload: response,
      current_page: page,
    });
  } else {
    getBuyerDownloadsFail(response.downloadlist, dispatch);
  }
};

const getBuyerDownloadsFail = (error, dispatch) => {
  dispatch({
    type: GET_BUYER_DOWNLOAD_HISTORY_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const emptyDownloadHistory = () => {
  return {
    type: EMPTY_DOWNLOAD_HISTORY,
  };
};

// =====================================================================================================

export const getOrderHistoryStatusList = (_token) => {
  var details = {
    _token: _token,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_ORDER_STATUS_LIST});
    fetch('https://syoobe.co.id/api/orderStatusAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getOrderHistoryStatusList_Success(response, dispatch))
      .catch((error) => getOrderHistoryStatusList_Fail(error, dispatch));
  };
};

const getOrderHistoryStatusList_Success = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_ORDER_STATUS_LIST_SUCCESS,
      payload: response,
    });
  }
};

const getOrderHistoryStatusList_Fail = (error, dispatch) => {
  dispatch({
    type: GET_ORDER_STATUS_LIST_FAIL,
    payload: error,
  });
};

// =======================================================================================================

export const getBuyerOrders = (details, searchType) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({
      type: GET_BUYER_ORDER_HISTORY,
      search: searchType,
    });
    fetch('https://syoobe.co.id/api/myorderListing', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        getBuyerOrdersSuccess(details.page, response, dispatch),
      )
      .catch((error) => getBuyerOrdersFail(error, dispatch));
  };
};

const getBuyerOrdersSuccess = (page, response, dispatch) => {
  console.log(response);
  if (response.status == 1) {
    dispatch({
      type: GET_BUYER_ORDER_HISTORY_SUCCESS,
      payload: response,
      current_page: page,
    });
  } else {
    getBuyerOrdersFail(response.orderlist, dispatch);
  }
};

const getBuyerOrdersFail = (error, dispatch) => {
  dispatch({
    type: GET_BUYER_ORDER_HISTORY_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

export const emptyOrderHistory = () => {
  return {
    type: EMPTY_ORDER_HISTORY,
  };
};

// =======================================================================================================

export const deleteSellerProduct = (details, status) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: DELETE_SELLER_PRODUCT});
    fetch('https://syoobe.co.id/api/deleteItem', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        deleteSellerProductSuccess(status, response, dispatch),
      )
      .catch((error) => deleteSellerProductFail(error, dispatch));
  };
};

const deleteSellerProductSuccess = (status, response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: DELETE_SELLER_PRODUCT_SUCCESS,
      payload: response,
      status: status,
    });
    showToast({
      message: response.msg,
    });
  } else {
    deleteSellerProductFail(response, dispatch);
  }
};

const deleteSellerProductFail = (error, dispatch) => {
  dispatch({
    type: DELETE_SELLER_PRODUCT_FAIL,
    payload: error,
  });
  showToast({
    message: error.msg,
  });
};

// =======================================================================================================

export const changeProductStatus = (details, status) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: CHANGE_SELLER_PRODUCT_STATUS});
    fetch('https://syoobe.co.id/api/productStatusAPI', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        changeProductStatusSuccess(status, response, dispatch),
      )
      .catch((error) => changeProductStatusFail(error, dispatch));
  };
};

const changeProductStatusSuccess = (status, response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: CHANGE_SELLER_PRODUCT_STATUS_SUCCESS,
      payload: response,
      status: status,
    });
    showToast({
      message: response.msg,
    });
  } else {
    changeProductStatusFail(response, dispatch);
  }
};

const changeProductStatusFail = (error, dispatch) => {
  dispatch({
    type: CHANGE_SELLER_PRODUCT_STATUS_FAIL,
    payload: error,
  });
  showToast({
    message: error.msg,
  });
};

// =======================================================================================================

export const getSellerPausedItems = (_token) => {
  var details = {
    _token: _token,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_SELLER_PAUSED_ITEMS});
    fetch('https://syoobe.co.id/api/sellerPausedItemList', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getSellerPausedItemsSuccess(response, dispatch))
      .catch((error) => getSellerPausedItemsFail(error, dispatch));
  };
};

const getSellerPausedItemsSuccess = (response, dispatch) => {
  console.log(response);
  if (response.status == 1) {
    dispatch({
      type: GET_SELLER_PAUSED_ITEMS_SUCCESS,
      payload: response,
    });
  } else {
    getSellerPausedItemsFail(response, dispatch);
  }
};

const getSellerPausedItemsFail = (error, dispatch) => {
  console.log(error);
  dispatch({
    type: GET_SELLER_PAUSED_ITEMS_FAIL,
    payload: error,
  });
  showToast({
    message: error.paused_product,
  });
};

// =======================================================================================================

export const getSellerActiveItems = (_token) => {
  var details = {
    _token: _token,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_SELLER_ACTIVE_ITEMS});
    fetch('https://syoobe.co.id/api/sellerActiveItemList', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getSellerActiveItemsSuccess(response, dispatch))
      .catch((error) => getSellerActiveItemsFail(error, dispatch));
  };
};

const getSellerActiveItemsSuccess = (response, dispatch) => {
  console.log('ini res seller active', JSON.stringify(response));
  if (response.status === 1) {
    dispatch({
      type: GET_SELLER_ACTIVE_ITEMS_SUCCESS,
      payload: response,
    });
  } else {
    getSellerActiveItemsFail(response, dispatch);
  }
};

const getSellerActiveItemsFail = (error, dispatch) => {
  dispatch({
    type: GET_SELLER_ACTIVE_ITEMS_FAIL,
    payload: error,
  });
  showToast({
    message: error.active_product,
  });
};

// =======================================================================================================

export const saveAddressInfo = (navigation, details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: SAVE_ADDRESS_INFO});
    fetch('https://syoobe.co.id/api/return_info', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        saveAddressInfoSuccess(navigation, response, dispatch),
      )
      .catch((error) => saveAddressInfoFail(error, dispatch));
  };
};

const saveAddressInfoSuccess = (navigation, response, dispatch) => {
  console.log(response);
  if (response.status == 1) {
    dispatch({
      type: SAVE_ADDRESS_INFO_SUCCESS,
      payload: response,
    });
    navigation.pop();
    showToast({
      message: 'Alamat berhasil diupdate!',
    });
  } else {
    saveAddressInfoFail(response.msg, dispatch);
  }
};

const saveAddressInfoFail = (error, dispatch) => {
  console.log(error);
  dispatch({
    type: SAVE_ADDRESS_INFO_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

// ==========================================SAVE RETURN ADDRESS=============================================================

export const changeSellerFullName = (text) => {
  return {
    type: SELLER_FULL_NAME,
    payload: text,
  };
};

export const changeAddressLine1 = (text) => {
  return {
    type: SELLER_ADDRESS_LINE_1,
    payload: text,
  };
};

export const changeAddressLine2 = (text) => {
  return {
    type: SELLER_ADDRESS_LINE_2,
    payload: text,
  };
};

export const changeSellerCity = (text) => {
  return {
    type: SELLER_CITY,
    payload: text,
  };
};

export const changeSellerZipCode = (text) => {
  return {
    type: SELLER_ZIP_CODE,
    payload: text,
  };
};

export const changeSellerPhoneNumber = (text) => {
  return {
    type: SELLER_PHONE_NUMBER,
    payload: text,
  };
};

export const getAddressInfo = (_token) => {
  var details = {
    _token: _token,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_ADDRESS_INFO});
    fetch('https://syoobe.co.id/api/get_return_info', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getAddressInfoSuccess(response, dispatch))
      .catch((error) => getAddressInfoFail(error, dispatch));
  };
};

const getAddressInfoSuccess = (response, dispatch) => {
  console.log('adressinfo', response);
  if (response.status == 1) {
    dispatch({
      type: GET_ADDRESS_INFO_SUCCESS,
      payload: response.seller_return_info,
    });
  } else {
    getAddressInfoFail(response.msg, dispatch);
  }
};

const getAddressInfoFail = (error, dispatch) => {
  dispatch({
    type: GET_ADDRESS_INFO_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

// ==========================================GET FAVS=============================================================

export const getFavourites = (_token, list_id) => {
  let subFavUrl = 'favProdListAccordUserFavlist';
  var details = {
    _token: _token,
  };
  if (list_id) {
    details = {
      ...details,
      ...{
        list_id: list_id,
      },
    };
  }
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_FAVOURITES});
    fetch(`https://syoobe.co.id/api/${list_id ? subFavUrl : 'favouriteItem'}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getFavouritesSuccess(response, dispatch))
      .catch((error) => getFavouritesFail(error, dispatch));
  };
};

const getFavouritesSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_FAVOURITES_SUCCESS,
      payload: response,
    });
  } else {
    getFavouritesFail(response.data, dispatch);
  }
};

const getFavouritesFail = (error, dispatch) => {
  dispatch({
    type: GET_FAVOURITES_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

// ==========================================GET SHOP=============================================================

export const getShopDetails = (_token) => {
  var details = {
    _token: _token,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_SHOP_DETAILS});
    fetch('https://syoobe.co.id/api/getShopInfo', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getShopDetailsSuccess(response, dispatch))
      .catch((error) => getShopDetailsFail(error, dispatch));
  };
};

const getShopDetailsSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_SHOP_DETAILS_SUCCESS,
      payload: response,
    });
  }
  // else if(response.shop_info.length < 1){
  //     noShopforUser(response.msg, dispatch);
  // }
  else {
    getShopDetailsFail(response.msg, dispatch);
  }
};

// const noShopforUser = (error, dispatch) => {
//     dispatch({
//         type: NO_SHOP_DETAILS_FOR_USER,
//         payload: error
//     });
// }

const getShopDetailsFail = (error, dispatch) => {
  dispatch({
    type: GET_SHOP_DETAILS_FAIL,
    payload: error,
  });
};

// ==========================================SAVE SHOP=============================================================

export const saveShopDetails = (
  navigation,
  _token,
  shop_name,
  url_keywords,
  description,
  shop_city,
  logo,
  banner,
  country_id,
  state_id,
  announcement,
  message_to_buyer,
  shop_vendor_display_status,
  is_prod_tax,
  welcome_message,
  payment_policy,
  delivery_policy,
  refund_policy,
  additional_info,
  seller_info,
  shop_npwp,
  meta_tag_title,
  meta_tag_keywords,
  meta_tag_description,
  shop_id,
  shop_state_name,
  shop_district_id,
  shop_district_name,
  shop_city_id,
) => {
  console.log('shop_vendor_display_status', shop_vendor_display_status);

  let shop_is_prod_tax;
  if (is_prod_tax === true) {
    shop_is_prod_tax = 1;
  } else {
    shop_is_prod_tax = 0;
  }

  let display_status;
  if (shop_vendor_display_status !== false) {
    display_status = 1;
  } else {
    display_status = 0;
  }

  var details = {
    _token: _token,
    shop_name: shop_name,
    seo_url_keyword: url_keywords,
    shop_description: description,
    shop_city: shop_city,
    shop_country: 106,
    // shop_country: country_id,
    shop_state: state_id,
    shop_announcement: announcement,
    shop_general_message: message_to_buyer,
    shop_vendor_display_status: display_status,
    is_prod_tax: shop_is_prod_tax,
    shop_welcome_message: welcome_message,
    shop_payment_policy: payment_policy,
    shop_delivery_policy: delivery_policy,
    shop_refund_policy: refund_policy,
    shop_additional_info: additional_info,
    shop_seller_info: seller_info,
    shop_npwp: shop_npwp,
    shop_page_title: meta_tag_title,
    shop_meta_keywords: meta_tag_keywords,
    shop_meta_description: meta_tag_description,
    shop_id: shop_id,
    shop_state_name,
    shop_district_id,
    shop_district_name,
    shop_city_id,
  };

  console.log('ini respon123 2', JSON.stringify(details));

  if (logo != null) {
    if (logo.startsWith('data:image/png;base64')) {
      var pair = {shop_logo: logo};
      details = {...details, ...pair};
    }
  } else {
    var pair = {shop_logo: null};
    details = {...details, ...pair};
  }

  if (banner != null) {
    if (banner.startsWith('data:image/png;base64')) {
      var pair = {shop_banner: banner};
      details = {...details, ...pair};
    }
  } else {
    var pair = {shop_banner: null};
    details = {...details, ...pair};
  }

  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  // console.log('abc', formBody)
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: SAVE_SHOP});
    fetch('https://syoobe.co.id/api/manageShop', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => {
        saveShopDetailsSuccess(navigation, response, dispatch);
      })
      .catch((error) => {
        console.log(error.response);
        saveShopDetailsFail(
          error,
          navigation,
          'Save failed. Please try again!',
          dispatch,
        );
      });
  };
};

const saveShopDetailsSuccess = (navigation, response, dispatch) => {
  console.log('ini respon 34241', JSON.stringify(response));

  if (response.status === 1) {
    _storeShopId(response.shop_id);
    dispatch({
      type: SAVE_SHOP_SUCCESS,
      payload: response,
    });
    Alert.alert(
      'Informasi',
      'Untuk selanjutnya silahkan lengkapi data bank dan alamt pengembalian toko',
      [
        {
          text: 'OK',
          onPress: () =>
            navigation.replace('HomeDrawer', {
              screen: 'EditProfile',
            }),
        },
      ],
    );

    showToast({
      message:
        'Data tersimpan, silahkan lengkapi data alamat pengambalian dan informasi bank',
    });
  } else {
    saveShopDetailsFail(navigation, response.msg, dispatch);
  }
};

const saveShopDetailsFail = (error, navigation, errorMsg, dispatch) => {
  console.log('err', error);
  navigation.popToTop();
  dispatch({
    type: SAVE_SHOP_FAIL,
    payload: error,
  });
  showToast({
    message: errorMsg,
  });
};

export const skip_step_2 = () => {
  return {
    type: SKIP_STEP_2,
  };
};

export const skip_step_3 = () => {
  return {
    type: SKIP_STEP_3,
  };
};

export const setCountryId = (id) => {
  return {
    type: SET_COUNTRY,
    payload: id,
  };
};

export const setStateId = (id) => {
  return {
    type: SET_STATE,
    payload: id,
  };
};

export const setMetaTagTitle = (text) => {
  return {
    type: META_TAG_TITLE,
    payload: text,
  };
};

export const setMetaTagKeywords = (text) => {
  return {
    type: META_TAG_KEYWORDS,
    payload: text,
  };
};

export const setMetaTagDescription = (text) => {
  return {
    type: META_TAG_DESCRIPTION,
    payload: text,
  };
};

export const change_welcome_message = (text) => {
  return {
    type: WELCOME_MESSAGE,
    payload: text,
  };
};

export const change_payment_policy = (text) => {
  return {
    type: PAYMENT_POLICY,
    payload: text,
  };
};

export const change_delivery_policy = (text) => {
  return {
    type: DELIVERY_POLICY,
    payload: text,
  };
};

export const change_refund_policy = (text) => {
  return {
    type: REFUND_POLICY,
    payload: text,
  };
};

export const additional_information = (text) => {
  return {
    type: ADDITIONAL_INFORMATION,
    payload: text,
  };
};

export const seller_information = (text) => {
  return {
    type: SELLER_INFORMATION,
    payload: text,
  };
};

export const shoping_npwp = (text) => {
  return {
    type: SHOPING_NPWP,
    payload: text,
  };
};

export const display_status = () => {
  return {
    type: DISPLAY_STATUS,
  };
};

export const shop_is_prod_tax = () => {
  return {
    type: SHOP_IS_PROD_TAX,
  };
};

export const update_logo = (url) => {
  return {
    type: UPDATE_LOGO,
    payload: url,
  };
};

export const update_banner = (url) => {
  return {
    type: UPDATE_BANNER,
    payload: url,
  };
};

export const close_logo = () => {
  return {
    type: CLOSE_LOGO,
  };
};

export const close_banner = () => {
  return {
    type: CLOSE_BANNER,
  };
};

export const shopCity = (text) => {
  return {
    type: SHOP_CITY,
    payload: text,
  };
};

export const shop_announcement = (text) => {
  return {
    type: SHOP_ANNOUNCEMENT,
    payload: text,
  };
};

export const shop_district_id = (text) => {
  return {
    type: SHOP_DISTRICT_ID,
    payload: text,
  };
};
export const shop_district_name = (text) => {
  return {
    type: SHOP_DISTRICT_NAME,
    payload: text,
  };
};
export const shop_state_name = (text) => {
  return {
    type: SHOP_STATE_NAME,
    payload: text,
  };
};
export const shop_city_id = (text) => {
  return {
    type: SHOP_CITY_ID,
    payload: text,
  };
};

export const messageToBuyer = (text) => {
  return {
    type: MESSAGE_TO_BUYER,
    payload: text,
  };
};

export const shopDescription = (text) => {
  return {
    type: SHOP_DESCRIPTION,
    payload: text,
  };
};

export const changeShopName = (text) => {
  return {
    type: CHANGE_SHOP_NAME,
    payload: text,
  };
};

export const changeUrlKeywords = (text) => {
  return {
    type: CHANGE_URL_KEYWORDS,
    payload: text,
  };
};

// ==========================================FORGOT PASSWORD==============================================================

export const forgotPasswordChange = (text) => {
  return {
    type: FORGOT_PASSWORD_CHANGED,
    payload: text,
  };
};

export const sendResetPassword = (navigation, username) => {
  var details = {
    username: username,
  };
  console.log('details', details);
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: FORGOT_PASSWORD});
    fetch('https://syoobe.co.id/api/forgot_password', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        sendResetPasswordSuccess(navigation, response, dispatch),
      )
      .catch((error) => sendResetPasswordFail(error, dispatch));
  };
};

const sendResetPasswordSuccess = (navigation, response, dispatch) => {
  console.log('ini respo', response);
  if (response.status == 1) {
    dispatch({
      type: FORGOT_PASSWORD_SUCCESS,
      payload: response,
    });

    Alert.alert('Informasi', response.msg, [
      {text: 'OK', onPress: () => navigation.pop()},
    ]);

    // Alert.alert('Informasi', response.msg, [
    //   {text: 'OK', onPress: () => navigation.pop()},
    // ]);
  } else {
    sendResetPasswordFail(response.msg, dispatch);
  }
};

const sendResetPasswordFail = (error, dispatch) => {
  dispatch({
    type: FORGOT_PASSWORD_FAIL,
    payload: error,
  });
  Alert.alert('Informasi', error, [{text: 'OK'}]);
};

// ==========================================BANK DETAILS==============================================================

export const saveBankDetails = (fromWallet, navigation, data) => {
  if (
    data.ub_bank_name == '' ||
    data.ub_account_holder_name == '' ||
    data.ub_account_number == '' ||
    data.ub_ifsc_swift_code == '' ||
    data.ub_bank_address == ''
  ) {
    showToast({
      message: 'Please fill all the fields',
    });
  } else {
    let details = data;
    let formBody = [];
    for (let property in details) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    console.log('formBody', formBody);
    return (dispatch) => {
      dispatch({type: SAVE_BANK_DETAILS});
      fetch('https://syoobe.co.id/api/bank_info', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        body: formBody,
      })
        .then((res) => res.text())
        .then((response) => {
          saveBankDetailsSuccess(
            fromWallet,
            navigation,
            {status: 1, msg: 'akun bank berhasil diperbarui'},
            dispatch,
          );
        })
        .catch((error) => {
          saveBankDetailsFail(
            'Terjadi Kesalahan, Silahkan Coba Lagi',
            dispatch,
          );
        });
    };
  }
};

const saveBankDetailsSuccess = (fromWallet, navigation, response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: SAVE_BANK_DETAILS_SUCCESS,
      payload: response,
    });
    if (fromWallet) {
      navigation.replace('HomeDrawer', {screen: 'MyWallet'});
    } else {
      navigation.pop();
    }
    showToast({
      message: response.msg,
    });
  } else {
    saveBankDetailsFail(response.msg, dispatch);
  }
};

const saveBankDetailsFail = (error, dispatch) => {
  dispatch({
    type: SAVE_BANK_DETAILS_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

//=======================================================================================

export const changeBankName = (text) => {
  return {
    type: CHANGE_BANK_NAME,
    payload: text,
  };
};

export const changeAccountHolderName = (text) => {
  return {
    type: CHANGE_ACCOUNT_HOLDER_NAME,
    payload: text,
  };
};

export const changeAccountNumber = (text) => {
  return {
    type: CHANGE_ACCOUNT_NUMBER,
    payload: text,
  };
};

export const changeIfsc_code = (text) => {
  return {
    type: CHANGE_IFSC_CODE,
    payload: text,
  };
};

export const changePaypalId = (text) => {
  return {
    type: CHANGE_PAYPAL_ID,
    payload: text,
  };
};

export const changeBankAddress = (text) => {
  return {
    type: CHANGE_BANK_ADDRESS,
    payload: text,
  };
};

// ==========================================BANK DETAILS==============================================================

export const getBankDetails = (_token) => {
  var details = {
    _token: _token,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_BANK_DETAILS});
    fetch('https://syoobe.co.id/api/get_bank_info', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getBankDetailsSuccess(response, dispatch))
      .catch((error) => getBankDetailsFail(error, dispatch));
  };
};

const getBankDetailsSuccess = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_BANK_DETAILS_SUCCESS,
      payload: response.bank_info,
    });
  } else {
    getBankDetailsFail('Terjadi kesalahan', dispatch);
  }
};

const getBankDetailsFail = (error, dispatch) => {
  dispatch({
    type: GET_BANK_DETAILS_FAIL,
    payload: error,
  });
};

//=======================================================================================

export const newEmail = (text) => {
  return {
    type: NEW_EMAIL,
    payload: text,
  };
};

export const confirmNewEmail = (text) => {
  return {
    type: CONFIRM_NEW_EMAIL,
    payload: text,
  };
};

export const currentPasswordChangeEmail = (text) => {
  return {
    type: CURRENT_PASSWORD_CHANGE_EMAIL,
    payload: text,
  };
};

//=======================================================================================

export const saveChangeEmail = (
  navigation,
  _token,
  user_email,
  user_email1,
  current_password,
) => {
  if (user_email == '' || user_email1 == '' || current_password == '') {
    showToast({
      message: 'Please fill all the fields',
    });
  } else if (user_email != user_email1) {
    showToast({
      message: 'New email and Confirm email needs to be identical',
    });
  } else {
    var details = {
      _token: _token,
      user_email: user_email,
      user_email1: user_email1,
      current_password: current_password,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    return (dispatch) => {
      dispatch({type: SAVE_CHANGE_EMAIL});
      fetch('https://syoobe.co.id/api/change_email_address', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        body: formBody,
      })
        .then((res) => res.json())
        .then((response) =>
          saveChangeEmailSuccess(navigation, response, dispatch),
        )
        .catch((error) => saveChangeEmailFail(error, dispatch));
    };
  }
};

const saveChangeEmailSuccess = (navigation, response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: SAVE_CHANGE_EMAIL_SUCCESS,
      payload: response.status,
    });
    navigation.pop();
    showToast({
      message: response.msg,
    });
  } else {
    saveChangeEmailFail(response.msg, dispatch);
  }
};

export const saveChangeEmailFail = (error, dispatch) => {
  showToast({
    message: error,
  });
  dispatch({
    type: SAVE_CHANGE_EMAIL_FAIL,
    payload: error,
  });
};

//========================================================================================

export const saveChangePassword = (
  navigation,
  _token,
  current_pwd,
  new_pwd,
  confirm_pwd,
) => {
  var details = {
    _token: _token,
    current_pwd: current_pwd,
    new_pwd: new_pwd,
    confirm_pwd: confirm_pwd,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: SAVE_CHANGE_PASSWORD});
    fetch('https://syoobe.co.id/api/change_password', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        saveChangePasswordSuccess(navigation, response, dispatch),
      )
      .catch((error) => saveChangePasswordFail(error, dispatch));
  };
};

const saveChangePasswordSuccess = (navigation, response, dispatch) => {
  console.log('ini response', JSON.stringify(response));
  console.log('ini navigation', JSON.stringify(navigation));

  if (response.status === 1) {
    dispatch({
      type: SAVE_CHANGE_PASSWORD_SUCCESS,
      payload: response.status,
    });

    Alert.alert('Informasi', response.msg, [
      {
        text: 'OK',
        onPress: () =>
          removeToken()
            .then(() => {
              logOutUser(navigation);
              emptyNotifications();
              navigation.navigate('AuthDrawer', {screen: 'Login'});
            })
            .catch((error) => {
              Bugsnag.notify(error);
              console.log('error', error);
            }),
      },
    ]);
  } else {
    saveChangePasswordFail(response.msg, dispatch);
  }
};

export const saveChangePasswordFail = (error, dispatch) => {
  Alert.alert('Informasi', error, [{text: 'OK'}]);
  showToast({
    message: error,
  });
  dispatch({
    type: SAVE_CHANGE_PASSWORD_FAIL,
    payload: error,
  });
};

const removeToken = async () => {
  console.log('ini remove token');
  try {
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('fcmToken');
    await AsyncStorage.removeItem('deviceInfo');
    await AsyncStorage.removeItem('shop_id');
    await AsyncStorage.removeItem('image');
    await AsyncStorage.removeItem('name');
    await AsyncStorage.removeItem('email');
    await AsyncStorage.removeItem('emailStore');
    await AsyncStorage.removeItem('passwordStore');
  } catch (error) {
    console.log(error);
  }
};

//=======================================================================================

export const currentPassword = (text) => {
  return {
    type: CURRENT_PASSWORD,
    payload: text,
  };
};

export const newPassword = (text) => {
  return {
    type: NEW_PASSWORD,
    payload: text,
  };
};

export const confirmNewPassword = (text) => {
  return {
    type: CONFIRM_NEW_PASSWORD,
    payload: text,
  };
};

export const updateProfile = (navigation, details) => {
  let formBody = [];
  for (let property in details) {
    let encodedKey = encodeURIComponent(property);
    let encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: UPDATE_PROFILE});
    fetch('https://syoobe.co.id/api/update_profile_info', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(details),
    })
      .then((res) => res.json())
      .then((response) => updateProfileSuccess(navigation, response, dispatch))
      .catch((error) => updateProfileFail(error, dispatch));
  };
};

const updateProfileSuccess = (navigation, response, dispatch) => {
  console.log(response);
  if (response.status == 1) {
    dispatch({
      type: UPDATE_PROFILE_SUCCESS,
      payload: response.status,
    });
    navigation.navigate('HomeDrawer', {screen: 'EditProfile'});
    showToast({
      message: 'Berhasil, profile berhasil diperbarui',
    });
  } else {
    updateProfileFail('Terjadi kesalahan', dispatch);
  }
};

export const updateProfileFail = (error, dispatch) => {
  console.log(error);
  dispatch({
    type: UPDATE_PROFILE_FAIL,
    payload: error,
  });
};

export const getStates = ({country_id}) => {
  console.log(country_id);
  return (dispatch) => {
    dispatch({type: GET_STATES});
    fetch('https://syoobe.co.id/api/states?country_id=' + country_id, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((response) => getStates_Success(response, dispatch))
      .catch((error) => getStates_Fail(error, dispatch));
  };
};

const getStates_Success = (response, dispatch) => {
  console.log(response);
  if (response.status == 1) {
    dispatch({
      type: GET_STATES_SUCCESS,
      payload: response.states,
    });
  }
};

const getStates_Fail = (error, dispatch) => {
  dispatch({
    type: GET_STATES_FAIL,
    payload: error,
  });
};

export const getCarrierService = (details) => {
  if (details == undefined || details == null) {
    console.log('val', details);
    return (dispatch) => {
      dispatch({type: GET_CARRIER_SERVICE});
      fetch('https://syoobe.co.id/api/carrierService', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((res) => res.json())
        .then((response) => getCarrierService_Success(response, dispatch))
        .catch((error) => getCarrierService_Fail(error, dispatch));
    };
  } else {
    console.log('val1', details);
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    return (dispatch) => {
      dispatch({type: GET_CARRIER_SERVICE});
      fetch('https://syoobe.co.id/api/carrierService', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        body: formBody,
      })
        .then((res) => res.json())
        .then((response) => getCarrierService_Success(response, dispatch))
        .catch((error) => getCarrierService_Fail(error, dispatch));
    };
  }
};

const getCarrierService_Success = (response, dispatch) => {
  console.log(response);
  if (response.status == 1) {
    dispatch({
      type: GET_CARRIER_SERVICE_SUCCESS,
      payload: response.carrier_service_details,
    });
  }
};

const getCarrierService_Fail = (error, dispatch) => {
  dispatch({
    type: GET_CARRIER_SERVICE_FAIL,
    payload: error,
  });
};

export const getShippingCompany = () => {
  return (dispatch) => {
    dispatch({type: GET_SHIPPING_COMPANY});
    fetch('https://syoobe.co.id/api/shippingCompany', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((response) => getShippingCompany_Success(response, dispatch))
      .catch((error) => getShippingCompany_Fail(error, dispatch));
  };
};

const getShippingCompany_Success = (response, dispatch) => {
  console.log(response);
  if (response.status == 1) {
    dispatch({
      type: GET_SHIPPING_COMPANY_SUCCESS,
      payload: response.company,
    });
  }
};

const getShippingCompany_Fail = (error, dispatch) => {
  console.log(error);
  dispatch({
    type: GET_SHIPPING_COMPANY_FAIL,
    payload: error,
  });
};

export const getShipsToCountries = () => {
  return (dispatch) => {
    dispatch({type: GET_SHIPS_TO_COUNTRIES});
    fetch('https://syoobe.co.id/api/shipsToCountry', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((response) => getShipsToCountries_Success(response, dispatch))
      .catch((error) => getShipsToCountries_Fail(error, dispatch));
  };
};

const getShipsToCountries_Success = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_SHIPS_TO_COUNTRIES_SUCCESS,
      payload: response.country,
    });
  }
};

const getShipsToCountries_Fail = (error, dispatch) => {
  dispatch({
    type: GET_SHIPS_TO_COUNTRIES_FAIL,
    payload: error,
  });
};

export const getCountries = () => {
  return (dispatch) => {
    dispatch({type: GET_COUNTRIES});
    fetch('https://syoobe.co.id/api/countries', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((response) => getCountries_Success(response, dispatch))
      .catch((error) => getCountries_Fail(error, dispatch));
  };
};

const getCountries_Success = (response, dispatch) => {
  if (response.status == 1) {
    dispatch({
      type: GET_COUNTRIES_SUCCESS,
      payload: response.countries,
    });
  }
};

const getCountries_Fail = (error, dispatch) => {
  dispatch({
    type: GET_COUNTRIES_FAIL,
    payload: error,
  });
};

export const changePersonalInfoName = (text) => {
  return {
    type: PERSONAL_NAME_CHANGED,
    payload: text,
  };
};

export const changePersonalInfoPhone = (text) => {
  return {
    type: PERSONAL_PHONE_CHANGED,
    payload: text,
  };
};

export const changePersonalInfoCity = (text) => {
  return {
    type: PERSONAL_CITY_CHANGED,
    payload: text,
  };
};

// export const nameChanged = (text) => {
//     return {
//         type: NAME_CHANGED,
//         payload: text
//     };
// };

export const update_profile_pic = (
  navigation,
  {_token, user_profile_image},
) => {
  var details = {
    _token: _token,
    user_profile_image: user_profile_image,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: UPLOAD_PROFILE_PICTURE});
    fetch('https://syoobe.co.id/api/update_profile_pic', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        update_profile_pic_Success(
          navigation,
          response,
          dispatch,
          user_profile_image,
        ),
      )
      .catch((error) => update_profile_pic_Fail(error, dispatch));
  };
};

const removeImageUrl = async () => {
  try {
    await AsyncStorage.removeItem('image');
  } catch (error) {
    console.log(error);
  }
};

const storeImageUrl = async (image) => {
  try {
    await AsyncStorage.setItem('image', image);
  } catch (error) {
    console.log(error);
  }
};

const update_profile_pic_Success = (navigation, response, dispatch, image) => {
  if (response.status == true) {
    removeImageUrl();
    storeImageUrl(image);
    dispatch({
      type: UPLOAD_PROFILE_PICTURE_SUCCESS,
      payload: response,
    });
    navigation.navigate('HomeDrawer', {screen: 'EditProfile'});
    showToast({
      message: response.msg,
    });
  } else {
    update_profile_pic_Fail('Terjadi kesalahan', dispatch);
  }
};

export const update_profile_pic_Fail = (error, dispatch) => {
  dispatch({
    type: UPLOAD_PROFILE_PICTURE_FAIL,
    payload: error,
  });
  showToast({
    message: 'Image Upload failed!',
  });
};

// ==========================================PROFILE DETAILS==============================================================
export const getUserStatus = (user_id) => {
  var details = {
    user_id,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return new Promise((resolve, reject) => {
    return (
      Axios.post('https://syoobe.co.id/api/getUserStatus', formBody)
        // .then((res) => res.json())
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        })
    );
  });
};

export const getProfileDetails = (_token) => {
  var details = {
    _token: _token,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_PROFILE_DETAILS});
    fetch('https://syoobe.co.id/api/profile_info', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getProfileDetailsSuccess(response, dispatch))
      .catch((error) => getProfileDetailsFail(error, dispatch));
  };
};

const getProfileDetailsSuccess = (response, dispatch) => {
  console.log(response);
  if (response.name) {
    dispatch({
      type: GET_PROFILE_DETAILS_SUCCESS,
      payload: response,
    });
  } else {
    getProfileDetailsFail('Terjadi kesalahan', dispatch);
  }
};

export const getProfileDetailsFail = (error, dispatch) => {
  dispatch({
    type: GET_PROFILE_DETAILS_FAIL,
    payload: error,
  });
};

// ==========================================Recently Viewed==============================================================

// export const addProductToRecentlyViewed = (productDetails) => {
//     return {
//         type: RECENTLY_VIEWED,
//         payload: productDetails
//     };
// }

// ==========================================Unfavourite Product==============================================================

export const setProductAsUnfavourite = ({_token, product_id}) => {
  var details = {
    _token: _token,
    product_id: product_id,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: SET_UNFAVOURITE});
    fetch('https://syoobe.co.id/api/mark_product_favorite', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        setProductAsUnfavouriteSuccess(response, dispatch, product_id),
      )
      .catch((error) => setProductAsUnfavouriteFail(error, dispatch));
  };
};

const setProductAsUnfavouriteSuccess = (response, dispatch, product_id) => {
  if (response.status === true) {
    dispatch({
      type: SET_UNFAVOURITE_SUCCESS,
      payload: product_id,
    });
  } else {
    setProductAsUnfavouriteFail('Terjadi kesalahan', dispatch);
  }
};

export const setProductAsUnfavouriteFail = (error, dispatch) => {
  dispatch({
    type: SET_UNFAVOURITE_FAIL,
    payload: error,
  });
};

export const getProductFee = (user_id = null, prod_price = 0) => {
  return (dispatch) => {
    dispatch({type: GET_PRODUCT_FEE});
    fetch('https://syoobe.co.id/api/getFeeProduct', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((response) =>
        getProductFeeSuccess(response, dispatch, user_id, prod_price),
      )
      .catch((error) => getProductFeeFail(error, dispatch));
  };
};

const getProductFeeSuccess = (response, dispatch, user_id, prod_price) => {
  // console.log(response);
  if (response.status == 1) {
    let dcommission = 0;
    let pcommission = 0;
    let admin_commission = 0;

    if (response.admincommdigital.length > 0) {
      response.admincommdigital.map((digitalp) => {
        if (user_id === digitalp.commsetting_vendor) {
          dcommission = digitalp.commsetting_fees;
          admin_commission = (prod_price * dcommission) / 100;
        } else if (
          digitalp.commsetting_vendor == 0 &&
          digitalp.commsetting_category == 0 &&
          digitalp.commsetting_product == 0
        ) {
          dcommission = digitalp.commsetting_fees;
          admin_commission = (prod_price * dcommission) / 100;
        }
      });
    }
    if (response.admincommphysical.length > 0) {
      response.admincommphysical.map((physicalp) => {
        if (user_id === physicalp.commsetting_vendor) {
          pcommission = physicalp.commsetting_fees;
          admin_commission = (prod_price * pcommission) / 100;
        } else if (
          physicalp.commsetting_vendor == 0 &&
          physicalp.commsetting_category == 0 &&
          physicalp.commsetting_product == 0
        ) {
          pcommission = physicalp.commsetting_fees;
          admin_commission = (prod_price * pcommission) / 100;
        }
      });
    }
    // console.log({...response, dcommission, pcommission, admin_commission});
    dispatch({
      type: GET_PRODUCT_FEE_SUCCESS,
      payload: {...response, dcommission, pcommission, admin_commission},
    });
  } else {
    getProductFeeFail('Terjadi kesalahan', dispatch);
  }
};

export const getProductFeeFail = (error, dispatch) => {
  dispatch({
    type: GET_PRODUCT_FEE_FAIL,
    payload: error,
  });
};

// ==========================================Favourite Product==============================================================

export const setProductAsFavourite = ({_token, product_id}) => {
  var details = {
    _token: _token,
    product_id: product_id,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: SET_FAVOURITE});
    fetch('https://syoobe.co.id/api/mark_product_favorite?favorite=1', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) =>
        setProductAsFavouriteSuccess(response, dispatch, product_id),
      )
      .catch((error) => setProductAsFavouriteFail(error, dispatch));
  };
};

const setProductAsFavouriteSuccess = (response, dispatch, product_id) => {
  if (response.status === true) {
    dispatch({
      type: SET_FAVOURITE_SUCCESS,
      payload: product_id,
    });
  } else {
    setProductAsFavouriteFail('Terjadi kesalahan', dispatch);
  }
};

export const setProductAsFavouriteFail = (error, dispatch) => {
  dispatch({
    type: SET_FAVOURITE_FAIL,
    payload: error,
  });
};

// ==========================================Get Product by Category==============================================================

export const getDashboardDetails = ({_token}) => {
  var details = {
    _token: _token,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_DASHBOARD});
    fetch('https://syoobe.co.id/api/dashboard', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getDashboardDetailsSuccess(response, dispatch))
      .catch((error) => getDashboardDetailsFail(error, dispatch));
  };
};

const getDashboardDetailsSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: GET_DASHBOARD_SUCCESS,
      payload: response,
    });
  } else {
    getDashboardDetailsFail('Terjadi kesalahan', dispatch);
  }
};

export const getDashboardDetailsFail = (error, dispatch) => {
  dispatch({
    type: GET_DASHBOARD_FAIL,
    payload: error,
  });
};

// ==========================================Get Product by Category==============================================================

export const getProductsByCategory = (details) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_PRODUCT_LIST_BY_CATEGORY});
    fetch('https://syoobe.co.id/api/category_products', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => getProductsByCategorySuccess(response, dispatch))
      .catch(() => getProductsByCategoryFail('Terjadi kesalahan', dispatch));
  };
};

const getProductsByCategorySuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: GET_PRODUCT_LIST_BY_CATEGORY_SUCCESS,
      payload: response,
    });
    showToast({
      message: response.total_records + ' records found',
    });
  } else {
    getProductsByCategoryFail(response.msg, dispatch);
  }
};

export const getProductsByCategoryFail = (error, dispatch) => {
  dispatch({
    type: GET_PRODUCT_LIST_BY_CATEGORY_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

// ==========================================Edit Cart==============================================================

export const expandCategory = (updatedArray) => {
  return {
    type: EXPAND_CATEGORY,
    payload: updatedArray,
  };
};

// ==========================================Edit Cart==============================================================

export const getCategoryList = () => {
  return (dispatch) => {
    dispatch({type: GET_CATEGORY_LIST});
    fetch('https://syoobe.co.id/api/categoriesAPI', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    })
      .then((res) => res.json())
      .then((response) => getCategoryListSuccess(response, dispatch))
      .catch((error) => getCategoryListFail(error, dispatch));
  };
};

export const getCategoryListSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: GET_CATEGORY_LIST_SUCCESS,
      payload: response,
    });
  } else {
    getCategoryListFail('Terjadi kesalahan', dispatch);
  }
};

export const getCategoryListFail = (error, dispatch) => {
  dispatch({
    type: GET_CATEGORY_LIST_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

// ==========================================Edit Cart==============================================================

export const editCartItemCount = ({_token, product_key, quantity}) => {
  var details = {
    _token: _token,
    product_key: product_key,
    quantity: quantity,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: EDIT_CART_ITEM});
    fetch('https://syoobe.co.id/api/cart_edit_qty', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => editCartItemCountSuccess(response, dispatch))
      .catch((error) => editCartItemCountFail('Terjadi kesalahan', dispatch));
  };
};

export const editCartItemCountSuccess = (response, dispatch) => {
  // console.log('ini respon edit', JSON.stringify(response));
  if (response.status === 1) {
    dispatch({
      type: EDIT_CART_ITEM_SUCCESS,
      payload: response,
    });
  } else {
    editCartItemCountFail(response.msg, dispatch);
  }
};

export const editCartItemCountFail = (error, dispatch) => {
  console.log('fail', error);
  dispatch({
    type: EDIT_CART_ITEM_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

// ==========================================Delete Cart==============================================================

//this action is called when user clicks Delete Cart button.

export const deleteCartItem = ({_token, item_key}) => {
  var details = {
    _token: _token,
    item_key: item_key,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: DELETE_CART_ITEM});
    fetch('https://syoobe.co.id/api/cart_remove_item_api', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => deleteCartItemSuccess(response, dispatch))
      .catch((error) => deleteCartItemFail(error, dispatch));
  };
};

export const deleteCartItemSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: DELETE_CART_ITEM_SUCCESS,
      payload: response,
    });
    console.log('ini deleteCartItemSuccess', response);
  } else {
    deleteCartItemFail('Terjadi Kesalahan, Silahkan Coba Lagi', dispatch);
  }
};

export const deleteCartItemFail = (error, dispatch) => {
  dispatch({
    type: DELETE_CART_ITEM_FAIL,
    payload: error,
  });
  showToast({
    message: error,
  });
};

// ==========================================View Cart==============================================================

//this action is called when user opens the View Cart Page.

export const viewCart = ({_token}) => {
  if (_token == null) {
    return {
      type: VIEW_CART_FAIL,
      payload: 0,
    };
  } else {
    var details = {
      _token: _token,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    return (dispatch) => {
      dispatch({type: VIEW_CART});
      fetch('https://syoobe.co.id/api/view_cart_app', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        body: formBody,
      })
        .then((res) => res.json())
        .then((response) => viewCartSuccess(response, dispatch))
        .catch((error) => viewCartFail('Some error occureds', dispatch));
    };
  }
};

export const viewCartSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: VIEW_CART_SUCCESS,
      payload: response,
    });
  } else {
    viewCartFail('Silahkan login kembali', dispatch);
  }
};

export const viewCartFail = (error, dispatch) => {
  dispatch({
    type: VIEW_CART_FAIL,
    payload: 0,
  });
};

// ==========================================add to cart==============================================================

export const addToCart = ({_token, product_id, quantity, option}) => {
  var details = {
    _token: _token,
    product_id: product_id,
    quantity: quantity,
  };
  if (option) {
    details = {
      ...details,
      ...{option: option},
    };
  }
  var formBody = [];
  for (var property in details) {
    if (property == 'option') {
      var array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: ADD_TO_CART});
    return fetch('https://syoobe.co.id/api/add_to_cart_app_api', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((response) => addToCartSuccess(response, dispatch, _token))
      .catch((error) => addToCartFail(error, dispatch));
  };
};

export const addToCartSuccess = (response, dispatch, _token) => {
  console.log('ini respon', response);

  if (response.status === true) {
    dispatch({
      type: ADD_TO_CART_SUCCESS,
      payload: response,
    });
    // showToast({
    //   message: response.msg,
    // });
  } else {
    addToCartFail(response.msg, dispatch);
  }
};

export const addToCartFail = (error, dispatch) => {
  console.log('ini error', error);
  dispatch({
    type: ADD_TO_CART_FAIL,
    payload: error,
  });
  // showToast({
  //   message: error,
  // });
  Alert.alert('Gagal', error, [{text: 'OK'}]);
};

// ----------------home-------------------------------------------------------

/**
 * With Token
 */
export const homeProductsWithToken = ({_token}) => {
  var details = {
    _token: _token,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return (dispatch) => {
    dispatch({type: GET_HOME_TOKEN_PRODUCTS});
    fetch('https://syoobe.co.id/api/home?_token=' + _token, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((homeProducts) => homeProductsTokenSuccess(homeProducts, dispatch))
      .catch((error) => homeProductsFail('Please try agin!', dispatch));
  };
};

export const homeProductsTokenSuccess = (response, dispatch) => {
  if (response.status === 1) {
    dispatch({
      type: GET_HOME_PRODUCTS_TOKEN_SUCCESS,
      payload: response.data,
      bannerImages: response.slider_array,
    });
  } else {
    homeProductsTokenFail(response.msg, dispatch);
  }
};

export const homeProductsTokenFail = (error, dispatch) => {
  dispatch({
    type: GET_HOME_PRODUCTS_TOKEN_FAIL,
    payload: error,
  });
  showToast({message: error});
};

/**
 * Without token
 */
export const homeProducts = () => {
  return (dispatch) => {
    dispatch({type: GET_HOME_PRODUCTS});
    fetch('https://syoobe.co.id/api/home', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
    })
      .then((res) => res.json())
      .then((response) => homeProductsSuccess(response, dispatch))
      .catch((res) => homeProductsFailNoInternet(res.msg, dispatch));
  };
};

export const homeProductsSuccess = (response, dispatch) => {
  // console.log('ini response 123432', JSON.stringify(response.data));
  if (response.status === 1) {
    dispatch({
      type: GET_HOME_PRODUCTS_SUCCESS,
      payload: response.data,
      bannerImages: response.slider_array,
    });
  } else {
    homeProductsFail(response.msg, dispatch);
  }
};

export const homeProductsFail = (error, dispatch) => {
  dispatch({
    type: GET_HOME_PRODUCTS_FAIL,
    payload: error,
  });
  showToast({message: error});
};

export const homeProductsFailNoInternet = (error, dispatch) => {
  dispatch({
    type: GET_HOME_PRODUCTS_FAIL_NO_INTERNET,
    payload: error,
  });
  showToast({message: error});
};

//---------------------login---------------------------------------------------

export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text,
  };
};

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text,
  };
};

const _storeData = async (token) => {
  try {
    await AsyncStorage.setItem('token', token);
  } catch (error) {
    console.log(error);
  }
};

const _storeUserId = async (user_id) => {
  try {
    await AsyncStorage.setItem('user_id', user_id);
  } catch (error) {
    console.log(error);
  }
};

const _storeShopId = async (shop_id) => {
  try {
    await AsyncStorage.setItem('shop_id', shop_id.toString());
    console.log('ini shopid', shop_id);
  } catch (error) {
    console.log('error123', error);
  }
};

export const loginUser = (
  navigation,
  username,
  password,
  device_token,
  otp,
) => {
  console.log('index device token', device_token);
  return (dispatch) => {
    dispatch({type: LOGIN_USER});
    fetch('https://syoobe.co.id/api/otpVerification', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username,
        password,
        device_token,
        otp,
      }),
    })
      .then((res) => res.json())
      .then((user) => {
        loginUserSuccess(navigation, dispatch, user);
      })
      .catch((err) => {
        loginUserFail(dispatch, err);
      });
  };
};

const loginUserSuccess = (navigation, dispatch, user) => {
  console.log('respon', JSON.stringify(user));
  if (user.status === 1) {
    _storeData(user.token);
    _storeUserId(user.user_id);
    if (user.shop_id != 0) {
      _storeShopId(user.shop_id);
    }
    dispatch({
      type: LOGIN_USER_SUCCESS,
      payload: user,
    });
    dispatch({
      type: CLEAR_HOME_PRODUCTS,
    });
    showToast({message: user.msg});
    navigation.replace('HomeDrawer', {screen: 'Home'});

    // Alert.alert('Informasi', 'Login Berhasil', [
    //   {
    //     text: 'OK',
    //     onPress: () => navigation.replace('HomeDrawer', {screen: 'Home'}),
    //   },
    // ]);
  } else if (user.status == 2) {
    loginUserFailVerify(dispatch, user.msg);
  } else {
    loginUserFail(dispatch, user.msg);
  }
};

const loginUserFail = (dispatch, error) => {
  dispatch({
    type: LOGIN_USER_FAIL,
  });
  Alert.alert('Gagal', String(error).replace(/<\/?[^>]+(>|$)/g, ''), [
    {text: 'OK'},
  ]);
};

const loginUserFailVerify = (dispatch, error) => {
  dispatch({
    type: LOGIN_USER_FAIL_VERIFY,
    payload: error,
  });
};

export const setVerifyFalse = () => {
  return {
    type: SET_VERIFY_FALSE,
  };
};

//========================Otp Send=======================

export const sendOtpVerification = (navigation, username, password) => {
  console.log(username, password);
  return (dispatch) => {
    dispatch({type: SEND_VERIFY_OTP});
    fetch('https://syoobe.co.id/api/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username,
        password,
      }),
    })
      .then((res) => res.json())
      .then((user) => {
        sendOtpVerificationSuccess(
          navigation,
          dispatch,
          user,
          username,
          password,
        );
      })
      .catch(() => {
        sendOtpVerificationFail(dispatch, 'Login gagal');
      });
  };
};

const sendOtpVerificationSuccess = (
  navigation,
  dispatch,
  user,
  username,
  password,
) => {
  console.log('sendOtpVerificationSuccess', user);
  if (user.status === 1) {
    dispatch({
      type: SEND_VERIFY_OTP_SUCCESS,
      payload: user,
    });
    showToast({
      message: 'Kode OTP Sudah Terkirim Ke Nomor Kamu',
    });
    navigation.navigate('Otp', {username, password});
  } else if (user.status == 2) {
    loginUserFailVerify(dispatch, user.msg);
  } else {
    sendOtpVerificationFail(dispatch, user.msg);
  }
};

const sendOtpVerificationFail = (dispatch, error) => {
  console.log('sendOtpVerificationFail', error);

  dispatch({
    type: SEND_VERIFY_OTP_FAIL,
  });
  Alert.alert('Informasi', error, [{text: 'OK'}]);
};

//---------------------logout---------------------------------------------------

export const logOutUser = (navigation) => {
  return (dispatch) => {
    dispatch({type: LOGOUT_USER});
    fetch('https://syoobe.co.id/api/logout', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((response) => logoutUserSuccess(navigation, dispatch, response))
      .catch(() => logoutUserFail(dispatch));
  };
};

/**
 * Remove the locally stored token when the user logs out.
 */
// const removeToken = async () => {
//     try {
//       await AsyncStorage.removeItem('token');
//     } catch (error) {
//       console.log(error);
//     }
// }

const logoutUserSuccess = async (navigation, dispatch, response) => {
  console.log('logoutUserSuccess');
  dispatch({
    type: LOGOUT_USER_SUCCESS,
    payload: response,
  });
  dispatch({
    type: CLEAR_HOME_PRODUCTS,
  });
  navigation.navigate('AuthDrawer', {screen: 'Login'});
  showToast({
    message: 'Logged Out',
  });
  // } else{
  //     logoutUserFail(dispatch);
  // }
};

const logoutUserFail = (dispatch) => {
  dispatch({
    type: LOGOUT_USER_FAIL,
  });
  showToast({
    message: 'Terjadi kesalahan',
  });
};

//---------------------registation---------------------------------------------------

export const nameChanged = (text) => {
  return {
    type: NAME_CHANGED,
    payload: text,
  };
};

export const usernameChanged = (text) => {
  return {
    type: USERNAME_CHANGED,
    payload: text,
  };
};

export const regEmailChanged = (text) => {
  return {
    type: REG_EMAIL_CHANGED,
    payload: text,
  };
};

export const regPasswordChanged = (text) => {
  return {
    type: REG_PASSWORD_CHANGED,
    payload: text,
  };
};

export const registerUser = (
  navigation,
  {name, username, email, password, user_phone, user_isd_phone_code},
) => {
  return (dispatch) => {
    dispatch({type: REGISTER_USER});
    fetch('https://syoobe.co.id/api/signup', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username,
        password,
        name,
        email,
        user_phone,
        user_isd_phone_code,
      }),
    })
      .then((res) => res.json())
      .then((user) =>
        registrationSuccess(
          navigation,
          dispatch,
          user,
          email,
          password,
          name,
          username,
        ),
      )
      .catch(() => registrationFail(dispatch, 'Terjadi Kesalahan'));
  };
};

const registrationSuccess = (
  navigation,
  dispatch,
  user,
  email,
  password,
  name,
  username,
) => {
  if (user.status === 1) {
    var data = queryString.stringify({
      email: email,
      password: password,
      name: name,
      username: username,
    });
    var config = {
      method: 'post',
      url:
        'https://us-central1-syoobe-messages.cloudfunctions.net/app/register',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: data,
    };

    Axios(config).then((res) =>
      console.log(' ini res firebase', JSON.stringify(res)),
    );

    dispatch({
      type: REGISTRATION_SUCCESS,
      payload: user,
    });
    Alert.alert(
      'Sukses',
      'Registrasi berhasil, link verifikasi telah dikirim ke alamat email kamu',
      [{text: 'OK', onPress: () => navigation.navigate('Login')}],
    );
  } else {
    registrationFail(dispatch, user.msg);
  }
};

const registrationFail = (dispatch, error) => {
  dispatch({
    type: REGISTRATION_FAIL,
  });
  Alert.alert('Gagal', error, [{text: 'OK'}]);
};

export const setAmount = (amount) => (dispatch) => {
  dispatch({type: 'CHANGE_PAYABLE_AMOUNT', payload: amount});
};

export const handleSelectShippingMethod = (details) => (dispatch) => {
  var formBody = [];
  for (var property in details) {
    if (property == 'shipping_locations') {
      let array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  }
  formBody = formBody.join('&');
  return new Promise((resolve, reject) => {
    return Axios.post(
      'https://syoobe.co.id/api/saveShippingAddressDetailsAPI',
      formBody,
    )
      .then((response) => {
        console.log(response);
        resolve(response.data);
      })
      .catch((err) => reject(err.response));
  });
};

export const clearCartApi = (token) => (dispatch) => {
  let details = {
    _token: token,
  };
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  Axios.post('https://syoobe.co.id/api/clearCartAPI', formBody).then(
    (response) => {
      console.log('clear', response);
    },
  );
};

export const updateUserStatus = (status, _token) => {
  let details = {status, _token};
  // console.log(details);
  let formBody = [];
  for (let property in details) {
    let encodedKey = encodeURIComponent(property);
    let encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  Axios.post('https://syoobe.co.id/api/update_user_status', formBody)
    // .then((res) => res.json())
    .then((response) => console.log(response))
    .catch((error) => console.log(error));
};

export const getSalesList = (details) => (dispatch) => {
  var formBody = [];
  for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  Axios.post('https://syoobe.co.id/api/mysaleListing', formBody).then(
    (response) => {
      // console.log('tets get list sales', JSON.stringify(response.data));
      if (response.data.status == 1) {
        dispatch({
          type: GET_SALES_LIST,
          payload: response.data.sales_details_listing,
        });
      } else {
        showToast({
          message: 'Tidak ada catatan yang ditemukan',
        });
        dispatch({type: GET_SALES_LIST, payload: null});
      }
    },
  );
};
export const setValue = (type, payload) => (dispatch) => {
  dispatch({type, payload});
};
