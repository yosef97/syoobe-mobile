export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';

export const SEND_VERIFY_OTP = 'send_verify_otp';
export const SEND_VERIFY_OTP_SUCCESS = 'send_verify_otp_success';
export const SEND_VERIFY_OTP_FAIL = 'send_verify_otp_fail';

export const LOGIN_USER = 'login_user';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER_FAIL_VERIFY = 'login_user_fail_verify';
export const SET_VERIFY_FALSE = 'set_verify_false';
export const LOGOUT_USER = 'logout_user';
export const LOGOUT_USER_SUCCESS = 'logout_user_success';
export const LOGOUT_USER_FAIL = 'logout_user_fail';
export const REGISTER_USER = 'register_user';
export const NAME_CHANGED = 'name_changed';
export const USERNAME_CHANGED = 'username_changed';
export const REG_PASSWORD_CHANGED = 'reg_password_changed';
export const REG_EMAIL_CHANGED = 'reg_email_changed';
export const REGISTRATION_SUCCESS = 'register_user_success';
export const REGISTRATION_FAIL = 'register_user_fail';


export const GET_HOME_PRODUCTS = 'get_home_products';
export const GET_HOME_PRODUCTS_SUCCESS = 'get_home_products_success';
export const GET_HOME_PRODUCTS_FAIL = 'get_home_products_fail';

export const GET_HOME_TOKEN_PRODUCTS = 'get_home_token_products';
export const GET_HOME_PRODUCTS_TOKEN_SUCCESS =
  'get_home_products_token_success';
export const GET_HOME_PRODUCTS_TOKEN_FAIL = 'get_home_products_token_fail';


export const ADD_TO_CART = 'add_to_cart';
export const ADD_TO_CART_SUCCESS = 'add_to_cart_success';
export const ADD_TO_CART_FAIL = 'add_to_cart_fail';
export const VIEW_CART = 'view_cart';
export const VIEW_CART_SUCCESS = 'view_cart_success';
export const VIEW_CART_FAIL = 'view_cart_fail';
export const DELETE_CART_ITEM = 'delete_cart_item';
export const DELETE_CART_ITEM_SUCCESS = 'delete_cart_item_success';
export const DELETE_CART_ITEM_FAIL = 'delete_cart_item_fail';
export const EDIT_CART_ITEM = 'edit_cart_item';
export const EDIT_CART_ITEM_SUCCESS = 'edit_cart_item_success';
export const EDIT_CART_ITEM_FAIL = 'edit_cart_item_fail';
export const GET_CATEGORY_LIST = 'get_category_list';
export const GET_CATEGORY_LIST_SUCCESS = 'get_category_list_success';
export const GET_CATEGORY_LIST_FAIL = 'get_category_list_fail';
export const EXPAND_CATEGORY = 'expand_category';
export const GET_PRODUCT_LIST_BY_CATEGORY = 'get_product_list_by_category';
export const GET_PRODUCT_LIST_BY_CATEGORY_SUCCESS =
  'get_product_list_by_category_success';
export const GET_PRODUCT_LIST_BY_CATEGORY_FAIL =
  'get_product_list_by_category_fail';
export const GET_DASHBOARD = 'get_dashboard';
export const GET_DASHBOARD_SUCCESS = 'get_dashboard_success';
export const GET_DASHBOARD_FAIL = 'get_dashboard_fail';
export const SET_FAVOURITE = 'set_favourite';
export const SET_FAVOURITE_SUCCESS = 'set_favourite_success';
export const SET_FAVOURITE_FAIL = 'set_favourite_fail';
export const SET_UNFAVOURITE = 'set_unfavourite';
export const SET_UNFAVOURITE_SUCCESS = 'set_unfavourite_success';
export const SET_UNFAVOURITE_FAIL = 'set_unfavourite_fail';
export const GET_PROFILE_DETAILS = 'get_profile_detials';
export const GET_PROFILE_DETAILS_SUCCESS = 'get_profile_detials_success';
export const GET_PROFILE_DETAILS_FAIL = 'get_profile_detials_fail';
export const UPLOAD_PROFILE_PICTURE = 'upload_profile_picture';
export const UPLOAD_PROFILE_PICTURE_SUCCESS = 'upload_profile_picture_success';
export const UPLOAD_PROFILE_PICTURE_FAIL = 'upload_profile_picture_fail';
export const PERSONAL_NAME_CHANGED = 'personal_name_changed';
export const PERSONAL_PHONE_CHANGED = 'personal_phone_changed';
export const PERSONAL_CITY_CHANGED = 'personal_city_changed';
export const GET_COUNTRIES = 'get_countries';
export const GET_COUNTRIES_SUCCESS = 'get_countries_success';
export const GET_COUNTRIES_FAIL = 'get_countries_fail';
export const GET_STATES = 'get_States';
export const GET_STATES_SUCCESS = 'get_States_success';
export const GET_STATES_FAIL = 'get_States_fail';
export const UPDATE_PROFILE = 'update_profile';
export const UPDATE_PROFILE_SUCCESS = 'update_profile_success';
export const UPDATE_PROFILE_FAIL = 'update_profile_fail';
export const CURRENT_PASSWORD = 'current_password';
export const NEW_PASSWORD = 'new_password';
export const CONFIRM_NEW_PASSWORD = 'confirm_new_password';
export const SAVE_CHANGE_PASSWORD = 'save_change_password';
export const SAVE_CHANGE_PASSWORD_SUCCESS = 'save_change_password_success';
export const SAVE_CHANGE_PASSWORD_FAIL = 'save_change_password_fail';
export const NEW_EMAIL = 'new_email';
export const CONFIRM_NEW_EMAIL = 'confirm_new_email';
export const CURRENT_PASSWORD_CHANGE_EMAIL = 'current_password_change_email';
export const SAVE_CHANGE_EMAIL = 'save_change_email';
export const SAVE_CHANGE_EMAIL_SUCCESS = 'save_change_email_success';
export const SAVE_CHANGE_EMAIL_FAIL = 'save_change_email_fail';
export const GET_BANK_DETAILS = 'get_bank_details';
export const GET_BANK_DETAILS_SUCCESS = 'get_bank_details_success';
export const GET_BANK_DETAILS_FAIL = 'get_bank_details_fail';
export const CHANGE_BANK_NAME = 'change_bank_name';
export const CHANGE_ACCOUNT_HOLDER_NAME = 'change_account_holder_name';
export const CHANGE_ACCOUNT_NUMBER = 'change_account_number';
export const CHANGE_IFSC_CODE = 'change_ifsc_code';
export const CHANGE_PAYPAL_ID = 'change_paypal_id';
export const CHANGE_BANK_ADDRESS = 'change_bank_address';
export const SAVE_BANK_DETAILS = 'save_bank_details';
export const SAVE_BANK_DETAILS_SUCCESS = 'save_bank_details_success';
export const SAVE_BANK_DETAILS_FAIL = 'save_bank_details_fail';
export const FORGOT_PASSWORD_CHANGED = 'forgot_password_changed';
export const FORGOT_PASSWORD = 'forgot_password';
export const FORGOT_PASSWORD_SUCCESS = 'forgot_password_success';
export const FORGOT_PASSWORD_FAIL = 'forgot_password_fail';
export const CHANGE_SHOP_NAME = 'change_shop_name';
export const CHANGE_URL_KEYWORDS = 'change_url_keywords';
export const SHOP_DESCRIPTION = 'shop_description';
export const SHOP_CITY = 'shop_city';
export const SHOP_ANNOUNCEMENT = 'shop_announcement';
export const MESSAGE_TO_BUYER = 'message_to_buyer';
export const UPDATE_LOGO = 'update_logo';
export const UPDATE_BANNER = 'update_banner';
export const CLOSE_LOGO = 'close_logo';
export const CLOSE_BANNER = 'close_banner';
export const SHOP_IS_PROD_TAX = 'shop_is_prod_tax';
export const DISPLAY_STATUS = 'display_status';
export const WELCOME_MESSAGE = 'welcome_message';
export const PAYMENT_POLICY = 'payment_policy';
export const DELIVERY_POLICY = 'delivery_policy';
export const REFUND_POLICY = 'refund_policy';
export const ADDITIONAL_INFORMATION = 'additional_information';
export const SELLER_INFORMATION = 'seller_information';
export const SHOPING_NPWP = 'shoping_npwp';
export const META_TAG_TITLE = 'meta_tag_title';
export const META_TAG_KEYWORDS = 'meta_tag_keywords';
export const META_TAG_DESCRIPTION = 'meta_tag_description';
export const SAVE_SHOP = 'save_shop';
export const SAVE_SHOP_SUCCESS = 'save_shop_success';
export const SAVE_SHOP_FAIL = 'save_shop_fail';
export const SET_COUNTRY = 'set_country';
export const SET_STATE = 'set_state';
export const SKIP_STEP_2 = 'skip_step_2';
export const SKIP_STEP_3 = 'skip_step_3';
export const GET_SHOP_DETAILS = 'get_shop_details';
export const GET_SHOP_DETAILS_SUCCESS = 'get_shop_details_success';
export const GET_SHOP_DETAILS_FAIL = 'get_shop_details_fail';
// export const NO_SHOP_DETAILS_FOR_USER = 'no_shop_details_for_user';
export const GET_FAVOURITES = 'get_favourites';
export const GET_FAVOURITES_SUCCESS = 'get_favourites_success';
export const GET_FAVOURITES_FAIL = 'get_favourites_fail';
export const GET_ADDRESS_INFO = 'get_address_info';
export const GET_ADDRESS_INFO_SUCCESS = 'get_address_info_success';
export const GET_ADDRESS_INFO_FAIL = 'get_address_info_fail';
export const SELLER_FULL_NAME = 'seller_full_name';
export const SELLER_ADDRESS_LINE_1 = 'seller_address_line_1';
export const SELLER_ADDRESS_LINE_2 = 'seller_address_line_2';
export const SELLER_CITY = 'seller_city';
export const SELLER_ZIP_CODE = 'seller_zip_code';
export const SELLER_PHONE_NUMBER = 'seller_phone_number';
export const SAVE_ADDRESS_INFO = 'save_address_info';
export const SAVE_ADDRESS_INFO_SUCCESS = 'save_address_info_success';
export const SAVE_ADDRESS_INFO_FAIL = 'save_address_info_fail';
export const GET_SELLER_ACTIVE_ITEMS = 'get_seller_active_items';
export const GET_SELLER_ACTIVE_ITEMS_SUCCESS =
  'get_seller_active_items_success';
export const GET_SELLER_ACTIVE_ITEMS_FAIL = 'get_seller_active_items_fail';
export const GET_SELLER_PAUSED_ITEMS = 'get_seller_paused_items';
export const GET_SELLER_PAUSED_ITEMS_SUCCESS =
  'get_seller_paused_items_success';
export const GET_SELLER_PAUSED_ITEMS_FAIL = 'get_seller_paused_items_fail';
export const CHANGE_SELLER_PRODUCT_STATUS = 'change_seller_product_status';
export const CHANGE_SELLER_PRODUCT_STATUS_SUCCESS =
  'change_seller_product_status_success';
export const CHANGE_SELLER_PRODUCT_STATUS_FAIL =
  'change_seller_product_status_fail';
export const DELETE_SELLER_PRODUCT = 'delete_seller_product';
export const DELETE_SELLER_PRODUCT_SUCCESS = 'delete_seller_product_success';
export const DELETE_SELLER_PRODUCT_FAIL = 'delete_seller_product_fail';
export const GET_BUYER_ORDER_HISTORY = 'get_buyer_order_history';
export const GET_BUYER_ORDER_HISTORY_SUCCESS =
  'get_buyer_order_history_success';
export const GET_BUYER_ORDER_HISTORY_FAIL = 'get_buyer_order_history_fail';
export const GET_ORDER_STATUS_LIST = 'get_order_status_list';
export const GET_ORDER_STATUS_LIST_SUCCESS = 'get_order_status_list_success';
export const GET_ORDER_STATUS_LIST_FAIL = 'get_order_status_list_fail';
export const EMPTY_ORDER_HISTORY = 'empty_order_history';

export const GET_BUYER_DOWNLOAD_HISTORY = 'get_buyer_download_history';
export const GET_BUYER_DOWNLOAD_HISTORY_SUCCESS =
  'get_buyer_download_history_success';
export const GET_BUYER_DOWNLOAD_HISTORY_FAIL =
  'get_buyer_download_history_fail';
export const EMPTY_DOWNLOAD_HISTORY = 'empty_download_history';

export const GET_ORDER_DETAILS = 'get_order_details';
export const GET_ORDER_DETAILS_SUCCESS = 'get_order_details_success';
export const GET_ORDER_DETAILS_FAIL = 'get_order_details_fail';

export const GET_BRANDS = 'get_brands';
export const GET_BRANDS_SUCCESS = 'get_brands_success';
export const GET_BRANDS_FAIL = 'get_brands_fail';

export const GET_PROCESSING_TIME = 'get_processing_time';
export const GET_PROCESSING_TIME_SUCCESS = 'get_processing_time_success';
export const GET_PROCESSING_TIME_FAIL = 'get_processing_time_fail';

export const GET_SHIPS_TO_COUNTRIES = 'get_ships_to_countries';
export const GET_SHIPS_TO_COUNTRIES_SUCCESS = 'get_ships_to_countries_success';
export const GET_SHIPS_TO_COUNTRIES_FAIL = 'get_ships_to_countries_fail';

export const GET_SHIPPING_COMPANY = 'get_shipping_company';
export const GET_SHIPPING_COMPANY_SUCCESS = 'get_shipping_company_success';
export const GET_SHIPPING_COMPANY_FAIL = 'get_shipping_company_fail';

export const GET_CARRIER_SERVICE = 'get_carrier_service';
export const GET_CARRIER_SERVICE_SUCCESS = 'get_carrier_service_success';
export const GET_CARRIER_SERVICE_FAIL = 'get_carrier_service_fail';

export const SAVE_PRODUCT = 'save_product';
export const SAVE_PRODUCT_SUCCESS = 'save_product_success';
export const SAVE_PRODUCT_FAIL = 'save_product_fail';

export const UPLOAD_IMAGES_GENERAL = 'upload_images_general';
export const UPLOAD_IMAGES_GENERAL_SUCCESS = 'upload_images_general_success';
export const UPLOAD_IMAGES_GENERAL_FAIL = 'upload_images_general_fail';

export const DELETE_IMAGE_GENERAL = 'delete_image_general';
export const DELETE_IMAGE_GENERAL_SUCCESS = 'delete_image_general_success';
export const DELETE_IMAGE_GENERAL_FAIL = 'delete_image_general_fail';

export const REMOVE_GENERAL_IMAGES = 'remove_general_images';
export const EMPTY_PRODUCT_DATA = 'empty_product_data';

export const GET_SPECIFICATIONS = 'get_specifications';
export const GET_SPECIFICATIONS_SUCCESS = 'get_specifications_success';
export const GET_SPECIFICATIONS_FAIL = 'get_specifications_fail';

export const GET_PRODUCT_FILTERS = 'get_product_filters';
export const GET_PRODUCT_FILTERS_SUCCESS = 'get_product_filters_success';
export const GET_PRODUCT_FILTERS_FAIL = 'get_product_filters_fail';

export const GET_RELATED_PRODUCTS = 'get_related_products';
export const GET_RELATED_PRODUCTS_SUCCESS = 'get_related_products_success';
export const GET_RELATED_PRODUCTS_FAIL = 'get_related_products_fail';

export const CREATE_OPTION = 'create_option';
export const CREATE_OPTION_SUCCESS = 'create_option_success';
export const CREATE_OPTION_FAIL = 'create_option_fail';

export const LOAD_OPTIONS = 'load_options';
export const LOAD_OPTIONS_SUCCESS = 'load_options_success';
export const LOAD_OPTIONS_FAIL = 'load_options_fail';

export const LOAD_COMPATIBILITY = 'load_compatibility';
export const LOAD_COMPATIBILITY_SUCCESS = 'load_compatibility_success';
export const LOAD_COMPATIBILITY_FAIL = 'load_compatibility_fail';

export const LOAD_COMPATIBILITY_CONTENT = 'load_compatibility_content';
export const LOAD_COMPATIBILITY_CONTENT_SUCCESS =
  'load_compatibility_content_success';
export const LOAD_COMPATIBILITY_CONTENT_FAIL =
  'load_compatibility_content_fail';

export const LOAD_COMPATIBILITY_CONTENT_DROPDOWN =
  'load_compatibility_content_dropdown';
export const LOAD_COMPATIBILITY_CONTENT_DROPDOWN_SUCCESS =
  'load_compatibility_content_dropdown_success';
export const LOAD_COMPATIBILITY_CONTENT_DROPDOWN_FAIL =
  'load_compatibility_content_dropdown_fail';

export const GET_RECENTLY_VIEWED = 'get_recently_viewed';
export const GET_RECENTLY_VIEWED_SUCCESS = 'get_recently_viewed_success';
export const GET_RECENTLY_VIEWED_FAIL = 'get_recently_viewed_fail';

export const SHOW_SEARCH_FIELD = 'show_search_field';

export const SEARCH_RESULTS = 'search_results';
export const SEARCH_RESULTS_SUCCESS = 'search_results_success';
export const SEARCH_RESULTS_FAIL = 'search_results_fail';

export const CLEAR_HOME_PRODUCTS = 'clear_home_products';

export const SOCIAL_LOGIN = 'social_login';
export const SOCIAL_LOGIN_SUCCESS = 'social_login_success';
export const SOCIAL_LOGIN_FAIL = 'social_login_fail';

export const GOOGLE_LOGIN = 'google_login';
export const GOOGLE_LOGIN_SUCCESS = 'google_login_success';
export const GOOGLE_LOGIN_FAIL = 'google_login_fail';

export const FETCH_WALLET_DETAILS = 'fetch_wallet_details';
export const FETCH_WALLET_DETAILS_SUCCESS = 'fetch_wallet_details_success';
export const FETCH_WALLET_DETAILS_FAIL = 'fetch_wallet_details_fail';
export const EMPTY_WALLET_DETAILS = 'empty_wallet_details';

export const FETCH_MESSAGES = 'fetch_messages';
export const FETCH_MESSAGES_SUCCESS = 'fetch_messages_success';
export const FETCH_MESSAGES_FAIL = 'fetch_messages_fail';
export const CLEAR_MESSAGES = 'clear_messages';

export const APPLY_COUPON_CART = 'apply_coupon_cart';
export const APPLY_COUPON_CART_SUCCESS = 'apply_coupon_cart_success';
export const APPLY_COUPON_CART_FAIL = 'apply_coupon_cart_fail';

export const REMOVE_COUPON_CART = 'remove_coupon_cart';
export const REMOVE_COUPON_CART_SUCCESS = 'remove_coupon_cart_success';
export const REMOVE_COUPON_CART_FAIL = 'remove_coupon_cart_fail';

export const APPLY_REWARD_POINT = 'apply_reward_point';
export const APPLY_REWARD_POINT_SUCCESS = 'apply_reward_point_success';
export const APPLY_REWARD_POINT_FAIL = 'apply_reward_point_fail';

export const GET_ADDRESS_PAYMENT = 'get_address_payment';
export const GET_ADDRESS_PAYMENT_SUCCESS = 'get_address_payment_success';
export const GET_ADDRESS_PAYMENT_FAIL = 'get_address_payment_fail';

export const GET_PRODUCT_FEE = 'GET_PRODUCT_FEE';
export const GET_PRODUCT_FEE_SUCCESS = 'GET_PRODUCT_FEE_SUCCESS';
export const GET_PRODUCT_FEE_FAIL = 'GET_PRODUCT_FEE_FAIL';

export const SELECT_ADDRESS_PAYMENT = 'select_address_payment';
export const SELECT_ADDRESS_PAYMENT_SUCCESS = 'select_address_payment_success';
export const SELECT_ADDRESS_PAYMENT_FAIL = 'select_address_payment_fail';

export const EDIT_PRODUCT_VIEW = 'edit_product_view';
export const EDIT_PRODUCT_VIEW_SUCCESS = 'edit_product_view_success';
export const EDIT_PRODUCT_VIEW_FAIL = 'edit_product_view_fail';

export const GET_HOME_PRODUCTS_FAIL_NO_INTERNET =
  'get_home_products_fail_no_internet';

export const SET_MAIN_IMAGE = 'set_main_image';

export const NOTIFICATIONS = 'notifications';
export const UNREAD_NOTIFICATIONS = 'unread_notifications';
export const EMPTY_NOTIFICATIONS = 'empty_notifications';

export const UNREAD_MESSAGES = 'unread_messages';

export const GET_PROVINCE = 'GET_PROVINCE';
export const GET_PROVINCE_SUCCESS = 'GET_PROVINCE_SUCCESS';
export const GET_PROVINCE_FAIL = 'GET_PROVINCE_FAIL';
export const GET_PROVINCE_SELECTED = 'GET_PROVINCE_SELECTED';

export const GET_CITY = 'GET_CITY';
export const GET_CITY_SUCCESS = 'GET_CITY_SUCCESS';
export const GET_CITY_FAIL = 'GET_CITY_FAIL';
export const GET_CITY_SELECTED = 'GET_CITY_SELECTED';

export const GET_DISTRICT = 'GET_DISTRICT';
export const GET_DISTRICT_SUCCESS = 'GET_DISTRICT_SUCCESS';
export const GET_DISTRICT_FAIL = 'GET_DISTRICT_FAIL';
export const GET_DISTRICT_SELECTED = 'GET_DISTRICT_SELECTED';

export const GET_COUNTRY = 'GET_COUNTRY';
export const GET_COUNTRY_SUCCESS = 'GET_COUNTRY_SUCCESS';
export const GET_COUNTRY_FAIL = 'GET_COUNTRY_FAIL';
export const GET_COUNTRY_SELECTED = 'GET_COUNTRY_SELECTED';

export const SET_COMPONENT_REDIRECT = 'SET_COMPONENT_REDIRECT';

export const UPDATE_STATUS_USER = 'UPDATE_STATUS_USER';

export const GET_SALES_LIST = 'GET_SALES_LIST';

export const SHOP_STATE_NAME = 'SHOP_STATE_NAME';
export const SHOP_DISTRICT_ID = 'SHOP_DISTRICT_ID';
export const SHOP_DISTRICT_NAME = 'SHOP_DISTRICT_NAME';
export const SHOP_CITY_ID = 'SHOP_CITY_ID';

export const SET_SHIPPING_OPTIONS = 'SET_SHIPPING_OPTIONS';
export const SHIPPING_COMPANY_SELECTED = 'SHIPPING_COMPANY_SELECTED';
export const SHIPPING_SUMMARY = 'SHIPPING_SUMMARY';
export const SET_SHIPPING_COMPANY = 'SET_SHIPPING_COMPANY';
export const SET_SELECTED_SHIPPING_OPTIONS = 'SET_SELECTED_SHIPPING_OPTIONS';
export const SET_COST = 'SET_COST';
export const TOTAL_PRICE = 'TOTAL_PRICE';
export const TOTAL_TAX = 'TOTAL_TAX';
export const SALES_TAX = 'SALES_TAX';
export const DELIVERY_CHARGE = 'DELIVERY_CHARGE';
export const DATA_TO_SEND = 'DATA_TO_SEND';
export const SET_SELECTED_SHIPPING_OPTION_SERVICE =
  'SET_SELECTED_SHIPPING_OPTION_SERVICE';
