import Axios from 'axios';

export const handleSelectShippingMethod = (details) => (dispatch) => {
  var formBody = [];
  for (var property in details) {
    if (property == 'shipping_locations') {
      let array = [];
      for (var i = 0; i < details[property].length; i++) {
        array.push(details[property][i]);
      }
      formBody.push(property + '=' + JSON.stringify(array));
    } else {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
  }
  formBody = formBody.join('&');
  return new Promise((resolve, reject) => {
    return Axios.post('/saveShippingAddressDetailsAPI', formBody)
      .then((response) => {
        console.log(response);
        resolve(response.data);
      })
      .catch((err) => reject(err.response));
  });
};
