import React, {Component} from 'react';
import {View} from 'react-native';
import {WebView} from 'react-native-webview';

class PaymentRedirect extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <WebView
          source={{uri: this.props.route.params.url}}
          style={{flex: 1}}
          tar
          onNavigationStateChange={(e) => {
            console.log(e);
            if (e.title === 'https://www.syoobe.co.id/custom/payment_success') {
              this.props.navigation.replace('HomeDrawer', {
                screen: this.props.route.params.from_wallet
                  ? 'MyWallet'
                  : 'MyOrderList',
              });
            }
          }}
          // on
        />
      </View>
    );
  }
}

export default PaymentRedirect;
