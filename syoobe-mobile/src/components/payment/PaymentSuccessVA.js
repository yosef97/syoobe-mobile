import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, Alert} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Button} from '../common';
import {formatRupiah} from '../../helpers/helper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Clipboard from '@react-native-community/clipboard';

class PaymentSuccessVA extends Component {
  componentDidMount() {
    console.log('params', this.props.route.params);
  }
  _setContent = (text) => {
    // alert(text);
    Clipboard.setString(text);
    Alert.alert('Berhasil disalin ke clipboard');
  };
  render() {
    const {type, from_wallet} = this.props.route.params;
    const paymentType1 = type === 'bank_transfer';
    const paymentType2 = type === 'echannel';
    const paymentType3 = type === 'cstore';

    // console.log(this.props.route.params);
    return (
      <View style={{flex: 1, backgroundColor: '#f5f6fa'}}>
        <View style={styles.itemContainer}>
          <Text style={{fontSize: hp(2.2), fontWeight: 'bold'}}>
            Metode Pembayaran
          </Text>
          <View style={styles.itemContent}>
            <Text style={{fontSize: hp(2), paddingTop: 5}}>
              {this.props.route.params.title}
            </Text>
            <Image
              resizeMode="contain"
              source={this.props.route.params.logo}
              style={{width: wp(15), height: hp(3)}}
            />
          </View>
        </View>

        {paymentType1 && (
          <View style={styles.itemContainer}>
            <Text style={{fontSize: hp(2.2), fontWeight: 'bold'}}>
              Nomor Rekening VA
            </Text>
            <View style={styles.itemContent}>
              <Text
                style={{
                  fontSize: hp(2),
                  paddingTop: 5,
                }}>
                {this.props.route.params.va_number}
              </Text>
              <MaterialIcons
                name={'content-copy'}
                size={hp(2.5)}
                onPress={() =>
                  this._setContent(this.props.route.params.va_number)
                }
              />
            </View>
          </View>
        )}

        {paymentType2 && (
          <>
            <View style={styles.itemContainer}>
              <Text style={{fontSize: hp(2.2), fontWeight: 'bold'}}>
                Kode Perusahaan
              </Text>
              <View style={styles.itemContent}>
                <Text
                  style={{
                    fontSize: hp(2),
                    paddingTop: 5,
                  }}>
                  {this.props.route.params.biller_code}
                </Text>
                <MaterialIcons
                  name={'content-copy'}
                  size={hp(2.5)}
                  onPress={() =>
                    this._setContent(this.props.route.params.biller_code)
                  }
                />
              </View>
            </View>
            <View style={styles.itemContainer}>
              <Text style={{fontSize: hp(2.2), fontWeight: 'bold'}}>
                Kode Bayar
              </Text>
              <View style={styles.itemContent}>
                <Text
                  style={{
                    fontSize: hp(2),
                    paddingTop: 5,
                  }}>
                  {this.props.route.params.bill_key}
                </Text>
                <MaterialIcons
                  name={'content-copy'}
                  size={hp(2.5)}
                  onPress={() =>
                    this._setContent(this.props.route.params.bill_key)
                  }
                />
              </View>
            </View>
          </>
        )}

        {paymentType3 && (
          <View style={styles.itemContainer}>
            <Text style={{fontSize: hp(2.2), fontWeight: 'bold'}}>
              Kode Bayar
            </Text>
            <View style={styles.itemContent}>
              <Text
                style={{
                  fontSize: hp(2),
                  paddingTop: 5,
                }}>
                {this.props.route.params.payment_code}
              </Text>
              <MaterialIcons
                name={'content-copy'}
                size={hp(2.5)}
                onPress={() =>
                  this._setContent(this.props.route.params.payment_code)
                }
              />
            </View>
          </View>
        )}

        <View style={styles.itemContainer}>
          <Text style={{fontSize: hp(2.2), fontWeight: 'bold'}}>
            Total Bayar
          </Text>
          <View style={styles.itemContent}>
            <Text
              style={{
                fontSize: hp(2),
                paddingTop: 5,
                color: 'red',
                fontWeight: 'bold',
              }}>
              Rp. {formatRupiah(this.props.route.params.total)}
            </Text>
            <MaterialIcons
              name={'content-copy'}
              size={hp(2.5)}
              onPress={() => this._setContent(this.props.route.params.total)}
            />
          </View>
        </View>
        <View style={{paddingHorizontal: wp(3), marginTop: hp(2)}}>
          <Button
            onPress={() =>
              this.props.navigation.replace('HomeDrawer', {screen: 'Home'})
            }>
            Kembali Ke Beranda
          </Button>
        </View>
        <View style={{paddingHorizontal: wp(3), marginTop: hp(2)}}>
          <Button
            outline
            onPress={() =>
              this.props.navigation.replace('HomeDrawer', {
                screen: from_wallet ? 'MyWallet' : 'MyOrderList',
              })
            }>
            Lihat Pesanan
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    backgroundColor: '#fff',
    paddingVertical: hp(2),
    paddingHorizontal: wp(3),
    marginTop: hp(2),
    marginHorizontal: wp(3),
    borderRadius: 10,
  },
  itemContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default PaymentSuccessVA;
