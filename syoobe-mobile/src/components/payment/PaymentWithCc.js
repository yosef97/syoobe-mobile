import {formatCc, formatRupiah} from '../../helpers/helper';
import React, {Component} from 'react';
import {Text, TextInput, View, Alert} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Button} from '../common/Button';
import {env} from '../../../appconfig';
import Axios from 'axios';
import {connect} from 'react-redux';
import {clearCartApi} from '../../actions';
import {Spinner} from '../common';
import Bugsnag from '@bugsnag/react-native';

class PaymentWithCc extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // card_name: '',
      card_number: '',
      card_exp_month: '',
      card_exp_year: '',
      card_cvv: '',
      visible: false,
      data: [],
      loading: false,
    };
  }

  setVisible = () => {
    this.setState((state) => ({
      visible: !state.visible,
    }));
  };

  componentDidMount() {
    const {product_data} = this.props.route.params;
    let newData = [];
    product_data.map((item) => {
      let obj = {
        id: item.id,
        price: parseInt(this.props.route.params.payable_amount),
        quantity: 1,
        name: item.product_name,
        brand: '',
        category: '',
        merchant_name: item.store_name,
      };
      newData.push(obj);
    });
    this.setState({data: newData});
  }

  handleGetTokenCc = () => {
    const {card_number, card_cvv, card_exp_month, card_exp_year} = this.state;
    if (!card_number) {
      Alert.alert('Nomor kartu tidak boleh kosong');
    } else if (!card_exp_month) {
      Alert.alert('Bulan kedaluarsa tidak boleh kosong');
    } else if (!card_exp_year) {
      Alert.alert('Tahun kedaluarsa tidak boleh kosong');
    } else if (!card_cvv) {
      Alert.alert('CVV tidak boleh kosong');
    } else {
      var requestOptions = {
        method: 'GET',
        headers: {
          Authorization: env.MIDTRANS.authorization,
        },
        // redirect: 'follow'
      };
      this.setState((state) => ({loading: !state.loading}));
      fetch(
        `https://api.sandbox.midtrans.com/v2/token?client_key=${env.MIDTRANS.client_key}&card_number=${card_number}&card_exp_month=${card_exp_month}&card_exp_year=${card_exp_year}&card_cvv=${card_cvv}`,
        requestOptions,
      )
        .then((response) => response.json())
        .then((result) => {
          if (result.status_code == 200) {
            this.proccessTransaction(result.token_id);
          } else {
            this.setState((state) => ({loading: !state.loading}));
            Alert.alert('Kartu yang kamu masukkan tidak valid');
          }
        })
        .catch((error) => {
          Bugsnag.notify(error);
          console.log('error', error);
        });
    }
  };

  proccessTransaction = async (token) => {
    const {
      order_id,
      custom_field1,
      total_amount,
    } = this.props.route.params.data;
    let path = 'notification_url_cc_midtrans';
    if (this.props.route.params.product_type == 'W') {
      path = 'notification_postage_midtrans';
    }
    let data = {
      payment_type: 'credit_card',
      transaction_details: {
        order_id,
        custom_field1,
        gross_amount: formatRupiah(total_amount, false),
      },
      credit_card: {
        token_id: token,
        authentication: false,
      },
    };

    let requestOptions = {
      method: 'POST',
      headers: {
        Authorization: `Basic ${env.MIDTRANS.authorization}`,
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'X-Override-Notification': `https://www.syoobe.co.id/cart/${path}`,
      },
      body: JSON.stringify(data),
      // redirect: 'follow'
    };
    await fetch(env.MIDTRANS.payment_url, requestOptions)
      .then((res) => res.json())
      .then((result) => {
        this.handleSave(result, path);
        // Linking.openURL(result.redirect_url);
      })
      .catch((err) => {
        Bugsnag.notify(err);
        console.log(err);
      });
  };

  handleSave = async (data, path) => {
    await Axios.post(`https://syoobe.co.id/cart/${path}`, data)
      // .then((res) => res.json())
      .then(async (result) => {
        console.log(data);
        // this.props.navigation.navigate('ThankYou',{})
        await this.props.clearCartApi(this.props.route.params._token);
        this.setState((state) => ({loading: !state.loading}));
        if (data.status_code == 200) {
          if (data.redirect_url) {
            this.props.navigation.replace('PaymentRedirect', {
              url: data.redirect_url,
            });
          } else {
            this.props.navigation.replace('ThankYou', {
              product_type: this.props.route.params.product_type,
              from_wallet: this.props.route.params.from_wallet,
              order_id: data.order_id.split('-')[1],
            });
          }
        } else {
          this.props.navigation.navigate('PaymentFailed', {});
        }

        // console.log('handleSave', result);
      })
      .catch((err) => {
        Bugsnag.notify(err);
        console.log(err);
      });
  };

  render() {
    const {
      // card_name,
      card_number,
      card_cvv,
      card_exp_month,
      card_exp_year,
    } = this.state;

    const {total_amount} = this.props.route.params.data;

    if (this.state.loading) {
      return <Spinner color="red" size="large" />;
    }
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            paddingHorizontal: wp(3),
            paddingVertical: hp(2),
            backgroundColor: '#fff',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: hp(2),
            marginHorizontal: wp(2),
            borderRadius: 5,
          }}>
          <Text style={{fontSize: hp(2.3)}}>Total Bayar</Text>
          <Text style={{fontSize: hp(3), fontWeight: 'bold', color: 'red'}}>
            Rp. {total_amount}
          </Text>
        </View>
        {/* Card Detail */}
        <View
          style={{
            // paddingHorizontal: wp(3),
            marginTop: hp(2),
            marginHorizontal: wp(2),
            borderRadius: 5,
            backgroundColor: '#fff',
          }}>
          <View style={{paddingVertical: hp(1.5), paddingHorizontal: wp(3)}}>
            <Text style={{fontSize: hp(2.3), fontWeight: 'bold'}}>
              Tambahkan Kartu Debit/Kredit
            </Text>
            <View style={{marginTop: hp(2)}}>
              <Text style={{paddingBottom: 5}}>Nomor Kartu</Text>
              <TextInput
                style={{
                  backgroundColor: '#f1f2f6',
                  borderRadius: 5,
                  paddingHorizontal: wp(3),
                  marginBottom: hp(2),
                }}
                onChangeText={(e) => this.setState({card_number: e})}
                value={formatCc(card_number)}
                keyboardType={'numeric'}
                placeholder={'4811 1111 1111 1114'}
              />
              <View
                style={{
                  // flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View style={{width: '48%'}}>
                  <Text style={{paddingBottom: 5}}>Masa Berlaku</Text>
                  <View
                    style={{
                      // flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <TextInput
                      style={{
                        width: '75%',
                        backgroundColor: '#f1f2f6',
                        borderRadius: 5,
                        paddingHorizontal: wp(3),
                        marginRight: wp(2),
                      }}
                      onChangeText={(e) =>
                        this.setState({
                          card_exp_month: e,
                        })
                      }
                      value={card_exp_month}
                      placeholder={'bulan'}
                      keyboardType={'numeric'}
                    />
                    <TextInput
                      style={{
                        width: '75%',
                        backgroundColor: '#f1f2f6',
                        borderRadius: 5,
                        paddingHorizontal: wp(3),
                      }}
                      onChangeText={(e) => this.setState({card_exp_year: e})}
                      value={card_exp_year}
                      placeholder={'tahun'}
                      keyboardType={'numeric'}
                    />
                  </View>
                </View>
                <View style={{width: '20%'}}>
                  <Text style={{paddingBottom: 5}}>CVV</Text>
                  <TextInput
                    style={{
                      backgroundColor: '#f1f2f6',
                      borderRadius: 5,
                      paddingHorizontal: wp(3),
                      // marginLeft: 15,
                    }}
                    onChangeText={(e) => this.setState({card_cvv: e})}
                    value={card_cvv}
                    keyboardType={'numeric'}
                    placeholder={'123'}
                  />
                </View>
              </View>
              <View style={{marginTop: hp(2)}}>
                <Button onPress={this.handleGetTokenCc}>Bayar Sekarang</Button>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default connect(null, {clearCartApi})(PaymentWithCc);
