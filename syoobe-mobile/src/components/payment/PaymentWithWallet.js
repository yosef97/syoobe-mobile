import React, {Component} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Button} from '../common/Button';
import Axios from 'axios';
import {connect} from 'react-redux';
import {clearCartApi} from '../../actions';
import {Spinner} from '../common';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {formatRupiah} from '../../helpers/helper';

class PaymentWithWallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
    };
  }

  componentDidMount() {
    const {product_data} = this.props.route.params;
    let newData = [];
    product_data.map((item) => {
      let obj = {
        id: item.id,
        price: parseInt(this.props.route.params.payable_amount),
        quantity: 1,
        name: item.product_name,
        brand: '',
        category: '',
        merchant_name: item.store_name,
      };
      newData.push(obj);
    });
    this.setState({data: newData});
  }

  payWithWallet = () => {
    const {data, _token} = this.props.route.params;
    const {cart_actual_paid} = this.props.route.params.data.wallet_payment;
    const {order_id} = data;
    this.setState({
      changeLoading: true,
    });
    var details = {
      _token,
      order_id: order_id.split('-')[1],
      cart_actual_paid,
    };
    console.log('ini detail', details);
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    Axios.post('https://syoobe.co.id/api/paymentUsingWallet', formBody).then(
      (response) => {
        console.log('ini wallet 1232', JSON.stringify(response));
        this.setState({
          changeLoading: false,
        });
        this.finalTask(response);
      },
    );
  };

  finalTask = async (response) => {
    if (response.data.status === 1) {
      await this.props.clearCartApi(this.props.route.params._token);
      if (response.data.order_product_data) {
        this.props.navigation.replace('ThankYou', {
          product_type: this.props.route.params.product_type,
          order_id: this.props.route.params.data.order_id.split('-')[1],
          wallet: true,
        });
      }
    } else {
      this.props.navigation.replace('PaymentFailed', {});
    }
  };

  render() {
    const {image, title} = this.props.route.params;
    const {wallet_payment} = this.props.route.params.data;

    // console.log('wallet_payment', JSON.stringify(this.props));

    if (this.state.loading) {
      return <Spinner color="red" size="large" />;
    }
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
          <View
            style={{
              paddingHorizontal: wp(3),
              paddingVertical: hp(2),
              backgroundColor: '#DFEFD8',
              flexDirection: 'column',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: hp(2),
              marginHorizontal: wp(2),
              borderRadius: 5,
            }}>
            <Text style={{fontSize: hp(2.3)}}>
              {' '}
              Selamat karena anda menggunakan "Saldo Saya" sebagai metode
              pembayaran, anda telah berhemat biaya transaksi bank sebesar Rp.{' '}
              {formatRupiah(wallet_payment?.midtrans_commission)}
            </Text>
          </View>
          <View
            style={{
              paddingHorizontal: wp(3),
              paddingVertical: hp(2),
              backgroundColor: '#fff',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: hp(2),
              marginHorizontal: wp(2),
              borderRadius: 5,
            }}>
            <Text style={{fontSize: hp(2.3)}}>Total Bayar</Text>
            <Text style={{fontSize: hp(3), fontWeight: 'bold', color: 'red'}}>
              Rp. {formatRupiah(parseInt(wallet_payment?.cart_actual_paid))}
            </Text>
          </View>

          <View
            style={{
              // paddingHorizontal: wp(3),
              marginTop: hp(2),
              marginHorizontal: wp(2),
              borderRadius: 5,
              backgroundColor: '#fff',
            }}>
            <View style={{paddingVertical: hp(1.5), paddingHorizontal: wp(3)}}>
              <Text style={{fontSize: hp(2.3), fontWeight: 'bold'}}>
                Metode Pembayaran
              </Text>
              <View style={[styles.cardItemList]}>
                <View
                  style={{
                    width: hp(7),
                    height: hp(5),
                    maxWidth: hp(8),
                    maxHeight: hp(5),
                  }}>
                  <Image
                    resizeMode="contain"
                    style={{width: '100%', height: '100%'}}
                    source={image}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    paddingLeft: wp(5),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                    {title}
                  </Text>
                  {/* <Text>{subtitle}</Text> */}
                  <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
                </View>
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            // paddingHorizontal: wp(3),
            // marginTop: hp(2),
            paddingHorizontal: wp(2),
            borderRadius: 5,
            paddingVertical: hp(1),
            backgroundColor: '#fff',
          }}>
          <Button onPress={this.payWithWallet}>Bayar Sekarang</Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    // flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
    // alignItems:'c'
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  cardItemList: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingVertical: hp(1),
    paddingHorizontal: wp(2),
    marginTop: hp(1),
    borderRadius: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),
    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});

export default connect(null, {clearCartApi})(PaymentWithWallet);
