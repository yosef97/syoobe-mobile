import {formatRupiah} from '../../helpers/helper';
import React, {Component} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Button} from '../common/Button';
import {env} from '../../../appconfig';
import Axios from 'axios';
import {connect} from 'react-redux';
import {clearCartApi} from '../../actions';
import {Spinner} from '../common';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {Switch} from 'react-native-paper';
import Bugsnag from '@bugsnag/react-native';

class PaymentWithCsStore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      isSwitchOn: false,
    };
  }

  componentDidMount() {
    const {product_data} = this.props.route.params;
    let newData = [];

    product_data.map((item) => {
      let obj = {
        id: item.id,
        price: parseInt(this.props.route.params.payable_amount),
        quantity: 1,
        name: item.product_name,
        brand: '',
        category: '',
        merchant_name: item.store_name,
      };
      newData.push(obj);
    });
    this.setState({data: newData});
  }

  proccessTransaction = async (token) => {
    this.setState((state) => ({loading: !state.loading}));
    const {
      order_id,
      custom_field1,
      total_amount,
    } = this.props.route.params.data;
    const {isSwitchOn} = this.state;

    const {payable_amount, balance} = this.props.route.params;
    let amount = payable_amount
      ? payable_amount
      : formatRupiah(total_amount, false);
    let path = 'notification_url_cc_midtrans';
    if (this.props.route.params.product_type == 'W') {
      path = 'notification_postage_midtrans';
    }
    let data = {
      payment_type: this.props.route.params.payment_type,
      // "bank_transfer":{
      //   "bank": "bca",
      //   "va_number": "12345678901"},
      transaction_details: {
        order_id,
        custom_field1,
        gross_amount: formatRupiah(
          isSwitchOn ? amount - balance : amount,
          false,
        ),
      },

      // item_details: this.state.data,
      ...this.props.route.params.bank,
    };

    let requestOptions = {
      method: 'POST',
      headers: {
        Authorization: `Basic ${env.MIDTRANS.authorization}`,
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'X-Override-Notification': `https://www.syoobe.co.id/cart/${path}`,
      },
      body: JSON.stringify(data),
      // redirect: 'follow'
    };
    await fetch(env.MIDTRANS.payment_url, requestOptions)
      .then((res) => res.json())
      .then((result) => {
        this.handleSave(result, path);
        // Linking.openURL(result.redirect_url);
      })
      .catch((err) => {
        Bugsnag.notify(err);
        console.log(err);
      });
  };

  handleSave = async (data, path) => {
    await Axios.post(`https://syoobe.co.id/cart/${path}`, data)
      .then(async (result) => {
        console.log(data);
        // this.props.navigation.navigate('ThankYou',{})
        await this.props.clearCartApi(this.props.route.params._token);
        if (this.state.isSwitchOn) {
          await this.chargeWallet(this.props.route.params._token);
        }
        this.setState((state) => ({loading: !state.loading}));
        if (data.status_code == 201) {
          let params = {
            payment_code: data.payment_code,
            total: data.gross_amount,
            logo: this.props.route.params.image,
            title: this.props.route.params.title,
            type: data.payment_type,
            product_type: this.props.route.params.product_type,
            from_wallet: this.props.route.params.from_wallet,
            order_id: data.order_id.split('-')[1],
          };
          this.props.navigation.replace('PaymentSuccessVA', params);
        } else {
          this.props.navigation.replace('PaymentFailed', {});
        }
      })
      .catch((err) => {
        Bugsnag.notify(err);
        console.log(err);
      });
  };

  chargeWallet = (_token) => {
    const {balance, data} = this.props.route.params;
    const {order_id} = data;
    var details = {
      _token,
      order_id: order_id.split('-')[1],
      balance,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    Axios.post('https://syoobe.co.id/api/chargeWallet', formBody).then(
      (response) => {
        console.log('wallet', response);
        this.setState({
          changeLoading: false,
        });
        // this.finalTask(response);
      },
    );
  };

  onToggleSwitch = () => {
    let newData = [...this.state.data];
    const {balance} = this.props.route.params;
    // console.log(balance);
    let myBalance = {
      id: 'c-3',
      price: balance - balance * 2,
      quantity: 1,
      name: 'Saldo',
      brand: '',
      category: '',
      merchant_name: '',
    };
    this.setState({isSwitchOn: !this.state.isSwitchOn});
    if (!this.state.isSwitchOn) {
      newData.push(myBalance);
      this.setState({data: newData});
    } else {
      let data = newData.filter((val) => val.id !== 'c-3');
      this.setState({data});
    }
  };

  render() {
    const {
      data,
      image,
      title,
      isWallet,
      payable_amount,
    } = this.props.route.params;
    const {isSwitchOn} = this.state;
    const balance = parseInt(this.props.route.params.balance);

    let switchOn = parseInt(amount - balance);

    if (this.state.loading) {
      return <Spinner color="red" size="large" />;
    }

    let amount = payable_amount
      ? payable_amount
      : formatRupiah(data.total_amount, false);
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
          <View
            style={{
              paddingHorizontal: wp(3),
              paddingVertical: hp(2),
              backgroundColor: '#fff',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: hp(2),
              marginHorizontal: wp(2),
              borderRadius: 5,
            }}>
            <Text style={{fontSize: hp(2.3)}}>Total Bayar</Text>
            <Text style={{fontSize: hp(3), fontWeight: 'bold', color: 'red'}}>
              Rp. {formatRupiah(isSwitchOn ? switchOn : amount)}
            </Text>
          </View>
          {/* Card Detail */}
          <View
            style={{
              // paddingHorizontal: wp(3),
              marginTop: hp(2),
              marginHorizontal: wp(2),
              borderRadius: 5,
              backgroundColor: '#fff',
            }}>
            <View style={{paddingVertical: hp(1.5), paddingHorizontal: wp(3)}}>
              <Text style={{fontSize: hp(2.3), fontWeight: 'bold'}}>
                Metode Pembayaran
              </Text>
              <View style={[styles.cardItemList]}>
                <View
                  style={{
                    width: hp(7),
                    height: hp(5),
                    maxWidth: hp(8),
                    maxHeight: hp(5),
                  }}>
                  <Image
                    resizeMode="contain"
                    style={{width: '100%', height: '100%'}}
                    source={image}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    paddingLeft: wp(5),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                    {title}
                  </Text>
                  {/* <Text>{subtitle}</Text> */}
                  <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
                </View>
              </View>
            </View>
          </View>
          {!isWallet && balance > 0 && balance < amount && (
            <View
              style={{
                paddingVertical: hp(1.5),
                paddingHorizontal: wp(2),
              }}>
              <View
                style={{
                  ...styles.cardItemList,
                  borderRadius: 5,
                  paddingHorizontal: wp(4),
                }}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View>
                    <Text style={{fontSize: hp(2.2), fontWeight: 'bold'}}>
                      Gunakan Saldo
                    </Text>
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontWeight: 'bold',
                        color: 'red',
                      }}>
                      {`Rp. ${formatRupiah(balance)}`}
                    </Text>
                  </View>
                  <Switch
                    value={isSwitchOn}
                    onValueChange={this.onToggleSwitch}
                  />
                </View>
              </View>
            </View>
          )}
        </View>
        <View
          style={{
            // paddingHorizontal: wp(3),
            // marginTop: hp(2),
            paddingHorizontal: wp(2),
            borderRadius: 5,
            paddingVertical: hp(1),
            backgroundColor: '#fff',
          }}>
          <Button onPress={this.proccessTransaction}>Bayar Sekarang</Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    // flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
    // alignItems:'c'
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  cardItemList: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingVertical: hp(1),
    paddingHorizontal: wp(2),
    marginTop: hp(1),
    borderRadius: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),
    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});

export default connect(null, {clearCartApi})(PaymentWithCsStore);
