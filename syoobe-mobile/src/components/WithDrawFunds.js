import React, {Component} from 'react';
import {StyleSheet, View, ScrollView, Alert} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Spinner} from './common';
import axios from 'axios';
import WalletImage from './WalletImage';
import {getWalletDetails, emptyWalletDetails} from '../actions';
import {connect} from 'react-redux';

class WithDrawFunds extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _token: this.props.route.params._token,
      bankData: null,
      loading: false,
    };
  }

  async componentDidMount() {
    const {_token} = this.state;
    this.setState({
      loading: true,
    });
    let details = {
      _token: _token,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post(
        'https://syoobe.co.id/api/withdrawlRequestFormDataForWallet',
        formBody,
      )
      .then((response) => {
        console.log('bank view', response);
        if (response.data.status === 1) {
          if (response.data.user_details.ub_account_number == null) {
            this.props.navigation.navigate('BankInfo');
            Alert.alert(
              'Silahkan masukkan bank detail untuk proses penarikan dana',
            );
          } else {
            this.setState({
              bankData: response.data,
            });
          }
        } else {
          Alert.alert('Transaksi gagal');
        }
        this.setState({
          loading: false,
        });
      });
  }

  updateLoader = (event) => {
    this.setState({
      loading: event,
    });
  };

  goToBankInfo = () => {
    this.props.navigation.navigate('BankInfo', {fromWallet: true});
  };

  getWallet = async () => {
    await this.props.emptyWalletDetails();
    var details = {
      _token: this.state._token,
      page: 1,
    };
    this.props.getWalletDetails(details);
  };

  saveWithdrawal = (data) => {
    this.setState({
      loading: true,
    });
    this.getWallet();
    setTimeout(() => {
      this.setState({
        loading: false,
      });
      this.props.navigation.replace('HomeDrawer', {screen: 'Home'});
    }, 1000);
  };

  render() {
    // console.log('ini props', JSON.stringify(this.props));
    console.log('ini state', JSON.stringify(this.state));
    const {loading, bankData, _token} = this.state;
    const {bgColor, innerView} = styles;
    return (
      <ScrollView keyboardShouldPersistTaps={'handled'} style={bgColor}>
        {loading ? (
          <View style={innerView}>
            <Spinner size={'large'} color={'red'} />
          </View>
        ) : (
          <View style={innerView}>
            {bankData && (
              <WalletImage
                onWithDrawal={this.saveWithdrawal}
                toBankEdit={this.goToBankInfo}
                updateLoading={this.updateLoader}
                _token={_token}
                balance={this.props.route.params.balance}
                data={{...bankData}}
                navigation={this.props.navigation}
              />
            )}
          </View>
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  innerView: {
    height: hp('87%'),
    alignItems: 'center',
    justifyContent: 'center',
  },
  bgColor: {
    backgroundColor: '#fff',
    padding: 10,
  },
  continueBtn: {
    padding: 20,
    // backgroundColor: '#f1f0f0',
    paddingBottom: 10,
    marginTop: 15,
    paddingLeft: 35,
    paddingRight: 35,
  },
});

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {emptyWalletDetails, getWalletDetails})(
  WithDrawFunds,
);
