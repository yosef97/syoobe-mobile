import React from'react';
import { TextInput, View, Text } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const InputInner2 = ({placeholder, label, value, onChangeText, secureTextEntry }) => {
    const { inputStyle, containerStyle, labelStyle } = styles;

    return (
        <View style={containerStyle}>
            <View style={{flexDirection:'row'}}>
                <Text style={labelStyle}>{label}</Text>
                <Text style={{color: "#f00", fontSize:18, marginTop: 5}}> *</Text>
            </View>
            <TextInput
             secureTextEntry={secureTextEntry}
             autoCorrect={false} 
             placeholder = {placeholder}
             style={inputStyle}
             defaultValue = {value}
             onChangeText={onChangeText}/>
        </View>
    );
};

const styles = {
    inputStyle:{
        color: '#000',
       paddingLeft:20,
        fontSize: 18,
        width:'100%',
        marginTop:0,
        backgroundColor:'#fff',
        // borderRadius:50,
        elevation: 1,
        // height:50,
        height:hp('6%'),
        borderWidth:1,
        borderColor:'#cfcdcd',
    },

    labelStyle: {
        fontSize: 18,
        marginTop:5,
        padding:0,
        margin:0,
        marginLeft:4,
        marginBottom:10
        
    },

    containerStyle: {
        backgroundColor:'#fff',
        // borderRadius:50,
        width:'100%'
    }
}

export { InputInner2 };