import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const ButtonCart = ({children, onPress}) => {

    const { buttonStyle, textStyle } = styles

    return (
        <TouchableOpacity onPress={onPress} style={buttonStyle}>
            <Text style={textStyle}>
            {children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    textStyle: {
        alignSelf: 'center',
        color: '#fff',
      
        fontSize:wp('4%'),
        paddingTop:wp('2%'),
        paddingBottom:wp('2%'),
        // paddingLeft:wp('9%'),
        // paddingRight:wp('9%'),
        
    },

    buttonStyle:{
        
        alignSelf: 'stretch',
        backgroundColor: 'transparent',
        borderRadius:50,
        borderWidth:1,
        borderColor:'#c7c7c7',
        width:wp('30%'),
        justifyContent:'center',
        
        // width:'40%'
        
    }
}

export {ButtonCart};


