import React from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from '../Font';

const Button = (props) => {
  const {
    children,
    onPress,
    color = 'red',
    textColor = '#fff',
    outline,
    style,
  } = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{width: '100%'}}
      activeOpacity={0.5}>
      <View
        style={[
          styles.buttonFooterContainer,

          {
            ...style,
            backgroundColor: outline ? '#fff' : color,
            borderColor: outline ? 'red' : color,
            borderWidth: 2,
          },
        ]}>
        <Text style={{color: outline ? 'red' : textColor, fontSize: hp(2)}}>
          {children}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const FooterButton = (props) => {
  const {
    children,
    onPress,
    color = 'red',
    textColor = '#fff',
    container = true,
    style,
  } = props;
  return (
    <View style={[style, container && styles.footerContainer]}>
      <Button {...props}>{children}</Button>
    </View>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
  },

  buttonStyle: {
    alignSelf: 'stretch',
    backgroundColor: '#2cc0ff',
    borderRadius: 50,
    paddingTop: hp('1.8%'),
    paddingBottom: hp('1.8%'),
    marginBottom: 10,
  },
  footerContainer: {
    backgroundColor: '#fff',
    borderTopWidth: 0.5,
    borderTopColor: '#eaeaea',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: hp(1.5),
    paddingHorizontal: wp(4),
  },
  buttonFooterContainer: {
    paddingVertical: hp(2),
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
};

export {Button, FooterButton};
