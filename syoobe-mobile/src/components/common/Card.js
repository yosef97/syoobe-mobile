import React from 'react';
import {View} from 'react-native';

const Card = (props) => {
  return <View style={styles.containerStyle}>{props.children}</View>;
};

export const styles = {
  containerStyle: {
    // borderWidth: 1,
    // borderRadius : 2,
    // borderColor: '#ddd',
    // borderBottomWidth: 0,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 2},
    // shadowRadius: 2,
    // shadowOpacity: 0.1,
    // elevation: 1,
    marginleft: 5,
    marginRight: 5,
    marginTop: 10,
    height: '100%',
    // backgroundColor: '#f00',
    padding: 15,
    position: 'relative',
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center'
  },
};

export {Card};
