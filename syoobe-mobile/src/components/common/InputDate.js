import React, {useState} from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import moment from 'moment';
import 'moment/locale/id';

export const InputDate = ({onChange, value, title}) => {
  const [visible, setVisible] = useState(false);
  const handleChange = (e, date) => {
    setVisible(false);
    onChange(e, date);
  };
  return (
    <View style={{marginTop: hp(2)}}>
      <TouchableOpacity onPress={() => setVisible(true)} activeOpacity={0.5}>
        <Text style={{paddingBottom: 5}}>{title}</Text>
        <View style={styles.dropdown}>
          <Text style={{color: '#000', fontSize: hp(2)}}>
            {moment(value).format('Do MMMM YYYY')}
          </Text>
          <FontAwesome5Icon name={'calendar'} size={hp(2)} />
        </View>
      </TouchableOpacity>
      {visible && (
        <DateTimePicker
          testID="dateTimePicker"
          value={new Date(value).getTime()}
          is24Hour={true}
          display="default"
          onChange={handleChange}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  dropdown: {
    paddingVertical: hp(1.5),
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingHorizontal: wp(3),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#8395a7',
    borderWidth: 1,
  },
});
