import React, {createRef} from 'react';
import {Text, TouchableHighlight, View, StyleSheet} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Slider from '@react-native-community/slider';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {Button} from '../Button';
import InputText from '../InputText';
import {formatRupiah} from '../../../helpers/helper';

export const ApplyRewardPoin = ({onChange, onPress, value, availablePoint}) => {
  console.log(value, availablePoint);
  // return null;
  const sheetRef = createRef();
  return (
    <View>
      <TouchableHighlight
        underlayColor={'#fff'}
        onPress={() => sheetRef.current.open()}>
        <View
          style={{
            flexDirection: 'row',
            // justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: hp(2),
              fontWeight: 'bold',
              color: 'red',
              paddingRight: 10,
            }}>
            {!isNaN(value) && value > 0
              ? `Rp. ${formatRupiah(value)}`
              : 'Gunakan'}
          </Text>
          <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
        </View>
      </TouchableHighlight>
      <RBSheet
        ref={sheetRef}
        closeOnDragDown={false}
        closeOnPressMask={true}
        height={hp(50)}
        customStyles={card}>
        <View style={styles.container}>
          <View style={styles.cardHeader}>
            <Text style={styles.textHeader}>Poin Hadiah</Text>
            <FontAwesome
              onPress={() => sheetRef.current.close()}
              name={'times'}
              size={hp(3)}
            />
          </View>
          <View style={{flex: 1, marginTop: hp(2), paddingHorizontal: wp(3)}}>
            <InputText
              label={'Jumlah Poin'}
              placeholder={'Jumlah poin '}
              onChangeText={(e) => onChange(e)}
              value={!isNaN(value) && value > 0 ? formatRupiah(value) : 0}
            />
          </View>
          <View style={{flex: 1}}>
            <Slider
              style={{paddingTop: hp(2), width: '100%'}}
              value={!isNaN(value) && value > 0 ? value : 0}
              minimumValue={0}
              maximumValue={availablePoint}
              minimumTrackTintColor="#3498db"
              maximumTrackTintColor="red"
              thumbTintColor="#3498db"
              onValueChange={(val) => onChange(Math.round(val))}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: wp(4),
              }}>
              <Text>Poin Tersedia</Text>
              <Text>{`Rp. ${formatRupiah(availablePoint)}`}</Text>
            </View>
          </View>
          <Button onPress={onPress}>Gunakan</Button>
        </View>
      </RBSheet>
    </View>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 16,
    height: '100%',
    flex: 1,
  },
  cardHeader: {
    marginBottom: 10,
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 0.5,
    paddingBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // paddingTop: 10,
  },
  textHeader: {
    fontSize: hp(2.4),
    fontWeight: 'bold',
  },
});
