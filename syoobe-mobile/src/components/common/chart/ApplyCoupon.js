import React, {createRef} from 'react';
import {Text, TouchableHighlight, View, StyleSheet} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {Button} from '../Button';
import InputText from '../InputText';

export const ApplyCoupon = ({onChange, onPress, value, coupon, onReset}) => {
  // return null;
  const sheetRef = createRef();
  return (
    <View>
      <TouchableHighlight
        underlayColor={'#fff'}
        onPress={() => sheetRef.current.open()}>
        <View
          style={{
            flexDirection: 'row',
            // justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: hp(2),
              fontWeight: 'bold',
              color: 'red',
              paddingRight: 10,
            }}>
            {coupon ? coupon : 'Gunakan'}
          </Text>
          <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
        </View>
      </TouchableHighlight>
      <RBSheet
        ref={sheetRef}
        closeOnDragDown={false}
        closeOnPressMask={true}
        height={hp(50)}
        customStyles={card}>
        <View style={styles.container}>
          <View style={styles.cardHeader}>
            <Text style={styles.textHeader}>Kupon/Promo</Text>
            <FontAwesome
              onPress={() => sheetRef.current.close()}
              name={'times'}
              size={hp(3)}
            />
          </View>
          <View style={{flex: 1, marginTop: hp(2)}}>
            <InputText
              label={'Kupon'}
              placeholder={'Masukkan Kupon'}
              onChangeText={(e) => onChange(e)}
              value={value || coupon}
            />
          </View>

          {coupon ? (
            <View
              style={{
                // flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View style={{width: wp(45)}}>
                <Button onPress={onReset}>Hapus Kupon</Button>
              </View>
              <View style={{width: wp(45)}}>
                <Button color={'blue'} onPress={onPress}>
                  Gunakan
                </Button>
              </View>
            </View>
          ) : (
            <Button onPress={onPress}>Gunakan</Button>
          )}
        </View>
      </RBSheet>
    </View>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 16,
    height: '100%',
    flex: 1,
  },
  cardHeader: {
    marginBottom: 10,
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 0.5,
    paddingBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // paddingTop: 10,
  },
  textHeader: {
    fontSize: hp(2.4),
    fontWeight: 'bold',
  },
});
