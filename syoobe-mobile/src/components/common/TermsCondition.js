import React, {Component} from 'react';
import {WebView} from 'react-native-webview';

export default class TermsCondition extends Component {
  render() {
    return (
      <>
        <WebView
          source={{
            uri: 'https://www.syoobe.co.id/terms-and-conds/cms/view/10',
          }}
        />
      </>
    );
  }
}
