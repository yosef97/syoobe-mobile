import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
export const EmptyProductSeller = ({addProduct, onPress}) => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp(25),
      }}>
      <FontAwesome5Icon name={'box-open'} size={hp(20)} />
      <Text>Anda belum memiliki produk</Text>
      {addProduct && (
        <>
          <Text>klik tombol dibawah untuk mebuat produk</Text>
          <TouchableOpacity onPress={() => onPress()}>
            <View
              style={{
                padding: wp(3),
                backgroundColor: '#5f27cd',
                borderRadius: 5,
                marginTop: hp(2),
              }}>
              <Text style={{color: '#fff'}}>+ Tambah Produk</Text>
            </View>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};
