import React from 'react';
import {Platform, View} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const CardSectionLogin = (props) => {
  return <View style={styles.containerStyles}>{props.children}</View>;
};

const styles = {
  containerStyles: {
    // // justifyContent: 'flex-start',
    // flexDirection: 'row',
    // position: 'relative',
    // width:'100%',
    // backgroundColor:'transparent',
    // justifyContent: 'center',
    // alignItems: 'center',
    // marginBottom:15,
    // flex: 1,
    // justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
    width: '100%',
    backgroundColor: 'transparent',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: hp('2%'),
    backgroundColor: '#fff',
    borderRadius: 10,
    ...Platform.select({
      android: {
        elevation: 10,
      },
      ios: {
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
      },
    }),
  },
};

export {CardSectionLogin};
