import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from '../Font';
const ButtonRed = ({children, onPress, small}) => {
  const {buttonStyleSmall, buttonStyle, textStyle} = styles;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={small ? buttonStyleSmall : buttonStyle}>
      <Text style={textStyle}>{children}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
  },

  buttonStyle: {
    alignSelf: 'stretch',
    backgroundColor: '#c90305',
    borderRadius: 50,
    paddingTop: hp('2%'),
    paddingBottom: hp('2%'),
    marginBottom: 10,
  },
  buttonStyleSmall: {
    borderRadius: 5,
    backgroundColor: '#c90305',
    paddingLeft: wp('2%'),
    paddingRight: wp('2%'),
    paddingTop: hp('2%'),
    paddingBottom: hp('2%'),
    marginBottom: 10,
  },
};

export {ButtonRed};
