import React, {createRef} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

export const ProductType = ({onPress, value}) => {
  const sheetRef = createRef();
  const items = [
    {name: 'Produk Fisik', value: 'p'},
    {name: 'Produk Digital', value: 'd'},
  ];
  const getTitle = value === 'p' ? 'Produk Fisik' : 'Produk Digital';
  return (
    <View>
      <TouchableOpacity
        onPress={() => sheetRef.current.open()}
        activeOpacity={0.7}>
        <Text style={{paddingBottom: 5}}>Pilih Jenis Produk</Text>
        <View style={styles.dropdown}>
          <Text style={{color: '#000', fontSize: hp(2)}}>{getTitle}</Text>
          <FontAwesome5Icon name={'chevron-down'} size={hp(2)} />
        </View>
      </TouchableOpacity>
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        // height={hp(100)}
        customStyles={card}>
        <View style={{flex: 1}}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerContainerTitle}>Pilih Jenis Produk</Text>
          </View>
          <View style={styles.itemListContainer}>
            <FlatList
              data={items}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => (
                <CardItemList
                  key={index}
                  onPress={() =>
                    onPress({val: item.value, ref: sheetRef.current})
                  }
                  title={item.name}
                  style={{
                    borderColor: value === item.value ? 'red' : '#fff',
                    borderWidth: 1,
                    marginBottom: index === items.length - 1 ? hp(2) : 0,
                  }}
                />
              )}
            />
          </View>
        </View>
      </RBSheet>
    </View>
  );
};

const CardItemList = ({style, title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.cardItemList, style]}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>{title}</Text>
          <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  dropdown: {
    paddingVertical: hp(1.5),
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingHorizontal: wp(3),
    // marginBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#8395a7',
    borderWidth: 1,
  },
  cardItemList: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingVertical: hp(2),
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    borderRadius: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),
    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});
