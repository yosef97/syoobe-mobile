import React, {createRef} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Spinner} from '../Spinner';

export const ProductFileUpload = ({
  onPress,
  value,
  index,
  loading,
  fileName,
  label,
}) => {
  const sheetRef = createRef();
  const items = [
    {name: 'Pdf', value: 'pdf', icon: 'pdf-box', color: '#c0392b'},
    {name: 'Images', value: 'image', icon: 'image', color: '#2c3e50'},
    {name: 'Video', value: 'video', icon: 'video', color: '#009432'},
    {name: 'Audio', value: 'audio', icon: 'speaker', color: '#30336b'},
    {name: 'Zip', value: 'zip', icon: 'zip-box', color: '#f1c40f'},
    {name: 'Document', value: 'doc', icon: 'file-document', color: '#2980b9'},
  ];
  return (
    <View style={{marginVertical: hp(2)}}>
      <Text>{label ? label : '  '}</Text>

      <Picker
        onPress={() => sheetRef.current.open()}
        loading={loading}
        fileName={fileName}
      />
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(50)}
        customStyles={card}>
        <View style={{flex: 1}}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerContainerTitle}>Pilih Metode Upload</Text>
          </View>
          <View style={styles.itemListContainer}>
            {items.map((item, i) => (
              <CardItemList
                key={i}
                onPress={() =>
                  onPress({
                    type: item.value,
                    index,
                    ref: sheetRef.current,
                    item,
                  })
                }
                title={item.name}
                icon={item.icon}
                color={item.color}
              />
            ))}
          </View>
        </View>
      </RBSheet>
    </View>
  );
};

export const CardItemList = ({style, title, onPress, icon, color}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={{...styles.itemList, style}}>
        <MaterialCommunityIcons name={icon} size={hp(10)} color={color} />
        <Text style={{fontSize: hp(2)}}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

const Picker = ({onPress, loading, fileName}) => {
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={0.5}>
      <View style={styles.picker}>
        {loading ? (
          <Spinner color={'red'} />
        ) : (
          <>
            {!fileName && (
              <MaterialCommunityIcons
                name="file-multiple-outline"
                size={hp(5)}
                color={'#bdc3c7'}
              />
            )}
            <Text style={{paddingTop: fileName ? 0 : hp(2)}}>
              {fileName ? fileName : 'Pilih File'}
            </Text>
          </>
        )}
      </View>
    </TouchableOpacity>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  picker: {
    flex: 1,
    width: '100%',
    height: hp(14),
    backgroundColor: '#fff',
    borderWidth: 2,
    borderColor: '#bdc3c7',
    borderRadius: 5,
    borderStyle: 'dashed',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: hp(1),
  },
  dropdown: {
    paddingVertical: hp(1.5),
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingHorizontal: wp(3),
    // marginBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#8395a7',
    borderWidth: 1,
  },
  cardItemList: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingVertical: hp(2),
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    borderRadius: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),
    flexDirection: 'row',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
    // Bottom: hp(2),
  },
  itemList: {
    width: wp(30),
    height: hp(15),
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: hp(1),
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});
