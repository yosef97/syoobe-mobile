import React, {createRef} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export const ProductOrderFilter = ({onPress, value, data, style}) => {
  const sheetRef = createRef();

  const getTitle = 'Semua';
  const toggle = () => sheetRef.current.open();

  const {status_name = getTitle} =
    data.filter((val) => val.status_id === value)[0] || {};
  // console.log(name);
  return (
    <View>
      <TouchableOpacity onPress={() => toggle()} activeOpacity={0.7}>
        <View
          style={[
            {
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: '#fff',
              paddingVertical: hp(1),
              paddingHorizontal: wp(3),
            },
            style,
          ]}>
          <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
            {status_name}
          </Text>
          <MaterialCommunityIcons name="filter" size={hp(2)} color={'#000'} />
        </View>
      </TouchableOpacity>
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(100)}
        customStyles={card}>
        <View style={{flex: 1}}>
          <View style={styles.headerContainer}>
            <TouchableOpacity
              style={styles.headerContent}
              onPress={() => sheetRef.current.close()}>
              <View>
                <FontAwesome5Icon name={'times'} size={hp(2.6)} />
              </View>
            </TouchableOpacity>
            <Text style={styles.headerContainerTitle}>{'Filter'}</Text>
          </View>
          <View style={styles.itemListContainer}>
            <FlatList
              data={data}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => {
                if (item.status_id === 0) {
                  return (
                    <CardItemList
                      key={index}
                      onPress={() =>
                        onPress({val: null, ref: sheetRef.current})
                      }
                      title={'Semua'}
                      style={{
                        borderColor: '#fff',
                        borderWidth: 1,
                      }}
                    />
                  );
                } else {
                  return (
                    <CardItemList
                      key={index}
                      onPress={() =>
                        onPress({val: item.status_id, ref: sheetRef.current})
                      }
                      title={item.status_name}
                      style={{
                        borderColor: value === item.status_id ? 'red' : '#fff',
                        borderWidth: 1,
                        marginBottom: index === data.length - 1 ? hp(2) : 0,
                      }}
                    />
                  );
                }
              }}
            />
          </View>
        </View>
      </RBSheet>
    </View>
  );
};

const CardItemList = ({style, title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.cardItemList, style]}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>{title}</Text>
          <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2.4),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  dropdown: {
    paddingVertical: hp(1.5),
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingHorizontal: wp(3),
    // marginBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#8395a7',
    borderWidth: 1,
  },
  cardItemList: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingVertical: hp(2),
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    borderRadius: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),
    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});
