import React, {createRef, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

export const ProductCategory = ({onPress, value, data, setCategoryId}) => {
  const sheetRef = createRef();
  const initialData = data;
  const [items, setItems] = useState(initialData);
  const [tmpItems, setTmpItems] = useState([]);
  const [subItem, setSubItem] = useState(false);
  const [title, setTitle] = useState('Pilih Kategori Produk');
  const [keyName, setKeyName] = useState('category_name');
  const [keyId, setKeyId] = useState('category_id');
  // console.log(keyId, items);
  const toggle = () => {
    sheetRef.current.open();
    setItems(initialData);
  };

  const handleSetItem = (item, title) => {
    setKeyName(!subItem ? 'name' : 'category_name');
    setKeyId(!subItem ? 'sub_category_id' : 'category_id');
    setTimeout(() => {
      setItems(item);
      setSubItem(!subItem);
      setTitle(title);
    }, 200);
  };

  const handlePress = (item) => {
    if (item.sub_category && item.sub_category.length > 0) {
      setKeyName('name');
      setKeyId('sub_category_id');
      setTmpItems(item.sub_category);
      handleSetItem(item.sub_category, item[keyName]);
      // onPress({val: item[keyId], ref: sheetRef.current});
      setCategoryId({val: item[keyId]});
    } else {
      onPress({val: item[keyId], ref: sheetRef.current});
      subItem && setTmpItems(item.sub_category);
      setKeyName(subItem ? 'name' : 'category_name');
      setKeyId(subItem ? 'sub_category_id' : 'category_id');
    }
  };

  const getName = () => {
    // console.log(subItem);
    if (subItem) {
      let newItem = {};
      initialData.filter((val) => {
        if (val.sub_category.length > 0) {
          newItem = val.sub_category.filter(
            (sub) => sub?.sub_category_id === value,
          )[0];
        }
      }) || [];
      return (newItem && newItem[keyName]) || title;
    }
    const item = initialData.filter((val) => val[keyId] === value)[0] || {};
    return item[keyName] || title;
  };

  // console.log(keyName, keyId, tmpItems);
  return (
    <View style={{marginTop: hp(2)}}>
      <TouchableOpacity onPress={() => toggle()} activeOpacity={0.7}>
        <Text style={{paddingBottom: 5}}>{title}</Text>
        <View style={styles.dropdown}>
          <Text style={{color: '#000', fontSize: hp(2)}}>{getName()}</Text>
          <FontAwesome5Icon name={'chevron-down'} size={hp(2)} />
        </View>
      </TouchableOpacity>
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(100)}
        customStyles={card}>
        <View style={{flex: 1}}>
          <View style={styles.headerContainer}>
            {subItem && (
              <TouchableOpacity
                style={styles.headerContent}
                onPress={() =>
                  handleSetItem(initialData, 'Pilih Kategori Produk')
                }>
                <View>
                  <FontAwesome5Icon name={'chevron-left'} size={hp(3)} />
                </View>
              </TouchableOpacity>
            )}
            <Text style={styles.headerContainerTitle}>{title}</Text>
          </View>
          <View style={styles.itemListContainer}>
            <FlatList
              data={subItem ? tmpItems : items}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => (
                <CardItemList
                  key={index}
                  onPress={() => handlePress(item)}
                  title={item[keyName]}
                  style={{
                    borderColor: value === item[keyId] ? 'red' : '#fff',
                    borderWidth: 1,
                    marginBottom: index === items.length - 1 ? hp(2) : 0,
                  }}
                />
              )}
            />
          </View>
        </View>
      </RBSheet>
    </View>
  );
};

const CardItemList = ({style, title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.cardItemList, style]}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>{title}</Text>
          <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  dropdown: {
    paddingVertical: hp(1.5),
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingHorizontal: wp(3),
    // marginBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#8395a7',
    borderWidth: 1,
  },
  cardItemList: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingVertical: hp(2),
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    borderRadius: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),
    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});
