import React, {createRef} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RBSheet from 'react-native-raw-bottom-sheet';
import {Spinner} from '../Spinner';

export const ProductImage = (props) => {
  const sheetRef = createRef();
  return (
    <View>
      <ProductImageChild
        {...props}
        onPressOpen={() => sheetRef.current.open()}
      />
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(100)}
        customStyles={card}>
        <View style={{flex: 1}}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerContainerTitle}>Gambar Produk</Text>
          </View>
          <View style={{flex: 1, paddingHorizontal: wp(3)}}>
            <ProductImageChild
              {...props}
              // onPressOpen={() => sheetRef.current.open()}
              showAll
            />
          </View>
        </View>
      </RBSheet>
    </View>
  );
};

const ProductImageChild = ({
  data = [],
  onPress,
  setPrimary,
  onRemove,
  loading,
  showAll,
  onPressOpen,
}) => {
  let items = [];
  if (data && data.length > 2) {
    if (showAll) {
      items = data;
    } else {
      data.map((item, index) => {
        if (index < 2) {
          items.push(item);
        }
      });
    }
    items;
  } else {
    // console.log('ini items', data);
    items = data;
  }
  // return null;
  return (
    <View style={{flex: 1, marginTop: hp(2)}}>
      {!showAll && <Text>Pilih Gambar (500x500)</Text>}

      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'flex-start',
          justifyContent:
            data && data.length > 1 ? 'space-around' : 'flex-start',
          // alignContent: 'stretch',
          flexWrap: 'wrap',
        }}>
        {/* <ChildImage onPress={() => alert('ok')} more count={4} /> */}

        {items &&
          items.map((row, index) => {
            if (data && data.length > 2 && index === 1 && !showAll) {
              return (
                <ChildImage
                  key={index}
                  onPress={() => onPressOpen()}
                  more
                  count={data ? data.length - 2 : null}
                  uri={row.product_image}
                />
              );
            } else {
              return (
                <ChildImage
                  key={index}
                  onPress={() => setPrimary(row)}
                  primary={row.default_image === 'Y'}
                  uri={row.product_image}
                  onRemove={() => onRemove(row)}
                  style={{marginRight: data && data.length > 1 ? 0 : wp(2)}}
                />
              );
            }
          })}

        <Picker onPress={onPress} loading={loading} data={data} />
      </View>
    </View>
  );
};

const ChildImage = ({
  onPress,
  count = 0,
  more = false,
  uri,
  primary = false,
  onRemove,
  style,
}) => {
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={0.5}>
      <View
        style={[
          {
            ...styles.picker,
            borderStyle: 'solid',
            borderColor: '#f5f6fa',
          },
          style,
        ]}>
        {more && (
          <>
            <View style={styles.background} />
            <Text style={styles.text}>{`${count}+`}</Text>
          </>
        )}
        {!more && (
          <View style={styles.actionButton}>
            {primary && (
              <MaterialCommunityIcons
                name={'check-decagram'}
                size={hp(2.5)}
                color={'#0097e6'}
              />
            )}
            <MaterialCommunityIcons
              name={'close-circle'}
              size={hp(2.5)}
              color={'#c23616'}
              onPress={onRemove}
            />
          </View>
        )}
        <Image
          resizeMode="contain"
          source={{
            uri,
          }}
          style={{width: '100%', height: '100%'}}
        />
      </View>
    </TouchableOpacity>
  );
};

const Picker = ({onPress, loading, data}) => {
  return (
    <View>
      {loading ? (
        <TouchableOpacity onPress={onPress} activeOpacity={0.5}>
          <View style={styles.picker}>
            <Spinner color={'red'} />
          </View>
        </TouchableOpacity>
      ) : data && data.length === 0 ? (
        <TouchableOpacity onPress={onPress} activeOpacity={0.5}>
          <View style={styles.picker}>
            <MaterialCommunityIcons
              name="image-plus"
              size={hp(5)}
              color={'#bdc3c7'}
            />
          </View>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity onPress={onPress} activeOpacity={0.5}>
          <View style={styles.picker}>
            <MaterialCommunityIcons
              name="image-plus"
              size={hp(5)}
              color={'#bdc3c7'}
            />
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  picker: {
    flex: 1,
    width: wp(30),
    height: hp(15),
    maxWidth: wp(30),
    maxHeight: hp(15),
    backgroundColor: '#fff',
    borderWidth: 2,
    borderColor: '#bdc3c7',
    borderRadius: 5,
    borderStyle: 'dashed',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: hp(1),
    // marginHorizontal: wp(1),
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
    backgroundColor: '#000',
    borderRadius: 5,
    zIndex: 2,
    opacity: 0.5,
  },
  text: {
    color: '#fff',
    fontSize: hp(4),
    fontWeight: 'bold',
    position: 'absolute',
    zIndex: 3,
  },
  actionButton: {
    position: 'absolute',
    right: 5,
    top: 5,
    zIndex: 999,
    flexDirection: 'row',
  },
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
});
