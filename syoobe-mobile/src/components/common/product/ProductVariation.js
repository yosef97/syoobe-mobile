import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {ProgressBar, Colors} from 'react-native-paper';
import InputText from '../InputText';
import {getDate} from '../../../helpers/helper';

const ButtonVariation = (props) => {
  const {values, optionValue, onPress, dataIndex, type} = props;
  // console.log(type, values);
  const setSelectedVariant = (
    type,
    onPress,
    item = null,
    index = null,
    option = [],
    childIndex = null,
  ) => {
    if (type === 'select') {
      let dataSelect = {value: item.product_option_value_id, index};
      return onPress(dataSelect);
    }
    if (type === 'checkbox') {
      let dataCheckbox = {
        isChecked: option && !option[childIndex].isBoolean,
        value: item.product_option_value_id,
        index,
        childIndex,
      };
      // console.log(option, childIndex);
      return onPress(dataCheckbox);
    }
    if (type === 'radio') {
      let dataSelect = {
        value: item.product_option_value_id,
        optionValue: option && !option[childIndex].option_value,
        index,
      };
      return onPress(dataSelect);
    }
  };

  const getSlectedVariant = (
    type,
    option = null,
    item = null,
    index = null,
  ) => {
    if (type === 'select') {
      return option === item.product_option_value_id ? 'red' : '#576574';
    }
    if (type === 'checkbox') {
      return option && option[index].isBoolean ? 'red' : '#576574';
    }
    if (type === 'radio') {
      return option === item.product_option_value_id ? 'red' : '#576574';
    }
  };
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
      }}>
      {values &&
        values.map((item, index) => (
          <TouchableHighlight
            underlayColor={'#fff'}
            key={index}
            // style={{flex: 1}}
            onPress={() =>
              setSelectedVariant(
                type,
                onPress,
                item,
                dataIndex,
                optionValue,
                index,
              )
            }>
            <View
              style={{
                ...styles.buttonContainer,
                borderColor: getSlectedVariant(type, optionValue, item, index),
              }}>
              <Text
                style={{
                  fontSize: 12,
                  color: getSlectedVariant(type, optionValue, item, index),
                  fontWeight: '600',
                }}>
                {item.name}
              </Text>
            </View>
          </TouchableHighlight>
        ))}
    </View>
  );
};

const InputTextVariation = (props) => {
  const {
    onChange,
    option,
    data,
    dataIndex,
    label,
    type,
    optionValue,
    optionId,
  } = props;
  console.log(option);
  return option.map((item, index) => {
    if (optionId === item.product_option_id) {
      return (
        <View key={index} style={{flex: 1, paddingTop: hp(1.5)}}>
          <InputText
            label={label}
            placeholder={label}
            value={optionValue}
            multiline={type === 'textarea'}
            numberOfLines={type === 'textarea' ? 4 : 1}
            onChangeText={(e) =>
              onChange({value: e, childIndex: dataIndex}, type)
            }
          />
        </View>
      );
    }
  });
};
const InputFileVariation = (props) => {
  const {
    onChange,
    option,
    data,
    dataIndex,
    label,
    type,
    optionValue,
    optionId,
    onPress,
  } = props;
  let progress =
    !isNaN(parseInt(optionValue)) && optionValue > 0 && optionValue !== '';
  return option.map((item, index) => {
    if (optionId === item.product_option_id) {
      return (
        <View key={index} style={{flex: 1, paddingTop: hp(1.5)}}>
          <InputText
            label={label}
            icon={'attach-file'}
            Component={MaterialIcons}
            editable={false}
            placeholder={label}
            value={
              !isNaN(optionValue) && optionValue > 0
                ? 'Mengunggah data'
                : optionValue
            }
            onFocus={() => onPress({index: dataIndex}, type)}
          />
          {progress && (
            <ProgressBar
              progress={progress ? optionValue / 100 : 0}
              color={Colors.red800}
            />
          )}
        </View>
      );
    }
  });
};

const InputDateTimeVariation = (props) => {
  const {
    onChange,
    option,
    data,
    dataIndex,
    label,
    type,
    optionValue,
    optionId,
    onPress,
    onPressTime,
  } = props;
  let date = optionValue.split(' ');
  return option.map((item, index) => {
    if (optionId === item.product_option_id) {
      return (
        <View key={index} style={styles.containerDate}>
          <TouchableOpacity
            // underlayColor={'#fff'}
            style={styles.dateTimeCard}
            onPress={() =>
              onPress(
                {
                  value: getDate(date[0]),
                  optionValue: date[1],
                  index: dataIndex,
                },
                type,
              )
            }>
            <View>
              <Text style={{fontSize: hp(2), color: '#fff'}}>{date[0]}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            // underlayColor={'#fff'}
            style={styles.dateTimeCard}
            onPress={() =>
              onPressTime(
                {
                  value: date[1],
                  optionValue: getDate(date[0]),
                  index: dataIndex,
                },
                'time-datetime',
              )
            }>
            <View>
              <Text style={{fontSize: hp(2), color: '#fff'}}>{date[1]}</Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  });
};
const InputDateAndTimeVariation = (props) => {
  const {
    onChange,
    option,
    data,
    dataIndex,
    label,
    type,
    optionValue,
    optionId,
    onPress,
    onPressTime,
  } = props;
  // let date = optionValue.split(' ');
  return option.map((item, index) => {
    if (optionId === item.product_option_id) {
      return (
        <TouchableOpacity
          key={index}
          style={{...styles.dateTimeCard}}
          onPress={() =>
            onPress(
              {
                value: optionValue,
                index: dataIndex,
              },
              type,
            )
          }>
          <View>
            <Text style={{fontSize: hp(2), color: '#fff'}}>{optionValue}</Text>
          </View>
        </TouchableOpacity>
      );
    }
  });
};

export const ProductVariation = (props) => {
  const {
    data,
    onPress,
    type,
    types,
    option,
    onChange,
    variant,
    onPressTime,
  } = props;
  // console.log(type);
  return data.map((item, index) => {
    if (item.type == type && variant == 'button') {
      return (
        <View key={index} style={{flex: 1, marginTop: hp(2)}}>
          <Text style={{fontSize: hp(2)}}>{item.name}</Text>
          <View style={{paddingTop: 10}}>
            <ButtonVariation
              values={item.product_option_value}
              onPress={(data) => onPress(data)}
              optionValue={option[index].option_value}
              dataIndex={index}
              type={item.type}
              {...props}
            />
          </View>
        </View>
      );
    }
    if (item.type === type && variant === 'input') {
      return (
        <InputTextVariation
          key={index}
          onChange={(data, type) => onChange(data, type)}
          optionValue={option[index].option_value}
          optionId={option[index].product_option_id}
          dataIndex={index}
          type={item.type}
          label={item.name}
          {...props}
        />
      );
    }
    if (item.type === type && variant === 'file') {
      return (
        <InputFileVariation
          key={index}
          onPress={(data, type) => onPress(data, type)}
          optionValue={option[index].option_value}
          optionId={option[index].product_option_id}
          dataIndex={index}
          type={item.type}
          label={item.name}
          {...props}
        />
      );
    }
    if (item.type === type && variant === 'datetime') {
      return (
        <View key={index} style={{flex: 1, marginTop: hp(2)}}>
          <Text style={{fontSize: hp(2)}}>{item.name}</Text>
          <View style={{paddingTop: 10}}>
            <InputDateTimeVariation
              key={index}
              onPress={(data, type) => onPress(data, type)}
              onPressTime={(data, type) => onPressTime(data, type)}
              optionValue={option[index].option_value}
              optionId={option[index].product_option_id}
              dataIndex={index}
              type={item.type}
              label={item.name}
              {...props}
            />
          </View>
        </View>
      );
    }
    if (item.type === type && variant === 'time') {
      return (
        <View key={index} style={{flex: 1, marginTop: hp(2)}}>
          <Text style={{fontSize: hp(2)}}>{item.name}</Text>
          <View style={{paddingTop: 10}}>
            <InputDateAndTimeVariation
              key={index}
              onPress={(data, type) => onPress(data, type)}
              optionValue={option[index].option_value}
              optionId={option[index].product_option_id}
              dataIndex={index}
              type={item.type}
              label={item.name}
              {...props}
            />
          </View>
        </View>
      );
    }
    if (item.type === type && variant === 'date') {
      return (
        <View key={index} style={{flex: 1, marginTop: hp(2)}}>
          <Text style={{fontSize: hp(2)}}>{item.name}</Text>
          <View style={{paddingTop: 10}}>
            <InputDateAndTimeVariation
              key={index}
              onPress={(data, type) => onPress(data, type)}
              optionValue={option[index].option_value}
              optionId={option[index].product_option_id}
              dataIndex={index}
              type={item.type}
              label={item.name}
              {...props}
            />
          </View>
        </View>
      );
    }
  });
};

const styles = StyleSheet.create({
  buttonContainer: {
    paddingHorizontal: wp(5),
    // width: wp(33),
    paddingVertical: hp(1.5),
    borderRadius: 5,
    borderWidth: 1.5,
    marginTop: 0,
    // borderColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5,
  },
  dateTimeCard: {
    flex: 1,
    paddingVertical: hp(1.8),
    width: '100%',
    backgroundColor: '#95a5a6',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 3,
  },
  containerDate: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
