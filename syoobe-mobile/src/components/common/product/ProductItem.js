import FavouriteButton from '../../FavouriteButton';
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {substr, formatRupiah} from '../../../helpers/helper';

const ProductItem = (props) => {
  const data = props.item;
  if (data.shops && data.shops.length > 0) {
    return (
      <FlatList
        data={data.shops}
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{paddingHorizontal: 5}}
        renderItem={({item, index}) => (
          <CardOfficialStore key={index} {...props} row={item} />
        )}
      />
    );
  }
  return (
    <View>
      <FlatList
        data={data.products}
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{paddingHorizontal: 5}}
        renderItem={({item, index}) => (
          <ProductCard key={index} {...props} row={item} />
        )}
      />
    </View>
  );
};

export const ProductCard = (props) => {
  const {row, navigation, tab, _token} = props;
  const goToProduct = (row, navigation) => {
    navigation.push('ProductDetail', {
      product_id: row.product_id,
      product_requires_shipping: row.prod_requires_shipping,
      product_name: row.product_name,
    });
    console.log('ini row', row);
  };
  return (
    <View style={({flex: 1, marginTop: 10}, [styles.cardBorder])}>
      {tab !== 'recent' && _token !== null ? (
        <FavouriteButton {...row} />
      ) : null}
      <TouchableOpacity
        onPress={() => goToProduct(row, navigation)}
        style={{zIndex: -1}}>
        <View style={styles.container}>
          <Image
            source={{uri: row.product_image}}
            style={styles.productImage}
          />
          <View style={{paddingHorizontal: 1, flex: 1}}>
            <Text style={{color: '#000', textTransform: 'capitalize'}}>
              {substr(row.product_name || '', 30)}
            </Text>
            <Text style={styles.productPriceText}>
              {`${row.price_currency}. ${formatRupiah(row.product_price || 0)}`}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const CardOfficialStore = (props) => {
  const {row, navigation} = props;

  return (
    <TouchableOpacity
      onPress={() => navigation.push('ShopDetails', {shops_id: row.id})}>
      <View
        style={
          ({
            flex: 1,
            marginHorizontal: wp(2),
            backgroundColor: '#fff',
            justifyContent: 'center',
            marginVertical: hp(2),
          },
          [styles.cardBorder])
        }>
        <View style={styles.cardShops}>
          <Image
            source={{uri: row.logo}}
            style={{width: '100%', height: '100%', resizeMode: 'cover'}}
          />
        </View>
        <View style={{width: wp(49), alignItems: 'center'}}>
          <Text
            style={{
              // flex: 1,
              fontSize: hp(2),
              fontWeight: 'bold',
              textTransform: 'capitalize',
              marginBottom: hp(1.5),
            }}>
            {substr(row.name || '', 12)}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  shadow: {
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 15,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 12.22,
    elevation: 2,
  },
  cardBorder: {
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
  },
  container: {
    width: wp(45),
    height: hp(35),
    maxWidth: wp(50),
    backgroundColor: '#fff',
    marginHorizontal: wp(2),
    borderRadius: hp(1),
    // marginVertical: hp(1),
  },
  productImage: {
    // width: wp(40),
    height: hp(25),
    resizeMode: 'stretch',
    borderRadius: 5,
    // zIndex: -99,
  },
  productPriceText: {
    color: '#e74c3c',
    fontWeight: 'bold',
    fontSize: hp(2),
    position: 'absolute',
    bottom: 20,
    left: 5,
  },
  cardShops: {
    width: wp(45),
    height: hp(15),
    maxWidth: wp(45),
    maxHeight: hp(18),
    backgroundColor: '#fff',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 5,
  },
});
export default ProductItem;
