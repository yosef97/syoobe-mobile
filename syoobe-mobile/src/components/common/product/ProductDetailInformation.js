import React from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export const ProductDetailInformation = ({title, data}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>{title}</Text>
      <View style={styles.content}>
        <FlatList
          data={data}
          renderItem={({item, index}) => <Item key={index} item={item} />}
        />
      </View>
    </View>
  );
};

const Item = ({item}) => {
  if (item.isAvailable) {
    return (
      <View style={styles.item}>
        <Text style={{fontSize: hp(2)}}>{item.name}</Text>
        <Text style={{fontSize: hp(2)}}>{item.value}</Text>
      </View>
    );
  }
  return null;
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    backgroundColor: '#fff',
    marginTop: hp(2),
  },
  content: {
    paddingTop: hp(1),
    borderTopColor: '#f5f6fa',
    borderTopWidth: 1,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: hp(1),
  },
  textTitle: {
    fontSize: hp(2.2),
    fontWeight: 'bold',
    paddingBottom: hp(1),
  },
});
