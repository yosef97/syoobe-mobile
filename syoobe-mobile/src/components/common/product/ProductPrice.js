import {formatRupiah} from '../../../helpers/helper';
import React, {createRef} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {TextInputCostum} from '../TextInput';
import {Button} from '../Button';

export const ProductPrice = ({onChangeText, value, fee, feeTotal}) => {
  const sheetRef = createRef();
  const getTitle = value
    ? `Rp. ${formatRupiah(feeTotal)}`
    : 'Masukan harga produk';
  return (
    <View>
      <TouchableOpacity
        onPress={() => sheetRef.current.open()}
        activeOpacity={0.7}>
        <Text style={{paddingBottom: 5}}>Harga penjualan</Text>
        <View style={styles.dropdown}>
          <Text style={{color: '#000', fontSize: hp(2)}}>{getTitle}</Text>
        </View>
      </TouchableOpacity>
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(60)}
        customStyles={card}>
        <View style={{flex: 1}}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerContainerTitle}>Harga Produk</Text>
          </View>
          <View style={styles.itemListContainer}>
            <TextInputCostum
              onChangeText={onChangeText}
              value={formatRupiah(value)}
              keyboardType={'numeric'}
              label={'Harga Produk'}
            />
            <TextInputCostum
              // onChangeText={onChangeText}
              value={!isNaN(fee) ? formatRupiah(fee) : 0}
              label={'Biaya Transaksi'}
            />
            <TextInputCostum
              // onChangeText={onChangeText}
              value={!isNaN(feeTotal) ? formatRupiah(feeTotal) : 0}
              label={'Harga Penjualan'}
            />
            <View style={{marginTop: hp(2)}}>
              <Button onPress={() => sheetRef.current.close()}>Simpan</Button>
            </View>
          </View>
        </View>
      </RBSheet>
    </View>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  dropdown: {
    paddingVertical: hp(1.5),
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingHorizontal: wp(3),
    // marginBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#8395a7',
    borderWidth: 1,
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});
