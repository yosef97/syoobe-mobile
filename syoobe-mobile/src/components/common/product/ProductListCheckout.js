import React from 'react';
import {Image, Text, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {formatRupiah} from '../../../helpers/helper';

export const ProductListCheckout = (props) => {
  // console.log(props);

  return props.items.map((product, index) => (
    <View key={index}>
      <View
        style={{
          flex: 1,
          marginBottom: hp(1),
          backgroundColor: '#fff',
          paddingHorizontal: wp(3),
          paddingVertical: hp(1.5),
        }}>
        <Text
          style={{
            fontSize: hp(2),
            fontWeight: 'bold',
            color: '#000',
            textTransform: 'capitalize',
            paddingBottom: hp(1),
          }}>
          {product.store_name}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            //   borderTopColor: '#eaeaea',
            //   borderTopWidth: 1,
            paddingVertical: hp(1.5),
          }}>
          <View
            style={{
              width: 70,
              height: 70,
              maxWidth: 70,
              maxHeight: 70,
              backgroundColor: '#eaeaea',
              borderRadius: 5,
            }}>
            <Image
              style={{
                width: '100%',
                height: '100%',
                borderRadius: 5,
              }}
              source={{uri: product.product_image}}
            />
          </View>
          <View style={{flex: 1, marginLeft: wp(3)}}>
            <Text
              style={{
                fontSize: hp(2),
                color: '#000',
                fontWeight: '700',
              }}>
              {product.product_name}
            </Text>
            {/* <Text style={{fontSize: hp(2)}}>Jumlah barang 1</Text> */}
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  fontSize: hp(2.2),
                  color: 'rgba(235, 47, 6,1.0)',
                  fontWeight: 'bold',
                }}>
                {`Rp. ${formatRupiah(product.unit_price)}`}
              </Text>
              <Text
                style={{
                  fontSize: hp(2.2),
                  color: '#000',
                  fontWeight: 'bold',
                  marginLeft: wp(2),
                }}>
                {`(x${product.qty_number})`}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  ));
};
