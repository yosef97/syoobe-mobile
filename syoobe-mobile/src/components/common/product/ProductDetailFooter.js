import DateTimePicker from '@react-native-community/datetimepicker';
import React, {createRef, useState} from 'react';
import {Alert, Platform} from 'react-native';
import {
  Image,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  AsyncStorage,
} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {formatRupiah} from '../../../helpers/helper';
import {Button} from '../Button';
import {ProductVariation} from './ProductVariation';
// import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const ButtonSheet = (props) => {
  const {onPress, onPressMessage, title} = props;
  return (
    <View style={[styles.footer, {paddingHorizontal: wp(3.5)}]}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View style={{width: wp(20)}}>
          <Button
            onPress={() => onPressMessage()}
            style={{paddingVertical: hp('1.5%'), backgroundColor: '#fff'}}>
            <Ionicons name={'chatbox-ellipses'} size={hp(2.6)} />
          </Button>
        </View>
        <View style={{width: wp(70)}}>
          <Button
            style={{paddingVertical: hp('1.5%')}}
            onPress={() => onPress()}>
            {title}
          </Button>
        </View>
      </View>
    </View>
  );
};

const FooterActionButton = (props) => {
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState({});
  const [type, setType] = useState(null);
  const [mode, setMode] = useState('time');

  const setDate = (e, selectedDate) => {
    let date = new Date(selectedDate);
    setVisible(false);
    let newData = {};
    if (type === 'time-datetime' || type === 'time') {
      let hours = date.getHours();
      let minutes = date.getMinutes();
      let time = `${hours}:${minutes}`;
      newData = {...data, value: time};
    }

    if (type === 'date') {
      let dates = date.getDate();
      let month = date.getMonth();
      let year = date.getFullYear();
      let newDate = `${year}-${month}-${dates}`;
      newData = {...data, value: newDate};
    }

    // console.log(visible, newData);
    props.onPressButtonVariant(newData, type);
  };

  const {
    _token,
    productDetails,
    addToCart,
    navigation,
    option,
    totalItems,
    onPressAddCart,
    onPressBuyNow,
    onPressIncrement,
    onPressDecrement,
    onPressButtonVariant,
    closeModal,
  } = props;

  console.log('ini token', JSON.stringify(_token));

  const {
    product_name,
    product_price,
    user_own_product,
    product_options,
    price_currency,
    images,
    product_type,
  } = productDetails;
  const sheetRef = createRef();

  const openTimePicker = (data = null, type = null) => {
    setMode('time');
    setData(data);
    setType(type);
    setVisible(!visible);
  };
  const openDatePicker = (data = null, type = null) => {
    setMode('date');
    setData(data);
    setType(type);
    setVisible(!visible);
  };

  const handlePressMessage = () => {
    if (_token !== null) {
      navigation.navigate('ChatWebViewNavigate', {
        userSelected: productDetails.user_username_chat,
      });
    } else {
      Alert.alert('informasi', 'Silahkan login untuk melanjutkan', [
        {text: 'OK', onPress: () => navigation.push('AuthDrawer')},
      ]);
    }
  };

  return (
    <View style={{flex: 1}}>
      <ButtonSheet
        onPress={() => sheetRef.current.open()}
        onPressMessage={() => handlePressMessage()}
        title={product_type === 'P' ? 'Tambah Ke Keranjang' : 'Beli Sekarang'}
      />
      <RBSheet
        ref={sheetRef}
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(100)}
        customStyles={card}>
        <View style={[styles.container]}>
          <View style={styles.cardAbsolute} />
          <View style={{flex: 1, paddingTop: -30, zIndex: 2}}>
            <View style={{}}>
              <View style={styles.cardHeader}>
                <Text style={styles.textHeader}>Pilih Variant</Text>
                <FontAwesome
                  onPress={() => sheetRef.current.close()}
                  name={'times'}
                  size={hp(3)}
                />
              </View>
              {/* Card Product */}
              <View style={[styles.shadow, styles.cardContainer]}>
                <View style={styles.cardImage}>
                  <Image
                    style={{width: '100%', height: '100%', borderRadius: 10}}
                    source={{uri: images && images[0]}}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    paddingLeft: 10,
                    paddingBottom: hp(2),
                    marginVertical: hp(1),
                  }}>
                  <Text style={styles.textTitle}>{product_name}</Text>
                  <Text style={styles.textPrice}>
                    {`${price_currency}. ${formatRupiah(product_price)}`}
                  </Text>

                  {/* Increment & Decrement */}
                  <View style={styles.cardCounter}>
                    <FontAwesome
                      name={'minus-square'}
                      size={hp(3)}
                      onPress={totalItems > 1 ? onPressIncrement : null}
                    />
                    <Text style={styles.textCounter}>{totalItems}</Text>
                    <FontAwesome
                      name={'plus-square'}
                      size={hp(3)}
                      onPress={onPressDecrement}
                    />

                    {/* End Increment & Decrement
                     */}
                  </View>
                </View>
              </View>
            </View>

            {/* Product Variant */}
            <KeyboardAvoidingView
              behavior={'position'}
              style={{flex: 1}}
              enabled
              keyboardVerticalOffset={hp(8)}>
              <ScrollView showsVerticalScrollIndicator={false}>
                <View
                  style={{
                    flex: 1,
                    paddingBottom: hp(2.5),
                    // backgroundColor: 'red',
                    height: null,
                    marginHorizontal: wp(1),
                  }}>
                  <ProductVariation
                    onPress={(data) => onPressButtonVariant(data, 'select')}
                    data={product_options}
                    variant={'button'}
                    type={'select'}
                    option={option} // selected option
                  />

                  <ProductVariation
                    onPress={(data) => onPressButtonVariant(data, 'checkbox')}
                    data={product_options}
                    variant={'button'}
                    type={'checkbox'}
                    option={option} // selected option
                  />
                  <ProductVariation
                    onPress={(data) => onPressButtonVariant(data, 'radio')}
                    data={product_options}
                    variant={'button'}
                    type={'radio'}
                    option={option} // selected option
                  />
                  <ProductVariation
                    onChange={(data, type) => onPressButtonVariant(data, type)}
                    data={product_options}
                    variant={'input'}
                    type={'text'}
                    option={option} // selected option
                  />
                  <ProductVariation
                    onChange={(data, type) => onPressButtonVariant(data, type)}
                    data={product_options}
                    variant={'input'}
                    type={'textarea'}
                    option={option} // selected option
                  />
                  <ProductVariation
                    onPress={(data, type) => onPressButtonVariant(data, type)}
                    data={product_options}
                    variant={'file'}
                    type={'file'}
                    option={option} // selected option
                  />
                  <ProductVariation
                    onPress={(data, type) => onPressButtonVariant(data, type)}
                    onPressTime={(data, type) => openTimePicker(data, type)}
                    data={product_options}
                    variant={'datetime'}
                    type={'datetime'}
                    option={option} // selected option
                  />
                  <ProductVariation
                    // onPress={(data, type) => onPressButtonVariant(data, type)}
                    onPress={(data, type) => openTimePicker(data, type)}
                    data={product_options}
                    variant={'time'}
                    type={'time'}
                    option={option} // selected option
                  />
                  <ProductVariation
                    // onPress={(data, type) => onPressButtonVariant(data, type)}
                    onPress={(data, type) => openDatePicker(data, type)}
                    data={product_options}
                    variant={'date'}
                    type={'date'}
                    option={option} // selected option
                  />
                </View>
              </ScrollView>
            </KeyboardAvoidingView>
          </View>
          <View
            style={{
              paddingBottom: hp(2.5),
              // backgroundColor: 'red',
              height: null,
              marginHorizontal: wp(1),
            }}>
            <Button
              style={{paddingVertical: hp('2%')}}
              onPress={() => onPressAddCart(sheetRef.current)}>
              {product_type === 'P' ? 'Tambah Ke Keranjang' : 'Beli Sekarang'}
            </Button>
          </View>
          {visible && (
            <DateTimePicker
              testID="dateTimePicker"
              value={new Date(1598051730000)}
              mode={mode}
              is24Hour={true}
              display="default"
              onChange={(e, selectedTime) => setDate(e, selectedTime)}
            />
          )}
        </View>
      </RBSheet>
    </View>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 16,
    height: '100%',
    flex: 1,
  },
  cardAbsolute: {
    width: hp(100),
    height: hp(26),
    backgroundColor: '#fff',
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    zIndex: 1,
  },
  cardHeader: {
    marginBottom: 10,
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 0.5,
    paddingBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // paddingTop: 10,
  },
  cardContainer: {
    // flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginVertical: hp(1),
    paddingVertical: hp(0.5),
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginHorizontal: wp(1),
    borderRadius: 10,
  },
  cardCounter: {
    // flex:1,
    width: wp(30),
    height: Platform.OS === 'ios' ? hp(5) : hp(5),
    flexDirection: 'row',
    paddingTop: hp(2),
    justifyContent: 'flex-start',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  cardImage: {
    // flex: 1,
    width: wp(20),
    maxWidth: wp(20),
    height: hp(13),
    maxHeight: hp(13),
    backgroundColor: '#FFF',
    marginHorizontal: wp(1),
  },
  textHeader: {
    fontSize: hp(2.4),
    fontWeight: 'bold',
  },
  textCounter: {
    color: '#576574',
    fontWeight: 'bold',
    fontSize: hp(3),
    marginHorizontal: 10,
  },
  textPrice: {
    fontSize: hp(2.5),
    color: 'red',
    fontWeight: 'bold',
    paddingTop: hp(0.5),
  },
  textTitle: {
    fontSize: hp(2.3),
    fontWeight: 'bold',
    color: '#2d3436',
  },
  footer: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    position: 'absolute',
    width: '100%',
    paddingTop: 10,
    paddingBottom: 10,
    bottom: 0,
    zIndex: 99,
    elevation: 10,
    borderTopWidth: 1,
    borderColor: '#dcdcdc',
  },
  shadow: {
    shadowColor: '#eaeaea',
    shadowOffset: {
      width: 15,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 12.22,

    elevation: 4,
  },
});

export {FooterActionButton};
