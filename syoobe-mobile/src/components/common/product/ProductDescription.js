import React, {createRef} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import HTML from 'react-native-render-html';

export const ProductDescription = ({onPress, title, style}) => {
  const sheetRef = createRef();

  const toggle = () => sheetRef.current.open();

  return (
    <View>
      <TouchableOpacity onPress={() => toggle()} activeOpacity={0.7}>
        <View style={style.listItem}>
          <Text style={style.descriptionTxt}>Deskripsi Produk</Text>
          <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
        </View>
      </TouchableOpacity>
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(100)}
        customStyles={card}>
        <View style={{flex: 1}}>
          <View style={styles.headerContainer}>
            <TouchableOpacity
              style={styles.headerContent}
              onPress={() => sheetRef.current.close()}>
              <View>
                <FontAwesome5Icon name={'times'} size={hp(2.6)} />
              </View>
            </TouchableOpacity>
            <Text style={styles.headerContainerTitle}>{'Deskripsi'}</Text>
          </View>
          <View style={styles.itemListContainer}>
            <ScrollView
              style={{
                flex: 1,
                backgroundColor: '#fff',
                paddingHorizontal: wp(3),
                paddingVertical: hp(1),
              }}>
              <HTML html={title} staticContentMaxWidth={wp(100)} />
            </ScrollView>
          </View>
        </View>
      </RBSheet>
    </View>
  );
};


const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2.4),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  dropdown: {
    paddingVertical: hp(1.5),
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingHorizontal: wp(3),
    // marginBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#8395a7',
    borderWidth: 1,
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    marginTop: hp(1),
    // paddingHorizontal: wp(3),
    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});
