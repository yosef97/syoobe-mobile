import React from 'react';
import {FlatList, View, Text, StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ProductItem from './ProductItem';

const ProductList = (props) => {
  const {data} = props;
  return (
    <FlatList
      data={data}
      renderItem={({item, index}) => (
        <View
          key={index}
          style={{
            flex: 1,
            marginBottom: hp(4),
          }}>
          <View style={{flex: 1}}>
            <Text style={styles.productText}>
              {item.caption?.toUpperCase()}
            </Text>

            <ProductItem item={item} {...props} />
          </View>
        </View>
      )}
    />
  );
};

const styles = StyleSheet.create({
  productText: {
    paddingLeft: wp(2),
    fontSize: hp(2.3),
    fontWeight: 'bold',
    color: 'grey',
  },
});

export default ProductList;
