import React from 'react';
import {TextInput, View, Text} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from '../Font';

const Input = ({
  placeholder,
  label,
  value,
  onChangeText,
  onFocus,
  secureTextEntry,
  keyboardType,
  style,
  costumInputStyle,
}) => {
  const {inputStyle, containerStyle, labelStyle} = styles;

  return (
    <View style={[containerStyle, style]}>
      {/* <Text style={labelStyle}>{label}</Text> */}
      <TextInput
        keyboardType={keyboardType || 'default'}
        secureTextEntry={secureTextEntry}
        autoCorrect={false}
        placeholder={placeholder}
        style={[inputStyle, costumInputStyle]}
        value={value}
        onChangeText={onChangeText}
        onFocus={onFocus}
        onTouchStart={onFocus}
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: '#000',
    fontSize: hp('2.5%'),
    // width: '100%',
    marginTop: 0,
    // height:38,
    height: hp('6%'),
    fontFamily: Font.RobotoRegular,
  },

  labelStyle: {
    fontSize: 16,
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    lineHeight: 16,
  },

  containerStyle: {
    paddingLeft: 15,
    backgroundColor: '#fff',
    // borderRadius:50,
    width: '80%',
  },
};

// const styles = {
//     inputStyle:{
//         color: '#000',
//         fontSize:hp('2%'),
//         width:'100%',
//         marginTop:0,
//         // height:38,
//         // borderWidth:1,
//         height:hp('6%'),

//     },

//     labelStyle: {
//         fontSize: hp('1.8%'),
//         marginTop:5,
//         padding:0,
//         margin:0,
//         marginLeft:4

//     },

//     containerStyle: {
//         paddingLeft: 15,
//         backgroundColor:'#fff',
//         borderRadius:50,
//         width:'80%'
//     }
// }

export {Input};
