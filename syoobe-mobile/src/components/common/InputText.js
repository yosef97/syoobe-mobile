import React from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const InputText = ({
  icon,
  colorIcon = '#636e72',
  label,
  numberOfLines = 1,
  multiline = false,
  onChangeText,
  value,
  keyboardType,
  style,
  onFocus,
  placeholder,
  editable = true,
  Component = Icon,
  secureTextEntry,
}) => {
  return (
    <View style={{flex: 1}}>
      {label && <Text>{label}</Text>}
      <View style={[styles.textInputContainer, styles.shadow]}>
        <TextInput
          secureTextEntry={secureTextEntry}
          value={value}
          onChangeText={(e) => onChangeText(e)}
          keyboardType={keyboardType}
          multiline={multiline}
          numberOfLines={numberOfLines}
          onFocus={onFocus}
          editable={editable}
          placeholder={placeholder}
          // onMagicTap={onFocus}
          style={[
            style,
            styles.textInput,
            {textAlignVertical: multiline ? 'top' : null},
          ]}
        />
        {icon && (
          <Component
            name={icon}
            size={20}
            color={colorIcon}
            onPress={onFocus}
            style={{
              position: 'absolute',
              right: 10,
              top: hp(1.9),
              backgroundColor: '#fff',
              paddingHorizontal: wp(1),
            }}
          />
        )}
      </View>
      {/* <Text style={{ fontSize: 12, color: 'red' }}>{props.errors}</Text> */}
    </View>
  );
};

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#eaeaea',
    shadowOffset: {
      width: 15,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 12.22,

    elevation: 4,
  },
  textInput: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    borderRadius: 5,
    width: '100%',
    position: 'relative',
    zIndex: -99,
  },
  textInputContainer: {
    borderRadius: 5,
    width: '100%',
    marginVertical: hp(1),
    zIndex: -1,
  },
});

// InputText.defaultProps = {
//   colorIcon: '#636e72',
// }

export default InputText;
