import React from "react";
import { TextInput, View, Text } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { Font } from "../Font";

const TextareaInner = ({
  placeholder,
  message,
  label,
  value,
  onChangeText,
  secureTextEntry
}) => {
  const { inputStyle, containerStyle, labelStyle } = styles;

  return (
    <View style={containerStyle}>
      <View style={{ flexDirection: "row" }}>
        <Text style={labelStyle}>{label}</Text>
        {message == "not-mandatory" ? null : (
          <Text style={{ color: "#f00", fontSize: 18, marginTop: 5 }}> *</Text>
        )}
      </View>
      <View style={inputStyle}>
        <TextInput
          secureTextEntry={secureTextEntry}
          autoCorrect={false}
          placeholder={placeholder}
          onChangeText={onChangeText}
          multiline={true}
          numberOfLines={3}
          defaultValue={value}
        />
      </View>
    </View>
  );
};

const styles = {
  inputStyle: {
    height: hp("10%"),
    color: "#000",
    paddingLeft: 20,
    fontSize: wp("4%"),
    width: "100%",
    marginTop: 0,
    backgroundColor: "#fff",
    borderRadius: 20,
    // elevation: 10,
    // height:hp('8%'),
    // marginLeft:2,
    // marginRight:2,
    fontFamily: Font.RobotoRegular,
    borderWidth: 1,
    borderColor: "#cfcdcd"
  },

  labelStyle: {
    fontSize: wp("4%"),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 10,
    fontFamily: Font.RobotoRegular
  },

  containerStyle: {
    backgroundColor: "#fff",
    // borderRadius:50,
    width: "100%"
  }
};

export { TextareaInner };
