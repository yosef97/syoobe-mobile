import Axios from 'axios';
import React, {createRef, useState} from 'react';
import {FlatList, ScrollView, StyleSheet, Text, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {env} from '../../../../appconfig';
import {Button} from '../Button';

export const TrackingProduct = ({
  waybill,
  courier,
  title = 'Lacak Pengiriman',
}) => {
  const sheetRef = createRef();
  const [tracks, setTracks] = useState([]);
  const setTitle = title;
  const config = {
    headers: {
      key: env.SHIPPING.api_key,
    },
  };

  const handleTracks = () => {
    sheetRef.current.open();
    Axios.post(
      `${env.SHIPPING.url}/waybill`,
      {
        waybill,
        courier,
      },
      config,
    )
      .then((res) => {
        setTracks(res.data.rajaongkir.result);
      })
      .catch((err) => console.log(err.response));
  };
  return (
    <View>
      <Button onPress={() => handleTracks()}>Lacak</Button>
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(100)}
        customStyles={card}>
        <View style={{flex: 1, backgroundColor: '#fff'}}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerContainerTitle}>{setTitle}</Text>
          </View>
          <ScrollView style={{flex: 1, marginBottom: hp(3)}}>
            <View style={styles.itemListContainer}>
              {tracks.delivered && (
                <TrackItem
                  index={0}
                  item={{
                    manifest_date: tracks.delivery_status.pod_date,
                    manifest_description: `${tracks.delivery_status.status} - ${tracks.delivery_status.pod_receiver}`,
                    city_name: '',
                    manifest_time: tracks.delivery_status.pod_time,
                  }}
                />
              )}
              <FlatList
                data={tracks.manifest}
                renderItem={({item, index}) => (
                  <TrackItem
                    key={index}
                    index={tracks.delivered ? null : index}
                    item={item}
                  />
                )}
              />
            </View>
          </ScrollView>
        </View>
      </RBSheet>
    </View>
  );
};

const TrackItem = ({item, index}) => {
  return (
    <View style={{flex: 1, flexDirection: 'row', paddingTop: 5}}>
      <View style={{flex: 1}}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View
            style={{
              width: 20,
              height: 20,
              borderRadius: 10,
              backgroundColor: index === 0 ? 'red' : '#353b48',
              justifyContent: 'center',
            }}
          />
          <Text
            style={{
              paddingLeft: wp(3),
              fontSize: hp(2),
              fontWeight: 'bold',
            }}>
            {`${item.manifest_date} - ${item.manifest_time}`}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
          }}>
          <View
            style={{
              width: 20,
              height: null,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 3,
                height: null,
                backgroundColor: index === 0 ? 'red' : '#353b48',
                paddingVertical: hp(2),
                alignItems: 'center',
              }}
            />
          </View>
          <Text
            style={{
              flex: 1,
              paddingLeft: wp(3),
              paddingTop: hp(1),
            }}>
            {item.manifest_description}[{item.city_name}]
          </Text>
        </View>
      </View>
    </View>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 10,
    borderBottomColor: '#f5f6fa',
  },

  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },

  itemListContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
    marginTop: hp(1),
    height: '100%',
    // Bottom: hp(2),
  },
});
