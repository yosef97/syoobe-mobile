import React, {createRef} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Entypo from 'react-native-vector-icons/Entypo';

export const PopUpInfo = ({message, title = 'Informasi'}) => {
  const sheetRef = createRef();

  const setTitle = title;
  return (
    <View>
      <TouchableOpacity
        onPress={() => sheetRef.current.open()}
        activeOpacity={0.7}>
        <Entypo name={'info-with-circle'} size={hp(2)} color={'#353b48'} />
        {/* <View style={styles.dropdown}>
          <Text style={{color: '#000', fontSize: hp(2)}}>{name}</Text>
          <FontAwesome5Icon name={'chevron-down'} size={hp(2)} />
        </View> */}
      </TouchableOpacity>
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(40)}
        customStyles={card}>
        <View style={{flex: 1, backgroundColor: '#fff'}}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerContainerTitle}>{setTitle}</Text>
          </View>
          <ScrollView style={{flex: 1}}>
            <View style={styles.itemListContainer}>
              <Text>{message}</Text>
            </View>
          </ScrollView>
        </View>
      </RBSheet>
    </View>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 10,
    borderBottomColor: '#f5f6fa',
  },

  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },

  itemListContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
    marginTop: hp(1),
    height: '100%',
    // Bottom: hp(2),
  },
});
