import React, {createRef} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import {randomColor} from '../../../helpers/helper';

export const ItemActionButton = ({
  onPress,
  onDownload,
  onAcceptRequest,
  onSendRevisionRequest,
  disableAccept,
  disableRevision,
  productions,
  downloadDisabled,
  value,
}) => {
  const sheetRef = createRef();
  const items = [
    {
      name: 'Unduh',
      value: 'D',
      onItemPress: onDownload,
      disabled: downloadDisabled,
      icon: 'download',
      productions: true,
    },
    {
      name: 'Terima Pengajuan',
      value: 'A',
      onItemPress: onAcceptRequest,
      disabled: disableAccept,
      icon: 'check-circle',
      productions: !productions,
    },
    {
      name: 'Ajukan Revisi',
      value: 'R',
      onItemPress: onSendRevisionRequest,
      disabled: disableRevision,
      icon: 'file-plus',
      productions: !productions,
    },
  ];
  const setTitle = 'Aksi';
  return (
    <View>
      <TouchableOpacity
        onPress={() => sheetRef.current.open()}
        activeOpacity={0.7}>
        <Entypo name={'dots-three-vertical'} size={hp(2)} color={'#353b48'} />
        {/* <View style={styles.dropdown}>
          <Text style={{color: '#000', fontSize: hp(2)}}>{name}</Text>
          <FontAwesome5Icon name={'chevron-down'} size={hp(2)} />
        </View> */}
      </TouchableOpacity>
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(40)}
        customStyles={card}>
        <View style={{flex: 1}}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerContainerTitle}>{setTitle}</Text>
          </View>
          <View style={styles.itemListContainer}>
            <FlatList
              data={items}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => {
                if (item.productions) {
                  return (
                    <CardItemList
                      key={index}
                      icon={item.icon}
                      onPress={() => item.onItemPress({ref: sheetRef.current})}
                      title={item.name}
                      disabled={item.disabled}
                      style={{
                        // borderWidth: 1,
                        marginBottom: index === items.length - 1 ? hp(2) : 0,
                      }}
                    />
                  );
                }
              }}
            />
          </View>
        </View>
      </RBSheet>
    </View>
  );
};

const CardItemList = ({style, title, onPress, disabled = false, icon}) => {
  return (
    <TouchableOpacity onPress={!disabled ? onPress : null}>
      <View
        style={[
          styles.cardItemList,
          style,
          {backgroundColor: disabled ? '#ecf0f1' : '#fff'},
        ]}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View
              style={{
                width: wp(10),
                height: hp(5),
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: randomColor(),
                borderRadius: hp(2.5),
              }}>
              <Feather name={icon} size={hp(3)} color={'#fff'} />
            </View>
            <Text
              style={{fontSize: hp(2), fontWeight: 'bold', paddingLeft: wp(3)}}>
              {title}
            </Text>
          </View>
          {/* <FontAwesome5Icon name={'chevron-right'} size={hp(2)} /> */}
        </View>
      </View>
    </TouchableOpacity>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  dropdown: {
    paddingVertical: hp(1.5),
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingHorizontal: wp(3),
    // marginBottom: hp(2),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#f5f6fa',
    borderWidth: 1,
  },
  cardItemList: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingVertical: hp(1),
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    borderRadius: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),
    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});
