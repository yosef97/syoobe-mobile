import {formatRupiah} from '../../../helpers/helper';
import React from 'react';
import {Image, Text, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

export const ProductItemCard = (props) => {
  const {data, is_free_shipping, product_type, shop} = props;
  // console.log('ini proos', JSON.stringify(props));
  // console.log('data', JSON.stringify(data));

  return (
    <View
      style={{
        backgroundColor: '#fff',
        paddingVertical: hp(2),
        paddingHorizontal: wp(3),
        marginBottom: hp(2),
      }}>
      {shop && (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingBottom: hp(1),
          }}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{fontSize: hp(2)}}>Dijual Oleh: </Text>
            <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
              {data.product_sold_by}
            </Text>
          </View>
          <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
        </View>
      )}
      <View
        style={{
          paddingTop: hp(1),
          flexDirection: 'row',
          borderTopColor: '#f5f6fa',
          borderTopWidth: 1,
        }}>
        <View
          style={{
            width: wp(22),
            height: hp(11),
            maxWidth: wp(22),
            maxHeight: hp(11),
            backgroundColor: '#dcdde1',
          }}>
          <Image
            style={{width: '100%', height: '100%'}}
            resizeMode="cover"
            source={{uri: data.product_image}}
          />
        </View>
        <View style={{flex: 1, paddingHorizontal: wp(3)}}>
          <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
            {data.product_name}
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                paddingTop: hp(1),
                fontSize: hp(2),
                fontWeight: 'bold',
                color: '#e84118',
              }}>
              Rp. {formatRupiah(data.product_unit_price)} {'  '} (
              {data.product_quantity})
            </Text>
            {/* <Text
              style={{
                paddingTop: hp(1),
                fontSize: hp(2),
                fontWeight: 'bold',
                color: '#000',
              }}>
              {` x${data.product_quantity}`}
            </Text> */}
          </View>
          {is_free_shipping && (
            <View style={{padding: 5}}>
              <Image
                resizeMode="cover"
                style={{width: 35, height: 25}}
                source={require('../../../images/free_shipping.png')}
              />
            </View>
          )}
          {product_type == 'I' && (
            <View style={{padding: 5}}>
              <Image
                style={{width: 25, height: 25}}
                resizeMode="cover"
                source={require('../../../images/download.png')}
              />
            </View>
          )}
          {product_type == 'G' && (
            <View style={{padding: 5}}>
              <Image
                style={{width: 25, height: 25}}
                resizeMode="cover"
                source={require('../../../images/graphic_design.png')}
              />
            </View>
          )}
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          borderTopColor: '#f5f6fa',
          borderTopWidth: 2,
          marginTop: hp(2),
        }}>
        <Text
          style={{
            paddingTop: hp(1),
            fontSize: hp(2),
            fontWeight: 'bold',
          }}>
          Total Harga
        </Text>
        <Text
          style={{
            paddingTop: hp(1),
            fontSize: hp(2),
            fontWeight: 'bold',
            color: '#e84118',
          }}>
          Rp. {formatRupiah(data.cart_total)}
        </Text>
      </View>
    </View>
  );
};

export const Section = ({style, children}) => {
  return (
    <View
      style={{
        paddingVertical: hp(2),
        paddingHorizontal: wp(3),
        backgroundColor: '#fff',
        marginBottom: hp(2),
        ...style,
      }}>
      {children}
    </View>
  );
};
