import React from 'react';
import {StyleSheet, TextInput, View, Text} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export const TextInputCostum = ({
  onChangeText,
  value,
  multiline = false,
  numberOfLines = 1,
  onFocus,
  label,
  placeholder,
  keyboardType,
  editable,
  style,
}) => {
  return (
    <View style={{marginTop: hp(2)}}>
      {label && <Text style={{paddingBottom: 5}}>{label}</Text>}
      <TextInput
        onChangeText={onChangeText}
        value={value}
        multiline={multiline}
        numberOfLines={numberOfLines}
        onFocus={onFocus}
        placeholder={placeholder}
        editable={editable}
        style={[
          styles.formInput,
          {textAlignVertical: multiline ? 'top' : null},
        ]}
        keyboardType={keyboardType}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  formInput: {
    paddingVertical: hp(1),
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingHorizontal: wp(3),

    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#8395a7',
    borderWidth: 1,
  },
});
