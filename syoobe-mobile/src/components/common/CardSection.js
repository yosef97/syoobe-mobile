import React from 'react';
import {View} from 'react-native';

const CardSection = (props) => {
  return (
    <View style={[styles.containerStyles, props.style]}>{props.children}</View>
  );
};

const styles = {
  containerStyles: {
    // justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
    width: '100%',
    backgroundColor: 'transparent',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 15,
  },
};

export {CardSection};
