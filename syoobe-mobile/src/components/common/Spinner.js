import React from 'react';
import {Text, View, ActivityIndicator} from 'react-native';
import {Font} from '../Font';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Spinner = ({text, secondText, size, color}) => {
  return (
    <View style={styles.spiinerStyle}>
      <ActivityIndicator color={color || 'white'} size={size || 'large'} />
      <Text style={styles.textStyle}> {text && {text}}</Text>
      <Text style={styles.textStyle}> {secondText && {secondText}}</Text>
    </View>
  );
};

const styles = {
  spiinerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  textStyle: {
    fontSize: hp('2%'),
    color: '#302e2a',
    fontFamily: Font.RobotoRegular,
    padding: 10,
    width: '100%',
    textAlign: 'center',
  },
};

export {Spinner};
