import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({children, onPress}) => {

    const { buttonStyle, textStyle } = styles

    return (
        <TouchableOpacity onPress={onPress} style={buttonStyle}>
            <Text style={textStyle}>
            {children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    textStyle: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 20,
    },

    buttonStyle:{
        
        alignSelf: 'stretch',
        backgroundColor: '#2cc0ff',
        borderRadius:50,
        paddingTop: 12,
        paddingBottom:12,
        fontSize:20,
        
    }
}

export {Button};


