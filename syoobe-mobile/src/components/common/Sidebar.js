import { DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import React from 'react'
import {
  Animated,
  Easing,
  ScrollView,
  SafeAreaView,
  Text,
  StyleSheet,
  View,
  ImageBackground,
  Image,
  TouchableOpacity
} from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import { DrawerItems } from '@react-navigation/drawer'
import { Font } from "../Font";

const Sidebar = (props) => {
  console.log(props)
  return (
    <View style={{ flex: 1 }}>
      <View style={styles.topWrapper}>
        <ImageBackground
          style={styles.bgImg}
          source={require("../../images/bg1.png")}
        >
          <ImageBackground
            style={styles.profilePicOuter}
            source={require("../../images/profileBorder.png")}
          >
            <View style={styles.profileImgDiv}>
              <Image
                style={styles.profileImg}
                source={require("../../images/no-image.png")}
              />
            </View>
          </ImageBackground>
        </ImageBackground>
        <View style={styles.bottomWrapper}>
          <ImageBackground
            style={styles.bottomBg}
            resizeMode="contain"
            source={require("../../images/bg2.png")}
          ></ImageBackground>
        </View>
      </View>
      <DrawerContentScrollView {...props}>
        <SafeAreaView
          style={{ flex: 1 }}
          forceInset={{ top: "always", horizontal: "never" }}
        >
          <DrawerItemList {...props} />
        </SafeAreaView>
      </DrawerContentScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  topWrapper: {
    textAlign: "center"
  },
  bgImg: {
    height: hp("25%"),
    width: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  profilePicOuter: {
    padding: 5
  },
  profileImgDiv: {
    position: "relative"
  },

  profileImg: {
    width: wp("20%"),
    height: wp("20%"),
    borderWidth: 3,
    borderColor: "#f00",
    borderRadius: wp("27%"),
    marginLeft: "auto",
    marginRight: "auto"
  },
  profileTitle: {
    color: "#ffffff",
    fontSize: wp("5%"),
    fontFamily: Font.RobotoRegular
  },
  emailTxt: {
    color: "#ffffff",
    fontSize: wp("4.5%"),
    fontFamily: Font.RobotoRegular
  },
  bottomWrapper: {
    height: hp("3.4%")
  },
  bottomBg: {
    width: "100%",
    height: "100%",
    marginBottom: 20
  }
});

export default Sidebar;
