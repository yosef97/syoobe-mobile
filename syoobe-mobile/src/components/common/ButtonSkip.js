import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Font } from '../Font';

const ButtonSkip = ({children, onPress}) => {

    const { buttonStyle, textStyle } = styles

    return (
        <TouchableOpacity onPress={onPress} style={buttonStyle}>
            <Text style={textStyle}>
            {children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    textStyle: {
        alignSelf: 'center',
        textAlign: 'center',
        color: '#fff',
        fontSize: hp('2%'),
        fontFamily:Font.RobotoRegular,
    },

    buttonStyle:{
        alignSelf: 'stretch',
        backgroundColor: '#00b3ff',
        borderRadius:50,
        paddingTop: hp('1.5%'),
        paddingBottom:hp('1.5%'),
        width:wp('40%'),
        marginLeft:'auto',
        marginRight:'auto',
        marginTop:hp('2%')
    }
}

export {ButtonSkip};
