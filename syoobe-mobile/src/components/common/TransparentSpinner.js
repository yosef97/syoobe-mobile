import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const TransparentSpinner = ({size, color}) => {
    return (
        <View style={styles.spiinerStyle}>
            <ActivityIndicator color={color || 'white'} size={size || 'large'} />
        </View>
    );
};

const styles = {
    spiinerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
}

export { TransparentSpinner };