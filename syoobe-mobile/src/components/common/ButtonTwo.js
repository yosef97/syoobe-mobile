import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from '../Font';

const ButtonTwo = ({children, onPress, download_enabled, view}) => {
  const {buttonStyle, textStyle} = styles;

  if (view) {
    return (
      <View disabled={download_enabled} onPress={onPress} style={buttonStyle}>
        <Text style={textStyle}>{children}</Text>
      </View>
    );
  } else {
    return (
      <TouchableOpacity
        disabled={download_enabled}
        onPress={onPress}
        style={buttonStyle}>
        <Text style={textStyle}>{children}</Text>
      </TouchableOpacity>
    );
  }
};

const styles = {
  enabled: {
    opacity: 1,
  },
  disabled: {
    opacity: 0.3,
  },
  textStyle: {
    alignSelf: 'center',
    textAlign: 'center',
    color: '#fff',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
  },

  buttonStyle: {
    alignItem: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    borderRadius: 50,
    paddingTop: hp('1.5%'),
    paddingBottom: hp('1.5%'),
    paddingHorizontal: hp('1.5%'),
    maxWidth: '70%',
    minWidth: '40%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: hp('2%'),
    backgroundColor: '#c90305',
  },
};

export {ButtonTwo};
