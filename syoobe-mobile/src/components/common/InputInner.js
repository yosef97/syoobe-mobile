import React from "react";
import { Dimensions, TextInput, View, Text } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

let inputFontSize = {};
if (Dimensions.get("window").height <= 550) {
  inputFontSize = hp("2.85%");
} else if (
  Dimensions.get("window").height > 550 &&
  Dimensions.get("window").height <= 600
) {
  inputFontSize = hp("2.5%");
} else if (Dimensions.get("window").height > 770) {
  inputFontSize = hp("2%");
} else {
  inputFontSize = hp("2.3%");
}

const InputInner = ({
  funds,
  card,
  labelon,
  editable,
  keyboardType,
  placeholder,
  message,
  label,
  value,
  onChangeText,
  onFocus,
  secureTextEntry
}) => {
  const {
    inputStyle3,
    inputStyle2,
    inputStyle,
    containerStyle,
    labelStyle
  } = styles;
  return (
    <View style={containerStyle}>
      {labelon != false && (
        <View style={{ flexDirection: "row" }}>
          <Text style={labelStyle}>
            {label}
            {message != "not-mandatory" && (
              <Text
                style={{ color: "#f00", fontSize: hp("2.2%"), marginTop: 5 }}
              >
                {" "}
                *
              </Text>
            )}
          </Text>
        </View>
      )}
      <TextInput
        editable={editable}
        keyboardType={keyboardType}
        secureTextEntry={secureTextEntry}
        autoCorrect={false}
        placeholder={placeholder}
        style={card ? inputStyle2 : funds ? inputStyle3 : inputStyle}
        defaultValue={value}
        onChangeText={onChangeText}
        onFocus={onFocus}
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: "#000",
    paddingLeft: 20,
    fontSize: inputFontSize,
    width: "100%",
    marginTop: 0,
    backgroundColor: "#fff",
    borderRadius: 50,
    height: hp("7%"),
    borderWidth: 1,
    borderColor: "#cfcdcd"
  },

  inputStyle2: {
    color: "#000",
    paddingLeft: 20,
    fontSize: inputFontSize,
    width: "100%",
    marginTop: 0,
    backgroundColor: "#fff",
    borderRadius: 50,
    height: hp("6%"),
    borderWidth: 1,
    borderColor: "#cfcdcd"
  },

  inputStyle3: {
    height: 40,
    borderRadius: 10,
    width: "100%",
    borderColor: "gray",
    borderWidth: 1
  },
  labelStyle: {
    fontSize: hp("2%"),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 10
  },

  containerStyle: {
    backgroundColor: "#fff",
    // borderRadius:50,
    width: "100%"
  }
};

export { InputInner };
