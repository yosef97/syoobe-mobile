import React from 'react';
import { Text,View,Image, TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Font} from '../Font'

const SocialIconComponent = (props) => {

    const { buttonStyle, textStyle } = styles 

    return (
        <View style={styles.menuWrapper}> 
            <TouchableOpacity onPress={props.shareFb} style={styles.menuItem}>
                <Image style={styles.fb} source={require('../../images/fbblue.png')}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={props.shareTwitter} style={styles.menuItem}>
                <Image style={styles.twit} source={require('../../images/twit.png')}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={props.sharePinterest} style={styles.menuItem}>
                <Image style={styles.pinterest} source={require('../../images/pintarest.png')}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={props.shareMail} style={styles.menuItem}>
                <Image style={styles.msg} source={require('../../images/msg2.png')}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={props.shareWhatsApp} style={styles.menuItem}>
                <Image style={styles.whatsapp} source={require('../../images/whatsapp.png')}/>
            </TouchableOpacity>
        </View>
    );
}; 

const styles = {
    menuWrapper:{
        width:wp('20%'),
        backgroundColor:'#fff',
        position:'absolute',
        top:wp('14%'),
        zIndex: 1001,
        right:10,
        // borderRadius:10,
        elevation:5,
        justifyContent:'center',
        alignItems:'center'
        
    },
    menuItem:{
        padding:10,
        color:'#7d7d7d',
        borderWidth:1,
        borderColor:'#e1e1e1',
        width:'100%',
        textAlign:'center',
        alignItems:'center'
    },
    textStyle:{
        color:'#7d7d7d',
        fontFamily:Font.RobotoRegular,
        fontSize:wp('4%'),
    },
    fb:{
        width:10,
        height:20
    },
    twit:{
        width:20,
        height:20
    },
    pinterest:{
        width:17,
        height:20
    },
    msg:{
        width:28,
        height:18
    },
    whatsapp:{
        width:20,
        height:20
    }
    
}

export {SocialIconComponent};
