import {
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';
import React, {Component} from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  Image,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {connect} from 'react-redux';
import {emptyNotifications, getProfileDetails, logOutUser} from '../../actions';
// import { DrawerItems } from '@react-navigation/drawer'
import {Font} from '../Font';
import Bugsnag from '@bugsnag/react-native';
import {GoogleSignin} from '@react-native-community/google-signin';

class SidebarSecure extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      name: '',
      image: '',
      loadedTimes: 1,
      _token: null,
    };
  }

  removeToken = async () => {
    try {
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('fcmToken');
      await AsyncStorage.removeItem('deviceInfo');
    } catch (error) {
      console.log(error);
    }
  };

  removeShopId = async () => {
    try {
      await AsyncStorage.removeItem('shop_id');
    } catch (error) {
      console.log(error);
    }
  };

  removeImageUrl = async () => {
    try {
      await AsyncStorage.removeItem('image');
    } catch (error) {
      console.log(error);
    }
  };

  removeUsername = async () => {
    try {
      await AsyncStorage.removeItem('name');
    } catch (error) {
      console.log(error);
    }
  };

  removeEmail = async () => {
    try {
      await AsyncStorage.removeItem('email');
    } catch (error) {
      console.log(error);
    }
  };

  removeStore = async () => {
    try {
      await AsyncStorage.removeItem('emailStore');
      await AsyncStorage.removeItem('passwordStore');
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
  };

  requestLogOut = async () => {
    console.log('requestLogOut');
    this.removeStore();
    this.removeUsername();
    this.removeImageUrl();
    this.removeEmail();
    this.removeShopId();
    GoogleSignin.revokeAccess();
    GoogleSignin.signOut();
    GoogleSignin.clearCachedAccessToken(this.setState({userInfo: null}));
    await this.removeToken()
      .then(() => {
        this.props.logOutUser(this.props.navigation);
        this.props.emptyNotifications();
      })
      .catch((error) => {
        Bugsnag.notify(error);
        console.log('error', error);
      });
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = () => {
    this.retrieveToken()
      .then((_token) => {
        this.props.getProfileDetails(_token);
      })
      .catch((error) => {
        console.log('Promise is rejected with error: ' + error);
      });
  };

  render() {
    console.log('ini loading', this.props.loading);
    return (
      <View style={{flex: 1}}>
        <View style={styles.topWrapper}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('EditProfile')}>
            <View>
              <ImageBackground
                style={styles.bgImg}
                source={require('../../images/bg1.png')}>
                <ImageBackground
                  style={styles.profilePicOuter}
                  source={require('../../images/profileBorder.png')}>
                  <View style={styles.profileImgDiv}>
                    <Image
                      resizeMode="cover"
                      style={styles.profileImg}
                      source={{uri: this.props.user_image_url}}
                    />
                  </View>
                </ImageBackground>
                <Text style={styles.profileTitle}>
                  {this.props.profileDetails.name}
                </Text>
                <Text style={styles.emailTxt}>
                  {this.props.profileDetails.user_email}
                </Text>
              </ImageBackground>
            </View>
          </TouchableOpacity>
          <View style={styles.bottomWrapper}>
            <ImageBackground
              style={styles.bottomBg}
              resizeMode="contain"
              source={require('../../images/bg2.png')}></ImageBackground>
          </View>
        </View>
        <DrawerContentScrollView {...this.props}>
          <SafeAreaView
            style={{flex: 1}}
            forceInset={{top: 'always', horizontal: 'never'}}>
            <DrawerItemList {...this.props} />
            {this.props.loading ? (
              <DrawerItem
                label="Keluar"
                onPress={() => null}
                icon={({size, color}) => (
                  <ActivityIndicator size={size} color="red" />
                )}
              />
            ) : (
              <DrawerItem
                label="Keluar"
                onPress={() => this.requestLogOut()}
                icon={({size, color}) => (
                  <FontAwesome5Icon
                    name={'sign-out-alt'}
                    size={size}
                    color={color}
                  />
                )}
              />
            )}
          </SafeAreaView>
        </DrawerContentScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  topWrapper: {
    textAlign: 'center',
  },
  bgImg: {
    height: hp('25%'),
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  profilePicOuter: {
    padding: 5,
  },
  profileImgDiv: {
    position: 'relative',
  },

  profileImg: {
    width: wp('20%'),
    height: wp('20%'),
    borderWidth: 3,
    borderColor: '#f00',
    borderRadius: wp('27%'),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  profileTitle: {
    color: '#ffffff',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoRegular,
  },
  emailTxt: {
    color: '#ffffff',
    fontSize: wp('4.5%'),
    fontFamily: Font.RobotoRegular,
  },
  bottomWrapper: {
    height: hp('3.4%'),
  },
  bottomBg: {
    width: '100%',
    height: '100%',
    marginBottom: 20,
  },
});
const mapStateToProps = (state) => {
  return {
    user_image_url: state.profile.user_image_url,
    profileDetails: state.profile.profile_details,
    loading: state.auth.loading,
    loggedIn: state.auth.loggedIn,
    registered: state.auth.registered,
    toast: state.auth.toast,
    image: state.auth.loggedInUserImage,
    email: state.auth.loggedInEmail,
    name: state.auth.loggedInUserName,
  };
};

export default connect(mapStateToProps, {
  emptyNotifications,
  getProfileDetails,
  logOutUser,
})(SidebarSecure);
