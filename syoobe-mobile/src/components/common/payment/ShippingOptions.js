import React, {createRef} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {RadioButton} from 'react-native-paper';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {Spinner} from '../Spinner';
import {formatRupiah} from '../../../helpers/helper';

export const ShippingOptions = (props) => {
  const {
    options,
    selectedOption,
    onSelectOption,
    onSelectOptionService,
    selectedOptionService,
    parentIndex,
    productId,
    isLoading,
    optionsChild,
    companyName,
  } = props;
  const sheetRef = createRef();
  const RefChild = createRef();
  console.log('shippingoption');
  const getSelected = (item) => {
    const data =
      selectedOption &&
      selectedOption.filter(
        (option) =>
          option.product_id === productId &&
          option.company_id === item.company_id,
      );

    return data[0]?.company_id === item.company_id ? 'checked' : 'unchecked';
  };
  const getSelectedService = (item) => {
    const value = selectedOptionService.filter(
      (val) => val.value === item.pship_id && val.product_id === productId,
    );

    return value[0]?.value === item.pship_id ? 'checked' : 'unchecked';
  };
  // console.log('ini props', JSON.stringify(props));
  return (
    <View>
      <TouchableOpacity onPress={() => sheetRef.current.open()}>
        <Text style={{color: 'red'}}>
          {companyName ? companyName : 'Pilih Kurir'}
        </Text>
      </TouchableOpacity>
      <RBSheet
        ref={RefChild}
        closeOnPressBack
        closeOnDragDown={false}
        closeOnPressMask={true}
        height={hp(50)}
        customStyles={{
          ...card,
          wrapper: {
            backgroundColor: 'transparent',
          },
        }}>
        <View style={{flex: 1}}>
          <View
            style={{
              ...styles.headerContainer,
              // flexDirection: 'row',
            }}>
            <View
              style={{
                position: 'absolute',
                left: wp(5),
                top: hp(2),
                bottom: hp(2),
              }}>
              <FontAwesome5Icon
                name={'chevron-left'}
                size={hp(3)}
                onPress={() => RefChild.current.close()}
              />
            </View>
            <Text style={styles.headerContainerTitle}>Pilih Service</Text>
          </View>
          <View style={styles.itemListContainer}>
            <FlatList
              data={optionsChild?.options}
              scrollEnabled
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => (
                <View
                  key={item.pship_id}
                  style={{
                    ...styles.itemList,
                    marginBottom:
                      index === optionsChild?.options.length - 1 ? hp(1) : 0,
                  }}>
                  <View>
                    <Text style={styles.itemListText}>
                      {`${item.pship_service} - ${
                        item.pship_duration.length ? item.pship_duration : 0
                      }`}
                    </Text>
                    <Text style={{...styles.itemListText, fontSize: hp(2)}}>
                      {`Rp. ${formatRupiah(item.pship_value)}`}
                    </Text>
                  </View>
                  <RadioButton
                    value={item.pship_id}
                    status={getSelectedService(item)}
                    onPress={(value) =>
                      onSelectOptionService({
                        index: parentIndex,
                        product_id: productId,
                        value: item.pship_id,
                        item,
                        options: optionsChild?.options,
                        company_id: optionsChild?.company_id,
                        refParent: sheetRef.current,
                        refChild: RefChild.current,
                      })
                    }
                  />
                </View>
              )}
            />
          </View>
        </View>
      </RBSheet>
      <RBSheet
        ref={sheetRef}
        closeOnDragDown={false}
        closeOnPressMask={true}
        height={hp(50)}
        customStyles={card}>
        {isLoading ? (
          <Loading loading={isLoading} />
        ) : (
          <View style={{flex: 1, height: 'auto', width: 'auto'}}>
            <View style={styles.headerContainer}>
              <Text style={styles.headerContainerTitle}>Pilih Kurir</Text>
            </View>
            <View style={styles.itemListContainer}>
              <FlatList
                data={options}
                scrollEnabled
                showsVerticalScrollIndicator={false}
                renderItem={({item, index}) => (
                  <View
                    key={item.company_id}
                    style={{
                      ...styles.itemList,
                      marginBottom: index === options.length - 1 ? hp(1) : 0,
                    }}>
                    <Text style={styles.itemListText}>{item.company_name}</Text>
                    <RadioButton
                      value={item.company_id}
                      status={getSelected(item)}
                      onPress={() =>
                        onSelectOption(
                          item.company_service_code,
                          parentIndex,
                          productId,
                          item.company_id,
                          sheetRef.current,
                          RefChild.current,
                        )
                      }
                    />
                  </View>
                )}
              />
            </View>
          </View>
        )}
      </RBSheet>
    </View>
  );
};

const Loading = (loading) => {
  if (loading) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Spinner color="red" size="large" />
      </View>
    );
  }
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    // height:
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    // flex: 1,
    width: 'auto',
    paddingHorizontal: wp(2),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
    // alignItems:'c'
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),

    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});
