import React, {createRef, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {formatRupiah} from '../../../helpers/helper';
import {Button} from '../Button';
import {data} from './payment_list';

export const PaymentOptions = ({
  onPress,
  onPressPayment,
  productData,
  isCouponApplied,
  coupon_value,
  available_reward_point,
  reward_data_applied,
  rewardsCouldbeApplied,
  reward_point_message,
  payable_amount,
  product_type = 'P',
  balance,
}) => {
  const sheetRef = createRef();
  const [items, setItem] = useState(data);
  const [subItem, setSubItem] = useState(false);
  const [title, setTitle] = useState('Pilih Metode Pembayaran');

  const handleSetItem = (item, title) => {
    setTimeout(() => {
      setItem(item);
      setSubItem(!subItem);
      setTitle(title);
    }, 200);
  };

  const onNavigate = (params) => {
    // console.log(params);
    params.route
      ? onPressPayment(params.route, {product_data: productData, ...params})
      : null;
    sheetRef.current.close();
  };

  const getPaymentList = (data) => {
    if (product_type === 'W') {
      return data.filter((val) => val.payment_type !== 'wallet');
    }
    return data;
  };
  return (
    <View>
      <Button onPress={() => onPress({ref: sheetRef.current})}>
        Pembayaran
      </Button>
      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={false}
        closeOnPressMask={true}
        height={hp(100)}
        customStyles={card}>
        <View style={{flex: 1}}>
          <View style={styles.headerContainer}>
            <TouchableOpacity
              style={styles.headerContent}
              onPress={() => {
                subItem
                  ? handleSetItem(data, 'Pilih Metode Pembayaran')
                  : sheetRef.current.close();
              }}>
              <View>
                <FontAwesome5Icon name={'chevron-left'} size={hp(3)} />
              </View>
            </TouchableOpacity>
            <Text style={styles.headerContainerTitle}>{title}</Text>
          </View>
          <View style={styles.itemListContainer}>
            <FlatList
              data={getPaymentList(items)}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => (
                <CardItemList
                  key={index}
                  disabled={item.wallet ? balance < payable_amount : false}
                  isWallet={item.wallet}
                  balance={balance}
                  onPress={(e) =>
                    item.sub_list
                      ? handleSetItem(item.sub_list, item.name)
                      : onNavigate({
                          route: item?.route,
                          image: item.logo,
                          title: item.name,
                          isWallet: item.wallet,
                          payment_type: item.payment_type,
                          bank: item.params,
                          isCouponApplied,
                          coupon_value,
                          available_reward_point,
                          reward_data_applied,
                          rewardsCouldbeApplied,
                          reward_point_message,
                          payable_amount,
                          balance,
                          product_type,
                          // ...item?.params,
                        })
                  }
                  image={item.logo}
                  title={item.name}
                  subtitle={item.description}
                  style={{
                    borderColor: '#fff',
                    borderWidth: 2,
                    marginBottom: index === data.length - 1 ? hp(2) : 0,
                  }}
                />
              )}
            />
          </View>
        </View>
      </RBSheet>
    </View>
  );
};

const CardItemList = ({
  style,
  image,
  title,
  subtitle,
  onPress,
  disabled,
  balance,
  isWallet,
}) => {
  return (
    <TouchableOpacity disabled={disabled} onPress={onPress}>
      <View
        style={[
          styles.cardItemList,
          style,
          {backgroundColor: disabled ? '#f5f6fa' : '#fff'},
        ]}>
        <View
          style={{
            width: hp(7),
            height: hp(5),
            maxWidth: hp(8),
            maxHeight: hp(5),
          }}>
          <Image
            resizeMode="contain"
            style={{width: '100%', height: '100%'}}
            source={image}
          />
        </View>
        <View
          style={{
            flex: 1,
            paddingLeft: wp(5),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View>
            <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>{title}</Text>
            {isWallet && <Text>Rp. {formatRupiah(parseInt(balance))}</Text>}
          </View>
          <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    // height:
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  headerContainer: {
    // flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
    // alignItems:'c'
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },
  cardItemList: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingVertical: hp(1),
    paddingHorizontal: wp(2),
    marginTop: hp(1),
    borderRadius: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),
    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});
