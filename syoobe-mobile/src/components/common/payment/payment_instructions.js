const payment_instructions = [
  {
    name: 'mandiri',
    data: [
      {
        title: 'ATM Mandiri',
        steps: [
          {
            title: 'Pada Menu Utama Pilih Bayar/Beli',
          },
          {
            title: 'Pilih Lainnya',
          },
          {
            title: 'Pilih Multi Payment',
          },
          {
            title: 'Masukkan kode perusahaan lalu tekan Benar',
          },
          {
            title: 'Masukkan Kode Pembayarannya lalu tekan Benar',
          },
          {
            title:
              'Pada halaman konfirmasi akan muncul detail pembayaran Anda. Jika informasi telah sesuai tekan Ya.',
          },
        ],
      },
      {
        title: 'Internet Banking',
        steps: [
          {
            title: '	Login ke Internet Banking Mandiri.',
          },
          {
            title: 'Pada menu utama, pilih Bayar, lalu pilih Multi Payment.',
          },
          {
            title:
              'Pilih akun Anda di Dari Rekening, kemudian di Penyedia Jasa pilih Midtrans.',
          },
          {
            title: 'Masukkan Kode Pembayaran Anda dan klik Lanjutkan.',
          },
          {
            title: 'Konfirmasi pembayaran Anda menggunakan Mandiri Token.',
          },
        ],
      },
    ],
  },
  {
    name: 'permata',
    data: [
      {
        title: 'ATM atau Internet Banking',
        steps: [
          {
            title: 'Pada menu utama, pilih Transaksi Lainnya.',
          },
          {
            title: 'Pilih Pembayaran.',
          },
          {
            title: 'Pilih Pembayaran Lainnya.',
          },
          {
            title: 'Pilih Virtual Account.',
          },
          {
            title:
              'Masukkan 16 digit No. Virtual Account yang dituju, lalu tekan Benar.',
          },
          {
            title:
              'Pada halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, nomor rekening, & nama Merchant. Jika informasi telah sesuai tekan Benar.',
          },
          {
            title: 'Pilih rekening pembayaran Anda dan tekan Benar.',
          },
        ],
      },
    ],
  },
  {
    name: 'bca',
    data: [
      {
        title: 'ATM BCA',
        steps: [
          {
            title: 'Pada menu utama, pilih Transaksi Lainnya.',
          },
          {
            title: 'Pilih Transfer.',
          },
          {
            title: 'Pilih Ke Rek BCA Virtual Account.',
          },
          {
            title:
              'Masukkan Nomor Rekening pembayaran (11 digit) Anda lalu tekan Benar.',
          },
          {
            title: 'Masukkan jumlah tagihan yang akan anda bayar.',
          },
          {
            title:
              'Pada halaman konfirmasi transfer akan muncul detail pembayaran Anda. Jika informasi telah sesuai tekan Ya.',
          },
        ],
      },
      {
        title: 'Klik BCA',
        steps: [
          {
            title: 'Pilih menu Transfer Dana.',
          },
          {
            title: 'Pilih Transfer ke BCA Virtual Account.',
          },
          {
            title:
              'Masukkan nomor BCA Virtual Account, atau pilih Dari Daftar Transfer.',
          },
          {
            title:
              'Jumlah yang akan ditransfer, nomor rekening dan nama merchant akan muncul di halaman konfirmasi pembayaran, jika informasi benar klik Lanjutkan.',
          },
          {
            title:
              'Ambil BCA Token Anda dan masukkan KEYBCA Response APPLI 1 dan Klik Submit.',
          },
          {
            title: 'Transaksi Anda selesai.',
          },
        ],
      },
      {
        title: 'm-BCA',
        steps: [
          {
            title: 'Lakukan log in pada aplikasi BCA Mobile.',
          },
          {
            title: 'Pilih menu m-BCA, kemudian masukkan kode akses m-BCA.',
          },
          {
            title: 'Pilih m-Transfer > BCA Virtual Account.',
          },
          {
            title:
              'Pilih dari Daftar Transfer, atau masukkan Nomor Virtual Account tujuan.',
          },
          {
            title: 'Masukkan jumlah yang ingin dibayarkan.',
          },
          {
            title: 'Masukkan pin m-BCA.',
          },
          {
            title:
              'Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran.',
          },
        ],
      },
    ],
  },
  {
    name: 'bni',
    data: [
      {
        title: 'ATM BNI',
        steps: [
          {
            title: 'Pada menu utama, pilih Menu Lainnya.',
          },
          {
            title: 'Pilih Transfer.',
          },
          {
            title: 'Pilih Rekening Tabungan.',
          },
          {
            title: 'Pilih Ke Rekening BNI.',
          },
          {
            title: 'Masukkan nomor virtual account dan pilih Tekan Jika Benar.',
          },
          {
            title:
              'Masukkan jumlah tagihan yang akan anda bayar secara lengkap. Pembayaran dengan jumlah tidak sesuai akan otomatis ditolak.',
          },
          {
            title:
              'Jumlah yang dibayarkan, nomor rekening dan nama Merchant akan ditampilkan. Jika informasi telah sesuai, tekan Ya.',
          },
          {
            title: '	Transaksi Anda sudah selesai.',
          },
        ],
      },
      {
        title: 'Internet Banking',
        steps: [
          {
            title: 'Masuk ke akun Internet Bangking anda.',
          },
          {
            title: 'Klik menu Transfer kemudian pilih Tambah Rekening Favorit.',
          },
          {
            title:
              'Masukkan nama, nomor rekening, dan email, lalu klik Lanjut.',
          },
          {
            title: 'Masukkan Kode Otentikasi dari token Anda dan klik Lanjut.',
          },
          {
            title:
              'Kembali ke menu utama dan pilih Transfer lalu Transfer Antar Rekening BNI.',
          },
          {
            title:
              'Pilih rekening yang telah Anda favoritkan sebelumnya di Rekening Tujuan lalu lanjutkan pengisian, dan tekan Lanjut.',
          },
          {
            title:
              '	Pastikan detail transaksi Anda benar, lalu masukkan Kode Otentikasi dan tekan Lanjut.',
          },
          {
            title: '	Transaksi Anda sudah selesai.',
          },
        ],
      },
      {
        title: 'Mobile Banking',
        steps: [
          {
            title: 'Buka aplikasi BNI Mobile Banking dan login',
          },
          {
            title: 'Pilih menu Transfer',
          },
          {
            title: 'Pilih menu Virtual Account Billing',
          },
          {
            title: 'Pilih rekening debit yang akan digunakan',
          },
          {
            title:
              'Pilih menu Input Baru dan masukkan 16 digit nomor Virtual Account',
          },
          {
            title: 'Informasi tagihan akan muncul pada halaman validasi',
          },
          {
            title:
              'Jika informasi telah sesuai, masukkan Password Transaksi dan klik Lanjut',
          },
          {
            title: 'Transaksi Anda akan diproses',
          },
        ],
      },
    ],
  },
  {
    name: 'all_bank',
    data: [
      {
        title: 'Prima',
        steps: [
          {
            title: 'Pada menu utama, pilih Transaksi Lainnya.',
          },
          {
            title: 'Pilih Transfer.',
          },
          {
            title: 'Pilih Rek Bank Lain.',
          },
          {
            title: 'Masukkan nomor 009 (kode Bank BNI) lalu tekan Benar.',
          },
          {
            title:
              'Masukkan jumlah tagihan yang akan Anda bayar secara lengkap, pembayaran dengan jumlah tidak sesuai akan otomatis ditolak.',
          },
          {
            title:
              'Masukkan 16 digit No. Rekening pembayaran lalu tekan Benar.',
          },
          {
            title:
              'Pada halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, nomor rekening & nama Merchant. Jika informasi telah sesuai tekan Benar.',
          },
        ],
      },
      {
        title: 'ATM Bersama',
        steps: [
          {
            title: 'Pada menu utama, pilih Transaksi Lainnya.',
          },
          {
            title: 'Pilih Transfer.',
          },
          {
            title: 'Pilih Antar Bank Online.',
          },
          {
            title: 'Masukkan 009 (kode Bank BNI) dan 16 digit No. Rekening.',
          },
          {
            title:
              'Masukkan jumlah tagihan yang akan Anda bayar secara lengkap, Pembayaran dengan jumlah tidak sesuai akan otomatis ditolak.',
          },
          {
            title: 'Kosongkan No. Referensi, lalu tekan Benar.',
          },
          {
            title:
              'Pada halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, nomor rekening & nama Merchant. Jika informasi telah sesuai tekan Benar.',
          },
        ],
      },
      {
        title: 'Alto',
        steps: [
          {
            title: 'Pada menu utama, pilih Transaksi Lainnya.',
          },
          {
            title: 'Pilih Transfer.',
          },
          {
            title: 'Pilih Rek. Bank Lain.',
          },
          {
            title: 'Masukkan nomor 009 (kode Bank BNI) lalu tekan Benar.',
          },
          {
            title:
              'Masukkan jumlah tagihan yang akan Anda bayar secara lengkap, Pembayaran dengan jumlah tidak sesuai akan otomatis ditolak.',
          },
          {
            title:
              'Masukkan 16 digit No. Rekening pembayaran lalu tekan Benar.',
          },
          {
            title:
              'Pada halaman konfirmasi transfer akan muncul jumlah yang dibayarkan, nomor rekening & nama Merchant. Jika informasi telah sesuai tekan Benar.',
          },
        ],
      },
    ],
  },
  {
    name: 'bca_klikbca',
    data: [
      {
        title: 'Klik BCA',
        steps: [
          {
            title: 'Kunjungi situs web KlikBCA www.klikbca.com.',
          },
          {
            title: 'Login menggunakan userID KlikBCA Anda.',
          },
          {
            title: 'Pilih Menu Pembayaran e-commerce.',
          },
          {
            title: 'Pilih Kategori Lainnya.',
          },
          {
            title: 'Pilih Nama Perusahaan.',
          },
          {
            title: 'Klik Lanjutkan.',
          },
          {
            title: 'Pilih transaksi yang ingin Anda bayar dan pilih lanjutkan.',
          },
          {
            title:
              'Konfirmasikan kembali pembayaran dengan memasukkan kunci token dan pilih kirim/lanjutkan.',
          },
        ],
      },
    ],
  },
  {
    name: 'bca_klikpay',
    data: [
      {
        title: 'BCA KlikPay',
        steps: [
          {
            title:
              'Setelah Anda menekan tombol "Bayar Sekarang", Anda akan diarahkan ke halaman pembayaran BCA KlikPay.',
          },
          {
            title:
              'Setelah Login ke Akun BCA Klikpay Anda dengan memasukkan alamat email dan password, maka akan tampil informasi data transaksi seperti nama merchant, waktu transaksi, dan jumlah yang harus dibayar. Pilihlah jenis pembayaran KlikBCA atau BCA Card untuk transaksi tersebut.',
          },
          {
            title:
              'Untuk otorisasi pembayaran dengan BCA KlikPay, tekan tombol "kirim OTP", dan Anda akan menerima kode OTP (One Time Password) yang dikirim melalui SMS ke handphone Anda. Masukkan kode OTP tersebut pada kolom yang tersedia.',
          },
          {
            title:
              'Apabila kode OTP yang Anda masukkan benar, transaksi pembayaran akan langsung diproses dan saldo rekening Anda (untuk jenis pembayaran KlikBCA) atau limit BCA Card Anda (untuk jenis pembayaran BCA Card) akan berkurang sejumlah nilai transaksi.',
          },
          {
            title:
              'Status keberhasilan transaksi Anda akan tampil pada layar transaksi dan Anda akan menerima email notifikasi.',
          },
          {
            title:
              'nformasi lebih lanjut tentang BCA KlikPay dapat hubungi Halo BCA di 1500888 atau kunjungi http://klikbca.com/KlikPay/klikpay.html',
          },
        ],
      },
    ],
  },
  {
    name: 'bri_epay',
    data: [
      {
        title: 'e-Pay BRI',
        steps: [
          {
            title:
              'Pastikan Anda telah memiliki User ID Internet Banking BRI dan sudah mendaftarkan mTOKEN sebelum melakukan pembayaran menggunakan e-Pay BRI langsung dari website kami.',
          },
          {
            title:
              'Untuk mendapatkan User ID Internet Banking BRI, silakan melakukan registrasi melalui ATM BRI terdekat.',
          },
          {
            title:
              'Untuk mendaftarkan mTOKEN, silakan melakukan registrasi finansial di cabang BRI terdekat.',
          },
          {
            title:
              'Pembayaran menggunakan e-Pay BRI akan diproses secara online, saldo rekening BRI Anda akan didebet secara otomatis sesuai jumlah pembelanjaan Anda.',
          },
          {
            title:
              'Transaksi Anda akan dibatalkan jika pembayaran tidak diselesaikan dalam 2 jam.',
          },
        ],
      },
    ],
  },
  {
    name: 'mandiri_ecash',
    data: [
      {
        title: 'Mandiri e-cash',
        steps: [
          {
            title:
              'Pastikan Anda telah mendaftarkan nomor ponsel untuk LINE Pay e-cash | mandiri e-cash.',
          },
          {
            title: 'Klik Bayar Sekarang.',
          },
          {
            title: 'Masukkan nomor ponsel yang sudah terdaftar dan PIN Anda.',
          },
          {
            title:
              'Tunggu kiriman SMS e-cash yang berisi One Time Password (OTP).',
          },
          {
            title:
              'Masukkan kode 6 angka OTP yang diterima, kemudian tekan tombol bayar untuk melakukan pembayaran.',
          },
          {
            title:
              'Jika SMS OTP belum diterima / OTP Anda kadaluarsa dalam 5 menit. Klik tombol batal untuk mengulang transaksi dan Anda belum di debet.',
          },
        ],
      },
    ],
  },
  {
    name: 'indomaret',
    data: [
      {
        title: 'Indomaret',
        steps: [
          {
            title:
              'Setelah melakukan konfirmasi pembayaran, Anda akan diberikan Kode Pembayaran yang unik.',
          },
          {
            title:
              'Salinlah Kode Pembayaran Anda dan jumlah pembayaran yang akan dibayarkan. Jangan Khawatir, Kami juga akan mengirimkan salinan Kode Pembayaran dan Instruksi Pembayaran melalui email kepada Anda.',
          },
          {
            title:
              'Pergi ke toko Indomaret terdekat dan berikanlah nomor Kode Pembayaran Anda ke kasir.',
          },
          {
            title:
              'Kasir Indomaret akan mengkonfirmasi transaksi dengan menanyakan jumlah pembayaran dan nama merchant.',
          },
          {
            title: 'Konfirmasikan pembayaran Anda ke kasir.',
          },
          {
            title:
              'Transaksi Anda berhasil! Anda akan mendapatkan email konfirmasi pembayaran dan Simpanlah struk transaksi Indomaret Anda.',
          },
        ],
      },
    ],
  },
  {
    name: 'alfamart',
    data: [
      {
        title: 'Alfamart',
        steps: [
          {
            title:
              'Setelah melakukan konfirmasi pembayaran, Anda akan diberikan Kode Pembayaran yang unik.',
          },
          {
            title:
              'Salinlah Kode Pembayaran Anda dan jumlah pembayaran yang akan dibayarkan. Jangan Khawatir, Kami juga akan mengirimkan salinan Kode Pembayaran dan Instruksi Pembayaran melalui email kepada Anda.',
          },
          {
            title:
              'Silakan pergi ke gerai Alfamart, Alfamidi, atau Dan+Dan terdekat dan berikanlah nomorKode Pembayaran Anda ke kasir.',
          },
          {
            title:
              'Kasir akan mengkonfirmasi transaksi dengan menanyakan jumlah transaksi dan nama merchant.',
          },
          {
            title: 'Konfirmasikan pembayaran Anda ke kasir.',
          },
          {
            title:
              'Transaksi Anda berhasil! Anda akan mendapatkan email konfirmasi pembayaran dan simpanlah struk transaksi Anda.',
          },
        ],
      },
    ],
  },
  {
    name: 'danamon_online',
    data: [
      {
        title: 'Danamon Online Banking',
        steps: [
          {
            title:
              'Masukkan User ID dan Password Danamon Online Banking Anda dan pilih sumber rekening untuk pembayaran Anda.',
          },
          {
            title:
              'Cek rincian informasi transaksi Anda, masukkan Kode Token, lalu tekan lanjut.',
          },
          {
            title:
              'Konfirmasi transaksi pembayaran akan muncul, pembayaran selesai. Simpan informasi referensi merchant dan referensi pembayaran, tekan Lanjut untuk kembali ke situs merchant.',
          },
        ],
      },
    ],
  },
  {
    name: 'akulaku',
    data: [
      {
        title: 'Akulaku',
        steps: [
          {
            title:
              'Tekan tombol "Bayar Sekarang", lalu Anda akan diarahkan ke halaman Pusat Pembayaran Akulaku.',
          },
          {
            title:
              'Pilih tenor cicilan yang diinginkan, lalu Login ke akun Akulaku Anda dengan memasukkan nomor ponsel dan password.',
          },
          {
            title:
              'Masukkan kode verifikasi (OTP) yang telah dikirimkan ke ponsel Anda, lalu klik tombol "Selanjutnya".',
          },
          {
            title:
              'Akan tampil halaman konfirmasi, lalu Anda dapat menyelesaikan transaksi.',
          },
        ],
      },
    ],
  },
  {
    name: 'akulaku',
    data: [
      {
        title: 'Akulaku',
        steps: [
          {
            title:
              'Tekan tombol "Bayar Sekarang", lalu Anda akan diarahkan ke halaman Pusat Pembayaran Akulaku.',
          },
          {
            title:
              'Pilih tenor cicilan yang diinginkan, lalu Login ke akun Akulaku Anda dengan memasukkan nomor ponsel dan password.',
          },
          {
            title:
              'Masukkan kode verifikasi (OTP) yang telah dikirimkan ke ponsel Anda, lalu klik tombol "Selanjutnya".',
          },
          {
            title:
              'Akan tampil halaman konfirmasi, lalu Anda dapat menyelesaikan transaksi.',
          },
        ],
      },
    ],
  },
];
