import React from 'react';
import { Text, View } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const Footer = (props) => {
    //const { textStyle, viewStyle } = styles;
        return (
        <View style= {styles.viewStyle}>
            {props.children}
        </View>
        );
};

const styles = {
    viewStyle: {
        backgroundColor:'#ce0404',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: hp('2.5%'),
        paddingBottom:hp('3%'),
        position:'relative',
        width:'100%',
        bottom:0,
        right:0,
        color:'ffffff',
        // flex:1,
        display:'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft:15,
        paddingRight:15,
        // shadowOffset:{  width: 10,  height: 10},
        // shadowColor: 'black',
        // shadowOpacity: 1.0,
        // elevation: 5
    }
}

export {Footer};