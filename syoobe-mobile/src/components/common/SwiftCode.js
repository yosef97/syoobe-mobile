import React, {Component} from 'react';
import {WebView} from 'react-native-webview';

export default class SwiftCode extends Component {
  render() {
    return (
      <>
        <WebView
          source={{
            uri: 'https://www.syoobe.co.id/cms/view/55',
          }}
        />
      </>
    );
  }
}
