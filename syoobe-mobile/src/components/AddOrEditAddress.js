import React, {Component} from 'react';
import {Picker, Alert, ScrollView, View, Text} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {CardSection, InputInner, ButtonTwo, Spinner, Button} from './common';
import {selectAddressPayment, getCountries, getStates} from '../actions';
import {Font} from './Font';
import axios from 'axios';
import {
  resetAdress,
  setSelectedProvince,
  setSelectedCity,
  setSelectedDistrict,
  setComponentRedirect,
} from '../actions/AdressAction';
import InputText from './common/InputText';

class AddOrEditAddress extends Component {
  state = {
    _token: this.props.route.params._token,
    fullName: '',
    phoneNumber: '',
    addressLine1: '',
    addressLine2: '',
    city_name: '',
    city_id: 0,
    province_id: 0,
    province: '',
    subdistrict_name: '',
    subdistrict_id: 0,
    zipCode: 0,
    selectedCountry: 0,
    selectedState: 0,
    address_id: this.props.route.params.address_id,
    order_product_id: this.props.route.params.order_product_id,
    type: this.props.route.params.type,
    loading: false,
    fromSales: this.props.route.params.fromSales,
  };

  componentDidMount = async () => {
    const {address_id, _token, order_product_id, fromSales} = this.state;
    console.log(order_product_id);
    const url = fromSales ? 'editShippingAddressInSeller' : 'editChangeAddress';
    this.props.getCountries();
    if (address_id) {
      this.setState({
        loading: true,
      });
      let details = {
        _token: _token,
        ua_id: address_id,
      };
      if (order_product_id) {
        details = {
          ...details,
          ...{
            order_product_id: order_product_id,
          },
        };
      }
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      axios
        .post(`https://syoobe.co.id/api/${url}`, formBody)
        .then((response) => {
          console.log('adrssd', response.data);
          if (response.data.status == 1) {
            if (response.data.edit_address) {
              const {
                ua_country,
                ua_name,
                ua_phone,
                ua_address1,
                ua_address2,
                ua_city,
                ua_city_id,
                ua_state,
                ua_state_name,
                ua_district,
                ua_district_id,
                ua_zip,
              } = response.data.edit_address;
              this.loadEditState(parseInt(ua_country));
              // Update address edited
              this.props.setSelectedProvince(
                {province: ua_state_name, province_id: ua_state},
                this.props.navigation,
                true,
              );
              this.props.setSelectedCity(
                {city_name: ua_city, city_id: ua_city_id},
                this.props.navigation,
                true,
              );
              this.props.setSelectedDistrict(
                {subdistrict_name: ua_district, subdistrict_id: ua_district_id},
                this.props.navigation,
                true,
              );

              this.setState({
                fullName: ua_name,
                phoneNumber: ua_phone,
                addressLine1: ua_address1,
                addressLine2: ua_address2,
                city_name: ua_city,
                city_id: ua_city_id,
                province_id: ua_state,
                province: ua_state_name,
                subdistrict_name: ua_district,
                subdistrict_id: ua_district_id,
                zipCode: ua_zip,
              });
            } else if (response.data.address_details) {
              const {
                country_id,
                username,
                phn,
                address1,
                address2,
                city,
                zip,
                state_id,
              } = response.data.address_details[0];
              this.loadEditState(parseInt(country_id));
              this.setState({
                fullName: username,
                phoneNumber: phn,
                addressLine1: address1,
                addressLine2: address2,
                city: city,
                zipCode: zip,
                selectedCountry: parseInt(country_id),
                selectedState: parseInt(state_id),
              });
            }
          } else {
            this.setState({
              loading: false,
            });
          }
        });
    }
  };

  loadEditState = async (countryId) => {
    this.props.getStates({country_id: countryId});
    await this.setState({
      loading: false,
    });
  };

  getStates = (itemValue) => {
    this.setState({selectedCountry: itemValue});
    this.props.getStates({country_id: itemValue});
  };

  addAddress = async () => {
    if (String(this.state.fullName).trim() == '') {
      Alert.alert('Informasi', 'Nama tidak boleh kosong', [
        {text: 'OK', onPress: () => console.log('ok')},
      ]);
    } else if (String(this.state.phoneNumber).trim() == '') {
      Alert.alert('Informasi', 'Nomor telepon tidak boleh kosong', [
        {text: 'OK', onPress: () => console.log('ok')},
      ]);
    } else if (String(this.state.addressLine1).trim() == '') {
      Alert.alert('Informasi', 'Alamat tidak boleh kosong', [
        {text: 'OK', onPress: () => console.log('ok')},
      ]);
    } else if (!this.props.selectedProvince.province_id) {
      Alert.alert('Informasi', 'Provinsi tidak boleh kosong', [
        {text: 'OK', onPress: () => console.log('ok')},
      ]);
    } else if (!this.props.selectedCity.city_id) {
      Alert.alert('Informasi', 'Kota/Kabupaten tidak boleh kosong', [
        {text: 'OK', onPress: () => console.log('ok')},
      ]);
    } else if (!this.props.selectedDistrict.subdistrict_id) {
      Alert.alert('Informasi', 'Kecamatan tidak boleh kosong', [
        {text: 'OK', onPress: () => console.log('ok')},
      ]);
    } else if (this.state.zipCode === 0) {
      Alert.alert('Informasi', 'Kodepos tidak boleh kosong', [
        {text: 'OK', onPress: () => console.log('ok')},
      ]);
    } else {
      let url = this.state.fromSales
        ? 'updateShippingAddressInSalesView'
        : 'addUpdateAddressInCart';
      this.setState({
        loading: true,
      });
      let details = {
        _token: this.state._token,
        ua_name: this.state.fullName,
        ua_address1: this.state.addressLine1,
        ua_address2: this.state.addressLine2,
        ua_city: this.props.selectedCity.city_name,
        ua_city_id: this.props.selectedCity.city_id,
        ua_state: this.props.selectedProvince.province_id,
        ua_state_name: this.props.selectedProvince.province,
        ua_district: this.props.selectedDistrict.subdistrict_name,
        ua_district_id: this.props.selectedDistrict.subdistrict_id,
        ua_country: 106,
        ua_zip: this.state.zipCode,
        ua_phone: this.state.phoneNumber,
      };
      if (this.state.address_id != undefined) {
        details = {
          ...details,
          ...{ua_id: this.state.address_id},
        };
      } else {
        details = {
          ...details,
          ...{ua_id: 0},
        };
      }
      if (this.state.order_product_id) {
        details = {
          ...details,
          ...{
            order_product_id: this.state.order_product_id,
          },
        };
      }
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post(`https://syoobe.co.id/api/${url}`, formBody)
        .then((response) => {
          this.props.resetAdress();
          if (response.data.status == 1) {
            if (!this.state.fromSales) {
              let detailsInside = {
                _token: this.state._token,
                ua_id: response.data.address_list.ua_id,
              };
              this.props.selectAddressPayment(this.state.type, detailsInside);
              this.setState({
                loading: false,
              });
              Alert.alert('Informasi', 'Alamat baru berhasil ditambah', [
                {
                  text: 'OK',
                },
              ]);
              this.props.navigation.navigate('Payment');
              console.log('ini berhasil1');
            } else {
              // console.log("pp", this.props);
              this.props.route.params.callParentSales();
              this.setState({
                loading: false,
              });
              Alert.alert('Alamat baru berhasil ditambah');
              this.props.navigation.pop();
              console.log('ini berhasil2');
            }
          } else {
            this.setState({
              loading: false,
            });
            Alert.alert('Terjadi keslahan saat menambah alamat');
          }
        });
    }
  };

  render() {
    if (this.state.loading) {
      return <Spinner color="red" size="large" />;
    }

    const {
      selectedProvince,
      selectedCity,
      selectedDistrict,
      navigation,
    } = this.props;

    let items;
    let states;
    if (this.props.countries) {
      items = this.props.countries.map((value) => {
        return (
          <Picker.Item key={value.id} value={value.id} label={value.name} />
        );
      });
    }
    if (this.props.states) {
      states = this.props.states.map((value) => {
        return (
          <Picker.Item key={value.id} value={value.id} label={value.name} />
        );
      });
    }
    return (
      <ScrollView keyboardShouldPersistTaps={'always'}>
        <View style={{padding: 20, backgroundColor: '#fff'}}>
          <CardSection>
            <InputText
              value={this.state.fullName}
              onChangeText={(val) =>
                this.setState({
                  fullName: val,
                })
              }
              placeholder={'Masukkan Nama Lengkap'}
              label="Nama Lengkap"
            />
          </CardSection>
          <CardSection>
            <InputText
              value={this.state.phoneNumber}
              onChangeText={(val) =>
                this.setState({
                  phoneNumber: val,
                })
              }
              keyboardType={'numeric'}
              label="Nomor Telepon"
              placeholder={'Masukkan Nomor Telepon'}
            />
          </CardSection>
          <CardSection>
            <InputText
              value={this.state.addressLine1}
              onChangeText={(val) =>
                this.setState({
                  addressLine1: val,
                })
              }
              multiline
              numberOfLines={4}
              label="Alamat Utama"
              placeholder={'Masukkan Alamat Utama'}
            />
          </CardSection>
          <CardSection>
            <InputText
              value={this.state.addressLine2}
              onChangeText={(val) =>
                this.setState({
                  addressLine2: val,
                })
              }
              multiline
              numberOfLines={4}
              placeholder={'Masukkan Alamat Tambahan'}
              label="Alamat Tambahan"
            />
          </CardSection>

          <CardSection>
            <InputText
              value={selectedProvince.province || this.state.province}
              onFocus={() => {
                navigation.navigate('Province');
              }}
              label="Provinsi"
              placeholder={'Pilih Provinsi'}
              icon="chevron-down"
            />
          </CardSection>
          <CardSection>
            <InputText
              value={selectedCity.city_name || this.state.city_name}
              onFocus={() =>
                navigation.navigate(
                  selectedProvince.province ? 'City' : 'Province',
                  {
                    province_id:
                      selectedProvince.province_id || this.state.province_id,
                  },
                )
              }
              label="Kota/Kabupaten"
              placeholder="Pilih Kota/Kabupaten"
              icon="chevron-down"
            />
          </CardSection>
          <CardSection>
            <InputText
              value={
                selectedDistrict.subdistrict_name || this.state.subdistrict_name
              }
              onFocus={() =>
                navigation.navigate(
                  selectedCity.city_name ? 'Subdistrict' : 'Province',
                  {
                    city_id: selectedCity.city_id || this.state.city_id,
                    component: 'AddOrEditAddress',
                  },
                )
              }
              label="Kecamatan"
              placeholder="Pilih Kecamatan"
              icon="chevron-down"
            />
          </CardSection>
          <CardSection>
            <InputText
              value={this.state.zipCode}
              onChangeText={(val) =>
                this.setState({
                  zipCode: val,
                })
              }
              label="Kode Pos"
              placeholder="Masukkan Kode Pos"
              keyboardType={'numeric'}
            />
          </CardSection>
          <Button onPress={() => this.addAddress()}>Simpan</Button>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  dropdownClass: {
    // elevation: 1,
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
    borderRadius: 50,
  },
  pickerClass: {
    height: hp('8%'),
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
};

const mapStateToProps = (state) => {
  return {
    states: state.country.states,
    countries: state.country.countries,
    selectedProvince: state.country.province.selectedProvince,
    selectedCity: state.country.city.selectedCity,
    selectedDistrict: state.country.district.selectedDistrict,
  };
};

export default connect(mapStateToProps, {
  selectAddressPayment,
  getCountries,
  getStates,
  resetAdress,
  setSelectedProvince,
  setSelectedCity,
  setSelectedDistrict,
})(AddOrEditAddress);
