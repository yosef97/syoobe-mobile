import {createDrawerNavigator} from '@react-navigation/drawer';
import MyOrderList from '../MyOrderList';
import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SidebarSecure from '../../components/common/SidebarSecure';
import Category from '../Category';
import CreatShopStepOne from '../CreatShopStepOne';
import Dashboard from '../Dashboard';
import EditProfile from '../EditProfile';
import Faq from '../Faq';
import FavouriteListsforItems from '../FavouriteListsforItems';
import Home from '../Home';
import ChatWebView from '../ChatWebView';
import MyWallet from '../MyWallet';
import Sales from '../Sales';
import SellerMyProductList from '../SellerMyProductList';
import ShareEarn from '../Share_Earn';
import ShopDetails from '../ShopDetails';
import DisputeManagement from '../DisputeManagement';
import MyReward from '../MyReward';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
// import AddProduct from '../AddProduct';
// import OrderHistory from '../OrderHistory';
// import DisputeManagementList from '../DisputeManagementList';

const HomePageDrawer = createDrawerNavigator();

export const HomeDrawer = () => {
  return (
    <HomePageDrawer.Navigator
      initialRouteName="Home"
      drawerContent={(props) => <SidebarSecure {...props} />}>
      <HomePageDrawer.Screen
        name="Home"
        component={Home}
        options={{
          drawerLabel: 'Home',
          drawerIcon: ({size, color}) => (
            <MaterialIcons name={'home'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="Dashboard"
        component={Dashboard}
        options={{
          drawerLabel: 'Dashboard',
          drawerIcon: ({size, color}) => (
            <MaterialIcons name={'dashboard'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="Categories"
        component={Category}
        options={{
          drawerLabel: 'Kategori',
          drawerIcon: ({size, color}) => (
            <MaterialIcons name={'apps'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="EditProfile"
        component={EditProfile}
        options={{
          drawerLabel: 'Akun Saya',
          drawerIcon: ({size, color}) => (
            <Feather name={'user'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="CreatShopStepOne"
        component={CreatShopStepOne}
        options={{
          drawerLabel: 'Buat Toko',
          drawerIcon: ({size, color}) => (
            <MaterialIcons name={'store'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="FavouriteListsforItems"
        component={FavouriteListsforItems}
        options={{
          drawerLabel: 'Produk Favorit',
          drawerIcon: ({size, color}) => (
            <MaterialIcons name={'favorite'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="Sales"
        component={Sales}
        options={{
          drawerLabel: 'Penjualan',
          drawerIcon: ({size, color}) => (
            <MaterialIcons name={'point-of-sale'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="DisputeManagementList"
        component={DisputeManagement}
        options={{
          drawerLabel: 'Manajemen Perselisihan',
          drawerIcon: ({size, color}) => (
            <AntDesign name={'profile'} size={size} color={color} />
          ),
        }}
      />

      <HomePageDrawer.Screen
        name="MyOrderList"
        component={MyOrderList}
        options={{
          drawerLabel: 'Pesanan Saya',
          drawerIcon: ({size, color}) => (
            <Feather name={'shopping-bag'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="SellerMyProductList"
        component={SellerMyProductList}
        options={{
          drawerLabel: 'Produk Saya',
          drawerIcon: ({size, color}) => (
            <Feather name={'box'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="ShopDetails"
        component={ShopDetails}
        options={{
          drawerLabel: 'Toko Saya',
          drawerIcon: ({size, color}) => (
            <MaterialCommunityIcons
              name={'storefront-outline'}
              size={size}
              color={color}
            />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="ChatWebView"
        component={ChatWebView}
        options={{
          drawerLabel: 'Chat',
          drawerIcon: ({size, color}) => (
            <Ionicons name={'paper-plane-outline'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="MyWallet"
        component={MyWallet}
        options={{
          drawerLabel: 'Saldo Saya',
          drawerIcon: ({size, color}) => (
            <Ionicons name={'wallet-outline'} size={size} color={color} />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="MyReward"
        component={MyReward}
        options={{
          drawerLabel: 'Poin Hadiah',
          drawerIcon: ({size, color}) => (
            <FontAwesome5Icon name={'medal'} size={size} color={color} />
          ),
        }}
      />

      <HomePageDrawer.Screen
        name="ShareEarn"
        component={ShareEarn}
        options={{
          drawerLabel: 'Refferal',
          drawerIcon: ({size, color}) => (
            <MaterialCommunityIcons
              name={'charity'}
              size={size}
              color={color}
            />
          ),
        }}
      />
      <HomePageDrawer.Screen
        name="Faq"
        component={Faq}
        options={{
          drawerLabel: 'Pusat Bantuan',
          drawerIcon: ({size, color}) => (
            <Feather name={'help-circle'} size={size} color={color} />
          ),
        }}
      />
    </HomePageDrawer.Navigator>
  );
};
