import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import DashboardSectionSeller from '../DashboardSectionSeller';
import DashboardSectionBuyer from '../DashboardSectionBuyer';

const Tab = createMaterialTopTabNavigator();

export const DashboardTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Penjual" component={DashboardSectionSeller} />
      <Tab.Screen name="Pembeli" component={DashboardSectionBuyer} />
    </Tab.Navigator>
  );
};
