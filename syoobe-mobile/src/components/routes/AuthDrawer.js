import {createDrawerNavigator} from '@react-navigation/drawer';
import React from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Sidebar from '../common/Sidebar';
import Home from '../Home';
import Login from '../Login';
import Registration from '../Registration';

const LoginDrawer = createDrawerNavigator();

export const AuthDrawer = () => {
  return (
    <LoginDrawer.Navigator
      initialRouteName="Login"
      drawerContent={(props) => <Sidebar {...props} />}>
      <LoginDrawer.Screen
        name="Home"
        component={Home}
        options={{
          drawerLabel: 'Home',
          drawerIcon: ({size, color}) => (
            <MaterialIcons name={'home'} size={size} color={color} />
          ),
        }}
      />
      <LoginDrawer.Screen
        name="Login"
        component={Login}
        options={{
          drawerLabel: 'Masuk',
          drawerIcon: ({size, color}) => (
            <MaterialIcons name={'login'} size={size} color={color} />
          ),
        }}
      />
      <LoginDrawer.Screen
        name="Registration"
        component={Registration}
        options={{
          drawerLabel: 'Daftar',
          drawerIcon: ({size, color}) => (
            <MaterialIcons
              name={'app-registration'}
              size={size}
              color={color}
            />
          ),
        }}
      />
    </LoginDrawer.Navigator>
  );
};
