import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import NotificationActive from '../NotificationActive';
import NotificationArchive from '../NotificationArchive';

const Tab = createMaterialTopTabNavigator();

export const NotificationTab = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Aktif" component={NotificationActive} />
      <Tab.Screen name="Arsip" component={NotificationArchive} />
    </Tab.Navigator>
  );
};
