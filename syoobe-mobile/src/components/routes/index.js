import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
// import {createDrawerNavigator} from '@react-navigation/drawer';
import {Text, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {AuthDrawer} from './AuthDrawer';
import {HomeDrawer} from './HomeDrawer';
import PaymentWithCc from '../payment/PaymentWithCc';
import PaymentWithBcaVa from '../payment/PaymentWithBcaVa';
import PaymentWithMandiriBill from '../payment/PaymentWithMandiriBill';
import PaymentWithEwallet from '../payment/PaymentWithEwallet';
import PaymentRedirect from '../payment/PaymentRedirect';
import PaymentWithCsStore from '../payment/PaymentWithCsStore';
import PaymentSuccessVA from '../payment/PaymentSuccessVA';
import PaymentFailed from '../PaymentFailed';
import MyDownloadFile from '../MyDownloadFile';
import PaymentWithWallet from '../payment/PaymentWithWallet';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {DrawerActions} from '@react-navigation/native';
import ForgotPassword from '../ForgotPassword';
import AuthLoadingScreen from '../AuthLoadingScreen';
import MyCart from '../MyCart';
import ImgDetail from '../ImgDetail';
import MyProduct from '../MyProduct';
import PersonalInfo from '../PersonalInfo';
import ChangeEmail from '../ChangeEmail';
import ChangePassword from '../ChangePassword';
import Favorites from '../Favorites';
import BankInfo from '../BankInfo';
import CreatShopStepTwo from '../CreatShopStepTwo';
import CreatShopStepThree from '../CreatShopStepThree';
import CreatShopStepFour from '../CreatShopStepFour';
import ReturnAddressInfo from '../ReturnAddressInfo';
import ViewOrderDigital from '../ViewOrderDigital';
import ViewOrderPhysical from '../ViewOrderPhysical';
import ProductsByCategory from '../ProductsByCategory';
import AddProduct from '../AddProduct';
import ShopDetails from '../ShopDetails';
import ProductDetailsInfo from '../ProductDetailsInfo';
import ProductDetail from '../ProductDetail';
import RequestNewCompatibility from '../RequestNewCompatibility';
import Payment from '../Payment';
import AddressList from '../AddressList';
import AddOrEditAddress from '../AddOrEditAddress';
import CancelForm from '../CancelForm';
import FinalPayment from '../FinalPayment';
import EnterCardDetails from '../EnterCardDetails';
import ThankYou from '../ThankYou';
import SalesPhysical from '../SalesPhysical';
import SalesDigital from '../SalesDigital';
import DisputeManagementView from '../DisputeManagementView';
import SendYourRequirements from '../SendYourRequirements';
import SendRevision from '../SendRevision';
import AddFunds from '../AddFunds';
import WithDrawFunds from '../WithDrawFunds';
import Notifications from '../Notifications';
import ProductList from '../ProductList';
import SearchParent from '../Search';
import Otp from '../Otp';
import HomePageRightIcons from '../HomePageRightIcons';
import HomePageMenu from '../HomePageMenu';
import HomePageLogo from '../HomePageLogo';
import Home from '../Home';
import Province from '../../container/pages/Adress/Province';
import City from '../../container/pages/Adress/City';
import District from '../../container/pages/Adress/District';
import Country from '../../container/pages/Adress/Country';
import State from '../../container/pages/Adress/State';
import ChatWebViewNavigate from '../ChatWebViewNavigate';
import SwiftCode from '../common/SwiftCode';
import TermsCondition from '../common/TermsCondition';
import LearnSellingLimit from '../LearnSellingLimit';
import ContactSupport from '../ContactSupport';
import VideoProduct from '../VideoProduct';
import ApplySellingLimit from '../ApplySellingLimit';
import Messages from '../Messages';
import ReplyMessage from '../ReplyMessage';

const AppStack = createStackNavigator();

export const AppStackNavigator = () => {
  return (
    <AppStack.Navigator
      initialRouteName="AuthLoadingScreen"
      screenOptions={() => ({
        headerBackTitleVisible: false,
      })}>
      {/* Drawer Navigation */}
      <AppStack.Screen
        name="AuthDrawer"
        component={AuthDrawer}
        options={({route, navigation}) => ({
          headerTitle: <HomePageLogo />,
          headerTitleStyle: {
            width: 130,
            height: 25,
          },
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerRight: () => <HomePageRightIcons {...navigation} />,
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
              <HomePageMenu />
            </TouchableOpacity>
          ),
        })}
      />
      <AppStack.Screen
        name="HomeDrawer"
        component={HomeDrawer}
        options={({route, navigation}) => ({
          headerTitle: <HomePageLogo />,
          headerTitleStyle: {
            width: 130,
            height: 25,
          },
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerRight: () => <HomePageRightIcons {...navigation} />,
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
              <HomePageMenu />
            </TouchableOpacity>
          ),
        })}
      />
      {/* Stack Navigation */}
      <AppStack.Screen name="Home" component={Home} />
      <AppStack.Screen
        name="AuthLoadingScreen"
        component={AuthLoadingScreen}
        options={{headerShown: false}}
      />
      <AppStack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Lupa Kata Sandi',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="Otp"
        component={Otp}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Verifikasi 2 langkah',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="ProductList"
        component={ProductList}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Daftar Produk',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="ProductDetail"
        component={ProductDetail}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: () => (
            <Text style={{fontSize: hp('2.5%'), color: '#fff'}}>
              {String(route.params.product_name).length > 20
                ? String(route.params.product_name).substring(0, 25) + '...'
                : route.params.product_name}
            </Text>
          ),
          headerRight: () => (
            <HomePageRightIcons onlyCart={true} {...navigation} />
          ),
        })}
      />
      <AppStack.Screen
        name="ProductDetailsInfo"
        component={ProductDetailsInfo}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle:
            route.params.pageHeader == 'Policies'
              ? 'Kebijakan Pengiriman'
              : 'Detail Unduhan',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="MyCart"
        component={MyCart}
        options={({route, navigation}) => ({
          headerTitle: 'Keranjang Saya',
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
        })}
      />
      <AppStack.Screen
        name="Notifications"
        component={Notifications}
        options={({route, navigation}) => ({
          headerTitle: 'Notifikasi',
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
        })}
      />
      <AppStack.Screen
        name="MyProduct"
        component={MyProduct}
        options={({route, navigation}) => ({
          headerTitle: 'Produk Saya',
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
        })}
      />
      <AppStack.Screen
        name="PersonalInfo"
        component={PersonalInfo}
        options={({route, navigation}) => ({
          headerTitle: 'Informasi Akun',
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
        })}
      />
      <AppStack.Screen
        name="ReturnAddressInfo"
        component={ReturnAddressInfo}
        options={({route, navigation}) => ({
          headerTitle: 'Alamat Pengembalian',
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
        })}
      />
      <AppStack.Screen
        name="AddFunds"
        component={AddFunds}
        options={({route, navigation}) => ({
          headerTitle: 'Pengisian Dana',
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
        })}
      />
      <AppStack.Screen
        name="ChangeEmail"
        component={ChangeEmail}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Edit Email',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Ubah Kata Sandi',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="BankInfo"
        component={BankInfo}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Informasi Bank',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="SwiftCode"
        component={SwiftCode}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Informasi Kode Swift',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="TermsCondition"
        component={TermsCondition}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Syarat dan Ketentuan',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />

      <AppStack.Screen
        name="LearnSellingLimit"
        component={LearnSellingLimit}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Batas Penjualan',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />

      <AppStack.Screen
        name="CreatShopStepTwo"
        component={CreatShopStepTwo}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Buat Toko',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="CreatShopStepThree"
        component={CreatShopStepThree}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Buat Toko',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="CreatShopStepFour"
        component={CreatShopStepFour}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Buat Toko',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="ViewOrderDigital"
        component={ViewOrderDigital}
        options={({route, navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="arrowleft"
              size={hp(3)}
              color={'#fff'}
              style={{paddingLeft: wp(3), fontWeight: 'bold'}}
              onPress={() =>
                navigation.replace('HomeDrawer', {screen: 'MyOrderList'})
              }
            />
          ),
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Detail Pesanan',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="SearchScreen"
        component={SearchParent}
        options={({route, navigation}) => ({
          headerTitle: 'Pencarian',
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
        })}
      />
      <AppStack.Screen
        name="ViewOrderPhysical"
        component={ViewOrderPhysical}
        options={({route, navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="arrowleft"
              size={hp(3)}
              color={'#fff'}
              style={{paddingLeft: wp(3), fontWeight: 'bold'}}
              onPress={() =>
                navigation.replace('HomeDrawer', {screen: 'MyOrderList'})
              }
            />
          ),
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Detail Pesanan',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="ProductsByCategory"
        component={ProductsByCategory}
        options={({route, navigation}) => ({
          headerTitle: 'Hasil Pencarian',
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
        })}
      />
      <AppStack.Screen
        name="ShopDetails"
        component={ShopDetails}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Detail Toko',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="RequestNewCompatibility"
        component={RequestNewCompatibility}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: '#fff',
          headerTitle: () => (
            <TouchableOpacity
              style={{
                width: wp('33%'),
                height: wp('4.5%'),
                marginLeft: 'auto',
                marginRight: 'auto',
              }}>
              <HomePageLogo />
            </TouchableOpacity>
          ),
          headerRight: () => <HomePageRightIcons {...navigation} />,
        })}
      />
      <AppStack.Screen
        name="Favorites"
        component={Favorites}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Produk favorit',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />

      <AppStack.Screen
        name="ChatWebViewNavigate"
        component={ChatWebViewNavigate}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Chat',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />

      <AppStack.Screen
        name="VideoProduct"
        component={VideoProduct}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Video Produk',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />

      <AppStack.Screen
        name="ApplySellingLimit"
        component={ApplySellingLimit}
        options={({route, navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="arrowleft"
              size={hp(3)}
              color={'#fff'}
              style={{paddingLeft: wp(3), fontWeight: 'bold'}}
              onPress={() =>
                navigation.replace('HomeDrawer', {
                  screen: 'SellerMyProductList',
                })
              }
            />
          ),
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Penghapusan Batas Penjualan',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />

      <AppStack.Screen
        name="Messages"
        component={Messages}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Kotak Pesan',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />

      <AppStack.Screen
        name="ReplyMessage"
        component={ReplyMessage}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Balas Pesan',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />

      {/* <AppStack.Screen
        name="ChatPHP"
        component={ChatPHP}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Chat',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      /> */}

      <AppStack.Screen
        name="Payment"
        component={Payment}
        options={({route, navigation}) => ({
          headerTitle: 'Rincian Belanja',
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
        })}
      />
      <AppStack.Screen
        name="AddressList"
        component={AddressList}
        options={({route, navigation}) => ({
          headerTitle: 'Daftar Alamat',
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
        })}
      />
      <AppStack.Screen
        name="AddOrEditAddress"
        component={AddOrEditAddress}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: () => (
            <Text style={{fontSize: hp('2.5%'), color: '#fff'}}>
              {route.params.typeHeader}
            </Text>
          ),
        })}
      />
      <AppStack.Screen
        name="FinalPayment"
        component={FinalPayment}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pembayaran',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="ThankYou"
        component={ThankYou}
        options={({route, navigation}) => ({
          headerShown: false,
        })}
      />
      <AppStack.Screen
        name="EnterCardDetails"
        component={EnterCardDetails}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Masukkan Detail Kartu',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="SendYourRequirements"
        component={SendYourRequirements}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Kirim Persyaratan',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="SendRevision"
        component={SendRevision}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Kirim Revisi',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="AddProduct"
        component={AddProduct}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: route.params ? 'Edit Produk' : 'Tambah Produk',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="CancelForm"
        component={CancelForm}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: route.params.headerName,
        })}
      />
      <AppStack.Screen
        name="WithDrawFunds"
        component={WithDrawFunds}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Penarikan Dana',
        })}
      />
      <AppStack.Screen
        name="SearchParent"
        component={SearchParent}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Cari',
          // headerRight: () => <HomePageRightIcons onlyCart={true} {...navigation} />
        })}
      />
      <AppStack.Screen
        name="ImgDetail"
        component={ImgDetail}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Detail Gambar',
        })}
      />
      <AppStack.Screen
        name="Country"
        component={Country}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pilih Negara',
        })}
      />
      <AppStack.Screen
        name="Province"
        component={Province}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pilih Provinsi',
        })}
      />
      <AppStack.Screen
        name="City"
        component={City}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pilih Kota/Kabupaten',
        })}
      />
      <AppStack.Screen
        name="District"
        component={District}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pilih Kecamatan',
        })}
      />
      <AppStack.Screen
        name="State"
        component={State}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pilih Provinsi',
        })}
      />
      <AppStack.Screen
        name="PaymentWithCc"
        component={PaymentWithCc}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pembayaran',
        })}
      />
      <AppStack.Screen
        name="PaymentWithBcaVa"
        component={PaymentWithBcaVa}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pembayaran',
        })}
      />
      <AppStack.Screen
        name="PaymentWithMandiriBill"
        component={PaymentWithMandiriBill}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pembayaran',
        })}
      />
      <AppStack.Screen
        name="PaymentWithEwallet"
        component={PaymentWithEwallet}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pembayaran',
        })}
      />
      <AppStack.Screen
        name="PaymentWithCsStore"
        component={PaymentWithCsStore}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pembayaran',
        })}
      />
      <AppStack.Screen
        name="PaymentWithWallet"
        component={PaymentWithWallet}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pembayaran',
        })}
      />
      <AppStack.Screen
        name="PaymentRedirect"
        component={PaymentRedirect}
        options={({route}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Pembayaran',
        })}
      />
      <AppStack.Screen
        name="PaymentSuccessVA"
        component={PaymentSuccessVA}
        options={({route, navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="arrowleft"
              size={hp(3)}
              color={'#fff'}
              style={{paddingLeft: wp(3), fontWeight: 'bold'}}
              onPress={() => navigation.replace('MyCart')}
            />
          ),
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Selesaikan Pembayaran',
        })}
      />
      <AppStack.Screen
        name="MyDownloadFile"
        component={MyDownloadFile}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Daftar Unduhan',
        })}
      />
      <AppStack.Screen
        name="PaymentFailed"
        component={PaymentFailed}
        options={({route}) => ({
          headerShown: false,
        })}
      />
      <AppStack.Screen
        name="SalesPhysical"
        component={SalesPhysical}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Penjualan',
        })}
      />
      <AppStack.Screen
        name="SalesDigital"
        component={SalesDigital}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Penjualan',
        })}
      />
      <AppStack.Screen
        name="DisputeManagementView"
        component={DisputeManagementView}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: `Detail ${route.params.type}`,
        })}
      />
      <AppStack.Screen
        name="ContactSupport"
        component={ContactSupport}
        options={({route, navigation}) => ({
          headerStyle: {
            backgroundColor: '#C90205',
          },
          headerTintColor: 'white',
          headerTitle: 'Hubungi Customer Care Kami',
        })}
      />
    </AppStack.Navigator>
  );
};
