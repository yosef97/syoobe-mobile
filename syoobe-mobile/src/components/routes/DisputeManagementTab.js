import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import React from 'react';
import DisputeManagementList from '../DisputeManagementList';

const Tab = createMaterialTopTabNavigator();

export const DisputeManagementTab = (props) => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        // scrollEnabled: true,
        labelStyle: {
          fontSize: 14,
        },
        tabStyle: {
          // width: 'auto',
        },
        indicatorStyle: {
          borderBottomWidth: 2,
          borderBottomColor: '#C90205',
        },
        style: {
          elevation: 0,
          shadowOpacity: 0,
          borderBottomWidth: 1,
          borderBottomColor: '#f5f6fa',
        },
        activeTintColor: '#C90205',
        inactiveTintColor: '#000',
      }}>
      <Tab.Screen
        name="Pengembalian"
        children={() => (
          <DisputeManagementList
            {...props}
            url={'disputeManagementReturnRequest'}
            type={'Pengembalian'}
          />
        )}
      />
      <Tab.Screen
        name="Pembatalan"
        children={() => (
          <DisputeManagementList
            {...props}
            url={'disputeManagementCancelRequest'}
            type={'Pembatalan'}
          />
        )}
      />
    </Tab.Navigator>
  );
};
