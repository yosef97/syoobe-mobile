import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {Spinner} from '../common';
import React from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import MyOrderListComponent from '../MyOrderListComponent';
import {View} from 'react-native';
import MyDownloadFile from '../MyDownloadFile';

const Tab = createMaterialTopTabNavigator();

export const MyOrderTabNavigator = ({
  lists,
  loading,
  navigation,
  getOrder,
  type,
  title,
}) => {
  if (loading) {
    return (
      <View
        style={{
          marginTop: hp('29%'),
        }}>
        <Spinner color="red" size="large" />
      </View>
    );
  }

  if (type === 'sales') {
    return <MyOrderListComponent type={type} navigation={navigation} />;
  }

  return (
    <Tab.Navigator
      lazy={true}
      tabBarOptions={{
        // scrollEnabled: true,
        labelStyle: {
          fontSize: 12,
        },
        tabStyle: {
          // width: 'auto',
        },
        indicatorStyle: {
          borderBottomWidth: 2,
          borderBottomColor: '#C90205',
        },
        style: {
          elevation: 0,
          shadowOpacity: 0,
          borderBottomWidth: 1,
          borderBottomColor: '#f5f6fa',
        },
        activeTintColor: '#C90205',
        inactiveTintColor: '#000',
      }}>
      <Tab.Screen
        // key={index}
        name={type === 'sales' ? 'Penjualan' : 'Belanja'}
        // listeners={({navigation, route}) => ({
        //   tabPress: (e) => getOrder(row.status_id),
        //   swipeEnd: (e) => getOrder(row.status_id),
        // })}
        children={() => (
          <MyOrderListComponent
            type={type}
            navigation={navigation}
            // status={row}
            // title={title}
          />
        )}
      />
      <Tab.Screen
        name={'Daftar Unduhan'}
        children={() => <MyDownloadFile navigation={navigation} />}
      />
    </Tab.Navigator>
  );
};
