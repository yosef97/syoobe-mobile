import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import ProductActive from '../ProductActive';
import ProductPause from '../ProductPause';
// import {View} from 'react-native';

const Tab = createMaterialTopTabNavigator();

export const ProductTabNavigator = () => {
  return (
    <Tab.Navigator lazy={true}>
      <Tab.Screen name="Produk Aktif" component={ProductActive} />
      <Tab.Screen name="Produk Dijeda" component={ProductPause} />
    </Tab.Navigator>
  );
};
