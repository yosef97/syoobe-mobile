import React from 'react';
import {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import DashboardSection from './DashboardSection';
import {getDashboardDetails} from '../actions';
import {connect} from 'react-redux';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';

class DashboardSectionBuyer extends Component {
  render() {
    console.log('ini state', JSON.stringify(this.state));
    console.log('ini props', JSON.stringify(this.props));

    if (this.props.buyerData) {
      var obj = this.props.buyerData;
      var res = Object.entries(obj).map(([k, v]) => [k, v]);
      return (
        <View style={styles.scrollView}>
          <View style={styles.accountBalance}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('MyWallet')}>
              <Text style={styles.accountTxt}>SALDO REKENING</Text>
              <Text style={styles.accountPrice}>
                {this.props.buyerData.total_credit}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.boxWrapper}>
            {res.map((box, index) => (
              <DashboardSection
                key={index}
                box={box}
                navigation={this.props.navigation}
                route={this.props.route}
                buyerData={this.props.buyerData}
              />
            ))}
          </View>
        </View>
      );
    }
    return null;
  }
}

const styles = {
  accountBalance: {
    elevation: 1,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'transparent',
    marginBottom: 15,
    height: wp(25),
    padding: wp(5),
  },
  accountTxt: {
    color: '#848484',
    textTransform: 'uppercase',
    fontSize: wp(3.5),
    fontFamily: Font.RobotoRegular,
  },
  accountPrice: {
    color: '#00b3ff',
    fontSize: wp(6),
  },
  boxWrapper: {
    // paddingHorizontal:15
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 5,
    paddingRight: 5,
    flexWrap: 'wrap',
  },
  scrollView: {
    paddingTop: 15,
    backgroundColor: '#fff',
    height: hp(60),
    paddingHorizontal: 10,
  },
};

const mapStateToProps = (state) => {
  return {
    buyerData: state.dashboard.buyerData,
  };
};

export default connect(mapStateToProps, {getDashboardDetails})(
  DashboardSectionBuyer,
);
