import axios from 'axios';
import React, {Component} from 'react';
import {
  Alert,
  Image,
  ScrollView,
  Text,
  View,
  AsyncStorage,
  FlatList,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {CardSection, Spinner} from './common';
import {Font} from './Font';
import {connect} from 'react-redux';
import {formatRupiah} from '../helpers/helper';
import {Textarea, Form, Content, Button, Text as TextNative} from 'native-base';
import {ProductFileUpload} from './common/product/ProductFileUpload';
import DocumentPicker from 'react-native-document-picker';
import Bugsnag from '@bugsnag/react-native';

class DisputeManagementView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      return_request_id: props.route.params.return_request_id,
      _token: props.route.params._token,
      return_data: null,
      loading: false,
      type: props.route.params.type,
      url: props.route.params.url,
      user_type: null,
      user_id: null,
      files: [],
      fileNames: '',
      message: '',
    };
  }

  fetchData = () => {
    this.setState({
      loading: true,
    });

    if (this.state.type == 'Pengembalian') {
      var details = {
        _token: this.state._token,
        return_refund_id: this.state.return_request_id,
      };
    } else {
      var details = {
        _token: this.state._token,
        cancel_request_id: this.state.return_request_id,
      };
    }

    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/' + this.state.url, formBody)
      .then((response) => {
        // console.log('respons disput', JSON.stringify(response.data));
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          this.setState({
            return_data: response.data.view_data,
            user_type: response.data.user_type,
          });
        }
      });
  };

  async componentDidMount() {
    await this.fetchData();

    AsyncStorage.getItem('user_id').then((res) =>
      this.setState({user_id: res}),
    );
  }

  escalateRequest = async () => {
    this.setState({
      loading: true,
    });
    var details = {
      _token: this.state._token,
      return_refund_id: this.state.return_request_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/escalateRequest', formBody)
      .then((response) => {
        console.log('dis11', response);
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          Alert.alert('Informasi', response.data.msg, [
            {
              text: 'OK',
              onPress: () => {
                this.props.navigation.replace('HomeDrawer', {
                  screen: 'DisputeManagementList',
                });
              },
            },
          ]);
        } else {
          Alert.alert('Informasi', response.data.msg, [
            {
              text: 'OK',
              onPress: () => {
                this.props.navigation.replace('HomeDrawer', {
                  screen: 'DisputeManagementList',
                });
              },
            },
          ]);
        }
      });
  };

  closeReturnRequest = async () => {
    this.setState({
      loading: true,
    });
    var details = {
      _token: this.state._token,
      return_refund_id: this.state.return_request_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/closeReturnRequest', formBody)
      .then((response) => {
        console.log('dis22', response);
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          Alert.alert('Informasi', response.data.msg, [
            {
              text: 'OK',
              onPress: () => {
                this.props.navigation.replace('HomeDrawer', {
                  screen: 'DisputeManagementList',
                });
              },
            },
          ]);
        } else {
          Alert.alert('Informasi', response.data.msg, [
            {
              text: 'OK',
              onPress: () => {
                this.props.navigation.replace('HomeDrawer', {
                  screen: 'DisputeManagementList',
                });
              },
            },
          ]);
        }
      });
  };

  approveReturnRequest = async () => {
    this.setState({
      loading: true,
    });
    var details = {
      _token: this.state._token,
      return_refund_id: this.state.return_request_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/closeReturnRequest', formBody)
      .then((response) => {
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          Alert.alert('Informasi', response.data.msg, [
            {
              text: 'OK',
              onPress: () => {
                this.props.navigation.replace('HomeDrawer', {
                  screen: 'DisputeManagementList',
                });
              },
            },
          ]);
        } else {
          Alert.alert('Informasi', response.data.msg, [
            {
              text: 'OK',
              onPress: () => {
                this.props.navigation.replace('HomeDrawer', {
                  screen: 'DisputeManagementList',
                });
              },
            },
          ]);
        }
      });
  };

  selectFile = async (fileType, item) => {
    try {
      const file = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      await this.setState({
        loading: true,
      });
      this.setState({
        files: file,
      });

      this.setState({
        fileNames: file.name,
        loading: false,
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        Bugsnag.notify(err);
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw Bugsnag.notify(err);
      }
    }
  };

  onSubmit = async () => {
    if (this.state.subject !== '' && this.state.message !== '') {
      this.setState({
        loading: true,
      });
      var formData = new FormData();

      if (this.state.fileNames) {
        const file = {
          name: this.state.files.name,
          uri: this.state.files.uri,
          type: this.state.files.type,
        };

        formData.append('_token', this.state._token);
        formData.append('return_refund_id', this.state.return_request_id);
        formData.append('refmsg_text', this.state.message);
        formData.append('attachment', file);
        await axios
          .post('https://syoobe.co.id/api/sendReturnRequestMessage', formData)
          .then((response) => {
            console.log('ini res response', JSON.stringify(response));
            if (response.data.status === 1) {
              this.setState({
                loading: false,
              });
              Alert.alert('Informasi', response.data.msg, [
                {
                  text: 'OK',
                  onPress: () => {
                    this.props.navigation.pop();
                  },
                },
              ]);
            } else {
              this.setState({
                loading: false,
              });
              Alert.alert('Gagal');
            }
          })
          .catch((err) => {
            Bugsnag.notify(err);
            console.log('e', err);
            this.setState({
              loading: false,
            });
          });
      } else {
        formData.append('_token', this.state._token);
        formData.append('return_refund_id', this.state.return_request_id);
        formData.append('refmsg_text', this.state.message);
        await axios
          .post('https://syoobe.co.id/api/sendReturnRequestMessage', formData)
          .then((response) => {
            console.log('ini res response', JSON.stringify(response));
            if (response.data.status === 1) {
              this.setState({
                loading: false,
              });
              Alert.alert('Informasi', response.data.msg, [
                {
                  text: 'OK',
                  onPress: () => {
                    this.props.navigation.pop();
                  },
                },
              ]);
            } else {
              this.setState({
                loading: false,
              });
              Alert.alert('Gagal');
            }
          })
          .catch((err) => {
            Bugsnag.notify(err);
            console.log('e', err);
            this.setState({
              loading: false,
            });
          });
      }
    } else {
      Alert.alert('Mohon cek kembali persaratan anda.');
    }
  };

  render() {
    // console.log('Ini state xxxx ', JSON.stringify(this.state.return_data));
    // console.log('Ini state user type ', JSON.stringify(this.state.user_type));
    // console.log('Ini status ', JSON.stringify(this.state.return_data?.status));
    // console.log('Ini props ', JSON.stringify(this.props));
    // console.log('Ini props ', JSON.stringify(this.state.url));

    console.log('return_request_id', this.state.return_request_id);
    console.log('this.state.message', this.state.message);

    if (this.state.files) {
      console.log('file if else', this.state.files);
    } else {
      console.log('file', this.state.files);
    }

    if (this.state.loading) {
      return <Spinner color="red" size="large" />;
    }
    return (
      <>
        {this.state.type == 'Pengembalian' ? (
          <View style={{flex: 1}}>
            <ScrollView style={styles.bgColor}>
              {this.state.return_data && (
                <View>
                  <View style={styles.addressWrapper}>
                    <View style={styles.addressDiv}>
                      <View style={styles.flexCss}>
                        <Text style={styles.billingAddress}>
                          ALAMAT PENGEMBALIAN
                        </Text>
                        <Text style={styles.productName2}>
                          {this.state.return_data.user_name}
                        </Text>
                        <View style={styles.iconBox}>
                          <Image
                            style={styles.iconAddress}
                            source={require('../images/addressIcon.png')}
                          />
                          <Text style={styles.iconTxt}>
                            {this.state.return_data.address1}{' '}
                            {this.state.return_data.address2},{' '}
                            {this.state.return_data.city},{' '}
                            {this.state.return_data.state} -{' '}
                            {this.state.return_data.zip},{' '}
                            {this.state.return_data.country_name}
                          </Text>
                        </View>
                        <View style={styles.iconBox}>
                          <Image
                            style={styles.icon}
                            source={require('../images/phIcon.png')}
                          />
                          <Text style={styles.iconTxt}>
                            {this.state.return_data.phone_number}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                  {this.state.return_data.status === 'Tertunda' &&
                    this.state.user_type === 'B' && (
                      <View
                        style={{
                          backgroundColor: '#f6edc1',
                          padding: 10,
                        }}>
                        <Text>{this.state.return_data.buyer_tips}</Text>
                        <View style={styles.cardActionButton}>
                          <Button
                            small
                            danger
                            onPress={() => this.escalateRequest()}>
                            <TextNative>TERUSKAN KE SYOOBE</TextNative>
                          </Button>
                          <Button
                            small
                            primary
                            onPress={() => this.closeReturnRequest()}>
                            <TextNative>TUTUP PERMINTAAN</TextNative>
                          </Button>
                        </View>
                      </View>
                    )}

                  {this.state.return_data.status === 'Tertunda' &&
                    this.state.user_type === 'S' && (
                      <View
                        style={{
                          backgroundColor: '#f6edc1',
                          padding: 10,
                        }}>
                        <Text>{this.state.return_data.seller_tips}</Text>
                        <View style={styles.cardActionButton}>
                          <Button
                            small
                            danger
                            onPress={() => this.escalateRequest()}>
                            <TextNative>TERUSKAN KE SYOOBE</TextNative>
                          </Button>
                          <Button
                            small
                            primary
                            onPress={() => this.approveReturnRequest()}>
                            <TextNative>SETUJUI PENGEMBALIAN</TextNative>
                          </Button>
                        </View>
                      </View>
                    )}

                  {this.state.return_data.status === 'Dieskalasi' &&
                    this.state.user_type === 'B' && (
                      <View
                        style={{
                          backgroundColor: '#f6edc1',
                          padding: 10,
                        }}>
                        <Text>{this.state.return_data.buyer_tips}</Text>
                        <View style={styles.cardActionButton}>
                          <Button
                            small
                            primary
                            onPress={() => this.closeReturnRequest()}>
                            <TextNative>TUTUP PERMINTAAN</TextNative>
                          </Button>
                        </View>
                      </View>
                    )}

                  {this.state.return_data.status === 'Dieskalasi' &&
                    this.state.user_type === 'S' && (
                      <View
                        style={{
                          backgroundColor: '#f6edc1',
                          padding: 10,
                        }}>
                        <Text>{this.state.return_data.seller_tips}</Text>
                        <View style={styles.cardActionButton}>
                          <Button
                            small
                            primary
                            onPress={() => this.approveReturnRequest()}>
                            <TextNative>SETUJUI PENGEMBALIAN</TextNative>
                          </Button>
                        </View>
                      </View>
                    )}
                  <View>
                    <ScrollView
                      horizontal={true}
                      style={styles.tableWrapperOuter}>
                      <View style={styles.tableWrapper}>
                        <View style={styles.tableHeader}>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Nomor Permintaan Pengembalian
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Produk
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Jumlah
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Jenis Permintaan
                          </Text>
                        </View>
                        <View style={styles.tableRow}>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.return_request_number}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.product_name}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.qty}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.request_type}
                          </Text>
                        </View>
                      </View>
                    </ScrollView>
                  </View>
                  <CardSection />
                  <View>
                    <ScrollView
                      horizontal={true}
                      style={styles.tableWrapperOuter}>
                      <View style={styles.tableWrapper}>
                        <View style={styles.tableHeader}>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Alasan
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Tanggal
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            STATUS
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Jumlah
                          </Text>
                        </View>
                        <View style={styles.tableRow}>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.reason}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.date}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.status}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.price_currency}.{' '}
                            {formatRupiah(this.state.return_data.amount)}
                            {'  '}
                            {'(+Tax: Rp. ' +
                              formatRupiah(this.state.return_data.tax)}
                            {')'}
                          </Text>
                        </View>
                      </View>
                    </ScrollView>
                    <ScrollView style={{flex: 1, backgroundColor: '#fff'}}>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          alignItems: 'flex-end',
                          justifyContent: 'space-between',
                        }}>
                        <View style={{margin: wp(3), alignItems: 'center'}}>
                          <Image
                            style={styles.chatIcon}
                            // resize="contain"
                            source={{
                              uri: this.state.return_data
                                .sent_user_pic_for_chat,
                            }}
                          />
                          <Text style={styles.iconName}>
                            {this.state.return_data.sent_user_name_for_chat}
                          </Text>
                        </View>
                        <View style={{margin: wp(3), alignItems: 'center'}}>
                          <Image
                            style={styles.chatIcon}
                            // resize="contain"
                            source={{
                              uri: this.state.return_data
                                .sent_user_pic_from_chat,
                            }}
                          />
                          <Text style={styles.iconName}>
                            {this.state.return_data.sent_user_name_from_chat}
                          </Text>
                        </View>
                      </View>
                      {this.state.return_data.message_data.map(
                        (message, index) => (
                          <View style={{marginBottom: hp(3)}}>
                            {/* {console.log('message', message.msg_from)} */}
                            {/* {console.log('userid', this.state.user_id)} */}

                            <View
                              style={
                                this.state.user_id !== message.msg_from
                                  ? styles.leftChat
                                  : styles.rightChat
                              }
                              key={index}>
                              <Text
                                style={
                                  this.state.user_id !== message.msg_from
                                    ? styles.bodyLeftDateChat
                                    : styles.bodyRightDateChat
                                }>
                                {message.date}
                              </Text>
                              <Text
                                style={
                                  this.state.user_id !== message.msg_from
                                    ? styles.bodyLeftChat
                                    : styles.bodyRighChat
                                }
                                key={message.msg_id}>
                                {message.msg}
                              </Text>

                              <View
                                style={
                                  this.state.user_id !== message.msg_from
                                    ? styles.leftArrow
                                    : styles.rightArrow
                                }></View>

                              <View
                                style={
                                  this.state.user_id !== message.msg_from
                                    ? styles.leftArrowOverlap
                                    : styles.rightArrowOverlap
                                }></View>
                            </View>
                          </View>
                        ),
                      )}

                      {this.state.return_data.status !==
                        'Transaksi ini dibatalkan oleh pembeli.' &&
                        this.state.return_data.status !==
                          'Dana dikembalikan' && (
                          <Content style={{margin: hp(2)}}>
                            <Form>
                              <Textarea
                                rowSpan={5}
                                value={this.state.message}
                                bordered
                                placeholder="Masukan pesan"
                                onChangeText={(value) =>
                                  this.setState({message: value})
                                }
                              />
                            </Form>

                            <ProductFileUpload
                              // index={i}
                              loading={this.state.loading}
                              onPress={({type, index, ref, item}) => {
                                ref.close();
                                this.selectFile(type, item);
                              }}
                              fileName={this.state.fileNames}
                            />

                            <View style={{marginVertical: hp(1)}}>
                              <Button
                                rounded
                                danger
                                onPress={() => {
                                  this.onSubmit();
                                }}>
                                <TextNative> Kirim Pesan </TextNative>
                              </Button>
                            </View>
                          </Content>
                        )}
                    </ScrollView>
                  </View>
                </View>
              )}
            </ScrollView>
          </View>
        ) : (
          <View>
            <ScrollView style={styles.bgColor}>
              {this.state.return_data && (
                <View>
                  <View style={styles.addressWrapper}>
                    <View style={styles.addressDiv}>
                      <View style={styles.flexCss}>
                        <Text style={styles.billingAddress}>
                          INFORMASI PEMBELI
                        </Text>
                        <Text style={styles.productName2}>
                          {this.state.return_data.data?.user_name}
                        </Text>

                        <View style={styles.iconBox}>
                          <Image
                            style={styles.icon}
                            source={require('../images/phIcon.png')}
                          />
                          <Text style={styles.iconTxt}>
                            {this.state.return_data.data.user_isd_phone_code +
                              this.state.return_data.data.user_phone}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>

                  <View>
                    <ScrollView
                      horizontal={true}
                      style={styles.tableWrapperOuter}>
                      <View style={styles.tableWrapper}>
                        <View style={styles.tableHeader}>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Nomor Permintaan Pengembalian
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Produk
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Jumlah
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Komentar
                          </Text>
                        </View>
                        <View style={styles.tableRow}>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.cancel_id}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.product_name}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.data?.opr_qty}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.comments}
                          </Text>
                        </View>
                      </View>
                    </ScrollView>
                  </View>
                  <CardSection />
                  <View>
                    <ScrollView
                      horizontal={true}
                      style={styles.tableWrapperOuter}>
                      <View style={styles.tableWrapper}>
                        <View style={styles.tableHeader}>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Alasan
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Tanggal
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            STATUS
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Jumlah
                          </Text>
                        </View>
                        <View style={styles.tableRow}>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.cancel_reason}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.date}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            {this.state.return_data.status}
                          </Text>
                          <Text style={[styles.statusDetail, {width: '25%'}]}>
                            Rp.{'  '}
                            {this.state.return_data.data?.opr_net_charged}
                          </Text>
                        </View>
                      </View>
                    </ScrollView>
                  </View>
                </View>
              )}
            </ScrollView>
          </View>
        )}
      </>
    );
  }
}

const styles = {
  cardButton: {
    backgroundColor: '#fff',
    // paddingHorizontal: wp(3),
    marginBottom: hp(1),
  },
  cardAction: {
    alignItems: 'center',
    paddingTop: hp(1),
    borderTopColor: '#f5f6fa',
    borderTopWidth: 1,
    marginTop: hp(1),
    justifyContent: 'center',
  },
  cardActionButton: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: hp(1),
    paddingBottom: hp(2),
  },
  btnFont: {
    fontSize: hp('1.6%'),
    fontFamily: Font.RobotoRegular,
  },
  btnDiv: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: hp('2%'),
    marginBottom: hp('2%'),
  },
  radioWrappper: {
    flexDirection: 'row',
    paddingTop: 8,
    paddingBottom: 8,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  labelstyle: {
    fontSize: hp('2.5%'),
    color: '#545454',
    marginRight: 10,
    marginLeft: 0,
    fontFamily: Font.RobotoRegular,
  },
  heading_style: {
    color: '#545454',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
  },
  continueBtn: {
    bottom: 0,
    width: '100%',
    // position: 'absolute',
    padding: 20,
    paddingBottom: 10,
    marginBottom: 20,
    paddingLeft: 35,
    paddingRight: 35,
  },
  dropdownClass: {
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  pickerClass: {
    height: hp('6%'),
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  statusDetail: {
    borderRightColor: '#dedddd',
    borderRightWidth: 1,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 8,
  },
  tableWrapper: {
    borderColor: '#f1f0f0',
    borderWidth: 1,
    width: 600,
  },
  tableWrapperOuter: {
    width: '100%',
    overFlow: 'scroll',
    borderWidth: 1,
    borderColor: '#dedddd',
    zIndex: 1001,
    // marginLeft: 10,
    // marginRight: 10
  },
  tableHeader: {
    flexDirection: 'row',
    borderBottomColor: '#dedddd',
    borderBottomWidth: 1,
    backgroundColor: '#f1f0f0',
  },
  tableRow: {
    flexDirection: 'row',
    borderBottomColor: '#dedddd',
  },
  AcccontentWrapper: {
    padding: 10,
    borderBottomColor: '#e7e7e7',
    borderBottomWidth: 1,
  },
  iconAddress: {
    width: 13,
    height: 18,
    marginTop: 3,
  },
  addressWrapper: {
    marginTop: hp('1%'),
    // borderBottomWidth:1,
    // borderColor:'#e7e7e7',
    // paddingBottom: hp('0.5'),
    paddingHorizontal: 10,
  },
  addressDiv: {
    paddingBottom: hp(0.5),
    marginBottom: hp(0.5),
    borderBottomWidth: 1,
    borderBottomColor: '#e7e7e7',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  billingAddress: {
    fontSize: wp('4.3%'),
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    marginBottom: hp('1%'),
  },
  billingAddress1: {
    fontSize: wp('4.3%'),
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    marginBottom: hp('1%'),
    padding: wp('2%'),
  },
  productName2: {
    fontSize: wp('4.2%'),
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },
  iconBox: {
    flexDirection: 'row',
    marginTop: 5,
    // alignItems:'center',
  },
  iconEmail: {
    width: 14,
    height: 11,
    marginTop: 4,
  },
  iconTxt: {
    fontSize: wp('4%'),
    marginLeft: 8,
  },
  btn_style: {
    paddingLeft: 10,
    paddingRight: 10,
    width: 'auto',
    marginTop: hp('3%'),
  },
  bgColor: {
    backgroundColor: '#fff',
  },
  boxStyle: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    paddingBottom: 0,
    flexDirection: 'row',
    flexWarp: 'warp',
    position: 'relative',
  },
  imgContentWrapper: {
    flexDirection: 'row',
    // flexBasis:'80%',
    flex: 1,
    paddingRight: 15,
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    paddingBottom: 10,
  },
  boxImg: {
    width: wp('20%'),
    height: wp('32%'),
  },
  imgWrapper: {
    width: wp('20%'),
    marginRight: wp('3%'),
  },
  available: {
    color: '#858585',
    fontSize: hp('2%'),
    marginRight: wp('10%'),
    // fontFamily:Font.RobotoMedium,
  },
  price: {
    color: '#00b3ff',
    fontSize: wp('5%'),
    // fontFamily:Font.RobotoBold,
  },
  availableWrapper: {
    flexDirection: 'row',
    paddingTop: hp('0.4%'),
    paddingBottom: hp('0.4%'),
  },
  iconName: {
    fontSize: 16,
    color: '#000',
    justifyContent: 'center',
    margin: wp(2),
  },
  chatIcon: {
    width: 50,
    height: 50,
    borderRadius: 50,
  },

  bodyRighChat: {
    fontSize: 16,
    color: '#fff',
  },

  rightChat: {
    backgroundColor: '#0078fe',
    padding: 10,
    marginLeft: '45%',
    borderRadius: 25,
    marginTop: 5,
    marginRight: '5%',
    maxWidth: '50%',
    alignSelf: 'flex-end',
  },

  rightArrow: {
    position: 'absolute',
    backgroundColor: '#0078fe',
    //backgroundColor:"red",
    width: 20,
    height: 25,
    bottom: 0,
    borderBottomLeftRadius: 25,
    right: -10,
  },

  rightArrowOverlap: {
    position: 'absolute',
    backgroundColor: '#fff',
    //backgroundColor:"green",
    width: 20,
    height: 35,
    bottom: -6,
    borderBottomLeftRadius: 18,
    right: -20,
  },

  /*Arrow head for recevied messages*/
  leftArrow: {
    position: 'absolute',
    backgroundColor: '#dedede',
    //backgroundColor:"red",
    width: 20,
    height: 25,
    bottom: 0,
    borderBottomRightRadius: 25,
    left: -10,
  },

  leftArrowOverlap: {
    position: 'absolute',
    backgroundColor: '#fff',
    width: 20,
    height: 35,
    bottom: -6,
    borderBottomRightRadius: 18,
    left: -20,
  },
  leftChat: {
    backgroundColor: '#dedede',
    padding: 10,
    borderRadius: wp(10),
    marginTop: 5,
    marginLeft: '5%',
    maxWidth: '50%',
    alignSelf: 'flex-start',
  },
  bodyLeftChat: {
    fontSize: 16,
    color: '#000',
    justifyContent: 'center',
  },
  bodyRightDateChat: {
    fontSize: 10,
    color: '#fff',
  },
  bodyLeftDateChat: {
    fontSize: 10,
    color: '#000',
    justifyContent: 'center',
  },
  touchimgChangeIcon: {
    position: 'absolute',
    width: wp('10%'),
    height: wp('10%'),
    right: -20,
    top: 20,
  },
  imgChangeIcon: {
    width: wp('10%'),
    height: wp('10%'),
  },
};

export default connect()(DisputeManagementView);
