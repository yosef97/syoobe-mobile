import React, {Component} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SearchComponent from './SearchComponent';
import HomePageLogo from './HomePageLogo';

export default class SearchResult extends Component {
  static navigationOptions = ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
    headerTitle: (
      <TouchableOpacity
        style={{
          width: wp('33%'),
          height: wp('4.5%'),
          marginLeft: 'auto',
          marginRight: 'auto',
        }}>
        <HomePageLogo />
      </TouchableOpacity>
    ),
  });

  render() {
    return (
      <View>
        <SearchComponent />
      </View>
    );
  }
}
