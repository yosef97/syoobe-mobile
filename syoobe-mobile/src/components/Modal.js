import React, {Fragment} from 'react';
import { Text, View, TouchableWithoutFeedback, StyleSheet, Modal} from 'react-native';
import { Spinner } from './common';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Font } from './Font';

const modal = props => (
    <Modal
        animationType="fade"
        transparent={true}
        visible={props.showModalState}
        onRequestClose={props.changeModalState}
    >
        <TouchableWithoutFeedback onPress={props.changeModalState}>
        <View style={styles.outerModal}
        >
            <TouchableWithoutFeedback>
                <View style={styles.innerModal}>
                {
                    !props.loadingModal &&
                    <Text style={styles.modalHeadingStyle}> Select </Text>
                }
                {
                    props.loadingModal ?
                    <Spinner color={'red'} />
                    :
                    <Fragment>
                        {props.children}
                    </Fragment>
                }
                </View>
            </TouchableWithoutFeedback>
        </View>
        </TouchableWithoutFeedback>
    </Modal>
)

const styles = StyleSheet.create({
    modalHeadingStyle:{
        color:'#545454',
        fontSize:hp("2.5%"),
        fontFamily: Font.RobotoRegular,
        textAlign: 'center',
    },
    innerModal:{
        width: wp('80%'),
        minHeight: hp('20%'),
        backgroundColor: '#fff',
        padding: 10,
        zIndex:5,
        position:'relative',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    outerModal:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
})

export default modal;
