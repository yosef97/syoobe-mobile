import React, {Component} from 'react';
import {
  TouchableOpacity,
  Alert,
  AsyncStorage,
  Text,
  View,
  StatusBar,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {TextInput} from 'react-native';
import axios from 'axios';
import {formatRupiah} from '../helpers/helper';
import {PaymentOptions} from './common/payment/PaymentOptions';
import {Spinner} from './common';

export default class AddFunds extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      checked: this.props.route.params.checked,
      innerChecked: null,
      _token: null,
      text: '0',
      note: '',
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = async () => {
    await this.retrieveToken();
  };

  addFunds = async (route, params) => {
    this.setState({
      loading: true,
    });
    let details = {
      _token: this.state._token,
      ptype: this.state.checked,
      amount: this.state.text,
    };

    if (this.state.note.toString().trim() !== '') {
      details = {
        ...details,
        ...{
          desc: this.state.note,
        },
      };
    }
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/addFundPrepaid', formBody)
      .then((response) => {
        this.setState({
          loading: false,
        });
        console.log('selectAddFundSection', response);
        if (response.data.status == 1) {
          this.props.navigation.navigate(route, {
            data: {
              order_id: response.data.order_id,
              total_amount: response.data.amount || this.state.text,
              costum_field1: response.data.payKey,
            },
            _token: this.state._token,
            payable_amount: response.data.amount || this.state.text,
            // postageDetails: this.props.route.params.postageDetails,
            ...params,
            from_wallet: true,
            isWallet: true,
          });
        } else {
          Alert.alert('Terjadi kesalahan, silahkan coba lagi?');
        }
      });
  };
  showToast = () => {
    Alert.alert('Silahkan pilih atau masukkan nominal terlebih dahulu.');
  };

  render() {
    console.log('ini state', this.state)
    const {checked, innerChecked} = this.state;
    const isAmount = formatRupiah(this.state.text, false) < 10000;
    const isFourth = innerChecked === 'fourth';
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner size={'large'} color={'red'} />
        </View>
      );
    }
    return (
      <View style={{flex: 1, backgroundColor: '#f5f6fa'}}>
        <View style={{flex: 1}}>
          <View
            style={{
              backgroundColor: '#fff',
              paddingHorizontal: wp(3),
              paddingVertical: hp(2),
              marginBottom: hp(2),
            }}>
            <Text
              style={{
                paddingVertical: hp(2),
                fontWeight: 'bold',
                fontSize: hp(2),
              }}>
              Pilih Nominal Top Up
            </Text>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() =>
                  this.setState({innerChecked: 'first', text: 20000})
                }>
                <View
                  style={{
                    ...styles.buttonAmount,
                    borderColor:
                      innerChecked === 'first' ? '#e84118' : '#eaeaea',
                  }}>
                  <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                    Rp. 20.000
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() =>
                  this.setState({innerChecked: 'second', text: 50000})
                }>
                <View
                  style={{
                    ...styles.buttonAmount,
                    borderColor:
                      innerChecked === 'second' ? '#e84118' : '#eaeaea',
                  }}>
                  <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                    Rp. 50.000
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() =>
                  this.setState({innerChecked: 'third', text: 100000})
                }>
                <View
                  style={{
                    ...styles.buttonAmount,
                    borderColor:
                      innerChecked === 'third' ? '#e84118' : '#eaeaea',
                  }}>
                  <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                    Rp. 100.000
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{paddingTop: hp(2)}}>
              <Text
                style={{paddingBottom: 5, fontWeight: 'bold', fontSize: hp(2)}}>
                Masukkan Nominal
              </Text>
              <TextInput
                style={{
                  backgroundColor: '#f1f2f6',
                  borderRadius: 5,
                  paddingHorizontal: wp(3),
                  // marginBottom: hp(2),
                }}
                onChangeText={(e) =>
                  this.setState({innerChecked: 'fourth', text: e})
                }
                value={
                  innerChecked === 'fourth' ? formatRupiah(this.state.text) : ''
                }
                keyboardType={'numeric'}
                placeholder={'Minimal Rp 10.000'}
              />
              {isAmount && isFourth ? (
                <Text style={{fontSize: 12, color: 'red'}}>
                  Minimal Pengisian saldo adalah Rp 10.000
                </Text>
              ) : null}
            </View>
          </View>
          <View
            style={{
              backgroundColor: '#fff',
              paddingHorizontal: wp(3),
            }}>
            <View style={{paddingTop: hp(2)}}>
              <Text
                style={{paddingBottom: 5, fontWeight: 'bold', fontSize: hp(2)}}>
                Tambahkan Catatan
              </Text>
              <TextInput
                style={{
                  backgroundColor: '#f1f2f6',
                  borderRadius: 5,
                  paddingHorizontal: wp(3),
                  marginBottom: hp(2),
                }}
                onChangeText={(e) => this.setState({note: e})}
                value={this.state.note}
                // keyboardType={'numeric'}
                placeholder={'Catatan (opsional)'}
              />
            </View>
          </View>
        </View>
        <View style={{padding: wp(3)}}>
          <PaymentOptions
            onPressPayment={(route, params) => this.addFunds(route, params)}
            onPress={({ref}) => (!isAmount ? ref.open() : this.showToast())}
            payable_amount={formatRupiah(this.state.text, false)}
            product_type={'W'}
            productData={[
              {
                id: 1,
                product_name: 'Top Up e-Wallet',
                product_total: formatRupiah(this.state.text, false),
                store_name: 'Syoobe',
              },
            ]}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  buttonAmount: {
    borderColor: '#eaeaea',
    borderWidth: 2,
    width: wp(30),
    height: hp(5),
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noteStyle: {
    marginTop: hp('2%'),
    backgroundColor: '#dff0d8',
  },
  noteStyleText: {
    color: '#0b5014',
  },
  bgColor: {
    backgroundColor: '#fff',
    padding: 10,
  },
  selectDestination: {
    padding: 15,
    paddingLeft: 10,
    color: '#000',
    fontSize: hp('3%'),
    fontFamily: Font.RobotoMedium,
  },
  checkoutPanel: {
    borderWidth: 1,
    borderColor: '#e8e9ea',
    padding: 10,
    paddingBottom: 15,
    marginBottom: 15,
  },
  amountTxt: {
    color: '#0c0c0c',
    fontSize: hp('2.5%'),
    paddingBottom: 10,
    marginBottom: 10,
    fontFamily: Font.RobotoRegular,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9ea',
  },
  rowView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rowView2: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginBottom: 10,
  },
  heading_style: {
    color: '#545353',
    fontSize: hp('1.9%'),
    fontFamily: Font.RobotoMedium,
    marginLeft: 5,
  },
  heading_style2: {
    color: '#545353',
    fontSize: hp('2.4%'),
    fontFamily: Font.RobotoMedium,
    marginLeft: 5,
  },
  otherPrice: {
    color: '#545353',
    fontSize: hp('2.2%'),
    fontFamily: Font.RobotoMedium,
    marginRight: 10,
  },
  otherInput: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  totalItem: {
    color: '#484848',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
    marginBottom: 10,
  },
  netPayable: {
    color: '#000',
    fontFamily: Font.RobotoMedium,
    fontSize: hp('2.2%'),
  },
  continueBtn: {
    padding: 20,
    // backgroundColor: '#f1f0f0',
    paddingBottom: 10,
    marginTop: 15,
    paddingLeft: 35,
    paddingRight: 35,
  },
};
