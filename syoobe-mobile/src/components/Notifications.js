// import {changeProductStatus, deleteSellerProduct} from '../actions';
import React, {Component} from 'react';
import {StatusBar, View} from 'react-native';
// import {connect} from 'react-redux';
import {NotificationTab} from './routes/NotificationTab';

class Notifications extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    // console.log(this.props);
    return (
      <View style={styles.containerStyle}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <View style={{flex: 1}}>
          <NotificationTab test={'ok'} />
        </View>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    padding: 0,
    flex: 1,
    backgroundColor: '#fff',
  },
};

export default Notifications;
