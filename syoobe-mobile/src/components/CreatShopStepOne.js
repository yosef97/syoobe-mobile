import React, {Component} from 'react';
import {
  StatusBar,
  AsyncStorage,
  Text,
  View,
  ScrollView,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {CardSection, Spinner, Button} from './common';
import {Font} from './Font';
import {connect} from 'react-redux';
import {getShopDetails, changeShopName, changeUrlKeywords} from '../actions';
import InputText from './common/InputText';
import {getSlug} from '../helpers/helper';

class CreatShopStepOne extends Component {
  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount() {
    this.retrieveToken().then((_token) => {
      this.props.getShopDetails(_token);
    });
  }

  changeShopName = (text) => {
    this.props.changeShopName(text);
    this.props.changeUrlKeywords(getSlug(text));
  };

  changeUrlKeywords = (text) => {
    this.props.changeUrlKeywords(text);
  };

  goToStep2 = () => {
    if (this.props.shop_name === '') {
      Alert.alert('Informasi', 'Nama toko tidak boleh kosong', [{text: 'OK'}]);
    } else if (this.props.url_keywords === '') {
      Alert.alert('Informasi', 'Silahkan Masukkan Kata Kunci Toko', [
        {text: 'OK'},
      ]);
    } else {
      this.props.navigation.navigate('CreatShopStepTwo');
    }
  };

  render() {
    // console.log('in state', JSON.stringify(this.state));
    console.log('in props', JSON.stringify(this.props));
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color="red" size="large" />
        </View>
      );
    }
    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          style={styles.scrollClass}>
          <Text style={styles.shopHeading}>Informasi Toko</Text>
          <View style={styles.viewStyle}>
            <View style={{flex: 1}}>
              <CardSection>
                <InputText
                  value={this.props.shop_name}
                  onChangeText={this.changeShopName.bind(this)}
                  placeholder=""
                  label="Nama Toko"
                />
              </CardSection>
              <CardSection>
                <InputText
                  value={getSlug(this.props.url_keywords)}
                  // onChangeText={this.changeUrlKeywords.bind(this)}
                  placeholder=""
                  label="Kata kunci URL"
                />
              </CardSection>
            </View>
            <Text style={styles.errorTextStyle}></Text>
            <Button onPress={() => this.goToStep2()}>Lanjut</Button>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = {
  errorTextStyle: {
    fontSize: wp('4%'),
    alignSelf: 'center',
    // color: 'red',
    fontFamily: Font.RobotoRegular,
  },
  bgImg: {
    height: '100%',
  },
  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
    // padding:10
  },
  shopHeading: {
    fontSize: wp('6%'),
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 10,
    color: '#696969',
    fontFamily: Font.RobotoRegular,
  },
  viewStyle: {
    marginleft: 5,
    marginRight: 5,
    paddingLeft: 10,
    paddingRight: 10,
  },
};

const mapStateToProps = (state) => {
  return {
    shop_name: state.createShop.shop_name,
    url_keywords: state.createShop.url_keywords,
    loading: state.createShop.loading,
  };
};

export default connect(mapStateToProps, {
  getShopDetails,
  changeShopName,
  changeUrlKeywords,
})(CreatShopStepOne);
