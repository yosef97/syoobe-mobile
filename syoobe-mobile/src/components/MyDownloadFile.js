import Axios from 'axios';
import React, {Component} from 'react';
import {
  AsyncStorage,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import PTRView from 'react-native-pull-to-refresh';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {connect} from 'react-redux';
import {getBuyerDownloads} from '../actions';
import {formatRupiah} from '../helpers/helper';

class MyDownloadFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _token: null,
      loading: false,
      donwloadPageNo: 1,
      loaderForDownloadDetails: false,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = () => {
    this.fetchData();
  };

  fetchData = async () => {
    await this.setState({loading: true});
    this.retrieveToken().then((_token) => {
      let details = {
        _token,
        // page: this.state.donwloadPageNo,
      };
      this.props.getBuyerDownloads(details);
      this.setState({loading: false});
    });
  };

  getDownloadDetails = (id) => {
    this.setState({
      loaderForDownloadDetails: true,
    });
    this.retrieveToken().then((_token) => {
      let details = {
        _token,
        order_product_id: id,
      };
      let formBody = [];
      for (let property in details) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      Axios.post('https://syoobe.co.id/api/mydownloadsorderview', formBody)
        .then((response) => {
          console.log(response.data);
          this.setState({
            loaderForDownloadDetails: false,
          });
          if (response.data.status == 1) {
            let downloadDetails = response.data.download_order_view;
            this.props.navigation.navigate('ProductDetailsInfo', {
              pageHeader: 'File Details',
              file_details: downloadDetails,
            });
          } else {
            Alert.alert('File tidak ditemukan');
          }
        })
        .catch((err) => {
          console.log(err);
          Alert.alert('Terjadi kesalahan, silahkan coba lagi!');
          this.setState({
            loaderForDownloadDetails: false,
          });
        });
    });
  };
  render() {
    console.log('ini props', JSON.stringify(this.props));
    return (
      <View style={{flex: 1, backgroundColor: '#f5f6fa'}}>
        <PTRView onRefresh={() => this.fetchData()}>
          <FlatList
            data={this.props.download_list}
            showsVerticalScrollIndicator={false}
            renderItem={({item, index}) => (
              <TouchableOpacity
                key={index}
                activeOpacity={0.5}
                onPress={() => this.getDownloadDetails(item.order_product_id)}>
                <View style={styles.cardContainer}>
                  <View style={{flexDirection: 'row', paddingTop: hp(1)}}>
                    <View style={styles.image}>
                      <Image
                        style={{width: '100%', height: '100%'}}
                        source={{uri: item.product_image}}
                      />
                    </View>
                    <View style={{flex: 1, paddingLeft: wp(3)}}>
                      <Text style={styles.textTitle}>{item.product_name}</Text>
                      <View style={styles.textContainerPrice}>
                        <Text style={styles.textPrice}>{`Rp. ${formatRupiah(
                          item.product_price,
                        )}`}</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.cardItems}>
                    <Text
                      style={{
                        ...styles.textPrice,
                        fontWeight: 'normal',
                        color: '#000',
                      }}>
                      Tanggal Pembelian
                    </Text>
                    <Text
                      style={{
                        ...styles.textPrice,
                        fontWeight: 'normal',
                        color: '#000',
                      }}>
                      {item.order_placed_on}
                    </Text>
                  </View>

                  <View style={styles.cardItems}>
                    <Text
                      style={{
                        ...styles.textPrice,
                        fontWeight: 'normal',
                        color: '#000',
                      }}>
                      No Faktur
                    </Text>
                    <Text
                      style={{
                        ...styles.textPrice,
                        fontWeight: 'normal',
                        color: '#000',
                      }}>
                      {` ${item.order_invoice}`}
                    </Text>
                  </View>

                  <View style={styles.cardItems}>
                    <Text style={{...styles.textPrice, color: '#000'}}>
                      Lihat Rincian
                    </Text>
                    <FontAwesome5Icon name="chevron-right" size={hp(2)} />
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </PTRView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: '#fff',
    paddingVertical: hp(1),
    paddingHorizontal: wp(3),
    marginVertical: hp(1),
  },
  image: {
    width: wp(18),
    height: hp(9),
    maxHeight: hp(9),
    maxWidth: wp(18),
    backgroundColor: '#eaeaea',
  },
  textTitle: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#353b48',
  },
  textContainerPrice: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textPrice: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#e84118',
  },
  textPriceQty: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#353b48',
  },
  cardItems: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: hp(1),
    borderTopColor: '#f5f6fa',
    borderTopWidth: 1,
    marginTop: hp(1),
  },
});

const mapStateToProps = (state) => ({
  loading: state.buyerOrders.loading,
  download_list: state.buyerOrders.download_list,
  total_page_downloads: state.buyerOrders.total_page_downloads,
  endOfDownloads: state.buyerOrders.endOfDownloads,
  no_downloads: state.buyerOrders.no_downloads,
});

export default connect(mapStateToProps, {getBuyerDownloads})(MyDownloadFile);
