import React, {Component} from 'react';
import {WebView} from 'react-native-webview';
import {AsyncStorage} from 'react-native';
import base64 from 'react-native-base64';

export class ChatWebViewNavigate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      userSelected: this.props.route.params.userSelected,
      needToDecode: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('needToDecode').then((res) =>
      this.setState({needToDecode: res}),
    );

    AsyncStorage.getItem('emailStore').then((res) =>
      this.setState({email: res}),
    );

    AsyncStorage.getItem('passwordStore').then((res) =>
      this.setState({password: res}),
    );
  }

  render() {
    const userSelected = this.state.userSelected;
    const passwordDecode = base64.decode(this.state.password);

    console.log(passwordDecode, 'ini decode', this.state.needToDecode);

    return (
      <>
        {this.state.needToDecode === 'true' ? (
          <WebView
            source={{
              uri: `https://syoobe.co.id/pesan/?email=${this.state.email}&password=${passwordDecode}&userSelected=${userSelected}`,
            }}
          />
        ) : (
          <WebView
            source={{
              uri: `https://syoobe.co.id/pesan/?email=${this.state.email}&password=${this.state.password}&userSelected=${userSelected}`,
            }}
          />
        )}
      </>
    );
  }
}

export default ChatWebViewNavigate;
