import React, {Component} from 'react';
import {
  Keyboard,
  Modal,
  Image,
  AsyncStorage,
  StatusBar,
  Picker,
  View,
  Alert,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {WebView} from 'react-native-webview';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Text, Button, Segment, DatePicker} from 'native-base';
import {
  InputInner,
  CardSection,
  TextareaInner,
  ButtonTwo,
  Spinner,
  Button as Btn,
} from './common';
// import CheckBoxComponent from './CheckBoxComponent';
import {Font} from './Font';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import DocumentPicker from 'react-native-document-picker';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from 'accordion-collapse-react-native';
// import ActionButton from 'react-native-action-button';
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Checkbox} from 'react-native-paper';
import {
  setMainImageDone,
  emptyProductData,
  editProductView,
  loadCompatibilityContentDropdown,
  loadCompatibilityContent,
  loadCompatibilities,
  loadOptions,
  createNewOption,
  getRelatedProducts,
  getProdcutFilters,
  getSpecifications,
  saveProduct,
  removeGeneralImages,
  delete_image_general,
  upload_images_general,
  getCarrierService,
  getShippingCompany,
  getShipsToCountries,
  getCountries,
  getProcessingTime,
  getBrands,
  getCategoryList,
  getProductFee,
} from '../actions';
import axios from 'axios';
import {BASE_URL} from '../services/baseUrl';
import {ERROR_MESSAGE} from '../constants/constants';
import {ProductType} from './common/product/ProductType';
import {TextInputCostum} from './common/TextInput';
import {countNumber, formatRupiah, getSlug} from '../helpers/helper';
import {ProductPrice} from './common/product/ProductPrice';
import {FakeProductPrice} from './common/product/FakeProductPrice';
import {ProductBrand} from './common/product/ProductBrand';
import {ProductCategory} from './common/product/ProductCategory';
import {ProductStatus} from './common/product/ProductStatus';
import {ProductImage} from './common/product/ProductImage';
import {ProductLength} from './common/product/ProductLength';
import {ProductWeight} from './common/product/ProductWeight';
import {ProductCountry} from './common/product/ProductCountry';
import {ProductUploadMethod} from './common/product/ProductUploadMethod';
import {ProductFileUpload} from './common/product/ProductFileUpload';
import {ProductProcessingTime} from './common/product/ProductProcessingTime';
import {ProductDropdown} from './common/product/ProductDropdown';
import {InputDate} from './common/InputDate';
import {ProductDinamicDropdown} from './common/product/ProductDinamicDropdown';
import {ProductCompanyShipping} from './common/product/shipping/ProductCompanyShipping';
import {ProductServiceShipping} from './common/product/shipping/ProductServiceShipping';
import {FakeServiceShipping} from './common/product/shipping/FakeServiceShipping';
import Bugsnag from '@bugsnag/react-native';

class AddProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      prod_id: props.route.params && props.route.params.prod_id,
      _token: null,
      user_id: null,
      seg: 1,
      segInner: 1,
      segInner2: 1,
      generalTab: 1,
      checked: false,
      tags: '',
      meta_tag_title: '',
      meta_tag_description: '',
      meta_tag_keywords: '',
      requires_type: 'p',
      product_title: '',
      url_keywords: '',
      selling_price: '',
      selling_price_fee: '',
      selling_price_total: '',
      prod_admin_commission: '',
      prod_admin_amount: '',
      prod_midtrans_calculation: '',
      prod_midtrans_commssion: '',
      quantity: 1,
      minimum_quantity: 1,
      model: '',
      sku: '',
      product_description: '',
      PickerSelectedBrand: 'Select Brand',
      PickerSelectedBrandId: 0,
      PickerSelectedCategory: 'Select Category',
      PickerSelectedCategoryId: 0,
      PickerSelectProductCondition: '',
      PickerSelectedUploadMethod: 0,
      PickerSelectedProcessingTime: 'Select Processing Time',
      PickerSelectedProcessingTimeId: 0,
      general_photos: [],
      physicalLength: '',
      physicalHeight: '',
      physicalWidth: '',
      PickerSelectLengthClass: 'cm',
      PickerSelectWeightClass: 'kg',
      PickerSelectedShippingCountryId: 106,
      product_weight: '',
      physicalCost: '',
      physicalAdditionalItem: '',
      imagesLength: 0,
      generalInstantArray: [
        {
          extension_name: '',
          fileurl: '',
          validity: '',
          max_download_times: '',
          uploadProgressPercentile: 0,
          download_name: '',
          filename: '',
        },
      ],
      specificationsArray: [
        {
          product_attribute_description: '',
          attribute_id: 0,
          attribute_name: 'Select',
        },
      ],
      // PickerSelectedShipsToCountry: 'Pilih Negara',
      // PickerSelectPhysicalPTime: 'Select Processing Time',
      // PickerSelectedCarrierService: 'Select',
      // PickerSelectedCompany: 'Select Company',
      generalShipArray: [
        {
          cost: '',
          additional_cost: '',
          country_id: 0,
          company_id: 0,
          pship_service_id: 0,
          processing_time_id: 0,
          // processing_time: 'Select Processing Time',
          // pship_service: 'Select',
          // company_name: 'Select Company',
          // country_name: 'Pilih Negara',
          // pship_service: 'Select',
        },
      ],
      returnAccepted: 1,
      validReturnDays: 7,
      shippingPaidBy: 1,
      restockingFee: 0,
      selectedItems: [],
      selectedOptions: [],
      selectedRelatedProducts: [],
      substractStock: 1,
      trackInventory: 0,
      productInStock: 1,
      alertStockLevel: '',
      youtubeLink: '',
      displayOrder: '1',
      dateAvailable: new Date().toISOString().slice(0, 10),
      optionsModal: false,
      optionType: '0',
      optionName: '',
      option_display_order: '',
      optionArray: [{name: '', sort: ''}],
      active_index: '',
      active_option_type: '',
      optionSelectArray: [],
      dmState1: false,
      dmState2: false,
      compatArray: [],
      innerCompatFields: [],
      innerCompatabilityContent: [{data: []}],
      dataToSend: [],
      innerCompatLoading: false,
      innerFieldsArray: [],
      // uploadProgressPercentile: 0,
      DiscountManagementQD: [
        {
          quantity: '',
          priority: '',
          price: '',
          start_date: null,
          end_date: null,
        },
      ],
      DiscountManagementSD: [
        {priority: '', price: '', start_date: null, end_date: null},
      ],
      segmentForwardArrow: true,
      segmentBackArrow: false,
      contentOff: 0,
      generalTabStep1: true,
      generalTabStep2: false,
      groupd_id_compatibility: null,
      _shopId: null,
      getCompatibilityDetails: null,
    };
    this.setDateAvailable = this.setDateAvailable.bind(this);
    this.icons = {
      up: require('../images/arrowDown.png'),
      down: require('../images/arrowUp.png'),
    };
  }

  closeOptionsModal = () => {
    this.setState({
      optionsModal: false,
      optionType: '0',
      optionName: '',
      option_display_order: '',
      optionArray: [{name: '', sort: ''}],
    });
  };

  createNewOption = () => {
    Keyboard.dismiss();
    var details = {};
    if (this.state.optionType == '0') {
      this.alertMessageForSelect('Options Type');
    } else if (this.state.optionName == '') {
      this.alertMessage('Options Name');
    } else {
      details = {
        ...details,
        ...{_token: this.state._token},
        ...{option_type: this.state.optionType},
        ...{option_name: this.state.optionName},
        ...{option_display_order: this.state.option_display_order},
      };
      if (
        this.state.optionType == 'select' ||
        this.state.optionType == 'radio' ||
        this.state.optionType == 'checkbox'
      ) {
        details = {...details, ...{option_values: this.state.optionArray}};
      }
      this.props.createNewOption(details);
    }
  };

  saveAndNext = (value) => {
    this.saveData(value, 'next');
  };

  saveAndExit = (value) => {
    this.saveData(value, 'exit');
  };

  saveData = async (value, task) => {
    var details = {};
    if (
      (value == 'seo' && this.state.meta_tag_title == '') ||
      (value == 'specifications' && this.state.meta_tag_title == '')
    ) {
      this.setStateDynamic(2);
      this.alertMessage('Meta Tag Title');
    } else if (value == 'returns' && this.state.returnAccepted == undefined) {
      this.alertMessageForSelect('Return Accepted');
    } else if (this.state.product_title == '') {
      this.setStateDynamic(1);
      this.alertMessage('Product Title');
    } else if (this.state.url_keywords == '') {
      this.setStateDynamic(1);
      this.alertMessage('Url Keywords');
    } else if (this.state.selling_price == '') {
      this.setStateDynamic(1);
      this.alertMessage('Selling Price');
    } else if (this.state.quantity == '') {
      this.setStateDynamic(1);
      this.alertMessage('Quantity');
    } else if (this.state.minimum_quantity == '') {
      this.setStateDynamic(1);
      this.alertMessage('Minimum Quantity');
    } else if (this.state.model == '') {
      this.setStateDynamic(1);
      this.alertMessage('Model');
    } else if (this.state.sku == '') {
      this.setStateDynamic(1);
      this.alertMessage('SKU');
    } else if (this.state.PickerSelectProductCondition == 'NA') {
      this.setStateDynamic(1);
      this.alertMessageForSelect('Product Condition');
    } else {
      if (value == 'seo') {
        details = {
          ...details,
          ...{prod_meta_title: this.state.meta_tag_title},
        };
        if (this.state.meta_tag_description != '') {
          var add = {prod_meta_description: this.state.meta_tag_description};
          details = {...details, ...add};
        }
        if (this.state.meta_tag_keywords != '') {
          var add = {prod_meta_keywords: this.state.meta_tag_keywords};
          details = {...details, ...add};
        }
        if (this.state.tags != '') {
          var add = {prod_tags: this.state.tags};
          details = {...details, ...add};
        }
      }
      if (value == 'specifications') {
        details = {
          ...details,
          ...{product_attribute: this.state.specificationsArray},
        };
      }
      if (value == 'returns') {
        details = {
          ...details,
          ...{prod_return_option: this.state.returnAccepted},
        };
        if (this.state.validReturnDays != undefined) {
          details = {
            ...details,
            ...{prod_track_return: this.state.validReturnDays},
          };
        }
        if (this.state.shippingPaidBy != undefined) {
          var add = {prod_return_paid_by: this.state.shippingPaidBy};
          details = {...details, ...add};
        }
        if (this.state.restockingFee != undefined) {
          var add = {prod_restocking_fee: this.state.restockingFee};
          details = {...details, ...add};
        }
      }
      if (value == 'data') {
        details = {
          ...details,
          ...{prod_subtract_stock: this.state.substractStock},
        };
        details = {
          ...details,
          ...{prod_track_inventory: this.state.trackInventory},
        };
        if (this.state.alertStockLevel != '') {
          details = {
            ...details,
            ...{prod_threshold_stock_level: this.state.alertStockLevel},
          };
        }
        if (this.state.youtubeLink != '') {
          details = {
            ...details,
            ...{prod_youtube_video: this.state.youtubeLink},
          };
        }
        if (this.state.dateAvailable != null) {
          details = {
            ...details,
            ...{
              prod_available_date: this.state.dateAvailable,
            },
          };
        }
        details = {...details, ...{prod_status: this.state.productInStock}};
        if (this.state.displayOrder != '') {
          details = {
            ...details,
            ...{prod_display_order: this.state.displayOrder},
          };
        }
        if (this.state.selectedItems.length > 0) {
          details = {
            ...details,
            ...{product_filter: this.state.selectedItems},
          };
        }
        if (this.state.selectedRelatedProducts.length > 0) {
          details = {
            ...details,
            ...{product_related: this.state.selectedRelatedProducts},
          };
        }
      }
      if (value == 'discount') {
        if (this.state.DiscountManagementQD.length > 0) {
          details = {
            ...details,
            ...{product_discount: this.state.DiscountManagementQD},
          };
        }
        if (this.state.DiscountManagementSD.length > 0) {
          details = {
            ...details,
            ...{product_special: this.state.DiscountManagementSD},
          };
        }
      }
      if (value == 'option') {
        details = {
          ...details,
          ...{product_option: this.state.optionSelectArray},
        };
      }
      if (value == 'compat') {
        let dataToSave = [...this.state.innerCompatabilityContent];
        for (let i = 0; i < dataToSave.length; i++) {
          for (let j = 0; j < dataToSave[i].data.length; j++) {
            Object.assign(
              dataToSave[i].data[j],
              {com_attribute_id: ''},
              {com_attribute_name: ''},
              {com_model_id: ''},
            );
            dataToSave[i].data[j].com_attribute_id =
              dataToSave[i].data[j].attribute_id;
            dataToSave[i].data[j].com_attribute_name =
              dataToSave[i].data[j].attribute_name;
            dataToSave[i].data[j].com_model_id = dataToSave[i].data[j].value;
            delete dataToSave[i].data[j].attribute_id;
            delete dataToSave[i].data[j].attribute_name;
            delete dataToSave[i].data[j].value;
            delete dataToSave[i].data[j].data;
          }
        }
        details = {
          ...details,
          ...{product_compatibility: dataToSave},
          ...{group_id: this.state.groupd_id_compatibility},
        };
      }
      details = {
        ...details,
        ...{_token: this.state._token},
        ...{prod_name: this.state.product_title},
        ...{seo_url_keyword: this.state.url_keywords},
        ...{prod_sale_price: this.state.selling_price_total},
        ...{prod_price: countNumber(this.state.selling_price)},
        ...{prod_midtrans_calculation: this.state.selling_price_fee},
        ...{prod_admin_commission: this.state.prod_admin_commission},
        ...{prod_admin_amount: this.state.prod_admin_amount},
        ...{prod_midtrans_commssion: this.state.prod_midtrans_commssion},
        ...{prod_stock: this.state.quantity},
        ...{prod_min_order_qty: this.state.minimum_quantity},
        ...{prod_model: this.state.model},
        ...{prod_sku: this.state.sku},
        ...{prod_condition: this.state.PickerSelectProductCondition},
      };
      if (value != 'data') {
        details = {...details, ...{prod_status: this.state.productInStock}};
      }
      if (this.state.product_description != '') {
        var add = {prod_long_desc: this.state.product_description};
        details = {...details, ...add};
      }
      if (this.state.PickerSelectedBrandId != 0) {
        var add = {prod_brand: this.state.PickerSelectedBrandId};
        details = {...details, ...add};
      }
      if (this.state.PickerSelectedCategoryId != 0) {
        var add = {prod_category: this.state.PickerSelectedCategoryId};
        details = {...details, ...add};
      }
      if (this.props.product_image && this.props.product_image.length > 0) {
        var images = Array.from({length: this.props.product_image.length});
        for (var i = 0; i < this.props.product_image.length; i++) {
          images[i] = {
            image_session: this.props.product_image[i].image_session,
            image_id: this.props.product_image[i].image_id,
            image_file: this.props.product_image[i].image_file,
          };
        }
        details = {...details, ...{product_images: images}};
      }
      if (this.state.requires_type == 'p') {
        if (this.state.physicalLength == '') {
          this.setStateDynamic(1);
          this.alertMessage('Product Length');
        } else if (this.state.physicalWidth == '') {
          this.setStateDynamic(1);
          this.alertMessage('Product Width');
        } else if (this.state.physicalHeight == '') {
          this.setStateDynamic(1);
          this.alertMessage('Product Height');
        } else if (this.state.product_weight == '') {
          this.setStateDynamic(1);
          this.alertMessage('Product Weight');
        } else if (this.state.PickerSelectedShippingCountryId == 0) {
          this.setStateDynamic(1);
          this.alertMessageForSelect('Shipping Country');
        } else if (
          this.state.PickerSelectLengthClass == '' ||
          this.state.PickerSelectLengthClass == undefined ||
          this.state.PickerSelectLengthClass == null
        ) {
          this.setStateDynamic(1);
          this.alertMessageForSelect('Length Unit');
        } else if (
          this.state.PickerSelectWeightClass == '' ||
          this.state.PickerSelectWeightClass == undefined ||
          this.state.PickerSelectWeightClass == null
        ) {
          this.setStateDynamic(1);
          this.alertMessageForSelect('Weight Unit');
        } else {
          var hitApi = true;
          if (hitApi) {
            var add1 = {prod_requires_shipping: 1};
            var add2 = {prod_length: this.state.physicalLength};
            var add3 = {prod_width: this.state.physicalWidth};
            var add4 = {prod_height: this.state.physicalHeight};
            var add5 = {prod_weight: this.state.product_weight};
            var add6 = {
              prod_length_class: this.state.PickerSelectLengthClass,
            };
            var add7 = {
              prod_weight_class: this.state.PickerSelectWeightClass,
            };
            var add8 = {
              prod_shipping_country: this.state.PickerSelectedShippingCountryId,
            };
            if (this.state.checked) {
              var add9 = {prod_ship_free: 1};
            } else {
              var add9 = {prod_ship_free: 0};
            }
            if (this.state.prod_id) {
              details = {
                ...details,
                ...{prod_id: this.state.prod_id},
              };
            }
            var add10 = {product_shipping: this.state.generalShipArray};
            details = {
              ...details,
              ...add1,
              ...add2,
              ...add3,
              ...add4,
              ...add5,
              ...add6,
              ...add7,
              ...add8,
              ...add9,
              ...add10,
            };
            this.props.saveProduct(
              details,
              this.props.navigation,
              task,
              this.state.seg,
            );
          }
        }
      } else if (this.state.requires_type == 'd') {
        details = {...details, ...{prod_requires_shipping: 0}};
        if (this.state.PickerSelectedUploadMethod == '1') {
          if (this.state.PickerSelectedProcessingTimeId == '0') {
            this.alertMessageForSelect('waktu memproses');
          } else {
            details = {
              ...details,
              ...{prod_upload_methods: this.state.PickerSelectedUploadMethod},
              ...{
                prod_upload_processing_manual_id: this.state
                  .PickerSelectedProcessingTimeId,
              },
            };
            if (this.state.prod_id) {
              details = {
                ...details,
                ...{prod_id: this.state.prod_id},
              };
            }
            this.props.saveProduct(
              details,
              this.props.navigation,
              task,
              this.state.seg,
            );
          }
        } else if (this.state.PickerSelectedUploadMethod == '0') {
          //Instant Download
          var hitApi = true;
          for (var i = 0; i < this.state.generalInstantArray.length; i++) {
            if (this.state.generalInstantArray[i].download_name == '') {
              this.alertMessage('Nama Unduhan');
              hitApi = false;
              break;
            } else if (
              this.state.generalInstantArray[i].max_download_times == ''
            ) {
              this.alertMessage('Nama Unduhan Maks');
              hitApi = false;
              break;
            } else if (this.state.generalInstantArray[i].validity == '') {
              this.alertMessage('Hari Validitas');
              hitApi = false;
              break;
            }
          }
          if (hitApi) {
            details = {
              ...details,
              ...{prod_upload_methods: this.state.PickerSelectedUploadMethod},
              ...{product_download: this.state.generalInstantArray},
            };
            if (this.state.prod_id) {
              details = {
                ...details,
                ...{prod_id: this.state.prod_id},
              };
            }
            this.props.saveProduct(
              details,
              this.props.navigation,
              task,
              this.state.seg,
            );
          }
        }
      }
    }
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      const userId = await AsyncStorage.getItem('user_id');
      const shopId = await AsyncStorage.getItem('shop_id');
      this.setState({
        user_id: userId,
        _token: userToken,
        _shopId: shopId,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  setEditData = async () => {
    const {
      final_compatibility,
      prod_requires_shipping,
      prod_name,
      seo_url_keyword,
      prod_min_order_qty,
      prod_model,
      prod_sku,
      prod_long_desc,
      prod_length,
      prod_width,
      prod_height,
      brand_id,
      prod_category,
      prod_condition,
      prod_weight_class,
      prod_length_class,
      prod_weight,
      prod_stock,
      prod_shipping_country,
      prod_ship_free,
      prod_meta_title,
      prod_meta_keywords,
      prod_tags,
      prod_meta_description,
      product_shipping_rates,
      prod_youtube_video,
      prod_subtract_stock,
      prod_track_inventory,
      prod_threshold_stock_level,
      prod_available_date,
      prod_status,
      prod_display_order,
      prod_upload_methods,
      prod_upload_processing_manual_id,
      digital_products,
      product_filters,
      products_related,
      product_attributes,
      prod_price,
      product_options,
      prod_return_option,
      prod_track_return,
      prod_return_paid_by,
      prod_restocking_fee,
      product_discounts,
      product_specials,
      // prod_sale_price,
    } = this.props.product_data;
    // const {groupd_id_compatibility} = this.props.product_data.compatibility_list[0].group_id

    // console.log('prod_sale_price', prod_sale_price)

    let selectedOptionIdFromEdit = [];
    if (product_options) {
      product_options.map((product) => {
        selectedOptionIdFromEdit.push(product.option_id);
      });
    }
    await this.setState({
      requires_type: prod_requires_shipping == 1 ? 'p' : 'd',
      product_title: prod_name,
      url_keywords: seo_url_keyword,
      // selling_price: prod_sale_price,
      minimum_quantity: prod_min_order_qty,
      model: prod_model,
      sku: prod_sku,
      product_description: prod_long_desc,
      physicalLength: prod_length,
      physicalWidth: prod_width,
      physicalHeight: prod_height,
      PickerSelectedBrandId: brand_id,
      PickerSelectedCategoryId: prod_category,
      PickerSelectProductCondition: prod_condition,
      PickerSelectWeightClass: prod_weight_class,
      PickerSelectLengthClass: prod_length_class,
      product_weight: prod_weight,
      quantity: prod_stock,
      PickerSelectedShippingCountryId: prod_shipping_country,
      checked: prod_ship_free == 1 ? true : false,
      meta_tag_title: prod_meta_title,
      meta_tag_description: prod_meta_description,
      meta_tag_keywords: prod_meta_keywords,
      tags: prod_tags,
      generalShipArray: product_shipping_rates,
      PickerSelectedUploadMethod: prod_upload_methods,
      PickerSelectedProcessingTimeId: prod_upload_processing_manual_id,
      generalInstantArray: digital_products,
      youtubeLink: prod_youtube_video,
      substractStock: prod_subtract_stock,
      trackInventory: prod_track_inventory,
      alertStockLevel: prod_threshold_stock_level,
      dateAvailable: new Date().toISOString().slice(0, 10),
      productInStock: prod_status,
      displayOrder: prod_display_order,
      selectedItems: product_filters,
      selectedRelatedProducts: products_related,
      specificationsArray: product_attributes,
      optionSelectArray: product_options,
      selectedOptions: selectedOptionIdFromEdit,
      returnAccepted: prod_return_option,
      validReturnDays: prod_track_return,
      shippingPaidBy: prod_return_paid_by,
      restockingFee: prod_restocking_fee,
      DiscountManagementQD: product_discounts,
      DiscountManagementSD: product_specials,
      // innerCompatabilityContent: compatibility_list
      getCompatibilityDetails:
        final_compatibility.com_data.length < 1 ? null : final_compatibility,
    });

    // if (product_shipping_rates) {
    //   this.handleGetShippingService(product_shipping_rates[0].company_id);
    // }

    this.generalSellingPrice(prod_price);
    this.updateOptionsArray();
    this.setState({loading: false});
  };

  // handleGetShippingService = (company_id) => {
  //   var details = {
  //     shipping_company: company_id,
  //   };

  //   this.props.getCarrierService(details);
  // };

  updateOptionsArray = async () => {
    const {optionSelectArray} = this.state;
    const {options} = this.props;
    let array = optionSelectArray;
    await array.map((optionSelect) => {
      options.map((option) => {
        if (optionSelect.option_id == option.option_id) {
          if (
            optionSelect.type == 'radio' ||
            optionSelect.type == 'checkbox' ||
            optionSelect.type == 'select'
          ) {
            let obj = {
              option_value: option.option_value,
            };
            Object.assign(optionSelect, obj);
          }
        }
      });
    });
    this.setState({
      optionSelectArray: [...array],
    });

    // await this.setState({loading: false});

    // if (this.state.product_title == '' || this.state.url_keywords == '') {
    //   this.setState({loading: true});
    // } else {
    //   this.setState({loading: false});
    // }
    console.log('first', optionSelectArray);
  };

  getEditProduct = () => {
    this.retrieveToken().then((_token) => {
      let details = {
        _token: _token,
        prod_id: this.state.prod_id,
      };

      this.props.editProductView(details);
      this.setState({loading: true});
    });
  };

  componentDidMount() {
    console.log('componentDidMount');
    if (this.state.prod_id) {
      console.log('if prod id');
      this.getEditProduct();
    } else if (this.state.prod_id && this.props.product_data == null) {
      console.log('if prod id && == null');
      this.getEditProduct();
    }

    this.retrieveToken().then((_token) => {
      let details = {
        _token: _token,
      };
      this.props.getProcessingTime(details);
    });

    this.props.getShippingCompany();
    this.props.getProductFee(this.props.user_id);
    this.props.getCategoryList();
    this.props.getBrands();
    this.props.getCountries();
    this.props.getShipsToCountries();
  }

  handleOnChangeShipping = () => {
    this.setState((state) => ({
      checked: !state.checked,
    }));

    this.handleRemoveShippping();
  };

  metaTagTitle = (text) => {
    this.setState({meta_tag_title: text});
  };

  metaTagDescription = (text) => {
    this.setState({meta_tag_description: text});
  };

  metaTagKeywords = (text) => {
    this.setState({meta_tag_keywords: text});
  };

  tags = (text) => {
    this.setState({tags: text});
  };

  alertMessage = (value) => {
    Alert.alert('Harap isi' + value);
  };

  alertMessageForSelect = (value) => {
    Alert.alert('Silahkan pilih ' + value);
  };

  createOptionsName = (text) => {
    this.setState({
      optionName: text,
    });
  };

  createOptionsDisplayOrder = (text) => {
    this.setState({
      option_display_order: text,
    });
  };

  generalProductTitle = (text) => {
    this.setState({
      product_title: text,
      url_keywords: getSlug(text),
      meta_tag_title: text,
    });
  };

  generalUrlKeywords = (text) => {
    this.setState({
      url_keywords: getSlug(text),
    });
  };

  generalSellingPrice = (text) => {
    var willSet = true;
    for (var i = 0; i < text.length; i++) {
      if (text[i].match('[A-Za-z]')) {
        willSet = false;
        Alert.alert('Silakan masukkan nomor saja!');
      }
    }

    if (willSet) {
      if (this.state.requires_type == 'd') {
        if (this.state.PickerSelectedUploadMethod == 0) {
          var ad_com = this.props.admincommdigital[1]?.commsetting_fees;
        } else if (this.state.PickerSelectedUploadMethod == 1) {
          var ad_com = this.props.admincommdigital[0]?.commsetting_fees;
        }
      } else {
        if (this.state.PickerSelectedCategoryId == '320') {
          var ad_com = this.props.admincommphysical[5]?.commsetting_fees;
        } else if (this.state.PickerSelectedCategoryId == '26') {
          var ad_com = this.props.admincommphysical[3]?.commsetting_fees;
        } else if (this.state.PickerSelectedCategoryId == '262') {
          var ad_com = this.props.admincommphysical[4]?.commsetting_fees;
        } else if (this.state.PickerSelectedCategoryId == '257') {
          var ad_com = this.props.admincommphysical[0]?.commsetting_fees;
        } else {
          var ad_com = 0;
        }
      }

      console.log('ini ad com', ad_com);
      let price = formatRupiah(text, false);

      let mid_fee = this.props.midtrans_fee.Value;
      let mid_amount = this.props.midtrans_fee.Amount;
      let commission = parseFloat((price * ad_com) / 100);
      let n_commission = parseInt(commission) + parseInt(price);
      let total_fee =
        parseInt((n_commission * mid_fee) / 100) +
        parseInt(mid_amount) +
        parseInt(commission);

      let tprice = countNumber(total_fee, price);
      this.setState({
        selling_price: text,
        selling_price_fee: total_fee,
        selling_price_total: tprice,
        prod_admin_commission: mid_amount,
        prod_admin_amount: mid_amount,
        prod_midtrans_calculation: total_fee,
        prod_midtrans_commssion: mid_fee,
      });
    }
  };

  generalQuantity = (text) => {
    var willSet = true;
    for (var i = 0; i < text.length; i++) {
      if (text[i].match('[A-Za-z]')) {
        willSet = false;
        Alert.alert('Silakan masukkan nomor saja!');
      }
    }
    if (willSet) {
      this.setState({
        quantity: text,
      });
    }
  };

  generalMinimumQuantity = (text) => {
    var willSet = true;
    for (var i = 0; i < text.length; i++) {
      if (text[i].match('[A-Za-z]')) {
        willSet = false;
        Alert.alert('Silakan masukkan nomor saja!');
      }
    }
    if (willSet) {
      this.setState({
        minimum_quantity: text,
      });
    }
  };

  generalModel = (text) => {
    this.setState({
      model: text,
    });
  };

  generalSku = (text) => {
    this.setState({
      sku: text,
    });
  };

  generalProductDescription = (text) => {
    this.setState({
      product_description: text,
    });
  };

  setStateDynamic = (value) => {
    this.setState({
      seg: value,
    });
  };

  setDownloadName = (index, text) => {
    this.setState((state) => {
      const list = state.generalInstantArray.map((item, i) => {
        if (index === i) {
          return (item.download_name = text);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setMaxDownloadTime = (index, text) => {
    var willSet = true;
    for (var i = 0; i < text.length; i++) {
      if (text[i].match('[A-Za-z]')) {
        willSet = false;
        Alert.alert('Silakan masukkan nomor saja!');
      }
    }
    if (willSet) {
      this.setState((state) => {
        const list = state.generalInstantArray.map((item, i) => {
          if (index === i) {
            return (item.max_download_times = text);
          } else {
            return item;
          }
        });
        return {
          list,
        };
      });
    }
  };

  setValidityDays = (index, text) => {
    var willSet = true;
    for (var i = 0; i < text.length; i++) {
      if (text[i].match('[A-Za-z]')) {
        willSet = false;
        Alert.alert('Silakan masukkan nomor saja!');
      }
    }
    if (willSet) {
      this.setState((state) => {
        const list = state.generalInstantArray.map((item, i) => {
          if (index === i) {
            return (item.validity = text);
          } else {
            return item;
          }
        });
        return {
          list,
        };
      });
    }
  };

  setPhysicalLength = (text) => {
    var willSet = true;
    for (var i = 0; i < text.length; i++) {
      if (text[i].match('[A-Za-z]')) {
        willSet = false;
        Alert.alert('Silakan masukkan nomor saja!');
      }
    }
    if (willSet) {
      this.setState({
        physicalLength: text,
      });
    }
  };

  setPhysicalHeight = (text) => {
    var willSet = true;
    for (var i = 0; i < text.length; i++) {
      if (text[i].match('[A-Za-z]')) {
        willSet = false;
        Alert.alert('Silakan masukkan nomor saja!');
      }
    }
    if (willSet) {
      this.setState({
        physicalHeight: text,
      });
    }
  };

  setPhysicalWidth = (text) => {
    var willSet = true;
    for (var i = 0; i < text.length; i++) {
      if (text[i].match('[A-Za-z]')) {
        willSet = false;
        Alert.alert('Silakan masukkan nomor saja!');
      }
    }
    if (willSet) {
      this.setState({
        physicalWidth: text,
      });
    }
  };

  setProductWeight = (text) => {
    var willSet = true;
    for (var i = 0; i < text.length; i++) {
      if (text[i].match('[A-Za-z]')) {
        willSet = false;
        Alert.alert('Silakan masukkan nomor saja!');
      }
    }
    if (willSet) {
      this.setState({
        product_weight: text,
      });
    }
  };

  setGDProcessingTime = async (value, processing_times) => {
    await this.setState({PickerSelectedProcessingTimeId: value});
    // processing_times.map((time) => {
    //     if(this.state.PickerSelectedProcessingTime == time.props.value){
    //         this.setState({
    //             PickerSelectedProcessingTimeId: time.key
    //         })
    //     }
    // })
  };

  setShippingCompany = async (val, index) => {
    this.setState((state) => {
      const list = state.generalShipArray.map((item, i) => {
        console.log('item', i);
        if (index) {
          item.company_id = val;
          var details = {
            shipping_company: val,
          };
          this.props.getCarrierService(details);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  selectCarrierService = async (val, index) => {
    console.log('selectCarrierService val', index);
    this.setState((state) => {
      const list = state.generalShipArray.map((item, i) => {
        console.log('selectCarrierService item', item);
        if (index) {
          item.pship_service_id = val;
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setProcessingTimePhysical = (val, index) => {
    this.setState((state) => {
      const list = state.generalShipArray.map((item, i) => {
        if (index) {
          item.processing_time_id = val;
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setShippingCost = (text) => {
    this.setState((state) => {
      const list = state.generalShipArray.map((item, i) => {
        item.cost = countNumber(text);
      });
      return {
        list,
      };
    });
  };

  setShippingAdditionalCost = (text) => {
    this.setState((state) => {
      const list = state.generalShipArray.map((item, i) => {
        item.additional_cost = countNumber(text);
      });
      return {
        list,
      };
    });
  };

  pickMultipleImage = async () => {
    await ImagePicker.openPicker({
      multiple: true,
      includeBase64: true,
      // compressImageQuality: 0.3
      height: 200,
      width: 200,
      mediaType: 'photo',
    }).then((images) => {
      // this.setState({
      //     imagesLength: this.state.imagesLength + images.length
      // })
      let array = [];
      for (let i = 0; i < images.length; i++) {
        array.push({
          extname: String(images[i].path).substring(
            String(images[i].path).lastIndexOf('.') + 1,
          ),
          image: images[i].data,
          product_id: this.state.prod_id,
        });
      }
      var details = {
        _token: this.state._token,
        prod_image: array,
        // 'prod_image':'data:image/png;base64,'+images[i].data
      };
      console.log('de', details);
      this.props.upload_images_general(details, this.state.imagesLength);
    });
  };

  removeImage = (image) => {
    var details = {
      _token: this.state._token,
      image_id: image.image_id,
    };

    this.props.delete_image_general(details);
  };

  componentWillUnmount = () => {
    this.props.removeGeneralImages();
    this.props.emptyProductData();
  };

  increaseGS = async () => {
    await this.setState({
      generalShipArray: [
        ...this.state.generalShipArray,
        {
          cost: '',
          additional_cost: '',
          country_name: 'Pilih Negara',
          country_id: 0,
          company_name: 'Select Company',
          company_id: 0,
          pship_service: 'Select',
          pship_service_id: 0,
          processing_time: 'Select Processing Time',
          processing_time_id: 0,
        },
      ],
    });
  };

  decreaseGS = async () => {
    var array = [...this.state.generalShipArray];
    array.splice(array.length - 1, 1);
    await this.setState({
      generalShipArray: array,
    });
  };

  increaseGDInstant = async () => {
    await this.setState({
      generalInstantArray: [
        ...this.state.generalInstantArray,
        {
          extension_name: '',
          fileurl: '',
          validity: '',
          max_download_times: '',
          uploadProgressPercentile: 0,
          download_name: '',
          filename: '',
        },
      ],
    });
  };

  decreaseGDInstant = async () => {
    var array = [...this.state.generalInstantArray];
    array.splice(array.length - 1, 1);
    await this.setState({
      generalInstantArray: array,
    });
  };

  increaseSpecArray = async () => {
    await this.setState({
      specificationsArray: [
        ...this.state.specificationsArray,
        {
          product_attribute_description: '',
          attribute_id: 0,
          attribute_name: 'Select',
        },
      ],
    });
  };

  decreaseSpecArray = async () => {
    var array = [...this.state.specificationsArray];
    array.splice(array.length - 1, 1);
    await this.setState({
      specificationsArray: array,
    });
  };

  increaseOptionsArray = async () => {
    await this.setState({
      optionArray: [...this.state.optionArray, {name: '', sort: ''}],
    });
  };

  decreaseOptionsArray = async () => {
    var array = [...this.state.optionArray];
    array.splice(array.length - 1, 1);
    await this.setState({
      optionArray: array,
    });
  };

  UNSAFE_componentWillReceiveProps = () => {
    console.log('UNSAFE_componentWillReceiveProps');
    if (this.props.optionCreated) {
      this.closeOptionsModal();
      var details = {
        _token: this.state._token,
        owner: this.state.user_id,
      };
      this.props.loadOptions(details);
    }
    if (this.state.prod_id && this.props.product_data != null) {
      console.log('TRUEEeeeeeee*******************');
      this.setEditData();
    }
  };

  setTabState = (value) => {
    this.setState({seg: value});
    var details = {
      _token: this.state._token,
    };
    if (value == 4) {
      if (this.props.specification_details.length < 1) {
        this.props.getSpecifications(details);
      }
    } else if (value == 3) {
      if (this.props.product_filters.length < 1) {
        this.props.getProdcutFilters(details);
      }
      if (this.props.related_products.length < 1) {
        this.props.getRelatedProducts(details);
      }
    } else if (value == 6) {
      details = {...details, ...{owner: this.state.user_id}};
      if (this.props.options.length < 1) {
        this.props.loadOptions(details);
      }
    } else if (value == 5) {
      // if (this.props.compatHeaders < 1) {
      this.props.loadCompatibilities(details);
      // }
    }
  };

  onSelectedItemsChange = (selectedItems) => {
    this.setState({selectedItems});
  };

  onSelectedOptionsChange = (selectedOptions) => {
    this.setState({selectedOptions});
  };

  onSelectedRelatedProductsChange = (selectedRelatedProducts) => {
    this.setState({selectedRelatedProducts});
  };

  setSpecificationsId = (value, specifications, index) => {
    this.setState((state) => {
      const list = state.specificationsArray.map((item, i) => {
        if (index === i) {
          // specifications.map((specification) => {
          // if(specification.props.value == value){
          // item.attribute_id = specification.key;
          item.attribute_id = value;
          // }
          // })
          return item;
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setSpecificationDes = (index, text) => {
    this.setState((state) => {
      const list = state.specificationsArray.map((item, i) => {
        if (index === i) {
          return (item.product_attribute_description = text);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setOptionValueName = (index, text) => {
    this.setState((state) => {
      const list = state.optionArray.map((item, i) => {
        if (index === i) {
          return (item.name = text);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setOptionValueSort = (index, text) => {
    this.setState((state) => {
      const list = state.optionArray.map((item, i) => {
        if (index === i) {
          return (item.sort = text);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setDateAvailable = (event, newDate) => {
    this.setState({
      dateAvailable: new Date(newDate).toISOString().slice(0, 10),
    });
  };

  selectOption = (type, id, array) => {
    console.log('ar', this.state.optionSelectArray);
    for (var i = 0; i < array.length; i++) {
      if (id == array[i].option_id) {
        this.setState({
          active_index: id,
          active_option_type: type,
        });
      }
    }
  };

  removeOptionSelected = async (id) => {
    var array = [...this.state.selectedOptions];
    for (var i = 0; i < array.length; i++) {
      if (array[i] == id) {
        array.splice(i, 1);
      }
    }
    await this.setState({
      selectedOptions: array,
    });
  };

  setOptionTextArea = (index, text) => {
    this.setState((state) => {
      const list = state.optionSelectArray.map((item, i) => {
        if (index === i) {
          return (item.value = text);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setOptionText = (index, text) => {
    this.setState((state) => {
      const list = state.optionSelectArray.map((item, i) => {
        if (index === i) {
          return (item.value = text);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setOptionTextRequired = (value, index) => {
    this.setState((state) => {
      const list = state.optionSelectArray.map((item, i) => {
        if (index === i) {
          return (item.required = value);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setOptionTextAreaRequired = (value, index) => {
    this.setState((state) => {
      const list = state.optionSelectArray.map((item, i) => {
        if (index === i) {
          return (item.required = value);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setOptionSelectRequired = (value, index) => {
    this.setState((state) => {
      const list = state.optionSelectArray.map((item, i) => {
        if (index === i) {
          return (item.required = value);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setInnerOptionValueId = (value, innerIndex, index) => {
    this.setState((state) => {
      const list = state.optionSelectArray[index].product_option_value.map(
        (item, i) => {
          if (innerIndex === i) {
            return (item.option_value_id = value);
          } else {
            return item;
          }
        },
      );
      return {
        list,
      };
    });
  };

  setInnerSubtract = (value, innerIndex, index) => {
    this.setState((state) => {
      const list = state.optionSelectArray[index].product_option_value.map(
        (item, i) => {
          if (innerIndex === i) {
            return (item.subtract = value);
          } else {
            return item;
          }
        },
      );
      return {
        list,
      };
    });
  };

  setInnerPriceSelect = (value, innerIndex, index) => {
    this.setState((state) => {
      const list = state.optionSelectArray[index].product_option_value.map(
        (item, i) => {
          if (innerIndex === i) {
            return (item.price_prefix = value);
          } else {
            return item;
          }
        },
      );
      return {
        list,
      };
    });
  };

  setInnerWeightSelect = (value, innerIndex, index) => {
    this.setState((state) => {
      const list = state.optionSelectArray[index].product_option_value.map(
        (item, i) => {
          if (innerIndex === i) {
            return (item.weight_prefix = value);
          } else {
            return item;
          }
        },
      );
      return {
        list,
      };
    });
  };

  setOptionInnerQuantity = (index, innerIndex, value) => {
    this.setState((state) => {
      const list = state.optionSelectArray[index].product_option_value.map(
        (item, i) => {
          if (innerIndex === i) {
            return (item.quantity = value);
          } else {
            return item;
          }
        },
      );
      return {
        list,
      };
    });
  };

  setOptionInnerPrice = (index, innerIndex, value) => {
    this.setState((state) => {
      const list = state.optionSelectArray[index].product_option_value.map(
        (item, i) => {
          if (innerIndex === i) {
            return (item.price = value);
          } else {
            return item;
          }
        },
      );
      return {
        list,
      };
    });
  };

  setOptionInnerWeight = (index, innerIndex, value) => {
    this.setState((state) => {
      const list = state.optionSelectArray[index].product_option_value.map(
        (item, i) => {
          if (innerIndex === i) {
            return (item.weight = value);
          } else {
            return item;
          }
        },
      );
      return {
        list,
      };
    });
  };

  increaseInnerOptionArray = async (index, option) => {
    var array = [...this.state.optionSelectArray];
    array[index].product_option_value.push({
      option_value_id: option.option_value[0].option_value_id,
      quantity: '',
      subtract: '1',
      price_prefix: 'P',
      price: '',
      weight_prefix: 'P',
      weight: '',
    });
    await this.setState({
      optionSelectArray: array,
    });
  };

  decreaseInnerOptionArray = async (index) => {
    var array = [...this.state.optionSelectArray];
    var length = array[index].product_option_value;
    array[index].product_option_value.splice(length - 1, 1);
    await this.setState({
      optionSelectArray: array,
    });
  };

  onContentUploadSuccess_ = async (name, value) => {
    this.setState({loading: true});

    console.log('onContentUploadSuccess_ name', JSON.stringify(name));
    console.log(
      'onContentUploadSuccess_ value',
      JSON.parse(value.target._response),
    );

    if (JSON.parse(value.target._response).status == 1) {
      var index = name.index;
      this.setState((state) => {
        const list = state.generalInstantArray.map((item, i) => {
          if (index === i) {
            item.extension_name = String(name.data._parts[1][1].name)
              .substring(String(name.data._parts[1][1].name).lastIndexOf('.'))
              .replace('.', '');
            item.fileurl = JSON.parse(value.target._response).url;
            item.filename = JSON.parse(value.target._response).filename;
            return item;
          } else {
            return item;
          }
        });
        return {
          list,
        };
      });
      await this.setState({loading: false});

      Alert.alert('Data berhasil diunggah');
    } else if (JSON.parse(value.target._response).status == 0) {
      Alert.alert('Informasi', JSON.parse(value.target._response).msg, [
        {text: 'OK'},
      ]);
      this.setState({loading: false});
    }
  };

  onContentUploadError_ = (error) => {
    Alert.alert('Informasi', error, [{text: 'OK'}]);
    this.setState({
      uploadProgressPercentile: 0,
    });
  };

  xmlRequest = (method, url, data, name, index) => {
    console.log('xmlRequest');
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.upload.onprogress = (progressEvent) => {
      var total = progressEvent.total;
      var loaded = progressEvent.loaded;
      var percent = Math.trunc((loaded / total) * 100);
      this.setState((state) => {
        const list = state.generalInstantArray.map((item, i) => {
          if (index === i) {
            item.uploadProgressPercentile = percent;
            console.log('xmlRequest', percent);
            return item;
          } else {
            return item;
          }
        });
        return {
          list,
        };
      });
    };
    xhr.setRequestHeader('Content-type', 'multipart/form-data');
    xhr.onload = this.onContentUploadSuccess_.bind(this, {name, data, index});
    xhr.onerror = this.onContentUploadError_.bind(this, {name, data});
    xhr.send(data);
  };

  uploadFile = async (value, index) => {
    var url = BASE_URL + '/instantDigitalFileUpload';
    if (value == 'video') {
      try {
        const file = await DocumentPicker.pick({
          type: [DocumentPicker.types.video],
        });
        const formData = new FormData();
        formData.append('_token', this.state._token);
        formData.append('file', {
          name: file.name,
          uri: file.uri,
          type: file.type,
        });
        this.xmlRequest('POST', url, formData, 'Video', index);
      } catch (error) {
        if (DocumentPicker.isCancel(error)) {
          Bugsnag.notify(error);
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw Bugsnag.notify(error);
        }
      }
    } else if (value == 'doc') {
      try {
        const file = await DocumentPicker.pick({
          type: [DocumentPicker.types.allFiles],
        });
        const formData = new FormData();
        formData.append('_token', this.state._token);
        formData.append('file', {
          name: file.name,
          uri: file.uri,
          type: file.type,
        });
        this.xmlRequest('POST', url, formData, 'File', index);
      } catch (error) {
        if (DocumentPicker.isCancel(error)) {
          Bugsnag.notify(error);
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw Bugsnag.notify(error);
        }
      }
    } else if (value == 'audio') {
      try {
        const file = await DocumentPicker.pick({
          type: [DocumentPicker.types.audio],
        });
        const formData = new FormData();
        formData.append('_token', this.state._token);
        formData.append('file', {
          name: file.name,
          uri: file.uri,
          type: file.type,
        });
        this.xmlRequest('POST', url, formData, 'Audio', index);
      } catch (error) {
        if (DocumentPicker.isCancel(error)) {
          Bugsnag.notify(error);
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw Bugsnag.notify(error);
        }
      }
    } else if (value == 'image') {
      try {
        const file = await DocumentPicker.pick({
          type: [DocumentPicker.types.images],
        });
        const formData = new FormData();
        formData.append('_token', this.state._token);
        formData.append('file', {
          name: file.name,
          uri: file.uri,
          type: file.type,
        });
        this.xmlRequest('POST', url, formData, 'Image', index);
      } catch (error) {
        if (DocumentPicker.isCancel(error)) {
          Bugsnag.notify(error);
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw Bugsnag.notify(error);
        }
      }
    } else if (value == 'zip') {
      try {
        const file = await DocumentPicker.pick({
          type: [DocumentPicker.types.zip],
        });
        const formData = new FormData();
        formData.append('_token', this.state._token);
        formData.append('file', {
          name: file.name,
          uri: file.uri,
          type: file.type,
        });
        this.xmlRequest('POST', url, formData, 'File', index);
      } catch (error) {
        if (DocumentPicker.isCancel(error)) {
          Bugsnag.notify(error);
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw Bugsnag.notify(error);
        }
      }
    } else if (value == 'pdf') {
      try {
        const file = await DocumentPicker.pick({
          type: [DocumentPicker.types.pdf],
        });
        const formData = new FormData();
        formData.append('_token', this.state._token);
        formData.append('file', {
          name: file.name,
          uri: file.uri,
          type: file.type,
        });
        this.xmlRequest('POST', url, formData, 'File', index);
      } catch (error) {
        if (DocumentPicker.isCancel(error)) {
          Bugsnag.notify(error);
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw Bugsnag.notify(error);
        }
      }
    }
  };

  updateComplicatedOptionsArray = () => {
    for (var j = 0; j < this.props.options.length; j++) {
      if (
        this.state.selectedOptions[this.state.selectedOptions.length - 1] ==
        this.props.options[j].option_id
      ) {
        if (
          this.props.options[j].type == 'text' ||
          this.props.options[j].type == 'file' ||
          this.props.options[j].type == 'date' ||
          this.props.options[j].type == 'datetime' ||
          this.props.options[j].type == 'time'
        ) {
          var obj = {
            ...{required: '1', value: ''},
          };
          Object.assign(obj, this.props.options[j]);
          this.setState({
            optionSelectArray: [...this.state.optionSelectArray, obj],
          });
        } else if (this.props.options[j].type == 'textarea') {
          var obj = {
            ...{required: '1', value: ''},
          };
          Object.assign(obj, this.props.options[j]);
          this.setState({
            optionSelectArray: [...this.state.optionSelectArray, obj],
          });
        } else if (
          this.props.options[j].type == 'radio' ||
          this.props.options[j].type == 'checkbox' ||
          this.props.options[j].type == 'select'
        ) {
          var obj = {
            ...{
              required: '1',
              product_option_value: [
                {
                  option_value_id: this.props.options[j].option_value[0]
                    .option_value_id,
                  quantity: '',
                  subtract: '1',
                  price_prefix: 'P',
                  price: '',
                  weight_prefix: 'P',
                  weight: '',
                },
              ],
            },
          };
          Object.assign(obj, this.props.options[j]);
          console.log('up11', obj);
          this.setState({
            optionSelectArray: [...this.state.optionSelectArray, obj],
          });
          console.log('up12', this.state.optionSelectArray);
        }
        break;
      }
    }

    console.log('second', this.state.optionSelectArray);
  };

  componentDidUpdate = async (prevProps, prevState) => {
    console.log('componentDidUpdate');
    if (prevState.selectedOptions.length < this.state.selectedOptions.length) {
      this.updateComplicatedOptionsArray();
    }
    if (prevState.selectedOptions.length > this.state.selectedOptions.length) {
      for (var i = 0; i < this.state.optionSelectArray.length; i++) {
        const found = this.state.selectedOptions.includes(
          this.state.optionSelectArray[i].option_id,
        );
        if (!found) {
          var array = [...this.state.optionSelectArray];
          array.splice(i, 1);
          await this.setState({
            optionSelectArray: array,
          });
        }
      }
    }
    if (prevProps.compatHeaders.length != this.props.compatHeaders.length) {
      var array = [];
      for (var i = 0; i < this.props.compatHeaders.length; i++) {
        var obj = {
          active: false,
          group_id: this.props.compatHeaders[i].group_id,
        };
        array.push(obj);
      }
      await this.setState({
        compatArray: array,
      });
    }
    if (prevProps.currentTab != this.props.currentTab) {
      this.setTabState(this.props.currentTab);
    }
  };

  handleRemoveShippping = () => {
    this.setState((state) => {
      const list = state.generalShipArray.map((item) => {
        item.company_id = 0;
        item.cost = '';
        item.additional_cost = '';
        item.company_id = 0;
        item.pship_service_id = 0;
        item.processing_time_id = 0;
      });
      return {
        list,
      };
    });

    this.setState({
      generalShipArray: [
        {
          cost: '',
          additional_cost: '',
          company_id: 0,
          pship_service_id: 0,
          processing_time_id: 0,
        },
      ],
    });
  };

  loadCompatCollapseContent = async (header) => {
    this.setState({
      groupd_id_compatibility: header.group_id,
    });
    this.setState({
      innerCompatLoading: true,
      innerCompatabilityContent: [{data: []}],
    });
    let array = [];
    for (var i = 0; i < (await this.state.compatArray.length); i++) {
      if (this.state.compatArray[i].group_id == header.group_id) {
        if (this.state.compatArray[i].active == true) {
          var obj = {
            active: false,
            group_id: this.state.compatArray[i].group_id,
          };
          array.push(obj);
        } else {
          var obj = {
            active: true,
            group_id: this.state.compatArray[i].group_id,
          };
          array.push(obj);
          var details = {
            _token: this.state._token,
            group_id: header.group_id,
          };
          var formBody = [];
          for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + '=' + encodedValue);
          }
          formBody = formBody.join('&');
          axios
            .post(BASE_URL + '/individuallyGroupAttribute', formBody)
            .then((response) => {
              const compatChangeable = [
                ...this.state.innerCompatabilityContent,
              ];
              Object.assign(compatChangeable[0].data, response.data.data);
              compatChangeable[0].data.map((item) => {
                Object.assign(item, {value: '0'});
              });
              this.setState({
                innerCompatFields: response.data.data,
                innerCompatabilityContent: compatChangeable,
              });
              var selectApi = {
                _token: this.state._token,
                group_id: header.group_id,
                attribute_id: this.state.innerCompatFields[0].attribute_id,
              };

              formBody = [];
              for (property in selectApi) {
                encodedKey = encodeURIComponent(property);
                encodedValue = encodeURIComponent(selectApi[property]);
                formBody.push(encodedKey + '=' + encodedValue);
              }
              formBody = formBody.join('&');
              axios
                .post(BASE_URL + '/attributeDetails', formBody)
                .then((values) => {
                  if (values.data.status == 1) {
                    let fieldData = {data: values.data.data};
                    let fieldsToAddArray = [
                      ...this.state.innerCompatabilityContent,
                    ];
                    Object.assign(fieldsToAddArray[0].data[0], fieldData);
                    this.setState({
                      innerCompatabilityContent: fieldsToAddArray,
                    });
                  }
                  this.setState({
                    innerCompatLoading: false,
                  });
                });
            });
        }
      } else {
        var obj = {
          active: false,
          group_id: this.state.compatArray[i].group_id,
        };
        array.push(obj);
      }
    }
    await this.setState({
      compatArray: array,
    });
  };

  loadCompatCollapseContentEdit = async (header) => {
    console.log('ini header', header);
    console.log('ini header', header.com_group_id);

    this.setState({groupd_id_compatibility: header.com_group_id});

    this.setState({
      innerCompatLoading: true,
      innerCompatabilityContent: [{data: []}],
    });

    var details = {
      _token: this.state._token,
      group_id: header.com_group_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post(BASE_URL + '/individuallyGroupAttribute', formBody)
      .then((response) => {
        console.log('ini respon', JSON.stringify(response));
        const compatChangeable = [...this.state.innerCompatabilityContent];
        Object.assign(compatChangeable[0].data, response.data.data);
        compatChangeable[0].data.map((item) => {
          Object.assign(item, {value: '0'});
        });
        this.setState({
          innerCompatFields: response.data.data,
          innerCompatabilityContent: compatChangeable,
        });
        var selectApi = {
          _token: this.state._token,
          group_id: header.com_group_id,
          attribute_id: this.state.innerCompatFields[0].attribute_id,
        };
        formBody = [];
        for (property in selectApi) {
          encodedKey = encodeURIComponent(property);
          encodedValue = encodeURIComponent(selectApi[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        axios.post(BASE_URL + '/attributeDetails', formBody).then((values) => {
          if (values.data.status == 1) {
            console.log('ini value', JSON.stringify(values));

            let fieldData = {data: values.data.data};
            let fieldsToAddArray = [...this.state.innerCompatabilityContent];
            Object.assign(fieldsToAddArray[0].data[0], fieldData);
            this.setState({
              innerCompatabilityContent: fieldsToAddArray,
            });
          }
          this.setState({
            innerCompatLoading: false,
          });
        });
      });
  };

  changeCompatValues = async (value, index, innerIndex) => {
    let valueChange = [...this.state.innerCompatabilityContent];
    valueChange[index].data[innerIndex].value = value;
    this.setState({
      innerCompatabilityContent: valueChange,
    });
    if (
      innerIndex <
      this.state.innerCompatabilityContent[index].data.length - 1
    ) {
      let details = {
        attribute_id: this.state.innerCompatabilityContent[index].data[
          innerIndex
        ].attribute_id,
      };
      let toIterate = [
        ...this.state.innerCompatabilityContent[index].data[innerIndex].data,
      ];
      toIterate.every((item) => {
        if (item.model_value == value) {
          details = {
            ...details,
            _token: this.state._token,
            group_id: item.group_id,
            model_value: item.model_value,
            nextID: item.nextID,
            updateField: item.updated_field
              ? item.updated_field
              : item.updateField,
            preMapID: item.preMapID,
            currentOnchangeID: item.currentOnchangeID,
            rowNumber: item.rowNumber,
          };
          var formBody = [];
          for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + '=' + encodedValue);
          }
          formBody = formBody.join('&');
          axios
            .post(BASE_URL + '/getModelValuesAPI', formBody)
            .then((values) => {
              // console.log('val', values);
              if (values.data.status == 1) {
                let fieldData = {data: values.data.data};
                let toBeAdded = [...this.state.innerCompatabilityContent];
                Object.assign(toBeAdded[index].data[++innerIndex], fieldData);
                this.setState({
                  innerCompatabilityContent: toBeAdded,
                });
              } else {
                let toRemove = [...this.state.innerCompatabilityContent];
                // let toAddNew = toRemove;
                for (
                  let i = innerIndex + 1;
                  i <=
                  this.state.innerCompatabilityContent[index].data.length - 1;
                  i++
                ) {
                  if (toRemove[index].data[i].data) {
                    toRemove[index].data[i].value = '0';
                    setTimeout(() => {
                      toRemove[index].data[i].data = null;
                    }, 1000);
                    // delete toRemove[index].data[i].data;
                    // Object.assign(toRemove[index].data[i], {"value": "0"})
                  }
                }
                this.setState({
                  innerCompatabilityContent: toRemove,
                });
              }
              this.setState({
                innerCompatLoading: false,
              });
            });
          return false;
        } else {
          return true;
        }
      });
    }
  };

  increaseCompatInside = async (header) => {
    var details = {
      _token: this.state._token,
      group_id: header.group_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/individuallyGroupAttribute', formBody)
      .then((response) => {
        const toIncreaseArray = [...this.state.innerCompatabilityContent];
        toIncreaseArray.push({data: response.data.data});
        Object.assign(
          toIncreaseArray[toIncreaseArray.length - 1].data,
          response.data.data,
        );
        toIncreaseArray[toIncreaseArray.length - 1].data.map((item) => {
          Object.assign(item, {value: '0'});
        });
        this.setState({
          innerCompatabilityContent: toIncreaseArray,
        });
        //Next api content
        var selectApi = {
          _token: this.state._token,
          group_id: header.group_id,
          attribute_id: this.state.innerCompatabilityContent[
            this.state.innerCompatabilityContent.length - 1
          ].data[0].attribute_id,
        };
        var formBodyinner = [];
        for (var property2 in selectApi) {
          var encodedKey2 = encodeURIComponent(property2);
          var encodedValue2 = encodeURIComponent(selectApi[property]);
          formBodyinner.push(encodedKey2 + '=' + encodedValue2);
        }
        formBodyinner = formBodyinner.join('&');
        axios
          .post(BASE_URL + '/attributeDetails', formBodyinner)
          .then((values) => {
            if (values.data.status == 1) {
              let fieldData = {data: values.data.data};
              let fieldsToAddArray = [...this.state.innerCompatabilityContent];
              Object.assign(
                fieldsToAddArray[fieldsToAddArray.length - 1].data[0],
                fieldData,
              );
              this.setState({
                innerCompatabilityContent: fieldsToAddArray,
              });
            }
          });
      });
  };

  decreaseCompatInside = async () => {
    let toDecreaseArray = [...this.state.innerCompatabilityContent];
    toDecreaseArray.splice(toDecreaseArray.length - 1, 1);
    await this.setState({
      innerCompatabilityContent: toDecreaseArray,
    });
  };

  increaseCompatInsideEdit = async (header) => {
    var details = {
      _token: this.state._token,
      group_id: header.com_group_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/individuallyGroupAttribute', formBody)
      .then((response) => {
        const toIncreaseArray = [...this.state.innerCompatabilityContent];
        toIncreaseArray.push({data: response.data.data});
        Object.assign(
          toIncreaseArray[toIncreaseArray.length - 1].data,
          response.data.data,
        );
        toIncreaseArray[toIncreaseArray.length - 1].data.map((item) => {
          Object.assign(item, {value: '0'});
        });
        this.setState({
          innerCompatabilityContent: toIncreaseArray,
        });
        //Next api content
        var selectApi = {
          _token: this.state._token,
          group_id: header.com_group_id,
          attribute_id: this.state.innerCompatabilityContent[
            this.state.innerCompatabilityContent.length - 1
          ].data[0].attribute_id,
        };
        var formBodyinner = [];
        for (property in selectApi) {
          encodedKey = encodeURIComponent(property);
          encodedValue = encodeURIComponent(selectApi[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBodyinner = formBodyinner.join('&');
        axios
          .post(BASE_URL + '/attributeDetails', formBodyinner)
          .then((values) => {
            if (values.data.status == 1) {
              let fieldData = {data: values.data.data};
              let fieldsToAddArray = [...this.state.innerCompatabilityContent];
              Object.assign(
                fieldsToAddArray[fieldsToAddArray.length - 1].data[0],
                fieldData,
              );
              this.setState({
                innerCompatabilityContent: fieldsToAddArray,
              });
            }
          });
      });
  };

  decreaseCompatInsideEdit = async () => {
    let toDecreaseArray = [...this.state.innerCompatabilityContent];
    toDecreaseArray.splice(toDecreaseArray.length - 1, 1);
    await this.setState({
      innerCompatabilityContent: toDecreaseArray,
    });
  };

  increaseDiscountManagementQD = async () => {
    await this.setState({
      DiscountManagementQD: [
        ...this.state.DiscountManagementQD,
        {
          quantity: '',
          priority: '',
          price: '',
          start_date: null,
          end_date: null,
        },
      ],
    });
  };

  decreaseDiscountManagementQD = async () => {
    var array = [...this.state.DiscountManagementQD];
    array.splice(array.length - 1, 1);
    await this.setState({
      DiscountManagementQD: array,
    });
  };

  increaseDiscountManagementSD = async () => {
    await this.setState({
      DiscountManagementSD: [
        ...this.state.DiscountManagementSD,
        {priority: '', price: '', start_date: null, end_date: null},
      ],
    });
  };

  decreaseDiscountManagementSD = async () => {
    var array = [...this.state.DiscountManagementSD];
    array.splice(array.length - 1, 1);
    await this.setState({
      DiscountManagementSD: array,
    });
  };

  setQtyDiscountQuantity = (index, text) => {
    this.setState((state) => {
      const list = state.DiscountManagementQD.map((item, i) => {
        if (index === i) {
          return (item.quantity = text);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setQtyDiscountPriority = (index, text) => {
    this.setState((state) => {
      const list = state.DiscountManagementQD.map((item, i) => {
        if (index === i) {
          return (item.priority = text);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setQtyDiscountPrice = (index, text) => {
    this.setState((state) => {
      const list = state.DiscountManagementQD.map((item, i) => {
        if (index === i) {
          return (item.price = countNumber(text));
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setQtyDiscountDateStart = async (index, newDate) => {
    this.setState((state) => {
      const list = state.DiscountManagementQD.map((item, i) => {
        if (index === i) {
          return (item.start_date = newDate.toLocaleDateString());
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setQtyDiscountDateEnd = (index, newDate) => {
    this.setState((state) => {
      const list = state.DiscountManagementQD.map((item, i) => {
        if (index === i) {
          return (item.end_date = newDate.toLocaleDateString());
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setSplDiscountPriority = (index, text) => {
    this.setState((state) => {
      const list = state.DiscountManagementSD.map((item, i) => {
        if (index === i) {
          return (item.priority = text);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setSplDiscountPrice = (index, text) => {
    this.setState((state) => {
      const list = state.DiscountManagementSD.map((item, i) => {
        if (index === i) {
          return (item.price = countNumber(text));
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setSplDiscountDateStart = async (index, newDate) => {
    this.setState((state) => {
      const list = state.DiscountManagementSD.map((item, i) => {
        if (index === i) {
          return (item.start_date = newDate.toLocaleDateString());
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  setSplDiscountDateEnd = (index, newDate) => {
    this.setState((state) => {
      const list = state.DiscountManagementSD.map((item, i) => {
        if (index === i) {
          return (item.end_date = newDate.toLocaleDateString());
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  scrollForward() {
    this.refs.segmentScroll.scrollTo({x: this.state.contentOff + 50});
  }

  scrollBack() {
    this.refs.segmentScroll.scrollTo({x: this.state.contentOff - 50});
  }

  handleScroll = async (event) => {
    this.setState({
      contentOff: event.nativeEvent.contentOffset.x,
    });
    if (
      event.nativeEvent.contentSize.width -
        (event.nativeEvent.contentOffset.x +
          event.nativeEvent.layoutMeasurement.width) <
      50
    ) {
      this.setState({
        segmentForwardArrow: false,
      });
    }
    if (event.nativeEvent.contentOffset.x < 50) {
      this.setState({
        segmentBackArrow: false,
      });
    }
    if (
      event.nativeEvent.contentOffset.x > 50 &&
      event.nativeEvent.contentSize.width -
        (event.nativeEvent.contentOffset.x +
          event.nativeEvent.layoutMeasurement.width) >
        50
    ) {
      this.setState({
        segmentBackArrow: true,
        segmentForwardArrow: true,
      });
    }
  };

  validateYoutubeUrl = () => {
    let {youtubeLink} = this.state;
    let patternYoutube = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/gm;
    if (youtubeLink !== '') {
      return (
        <View>
          {!youtubeLink.match(patternYoutube) ? (
            <View style={{flex: 1, padding: hp('10%')}}>
              <Text style={styles.validYoutube}>
                Format Link YouTube yang anda masukan tidak valid, contoh:
              </Text>
              <Text style={styles.validYoutube}>
                https://www.youtube.com/watch?v=contohLink
              </Text>
            </View>
          ) : (
            <View style={{height: hp('165%'), flex: 1}}>
              <WebView
                style={{margin: 20}}
                source={{
                  uri: youtubeLink,
                }}
                javaScriptEnabled={true}
                domStorageEnabled={true}
              />
            </View>
          )}
        </View>
      );
    }
  };

  deleteSavedCompatibility = async () => {
    this.setState({
      loading: true,
    });

    var details = {
      _token: this.state._token,
      product_id: this.state.prod_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/deleteCompatibilityProduct', formBody)
      .then((response) => {
        console.log('done', response);
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          this.setState({
            getCompatibilityDetails: null,
          });
          Alert.alert('Kompatibilitas terhapus');
          // this.props.navigation.pop();
        } else {
          Alert.alert(ERROR_MESSAGE);
        }
      });
  };

  setMainImage = async (id, session_id) => {
    var details = {
      _token: this.state._token,
      image_id: id,
      session_id: session_id,
    };
    if (this.state.prod_id) {
      details = {
        ...details,
        ...{
          product_id: this.state.prod_id,
        },
      };
    }
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/setDefaultProductImage', formBody)
      .then((response) => {
        console.log('set image', response);
        this.props.setMainImageDone(response.data);
      });
  };

  render() {
    // console.log(
    //   'ini generalInstantArray',
    //   JSON.stringify(this.state.generalInstantArray),
    // );
    // console.log('ini proooops', JSON.stringify(this.props.carrier_service));

    console.log(
      'ini state dateAvailable',
      JSON.stringify(this.state.dateAvailable),
    );
    let specification_details;
    if (this.props.specification_details) {
      specification_details = this.props.specification_details.map((value) => {
        return (
          <Picker.Item
            key={value.attribute_id}
            value={value.attribute_id}
            label={value.attribute_name}
          />
        );
      });
    }

    let processing_time;
    if (this.props.processing_time) {
      processing_time = this.props.processing_time.map((value) => {
        return (
          <Picker.Item key={value.id} value={value.id} label={value.name} />
        );
      });
    }

    if (
      this.props.saveProductloading ||
      this.state.loading ||
      this.props.loading2
    ) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner size={'large'} color={'red'} />
        </View>
      );
    }

    return (
      <Container style={styles.containerStyle}>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.optionsModal}
          onRequestClose={() => {
            this.closeOptionsModal();
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'rgba(0,0,0,0.5)',
            }}>
            <View
              style={{
                borderRadius: 10,
                width: wp('80%'),
                maxHeight: hp('80%'),
                backgroundColor: '#fff',
                padding: 20,
                zIndex: 5,
                position: 'relative',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ScrollView keyboardShouldPersistTaps={'handled'}>
                <Text
                  style={{
                    color: '#545454',
                    fontSize: hp('3%'),
                    paddingBottom: 0,
                    fontFamily: Font.RobotoRegular,
                    textAlign: 'center',
                  }}>
                  {' '}
                  Opsi / Varian{' '}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.dropheadingClass}>Tipe</Text>
                  <Text
                    style={{
                      color: '#f00',
                      fontSize: hp('2.2%'),
                      marginTop: 5,
                    }}>
                    {' '}
                    *
                  </Text>
                </View>
                <View style={styles.dropdownClass}>
                  <Picker
                    style={styles.pickerClass}
                    onValueChange={(itemValue) =>
                      this.setState({optionType: itemValue})
                    }
                    selectedValue={this.state.optionType}>
                    <Picker.Item label="Tolong pilih" value="0" />
                    <Picker.Item
                      label="Pilih / Listbox / Dropdown"
                      value="select"
                    />
                    <Picker.Item label="Radio" value="radio" />
                    <Picker.Item label="Kotak centang" value="checkbox" />
                    <Picker.Item label="Teks" value="text" />
                    <Picker.Item label="Textarea" value="textarea" />
                    <Picker.Item label="Mengajukan" value="file" />
                    <Picker.Item label="Tanggal" value="date" />
                    <Picker.Item label="Waktu" value="time" />
                    <Picker.Item label="Tanggal Waktu" value="datetime" />
                  </Picker>
                </View>
                <CardSection>
                  <InputInner
                    value={this.state.optionName}
                    onChangeText={this.createOptionsName.bind(this)}
                    label="Nama"
                  />
                </CardSection>
                <CardSection>
                  <InputInner
                    value={this.state.option_display_order}
                    onChangeText={this.createOptionsDisplayOrder.bind(this)}
                    message={'not-mandatory'}
                    label="Urutan Tampilan"
                  />
                </CardSection>
                {(this.state.optionType == 'select' ||
                  this.state.optionType == 'radio' ||
                  this.state.optionType == 'checkbox') && (
                  <View>
                    {this.state.optionArray.map((data, index) => (
                      <View key={index} style={styles.shippingWrapper}>
                        <CardSection>
                          <InputInner
                            value={data.name}
                            onChangeText={this.setOptionValueName.bind(
                              this,
                              index,
                            )}
                            message={'not-mandatory'}
                            label="Nama Nilai Opsi"
                          />
                        </CardSection>
                        <CardSection>
                          <InputInner
                            value={data.sort}
                            onChangeText={this.setOptionValueSort.bind(
                              this,
                              index,
                            )}
                            message={'not-mandatory'}
                            label="Urutan Tampilan"
                          />
                        </CardSection>
                      </View>
                    ))}
                    <TouchableOpacity
                      disabled={this.state.optionArray.length <= 1}
                      onPress={this.decreaseOptionsArray}
                      style={styles.addBtn}>
                      <Text style={styles.addTxt}>-</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={this.increaseOptionsArray}
                      style={styles.addBtn}>
                      <Text style={styles.addTxt}>+</Text>
                    </TouchableOpacity>
                  </View>
                )}
                {
                  <View style={styles.btnWrapper}>
                    {this.props.createOptionsLoader ? (
                      <View
                        style={{
                          paddingTop: hp('1.83%'),
                          paddingBottom: hp('1.5%'),
                        }}>
                        <Spinner size={'large'} color={'red'} />
                      </View>
                    ) : (
                      <ButtonTwo onPress={() => this.createNewOption()}>
                        Simpan perubahan
                      </ButtonTwo>
                    )}
                  </View>
                }
              </ScrollView>
              <TouchableOpacity
                onPress={() => this.closeOptionsModal()}
                style={styles.closeBtn2}>
                <Text style={styles.crossColor}>&#10005;</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <Segment style={styles.segmentStyle}>
          <ScrollView
            onScroll={this.handleScroll}
            ref="segmentScroll"
            showsHorizontalScrollIndicator={false}
            horizontal={true}>
            <Button
              first
              active={this.state.seg === 1 ? true : false}
              onPress={() => this.setState({seg: 1})}
              style={
                this.state.seg === 1 ? styles.segmentActive : styles.segmentBtn
              }>
              <Text
                style={
                  this.state.seg === 1
                    ? styles.segmentBtnTxtActive
                    : styles.segmentBtnTxt
                }>
                Umum
              </Text>
            </Button>
            <Button
              active={this.state.seg === 2 ? true : false}
              onPress={() => this.setState({seg: 2})}
              style={
                this.state.seg === 2 ? styles.segmentActive : styles.segmentBtn
              }>
              <Text
                style={
                  this.state.seg === 2
                    ? styles.segmentBtnTxtActive
                    : styles.segmentBtnTxt
                }>
                SEO
              </Text>
            </Button>
            <Button
              active={this.state.seg === 3 ? true : false}
              onPress={() => this.setTabState(3)}
              style={
                this.state.seg === 3 ? styles.segmentActive : styles.segmentBtn
              }>
              <Text
                style={
                  this.state.seg === 3
                    ? styles.segmentBtnTxtActive
                    : styles.segmentBtnTxt
                }>
                Data
              </Text>
            </Button>
            {this.state.requires_type == 'p' && (
              <Button
                active={this.state.seg === 4 ? true : false}
                onPress={() => this.setTabState(4)}
                style={
                  this.state.seg === 4
                    ? styles.segmentActive
                    : styles.segmentBtn
                }>
                <Text
                  style={
                    this.state.seg === 4
                      ? styles.segmentBtnTxtActive
                      : styles.segmentBtnTxt
                  }>
                  Spesifikasi
                </Text>
              </Button>
            )}
            {this.state.requires_type == 'p' && (
              <Button
                active={this.state.seg === 5 ? true : false}
                onPress={() => this.setTabState(5)}
                style={
                  this.state.seg === 5
                    ? styles.segmentActive
                    : styles.segmentBtn
                }>
                <Text
                  style={
                    this.state.seg === 5
                      ? styles.segmentBtnTxtActive
                      : styles.segmentBtnTxt
                  }>
                  Kompatibilitas
                </Text>
              </Button>
            )}
            {this.state.requires_type == 'p' && (
              <Button
                active={this.state.seg === 6 ? true : false}
                onPress={() => this.setTabState(6)}
                style={
                  this.state.seg === 6
                    ? styles.segmentActive
                    : styles.segmentBtn
                }>
                <Text
                  style={
                    this.state.seg === 6
                      ? styles.segmentBtnTxtActive
                      : styles.segmentBtnTxt
                  }>
                  Pilihan
                </Text>
              </Button>
            )}
            {this.state.requires_type == 'p' && (
              <Button
                active={this.state.seg === 7 ? true : false}
                onPress={() => this.setState({seg: 7})}
                style={
                  this.state.seg === 7
                    ? styles.segmentActive
                    : styles.segmentBtn
                }>
                <Text
                  style={
                    this.state.seg === 7
                      ? styles.segmentBtnTxtActive
                      : styles.segmentBtnTxt
                  }>
                  Pengembalian
                </Text>
              </Button>
            )}
            <Button
              active={this.state.seg === 8 ? true : false}
              onPress={() => this.setState({seg: 8})}
              style={
                this.state.seg === 8 ? styles.segmentActive : styles.segmentBtn
              }>
              <Text
                style={
                  this.state.seg === 8
                    ? styles.segmentBtnTxtActive
                    : styles.segmentBtnTxt
                }>
                Manajemen Diskon
              </Text>
            </Button>
          </ScrollView>
        </Segment>
        <ScrollView keyboardShouldPersistTaps={'handled'} ref="generalScroll">
          {this.state.seg === 1 && (
            <View>
              {this.props.loading ? (
                <View
                  style={{
                    marginTop: hp('39%'),
                  }}>
                  <Spinner size={'large'} color={'red'} />
                </View>
              ) : (
                <View style={{padding: 10}}>
                  <View>
                    <ProductType
                      onPress={({val, ref}) => {
                        this.setState({
                          requires_type: val,
                          quantity: 1,
                          minimum_quantity: 1,
                          PickerSelectedBrandId: 91,
                          model: 'Tidak Tersedia',
                          sku: '1',
                          PickerSelectProductCondition: 'N',
                          substractStock: 0,
                        });
                        ref.close();
                      }}
                      value={this.state.requires_type}
                    />
                    <TextInputCostum
                      value={this.state.product_title}
                      onChangeText={this.generalProductTitle.bind(this)}
                      label="Nama Produk"
                    />

                    <CardSection>
                      <View style={{width: '100%'}}>
                        <TextInputCostum
                          value={getSlug(this.state.url_keywords)}
                          onChangeText={this.generalUrlKeywords.bind(this)}
                          label="Kata Kunci URL"
                        />
                        <Text style={styles.fileType}>
                          Kata kunci URL yang dimasukkan di atas menjadi bagian
                          dari URL, Jangan gunakan spasi, ganti spasi dengan
                          tanda hubung (-).
                        </Text>
                      </View>
                    </CardSection>

                    {this.state.requires_type == 'd' && (
                      <ProductUploadMethod
                        onPress={({val, ref}) => {
                          this.setState({
                            PickerSelectedUploadMethod: val,
                          });
                          ref.close();
                        }}
                        value={this.state.PickerSelectedUploadMethod}
                      />
                    )}

                    {this.state.requires_type == 'p' && (
                      <View style={styles.inputWrapper2}>
                        <ProductBrand
                          data={this.props.brands}
                          value={this.state.PickerSelectedBrandId}
                          onPress={({val, ref}) => {
                            this.setState({
                              PickerSelectedBrandId: val,
                            });
                            ref.close();
                          }}
                        />
                      </View>
                    )}

                    {this.state.requires_type == 'p' && (
                      <View style={styles.inputWrapper2}>
                        <ProductCategory
                          data={this.props.categories}
                          value={this.state.PickerSelectedCategoryId}
                          onPress={({ref}) => {
                            ref.close();
                          }}
                          setCategoryId={({val}) => {
                            this.setState({
                              PickerSelectedCategoryId: val,
                            });
                          }}
                        />
                      </View>
                    )}

                    {this.state.requires_type == 'd' &&
                      this.state.PickerSelectedUploadMethod == '0' && (
                        <ProductCategory
                          data={this.props.categories}
                          value={this.state.PickerSelectedCategoryId}
                          onPress={({ref}) => {
                            ref.close();
                          }}
                          setCategoryId={({val}) => {
                            this.setState({
                              PickerSelectedCategoryId: val,
                            });
                          }}
                        />
                      )}

                    {this.state.PickerSelectedCategoryId == 0 &&
                    this.state.requires_type == 'p' ? (
                      <View style={styles.inputWrapper2}>
                        <FakeProductPrice />
                      </View>
                    ) : (
                      <View style={styles.inputWrapper2}>
                        <ProductPrice
                          onChangeText={this.generalSellingPrice.bind(this)}
                          value={this.state.selling_price}
                          fee={this.state.selling_price_fee}
                          feeTotal={this.state.selling_price_total}
                        />
                      </View>
                    )}

                    {this.state.requires_type == 'p' && (
                      <TextInputCostum
                        keyboardType={'numeric'}
                        value={this.state.quantity}
                        onChangeText={this.generalQuantity.bind(this)}
                        label="Jumlah Stock"
                      />
                    )}

                    {this.state.requires_type == 'p' && (
                      <View style={{width: '100%'}}>
                        <TextInputCostum
                          keyboardType={'numeric'}
                          value={this.state.minimum_quantity}
                          onChangeText={this.generalMinimumQuantity.bind(this)}
                          label="Min Pembelian"
                        />
                        <Text style={styles.fileType}>Minimal pemesanan.</Text>
                      </View>
                    )}

                    {this.state.requires_type == 'p' && (
                      <TextInputCostum
                        value={this.state.model}
                        onChangeText={this.generalModel.bind(this)}
                        placeholder=""
                        label="Model"
                      />
                    )}

                    {this.state.requires_type == 'p' && (
                      <TextInputCostum
                        value={this.state.sku}
                        onChangeText={this.generalSku.bind(this)}
                        placeholder=""
                        label="SKU"
                      />
                    )}

                    {this.state.requires_type == 'p' && (
                      <ProductStatus
                        value={this.state.PickerSelectProductCondition}
                        onPress={({val, ref}) => {
                          this.setState({
                            PickerSelectProductCondition: val,
                          });
                          ref.close();
                        }}
                      />
                    )}

                    <ProductImage
                      onPress={() => this.pickMultipleImage()}
                      data={this.props.product_image}
                      setPrimary={(item) =>
                        this.setMainImage(item.image_id, item.image_session)
                      }
                      onRemove={(item) => this.removeImage(item)}
                      loading={this.props.add_product_loading}
                    />

                    <TextInputCostum
                      value={this.state.product_description}
                      onChangeText={this.generalProductDescription.bind(this)}
                      label="Deskripsi Produk"
                      multiline
                      numberOfLines={5}
                    />
                  </View>

                  <View>
                    {this.state.requires_type == 'p' && (
                      <View>
                        <Text style={styles.billingAddress}>DIMENSI PAKET</Text>
                        <View>
                          <Text style={styles.labelStyle}>
                            Ukuran (L x W x H)
                            <Text
                              style={{
                                color: '#f00',
                                fontSize: hp('2.2%'),
                                marginTop: 5,
                              }}>
                              {' '}
                              *
                            </Text>
                          </Text>
                          <View style={styles.inputWrapper}>
                            <View style={{width: '32%'}}>
                              <TextInputCostum
                                keyboardType="numeric"
                                value={this.state.physicalLength}
                                onChangeText={this.setPhysicalLength.bind(this)}
                                placeholder="Panjangnya"
                              />
                            </View>
                            <View style={{width: '32%'}}>
                              <TextInputCostum
                                keyboardType="numeric"
                                value={this.state.physicalWidth}
                                onChangeText={this.setPhysicalWidth.bind(this)}
                                placeholder="Lebar"
                              />
                            </View>
                            <View style={{width: '32%'}}>
                              <TextInputCostum
                                keyboardType="numeric"
                                value={this.state.physicalHeight}
                                onChangeText={this.setPhysicalHeight.bind(this)}
                                placeholder=" Tinggi"
                              />
                            </View>
                          </View>

                          <ProductLength
                            onPress={({val, ref}) => {
                              this.setState({
                                PickerSelectLengthClass: val,
                              });
                              ref.close();
                            }}
                            value={this.state.PickerSelectLengthClass}
                          />

                          <TextInputCostum
                            keyboardType={'numeric'}
                            value={this.state.product_weight}
                            onChangeText={this.setProductWeight.bind(this)}
                            label="Bobot"
                          />
                          <ProductWeight
                            onPress={({val, ref}) => {
                              this.setState({
                                PickerSelectWeightClass: val,
                              });
                              ref.close();
                            }}
                            value={this.state.PickerSelectWeightClass}
                          />
                          <ProductCountry
                            onPress={({val, ref}) => {
                              this.setState({
                                PickerSelectedShippingCountryId: val,
                              });

                              this.setState((state) => {
                                const list = state.generalShipArray.map(
                                  (item, i) => {
                                    item.country_id = val;
                                  },
                                );
                                return {
                                  list,
                                };
                              });

                              ref.close();
                            }}
                            value={this.state.PickerSelectedShippingCountryId}
                            data={this.props.countries}
                          />
                        </View>
                        <View>
                          <Text style={styles.labelStyle}>Gratis Ongkir</Text>
                          <View style={styles.abc}>
                            <Checkbox
                              status={
                                this.state.checked ? 'checked' : 'unchecked'
                              }
                              onPress={this.handleOnChangeShipping.bind(this)}
                              checkedColor="#15b9ff"
                              uncheckedColor="rgba(0,0,0,0.5)"
                            />
                            <View style={styles.labelStyle2Wrapper}>
                              <Text style={styles.labelTxt}>
                                Jika Anda mencentang pilihan gratis ongkir, Anda
                                tetap harus menambahkan setidaknya satu pilihan
                                pengiriman supaya pembeli mengetahui opsi
                                pengiriman yang tertera adalah gratis ongkos
                                pengiriman.
                              </Text>
                            </View>
                          </View>
                          {this.state.checked && (
                            <>
                              {this.props.loadingShipping ? (
                                <View
                                  style={{
                                    marginTop: hp('39%'),
                                  }}>
                                  <Spinner size={'large'} color={'red'} />
                                </View>
                              ) : (
                                <>
                                  {this.state.generalShipArray.map((items) => (
                                    <View>
                                      <ProductCompanyShipping
                                        onPress={({val, index, ref}) => {
                                          this.setShippingCompany(val, index);
                                          ref.close();
                                        }}
                                        value={items.company_id}
                                        data={this.props.companies}
                                      />

                                      <ProductServiceShipping
                                        onPress={({val, index, ref}) => {
                                          this.selectCarrierService(val, index);
                                          ref.close();
                                        }}
                                        loading={this.props.loadingShipping}
                                        value={items.pship_service_id}
                                        data={this.props.carrier_service}
                                      />

                                      {/* {items && items.company_id > 0 ? (
                                        <ProductServiceShipping
                                          onPress={({val, index, ref}) => {
                                            this.selectCarrierService(
                                              val,
                                              index,
                                            );
                                            ref.close();
                                          }}
                                          loading={this.props.loadingShipping}
                                          value={items.pship_service_id}
                                          data={this.props.carrier_service}
                                        />
                                      ) : (
                                        <FakeServiceShipping
                                          onPress={() =>
                                            Alert.alert(
                                              'Informasi',
                                              'Harap memilih kurir terlebih dahulu sebelum memilih layanan pengiriman ',
                                              [{text: 'OK'}],
                                            )
                                          }
                                          value={items.pship_service_id}
                                          data={this.props.carrier_service}
                                        />
                                      )} */}

                                      <ProductProcessingTime
                                        onPress={({val, index, ref}) => {
                                          this.setProcessingTimePhysical(
                                            val,
                                            index,
                                          );
                                          ref.close();
                                        }}
                                        value={items.processing_time_id}
                                        data={this.props.processing_time}
                                      />

                                      <TextInputCostum
                                        keyboardType={'numeric'}
                                        value={formatRupiah(items.cost)}
                                        onChangeText={this.setShippingCost.bind(
                                          this,
                                        )}
                                        label="Biaya [Rp]"
                                      />

                                      <TextInputCostum
                                        keyboardType={'numeric'}
                                        value={formatRupiah(
                                          items.additional_cost,
                                        )}
                                        onChangeText={this.setShippingAdditionalCost.bind(
                                          this,
                                        )}
                                        label="Setiap Item Tambahan [Rp]"
                                      />
                                    </View>
                                  ))}
                                </>
                              )}
                            </>
                          )}
                        </View>

                        <View style={styles.btnWrapper}>
                          <Btn onPress={() => this.saveAndExit('general')}>
                            <Text style={{color: '#fff'}}>Menyimpan</Text>
                          </Btn>
                        </View>
                      </View>
                    )}
                    {this.state.requires_type == 'd' && (
                      <View>
                        {this.state.PickerSelectedUploadMethod == '0' ? (
                          <View
                            onStartShouldSetResponder={() =>
                              console.log('You click by View')
                            }>
                            {this.state.generalInstantArray.map((data, i) => (
                              <View
                                key={i}
                                style={{
                                  padding: 10,
                                  paddingBottom: 60,
                                  marginBottom: 15,
                                  borderColor: '#e7e7e7',
                                  borderWidth: 1,
                                  marginTop: hp('2%'),
                                }}>
                                <TextInputCostum
                                  value={data.download_name}
                                  onChangeText={this.setDownloadName.bind(
                                    this,
                                    i,
                                  )}
                                  label="Nama Unduhan"
                                />
                                <TextInputCostum
                                  keyboardType={'numeric'}
                                  value={data.max_download_times}
                                  onChangeText={this.setMaxDownloadTime.bind(
                                    this,
                                    i,
                                  )}
                                  label="Maksimal Unduhan"
                                />

                                <TextInputCostum
                                  keyboardType={'numeric'}
                                  value={data.validity}
                                  onChangeText={this.setValidityDays.bind(
                                    this,
                                    i,
                                  )}
                                  label="Batas Waktu"
                                  placeholder="2 Hari"
                                />

                                <ProductFileUpload
                                  index={i}
                                  onPress={({type, index, ref}) => {
                                    ref.close();
                                    this.uploadFile(type, index);
                                  }}
                                  fileName={data.filename}
                                  loading={
                                    data.uploadProgressPercentile > 0 &&
                                    data.uploadProgressPercentile < 100
                                  }
                                />
                              </View>
                            ))}
                          </View>
                        ) : (
                          <ProductProcessingTime
                            onPress={({val, ref}) => {
                              this.setGDProcessingTime(val, processing_time);
                              ref.close();
                            }}
                            value={this.state.PickerSelectedProcessingTimeId}
                            data={this.props.processing_time}
                          />
                        )}
                        {this.state.PickerSelectedUploadMethod == '0' && (
                          <View>
                            <TouchableOpacity
                              disabled={
                                this.state.generalInstantArray.length <= 1
                              }
                              onPress={this.decreaseGDInstant}
                              style={styles.addBtn}>
                              <Text style={styles.addTxt}>-</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                              onPress={this.increaseGDInstant}
                              style={styles.addBtn}>
                              <Text style={styles.addTxt}>+</Text>
                            </TouchableOpacity>
                          </View>
                        )}
                        <View style={styles.btnWrapper}>
                          <Btn onPress={() => this.saveAndExit('general')}>
                            <Text style={{color: '#fff'}}>Menyimpan</Text>
                          </Btn>
                        </View>
                      </View>
                    )}
                  </View>
                </View>
              )}
            </View>
          )}
          {this.state.seg === 2 && (
            <View style={styles.productReceive}>
              <TextInputCostum
                onChangeText={this.metaTagTitle.bind(this)}
                value={this.state.meta_tag_title}
                label="Judul Tag Meta"
              />

              <TextInputCostum
                value={this.state.meta_tag_description}
                onChangeText={this.metaTagDescription.bind(this)}
                message={'not-mandatory'}
                label="Deskripsi Meta Tag"
                multiline
                numberOfLines={4}
              />

              <TextInputCostum
                value={this.state.meta_tag_keywords}
                onChangeText={this.metaTagKeywords.bind(this)}
                message={'not-mandatory'}
                label="Kata Kunci Meta Tag"
              />

              <View style={{width: '100%'}}>
                <TextInputCostum
                  value={this.state.tags}
                  onChangeText={this.tags.bind(this)}
                  label="Tag"
                  multiline
                />
                <Text style={styles.fileType}>Tag (dipisahkan koma)</Text>
              </View>
              <View style={styles.btnWrapper}>
                <Btn onPress={() => this.saveAndExit('seo')}>
                  <Text style={{color: '#fff'}}>Menyimpan</Text>
                </Btn>
              </View>
            </View>
          )}
          {this.state.seg === 3 && (
            <View style={styles.productReceive}>
              {this.props.loading ? (
                <View
                  style={{
                    marginTop: hp('35%'),
                  }}>
                  <Spinner size={'large'} color={'red'} />
                </View>
              ) : (
                <View style={{marginHorizontal: wp(2)}}>
                  {this.state.requires_type == 'p' && (
                    <ProductDropdown
                      items={[
                        {name: 'Ya', value: 1},
                        {name: 'Tidak', value: 0},
                      ]}
                      onPress={({val, ref}) => {
                        this.setState({substractStock: val});
                        ref.close();
                      }}
                      value={this.state.substractStock}
                      title={'Kurangi Stok'}
                    />
                  )}
                  {this.state.requires_type == 'p' && (
                    <ProductDropdown
                      items={[
                        {name: 'Lacak', value: 1},
                        {name: 'Jangan lacak', value: 0},
                      ]}
                      onPress={({val, ref}) => {
                        this.setState({trackInventory: val});
                        ref.close();
                      }}
                      value={this.state.trackInventory}
                      title={'Lacak Inventaris'}
                    />
                  )}

                  {this.state.requires_type == 'p' && (
                    <View style={{width: '100%'}}>
                      <TextInputCostum
                        editable={this.state.trackInventory == 1 ? true : false}
                        value={this.state.alertStockLevel}
                        onChangeText={(text) =>
                          this.setState({alertStockLevel: text})
                        }
                        label="Tingkat Stok Peringatan"
                        message={'not-mandatory'}
                      />
                      <Text style={styles.fileType}>
                        Catatan: Anda akan menerima pemberitahuan email saat
                        jumlah stok produk di bawah atau sama dengan tingkat
                        ambang batas dan pelacakan inventaris diaktifkan.
                      </Text>
                    </View>
                  )}
                  <View style={{width: '100%'}}>
                    {this.validateYoutubeUrl()}
                    <TextInputCostum
                      value={this.state.youtubeLink}
                      onChangeText={(text) =>
                        this.setState({youtubeLink: text})
                      }
                      label="Youtube Video"
                      message={'not-mandatory'}
                    />
                    <Text style={styles.validText}>
                      Format Link YouTube yang valid:
                    </Text>
                    <Text style={styles.validText}>
                      https://www.youtube.com/watch?v=contohLink
                    </Text>
                  </View>
                  <InputDate
                    onChange={this.setDateAvailable}
                    value={this.state.dateAvailable}
                    title={'Tanggal Tersedia'}
                  />

                  {this.state.requires_type == 'p' && (
                    <ProductDropdown
                      items={[
                        {name: 'Ya', value: 1},
                        {name: 'Tidak', value: 0},
                      ]}
                      onPress={({val, ref}) => {
                        this.setState({productInStock: val});
                        ref.close();
                      }}
                      value={this.state.productInStock}
                      title={'Stok Produk'}
                    />
                  )}
                  <TextInputCostum
                    value={this.state.displayOrder}
                    onChangeText={(text) => this.setState({displayOrder: text})}
                    label="Urutan Tampilan"
                    message={'not-mandatory'}
                  />
                  <SectionedMultiSelect
                    selectText="Pilih Filter Produk"
                    items={this.props.product_filters}
                    selectedItems={this.state.selectedItems}
                    onSelectedItemsChange={this.onSelectedItemsChange}
                    uniqueKey={'filter_id'}
                    displayKey={'name'}
                    searchPlaceholderText="Cari Item..."
                  />
                  <SectionedMultiSelect
                    selectText="Pilih Produk Terkait"
                    items={this.props.related_products}
                    selectedItems={this.state.selectedRelatedProducts}
                    onSelectedItemsChange={this.onSelectedRelatedProductsChange}
                    uniqueKey={'id'}
                    displayKey={'name'}
                    searchPlaceholderText="Cari Item..."
                  />
                  <View style={styles.btnWrapper}>
                    <Btn onPress={() => this.saveAndExit('data')}>
                      <Text style={{color: '#fff'}}>Menyimpan</Text>
                    </Btn>
                  </View>
                </View>
              )}
            </View>
          )}
          {this.state.seg === 4 && (
            <View style={styles.productReceive}>
              {this.props.loading ? (
                <View
                  style={{
                    marginTop: hp('35%'),
                  }}>
                  <Spinner size={'large'} color={'red'} />
                </View>
              ) : (
                <View>
                  {this.state.specificationsArray.map((data, index) => (
                    <View key={index} style={styles.shippingWrapper}>
                      <View>
                        <View>
                          <ProductDinamicDropdown
                            indexParent={index}
                            onPress={({val, ref, indexParent}) => {
                              this.setSpecificationsId(
                                val,
                                specification_details,
                                indexParent,
                              );
                              ref.close();
                            }}
                            keyId={'attribute_id'}
                            keyName={'attribute_name'}
                            value={data.attribute_id}
                            items={this.props.specification_details}
                            title={'Spesifikasi'}
                          />
                        </View>
                        <TextInputCostum
                          value={data.product_attribute_description}
                          onChangeText={this.setSpecificationDes.bind(
                            this,
                            index,
                          )}
                          label="Keterangan"
                          multiline
                          numberOfLines={4}
                        />
                      </View>
                    </View>
                  ))}
                  <TouchableOpacity
                    disabled={this.state.specificationsArray.length <= 1}
                    onPress={this.decreaseSpecArray}
                    style={styles.addBtn}>
                    <Text style={styles.addTxt}>-</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={this.increaseSpecArray}
                    style={styles.addBtn}>
                    <Text style={styles.addTxt}>+</Text>
                  </TouchableOpacity>
                  <View style={styles.btnWrapper}>
                    <Btn onPress={() => this.saveAndExit('specifications')}>
                      <Text style={{color: '#fff'}}>Menyimpan</Text>
                    </Btn>
                  </View>
                </View>
              )}
            </View>
          )}
          {this.state.seg === 5 && (
            <View>
              {this.props.loading ? (
                <View
                  style={{
                    marginTop: hp('39%'),
                  }}>
                  <Spinner size={'large'} color={'red'} />
                </View>
              ) : (
                <View>
                  <View style={styles.topSectionBtn}>
                    <Text style={styles.notFinding}>
                      Tidak menemukan bagianmu?
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate(
                          'RequestNewCompatibility',
                        )
                      }
                      style={styles.reqBtn}>
                      <Text style={styles.reqBtnTxt}>
                        Minta Kompatibilitas baru
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{padding: 10}}>
                    {this.state.getCompatibilityDetails &&
                      this.state.getCompatibilityDetails.com_data.length >
                        0 && (
                        <View>
                          <View>
                            {this.state.getCompatibilityDetails.com_data.map(
                              (compatItem, index) => (
                                <View
                                  key={index}
                                  style={styles.compatEditStyle}>
                                  {compatItem.map((values, inn) => (
                                    <View
                                      key={inn}
                                      style={styles.compatEditTextStyle}>
                                      <Text>
                                        {values.select_attribute_name}:{' '}
                                      </Text>
                                      <Text>
                                        {values.select_attribute_value}
                                      </Text>
                                    </View>
                                  ))}
                                </View>
                              ),
                            )}
                          </View>

                          {console.log(
                            'this.state.getCompatibilityDetails',
                            this.state.getCompatibilityDetails,
                          )}

                          <View>
                            <Collapse
                              onToggle={() => {
                                this.loadCompatCollapseContentEdit(
                                  this.state.getCompatibilityDetails,
                                );
                              }}
                              key={
                                this.state.getCompatibilityDetails.com_group_id
                              }>
                              <CollapseHeader style={styles.Btn}>
                                <Text style={styles.Panel_Button_Text}>
                                  Tambah Kompatibilitas
                                </Text>
                                {/* <View style={styles.iconClass}>
                                <Image
                                  style={styles.iconImg}
                                  source={
                                    this.state.compatArray[index].active
                                      ? this.icons.up
                                      : this.icons.down
                                  }
                                />
                              </View> */}
                              </CollapseHeader>
                              <CollapseBody>
                                {this.state.innerCompatLoading ? (
                                  <View
                                    style={{
                                      height: hp('30%'),
                                    }}>
                                    <Spinner size={'large'} color={'red'} />
                                  </View>
                                ) : (
                                  <View>
                                    {console.log(
                                      'this.state.innerCompatabilityContent',
                                      this.state.innerCompatabilityContent,
                                    )}
                                    {this.state.innerCompatabilityContent.map(
                                      (field, index) => (
                                        <View
                                          key={index}
                                          style={styles.shippingWrapper}>
                                          {field.data.map(
                                            (innerField, innerIndex) => (
                                              <View key={innerIndex}>
                                                <ProductDinamicDropdown
                                                  indexParent={index}
                                                  innerIndex={innerIndex}
                                                  items={innerField.data || []}
                                                  keyId={'model_value'}
                                                  keyName={'com_name'}
                                                  onPress={({
                                                    val,
                                                    ref,
                                                    indexParent,
                                                    innerIndex,
                                                  }) => {
                                                    this.changeCompatValues(
                                                      val,
                                                      indexParent,
                                                      innerIndex,
                                                    );
                                                    ref.close();
                                                  }}
                                                  title={
                                                    innerField.attribute_name
                                                  }
                                                  value={innerField.value}
                                                />
                                              </View>
                                            ),
                                          )}
                                        </View>
                                      ),
                                    )}
                                    <View>
                                      <View
                                        style={{
                                          borderTopColor: '#e7e7e7',
                                          borderTopWidth: 1,
                                        }}
                                      />
                                      <TouchableOpacity
                                        onPress={() =>
                                          this.decreaseCompatInsideEdit(
                                            this.state.getCompatibilityDetails,
                                          )
                                        }
                                        style={styles.addBtn}>
                                        <Text style={styles.addTxt}>-</Text>
                                      </TouchableOpacity>
                                      <TouchableOpacity
                                        onPress={() =>
                                          this.increaseCompatInsideEdit(
                                            this.state.getCompatibilityDetails,
                                          )
                                        }
                                        style={styles.addBtn}>
                                        <Text style={styles.addTxt}>+</Text>
                                      </TouchableOpacity>
                                    </View>
                                    <View style={styles.btnWrapper}>
                                      <Btn
                                        onPress={() =>
                                          this.saveAndExit('compat')
                                        }>
                                        <Text style={{color: '#fff'}}>
                                          Menyimpan
                                        </Text>
                                      </Btn>
                                    </View>
                                  </View>
                                )}
                              </CollapseBody>
                            </Collapse>
                          </View>
                          <View style={styles.btnWrapper}>
                            <Btn
                              onPress={() => this.deleteSavedCompatibility()}>
                              <Text style={{color: '#fff'}}>Menghapus</Text>
                            </Btn>
                          </View>
                        </View>
                      )}

                    {!this.state.getCompatibilityDetails &&
                      this.state.compatArray.length > 0 &&
                      this.props.compatHeaders.map((header, index) => (
                        <View>
                          <Collapse
                            isCollapsed={this.state.compatArray[index].active}
                            onToggle={() => {
                              this.loadCompatCollapseContent(header);
                              // this.changeCompatCollapseState(header)
                            }}
                            key={header.group_id}>
                            <CollapseHeader style={styles.Btn}>
                              <Text style={styles.Panel_Button_Text}>
                                {header.group_name}
                              </Text>
                              <View style={styles.iconClass}>
                                <Image
                                  style={styles.iconImg}
                                  source={
                                    this.state.compatArray[index].active
                                      ? this.icons.up
                                      : this.icons.down
                                  }
                                />
                              </View>
                            </CollapseHeader>
                            <CollapseBody>
                              {this.state.innerCompatLoading ? (
                                <View
                                  style={{
                                    height: hp('30%'),
                                  }}>
                                  <Spinner size={'large'} color={'red'} />
                                </View>
                              ) : (
                                <View>
                                  {console.log(
                                    'this.state.innerCompatabilityContent',
                                    this.state.innerCompatabilityContent,
                                  )}
                                  {this.state.innerCompatabilityContent.map(
                                    (field, index) => (
                                      <View
                                        key={index}
                                        style={styles.shippingWrapper}>
                                        {field.data.map(
                                          (innerField, innerIndex) => (
                                            <View key={innerIndex}>
                                              <ProductDinamicDropdown
                                                indexParent={index}
                                                innerIndex={innerIndex}
                                                items={innerField.data || []}
                                                keyId={'model_value'}
                                                keyName={'com_name'}
                                                onPress={({
                                                  val,
                                                  ref,
                                                  indexParent,
                                                  innerIndex,
                                                }) => {
                                                  this.changeCompatValues(
                                                    val,
                                                    indexParent,
                                                    innerIndex,
                                                  );
                                                  ref.close();
                                                }}
                                                title={
                                                  innerField.attribute_name
                                                }
                                                value={innerField.value}
                                              />
                                            </View>
                                          ),
                                        )}
                                      </View>
                                    ),
                                  )}
                                  <View>
                                    <View
                                      style={{
                                        borderTopColor: '#e7e7e7',
                                        borderTopWidth: 1,
                                      }}
                                    />
                                    <TouchableOpacity
                                      onPress={() =>
                                        this.decreaseCompatInside(header)
                                      }
                                      style={styles.addBtn}>
                                      <Text style={styles.addTxt}>-</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      onPress={() =>
                                        this.increaseCompatInside(header)
                                      }
                                      style={styles.addBtn}>
                                      <Text style={styles.addTxt}>+</Text>
                                    </TouchableOpacity>
                                  </View>
                                  <View style={styles.btnWrapper}>
                                    <Btn
                                      onPress={() =>
                                        this.saveAndExit('compat')
                                      }>
                                      <Text style={{color: '#fff'}}>
                                        Menyimpan
                                        {/* INI MENYIMPAN */}
                                      </Text>
                                    </Btn>
                                  </View>
                                </View>
                              )}
                            </CollapseBody>
                          </Collapse>
                        </View>
                      ))}
                  </View>
                </View>
              )}
            </View>
          )}
          {this.state.seg === 6 && (
            <View style={styles.productReceive}>
              {this.props.loading ? (
                <View
                  style={{
                    marginTop: hp('39%'),
                  }}>
                  <Spinner size={'large'} color={'red'} />
                </View>
              ) : (
                <View>
                  <CardSection>
                    <View style={{width: '100%'}}>
                      <View style={styles.multi_SelectClass}>
                        <SectionedMultiSelect
                          ref={(SectionedMultiSelect) =>
                            (this.SectionedMultiSelect = SectionedMultiSelect)
                          }
                          selectText="Pilih opsi"
                          items={this.props.options}
                          selectedItems={this.state.selectedOptions}
                          onSelectedItemsChange={this.onSelectedOptionsChange}
                          uniqueKey={'option_id'}
                          displayKey={'name'}
                          searchPlaceholderText="Pilihan pencarian..."
                          showCancelButton={true}
                          showChips={false}
                        />
                      </View>
                      <TouchableOpacity
                        onPress={() => this.setState({optionsModal: true})}
                        style={{maxWidth: wp('50%')}}>
                        <Text style={styles.fileTypeRed}>
                          Buat opsi / varian baru
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </CardSection>
                  <View>
                    {this.state.optionSelectArray.length > 0 && (
                      <View>
                        {this.state.optionSelectArray.map((option, index) => (
                          <View key={index}>
                            <View style={styles.optionWrapper}>
                              <TouchableOpacity
                                onPress={() =>
                                  this.removeOptionSelected(option.option_id)
                                }
                                style={
                                  this.state.active_index == option.option_id
                                    ? styles.cross_Icon
                                    : styles.cross_Icon2
                                }>
                                <Text
                                  style={
                                    this.state.active_index == option.option_id
                                      ? styles.whiteColor
                                      : styles.redColor
                                  }>
                                  ✖
                                </Text>
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() =>
                                  this.selectOption(
                                    option.type,
                                    option.option_id,
                                    this.state.optionSelectArray,
                                  )
                                }
                                style={
                                  this.state.active_index == option.option_id
                                    ? styles.iconText
                                    : styles.iconText2
                                }>
                                <Text
                                  style={
                                    this.state.active_index == option.option_id
                                      ? styles.whiteColor
                                      : styles.blackColor
                                  }>
                                  {option.name}
                                </Text>
                              </TouchableOpacity>
                            </View>
                            {this.state.active_option_type == 'textarea' &&
                              this.state.active_index == option.option_id && (
                                <View style={styles.shippingWrapper}>
                                  <View
                                    style={{
                                      borderBottomColor: '#e7e7e7',
                                      borderBottomWidth: 1,
                                      marginBottom: 10,
                                    }}>
                                    <View style={{flexDirection: 'row'}}>
                                      <Text style={styles.dropheadingClass}>
                                        Required
                                      </Text>
                                    </View>
                                    <View style={styles.dropdownClass}>
                                      <Text>Wajib</Text>
                                      <Picker
                                        selectedValue={option.required}
                                        onValueChange={(itemValue) =>
                                          this.setOptionTextAreaRequired(
                                            itemValue,
                                            index,
                                          )
                                        }
                                        style={styles.pickerClass}>
                                        <Picker.Item label="Iya" value="1" />
                                        <Picker.Item label="Tidak" value="0" />
                                      </Picker>
                                    </View>
                                    <CardSection>
                                      <TextareaInner
                                        value={option.value}
                                        onChangeText={this.setOptionTextArea.bind(
                                          this,
                                          index,
                                        )}
                                        message={'not-mandatory'}
                                        label="Nilai Opsi"
                                      />
                                    </CardSection>
                                  </View>
                                </View>
                              )}
                            {(this.state.active_option_type == 'select' ||
                              this.state.active_option_type == 'checkbox' ||
                              this.state.active_option_type == 'radio') &&
                              this.state.active_index == option.option_id && (
                                <View>
                                  <View style={{flexDirection: 'row'}}>
                                    <Text>Wajib</Text>
                                  </View>

                                  <View style={styles.dropdownClass}>
                                    <Picker
                                      selectedValue={option.required}
                                      onValueChange={(itemValue) =>
                                        this.setOptionSelectRequired(
                                          itemValue,
                                          index,
                                        )
                                      }
                                      style={styles.pickerClass}>
                                      <Picker.Item label="Iya" value="1" />
                                      <Picker.Item label="Tidak" value="0" />
                                    </Picker>
                                  </View>
                                  {option.product_option_value.map(
                                    (innerArray, innerIndex) => (
                                      <View
                                        key={innerIndex}
                                        style={styles.shippingWrapper}>
                                        <View
                                          style={{
                                            borderBottomColor: '#e7e7e7',
                                            borderBottomWidth: 1,
                                            marginBottom: 10,
                                          }}>
                                          {option.option_value.length > 0 && (
                                            <View>
                                              <View
                                                style={{flexDirection: 'row'}}>
                                                <Text
                                                  style={
                                                    styles.dropheadingClass
                                                  }>
                                                  Nilai Opsi
                                                </Text>
                                              </View>
                                              <View>
                                                <Text>Nilai Opsi</Text>
                                              </View>
                                              <View
                                                style={styles.dropdownClass}>
                                                <Picker
                                                  selectedValue={
                                                    innerArray.option_value_id
                                                  }
                                                  onValueChange={(value) =>
                                                    this.setInnerOptionValueId(
                                                      value,
                                                      innerIndex,
                                                      index,
                                                    )
                                                  }
                                                  style={styles.pickerClass}>
                                                  {option.option_value.map(
                                                    (item) => {
                                                      return (
                                                        <Picker.Item
                                                          label={item.name}
                                                          value={
                                                            item.option_value_id
                                                          }
                                                          key={
                                                            item.option_value_id
                                                          }
                                                        />
                                                      );
                                                    },
                                                  )}
                                                </Picker>
                                              </View>
                                            </View>
                                          )}
                                          <CardSection>
                                            <InputInner
                                              message={'not-mandatory'}
                                              value={innerArray.quantity}
                                              onChangeText={this.setOptionInnerQuantity.bind(
                                                this,
                                                index,
                                                innerIndex,
                                              )}
                                              keyboardType={'numeric'}
                                              label="Jumlah Produk"
                                            />
                                          </CardSection>
                                          <View style={{flexDirection: 'row'}}>
                                            <Text>Mengurangi</Text>
                                          </View>
                                          <View style={styles.dropdownClass}>
                                            <Picker
                                              style={styles.pickerClass}
                                              selectedValue={
                                                innerArray.subtract
                                              }
                                              onValueChange={(value) =>
                                                this.setInnerSubtract(
                                                  value,
                                                  innerIndex,
                                                  index,
                                                )
                                              }>
                                              <Picker.Item
                                                label="Iya"
                                                value="1"
                                              />
                                              <Picker.Item
                                                label="Tidak"
                                                value="0"
                                              />
                                            </Picker>
                                          </View>
                                          <View style={{flexDirection: 'row'}}>
                                            <Text
                                              style={styles.dropheadingClass}>
                                              Harga
                                            </Text>
                                          </View>
                                          <View style={styles.dropdownClass}>
                                            <Picker
                                              style={styles.pickerClass}
                                              selectedValue={
                                                innerArray.price_prefix
                                              }
                                              onValueChange={(value) =>
                                                this.setInnerPriceSelect(
                                                  value,
                                                  innerIndex,
                                                  index,
                                                )
                                              }>
                                              <Picker.Item
                                                label="+"
                                                value="P"
                                              />
                                              <Picker.Item
                                                label="-"
                                                value="-"
                                              />
                                            </Picker>
                                          </View>
                                          <InputInner
                                            value={innerArray.price}
                                            onChangeText={this.setOptionInnerPrice.bind(
                                              this,
                                              index,
                                              innerIndex,
                                            )}
                                            message={'not-mandatory'}
                                            keyboardType={'numeric'}
                                            label="Harga"
                                          />
                                          <View style={{flexDirection: 'row'}}>
                                            <Text
                                              style={styles.dropheadingClass}>
                                              Bobot
                                            </Text>
                                          </View>
                                          <View style={styles.dropdownClass}>
                                            <Picker
                                              style={styles.pickerClass}
                                              selectedValue={
                                                innerArray.weight_prefix
                                              }
                                              onValueChange={(value) =>
                                                this.setInnerWeightSelect(
                                                  value,
                                                  innerIndex,
                                                  index,
                                                )
                                              }>
                                              <Picker.Item
                                                label="+"
                                                value="P"
                                              />
                                              <Picker.Item
                                                label="-"
                                                value="-"
                                              />
                                            </Picker>
                                          </View>
                                          <InputInner
                                            value={innerArray.weight}
                                            onChangeText={this.setOptionInnerWeight.bind(
                                              this,
                                              index,
                                              innerIndex,
                                            )}
                                            message={'not-mandatory'}
                                            label="Bobot"
                                            keyboardType={'numeric'}
                                          />
                                        </View>
                                      </View>
                                    ),
                                  )}
                                  <TouchableOpacity
                                    onPress={() =>
                                      this.decreaseInnerOptionArray(index)
                                    }
                                    style={styles.addBtn}>
                                    <Text style={styles.addTxt}>-</Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity
                                    onPress={() =>
                                      this.increaseInnerOptionArray(
                                        index,
                                        option,
                                      )
                                    }
                                    style={styles.addBtn}>
                                    <Text style={styles.addTxt}>+</Text>
                                  </TouchableOpacity>
                                </View>
                              )}
                            {(this.state.active_option_type == 'text' ||
                              this.state.active_option_type == 'file' ||
                              this.state.active_option_type == 'date' ||
                              this.state.active_option_type == 'datetime' ||
                              this.state.active_option_type == 'time') &&
                              this.state.active_index == option.option_id && (
                                <View style={styles.shippingWrapper}>
                                  <View
                                    style={{
                                      borderBottomColor: '#e7e7e7',
                                      borderBottomWidth: 1,
                                      marginBottom: 10,
                                    }}>
                                    <View style={{flexDirection: 'row'}}>
                                      <Text style={styles.dropheadingClass}>
                                        Yg dibutuhkan
                                      </Text>
                                    </View>
                                    <View style={styles.dropdownClass}>
                                      <Picker
                                        selectedValue={option.required}
                                        onValueChange={(itemValue) =>
                                          this.setOptionTextRequired(
                                            itemValue,
                                            index,
                                          )
                                        }
                                        style={styles.pickerClass}>
                                        <Picker.Item label="Iya" value="1" />
                                        <Picker.Item label="Tidak" value="0" />
                                      </Picker>
                                    </View>
                                    <CardSection>
                                      <InputInner
                                        value={option.value}
                                        onChangeText={this.setOptionText.bind(
                                          this,
                                          index,
                                        )}
                                        message={'not-mandatory'}
                                        label="Nilai Opsi"
                                      />
                                    </CardSection>
                                  </View>
                                </View>
                              )}
                          </View>
                        ))}
                      </View>
                    )}
                  </View>
                  <View style={styles.btnWrapper}>
                    <Btn onPress={() => this.saveAndExit('option')}>
                      <Text style={{color: '#fff'}}>Menyimpan</Text>
                    </Btn>
                  </View>
                </View>
              )}
            </View>
          )}
          {this.state.seg === 7 && (
            <View style={styles.productReceive}>
              <ProductDropdown
                value={this.state.returnAccepted}
                onPress={({val, ref}) => {
                  this.setState({returnAccepted: val});
                  ref.close();
                }}
                items={[
                  {name: 'Ya', value: 1},
                  {name: 'Tidak', value: 0},
                ]}
                title={'Terima Pengembalian?'}
              />
              <ProductDropdown
                value={this.state.validReturnDays}
                onPress={({val, ref}) => {
                  this.setState({validReturnDays: val});
                  ref.close();
                }}
                items={[
                  {name: '7 hari', value: 7},
                  {name: '14 hari', value: 14},
                  {name: '21 hari', value: 21},
                  {name: '30 hari', value: 30},
                ]}
                title={'Harus Dikembalikan Dalam'}
              />
              <ProductDropdown
                value={this.state.shippingPaidBy}
                onPress={({val, ref}) => {
                  this.setState({shippingPaidBy: val});
                  ref.close();
                }}
                items={[
                  {name: 'Pembeli', value: 1},
                  {name: 'Penjual', value: 0},
                ]}
                title={'Pengembalian Pengiriman Dibayar Oleh'}
              />

              <ProductDropdown
                value={this.state.restockingFee}
                onPress={({val, ref}) => {
                  this.setState({restockingFee: val});
                  ref.close();
                }}
                items={[
                  {name: 'Tidak', value: 0},
                  {name: '5%', value: 5},
                  {name: '10%', value: 10},
                  {name: '15%', value: 15},
                ]}
                title={'Biaya Restocking?'}
              />

              <View style={styles.btnWrapper}>
                <Btn onPress={() => this.saveAndExit('returns')}>
                  <Text style={{color: '#fff'}}>Menyimpan</Text>
                </Btn>
              </View>
            </View>
          )}
          {this.state.seg === 8 && (
            <View style={{padding: 10}}>
              <Collapse
                isCollapsed={this.state.dmState1}
                onToggle={() =>
                  this.setState({
                    dmState1: !this.state.dmState1,
                    dmState2: false,
                  })
                }>
                <CollapseHeader style={styles.Btn}>
                  <Text style={styles.Panel_Button_Text}>
                    Diskon Jumlah Masal
                  </Text>
                  <View style={styles.iconClass}>
                    <Image
                      style={styles.iconImg}
                      source={
                        this.state.dmState1 ? this.icons.up : this.icons.down
                      }
                    />
                  </View>
                </CollapseHeader>
                <CollapseBody>
                  {this.state.DiscountManagementQD.map((data, index) => (
                    <View key={index} style={styles.shippingWrapper}>
                      <View>
                        <CardSection>
                          <InputInner
                            value={data.quantity}
                            onChangeText={this.setQtyDiscountQuantity.bind(
                              this,
                              index,
                            )}
                            label="Kuantitas"
                            message={'not-mandatory'}
                          />
                        </CardSection>
                        <CardSection>
                          <InputInner
                            value={data.priority}
                            onChangeText={this.setQtyDiscountPriority.bind(
                              this,
                              index,
                            )}
                            label="Nomor Urut"
                            message={'not-mandatory'}
                          />
                        </CardSection>
                        <CardSection>
                          <InputInner
                            value={formatRupiah(data.price)}
                            onChangeText={this.setQtyDiscountPrice.bind(
                              this,
                              index,
                            )}
                            label="Harga spesial [Rp]"
                            message={'not-mandatory'}
                          />
                        </CardSection>
                        <View>
                          <Text
                            style={{
                              fontSize: hp('2%'),
                              marginTop: 5,
                              padding: 0,
                              margin: 0,
                              marginLeft: 4,
                              marginBottom: 10,
                              color: '#545454',
                            }}>
                            Pilih Tanggal Mulai
                          </Text>
                          <View style={styles.datepicherStyle}>
                            <DatePicker
                              defaultDate={data.start_date}
                              locale={'en'}
                              modalTransparent={false}
                              animationType={'fade'}
                              androidMode={'default'}
                              placeHolderText="Pilih Tanggal Mulai"
                              textStyle={{
                                color: '#000',
                                fontSize: hp('2%'),
                                lineHeight: hp('4.5%'),
                              }}
                              placeHolderTextStyle={{
                                height: '100%',
                                lineHeight: hp('4.5%'),
                                color: '#d3d3d3',
                                fontSize: hp('2%'),
                              }}
                              onDateChange={this.setQtyDiscountDateStart.bind(
                                this,
                                index,
                              )}
                            />
                          </View>
                        </View>
                        <CardSection />
                        <View>
                          <Text
                            style={{
                              fontSize: hp('2%'),
                              marginTop: 5,
                              padding: 0,
                              margin: 0,
                              marginLeft: 4,
                              marginBottom: 10,
                              color: '#545454',
                            }}>
                            Pilih Tanggal Berakhir
                          </Text>
                          <View style={styles.datepicherStyle}>
                            <DatePicker
                              defaultDate={data.end_date}
                              locale={'en'}
                              modalTransparent={false}
                              animationType={'fade'}
                              androidMode={'default'}
                              placeHolderText="Pilih Tanggal Berakhir"
                              textStyle={{
                                color: '#000',
                                fontSize: hp('2%'),
                                lineHeight: hp('4.5%'),
                              }}
                              placeHolderTextStyle={{
                                height: '100%',
                                lineHeight: hp('4.5%'),
                                color: '#d3d3d3',
                                fontSize: hp('2%'),
                              }}
                              onDateChange={this.setQtyDiscountDateEnd.bind(
                                this,
                                index,
                              )}
                            />
                          </View>
                        </View>
                      </View>
                      <View
                        style={{borderTopColor: '#e7e7e7', borderTopWidth: 1}}
                      />
                    </View>
                  ))}
                  <View>
                    <TouchableOpacity
                      disabled={this.state.DiscountManagementQD.length <= 1}
                      onPress={this.decreaseDiscountManagementQD}
                      style={styles.addBtn}>
                      <Text style={styles.addTxt}>-</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={this.increaseDiscountManagementQD}
                      style={styles.addBtn}>
                      <Text style={styles.addTxt}>+</Text>
                    </TouchableOpacity>
                  </View>
                </CollapseBody>
              </Collapse>
              <Collapse
                isCollapsed={this.state.dmState2}
                onToggle={() =>
                  this.setState({
                    dmState2: !this.state.dmState2,
                    dmState1: false,
                  })
                }>
                <CollapseHeader style={styles.Btn}>
                  <Text style={styles.Panel_Button_Text}>Diskon Khusus</Text>
                  <View style={styles.iconClass}>
                    <Image
                      style={styles.iconImg}
                      source={
                        this.state.dmState2 ? this.icons.up : this.icons.down
                      }
                    />
                  </View>
                </CollapseHeader>
                <CollapseBody>
                  {this.state.DiscountManagementSD.map((data, index) => (
                    <View key={index} style={styles.shippingWrapper}>
                      <View>
                        <CardSection>
                          <InputInner
                            value={data.priority}
                            onChangeText={this.setSplDiscountPriority.bind(
                              this,
                              index,
                            )}
                            label="Nomor Urut"
                            message={'not-mandatory'}
                          />
                        </CardSection>
                        <CardSection>
                          <InputInner
                            value={formatRupiah(data.price)}
                            onChangeText={this.setSplDiscountPrice.bind(
                              this,
                              index,
                            )}
                            label="Harga [Rp]"
                            message={'not-mandatory'}
                          />
                        </CardSection>
                        <View>
                          <Text
                            style={{
                              fontSize: hp('2%'),
                              marginTop: 5,
                              padding: 0,
                              margin: 0,
                              marginLeft: 4,
                              marginBottom: 10,
                              color: '#545454',
                            }}>
                            Tanggal Mulai
                          </Text>
                          <View style={styles.datepicherStyle}>
                            <DatePicker
                              defaultDate={data.start_date}
                              locale={'en'}
                              modalTransparent={false}
                              animationType={'fade'}
                              androidMode={'default'}
                              placeHolderText="Tanggal Mulai"
                              textStyle={{
                                color: '#000',
                                fontSize: hp('2%'),
                                lineHeight: hp('4.5%'),
                              }}
                              placeHolderTextStyle={{
                                height: '100%',
                                lineHeight: hp('4.5%'),
                                color: '#d3d3d3',
                                fontSize: hp('2%'),
                              }}
                              onDateChange={this.setSplDiscountDateStart.bind(
                                this,
                                index,
                              )}
                            />
                          </View>
                        </View>
                        <CardSection />
                        <View>
                          <Text
                            style={{
                              fontSize: hp('2%'),
                              marginTop: 5,
                              padding: 0,
                              margin: 0,
                              marginLeft: 4,
                              marginBottom: 10,
                              color: '#545454',
                            }}>
                            Tanggal Berakhir
                          </Text>
                          <View style={styles.datepicherStyle}>
                            <DatePicker
                              defaultDate={data.end_date}
                              locale={'en'}
                              modalTransparent={false}
                              animationType={'fade'}
                              androidMode={'default'}
                              placeHolderText="Tanggal Berakhir"
                              textStyle={{
                                color: '#000',
                                fontSize: hp('2%'),
                                lineHeight: hp('4.5%'),
                              }}
                              placeHolderTextStyle={{
                                height: '100%',
                                lineHeight: hp('4.5%'),
                                color: '#d3d3d3',
                                fontSize: hp('2%'),
                              }}
                              onDateChange={this.setSplDiscountDateEnd.bind(
                                this,
                                index,
                              )}
                            />
                          </View>
                        </View>
                      </View>
                      <View
                        style={{borderTopColor: '#e7e7e7', borderTopWidth: 1}}
                      />
                    </View>
                  ))}
                  <View>
                    <TouchableOpacity
                      disabled={this.state.DiscountManagementSD.length <= 1}
                      onPress={this.decreaseDiscountManagementSD}
                      style={styles.addBtn}>
                      <Text style={styles.addTxt}>-</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={this.increaseDiscountManagementSD}
                      style={styles.addBtn}>
                      <Text style={styles.addTxt}>+</Text>
                    </TouchableOpacity>
                  </View>
                </CollapseBody>
              </Collapse>
              <View style={styles.btnWrapper}>
                <Btn onPress={() => this.saveAndExit('discount')}>
                  <Text style={{color: '#fff'}}>Menyimpan</Text>
                </Btn>
              </View>
            </View>
          )}
        </ScrollView>
      </Container>
    );
  }
}
const styles = {
  compatEditStyle: {
    borderRadius: 3,
    elevation: 1,
    margin: 5,
    padding: 5,
    backgroundColor: '#F5F5F5',
  },
  compatEditTextStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rightArrow: {
    position: 'absolute',
    right: 0,
  },

  Panel_Button_Text: {
    fontSize: hp('2.1%'),
    textAlign: 'left',
    color: '#545454',
    fontFamily: Font.RobotoBold,
  },
  Btn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: hp('2.2%'),
    paddingHorizontal: 10,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  iconImg: {
    width: 16,
    height: 9,
  },
  multi_SelectClass: {
    elevation: 1,
    borderRadius: 50,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
    height: hp('8%'),
    lineHeight: 0,
  },
  optionWrapper: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  cross_Icon: {
    backgroundColor: '#f00',
    width: '20%',
    padding: 10,
  },
  cross_Icon2: {
    backgroundColor: '#fff',
    width: '20%',
    padding: 10,
  },
  iconText: {
    backgroundColor: '#7892EB',
    width: '80%',
    padding: 10,
  },
  iconText2: {
    backgroundColor: '#fff',
    width: '80%',
    padding: 10,
  },

  whiteColor: {
    color: '#fff',
  },
  blackColor: {
    color: '#000',
  },
  redColor: {
    color: '#000',
  },
  required: {
    borderColor: '#e7e7e7',
    borderWidth: 1,
    padding: 10,
    marginBottom: 10,
  },
  crossColor: {
    textAlign: 'center',
    color: '#000',
  },
  closeBtn2: {
    position: 'absolute',
    right: 0,
    width: 30,
    height: 30,
    top: 0,
    zIndex: 1002,
    color: '#fff',
    borderRadius: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },

  containerStyle: {
    padding: 0,
    // flex:1
  },

  row: {
    flexDirection: 'row',
    flexWarp: 'warp',
    justifyContent: 'space-between',
  },

  datepicherStyle: {
    width: '100%',
    color: '#000',
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: wp('4%'),
    marginTop: 1,
    backgroundColor: '#fff',
    height: hp('8%'),
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  pickerClass: {
    height: 40,
    width: 'auto',
  },
  dropheadingClass: {
    display: 'none',
  },
  btnWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 15,
  },
  segmentStyle: {
    backgroundColor: '#fff',
    elevation: 1,
    textAlign: 'left',
    justifyContent: 'flex-start',
    // paddingLeft: 30,
    height: 'auto',
    paddingTop: 10,
    paddingBottom: 0,
    // paddingRight: 30,
    alignItems: 'center',
  },
  segmentBtn: {
    borderWidth: 0,
    borderColor: 'transparent',
    bottom: 0,
    margin: 0,
    paddingTop: 10,
    paddingBottom: 10,
    height: 'auto',
    marginBottom: 0,
  },
  segmentBtnTxt: {
    color: '#000',
  },
  segmentBtnTxtActive: {
    color: '#e84118',
  },
  segmentActive: {
    backgroundColor: '#fff',
    borderWidth: 3,
    borderColor: 'transparent',
    borderBottomWidth: 4,
    borderBottomColor: '#e84118',
    // borderTopRightRadius: 10,
    // borderTopLeftRadius: 10,
    paddingTop: 10,
    height: 'auto',
    paddingBottom: 10,
    color: '#fff',
  },

  abc: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingLeft: 5,
    textAlign: 'left',
    flexWrap: 'wrap',
    width: '100%',
    flexBasis: '100%',
  },

  labelStyle2Wrapper: {
    width: '87%',
    flexBasis: '87%',
    // fontSize:hp('30%'),
  },
  inputWrapper2: {
    width: '100%',
    flexBasis: '87%',
    // fontSize:hp('30%'),
  },
  labelTxt: {
    color: '#545454',
    marginLeft: 0,
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.3%'),
  },
  billingAddress: {
    fontSize: hp('2.5%'),
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    marginBottom: hp('1%'),
    borderBottomWidth: 1,
    borderBottomColor: '#e7e7e7',
    paddingBottom: 10,
    marginTop: hp('1.5%'),
  },

  labelStyle: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  inputWrapper: {
    flexDirection: 'row',
    marginBottom: hp('3%'),
    justifyContent: 'space-between',
  },

  dropdownClass: {
    // elevation: 1,
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  shippingWrapper: {
    padding: 10,
    borderColor: '#e7e7e7',
    borderWidth: 1,
    marginTop: hp('2%'),
  },
  addBtn: {
    backgroundColor: '#DC0028',
    width: wp('20%'),
    height: wp('10%'),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    alignSelf: 'flex-end',
    marginBottom: 10,
  },
  addTxt: {
    color: '#fff',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: wp('7%'),
  },

  noFileTxt: {
    marginLeft: 10,
  },
  fileType: {
    fontSize: hp('1.75%'),
    // marginTop: 5,
    paddingVertical: 7,
    fontFamily: Font.RobotoLight,
  },
  validText: {
    fontSize: hp('1.75%'),
    // marginTop: 5,
    fontFamily: Font.RobotoLight,
  },
  validYoutube: {
    fontSize: hp('1.75%'),
    // marginTop: 5,
    color: '#DC0028',
    fontFamily: Font.RobotoLight,
  },
  fileTypeRed: {
    fontSize: hp('1.75%'),
    marginTop: 10,
    paddingLeft: 8,
    fontFamily: Font.RobotoLight,
    color: '#DC0028',
  },

  topSectionBtn: {
    marginTop: hp('2%'),
    marginBottom: hp('1%'),
    padding: 10,
    textAlign: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  notFinding: {
    fontSize: hp('2.2%'),
    fontFamily: Font.RobotoRegular,
    marginBottom: hp('1%'),
  },
  reqBtn: {
    backgroundColor: '#2daebf',
    alignSelf: 'center',
    marginBottom: hp('1%'),
    borderRadius: 5,
  },
  reqBtnTxt: {
    color: '#fff',
    fontSize: hp('1.7%'),
    fontFamily: Font.RobotoRegular,
    textAlign: 'center',
    paddingHorizontal: hp('1%'),
    paddingVertical: hp('0.9%'),
    borderRadius: 5,
  },
};

const mapStateToProps = (state) => {
  return {
    user_id: state.profile.profile_details.user_id,
    categories: state.category.categories,
    brands: state.brands.brands,
    processing_time: state.process.processing_time,
    loading: state.brands.loading,
    countries: state.country.countries,
    ships_to_countries: state.country.ships_to_countries,
    companies: state.country.companies,
    loadingShipping: state.country.loading,
    carrier_service: state.country.carrier_service,
    specification_details: state.country.specification_details,
    product_filters: state.country.product_filters,
    related_products: state.country.related_products,
    add_product_loading: state.addProduct.loading,
    saveProductloading: state.addProduct.saveload,
    product_image: state.addProduct.product_image,
    product_id: state.addProduct.product_id,
    admincommphysical: state.addProduct.admincommphysical,
    admincommdigital: state.addProduct.admincommdigital,
    midtrans_fee: state.addProduct.midtrans_fee,
    dcommission: state.addProduct.dcommission,
    pcommission: state.addProduct.pcommission,
    admin_commission: state.addProduct.admin_commission,
    createOptionsLoader: state.country.createOptionsLoader,
    optionCreated: state.country.optionCreated,
    options: state.country.options,
    compatHeaders: state.country.compatData,
    innerCompatibilityData: state.country.compatInnerContent,
    compatInnerLoading: state.country.compatInnerLoading,
    group_id: state.country.group_id,
    product_data: state.addProduct.product_data,
    loading2: state.addProduct.loading,
    currentTab: state.addProduct.currentTab,
  };
};

export default connect(mapStateToProps, {
  setMainImageDone,
  emptyProductData,
  editProductView,
  loadCompatibilityContentDropdown,
  loadCompatibilityContent,
  loadCompatibilities,
  loadOptions,
  createNewOption,
  getRelatedProducts,
  getProdcutFilters,
  getSpecifications,
  saveProduct,
  removeGeneralImages,
  delete_image_general,
  upload_images_general,
  getCarrierService,
  getShippingCompany,
  getShipsToCountries,
  getCountries,
  getProcessingTime,
  getCategoryList,
  getBrands,
  getProductFee,
})(AddProduct);
