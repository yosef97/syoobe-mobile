import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  ScrollView,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {
  notificationApi,
  notificationView,
  notificationViewAll,
} from '../services/services';
import {connect} from 'react-redux';
import {storeUnreadNotifications, storeNotifications} from '../actions';
import {Spinner} from './common';
import PTRView from 'react-native-pull-to-refresh';
import {Content, Button, Text as TextNative} from 'native-base';
import moment from 'moment';
import 'moment/locale/id';

class NotificationActive extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      _token: null,
    };
  }

  handleNotificationViewAll = (token) => {
    console.log('handleNotificationViewAll', token);

    var details = {
      _token: token,
    };
    notificationViewAll(details)
      .then((response) => {
        console.log('ressss', response.data);
        if (response.data.status === 1) {
          this.props.navigation.push('Notifications');
        }
      })
      .catch((error) => console.log('ee', error));
  };

  componentDidUpdate = (prevProps) => {
    if (
      this.props.unreadNotificationCount != prevProps.unreadNotificationCount
    ) {
      this.callNotifications(this.state._token);
    }
    // console.log('ini prevProps', prevProps);
  };

  async componentDidMount() {
    await AsyncStorage.getItem('token').then((res) =>
      this.setState({_token: res}),
    );
    this.callNotifications(this.state._token);
  }

  callNotifications = (token) => {
    this.setState({
      loading: true,
    });
    const details = {
      _token: token,
    };
    notificationApi({...details})
      .then((response) => {
        // console.log('response', response.data);
        if (response.data.status === 1) {
          this.props.storeUnreadNotifications(
            response.data.total_unread_notification,
          );
          this.props.storeNotifications(
            response.data.user_notification_details,
          );
        }
        this.setState({
          loading: false,
        });
      })
      .catch((error) => console.log('ee', error));
  };

  goToNotificationDetails = (
    sub,
    order_product_id,
    unique_tnd_id,
    order_id,
    type,
  ) => {
    console.log('ini sub', JSON.stringify(sub));

    // return;
    const detailsView = {
      _token: this.state._token,
      unique_notification_id: unique_tnd_id,
    };
    notificationView({...detailsView})
      .then((response) => {
        console.log('rrr', response);
      })
      .catch((error) => console.warn('error', error));
    let details = {
      _token: this.state._token,
      order_id: order_id,
      order_product_id: order_product_id,
    };
    if (sub === 'vendor_order_email') {
      this.props.navigation.replace('SalesPhysical', {details});
    } else if (sub === 'order_cancellation_notification') {
      this.props.navigation.replace('SalesPhysical', {details});
    } else if (sub === 'product_review') {
      this.props.navigation.replace('SalesPhysical', {details});
    } else if (sub === 'product_return') {
      this.props.navigation.replace('SalesPhysical', {details});
    } else if (sub === 'account_credited_debited') {
      this.props.navigation.replace('HomeDrawer', {screen: 'MyWallet'});
    } else if (sub === 'product_return') {
      this.props.navigation.replace('SalesPhysical', {details});
    } else if (sub === 'manual_file_accept_notification') {
      this.props.navigation.replace('SalesPhysical', {details});
    } else if (sub === 'revision_request_notification') {
      this.props.navigation.replace('SalesPhysical', {details});
    } else if (sub === 'revoke_access_notification_mail') {
      if (type === 'P') {
        this.props.navigation.replace('ViewOrderPhysical', {details});
      } else if (type === 'D') {
        this.props.navigation.replace('ViewOrderDigital', {details});
      }
    } else if (sub === 'grant_access_notification_mail') {
      if (type === 'P') {
        this.props.navigation.replace('ViewOrderPhysical', {details});
      } else if (type === 'D') {
        this.props.navigation.replace('ViewOrderDigital', {details});
      }
    } else if (sub === 'sample_upload_notification') {
      if (type === 'P') {
        this.props.navigation.replace('ViewOrderPhysical', {details});
      } else if (type === 'D') {
        this.props.navigation.replace('ViewOrderDigital', {details});
      }
    } else if (sub === 'child_order_status_change') {
      if (type === 'P') {
        this.props.navigation.replace('ViewOrderPhysical', {details});
      } else if (type === 'D') {
        this.props.navigation.replace('ViewOrderDigital', {details});
      }
    } else if (sub === 'return_request_status_change_notification') {
      this.props.navigation.replace('SalesPhysical', {details});
    } else if (sub === 'return_request_status_change_notification') {
      this.props.navigation.replace('SalesPhysical', {details});
    } else if (sub === 'syoobe_chat') {
      this.props.navigation.replace('HomeDrawer', {screen: 'ChatWebView'});
    }
  };

  render() {
    // console.log('ini state', JSON.stringify(this.state));
    // console.log('ini props', JSON.stringify(this.props));

    if (this.state.loading) {
      return (
        <View style={styles.loadingNoImageStyle}>
          <Spinner color="red" />
        </View>
      );
    }

    if (this.props.notifications.length === 0) {
      return (
        <View style={styles.loadingNoImageStyle}>
          <Image
            style={styles.notificationImgStyle}
            resizeMode="contain"
            source={require('../images/no_notifications.png')}
          />
          <Text style={styles.textStyleNoNoti}>No Notifications Present</Text>
        </View>
      );
    }
    // console.log(this.props.notifications);

    return (
      <View style={styles.bgColor}>
        <PTRView onRefresh={() => this.callNotifications(this.state._token)}>
          <ScrollView>
            <View
              style={{
                padding: 8,
                alignItems: 'center',
                backgroundColor: '#E4E7E9',
              }}>
              <Content>
                <Button
                  small
                  rounded
                  danger
                  disabled={!this.state._token}
                  onPress={() =>
                    this.handleNotificationViewAll(this.state._token)
                  }>
                  <TextNative>Tandai dan Pindahkan ke Arsip</TextNative>
                </Button>
              </Content>
            </View>
            <View style={{padding: 10, paddingBottom: 0}}>
              {this.props.notifications.map((notification, index) => (
                <View
                  key={notification.unique_tnd_id + index}
                  style={
                    notification.tnd_read_unread_status === '1'
                      ? styles.none
                      : styles.boxStyle
                  }>
                  <View style={styles.imgContentWrapper2}>
                    <View style={styles.imgWrapper2}>
                      <Image
                        style={styles.boxImg3}
                        resizeMode="cover"
                        source={{
                          uri: notification.tnd_notification_image
                            ? notification.tnd_notification_image
                            : null,
                        }}
                      />
                    </View>
                    <View style={{flex: 1}}>
                      <TouchableOpacity
                        onPress={() =>
                          this.goToNotificationDetails(
                            notification.tnd_notification_subject,
                            notification.tnd_opr_id,
                            notification.unique_tnd_id,
                            notification.tnd_order_id,
                            notification.tnd_product_type,
                          )
                        }>
                        <View style={styles.headingWrapper}>
                          <Text style={styles.productName}>
                            {notification.tnd_notification_title}
                          </Text>
                        </View>
                        <Text style={styles.rtText}>
                          {moment(notification.tnd_notification_created).format(
                            'MMMM Do YYYY, h:mm:ss a',
                          )}
                        </Text>
                        <Text style={styles.description}>
                          {notification.tnd_notification_msg}
                        </Text>
                        {
                          // notification.tnd_notification_subject == 'vendor_order_email' &&
                          <View style={styles.buttonViewStyle}>
                            <Text style={styles.notiViewStyle}>VIEW</Text>
                          </View>
                        }
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              ))}
            </View>
          </ScrollView>
        </PTRView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    notifications: state.notification.notifications,
    unreadNotificationCount: state.notification.unreadNotificationCount,
  };
};

// const mapDispatchToProps = dispatch => {
//     return {
//         storeNotifications : (data) => dispatch({type: actionTypes.NOTIFICATIONS, payload: data}),
//         storeUnreadNotifications: (count) => dispatch({type: actionTypes.UNREAD_NOTIFICATIONS, payload: count})
//     }
// }

const styles = StyleSheet.create({
  buttonViewStyle: {
    alignItems: 'flex-end',
    padding: 3,
    borderRadius: 5,
    width: 'auto',
  },
  notiViewStyle: {
    color: '#C90205',
    fontFamily: Font.RobotoBold,
    fontSize: hp('2%'),
  },
  opacity: {
    opacity: 0.5,
  },
  textStyleNoNoti: {
    fontSize: hp('2%'),
    fontFamily: Font.RobotoMedium,
  },
  notificationImgStyle: {
    height: 100,
  },
  loadingNoImageStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: '100%',
    width: '100%',
  },
  bgColor: {
    backgroundColor: '#fff',
  },
  rtText: {
    paddingVertical: 3,
    color: '#a6a6a6',
    fontSize: hp('1.4%'),
    fontFamily: Font.RobotoLight,
  },
  boxStyle: {
    width: '100%',
    minHeight: hp('13%'),
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 0,
    flexDirection: 'row',
    flexWrap: 'wrap',
    position: 'relative',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    marginBottom: 10,
  },
  imgContentWrapper2: {
    flexDirection: 'row',
    flex: 1,
    paddingRight: 15,
    paddingBottom: 10,
  },
  imgWrapper2: {
    flexDirection: 'row',
    overflow: 'hidden',
  },
  boxImg3: {
    width: wp('10%'),
    height: hp('7%'),
    marginRight: wp('3%'),
  },
  productName: {
    fontSize: hp('2.5%'),
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },

  headingWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  description: {
    color: '#858585',
    fontFamily: Font.RobotoLight,
    fontSize: hp('2%'),
  },

  none: {
    display: 'none',
  },
});

export default connect(mapStateToProps, {
  storeUnreadNotifications,
  storeNotifications,
})(NotificationActive);
