import React, {Component} from 'react';
import {AsyncStorage, View} from 'react-native';
import {connect} from 'react-redux';
import {getOrderHistoryStatusList, getSalesList} from '../actions';
import {MyOrderTabNavigator} from './routes/HistoryTab';

class Sales extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _token: null,
      fromDate: null,
      toDate: null,
      loading: false,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  goToSalesView = (item) => {
    this.props.navigation.navigate('SalesPhysical', {
      order_product_id: item.order_product_id,
      _token: this.state._token,
    });
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    this.retrieveToken().then((_token) => {
      this.props.getOrderHistoryStatusList(_token);
    });
  };

  getSalesList = (id) => {
    // this.setState({loading: true});
    this.retrieveToken().then((_token) => {
      let details = {
        _token,
        status: id,
      };
      this.props.getSalesList(details);
      this.setState({loading: false});
    });
  };

  render() {
    // console.log('ini state', JSON.stringify(this.state));
    console.log('ini props', JSON.stringify(this.props));
    return (
      <View style={{flex: 1, backgroundColor: '#f5f6fa'}}>
        {this.props.status_list && this.props.status_list.length > 0 && (
          <MyOrderTabNavigator
            loading={this.state.loading}
            lists={this.props.status_list}
            navigation={this.props.navigation}
            getOrder={(id) => this.getSalesList(id)}
            type="sales"
            title="Belum ada penjualan apappun disini"
          />
        )}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    status_list: state.buyerOrders.status_list,
    loading: state.buyerOrders.loading,
  };
};

export default connect(mapStateToProps, {
  getOrderHistoryStatusList,
  getSalesList,
})(Sales);
