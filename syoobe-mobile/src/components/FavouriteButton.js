import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';
import {Image, TouchableOpacity, AsyncStorage} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {setProductAsFavourite, setProductAsUnfavourite} from '../actions';
import axios from 'axios';
import {showToast} from '../helpers/toastMessage';

class FavouriteButton extends Component {
  state = {
    fav: null,
  };

  componentDidMount = () => {
    if (this.props.favorite == 1) {
      this.setState({
        fav: true,
      });
    } else {
      this.setState({
        fav: false,
      });
    }
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  favouriteProduct = async () => {
    this.retrieveToken().then((_token) => {
      var details = {
        _token: _token,
        product_id: this.props.product_id,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      axios
        .post(
          'https://syoobe.co.id/api/mark_product_favorite?favorite=1',
          formBody,
        )
        .then((response) => {
          console.log(response);
          if (response.data.status == true) {
            this.setState({
              fav: true,
            });
            showToast({
              message: 'Produk berhasil ditambah ke favorite',
            });
          } else {
            showToast({
              message: response.data.msg,
            });
          }
        })
        .catch((error) => {
          console.log('Error: ' + error);
        });
    });
  };

  unFavouriteProduct = () => {
    this.retrieveToken().then((_token) => {
      var details = {
        _token: _token,
        product_id: this.props.product_id,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      axios
        .post('https://syoobe.co.id/api/mark_product_favorite', formBody)
        .then((response) => {
          console.log('fav', response);
          if (response.data.status == true) {
            this.setState({
              fav: false,
            });
            showToast({
              message: 'Produk berhasil dihapus dari favorite',
            });
          }
        })
        .catch((error) => {
          console.log('Error: ' + error);
        });
    });
  };

  renderFavouriteIcon = () => {
    return (
      <TouchableOpacity
        onPress={() => this.unFavouriteProduct()}
        style={styles.button}>
        <Image
          style={styles.like_icon}
          source={require('../images/like3.png')}
        />
      </TouchableOpacity>
    );
  };

  renderUnfavouriteIcon = () => {
    return (
      <TouchableOpacity
        onPress={() => this.favouriteProduct()}
        style={styles.button}>
        <Image
          style={styles.like_icon}
          source={require('../images/like_icon.png')}
        />
      </TouchableOpacity>
    );
  };

  render() {
    // console.log(this.props)
    if (this.state.fav == true) {
      return this.renderFavouriteIcon();
    } else {
      return this.renderUnfavouriteIcon();
    }
  }
}

const styles = {
  like_icon: {
    width: wp('5%'),
    height: wp('4.5%'),
  },
  button: {
    position: 'absolute',
    top: 15,
    right: 15,
    width: wp('5%'),
    height: wp('4.5%'),
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.fav.loading,
  };
};

export default connect(mapStateToProps, {
  setProductAsFavourite,
  setProductAsUnfavourite,
})(FavouriteButton);
