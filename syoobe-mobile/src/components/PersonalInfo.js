import React, {Component} from 'react';
import {AsyncStorage, ScrollView, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {
  changePersonalInfoCity,
  changePersonalInfoName,
  changePersonalInfoPhone,
  getCountries,
  getProfileDetails,
  getStates,
  updateProfile,
} from '../actions';
import {
  setComponentRedirect,
  setSelectedCity,
  setSelectedCountry,
  setSelectedProvince,
  setSelectedStates,
} from '../actions/AdressAction';
import {showToast} from '../helpers/toastMessage';
import {CardSection, FooterButton} from './common';
import InputText from './common/InputText';
import {Font} from './Font';
import Loader from './Loading';

class PersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countries: [],
      states: [],
      PickerSelectedCountry: props.route.params.profileDetails.country,
      PickerSelectedState: props.route.params.profileDetails.state,
      selectedCountryId: props.route.params.profileDetails.country_id,
      selectedStateId: props.route.params.profileDetails.state_id,
      _token: null,
      loading: true,
      form: {
        name: props.profileDetails?.name,
        phone: props.profileDetails?.phone,
        city: props.profileDetails?.city_town,
        country_id: props.profileDetails?.country_id,
        country: props.profileDetails?.country,
        state: props.profileDetails?.state,
        state_id: props.profileDetails?.state_id,
      },
    };
  }

  componentDidUpdate = (prevProps, prevState) => {
    const {
      selectedProvince,
      profileDetails,
      selectedCity,
      selectedCountry,
    } = this.props;
    if (this.props.updated != prevProps.updated) {
      if (this.props.updated) {
        this.props.getProfileDetails(this.state._token);

        this.props.navigation.navigate('HomeDrawer', {screen: 'EditProfile'});
      }
    }
    let state =
      selectedProvince?.province ||
      selectedProvince?.name ||
      profileDetails?.state;
    let state_id =
      selectedProvince?.province_id ||
      selectedProvince?.id ||
      profileDetails?.state_id;
    let country_id = selectedCountry?.id || profileDetails?.country_id;
    let country = selectedCountry?.name || profileDetails?.country;
    if (prevState.form.city != prevProps.selectedCity?.city_name) {
      selectedCity?.city_name &&
        this.handleChangeText(selectedCity?.city_name, 'city');
    }
    if (prevState.form.state_id != state_id) {
      this.handleChangeText(state_id, 'state_id');
    }
    if (prevState.form.state != state) {
      this.handleChangeText(state, 'state');
    }
    if (prevState.form.country_id != country_id) {
      this.handleChangeText(country_id, 'country_id');
    }
    if (prevState.form.country != country) {
      this.handleChangeText(country, 'country');
    }
  };

  componentDidMount() {
    this.retrieveToken().then((_token) => {
      this.props.getProfileDetails(_token);
      this.setState({
        _token: _token,
      });
      this.setState({
        loading: false,
      });
    });
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  storeUsername = async (name) => {
    try {
      await AsyncStorage.setItem('name', name);
    } catch (error) {
      console.log(error);
    }
  };

  removeUsername = async () => {
    try {
      await AsyncStorage.removeItem('name');
    } catch (error) {
      console.log(error);
    }
  };

  updateProfile = async () => {
    const {
      selectedCountry,
      profileDetails,
      setSelectedCountry,
      setComponentRedirect,
      navigation,
      selectedProvince,
      selectedCity,
    } = this.props;
    if (!this.state.form.name) {
      showToast({
        message: 'Silakan pilih nama Anda!',
      });
    } else if (!this.state.form.phone) {
      showToast({
        message: 'Silakan pilih nomor telepon Anda!',
      });
    } else if (!this.state.form.country_id) {
      showToast({
        message: 'Silahkan pilih negara terlebih dahulu!',
      });
    } else if (!this.state.form.state_id) {
      console.log(
        !selectedProvince?.province_id ||
          (!selectedProvince?.id && selectedProvince?.id),
      );
      showToast({
        message: 'Silahkan pilih provinsi terlebih dahulu!',
      });
    } else if (!this.state.form.city) {
      showToast({
        message: 'Silakan masukkan Nama Kota!',
      });
    } else {
      // this.removeUsername();
      // this.storeUsername(this.props.name);
      this.props.updateProfile(this.props.navigation, {
        _token: this.state._token,
        user_id: profileDetails.user_id,
        name: this.state.form.name,
        phone: this.state.form.phone,
        city: this.state.form.city,
        country_id: this.state.form.country_id,
        state_id: this.state.form.state_id,
      });
    }
  };

  handleChangeText = (text, type) => {
    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        [type]: text,
      },
    }));
  };

  render() {
    if (this.props.loading || this.state.loading) {
      return <Loader />;
    }
    const {
      selectedCountry,
      profileDetails,
      setSelectedCountry,
      setComponentRedirect,
      navigation,
      selectedProvince,
      selectedCity,
    } = this.props;
    console.log('profileDetails', this.state);
    return (
      <View style={{flex: 1}}>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          style={styles.scrollClass}>
          <View style={styles.viewStyle}>
            <CardSection>
              <InputText
                onChangeText={(e) => null}
                value={profileDetails?.user_username}
                placeholder="Username"
                label="Username"
                editable={false}
              />
            </CardSection>
            <CardSection>
              <InputText
                onChangeText={(e) => null}
                value={profileDetails?.user_email}
                placeholder="Email"
                label="Email"
                editable={false}
              />
            </CardSection>

            <CardSection>
              <InputText
                onChangeText={(e) => this.handleChangeText(e, 'name')}
                value={this.state.form.name}
                placeholder="Masukkan Nama Lengkap"
                label="Nama Lengkap *"
              />
            </CardSection>
            <CardSection>
              <InputText
                onChangeText={(e) => this.handleChangeText(e, 'phone')}
                value={this.state.form.phone || profileDetails?.phone}
                placeholder="Masukkan nomor telepon"
                label="Nomor telepon *"
              />
            </CardSection>

            <CardSection>
              <InputText
                value={this.state.form.country}
                onFocus={() => {
                  setSelectedCountry(
                    {
                      id: this.state.form.country_id,
                      name: this.state.form.country,
                    },
                    navigation,
                    false,
                  );
                  setComponentRedirect('PersonalInfo');
                  navigation.navigate('Country');
                }}
                placeholder={'Pilih Negara'}
                label="Negara *"
                icon={'chevron-down'}
              />
            </CardSection>
            <CardSection>
              <InputText
                value={this.state.form.state}
                onFocus={() => {
                  navigation.navigate(
                    this.state.form.country_id ? 'Province' : 'Country',
                  );
                  setComponentRedirect('PersonalInfo');
                }}
                placeholder={'Pilih Provinsi'}
                label="Provinsi *"
                icon={'chevron-down'}
              />
            </CardSection>
            <CardSection>
              <InputText
                value={this.state.form.city}
                onFocus={() => {
                  if (
                    selectedCountry?.id == 106 ||
                    profileDetails?.country_id == 106
                  ) {
                    navigation.navigate(
                      this.state.form.country_id ? 'City' : 'Country',
                      {province_id: this.state.form.state_id},
                    );
                    setComponentRedirect('PersonalInfo');
                  }
                }}
                onChangeText={(e) => {
                  if (
                    selectedCountry?.id !== '106' &&
                    !profileDetails?.country_id !== '106'
                  ) {
                    this.handleChangeText(e, 'city');
                  }
                }}
                placeholder={'Pilih Kota/Kabupaten'}
                label="Kota/Kabupaten *"
                icon={
                  selectedCountry?.id == 106 ||
                  profileDetails?.country_id == 106
                    ? 'chevron-down'
                    : null
                }
              />
            </CardSection>
            <FooterButton
              container={false}
              onPress={() => this.updateProfile()}>
              Simpan Perubahan
            </FooterButton>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
  },
  viewStyle: {
    marginleft: 5,
    marginRight: 5,
    paddingLeft: 10,
    paddingRight: 10,
    marginVertical: 15,
  },
  btnWrapper: {
    paddingBottom: 15,
  },
  topTextWrapper: {
    flexDirection: 'column',
    paddingTop: 10,
    paddingBottom: 5,
  },
  topText: {
    flexDirection: 'row',
    marginBottom: 8,
    alignItems: 'center',
  },
  LftText: {
    width: wp('23%'),
    color: '#545454',
    fontSize: wp('4%'),
    fontFamily: Font.RobotoRegular,
  },
  rgtxt: {
    color: '#545454',
    fontFamily: Font.RobotoRegular,
    fontSize: wp('4.5%'),
  },

  dropdownClass: {
    // elevation: 1,
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    height: hp('8%'),
  },
  pickerClass: {
    height: hp('8%'),
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
};

const mapStateToProps = (state) => {
  return {
    username: state.profile.username,
    email: state.profile.user_email,
    name: state.profile.name,
    phone: state.profile.phone,
    city: state.profile.city,
    countries: state.country.countries,
    selectedCountry: state.country.selectedCountry,
    selectedProvince: state.country.province.selectedProvince,
    selectedCity: state.country.city.selectedCity,
    states: state.country.states,
    status: state.profile.status,
    profileDetails: state.profile.profile_details,
    updated: state.profile.updated,
    loading: state.profile.loading,
  };
};

export default connect(mapStateToProps, {
  getProfileDetails,
  updateProfile,
  changePersonalInfoPhone,
  changePersonalInfoCity,
  getStates,
  changePersonalInfoName,
  getCountries,
  setSelectedCountry,
  setComponentRedirect,
  setSelectedProvince,
  setSelectedStates,
  setSelectedCity,
})(PersonalInfo);
