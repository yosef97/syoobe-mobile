import React from 'react';
import {Image, View, StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const noRecords = () => (
    <View style={styles.view}>
        <Image style={styles.noImage} source={require('../images/norecord.png')}/>
    </View>
)

const styles = StyleSheet.create({
    noImage: {
        width:wp('50%'),
        height:hp('50%'),
        resizeMode: 'contain',
    },
    view:{
        width:'100%',
        height:hp('80%'),
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft:'auto',
        marginRight:'auto',
        alignSelf:'auto',
    }
})

export default noRecords;