import React, {Component} from 'react';
import {
  TouchableOpacity,
  AsyncStorage,
  View,
  FlatList,
  Text,
  Image,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {Spinner} from './common';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import axios from 'axios';
import {showToast} from '../helpers/toastMessage';
import {DisputeManagementFilter} from './common/product/DisputeManagementFilter';
import PTRView from 'react-native-pull-to-refresh';
import Clipboard from '@react-native-community/clipboard';
import {Button, Text as TextNativeBase} from 'native-base';

class DisputeManagementList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seg: 1,
      _token: null,
      loading: false,
      return_requests: [],
      cancellation_requests: [],
      noRecordsReturn: false,
      noRecordsCancel: false,
      selectedReturnReq: 'all',
      selectedReturnCancel: 'all',
      dropArray: [
        {name: 'Lihat semua', value: 'all'},
        {name: 'Kirim', value: 'sent'},
        {name: 'Diterima', value: 'received'},
      ],
      data: [],
      filter: 'all',
      noRecords: false,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = async () => {
    await this.retrieveToken()
      .then((_token) => {
        this.callRequests(this.props.url, this.state.filter);
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  callRequests = (url, option) => {
    this.setState({
      loading: true,
      noRecords: false,
    });

    var details = {
      _token: this.state._token,
      sts: option,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios.post('https://syoobe.co.id/api/' + url, formBody).then((response) => {
      console.log('callRequests', JSON.stringify(response));
      this.setState({
        loading: false,
      });
      if (response.data.status == 1) {
        if (Array.isArray(response.data.data)) {
          this.setState({
            data: response.data.data,
          });
          if (response.data.total_record == 1) {
            showToast({
              message: response.data.total_record + ' catatan ditemukan',
            });
          } else {
            showToast({
              message: response.data.total_record + ' catatan ditemukan',
            });
          }
        } else {
          this.setState({
            noRecords: true,
          });
        }
      } else {
        this.setState({
          noRecords: true,
        });
        showToast({
          message: 'Tidak ada catatan yang ditemukan',
        });
      }
    });
  };

  gotoView = (item) => {
    // console.log('ini return rq', JSON.stringify(item));
    if (this.props.type == 'Pengembalian') {
      this.props.navigation.navigate('DisputeManagementView', {
        _token: this.state._token,
        type: this.props.type,
        return_request_id: item.original_refund_id,
        url: 'disputeManagementReturnRequestView',
      });
    } else {
      this.props.navigation.navigate('DisputeManagementView', {
        _token: this.state._token,
        type: this.props.type,
        return_request_id: item.data.cancellation_request_id,
        url: 'disputeManagementCancelRequestView',
      });
    }
  };

  handleCopy = (text) => {
    // alert(text);
    Clipboard.setString(text);
    Alert.alert('Berhasil disalin ke clipboard');
  };

  render() {
    // console.log('ini states', JSON.stringify(this.state));
    // console.log('Ini props ', JSON.stringify(this.props));

    if (this.state.loading) {
      return (
        <View
          style={{
            paddingTop: hp('35%'),
          }}>
          <Spinner color="red" size="large" />
        </View>
      );
    }
    return (
      <View style={{flex: 1, backgroundColor: '#f5f6fa'}}>
        <PTRView
          onRefresh={() =>
            this.callRequests(this.props.url, this.state.filter)
          }>
          <DisputeManagementFilter
            items={this.state.dropArray}
            value={this.state.filter}
            onPress={({val, ref}) => {
              this.callRequests(this.props.url, val);
              this.setState({filter: val});
              ref.close();
            }}
            title={'Filter'}
          />
          <FlatList
            data={this.state.data}
            renderItem={({item, index}) => (
              <View style={{marginBottom: 20}}>
                <TouchableOpacity
                  key={index}
                  activeOpacity={0.5}
                  onPress={() => this.gotoView(item)}>
                  <View style={styles.cardContainer}>
                    <View style={{flexDirection: 'row', paddingTop: hp(1)}}>
                      <View style={styles.image}>
                        <Image
                          style={{width: '100%', height: '100%'}}
                          source={{uri: item.image}}
                        />
                      </View>
                      <View style={{flex: 1, paddingLeft: wp(3)}}>
                        <Text style={styles.textTitle}>
                          {item.product_name}
                        </Text>
                        <View style={styles.textContainerPrice}>
                          <Text
                            style={[
                              styles.textPrice,
                              {textTransform: 'uppercase'},
                            ]}>
                            {item.comments}
                          </Text>
                        </View>
                      </View>
                    </View>
                    <View style={styles.cardItems}>
                      <Text
                        style={{
                          ...styles.textPrice,
                          fontWeight: 'normal',
                          color: '#000',
                        }}>
                        Tanggal {this.props.type}
                      </Text>
                      <Text
                        style={{
                          ...styles.textPrice,
                          fontWeight: 'normal',
                          color: '#000',
                        }}>
                        {item.date}
                      </Text>
                    </View>
                    <View style={styles.cardItems}>
                      <Text style={{...styles.textPrice, color: '#000'}}>
                        No Faktur
                      </Text>
                      <Text
                        onPress={() => this.handleCopy(item.invoice_number)}
                        style={
                          styles.textPrice
                        }>{`${item.invoice_number}`}</Text>
                    </View>
                    <View style={styles.cardItems}>
                      <Text style={{...styles.textPrice, color: '#000'}}>
                        Status
                      </Text>
                      <Text style={styles.textPrice}>{item.status}</Text>
                    </View>
                    {this.props.type === 'Pengembalian' && (
                      <View style={styles.cardButton}>
                        <View style={styles.cardAction}>
                          <Text
                            style={{
                              ...styles.textPrice,
                              color: '#000',
                              fontWeight: 'bold',
                            }}>
                            TINDAKAN
                          </Text>
                        </View>
                        <View style={styles.cardActionButton}>
                          <Button
                            full
                            small
                            danger
                            onPress={() => this.gotoView(item)}>
                            <TextNativeBase>Lihat Permintaan</TextNativeBase>
                          </Button>
                        </View>
                      </View>
                    )}
                  </View>
                </TouchableOpacity>
                {this.props.type !== 'Pengembalian' && (
                  <View style={styles.cardButton}>
                    <View style={styles.cardAction}>
                      <Text
                        style={{
                          ...styles.textPrice,
                          color: '#000',
                          fontWeight: 'bold',
                        }}>
                        TINDAKAN
                      </Text>
                    </View>
                    <View style={styles.cardActionButton}>
                      <Button
                        small
                        primary
                        onPress={() =>
                          this.props.navigation.navigate('CancelForm', {
                            headerName: 'MENYETUJUI PEMBATALAN',
                            cancel_status: 3,
                            order_product_id:
                              item.data &&
                              parseInt(item.data.cancellation_request_order),
                            type: 'CancelRequestAccept',
                          })
                        }>
                        <TextNativeBase>Terima</TextNativeBase>
                      </Button>

                      <Button
                        small
                        danger
                        onPress={() =>
                          this.props.navigation.navigate('CancelForm', {
                            headerName: 'TOLAK PEMBATALAN',
                            cancel_status: 2,
                            order_product_id:
                              item.data &&
                              parseInt(item.data.cancellation_request_order),
                            type: 'CancelRequestDecline',
                          })
                        }>
                        <TextNativeBase>Tolak</TextNativeBase>
                      </Button>
                    </View>
                  </View>
                )}
              </View>
            )}
          />
        </PTRView>
      </View>
    );
  }
}

const styles = {
  cardContainer: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
  },
  cardButton: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginBottom: hp(0.1),
  },
  image: {
    width: wp(18),
    height: hp(9),
    maxHeight: hp(9),
    maxWidth: wp(18),
    backgroundColor: '#eaeaea',
  },
  textTitle: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#353b48',
  },
  textContainerPrice: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textPrice: {
    fontSize: hp(2),
    fontWeight: 'normal',
    color: '#e84118',
  },
  textPriceQty: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#353b48',
  },
  cardItems: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: hp(1),
    borderTopColor: '#f5f6fa',
    borderTopWidth: 1,
    marginTop: hp(1),
  },
  cardAction: {
    alignItems: 'center',
    paddingTop: hp(1),
    borderTopColor: '#f5f6fa',
    borderTopWidth: 1,
    marginTop: hp(1),
    justifyContent: 'center',
  },
  cardActionButton: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: hp(1),
    paddingBottom: hp(2),
  },
};

export default connect()(DisputeManagementList);
