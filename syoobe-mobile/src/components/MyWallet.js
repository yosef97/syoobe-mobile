import React, {Component} from 'react';
import {
  AsyncStorage,
  StatusBar,
  Text,
  Dimensions,
  StyleSheet,
  Image,
  View,
  FlatList,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Spinner} from './common';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {getWalletDetails, getRewardPoint} from '../actions';
// import FlashMessage from 'react-native-flash-message';
import {formatRupiah} from '../helpers/helper';
import Feather from 'react-native-vector-icons/Feather';
import PTRView from 'react-native-pull-to-refresh';

class MyWallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: Dimensions.get('window'),
      page: 1,
      _token: null,
    };
  }

  static navigationOptions = {
    drawerLabel: 'Saldo Saya',
    drawerIcon: () => (
      <Image
        source={require('../images/mywallet-icon.png')}
        style={{width: wp('4.4%'), height: hp('1.8%')}}
      />
    ),
  };
  getOrientation() {
    if (this.state.screen.width > this.state.screen.height) {
      return 'LANDSCAPE';
    } else {
      return 'PORTRAIT';
    }
  }

  getStyle() {
    if (this.getOrientation() === 'LANDSCAPE') {
      return landscapeStyles;
    } else {
      return portraitStyles;
    }
  }

  onLayout() {
    this.setState({screen: Dimensions.get('window')});
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount() {
    this.retrieveToken()
      .then((_token) => {
        var details = {
          _token: _token,
        };
        this.props.getWalletDetails(details);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getWalletDetails = () => {
    var details = {
      _token: this.state._token,
    };
    this.props.getWalletDetails(details);
  };

  isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 50
    );
  };

  render() {
    console.log('ini props', JSON.stringify(this.props.balance));
    // console.log('ini state', JSON.stringify(this.state));

    const {_token} = this.state;

    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color="red" size="large" />
        </View>
      );
    }

    return (
      <View style={{flex: 1}}>
        <PTRView onRefresh={() => this.getWalletDetails()}>
          <View
            style={{
              height: hp(20),
              width: '100%',
              backgroundColor: 'red',
              borderBottomLeftRadius: hp(5),
              borderBottomRightRadius: hp(5),
            }}>
            <View
              style={{
                width: wp(80),
                // height: hp(5),
                backgroundColor: '#fff',
                position: 'absolute',
                bottom: -hp(3),
                left: wp(10),
                right: wp(10),
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignContent: 'center',
                borderRadius: hp(2),
                // paddingVertical: hp(1),
              }}>
              {this.props.balance >= 5 ? (
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('WithDrawFunds', {
                      _token: _token,
                      balance: this.props.balance,
                    })
                  }>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: wp(2),
                    }}>
                    <Feather name={'arrow-down-circle'} size={hp(3)} />
                    <Text>Penarikan</Text>
                  </View>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() =>
                    Alert.alert('Saldo Anda harus lebih besar dari Rp 5.')
                  }>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      padding: wp(2),
                    }}>
                    <Feather name={'arrow-down-circle'} size={hp(3)} />
                    <Text>Penarikan</Text>
                  </View>
                </TouchableOpacity>
              )}

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('AddFunds', {
                    checked: 'wallet',
                  })
                }>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: wp(2),
                  }}>
                  <Feather name={'plus-circle'} size={hp(3)} />
                  <Text>Top Up</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{padding: wp(3)}}>
              <Text style={{color: '#fff', fontSize: 16, paddingTop: hp(2)}}>
                Saldo Tersedia
              </Text>
              <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                <Text
                  style={{color: '#fff', fontSize: hp(2), paddingTop: hp(1)}}>
                  Rp
                </Text>
                <Text style={{color: '#fff', fontSize: hp(5)}}>
                  {' '}
                  {formatRupiah(parseInt(this.props.balance))}
                </Text>
              </View>
            </View>
          </View>
          <View style={{flex: 1, paddingHorizontal: wp(3), marginTop: hp(5)}}>
            <Text
              style={{fontWeight: 'bold', fontSize: 14, paddingBottom: hp(2)}}>
              Transaksi Terbaru
            </Text>
            <FlatList
              data={this.props.transactions}
              showsVerticalScrollIndicator={false}
              scrollEnabled
              renderItem={({item, index}) => (
                <View
                  key={index}
                  style={{
                    backgroundColor: '#fff',
                    padding: wp(3),
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    borderRadius: 5,
                    marginBottom: hp(2),
                  }}>
                  <View style={{width: wp(60)}}>
                    <Text style={{fontWeight: 'bold'}}>{item.description}</Text>
                    <Text>{item.date}</Text>
                  </View>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 16,
                      color: item.creadit > 0 ? 'green' : 'red',
                    }}>
                    {' '}
                    Rp.{' '}
                    {item.creadit > 0
                      ? `+${formatRupiah(parseInt(item.creadit))}`
                      : `-${formatRupiah(parseInt(item.debit))}`}
                  </Text>
                </View>
              )}
            />
          </View>
        </PTRView>
      </View>
    );
  }
}

const portraitStyles = StyleSheet.create({
  bottomWrapper: {
    height: hp('4.5%'),
    backgroundColor: 'green',
  },
});
const landscapeStyles = StyleSheet.create({
  bottomWrapper: {
    height: hp('13%'),
    backgroundColor: 'blue',
  },
});

const mapStateToProps = (state) => {
  return {
    transactions: state.wallet.transactions,
    balance: state.wallet.balance,
    symbol: state.wallet.symbol,
    loading: state.wallet.loading,
    total_pages: state.wallet.total_pages,
    scrollLoading: state.wallet.scrollLoading,
    endOfRecords: state.wallet.endOfRecords,
    no_transactions: state.wallet.no_transactions,
  };
};

export default connect(mapStateToProps, {getWalletDetails, getRewardPoint})(
  MyWallet,
);
