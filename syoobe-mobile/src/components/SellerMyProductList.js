// import {changeProductStatus, deleteSellerProduct} from '../actions';
import React, {Component} from 'react';
import {StatusBar, View} from 'react-native';
import {ProductTabNavigator} from './routes/ProductTab';

class SellerMyProductList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    // console.log(this.props);
    return (
      <View style={styles.containerStyle}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <View style={{flex: 1}}>
          <ProductTabNavigator test={'ok'} />
        </View>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    padding: 0,
    flex: 1,
    backgroundColor: '#fff',
  },
};

export default SellerMyProductList;
