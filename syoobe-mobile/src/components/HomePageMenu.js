import React from 'react';
import {Image} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default class HomePageMenu extends React.Component {
  render() {
    return (
      <Image
        source={require('../images/menuIcon.png')}
        style={{
          width: wp('6.2%'),
          height: wp('5.2%'),
          marginLeft: 'auto',
          marginRight: 'auto',
          marginLeft: wp('2.5%'),
        }}
      />
    );
  }
}
