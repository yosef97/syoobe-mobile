import React, {Component} from 'react';
import {Text, Image, TouchableOpacity, View} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from '../Font';
import {ProductRightMenu} from './ProductRightMenu';

export default class MyProductComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {showComponmentB: false};
  }

  _toggleShow = () => {
    this.setState({showComponmentB: !this.state.showComponmentB});
  };

  render() {
    const {
      boxImg,
      available,
      price,
      availableWrapper,
      productName,
      rgtMenuIcon,
      imgContentWrapper,
      count,
      imgWrapper,
    } = styles;
    return (
      <View
        style={this.state.showComponmentB ? styles.boxStyle2 : styles.boxStyle}>
        {this.state.showComponmentB && <ProductRightMenu />}
        <View style={imgContentWrapper}>
          <View style={imgWrapper}>
            <Image style={boxImg} source={require('../../images/w1.png')} />
          </View>
          <View style={{flex: 1}}>
            <Text style={productName}>
              Reebok Original Blue Sporty Watch {this.props.heading}{' '}
            </Text>
            <View style={availableWrapper}>
              <Text style={available}>
                Available: <Text style={count}>1</Text>
              </Text>
              <Text style={available}>
                Sold: <Text style={count}>0</Text>
              </Text>
            </View>
            <Text style={price}>$1200.00 </Text>
          </View>
        </View>

        <TouchableOpacity onPress={this._toggleShow}>
          <Image
            style={rgtMenuIcon}
            source={require('../../images/rgtMenuIcon.png')}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  boxStyle: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    flexDirection: 'row',
    flexWarp: 'warp',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
  },
  boxStyle2: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    flexDirection: 'row',
    // flexWarp:'warp',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
    zIndex: 1,
  },
  boxImg: {
    width: wp('15%'),
    height: wp('22%'),
  },
  imgWrapper: {
    width: wp('15%'),
    marginRight: wp('3%'),
  },
  available: {
    color: '#858585',
    fontSize: wp('4.2%'),
    marginRight: wp('10%'),
    fontFamily: Font.RobotoMedium,
  },
  price: {
    color: '#00b3ff',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoBold,
  },
  availableWrapper: {
    flexDirection: 'row',
    paddingTop: hp('0.4%'),
    paddingBottom: hp('0.4%'),
  },
  rgtMenuIcon: {
    width: wp('1.6%'),
    height: wp('7.5%'),
  },
  imgContentWrapper: {
    flexDirection: 'row',
    // flexBasis:'80%',
    flex: 1,
    paddingRight: 15,
  },
  productName: {
    fontSize: wp('4.5%'),
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },
  count: {
    color: '#00b3ff',
  },
};
