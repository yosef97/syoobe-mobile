import React from 'react';
import {Text, View, Image} from 'react-native';
import {ProductButton} from '../product';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const ProductFeatureList = (props) => {
  const {
    product_img,
    viewStyle,
    product_text,
    product_price,
    buy_btn,
    like_icon,
    watchIcon,
  } = styles;
  return (
    <View style={viewStyle}>
      <Image style={product_img} source={require('../../images/w1.png')} />
      <Text style={product_text}>Reebok Original Blue Sporty Watch</Text>
      <Text style={product_price}>$500.000</Text>
      <ProductButton>
        <Text style={buy_btn}>Buy Now</Text>
        {/* <Image style={ buy_btn}  source={require('../../images/buy_now.png')}/> */}
      </ProductButton>
      <Image style={like_icon} source={require('../../images/like_icon.png')} />
    </View>
  );
};

const styles = {
  viewStyle: {
    // backgroundColor:'#f00',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    width: wp('50%'),
    bottom: 0,
    right: 0,
    color: 'ffffff',
    // flex:1,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    borderRightWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
  },
  product_img: {
    // width:65,
    // height:95,
    width: wp('20%'),
    height: wp('30%'),
  },

  product_text: {
    color: '#545454',
    // fontSize:16  ,
    fontSize: wp('3%'),
    width: '100%',
    textAlign: 'center',
    marginTop: wp('2%'),
    // marginBottom:15,
    marginBottom: wp('2%'),
  },
  product_price: {
    color: '#00b3ff',
    // fontSize:18,
    fontWeight: 'bold',
    marginBottom: wp('2%'),
    fontSize: wp('2.5%'),
  },
  like_icon: {
    position: 'absolute',
    top: 12,
    right: 12,
    // width:26,
    // height:24,
    width: wp('5%'),
    height: wp('4.5%'),
  },
  buy_btn: {
    // width:90,
    // height:26.5,
    width: wp('22%'),
    height: wp('6.5%'),
    borderColor: '#c7c7c7',
    borderWidth: 1,
    borderRadius: 50,
    textAlign: 'center',
    color: '#c90305',
    fontSize: wp('3%'),
    lineHeight: wp('6%'),
    // width:wp('30%'),
    // height:wp('10%')
  },
};

export {ProductFeatureList};
