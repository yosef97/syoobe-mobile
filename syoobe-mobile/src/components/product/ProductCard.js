import React from 'react';
import {View} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ProductCard = (props) => {
  return <View style={styles.containerStyle}>{props.children}</View>;
};

const styles = {
  containerStyle: {
    // borderWidth: 1,
    // borderRadius : 2,
    // borderColor: '#ddd',
    // borderBottomWidth: 0,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 2},
    // shadowRadius: 2,
    // shadowOpacity: 0.1,
    // elevation: 1,
    // marginleft: 5,
    // marginRight: 5,
    // marginTop: 10,
    height: '100%',
    backgroundColor: '#fff',
    // paddingLeft:10,
    // paddingRight:10
    // marginHorizontal: 10
    // marginBottom: hp(20)
  },
};

export {ProductCard};
