import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from '../Font';

const ProductNewArrival = (props) => {
  const {viewStyle, viewAll, newArrival} = styles;
  if (props.title != undefined) {
    return (
      <View style={viewStyle}>
        <Text style={newArrival}>{props.title.toUpperCase()}</Text>
        {/* onPress={() =>  props.navigation.navigate('ProductList')} */}
        {/* <TouchableOpacity>
                    <Text style={viewAll}>View All</Text>
                </TouchableOpacity> */}
      </View>
    );
  } else {
    return (
      <View style={viewStyle}>
        <Text style={newArrival}>
          {props.title || 'Terakhir Di Lihat'.toUpperCase()}
        </Text>
      </View>
    );
  }
};

const styles = {
  viewStyle: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
  },
  newArrival: {
    paddingLeft: wp(2),
    fontSize: hp(2.3),
    fontWeight: 'bold',
    color: 'grey',
  },
  viewAll: {
    color: '#c90305',
    // fontSize:16,
    fontSize: wp('3%'),
    fontFamily: Font.RobotoRegular,
  },
};

// const styles = {
//     viewStyle:{
//         marginBottom:2,
//         justifyContent: 'space-between',
//         flexDirection:'row',
//         paddingTop:20,
//         paddingBottom:10
//     },
//     newArrival:{
//         color:'#696969',
//         // fontSize:20,
//         fontSize:hp('2.1%'),
//         textTransform: 'uppercase'
//     },
//     viewAll:{
//         color:'#c90305',
//         // fontSize:16,
//         fontSize:hp('2.0%'),
//     }

// }

export {ProductNewArrival};
