import React from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AddToCart from '../AddToCart';

const ProductListFeature = (props) => {
  const {
    product_img,
    viewStyle,
    product_text,
    product_price_Style,
    buy_btn,
    like_icon,
  } = styles;
  const {
    product_id,
    product_image,
    product_name,
    product_price,
    price_currency,
  } = props.details;

  const renderLessText = () => {
    if (product_name.length > 17) {
      return (
        <Text style={product_text}>{product_name.substring(0, 17)} ...</Text>
      );
    } else {
      return <Text style={product_text}>{product_name}</Text>;
    }
  };

  return (
    <View style={viewStyle}>
      <TouchableOpacity
        onPress={() =>
          props.navigation.push('ProductDetail', {
            product_id: product_id,
          })
        }
        style={product_img}>
        <Image style={product_img} source={{uri: product_image}} />
      </TouchableOpacity>
      <TouchableWithoutFeedback
        onPress={() =>
          props.navigation.push('ProductDetail', {
            product_id: product_id,
          })
        }>
        {renderLessText()}
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback
        onPress={() =>
          props.navigation.push('ProductDetail', {
            product_id: product_id,
          })
        }>
        <Text style={product_price_Style}>
          {price_currency}
          {product_price}
        </Text>
      </TouchableWithoutFeedback>
      <AddToCart {...props}>
        {props.details.prod_requires_shipping === 1 ? (
          <Text style={buy_btn}>Add to Cart</Text>
        ) : (
          <Text style={buy_btn}>Buy Now</Text>
        )}
      </AddToCart>
      <Image style={like_icon} source={require('../../images/like_icon.png')} />
    </View>
  );
};

const styles = {
  viewStyle: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    // width:wp('48%'),
    width: '50%',
    bottom: 0,
    right: 0,
    color: 'ffffff',
    // flex:1,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
  },
  product_img: {
    // width:65,
    // height:95,
    width: wp('20%'),
    height: wp('30%'),
  },

  product_text: {
    color: '#545454',
    // fontSize:16  ,
    fontSize: wp('3%'),
    width: '100%',
    textAlign: 'center',
    marginTop: wp('2%'),
    // marginBottom:15,
    marginBottom: wp('2%'),
  },
  product_price_Style: {
    color: '#00b3ff',
    // fontSize:18,
    fontWeight: 'bold',
    marginBottom: wp('2%'),
    fontSize: wp('2.5%'),
  },
  like_icon: {
    position: 'absolute',
    top: 12,
    right: 12,
    // width:26,
    // height:24,
    width: wp('5%'),
    height: wp('4.5%'),
  },
  buy_btn: {
    // width:90,
    // height:26.5,
    width: wp('22%'),
    height: wp('6.5%'),
    borderColor: '#c7c7c7',
    borderWidth: 1,
    borderRadius: 50,
    textAlign: 'center',
    color: '#c90305',
    fontSize: wp('3%'),
    lineHeight: wp('6%'),
    // width:wp('30%'),
    // height:wp('10%')
  },
};

export {ProductListFeature};
