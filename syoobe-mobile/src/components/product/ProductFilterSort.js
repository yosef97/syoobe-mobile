import React from 'react';
import {Text, View, Image} from 'react-native';
import {Font} from '../Font';

const ProductFilterSort = () => {
  const {viewStyle, textStyle, filter_icon, wrapperStyle, sort_icon} = styles;

  return (
    <View style={viewStyle}>
      <View style={wrapperStyle}>
        <Image
          style={filter_icon}
          source={require('../../images/filterIcon.png')}
        />
        <Text style={textStyle}> Filters </Text>
      </View>
      <View style={wrapperStyle}>
        <Image
          style={sort_icon}
          source={require('../../images/sortIcon.png')}
        />
        <Text style={textStyle}>Sort By</Text>
      </View>
    </View>
  );
};

const styles = {
  viewStyle: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#e6e6e6',
  },
  textStyle: {
    color: '#6b6b6b',
    fontSize: 20,
    fontFamily: Font.RobotoRegular,
  },
  sort_icon: {
    marginRight: 8,
    width: 30,
    height: 15,
  },
  filter_icon: {
    marginRight: 8,
    width: 22,
    height: 20,
  },
  wrapperStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
};

export {ProductFilterSort};
