import {formatRupiah, substr} from '../../helpers/helper';
import React, {createRef} from 'react';
import {Image, View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RBSheet from 'react-native-raw-bottom-sheet';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

export const ProductCardSeller = (props) => {
  const {item, onPress} = props;
  console.log('ini item', JSON.stringify(item));
  return (
    <TouchableOpacity
      onPress={() => props.onPress(item.product_id, item.product_name)}
      activeOpacity={0.7}>
      <View style={styles.cardContent}>
        <View style={styles.cardImage}>
          <Image
            source={{uri: item.product_image}}
            style={{width: '100%', height: '100%', borderRadius: 5}}
          />
        </View>
        <View style={{flex: 1, paddingLeft: wp(2)}}>
          <Text style={styles.textTitle}>{substr(item.product_name, 50)}</Text>
          <Text style={styles.textPrice}>
            {`Rp. ${formatRupiah(item.product_price)}`}
          </Text>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            {item.product_type !== 'digital' && (
              <View>
                <Text style={{fontSize: 12}}>Stok</Text>
                <Text style={{fontSize: 11}}>{item.product_available}</Text>
              </View>
            )}

            {item.product_type !== 'digital' ? (
              <View style={{paddingLeft: 10}}>
                <Text style={{fontSize: 12}}>Terjual</Text>
                <Text style={{fontSize: 11}}>{item.product_sold}</Text>
              </View>
            ) : (
              <View>
                <Text style={{fontSize: 12}}>Terjual</Text>
                <Text style={{fontSize: 11}}>{item.product_sold}</Text>
              </View>
            )}

            <ActionMenu {...props} />
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const ActionMenu = (props) => {
  const sheetRef = createRef();
  return (
    <>
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        }}>
        <MaterialCommunityIcons
          name={'dots-vertical'}
          size={hp(3.5)}
          color={'#000'}
          onPress={() => sheetRef.current.open()}
        />
      </View>

      <RBSheet
        ref={sheetRef}
        closeOnPressBack
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={hp(40)}
        customStyles={card}>
        <View style={{flex: 1}}>
          <View style={styles.headerContainer}>
            <TouchableOpacity
              style={styles.headerContent}
              activeOpacity={0.5}
              onPress={() => sheetRef.current.close()}>
              <View>
                <FontAwesome5Icon name={'chevron-left'} size={hp(2.6)} />
              </View>
            </TouchableOpacity>
            <Text style={styles.headerContainerTitle}>{'Pilihan'}</Text>
          </View>
          <View style={styles.itemListContainer}>
            {/* <CardItemList
              onPress={() =>
                props.onPressEdit(props.item.product_id, sheetRef.current)
              }
              title={'Edit'}
              icon={'edit'}
            /> */}
            {!props.isPause && (
              <CardItemList
                onPress={() =>
                  props.onPressPause(props.item.product_id, sheetRef.current)
                }
                title={'Pause'}
                icon={'pause'}
              />
            )}
            {props.isPause && (
              <CardItemList
                onPress={() =>
                  props.onPressPublish(props.item.product_id, sheetRef.current)
                }
                title={'Publish'}
                icon={'arrow-up'}
              />
            )}

            <CardItemList
              onPress={() =>
                props.onPressDelete(
                  props.item.product_id,
                  props.item.product_name,
                  sheetRef.current,
                )
              }
              title={'Hapus'}
              icon={'times'}
            />
          </View>
        </View>
      </RBSheet>
    </>
  );
};

const CardItemList = ({style, title, onPress, icon}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.cardItemList, style]}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <FontAwesome5Icon name={icon} size={hp(2)} />
            <Text
              style={{fontSize: hp(2), fontWeight: 'bold', paddingLeft: 10}}>
              {title}
            </Text>
          </View>
          <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const card = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  container: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: '#fff',
  },
};

const styles = StyleSheet.create({
  cardContent: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    padding: wp(2),
    backgroundColor: '#fff',
    borderRadius: 10,
    marginBottom: hp(1),
  },
  cardImage: {
    width: wp(25),
    height: hp(10),
    maxWidth: wp(25),
    maxHeight: hp(10),
    backgroundColor: '#8395a7',
    borderRadius: 5,
  },
  textTitle: {fontSize: hp(2.2), fontWeight: 'bold', color: '#000'},
  textPrice: {fontSize: hp(2.4), fontWeight: 'bold', color: '#e74c3c'},
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#f5f6fa',
  },
  headerContent: {
    position: 'absolute',
    left: wp(5),
    top: hp(2.4),
    bottom: hp(2),
  },
  headerContainerTitle: {
    fontSize: hp(2.4),
    fontWeight: 'bold',

    paddingVertical: hp(1),
  },

  cardItemList: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingVertical: hp(2),
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    borderRadius: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  itemListContainer: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: wp(3),
    // Bottom: hp(2),
  },
  itemList: {
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    marginTop: hp(1),
    // marginBottom: hp(1),
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
  },
  itemListText: {fontSize: hp(2.7), fontWeight: 'bold'},
});
