import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from '../Font';

const OrderViewRightComponent = (props) => {
  return (
    <View style={styles.menuWrapper}>
      <TouchableOpacity onPress={props.onDownload} style={styles.menuItem}>
        <Text style={styles.textStyle}>Download</Text>
      </TouchableOpacity>
      {props.disableRevision == 'N' && (
        <TouchableOpacity
          onPress={props.onSendRevisionRequest}
          style={styles.menuItem}>
          <Text style={styles.textStyle}>Send Revision Request</Text>
        </TouchableOpacity>
      )}
      {props.disableAccept == 'N' && (
        <TouchableOpacity
          onPress={props.onAcceptRequest}
          style={styles.menuItem}>
          <Text style={styles.textStyle}>Accept request</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = {
  menuWrapper: {
    width: wp('40%'),
    backgroundColor: '#fff',
    position: 'absolute',
    top: 0,
    zIndex: 1001,
    right: 30,
    borderRadius: 10,
    elevation: 5,
  },
  menuItem: {
    paddingHorizontal: 10,
    paddingVertical: 7,
    color: '#7d7d7d',
    borderWidth: 1,
    borderColor: '#e1e1e1',
  },
  textStyle: {
    color: '#7d7d7d',
    fontFamily: Font.RobotoRegular,
    fontSize: 14,
  },
};

export {OrderViewRightComponent};
