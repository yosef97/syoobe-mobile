import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

const ProductButton = (props) => {
  const {buttonStyle, textStyle} = styles;

  return (
    <TouchableOpacity onPress={props.onPress} style={buttonStyle}>
      {props.children}
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    backgroundColor: 'transparent',
    fontSize: 20,
    borderColor: '#c7c7c7',
    borderWidth: 1,
    borderRadius: 50,
  },
};

export {ProductButton};
