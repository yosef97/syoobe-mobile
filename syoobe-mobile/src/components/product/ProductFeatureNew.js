import React from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {ProductButton} from '../product';

const ProductFeatureNew = (props) => {
  const {
    product_img,
    viewStyle,
    product_text,
    product_price_Style,
    buy_btn,
    like_icon,
  } = styles;

  return (
    <View style={viewStyle}>
      <TouchableOpacity style={product_img}>
        <Image
          style={product_img}
          source={require('../../images/buy_now.png')}
        />
      </TouchableOpacity>
      <TouchableOpacity>
        <Text style={product_text}>New One</Text>
      </TouchableOpacity>
      <Text style={product_price_Style}>$500</Text>
      <ProductButton>
        <Image style={buy_btn} source={require('../../images/buy_now.png')} />
      </ProductButton>
      <Image style={like_icon} source={require('../../images/like_icon.png')} />
    </View>
  );
};

const styles = {
  viewStyle: {
    // backgroundColor:'#f00',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,
    width: '50%',
    bottom: 0,
    right: 0,
    color: 'ffffff',
    // flex:1,
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    borderRightWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
  },
  product_img: {
    width: 100,
    height: 150,
  },
  product_text: {
    color: '#545454',
    fontSize: 16,
    width: '100%',
    textAlign: 'center',
    marginTop: 15,
    marginBottom: 15,
  },
  product_price_Style: {
    color: '#00b3ff',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 15,
  },
  like_icon: {
    position: 'absolute',
    top: 8,
    right: 8,
    width: 30,
    height: 28,
  },
  buy_btn: {
    width: 123,
    height: 36,
  },
};

export {ProductFeatureNew};
