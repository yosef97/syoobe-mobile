import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from '../Font';

const MyOrdersRightMenu = (props) => {
  return (
    <View style={styles.menuWrapper}>
      <TouchableOpacity onPress={props.onSendFeedback} style={styles.menuItem}>
        <Text style={styles.textStyle}>Feedback</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={props.onSendMessage} style={styles.menuItem}>
        <Text style={styles.textStyle}>Chat</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={props.onRefund} style={styles.menuItem}>
        <Text style={styles.textStyle}>Refund</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={props.onCancel} style={styles.menuItem}>
        <Text style={styles.textStyle}>Cancel</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = {
  menuWrapper: {
    width: wp('40%'),
    backgroundColor: '#fff',
    position: 'absolute',
    top: 0,
    zIndex: 1001,
    right: 30,
    borderRadius: 10,
    elevation: 5,
  },
  menuItem: {
    paddingHorizontal: 10,
    paddingVertical: 7,
    color: '#7d7d7d',
    borderWidth: 1,
    borderColor: '#e1e1e1',
  },
  textStyle: {
    color: '#7d7d7d',
    fontFamily: Font.RobotoRegular,
    fontSize: 14,
  },
};

export {MyOrdersRightMenu};
