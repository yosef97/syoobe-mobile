import React from 'react';
import {View} from 'react-native';

const ProductCardSection = (props) => {
  return <View style={styles.containerStyles}>{props.children}</View>;
};

const styles = {
  containerStyles: {
    // justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
    // width:'100%',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    // marginBottom:15,
    // borderBottomWidth:1,
    borderColor: '#e7e7e7',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
  },
};

export {ProductCardSection};
