import React, { Component } from "react";
import {
  Image,
  StatusBar,
  Platform,
  LayoutAnimation,
  StyleSheet,
  View,
  Text,
  UIManager,
  TouchableOpacity,
  ScrollView
} from "react-native";
import Accordion_Panel from "../components/Accordion_Panel";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import { Font } from "./Font";
import { getCategoryList, expandCategory } from "../actions";
import { Spinner } from "./common";
import { showToast } from "../helpers/toastMessage";

class Category extends Component {
  AccordionData = [];
  globalIndex = null;

  constructor(props) {
    super(props);
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    this.props.getCategoryList();
    // const array = [];
    // this.props.categories.forEach(element => {
    //   array.push(element);
    // });

    // console.log(array);
    // const array = [
    //   { expanded: false, title: "Automotive and Parts", body: "Hello Guys this is the Animated Accordion Panel." },
    //   { expanded: false, title: "Cellphone & Tablets", body: "Hello Guys this is the Animated Accordion Panel." },
    //   { expanded: false, title: "Device Repair", body: "Hello Guys this is the Animated Accordion Panel." },
    //   { expanded: false, title: "Digital Marketing", body: "Hello Guys this is the Animated Accordion Panel." },
    //   { expanded: false, title: "Graphic & Logo Design", body: "Hello Guys this is the Animated Accordion Panel." },
    // ];

    // this.state = { AccordionData: [...this.props.categories] }
    this.AccordionData = [...this.props.categories];
  }

  update_Layout = async (index, category_id) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    const array = this.AccordionData.map(item => {
      const newItem = Object.assign({}, item);
      if (item.sub_category.length == 0 && category_id == item.category_id) {
        showToast({
          message: "Tidak ada sub-kategori yang hadir"
        });
      }
      newItem.expanded = false;
      return newItem;
    });
    if (this.globalIndex == index) {
      array[index].expanded = false;
      this.globalIndex = null;
    } else {
      this.globalIndex = index;
      array[index].expanded = true;
    }
    await this.props.expandCategory(array);
    // this.AccordionData = array
  };

  render() {
    this.AccordionData = [...this.props.categories];

    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color="red" size="large" />
        </View>
      );
    }

    return (
      <View style={styles.MainContainer}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <ScrollView contentContainerStyle={{ paddingVertical: 5 }}>
          <Text style={styles.shopCatHeading}>
            {" "}
            {"Shop by category".toUpperCase()}
          </Text>
          {this.AccordionData.map((item, index) => {
            return (
              // <Accordion_Panel key={item.category_id} item={item} />
              <Accordion_Panel
                {...this.props}
                key={item.category_id}
                onClickFunction={this.update_Layout.bind(
                  this,
                  index,
                  item.category_id
                )}
                item={item}
              />
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: "#fff",
    height: "100%"
  },
  shopCatHeading: {
    color: "#696969",
    fontSize: hp("2.5%"),
    paddingLeft: 5,
    marginBottom: 5,
    marginTop: 5,
    fontFamily: Font.RobotoMedium
  }
});

const mapStateToProps = state => {
  return {
    error: state.category.error,
    loading: state.category.loading,
    categories: state.category.categories
  };
};

export default connect(mapStateToProps, { getCategoryList, expandCategory })(
  Category
);
