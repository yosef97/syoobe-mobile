import React, {Component} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
  Platform,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Font} from './Font';
import {connect} from 'react-redux';

class Accordion_Panel extends Component {
  constructor() {
    super();
    this.state = {
      updated_Height: 0,
    };
  }
  UNSAFE_componentWillReceiveProps(update_Props) {
    if (update_Props.item.expanded) {
      this.setState(() => {
        return {
          updated_Height: null,
        };
      });
    } else {
      this.setState(() => {
        return {
          updated_Height: 0,
        };
      });
    }
  }

  shouldComponentUpdate(update_Props, nextState) {
    if (update_Props.item.expanded !== this.props.item.expanded) {
      return true;
    }
    return false;
  }
  render() {
    return (
      <View style={styles.Panel_Holder}>
        <View style={styles.Btn}>
          <Text
            onPress={() =>
              this.props.navigation.navigate('ProductsByCategory', {
                category_id: this.props.item.category_id,
              })
            }
            style={styles.Panel_Button_Text}>
            {this.props.item.category_name}
          </Text>
          <View style={styles.iconClass}>
            {this.props.item.expanded ? (
              <TouchableOpacity
                style={styles.touch}
                onPress={this.props.onClickFunction}>
                <Image
                  style={styles.iconImg}
                  source={require('../images/minusIcon.png')}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={styles.touch}
                onPress={this.props.onClickFunction}>
                <Image
                  style={styles.iconImg}
                  source={require('../images/plusIcon.png')}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
        <View
          style={{
            height: this.state.updated_Height,
            overflow: 'hidden',
            paddingLeft: 15,
          }}>
          {this.props.item.sub_category.map((sub_cat) => {
            return (
              <View key={sub_cat.sub_category_id} style={styles.linkWrapper}>
                {/* <Image style={styles.linkImg} source={require('../images/arrowIcon.png')}/> */}
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('ProductsByCategory', {
                      category_id: sub_cat.sub_category_id,
                    })
                  }>
                  <Text style={styles.linkTxt}> {sub_cat.name}</Text>
                </TouchableOpacity>
              </View>
            );
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
  },

  // Panel_text: {
  //   fontSize: 18,
  //   color: '#000',
  //   // padding: 10,
  //   // paddingTop:10,
  //   // paddingBottom:10
  // },

  Panel_Button_Text: {
    fontSize: hp('2.2%'),
    textAlign: 'left',
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },

  Panel_Holder: {
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    // marginVertical: 5
  },

  Btn: {
    // padding: 10,
    // backgroundColor: '#FF6F00',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: hp('2.2%'),
    paddingHorizontal: 10,
  },
  iconImg: {
    width: 20,
    height: 20,
  },

  touch: {
    width: 40,
    height: 25,
  },

  linkWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',

    marginBottom: 7,
    paddingVertical: hp('1%'),
  },
  linkTxt: {
    color: '#676767',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
  },
  linkImg: {
    width: 6,
    height: 11,
  },
});

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {})(Accordion_Panel);
