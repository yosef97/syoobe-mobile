import React, {Component} from 'react';
import {
  PermissionsAndroid,
  Alert,
  Button,
  Modal,
  TextInput,
  Picker,
  Image,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import {
  CheckBox,
  Input,
  Item,
  Content,
  Button as ButtonNative,
  Text,
} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {CardSection, Spinner, Button as ButtonRed} from './common';
import axios from 'axios';
// import CheckBoxComponent from './CheckBoxComponent';
import {OrderViewRightComponent} from './product/OrderViewRightComponent';
import RNFetchBlob from 'react-native-fetch-blob';
import DocumentPicker from 'react-native-document-picker';
import {
  ProductItemCard,
  Section,
} from './common/view-product/ProductDetailCard';
import InputText from './common/InputText';
import {ProductFileUploadProgress} from './common/product/ProductFileUploadProgress';
import {FileUploadList} from './SendYourRequirements';
import {formatRupiah, substr} from '../helpers/helper';
import {PopUpInfo} from './common/view-product/PopUpInfo';
import {ProductDinamicDropdown} from './common/product/ProductDinamicDropdown';
import Clipboard from '@react-native-community/clipboard';
import Bugsnag from '@bugsnag/react-native';
import PTRView from 'react-native-pull-to-refresh';

export default class SalesItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order_product_id: props.route.params.details.order_product_id,
      _token: props.route.params.details._token,
      salesData: null,
      loading: false,
      progressBar: false,
      trackNum: '',
      comment: '',
      statusSelected: 1,
      shipCompanySelected: 1,
      customer_notify: false,
      additionalOptions: null,
      show_status_modal: false,
      status_details_response: null,
      additional_options_selected: 0,
      insuranceAmt: '',
      weight: '',
      weight_class_selected: '',
      provider_name_selected: '',
      processing_time_selected: '',
      object_id_selected: '',
      carrier_account_selected: null,
      service_label_details: null,
      service_level_name_select: '',
      postageArray: null,
      rateSelected: '',
      showDownloadMenu: false,
      selectedDownloadIndex: null,
      uploadWidth: 0,
      selectedUploadIndex: 0,
      file_download: '',
      product_download: '',
      revision_time: 0,
      file_name_download: '',
      upload_sample_file: false,
      upload_sample_file_array: null,
      uploadProductionFileArray: [
        {
          filename: '',
          fileurl: '',
          file_extension_name: '',
          download_name: '',
          comments: '',
        },
      ],
      fileNameSample: '',
      fileNameProduction: '',

      uploadSampleFileArray: [
        {
          product_download_name_row: '',
          product_download_name_file_type: '',
          product_download_name: '',
          file_name: '',
          file_url: '',
          revision_time: '',
          revision_time_muf_id: '',
          revision_time_value: '',
          upload_sample_file_status: '',
        },
      ],
      progress: 0,
      indeterminate: true,
      loadingOnUpload: false,
    };
  }

  handleCopy = (text) => {
    // alert(text);
    Clipboard.setString(text);
    Alert.alert('Berhasil disalin ke clipboard');
  };

  componentDidMount = () => {
    this.getSalesViewData();
  };

  // componentDidUpdate = (prevProps, prevState) => {
  //   // console.log('Prevstate', prevState);
  //   if (prevState.progressBar === true) {
  //     this.animate();
  //   }
  // };

  getSalesViewData = (event) => {
    this.setState({
      loading: true,
    });
    let details = {
      _token: this.state._token,
      oder_product_id: this.state.order_product_id,
    };
    // console.log('ini detail', JSON.stringify(details));

    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/salesViewDisplay', formBody)
      .then((response) => {
        console.log('sales v', JSON.stringify(response.data));
        if (response.data.status === 1) {
          this.setState({
            additionalOptions:
              response.data.data.package_details &&
              response.data.data.package_details[0].additional_option,
            salesData: response.data.data,
            statusSelected: response.data.data.selected_order_status,
            loading: false,
            trackNum: response.data.data.trackingNumber,
            upload_sample_file: response.data.data.upload_sample_file,
            upload_sample_file_array:
              response.data.data.upload_sample_file_array,
          });

          if (
            response.data.data.is_upload_file_sample_next
              ?.upload_file_sample_next !== false
          ) {
            this.setState((state) => {
              const list = state.uploadSampleFileArray.map((item) => {
                return (item.revision_time_value =
                  response.data.data.is_upload_file_sample_next?.revision_time_value_next);
              });
              return {
                list,
              };
            });
          }
        }
      });
  };

  updateSalesView = () => {
    if (this.state.statusSelected === 5) {
      if (
        this.state.trackNum === '' ||
        this.state.trackNum === null ||
        this.state.shipCompanySelected === 1
      ) {
        Alert.alert('Harap lengkapi data anda');
      } else {
        this.setState({
          loading: true,
        });
        let details = {
          _token: this.state._token,
          oder_product_id: this.state.order_product_id,
          tracking_number: this.state.trackNum,
          shipping_company: this.state.shipCompanySelected,
          opr_status: this.state.statusSelected,
        };

        if (this.state.customer_notify) {
          details = {
            ...details,
            ...{
              customer_notified: 1,
            },
          };
        }
        if (String(this.state.comment).trim() !== '') {
          details = {
            ...details,
            ...{
              comments: this.state.comment,
            },
          };
        }
        var formBody = [];
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        axios
          .post('https://syoobe.co.id/api/submitSaleViewData', formBody)
          .then((response) => {
            console.log('update1', response);
            if (response.data.status === 1) {
              this.setState({
                loading: false,
              });
              Alert.alert('Informasi', response.data.data, [
                {
                  text: 'OK',
                  onPress: () => {
                    this.props.navigation.replace('HomeDrawer', {
                      screen: 'Sales',
                    });
                  },
                },
              ]);
            }
          });
      }
    } else {
      this.setState({
        loading: true,
      });
      let details = {
        _token: this.state._token,
        oder_product_id: this.state.order_product_id,
        opr_status: this.state.statusSelected,
      };
      if (this.state.customer_notify) {
        details = {
          ...details,
          ...{
            customer_notified: 1,
          },
        };
      }
      if (String(this.state.comment).trim() !== '') {
        details = {
          ...details,
          ...{
            comments: this.state.comment,
          },
        };
      }
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      axios
        .post('https://syoobe.co.id/api/submitSaleViewData', formBody)
        .then((response) => {
          console.log('update2', response);
          if (response.data.status === 1) {
            this.setState({
              loading: false,
            });
            Alert.alert('Informasi', response.data.msg, [
              {
                text: 'OK',
                onPress: () => {
                  this.props.navigation.replace('HomeDrawer', {
                    screen: 'Sales',
                  });
                },
              },
            ]);
          }
        });
    }
  };

  updateSalesViewProductionFiles = () => {
    // this.animate();
    this.state.uploadProductionFileArray.map((data) => {
      // this.state.file_name_download !== '' &&
      // this.state.revision_time !== '' &&
      // this.state.file_download.filename !== ''
      if (data.download_name !== '' && data.comments !== '') {
        this.setState({
          loading: true,
        });
        var formData = new FormData();
        formData.append('_token', this.state._token);

        formData.append('oder_product_id', this.state.order_product_id);

        formData.append(
          'production_file[0][fileurl]',
          this.state.file_download.url,
        );

        formData.append(
          'production_file[0][extension_name]',
          this.state.file_download.extension_name,
        );

        formData.append(
          'production_file[0][download_name]',
          data.download_name,
        );

        formData.append(
          'production_file[0][filename]',
          this.state.file_download.filename,
        );

        formData.append('production_file[0][comments]', data.comments);

        axios
          .post('https://syoobe.co.id/api/submitSaleViewData', formData)
          .then((response) => {
            console.log('update1', JSON.stringify(response));
            if (response.data.status === 1) {
              this.setState({
                loading: false,
              });
              Alert.alert('Informasi', response.data.data, [
                {
                  text: 'OK',
                  onPress: () => {
                    this.props.navigation.replace('HomeDrawer', {
                      screen: 'Sales',
                    });
                  },
                },
              ]);
            } else {
              Alert.alert('Tindakan gagal');
              this.setState({
                loading: false,
              });
            }
          })
          .catch((err) => {
            Bugsnag.notify(err);
            this.setState({
              loading: false,
            });
          });
      } else {
        Alert.alert('Mohon cek kembali persaratan anda.');
      }
    });
    // Sample button update
  };

  grantAccess = (file_id) => {
    const {_token, order_product_id} = this.state;
    this.setState({
      loading: true,
    });
    let details = {
      _token: _token,
      opr_id: order_product_id,
      file_id: file_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post(
        'https://syoobe.co.id/api/instantDigitalProductGrantAccess',
        formBody,
      )
      .then((response) => {
        console.log('sales v', response);
        if (response.data.status == 1) {
          this.setState({
            loading: false,
          });
          Alert.alert('Informasi', response.data.msg, [
            {text: 'OK', onPress: () => this.getSalesViewData()},
          ]);
        }
      });
  };

  revokeAccess = (file_id) => {
    const {_token, order_product_id} = this.state;
    this.setState({
      loading: true,
    });
    let details = {
      _token: _token,
      opr_id: order_product_id,
      file_id: file_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/instantDigitalProductRevoke', formBody)
      .then((response) => {
        console.log('sales v', response);
        if (response.data.status == 1) {
          this.setState({
            loading: false,
          });
          Alert.alert('Informasi', response.data.msg, [
            {text: 'OK', onPress: () => this.getSalesViewData()},
          ]);
        }
      });
  };

  // animate() {
  //   let progress = 0;
  //   this.setState({progress});
  //   setTimeout(() => {
  //     this.setState({indeterminate: false});
  //     setInterval(() => {
  //       progress += Math.random() / 5;
  //       if (progress > 1) {
  //         progress = 1;
  //       }
  //       this.setState({progress});
  //     }, 500);
  //   }, 1500);
  // }

  goToProduct = () => {
    const {product_id, product_type} = this.state.salesData;
    this.props.navigation.push('ProductDetail', {
      product_id: product_id,
      product_requires_shipping: product_type == 'P' ? 1 : 0,
    });
  };

  changeModalState = () => {
    this.setState((prevState, prevProps) => {
      return {
        show_status_modal: !prevState.show_status_modal,
      };
    });
  };

  comfirmSubmit = () => {
    if (this.state.weight == '') {
      Alert.alert('Weight is mandatory!');
    } else if (this.state.provider_name_selected == '') {
      Alert.alert('Provider name is mandatory!');
    } else if (this.state.service_level_name_select == '') {
      Alert.alert('Service Level name is mandatory!');
    } else {
      this.setState({
        loading: true,
      });
      let details = {
        _token: this.state._token,
        weight: this.state.weight,
        weight_class: this.state.weight_class_selected,
        provider_name: this.state.provider_name_selected,
        object_id: this.state.object_id_selected,
        carrier_account: this.state.carrier_account_selected,
        shipping_amount: this.state.rateSelected,
        processing_time: this.state.processing_time_selected,
        insurance_amount: this.state.insuranceAmt,
        additional_option: this.state.additional_options_selected,
        opr_id: this.state.order_product_id,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      axios
        .post('https://syoobe.co.id/api/postagePurchaseSubmit', formBody)
        .then((response) => {
          this.setState({
            loading: false,
          });
          console.log('submit', response);
          this.changeModalState();
          if (response.data.status == 1) {
            this.props.navigation.navigate('PdfViewer', {
              pdf: response.data.image_link,
              type: 'url',
            });
          } else {
            Alert.alertt('Error', response.data.data, [{text: 'OK'}]);
          }
        });
    }
  };

  setProviderState = (val, array) => {
    array.forEach((item) => {
      if (item.name == val) {
        this.setState({
          provider_name_selected: val,
          service_label_details: item.value,
        });
      }
    });
  };

  setServiceLevelState = (val) => {
    this.state.service_label_details.forEach((item) => {
      if (val == item.servicelevel_name) {
        this.setState({
          service_level_name_select: val,
          processing_time_selected: item.duration_terms,
          carrier_account_selected: item.carrier_account,
          object_id_selected: item.object_id,
          rateSelected: item.rate,
        });
      }
    });
  };

  downloadFile = async (data) => {
    try {
      this.setState({
        loading: true,
      });
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Download to storage Permission',
          message:
            'Syoobe App needs access to your storage ' +
            'so that you can download files and save them.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        let dirs = RNFetchBlob.fs.dirs.DownloadDir + '/' + data.file_name;
        RNFetchBlob.config({
          path: dirs,
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: false,
            path: dirs,
          },
        })
          .fetch(
            'GET',
            'https://www.syoobe.co.id/account/download_graphic_file/' +
              data.file_name,
          )
          .then((res) => {
            console.log('ini res', res);
            this.setState({
              loading: false,
            });
            Alert.alert('Berhasil mengunduh file');
          })
          .catch((error) => {
            Bugsnag.notify(error);
            console.log('ee', error);
          });
      } else {
        this.setState({
          loading: false,
        });
        Alert.alert('Permintaan ditolak');
        console.log('Storage permission denied');
      }
    } catch (err) {
      this.setState({
        loading: false,
      });
      Bugsnag.notify(err);
      console.warn(err);
    }
  };

  toggleDownloadButton = (index) => {
    if (index == this.state.selectedDownloadIndex) {
      this.setState({
        showDownloadMenu: !this.state.showDownloadMenu,
        selectedDownloadIndex: index,
      });
    } else {
      this.setState({
        showDownloadMenu: true,
        selectedDownloadIndex: index,
      });
    }
  };

  selectFileSample = async (index, type) => {
    this.setState({
      loadingOnUpload: true,
      indeterminate: false,
    });
    const file = await DocumentPicker.pick({
      type: [DocumentPicker.types.allFiles],
    });

    try {
      const formData = new FormData();
      formData.append('_token', this.state._token);
      formData.append('file', {
        name: file.name,
        uri: file.uri,
        type: file.type,
      });

      const config = {
        onUploadProgress: (progressEvent) => {
          let {progress} = this.state;
          progress = progressEvent.loaded / progressEvent.total;
          this.setState({progress});
        },
      };

      axios
        .post(
          'https://syoobe.co.id/api/instantDigitalFileUpload',
          formData,
          config,
        )
        .then((response) => {
          if (response.data.status === 1) {
            this.setState({
              file_download: response.data,
              fileNameSample: response.data.filename,
              loadingOnUpload: false,
            });
          }
        });
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
  };

  selectFileProduction = async (index, type) => {
    this.setState({
      loadingOnUpload: true,
      indeterminate: false,
    });
    const file = await DocumentPicker.pick({
      type: [DocumentPicker.types.allFiles],
    });

    try {
      const formData = new FormData();
      formData.append('_token', this.state._token);
      formData.append('file', {
        name: file.name,
        uri: file.uri,
        type: file.type,
      });

      const config = {
        onUploadProgress: (progressEvent) => {
          let {progress} = this.state;
          progress = progressEvent.loaded / progressEvent.total;
          this.setState({progress});
        },
      };

      axios
        .post(
          'https://syoobe.co.id/api/instantDigitalFileUpload',
          formData,
          config,
        )
        .then((response) => {
          console.log(
            'item sales data url',
            JSON.stringify(this.state.file_download),
          );
          if (response.data.status === 1) {
            this.setState({
              file_download: response.data,
              fileNameProduction: response.data.filename,
              loadingOnUpload: false,
            });
          } else {
            Alert.alert('Informasi', response.data.msg, [{text: 'OK'}]);
            this.setState({
              loadingOnUpload: false,
            });
          }
        });
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
  };

  updateSalesViewSampleFiles = () => {
    this.state.uploadSampleFileArray.map((data) => {
      // this.state.file_name_download !== '' &&
      // this.state.revision_time !== '' &&
      // this.state.file_download.filename !== ''
      if (
        data.product_download_name !== '' &&
        data.revision_time_value !== '' &&
        this.state.file_download.status === 1
      ) {
        this.setState({
          loading: true,
        });
        var formData = new FormData();
        formData.append('_token', this.state._token);

        formData.append('oder_product_id', this.state.order_product_id);

        formData.append(
          'product_download[0][extension_name]',
          this.state.file_download.extension_name,
        );

        formData.append(
          'product_download[0][fileurl]',
          this.state.file_download.url,
        );
        formData.append(
          'product_download[0][opr_id]',
          this.state.order_product_id,
        );

        formData.append(
          'product_download[0][filename]',
          this.state.file_download.filename,
        );

        formData.append(
          'product_download[0][download_name]',
          data.product_download_name,
        );

        formData.append(
          'product_download[0][revision_time]',
          parseInt(data.revision_time_value),
        );

        axios
          .post('https://syoobe.co.id/api/submitSaleViewData', formData)
          .then((response) => {
            console.log('update1', JSON.stringify(response));
            if (response.data.status === 1) {
              setTimeout(() => {
                this.setState({
                  loading: false,
                });
                Alert.alert('Informasi', response.data.data, [
                  {
                    text: 'OK',
                    onPress: () =>
                      this.props.navigation.replace('HomeDrawer', {
                        screen: 'Sales',
                      }),
                  },
                ]);
              }, 1400);
            } else {
              Alert.alert('Tindakan gagal');
              this.setState({
                loading: false,
              });
            }
          })
          .catch((err) => {
            Bugsnag.notify(err);
            console.log('e', err);
            this.setState({
              loading: false,
            });
          });
      } else {
        Alert.alert('Mohon cek kembali persaratan anda.');
      }
    });
  };

  render() {
    console.log(
      'ini state wwww',
      JSON.stringify(this.state.uploadSampleFileArray),
    );
    // console.log('ini props sales', JSON.stringify(this.props));

    // console.log('ini state', JSON.stringify(this.state.salesData));
    console.log('ini state', JSON.stringify(this.state.progress));
    console.log('loadingOnUpload', this.state.loadingOnUpload);

    let additional_options_array = [];
    let additional_options_array_picker;
    if (this.state.additionalOptions) {
      for (let key in this.state.additionalOptions) {
        let obj = {id: key, name: this.state.additionalOptions[key]};
        additional_options_array.push({...obj});
      }
      additional_options_array_picker = additional_options_array.map(
        (value) => {
          return (
            <Picker.Item key={value.id} value={value.id} label={value.name} />
          );
        },
      );
    }

    let shipping_companies;
    if (this.state.salesData && this.state.salesData.com_data) {
      shipping_companies = this.state.salesData.com_data.map((value) => {
        return (
          <Picker.Item key={value.id} value={value.id} label={value.name} />
        );
      });
    }
    let pay_status_data;
    if (this.state.salesData && this.state.salesData.pay_status_data) {
      pay_status_data = this.state.salesData.pay_status_data.map((value) => {
        return (
          <Picker.Item key={value.id} value={value.id} label={value.value} />
        );
      });
    }
    let service_label_details;
    if (this.state.service_label_details) {
      service_label_details = this.state.service_label_details.map((value) => {
        return (
          <Picker.Item
            key={value.servicelevel_name}
            value={value.servicelevel_name}
            label={value.servicelevel_name}
          />
        );
      });
    }
    // console.log('loading', this.state.loading);
    // console.log('progressBar', this.state.progressBar);

    if (this.state.loading) {
      return <Spinner color="red" size="large" />;
    }

    // if (this.state.progressBar) {
    //   return (
    //     <View style={styles.container}>
    //       <Progress.Bar
    //         color={'#e84118'}
    //         style={styles.progress}
    //         progress={this.state.progress}
    //         indeterminate={this.state.indeterminate}
    //       />
    //     </View>
    //   );
    // }

    const {
      bottonWrapperView,
      textInputStyle,
      modalHeadingStyle,
      innerModal,
      outerModal,
    } = styles;
    const {postageArray} = this.state;
    return (
      <View>
        <PTRView onRefresh={() => this.getSalesViewData()}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.show_status_modal}
            onRequestClose={() => {
              this.changeModalState();
            }}>
            <View style={outerModal}>
              <View style={innerModal}>
                <View style={styles.textRowView}>
                  <Text style={modalHeadingStyle}> Weight</Text>
                  <TextInput
                    style={textInputStyle}
                    onChangeText={(val) => this.setState({weight: val})}
                    value={this.state.weight}
                    placeholder={'Weight'}
                    keyboardType={'numeric'}
                  />
                </View>
                <View style={styles.pickerView}>
                  <Text style={modalHeadingStyle}> Weight Class</Text>
                  <View style={styles.dropdownClassModal}>
                    <Picker
                      style={styles.pickerClassModal}
                      selectedValue={this.state.weight_class_selected}
                      onValueChange={(val) =>
                        this.setState({weight_class_selected: val})
                      }>
                      <Picker.Item label="kg" value="kg" />
                      <Picker.Item label="oz" value="oz" />
                      <Picker.Item label="g" value="g" />
                      <Picker.Item label="lbs" value="lbs" />
                    </Picker>
                  </View>
                </View>
                {postageArray && (
                  <>
                    <View style={styles.pickerView}>
                      <Text style={modalHeadingStyle}> Provider</Text>
                      <View style={styles.dropdownClassModal}>
                        <Picker
                          style={styles.pickerClassModal}
                          selectedValue={this.state.provider_name_selected}
                          onValueChange={(val) =>
                            this.setProviderState(val, postageArray)
                          }>
                          {postageArray &&
                            postageArray.map((item) => {
                              return (
                                <Picker.Item
                                  label={item.name}
                                  value={item.name}
                                  key={item.name}
                                />
                              );
                            })}
                        </Picker>
                      </View>
                    </View>
                    <View style={styles.pickerView}>
                      <Text style={modalHeadingStyle}> Service Level Name</Text>
                      <View style={styles.dropdownClassModal}>
                        <Picker
                          style={styles.pickerClassModal}
                          selectedValue={this.state.service_level_name_select}
                          onValueChange={(val) =>
                            this.setServiceLevelState(val)
                          }>
                          {service_label_details}
                        </Picker>
                      </View>
                    </View>
                    <View style={styles.textRowView}>
                      <Text style={modalHeadingStyle}>Shipping Cost ($)</Text>
                      <TextInput
                        style={textInputStyle}
                        onChangeText={(val) =>
                          this.setState({rateSelected: val})
                        }
                        value={this.state.rateSelected}
                        placeholder={'Shipping Cost'}
                        keyboardType={'numeric'}
                      />
                    </View>
                    <View style={styles.textRowView}>
                      <Text style={modalHeadingStyle}>Processing Time</Text>
                      <TextInput
                        style={textInputStyle}
                        onChangeText={(val) =>
                          this.setState({processing_time_selected: val})
                        }
                        value={this.state.processing_time_selected}
                        placeholder={'Shipping Cost'}
                        keyboardType={'numeric'}
                      />
                    </View>
                  </>
                )}
                <View style={bottonWrapperView}>
                  <Button
                    color={'#C90205'}
                    title={'Cancel'}
                    onPress={this.changeModalState}
                  />
                  <Button
                    title={'Save and Continue'}
                    onPress={this.comfirmSubmit}
                  />
                </View>
              </View>
            </View>
          </Modal>
          <ScrollView style={{backgroundColor: '#f5f6fa'}}>
            {this.state.salesData && (
              <View>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('ProductDetail', {
                      product_id: this.state.salesData.product_id,
                    })
                  }>
                  <ProductItemCard
                    // {...this.props.route.params.details}
                    shop={false}
                    data={{
                      ...this.state.salesData,
                      // product_unit_price: this.state.salesData.cart_total,
                      product_quantity: this.state.salesData.qty,
                    }}
                  />
                </TouchableOpacity>

                <Section>
                  <View style={{}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingBottom: hp(1.5),
                      }}>
                      <Text>Status Pesanan</Text>
                      <Text style={{fontWeight: 'bold', color: '#000'}}>
                        {this.state.salesData.status}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Tanggal Penjualan</Text>
                      <Text style={{fontWeight: 'bold', color: '#000'}}>
                        {this.state.salesData.sale_date}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingTop: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Nomor Faktur</Text>
                      <Text>{this.state.salesData.invoice}</Text>
                    </View>
                  </View>
                </Section>
                <Section>
                  <Text
                    style={{
                      fontSize: hp(2),
                      fontWeight: 'bold',
                      color: '#000',
                      paddingBottom: hp(2),
                    }}>
                    Lihat Pesanan Penjualan
                  </Text>
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Total Harga Produk</Text>
                      <Text style={{fontWeight: 'bold', color: '#000'}}>
                        Rp. {formatRupiah(this.state.salesData.cart_total)}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Ongkos Pengiriman</Text>
                      <Text style={{fontWeight: 'bold', color: '#000'}}>
                        {`Rp. ${this.state.salesData.shipping_charges}`}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Pajak Penjualan</Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Rp. {this.state.salesData.sales_tax}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingTop: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Total Pembayaran
                      </Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Rp. {formatRupiah(this.state.salesData.order_total)}
                      </Text>
                    </View>
                  </View>
                </Section>

                <Section>
                  <Text
                    style={{
                      fontSize: hp(2),
                      fontWeight: 'bold',
                      color: '#000',
                      paddingBottom: hp(2),
                    }}>
                    Rincian Produk Penjualan
                  </Text>
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Nama Produk</Text>
                      <Text style={{fontWeight: 'bold', color: '#000'}}>
                        {this.state.salesData.product_name}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Waktu Proses</Text>
                      <Text style={{fontWeight: 'bold', color: '#000'}}>
                        {this.state.salesData.processing_time}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Harga</Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Rp. {formatRupiah(this.state.salesData.listed_price)}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Jumlah (qty)</Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        {this.state.salesData.qty}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Pengiriman</Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        {this.state.salesData.shipping_charges}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Biaya Administrasi</Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Rp.{' '}
                        {formatRupiah(
                          this.state.salesData.administrartion_commission,
                        )}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Jumlah</Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Rp. {this.state.salesData.order_total}
                      </Text>
                    </View>
                  </View>
                </Section>

                {this.state.salesData.package_details &&
                  this.state.salesData.package_details.map((package_detail) => (
                    <Section>
                      <Text
                        style={{
                          fontSize: hp(2),
                          fontWeight: 'bold',
                          color: '#000',
                          paddingBottom: hp(2),
                        }}>
                        Rincian Paket Penjualan
                      </Text>
                      <View>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: hp(1.5),
                            borderTopWidth: 1,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <Text>Kurir</Text>
                          <Text style={{fontWeight: 'bold', color: '#000'}}>
                            {package_detail.carrier}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: hp(1.5),
                            borderTopWidth: 1,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <Text>Layanan</Text>
                          <Text style={{fontWeight: 'bold', color: '#000'}}>
                            {package_detail.service}
                          </Text>
                        </View>

                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: hp(1.5),
                            borderTopWidth: 1,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <Text>Berat</Text>
                          <Text
                            style={{
                              fontWeight: 'bold',
                              color: '#000',
                            }}>
                            {package_detail.weight}
                          </Text>
                        </View>
                      </View>
                    </Section>
                  ))}

                {this.state.salesData.shipping_address && (
                  <Section>
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontWeight: 'bold',
                        color: '#000',
                        paddingBottom: hp(1),
                      }}>
                      Alamat Penagihan
                    </Text>
                    <Text style={{fontSize: hp(1.7)}}>
                      {this.state.salesData.shipping_address.name}
                    </Text>
                    <Text style={{fontSize: hp(1.7)}}>
                      {this.state.salesData.shipping_address.phone}
                    </Text>
                    <Text
                      style={{
                        fontSize: hp(1.7),
                      }}>{`${this.state.salesData.shipping_address.address1} ${this.state.salesData.shipping_address.address2}, ${this.state.salesData.shipping_address.state}, ${this.state.salesData.shipping_address.city}, ${this.state.salesData.shipping_address.country} - ${this.state.salesData.shipping_address.zip}`}</Text>
                  </Section>
                )}

                <View>
                  {this.state.salesData.payment_status &&
                    this.state.salesData.payment_status[0]?.payment_data.map(
                      (items) => (
                        <Section>
                          <Text
                            style={{
                              fontSize: hp(2),
                              fontWeight: 'bold',
                              color: '#000',
                              paddingBottom: hp(2),
                            }}>
                            Rincian Pembayaran
                          </Text>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              alignItems: 'center',
                              paddingVertical: hp(1.5),
                              borderTopWidth: 1,
                              borderTopColor: '#f5f6fa',
                            }}>
                            <Text
                              style={{
                                color: '#000',
                              }}>
                              Nama Pembeli
                            </Text>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                color: '#000',
                              }}>
                              {items.buyer}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              alignItems: 'center',
                              paddingVertical: hp(1.5),
                              borderTopWidth: 1,
                              borderTopColor: '#f5f6fa',
                            }}>
                            <Text
                              style={{
                                color: '#000',
                              }}>
                              Kode Transaksi
                            </Text>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                color: '#000',
                              }}
                              onPress={() => this.handleCopy(items.txn_id)}>
                              {substr(items.txn_id || '', 25)}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              alignItems: 'center',
                              paddingVertical: hp(1.5),
                              borderTopWidth: 1,
                              borderTopColor: '#f5f6fa',
                            }}>
                            <Text
                              style={{
                                color: '#000',
                              }}>
                              Metode Pembayaran
                            </Text>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                color: '#000',
                              }}>
                              {items.payment_method}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              alignItems: 'center',
                              paddingVertical: hp(1.5),
                              borderTopWidth: 1,
                              borderTopColor: '#f5f6fa',
                            }}>
                            <Text
                              style={{
                                color: '#000',
                              }}>
                              Jumlah
                            </Text>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                color: '#000',
                              }}>
                              Rp. {formatRupiah(items.amount)}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              alignItems: 'center',
                              paddingTop: hp(1.5),
                              borderTopWidth: 1,
                              borderTopColor: '#f5f6fa',
                            }}>
                            <Text
                              style={{
                                color: '#000',
                              }}>
                              Komentar
                            </Text>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                color: '#000',
                              }}>
                              {items.comments}
                            </Text>
                          </View>
                        </Section>
                      ),
                    )}
                </View>

                {this.state.salesData.status_details &&
                  this.state.salesData.status_details.map((status_detail) => (
                    <Section>
                      <Text
                        style={{
                          fontSize: hp(2),
                          fontWeight: 'bold',
                          color: '#000',
                          paddingBottom: hp(2),
                        }}>
                        Rincian Status
                      </Text>
                      <View>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: hp(1.5),
                            borderTopWidth: 1,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <Text>Tanggal</Text>
                          <Text style={{fontWeight: 'bold', color: '#000'}}>
                            {status_detail.date}
                          </Text>
                        </View>

                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: hp(1.5),
                            borderTopWidth: 1,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <Text>Proses</Text>
                          <Text
                            style={{
                              fontWeight: 'bold',
                              color: '#000',
                            }}>
                            {status_detail.customer_notified}
                          </Text>
                        </View>

                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: hp(1.5),
                            borderTopWidth: 1,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <Text>Status</Text>
                          <Text style={{fontWeight: 'bold', color: '#000'}}>
                            {status_detail.status}
                          </Text>
                        </View>

                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: hp(1.5),
                            borderTopWidth: 1,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <Text>Komentar</Text>
                          <Text
                            style={{
                              fontWeight: 'bold',
                              color: '#000',
                            }}>
                            {' '}
                          </Text>
                        </View>
                      </View>
                    </Section>
                  ))}

                <View>
                  {this.state.salesData.buyer_requirement &&
                    this.state.salesData.buyer_requirement.map(
                      (buyer_requirement) => (
                        <View style={styles.wrapperStyle2}>
                          <Section>
                            <Text
                              style={{
                                fontSize: hp(2),
                                fontWeight: 'bold',
                                color: '#000',
                                paddingBottom: hp(2),
                              }}>
                              Persyaratan Penjual
                            </Text>

                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                paddingVertical: hp(1.5),
                                borderTopWidth: 1,
                                borderTopColor: '#f5f6fa',
                              }}>
                              <Text>Subjek</Text>
                            </View>
                            <View style={styles.contentDiv}>
                              <Text
                                style={{
                                  fontWeight: 'bold',
                                  color: '#000',
                                }}>
                                {buyer_requirement.subject}
                              </Text>
                            </View>
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                paddingVertical: hp(1.5),
                                borderTopWidth: 1,
                                borderTopColor: '#f5f6fa',
                              }}>
                              <Text>Rincian Persyaratan</Text>
                            </View>
                            <View style={styles.contentDiv}>
                              <Text
                                style={{
                                  fontWeight: 'bold',
                                  color: '#000',
                                }}>
                                {buyer_requirement.message_text}
                              </Text>
                            </View>

                            {/* {console.log('buyer_requirement', buyer_requirement)} */}

                            {buyer_requirement.requirement_details &&
                              buyer_requirement.requirement_details?.map(
                                (requirement, index) => (
                                  <View key={index}>
                                    <View style={styles.row_2}>
                                      {this.state.selectedDownloadIndex ==
                                        index &&
                                        this.state.showDownloadMenu && (
                                          <OrderViewRightComponent
                                            onDownload={() =>
                                              this.downloadFile(requirement)
                                            }
                                          />
                                        )}

                                      <View style={styles.contentDiv}>
                                        <Text style={styles.sampleHeading}>
                                          {substr(
                                            requirement.file_name || '',
                                            26,
                                          )}
                                        </Text>
                                      </View>
                                      <TouchableOpacity
                                        style={{marginRight: 3}}
                                        onPress={() =>
                                          this.toggleDownloadButton(index)
                                        }>
                                        <Image
                                          resizeMode="contain"
                                          style={styles.rgtMenuIcon}
                                          source={require('../images/rgtMenuIcon.png')}
                                        />
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                ),
                              )}
                          </Section>
                        </View>
                      ),
                    )}
                  {this.state.salesData.upload_sample_file && (
                    <>
                      <Section
                        style={{
                          marginBottom: 1,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text
                          style={{
                            fontSize: hp(2),
                            fontWeight: 'bold',
                            color: '#000',
                          }}>
                          Unggah File Contoh yang harus dikerjakan.
                        </Text>
                        <PopUpInfo
                          // title={'Catatan'}
                          message={
                            'Kami menyarankan Anda untuk menambahkan tanda air atau watermark ke file PDF, Dokumen dan gambar untuk melindungi hak cipta file Anda ketika Anda mengunggah file contoh kepada pembeli. Anda dapat mengirim file yang lainnya ketika status berubah menjadi "Diterima" tanpa tanda air dari tab "Unggah file Produksi". Anda diharuskan menghapus tanda air atau watermark Anda dari file produksi ketika Anda mengunggah file produksi.'
                          }
                        />
                      </Section>

                      {this.state.upload_sample_file_array &&
                        this.state.upload_sample_file_array.map(
                          (value, index) => (
                            <Section
                              style={{borderWidth: 1, borderColor: '#f5f6fa'}}>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                  alignItems: 'center',
                                  paddingVertical: hp(1.5),
                                }}>
                                <Text>Nama unduhan</Text>
                              </View>
                              <View style={styles.contentDiv}>
                                <Text
                                  style={{
                                    fontWeight: 'bold',
                                    color: '#000',
                                  }}>
                                  {value.product_download_name}
                                </Text>
                              </View>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                  alignItems: 'center',
                                  paddingVertical: hp(1.5),
                                }}>
                                <Text>Nama file</Text>
                              </View>
                              <View style={styles.contentDiv}>
                                <Text
                                  style={{
                                    fontWeight: 'bold',
                                    color: '#000',
                                  }}>
                                  {value.file_name}
                                </Text>
                              </View>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                  alignItems: 'center',
                                  paddingVertical: hp(1.5),
                                }}>
                                <Text>Waktu perubahan</Text>
                              </View>
                              <View style={styles.contentDiv}>
                                <Text
                                  style={{
                                    fontWeight: 'bold',
                                    color: '#000',
                                  }}>
                                  {value.revision_time_value}
                                </Text>
                              </View>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                  alignItems: 'center',
                                  paddingVertical: hp(1.5),
                                }}>
                                <Text>Status</Text>
                              </View>
                              <View style={styles.contentDiv}>
                                <Text
                                  style={{
                                    fontWeight: 'bold',
                                    color: '#000',
                                  }}>
                                  {value.upload_sample_file_status}
                                </Text>
                              </View>

                              {value.file_name != '' &&
                                this.state.uploadWidth == 0 && (
                                  <View>
                                    <FileUploadList
                                      sparator={false}
                                      data={['value']}
                                      item={{
                                        name: substr(value.file_name, 20),
                                        item: {
                                          icon: 'file-upload',
                                          size: 45,
                                        },
                                      }}
                                      comments={value.upload_sample_file_status}
                                    />
                                  </View>
                                )}
                            </Section>
                          ),
                        )}

                      {this.state.salesData.buyer_requirement !== null &&
                        this.state.salesData.upload_sample_file && (
                          <View style={{backgroundColor: '#fff'}}>
                            {this.state.uploadSampleFileArray.map(
                              (data, index) => (
                                <Content style={{marginHorizontal: wp(2)}}>
                                  <View style={{flex: 1}}>
                                    <Text>Nama File</Text>
                                    <Item regular style={{margin: 5}}>
                                      <Input
                                        onChangeText={(value) => {
                                          this.setState((state) => {
                                            const list = state.uploadSampleFileArray.map(
                                              (item, i) => {
                                                if (index === i) {
                                                  return (item.product_download_name = value);
                                                } else {
                                                  return item;
                                                }
                                              },
                                            );
                                            return {
                                              list,
                                            };
                                          });
                                        }}
                                        value={data.product_download_name}
                                      />
                                    </Item>
                                  </View>

                                  {this.state.salesData
                                    .is_upload_file_sample_next
                                    ?.upload_file_sample_next !== true ? (
                                    <Item regular style={{margin: 5}}>
                                      <Input
                                        keyboardType="numeric"
                                        value={data.revision_time_value}
                                        onChangeText={(value) => {
                                          this.setState((state) => {
                                            const list = state.uploadSampleFileArray.map(
                                              (item, i) => {
                                                if (index === i) {
                                                  return (item.revision_time_value = value);
                                                } else {
                                                  return item;
                                                }
                                              },
                                            );
                                            return {
                                              list,
                                            };
                                          });
                                        }}
                                        placeholder="Batas Revisi"
                                      />
                                    </Item>
                                  ) : (
                                    <View style={{flex: 1}}>
                                      <Text>Batas Revisi</Text>
                                      <Item regular style={{margin: 5}}>
                                        <Text
                                          style={[
                                            styles.textInputContainer,
                                            styles.shadow,
                                          ]}>
                                          {data.revision_time_value}
                                        </Text>
                                      </Item>
                                    </View>
                                  )}

                                  {/* <Input
                                      keyboardType="numeric"
                                      value={data.revision_time_value}
                                      onChangeText={(value) => {
                                        this.setState((state) => {
                                          const list = state.uploadSampleFileArray.map(
                                            (item, i) => {
                                              if (index === i) {
                                                return (item.revision_time_value = value);
                                              } else {
                                                return item;
                                              }
                                            },
                                          );
                                          return {
                                            list,
                                          };
                                        });
                                      }}
                                      placeholder="Batas Revisi"
                                    /> */}

                                  <ProductFileUploadProgress
                                    value={this.state.file_download.filename}
                                    // index={i}
                                    loading={this.state.loadingOnUpload}
                                    progressUpload={this.state.progress}
                                    intermediate={this.state.indeterminate}
                                    onPress={({type, index, ref, item}) => {
                                      ref.close();
                                      this.selectFileSample(type, item);
                                    }}
                                    fileName={this.state.fileNameSample}
                                  />

                                  <Section>
                                    <ButtonNative
                                      block
                                      danger
                                      disabled={!this.state.fileNameSample}
                                      onPress={() =>
                                        this.updateSalesViewSampleFiles()
                                      }>
                                      <Text>Update</Text>
                                    </ButtonNative>
                                  </Section>
                                </Content>
                              ),
                            )}
                          </View>
                        )}
                    </>
                  )}
                  {this.state.salesData.upload_production_file &&
                    this.state.salesData.upload_production_file === true && (
                      <>
                        <View>
                          <Section
                            style={{
                              marginBottom: 1,
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <Text
                              style={{
                                fontSize: hp(2),
                                fontWeight: 'bold',
                                color: '#000',
                              }}>
                              Unggah file produksi akhir.
                            </Text>
                            <PopUpInfo
                              // title={'Catatan'}
                              message={
                                'Silakan unggah file produksi di sini. Transaksi ini akan berubah status menjadi lengkap setelah pembeli menerima file ini.'
                              }
                            />
                          </Section>

                          {this.state.salesData.upload_product_file_array &&
                            this.state.salesData.upload_product_file_array.map(
                              (value, index) => (
                                <Section
                                  style={{
                                    borderWidth: 1,
                                    borderColor: '#f5f6fa',
                                  }}>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      paddingVertical: hp(1.5),
                                    }}>
                                    <Text>Nama unduhan</Text>
                                  </View>
                                  <View style={styles.contentDiv}>
                                    <Text
                                      style={{
                                        fontWeight: 'bold',
                                        color: '#000',
                                      }}>
                                      {value.product_download_name}
                                    </Text>
                                  </View>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      paddingVertical: hp(1.5),
                                    }}>
                                    <Text>Nama file</Text>
                                  </View>
                                  <View style={styles.contentDiv}>
                                    <Text
                                      style={{
                                        fontWeight: 'bold',
                                        color: '#000',
                                      }}>
                                      {value.file_name}
                                    </Text>
                                  </View>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      paddingVertical: hp(1.5),
                                    }}>
                                    <Text>Komentar</Text>
                                  </View>
                                  <View style={styles.contentDiv}>
                                    <Text
                                      style={{
                                        fontWeight: 'bold',
                                        color: '#000',
                                      }}>
                                      {value.production_file_comments}
                                    </Text>
                                  </View>

                                  {value.file_name != '' &&
                                    this.state.uploadWidth == 0 && (
                                      <FileUploadList
                                        sparator={false}
                                        data={['value']}
                                        item={{
                                          name: value.file_name,
                                          item: {
                                            icon: 'file-upload',
                                            size: 45,
                                          },
                                        }}
                                        comments={
                                          value.upload_sample_file_status
                                        }
                                      />
                                    )}
                                </Section>
                              ),
                            )}

                          {this.state.uploadProductionFileArray.map(
                            (data, index) => (
                              <Section>
                                <CardSection>
                                  <InputText
                                    value={data.download_name}
                                    onChangeText={(value) => {
                                      this.setState((state) => {
                                        const list = state.uploadProductionFileArray.map(
                                          (item, i) => {
                                            if (index === i) {
                                              return (item.download_name = value);
                                            } else {
                                              return item;
                                            }
                                          },
                                        );
                                        return {
                                          list,
                                        };
                                      });
                                    }}
                                    label="Nama Unduhan"
                                  />
                                </CardSection>
                                <View style={{marginBottom: hp(2)}}>
                                  <ProductFileUploadProgress
                                    value={this.state.file_download.filename}
                                    // index={i}
                                    // loadingText={this.state.progress}
                                    progressUpload={this.state.progress}
                                    loading={this.state.loadingOnUpload}
                                    intermediate={this.state.intermediate}
                                    onPress={({type, index, ref, item}) => {
                                      ref.close();
                                      this.selectFileProduction(type, item);
                                    }}
                                    fileName={this.state.fileNameProduction}
                                  />
                                </View>

                                <CardSection>
                                  <InputText
                                    value={data.comments}
                                    onChangeText={(value) => {
                                      this.setState((state) => {
                                        const list = state.uploadProductionFileArray.map(
                                          (item, i) => {
                                            if (index === i) {
                                              return (item.comments = value);
                                            } else {
                                              return item;
                                            }
                                          },
                                        );
                                        return {
                                          list,
                                        };
                                      });
                                    }}
                                    label="Komentar"
                                  />
                                </CardSection>
                              </Section>
                            ),
                          )}

                          <Section>
                            <ButtonNative
                              block
                              danger
                              disabled={!this.state.fileNameProduction}
                              onPress={() =>
                                this.updateSalesViewProductionFiles()
                              }>
                              <Text>Update File Produksi</Text>
                            </ButtonNative>
                          </Section>
                        </View>
                      </>
                    )}
                </View>
              </View>
            )}

            <View style={{backgroundColor: '#fff'}}>
              {this.state.salesData &&
                this.state.salesData.shipping_address &&
                this.state.salesData.com_data && (
                  <View style={{padding: 10, justifyContent: 'center'}}>
                    <Text
                      style={{
                        fontSize: wp('4.3%'),
                        color: '#000',
                        fontFamily: Font.RobotoMedium,
                        // marginBottom: hp('1%'),
                      }}>
                      Komentar Pesanan
                    </Text>
                    {this.state.statusSelected === 5 && (
                      <CardSection>
                        <InputText
                          label="Nomor Resi"
                          // card={true}
                          value={this.state.trackNum}
                          onChangeText={(val) => this.setState({trackNum: val})}
                        />
                      </CardSection>
                    )}

                    <ProductDinamicDropdown
                      items={this.state.salesData.pay_status_data}
                      keyName={'value'}
                      keyId={'id'}
                      value={this.state.statusSelected}
                      title={'Status'}
                      onPress={({val, ref}) => {
                        ref.close();
                        this.setState({
                          statusSelected: val,
                        });
                      }}
                    />
                    {/* <Picker.Item label="+" value="P" /> */}
                    {this.state.statusSelected == 5 && (
                      <ProductDinamicDropdown
                        items={this.state.salesData.com_data}
                        keyName={'name'}
                        keyId={'id'}
                        value={this.state.shipCompanySelected}
                        title={'Kurir'}
                        onPress={({val, ref}) => {
                          ref.close();
                          this.setState({
                            shipCompanySelected: val,
                          });
                        }}
                      />
                    )}
                    <CardSection style={{marginTop: hp(2)}}>
                      <InputText
                        label="Keterangan"
                        card={true}
                        message={'not-mandatory'}
                        value={this.state.comment}
                        onChangeText={(val) => this.setState({comment: val})}
                      />
                    </CardSection>
                    <View style={styles.radioWrappper}>
                      <CardSection style={{marginTop: hp(2)}}>
                        <Text style={styles.heading_style}>
                          Beritahu Pembeli
                        </Text>
                        <CheckBox
                          onPress={() =>
                            this.setState({
                              customer_notify: !this.state.customer_notify,
                            })
                          }
                          checked={this.state.customer_notify}
                        />

                        {/* <InputText
                        label="Notif"
                        card={true}
                        message={'not-mandatory'}
                        value={this.state.customer_notify}
                        onChangeText={(val) =>
                          this.setState({customer_notify: val})
                        }
                      /> */}
                      </CardSection>
                    </View>
                    <View style={{marginTop: hp(2)}}>
                      <ButtonRed onPress={() => this.updateSalesView()}>
                        Update
                      </ButtonRed>
                    </View>
                  </View>
                )}
            </View>
            {this.state.salesData &&
              this.state.salesData.downloadable_products_data && (
                <View>
                  <Section
                    style={{
                      marginBottom: 1,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontWeight: 'bold',
                        color: '#000',
                      }}>
                      RINCIAN PRODUK UNDUHAN
                    </Text>
                    <PopUpInfo
                      // title={'Catatan'}
                      message={
                        'TAB INI AKAN MEMBERI ANDA PILIHAN JIKA ANDA INGIN MEMBERIKAN KESEMPATAN SATU KALI AKSES UNDUHAN BAGI PEMBELI UNTUK MENGUNDUH KEMBALI WALAUPUN SISA UNDUHAN SUDAH HABIS ATAU TANGGAL VALIDITAS TELAH KEDALUWARSA TETAPI HANYA SATU KALI SAJA UNTUK MENINGKATKAN PELAYANAN TERBAIK ANDA DENGAN PEMBELI. ANDA DAPAT MENGGUNAKAN PEMBATALAN AKSES UNTUK MEMBATALKAN FILE UNDUHAN, KETIKA FILE UNDUHAN DIBATALKAN, TAUTAN UNDUHAN AKAN DINONAKTIFKAN. INGAT, KETIKA ANDA MENGAKTIFKAN MEMBATALKAN AKSES YANG BERARTI TOMBOL AKSES HIBAH AKAN MENJADI NONAKTIF.'
                      }
                    />
                  </Section>

                  {this.state.salesData.downloadable_products_data.map(
                    (item, index) => (
                      <Section key={index}>
                        <ListItem
                          title="Unduhan Yang Tersisa"
                          subtitle={item.remain_text}
                        />
                        <ListItem
                          title="Tanggal Berakhir"
                          subtitle={item.download_expire}
                        />
                        <ListItem
                          style={{fontWeight: 'bold'}}
                          title="Unduhan Sekali"
                          subtitle={substr(item.file_name, 15)}
                        />
                        <View
                          style={{
                            flexDirection: 'column',
                          }}>
                          <View style={{marginVertical: hp(1)}}>
                            <ButtonNative
                              block
                              danger
                              onPress={() => this.grantAccess(item.file_id)}
                              disabled={item.grant_access_btn_disable !== 'N'}>
                              <Text>AKSES DISETUJUI</Text>
                            </ButtonNative>
                          </View>
                          <View style={{marginVertical: hp(1)}}>
                            <ButtonNative
                              block
                              danger
                              onPress={() => this.revokeAccess(item.file_id)}
                              disabled={item.action !== 'MEMBATALKAN AKSES'}>
                              <Text>
                                {item.action !== 'disable'
                                  ? 'MEMBATALKAN AKSES'
                                  : 'REVOKED'}
                              </Text>
                            </ButtonNative>
                          </View>
                        </View>
                      </Section>
                    ),
                  )}
                </View>
              )}
          </ScrollView>
        </PTRView>
      </View>
    );
  }
}

const ListItem = ({title, subtitle, style}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: hp(2),
        // borderTopWidth: 1,
        // borderTopColor: '#f5f6fa',
      }}>
      <Text
        style={{
          ...style,
          color: '#000',
        }}>
        {title}
      </Text>
      <Text
        style={{
          ...style,
          color: '#000',
        }}>
        {subtitle}
      </Text>
    </View>
  );
};

const styles = {
  addBtn: {
    backgroundColor: '#DC0028',
    width: wp('20%'),
    height: wp('10%'),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    alignSelf: 'flex-end',
    marginBottom: 10,
  },
  addTxt: {
    color: '#fff',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: wp('7%'),
  },
  addFile: {
    fontSize: hp('2.5%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#C90205',
  },
  sampleHeading: {
    color: '#545454',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.3%'),
  },
  contentDiv: {
    marginLeft: 30,
    flex: 1,
  },
  lftImg: {
    width: '100%',
    height: hp('10%'),
  },
  imgDiv: {
    width: '25%',
    paddingRight: 10,
  },
  row_2: {
    flexDirection: 'row',
  },
  wrapperStyle2: {
    flex: 1,
    borderRadius: 3,
  },
  pickerView: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  textRowView: {
    margin: 20,
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
  },
  modalHeadingStyle: {
    color: '#545454',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
    textAlign: 'center',
    marginRight: 10,
  },
  bottonWrapperView: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    margin: 20,
    width: '100%',
  },
  innerModal: {
    width: wp('80%'),
    minHeight: hp('30%'),
    backgroundColor: '#fff',
    padding: 20,
    zIndex: 5,
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  outerModal: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  textInputStyle: {
    height: 40,
    borderRadius: 10,
    // width:'15%',
    flex: 1,
    // borderColor: 'cfcdcd',
    borderWidth: 1,
  },
  radioWrappper: {
    flexDirection: 'row',
    paddingTop: 8,
    paddingBottom: 8,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  labelstyle: {
    fontSize: hp('2.5%'),
    color: '#545454',
    marginRight: 10,
    marginLeft: 0,
    fontFamily: Font.RobotoRegular,
  },
  heading_style: {
    color: '#545454',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
  },
  continueBtn: {
    paddingHorizontal: wp(3),
  },
  dropdownClass: {
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  pickerClass: {
    height: hp('6%'),
  },
  dropdownClassModal: {
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
    flex: 1,
  },
  pickerClassModal: {
    height: hp('6%'),
    width: '100%',
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  statusDetail: {
    borderRightColor: '#dedddd',
    borderRightWidth: 1,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 8,
  },
  tableWrapper: {
    borderColor: '#f1f0f0',
    borderWidth: 1,
    width: 600,
  },
  tableWrapperOuter: {
    width: '100%',
    overFlow: 'scroll',
    borderWidth: 1,
    borderColor: '#dedddd',
    zIndex: 1001,
    marginLeft: 10,
    marginRight: 10,
  },
  tableHeader: {
    flexDirection: 'row',
    borderBottomColor: '#dedddd',
    borderBottomWidth: 1,
    backgroundColor: '#f1f0f0',
  },
  tableRow: {
    flexDirection: 'row',
    borderBottomColor: '#dedddd',
  },
  AcccontentWrapper: {
    padding: 10,
    borderBottomColor: '#e7e7e7',
    borderBottomWidth: 1,
  },
  // row_:{
  //     flexDirection:'row',
  //     justifyContent:'space-between',
  //     marginBottom:hp('1%')
  // },
  // lftTxt:{
  //     color:'#858585',
  //     fontFamily:Font.RobotoRegular,
  //     fontSize:hp('2.5%'),
  // },
  // rgtTxt:{
  //     color:'#00b3ff',
  //     fontFamily:Font.RobotoRegular,
  //     fontSize:hp('2.5%'),
  // },
  hea_der: {
    flexDirection: 'row',
    paddingVertical: hp('2%'),
    paddingHorizontal: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderBottomColor: '#e7e7e7',
    borderBottomWidth: 1,
    borderTopColor: '#f00',
    elevation: 0,
  },
  hea_der2: {
    backgroundColor: '#f1f1f1',
  },

  abc: {},
  flexCss: {
    width: '80%',
    flexWarp: 'warp',
  },
  editIcon: {
    width: 40,
    height: 40,
  },
  icon: {
    width: 14,
    height: 14,
  },
  iconAddress: {
    width: 13,
    height: 18,
    marginTop: 3,
  },
  addressWrapper: {
    marginTop: hp('1%'),
    // borderBottomWidth:1,
    // borderColor:'#e7e7e7',
    paddingBottom: hp('1%'),
    paddingHorizontal: 10,
  },
  addressDiv: {
    paddingBottom: hp('2%'),
    marginBottom: hp('2%'),
    borderBottomWidth: 1,
    borderBottomColor: '#e7e7e7',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  billingAddress: {
    fontSize: wp('4.3%'),
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    marginBottom: hp('1%'),
  },
  billingAddress1: {
    fontSize: wp('4.3%'),
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    marginBottom: hp('1%'),
    padding: wp('2%'),
  },
  productName2: {
    fontSize: wp('4.2%'),
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },
  iconBox: {
    flexDirection: 'row',
    marginTop: 5,
    // alignItems:'center',
  },
  iconEmail: {
    width: 14,
    height: 11,
    marginTop: 4,
  },
  iconTxt: {
    fontSize: wp('4%'),
    marginLeft: 8,
  },
  btn_style: {
    paddingLeft: 10,
    paddingRight: 10,
    width: 'auto',
    marginTop: hp('3%'),
  },
  bgColor: {
    backgroundColor: '#fff',
  },
  boxStyle: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    paddingBottom: 0,
    flexDirection: 'row',
    flexWarp: 'warp',
    position: 'relative',
  },
  imgContentWrapper: {
    flexDirection: 'row',
    // flexBasis:'80%',
    flex: 1,
    paddingRight: 15,
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    paddingBottom: 10,
  },
  boxImg: {
    width: wp('20%'),
    height: wp('32%'),
  },
  labelStyle: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  labelStyle2: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#C90205',
  },
  boxImg2: {
    marginBottom: 10,
    height: hp('5.3%'),
    width: wp('10%'),
  },
  imgWrapper: {
    width: wp('20%'),
    marginRight: wp('3%'),
  },
  available: {
    color: '#858585',
    fontSize: hp('2%'),
    marginRight: wp('10%'),
    // fontFamily:Font.RobotoMedium,
  },
  price: {
    color: '#00b3ff',
    fontSize: wp('5%'),
    // fontFamily:Font.RobotoBold,
  },
  availableWrapper: {
    flexDirection: 'row',
    paddingTop: hp('0.4%'),
    paddingBottom: hp('0.4%'),
  },
  rgtMenuIcon: {
    width: wp('7%'),
    height: wp('7%'),
  },
  productName: {
    fontSize: wp('4.5%'),
    color: '#545454',
    // fontFamily:Font.RobotoRegular,
  },
  count: {
    color: '#00b3ff',
    fontSize: hp('2%'),
  },
  stat_us: {
    color: '#c90305',
    fontSize: hp('2%'),
  },
  priceHeading: {
    color: '#696969',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoMedium,
  },
  totalPriceWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: hp('1.5%'),
    paddingTop: hp('1.5%'),
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  totalItem: {
    fontSize: wp('4.2%'),
    color: '#6f6f6f',
    fontFamily: Font.RobotoRegular,
  },
  boldClass: {
    fontFamily: Font.RobotoBold,
  },
  priceTxt: {
    color: '#00b3ff',
    // fontWeight:'bold',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoBold,
  },
  priceDetail: {
    padding: 10,
  },
  plusBtn: {
    position: 'absolute',
    width: 50,
    height: 50,
    right: 10,
    bottom: 30,
  },
  textInputContainer: {
    borderRadius: 2,
    width: '100%',
    // height: '100%',
    marginVertical: hp(1),
    paddingVertical: hp(1),
    paddingHorizontal: wp(3),
  },
  shadow: {},
  circles: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  progress: {
    margin: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingVertical: 20,
  },
};
