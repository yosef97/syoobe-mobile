import React from "react";
import { Image, StyleSheet, TouchableOpacity, View, Text } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

const postageAccountListComponent = props => (
  <View style={styles.bdSection}>
    <View style={styles.rowView}>
      <View style={styles.lftTextView}>
        <Text style={styles.lftTxt}>Tanggal</Text>
      </View>
      <View style={styles.rgtTextView}>
        <Text style={styles.rgtTxt}>{props.detail.date}</Text>
      </View>
    </View>
    <View style={styles.rowView}>
      <View style={styles.lftTextView}>
        <Text style={styles.lftTxt}>Faktur</Text>
      </View>
      <View style={styles.rgtTextView}>
        <Text style={[styles.rgtTxt, styles.txtColor]}>
          {props.detail.invoice_number}
        </Text>
      </View>
    </View>
    <View style={styles.rowView}>
      <View style={styles.lftTextView}>
        <Text style={styles.lftTxt}>Nama Produk Penerima</Text>
      </View>
      <View style={styles.rgtTextView}>
        <Text style={styles.rgtTxt}>{props.detail.recipient_product_name}</Text>
      </View>
    </View>
    <View style={styles.rowView}>
      <View style={styles.lftTextView}>
        <Text style={styles.lftTxt}>Pembawa</Text>
      </View>
      <View style={styles.rgtTextView}>
        <Text style={styles.rgtTxt}>{props.detail.carrier}</Text>
      </View>
    </View>
    <View style={styles.rowView}>
      <View style={styles.lftTextView}>
        <Text style={styles.lftTxt}>Layanan</Text>
      </View>
      <View style={styles.rgtTextView}>
        <Text style={styles.rgtTxt}>{props.detail.service}</Text>
      </View>
    </View>
    <View style={styles.rowView}>
      <View style={styles.lftTextView}>
        <Text style={styles.lftTxt}>Opsi tambahan</Text>
      </View>
      <View style={styles.rgtTextView}>
        <Text style={styles.rgtTxt}>{props.detail.additional_option}</Text>
      </View>
    </View>
    <View style={styles.rowView}>
      <View style={styles.lftTextView}>
        <Text style={styles.lftTxt}>Melacak nomor</Text>
      </View>
      <View style={styles.rgtTextView}>
        <Text style={[styles.rgtTxt, styles.txtColor]}>
          {props.detail.tracking_number}
        </Text>
      </View>
    </View>
    <View style={styles.rowView}>
      <View style={styles.lftTextView}>
        <Text style={styles.lftTxt}>Status</Text>
      </View>
      <View style={styles.rgtTextView}>
        <Text style={styles.rgtTxt}>{props.detail.status}</Text>
      </View>
    </View>
    <View style={styles.rowView}>
      <View style={styles.lftTextView}>
        <Text style={styles.lftTxt}>Biaya Ongkos Kirim</Text>
      </View>
      <View style={styles.rgtTextView}>
        <Text style={styles.rgtTxt}>$ {props.detail.postage_cost}</Text>
      </View>
    </View>
    <View style={[styles.rowView, styles.btnRow]}>
      <View style={styles.lftTextView}>
        <Text style={styles.lftTxt}>Tindakan</Text>
      </View>
      <View style={[styles.rgtTextView, styles.btnWrapper]}>
        {props.detail.reprint_the_lable == "Y" && (
          <TouchableOpacity
            disabled={
              props.detail.reprint_the_lable_active == "N" ? true : false
            }
            onPress={() => props.reprintLable(props.detail.tnd_id)}
            style={styles.actionBtn}
          >
            <Image
              style={styles.fileIcon}
              source={require("../images/textFileIcon.png")}
            />
          </TouchableOpacity>
        )}
        {props.detail.re_print_a_return_shipping_center_label_display ==
          "Y" && (
          <TouchableOpacity
            onPress={() => props.reprintReturnLabel(props.detail.tnd_id)}
            style={styles.actionBtn}
          >
            <Image
              style={styles.fileIcon}
              source={require("../images/printerIcon.png")}
            />
          </TouchableOpacity>
        )}
        {props.detail.re_print_a_return_label_display == "Y" && (
          <TouchableOpacity
            onPress={() => props.reprintReturnLabel(props.detail.tnd_id)}
            style={styles.actionBtn}
          >
            <Image
              style={styles.fileIcon}
              source={require("../images/printerIcon.png")}
            />
          </TouchableOpacity>
        )}
        {props.detail.print_a_shipping_center_return_lable_display == "Y" && (
          <TouchableOpacity
            onPress={() =>
              props.printReturnShippingCenterLabelHandler(
                props.detail.tnd_id,
                props.detail.tmu_id
              )
            }
            style={styles.actionBtn}
          >
            <Image
              style={styles.fileIcon}
              source={require("../images/printerIcon.png")}
            />
          </TouchableOpacity>
        )}
        {props.detail.print_return_label_display == "Y" && (
          <TouchableOpacity
            onPress={() =>
              props.printReturnLabelHanlder(
                props.detail.tnd_id,
                props.detail.tnd_opr_id
              )
            }
            style={styles.actionBtn}
          >
            <Image
              style={styles.fileIcon}
              source={require("../images/printerIcon.png")}
            />
          </TouchableOpacity>
        )}
        {props.detail.send_label_notification_display == "Y" && (
          <TouchableOpacity
            onPress={() => props.sendLabelEmail(props.detail.tnd_id)}
            style={styles.actionBtn}
          >
            <Image
              style={styles.fileIcon}
              source={require("../images/uploadIcon.png")}
            />
          </TouchableOpacity>
        )}
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  txtColor: {
    color: "#ed0030"
  },
  actionBtn: {
    width: "31%",
    backgroundColor: "#c90305",
    borderRadius: 50,
    paddingTop: 5,
    paddingBottom: 5,
    textAlign: "center"
  },
  fileIcon: {
    width: wp("4%"),
    height: hp("2.5%"),
    resizeMode: "contain",
    marginLeft: "auto",
    marginRight: "auto"
  },
  btnWrapper: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingLeft: 5,
    paddingRight: 5
  },
  btnRow: {
    marginBottom: 35
  },
  bdSection: {
    // flexDirection:'row'
  },
  rowView: {
    flexDirection: "row",
    justifyContent: "space-between",
    // alignItems:'center',
    marginBottom: 8
  },
  lftTextView: {
    width: "38%",
    backgroundColor: "#f6f8f9",
    borderRadius: 2,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: hp("1.8%"),
    borderWidth: 1,
    borderColor: "#e8edef",
    elevation: 0.5
  },
  rgtTextView: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingVertical: hp("1.8%"),
    flex: 1,
    marginLeft: 20,
    borderRadius: 2,
    elevation: 0.5,
    justifyContent: "center"
  },
  lftTxt: {
    textAlign: "center",
    color: "#4e4c4c",
    fontSize: hp("1.8%")
  },
  rgtTxt: {
    color: "#7d7d7d",
    fontSize: hp("1.8%"),
    textAlign: "left"
  }
});

export default postageAccountListComponent;
