import React, {Component} from 'react';
import {Text, AsyncStorage, TouchableOpacity, View} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {Font} from './Font';
import {loginUser} from '../actions';
import {Spinner} from './common';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

class Otp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      otp: '',
      username: props.route.params.username,
      password: props.route.params.password,
      newOtp: ['-', '-', '-', '-'],
      index: 0,
      device_token: null,
    };
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    return fcmToken;
  }

  verifyOtp = async () => {
    let device_token = await this.getToken();
    const {otp, username, password} = this.state;
    console.log('device token', device_token);

    if (String(username).includes('@')) {
      var email = String(username).toLowerCase();
      this.props.loginUser(
        this.props.navigation,
        email,
        password,
        device_token,
        otp,
      );
    } else {
      this.props.loginUser(
        this.props.navigation,
        username,
        password,
        device_token,
        otp,
      );
    }
  };

  handleChange = (text) => {
    const {newOtp, index} = this.state;
    let otpData = [...newOtp];
    otpData[index] = text;
    console.log(index);
    if (index < 4) {
      this.setState((state) => ({
        otp: state.otp + text,
        newOtp: otpData,
        index: index + 1,
      }));
    }

    if (index === 3) {
      this.verifyOtp();
    }
  };

  onReset = () => {
    this.setState({newOtp: ['-', '-', '-', '-'], index: 0});

    this.setState({otp: ''});
  };

  render() {
    const {loading, otpSentToNumber} = this.props;
    if (loading) {
      return <Spinner color="red" />;
    }
    return (
      <View style={{flex: 1, backgroundColor: '#d63031'}}>
        <View style={{flex: 1}}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text
              style={{
                fontSize: hp(2.6),
                fontWeight: 'bold',
                marginBottom: hp(1),
                color: '#f5f6fa',
              }}>
              Masukkan Kode Verifikasi
            </Text>
            <Text style={{color: '#f5f6fa'}}>
              Kami Telah mengirimkan Kode Verifikasi
            </Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={{color: '#f5f6fa'}}>Ke</Text>
              <Text
                style={{
                  fontSize: hp(2.1),
                  paddingLeft: wp(2),
                  color: '#f5f6fa',
                }}>
                {otpSentToNumber}
              </Text>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              paddingHorizontal: wp(15),
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              {this.state.newOtp.map((item, index) => (
                <View style={styles.otpText}>
                  <Text>{item}</Text>
                </View>
              ))}
            </View>
          </View>
          <View style={{flex: 2, paddingHorizontal: wp(5)}}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <TouchableOpacity
                onPress={() => this.handleChange(1)}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <Text style={styles.textKeyboard}>1</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleChange(2)}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <Text style={styles.textKeyboard}>2</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleChange(3)}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <Text style={styles.textKeyboard}>3</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <TouchableOpacity
                onPress={() => this.handleChange(4)}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <Text style={styles.textKeyboard}>4</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleChange(5)}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <Text style={styles.textKeyboard}>5</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleChange(6)}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <Text style={styles.textKeyboard}>6</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <TouchableOpacity
                onPress={() => this.handleChange(7)}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <Text style={styles.textKeyboard}>7</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleChange(8)}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <Text style={styles.textKeyboard}>8</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleChange(9)}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <Text style={styles.textKeyboard}>9</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <TouchableOpacity style={styles.cardEnter}>
                {/* <Text style={styles.textKeyboard}></Text> */}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleChange(0)}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <Text style={styles.textKeyboard}>0</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.onReset()}
                style={styles.cardEnter}
                activeOpacity={0.5}>
                <FontAwesome5Icon
                  name={'undo'}
                  style={{...styles.textKeyboard, fontSize: hp(2.6)}}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  cardEnter: {
    width: wp(15),
    height: hp(8),
    borderRadius: hp(2),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textKeyboard: {
    fontSize: hp(4),
    fontWeight: 'bold',
    color: '#f5f6fa',
    // paddingVertical: hp(2),
  },
  otpText: {
    width: wp(12),
    height: hp(8),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
  },
  bannerImg: {
    width: '100%',
    height: hp('40%'),
  },
  headingTxt: {
    textAlign: 'center',
    color: '#000',
    fontSize: hp('3%'),
    fontFamily: Font.RobotoBold,
    marginTop: 35,
  },
  inputWrapper: {
    marginTop: 35,
    textAlign: 'center',
  },
  inputTxt: {
    textAlign: 'center',
    color: '#000',
    fontSize: hp('2.5%'),
    marginBottom: 10,
  },
  inputStyle: {
    borderWidth: 1,
    borderColor: '#000',
    width: '60%',
    marginLeft: 'auto',
    marginRight: 'auto',
    height: 40,
    color: '#000',
    textAlign: 'center',
    marginBottom: 20,
  },
  buttonStyle: {
    width: '30%',
    marginLeft: 'auto',
    marginRight: 'auto',

    backgroundColor: '#ca145e',
    marginTop: 15,

    paddingTop: 10,
    paddingBottom: 10,
  },
  btntextStyle: {
    textAlign: 'center',
    color: '#fff',
    fontSize: hp('2%'),
  },
  bottomTxt: {
    textAlign: 'center',
    color: '#2f7ab4',
    fontSize: hp('2%'),
    marginTop: 20,
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.auth.loading,
    otpSentToNumber: state.auth.otpSentToNumber,
  };
};

export default connect(mapStateToProps, {loginUser})(Otp);
