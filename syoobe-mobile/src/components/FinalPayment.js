import React, {Component} from 'react';
import {ScrollView, View, Text} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {Font} from './Font';
import axios from 'axios';
import {Spinner} from './common';
import {getWalletDetails} from '../actions';
// import request from 'request';
// import qs from 'qs';
import {BASE_URL} from '../services/baseUrl';
import {ERROR_MESSAGE} from '../constants/constants';
import {ProductListCheckout} from './common/product/ProductListCheckout';
import {PaymentOptions} from './common/payment/PaymentOptions';
import {formatRupiah} from '../helpers/helper';
import {showToast} from '../helpers/toastMessage';
import Bugsnag from '@bugsnag/react-native';

class FinalPayment extends Component {
  state = {
    data: this.props.route.params.data,
    _token: this.props.route.params._token,
    // isChecked: this.props.route.params.data.payment_method_physical[0].pmethod_id,
    // checked: 0,
    // checked: this.props.route.params.digital
    //   ? this.props.route.params.data.payment_method_digital[0].pmethod_id
    //   : this.props.route.params.data.payment_method_physical
    //   ? this.props.route.params.data.payment_method_physical[0].pmethod_id
    //   : this.props.route.params.data.payment_method[0].pmethod_id,
    loading: false,
    changeLoading: false,
    paypalResponse: null,
    paypalAdaptive: null,
    paymentId: null,
    approval_url: null,
    access_token: null,
    type: this.props.route.params.data.type,
    useWallet: false,
    products: [],
    cart_total_price: 0,
    total_amount: 0,
    reward_data_applied: null,
    coupon_value: null,
    isCouponApplied: false,
    coupon_name: null,
  };

  componentDidMount = () => {
    this.onChangeSelectPayment(this.state.checked);
    this.props.getWalletDetails({_token: this.props.route.params._token});
    this.handleFetchProducts();
  };

  handleFetchProducts = () => {
    const {props} = this.props.route.params;
    const {products} = props.cart_items;
    let arr = [];
    arr = Object.values(products);
    this.setState({
      products: arr,
      cart_total_price: props.cart_total_price,
      total_amount: props.net_payable_amount,
      reward_data_applied: props.reward_data_applied,
      coupon_value: parseInt(props.coupon_value),
      isCouponApplied: props.isCouponApplied,
      coupon_name: props.coupon_name,
    });
  };

  onChangeSelectPayment = (value) => {
    this.setState({
      checked: value,
    });
  };

  paypalPayments = async (response) => {
    // console.log('data',this.state.paypalResponse);
    const token_type = response.data.token_type;
    const access_token = response.data.access_token;
    const dataDetail = {
      intent: 'sale',
      payer: {
        payment_method: 'paypal',
      },
      transactions: [
        {
          amount: {
            total: this.state.paypalResponse.payment_amount,
            currency: this.state.paypalResponse.price_currency,
            details: {
              subtotal: this.state.paypalResponse.subtotal,
              tax: this.state.paypalResponse.sales_tax,
              // "shipping": "0.03",
              // "handling_fee": "1.00",
              shipping_discount: -(
                parseFloat(this.state.paypalResponse.order_reward_points) +
                parseFloat(this.state.paypalResponse.order_discount_coupon)
              ),
              // "insurance": "0.01"
            },
          },
          invoice_number: this.state.paypalResponse.invoice_number,
          item_list: {
            items: this.state.paypalResponse.digital_product_details,
          },
        },
      ],
      note_to_payer: 'Contact us for any questions on your order.',
      redirect_urls: {
        return_url: 'https://syoobe.co.id/custom/payment_success',
        cancel_url: 'https://syoobe.co.id/custom/payment_failed',
      },
    };
    await axios
      .post('https://api.sandbox.paypal.com/v1/payments/payment', dataDetail, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: token_type + ' ' + access_token,
          // 'Authorization': `Bearer ${response.data.access_token}`
        },
      })
      .then((response2) => {
        this.setState({
          changeLoading: false,
        });
        console.log('success11', response2);
        const {id, links} = response2.data;
        const approvalUrl = links.find((data) => data.rel == 'approval_url');
        this.setState({
          paymentId: id,
          approval_url: approvalUrl.href,
        });
      })
      .catch((err) => {
        Bugsnag.notify(err);
        console.log('err', {...err});
        this.setState({
          changeLoading: false,
        });
        showToast({
          message: 'Pembayaran gagal!',
        });
      });
  };

  toPayment = (route, params) => {
    this.setState({
      changeLoading: true,
    });
    setTimeout(() => {
      this.props.navigation.replace(route, {
        data: this.props.route.params.data,
        _token: this.state._token,
        ...params,
      });
      this.setState({
        changeLoading: false,
      });
    }, 2000);
  };

  clearCartApi = async () => {
    let details = {
      _token: this.state._token,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios.post(BASE_URL + '/clearCartAPI', formBody).then((response) => {
      console.log('clear', response);
    });
  };

  finalTask = (response) => {
    if (response.data.status == 1) {
      this.clearCartApi();
      if (response.data.order_product_data) {
        this.props.navigation.navigate({
          routeName: 'ThankYou',
          params: {
            orders: response.data.order_product_data,
            _token: this.state._token,
            order_id: this.state.data.order_id,
          },
        });
      }
    } else {
      showToast({
        message: 'Ada yang salah!',
      });
    }
    this.setState({
      changeLoading: false,
    });
  };

  payByCC = async () => {
    this.setState({
      changeLoading: true,
    });
    var details = {
      _token: this.state._token,
      // pmethod_id: this.state.checked,
      amount: this.state.data.amount,
      ptype: this.state.data.type,
      desc: this.state.data.postage_notes,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/addFundPrepaid', formBody)
      .then((response) => {
        this.setState({
          changeLoading: false,
        });
        if (response.data.status == 1) {
          this.props.navigation.navigate('EnterCardDetails', {
            postage_notes: this.state.data.postage_notes,
            order_type: this.state.data.type,
            _token: this.state._token,
            currency: this.state.data.price_currency,
            amount: this.state.data.amount,
            postageDetails: this.props.route.params.postageDetails,
          });
        } else {
          showToast({
            message: ERROR_MESSAGE,
          });
        }
      });
  };

  payByPaypalAdpative = async () => {
    this.setState({
      changeLoading: true,
    });
    var details = {
      _token: this.state._token,
      pmethod_id: this.state.checked,
      amount: this.state.data.amount,
      postage_type: this.state.data.type,
      postage_notes: this.state.data.postage_notes,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/sendDataToPaypalInAddFund', formBody)
      .then((response) => {
        this.setState({
          changeLoading: false,
        });
        if (response.data.status == 1) {
          this.props.navigation.navigate('PaypalView', {
            data: response.data,
            _token: this.state._token,
            postageDetails: this.props.route.params.postageDetails,
          });
        } else {
          showToast({
            message: ERROR_MESSAGE,
          });
        }
      });
  };

  toPostage = () => {
    if (this.state.checked == '7') {
      this.payByPaypalAdpative();
    } else if (this.state.checked == '4') {
      this.payByCC();
    }
  };

  toWallet = () => {
    if (this.state.checked == '7') {
      this.payByPaypalAdpative();
    } else if (this.state.checked == '4') {
      this.payByCC();
    }
  };

  activityIndicatorLoadingView = () => {
    return <Spinner text={'Tunggu sebentar...'} color="red" size="large" />;
  };

  payWithWallet = () => {
    this.setState({
      changeLoading: true,
    });
    var details = {
      _token: this.state._token,
      order_id: this.state.data.order_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios.post(`${BASE_URL}/paymentUsingWallet`, formBody).then((response) => {
      console.log('wallet', JSON.stringify(response));
      this.setState({
        changeLoading: false,
      });
      this.finalTask(response);
    });
  };

  changeWalletPaymentState = () => {
    this.setState((prevState, prevProps) => {
      return {
        useWallet: !prevState.useWallet,
      };
    });
  };

  render() {
    console.log('ini props final payment', JSON.stringify(this.props));
    // console.log('ini state', JSON.stringify(this.state));

    const {loading, products} = this.state;
    if (loading) {
      return <Spinner color="red" size="large" />;
    }
    // console.log(products);

    return (
      <View style={styles.wrapper}>
        <ScrollView>
          <View style={{flex: 1}}>
            <ProductListCheckout items={products} />
          </View>
          <View style={styles.cardHeader}>
            <Text
              style={{
                ...styles.billingAddress,
                paddingBottom: hp(1.5),
                marginTop: 0,
                fontWeight: 'bold',
                fontSize: hp(2),
                // marginBottom:
              }}>
              Rincian Pembayaran
            </Text>
          </View>

          <View style={styles.cardBody}>
            {this.state.reward_data_applied && (
              <View style={styles.cardItem}>
                <Text>Point Hadiah</Text>
                <Text>
                  {'Rp. -'}
                  {formatRupiah(this.state.reward_data_applied)}
                </Text>
              </View>
            )}

            <View style={styles.cardItem}>
              <Text>Total Harga Produk</Text>
              <Text>
                Rp.
                {formatRupiah(this.state.cart_total_price)}
              </Text>
            </View>

            {this.props.route.params.props.tax_data && (
              <View style={styles.cardItem}>
                <Text>Pajak </Text>
                <Text>Rp. {this.props.route.params.props.tax_data?.value}</Text>
              </View>
            )}

            {this.state.isCouponApplied && (
              <View style={styles.cardItem}>
                <Text>Diskon</Text>
                <Text>Rp. -{formatRupiah(this.state.coupon_value)}</Text>
              </View>
            )}

            <View style={styles.cardItem}>
              <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                Total Bayar
              </Text>
              <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                Rp.
                {formatRupiah(this.state.total_amount)}
              </Text>
            </View>
          </View>
        </ScrollView>

        <View style={styles.cardButton}>
          {/* <PaymentOptions
                productData={this.props.shipping.shippingSummary}
                onPressPayment={(route, params) =>
                  this.toPayment(route, params)
                }
                onPress={(route) => this.saveShippingSummary(route)}
                {...this.props}
              /> */}
          <PaymentOptions
            productData={products}
            product_type="D"
            payable_amount={formatRupiah(this.state.total_amount, false)}
            onPressPayment={(route, params) => this.toPayment(route, params)}
            onPress={({ref}) => ref.open()}
            payWithWallet={() => this.payWithWallet()}
            {...this.props}
            // product_type
          />
        </View>
      </View>
    );
  }
}

const styles = {
  cardBody: {
    flex: 1,
    marginBottom: hp(1),
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    paddingVertical: hp(1.5),
  },
  cardItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 5,
  },
  cardButton: {
    // flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
    // marginBottom: hp(2),
    // bottom: 0,
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex: 1,
    marginTop: hp(1),
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    paddingTop: hp(1.5),
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
  },
  labelStyle2: {
    flex: 1,
    fontSize: hp('2%'),
    fontFamily: Font.RobotoBold,
    marginLeft: 10,
  },
  labelStyle3: {
    flex: 1,
    marginLeft: 10,
    color: '#C90205',
    fontSize: hp('2.5%'),
    fontFamily: Font.RobotoBold,
  },
  abc: {
    padding: 10,
    flexDirection: 'row-reverse',
    textAlign: 'left',
    flexWrap: 'wrap',
    width: '100%',
    flexBasis: '100%',
  },
  continueBtn: {
    bottom: 0,
    width: '100%',
    position: 'absolute',
    padding: 20,
    paddingBottom: 10,
    marginBottom: 20,
    paddingLeft: 35,
    paddingRight: 35,
  },
  continueBtn2: {
    width: '100%',
  },
  wrapper: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    height: hp('100%'),
  },
  payment_option: {
    color: '#696969',
    fontSize: hp('2.6%'),
    fontFamily: Font.RobotoRegular,
    marginBottom: hp('1.2%'),
  },
  rightIcon: {
    width: 20,
    height: 20,
  },
  transfar_bank: {
    fontSize: hp('2.2%'),
    color: '#545454',
    marginLeft: 10,
    fontFamily: Font.RobotoRegular,
  },
  totalPriceWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: hp('1.5%'),
    paddingTop: hp('1.5%'),
    // borderBottomWidth:1,
    borderColor: '#e7e7e7',
  },

  totalItem: {
    fontSize: wp('4.2%'),
    color: '#6f6f6f',
    fontFamily: Font.RobotoRegular,
  },
  fontClass: {
    fontSize: hp('2.5%'),
  },
  priceTxt: {
    color: '#00b3ff',
    // fontWeight:'bold',
    fontSize: hp('2.2%'),
    fontFamily: Font.RobotoBold,
  },
  bankDetailTxt: {
    color: '#00b3ff',
    fontFamily: Font.RobotoMedium,
    fontSize: hp('2.1%'),
    marginTop: hp('1.5%'),
    marginBottom: hp('0.5%'),
  },
  accountStyle: {
    color: '#6f6f6f',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.1%'),
  },
  paymentBox: {
    elevation: 1,
    shadowColor: 'transparent',
    borderColor: 'transparent',
    shadowOpacity: 0.2,
    borderWidth: 1,
    padding: 10,
    marginTop: 15,
    marginBottom: hp('3.5%'),
    borderRadius: 2,
  },
  radioWrappper: {
    height: hp('7.2%'),
    flexDirection: 'row',
    paddingTop: 4,
    paddingBottom: 4,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  heading_style: {
    color: '#545454',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.5%'),
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.payment.loading,
    product_type: state.payment.product_type,
    billing: state.payment.billing_address,
    shipping_address: state.payment.shipping_address,
    changeDisplayShip: state.payment.changeDisplayShip,
    changeDisplayBill: state.payment.changeDisplayBill,
    isCouponApplied: state.cart.isCouponApplied,
    coupon_value: state.cart.coupon_value,
    available_reward_point: state.cart.available_reward_point,
    reward_data_applied: state.cart.reward_data,
    rewardsCouldbeApplied: state.cart.cart_max_rewards_points,
    reward_point_message: state.cart.reward_point_message,
    // payable_amount: state.cart.net_payable_amount,
    balance: state.wallet.balance,
    shipping: state.shipping,
  };
};

const dispatchToprops = {
  getWalletDetails,
};

export default connect(mapStateToProps, dispatchToprops)(FinalPayment);

// saveData = async (paypalData) => {
//   this.setState({
//     approval_url: null,
//     changeLoading: true,
//   });
//   var details = {
//     _token: this.state._token,
//     order_id: this.state.data.order_id,
//     transaction_id: paypalData.transactions[0].related_resources[0].sale.id,
//     amount: paypalData.transactions[0].amount.total,
//     status: paypalData.transactions[0].related_resources[0].sale.state,
//     payment_id: paypalData.id,
//   };
//   var formBody = [];
//   for (var property in details) {
//     var encodedKey = encodeURIComponent(property);
//     var encodedValue = encodeURIComponent(details[property]);
//     formBody.push(encodedKey + '=' + encodedValue);
//   }
//   formBody = formBody.join('&');
//   await axios
//     .post(BASE_URL + '/payPalExpressCheckoutAPI', formBody)
//     .then((response) => {
//       this.finalTask(response);
//     });
// };

// _onNavigationStateChange = async (webViewState) => {
//   if (webViewState.url.includes('https://syoobe.co.id')) {
//     const {PayerID} = qs.parse(webViewState.url);
//     axios
//       .post(
//         `https://api.sandbox.paypal.com/v1/payments/payment/${this.state.paymentId}/execute`,
//         {payer_id: PayerID},
//         {
//           headers: {
//             'Content-Type': 'application/json',
//             Authorization: `Bearer ${this.state.access_token}`,
//           },
//         },
//       )
//       .then((response) => {
//         // console.log('final response', response);
//         if (response.data.state == 'approved') {
//           this.saveData(response.data);
//         }
//       })
//       .catch((err) => {
//         console.log({...err});
//       });
//   }
// };

// intoPayapl = async () => {
//   // const auth_code =
//   //     'Basic QWZmLUd2S2hMZVMyUkFydGpMX1RDMFlFR1E3VTNxU3c0enppOHFmcnIyZmNJbXZFcTM1azVmUnZXTkthYlI0T2RpZzdaN0YwSnA3blJ5aWc6RU1pQWF3alREV2d1UmtSRVAxSGhWYW44bzNtcjlqaVplR3B0OFhTUmc0dERlMURHVW9xMENmVk9icWQ1dlJnZ2pIQUNCSmNTdy1zWEdEdm0=';
//   const auth_code =
//     'Basic QWY2dExnRHJlem96ZHVCbjZMXzQ0eVh4V0lNZ3NNY1BtaExIR1p5M2VNWHBVdmh4Vlp5dUdIdjI3NEItUFZ0anN0LWlCNWlSZDFOSDhEQ206RU42eEZGdEdaLWpOZ2tHQ3FFaHhCZ3NaRWJYZkN0WWYzWGFTVjlEdHMydm5vZV9rQjZ6clBIVzJZckJxbDNrZkQ2ZXFoZlBZdVdLbnN1Zk0=';
//   await axios
//     .post(
//       'https://api.sandbox.paypal.com/v1/oauth2/token',
//       qs.stringify({grant_type: 'client_credentials'}),
//       {
//         headers: {
//           'Content-Type': 'application/x-www-form-urlencoded',
//           Authorization: auth_code,
//         },
//       },
//     )
//     .then((response) => {
//       this.setState({
//         access_token: response.data.access_token,
//       });
//       this.paypalPayments(response);
//     })
//     .catch((err) => {
//       console.log({...err});
//       this.setState({
//         changeLoading: false,
//       });
//     });
// };
