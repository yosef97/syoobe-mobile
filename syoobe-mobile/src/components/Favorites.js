import React, {Component} from 'react';
import {
  ToastAndroid,
  StatusBar,
  AsyncStorage,
  Text,
  Image,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {ProductCard} from './product';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {getFavourites} from '../actions';
import {Spinner} from './common';
import axios from 'axios';
import {Font} from './Font';
import FavouriteRightMenu from './FavouriteRightMenu';
import Modal from './Modal';
import CheckBoxComponent from './CheckBoxComponent';
import {BASE_URL} from '../services/baseUrl';
import {formatRupiah} from '../helpers/helper';

class Favorites extends Component {
  // static navigationOptions = {
  //     drawerLabel: 'Favorites',
  //     drawerIcon: () => (
  //       <Image
  //         source={require('../images/favourite-icon.png')}
  //         style={{width: wp('6%'), height: hp('3%')}}
  //       />
  //     )
  // }props.route.param
  constructor(props) {
    super(props);
    this.checkBoxRef = React.createRef();
  }

  state = {
    others_fav_physical: [],
    others_fav_digital: [],
    diff_user: false,
    loader: false,
    _token: null,
    user_name: '',
    list_id: this.props.route.params.list_id,
    showBox: false,
    selectedItemMenu: null,
    showModal: false,
    loadingModal: false,
    favItemList: [],
  };

  static navigationOptions = ({navigation}) => ({
    drawerLabel: 'Favorit',
    drawerIcon: () => (
      <Image
        source={require('../images/favourite-icon.png')}
        style={{width: wp('4.3%'), height: hp('2.2%')}}
      />
    ),
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
    headerTitle: (
      <Text style={{fontSize: hp('2.5%'), color: '#fff'}}>Favorit</Text>
    ),
  });

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  getDetails = async () => {
    const {list_id} = this.state;
    if (this.props.route.params.user_id != undefined) {
      this.setState({
        diff_user: true,
        loader: true,
        user_name: this.props.route.params.user_name,
      });
      await this.retrieveToken().then((_token) => {
        var details = {};
        details = {
          ...details,
          ...{_token: _token},
          ...details,
          ...{user_id: this.props.route.params.user_id},
        };
        var formBody = [];
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        axios
          .post(BASE_URL + '/favouriteItemForFavouriteuser', formBody)
          .then((response) => {
            if (response.data.favourite_item_details instanceof Array) {
              var phy = [];
              var dig = [];
              response.data.favourite_item_details.forEach((fav) => {
                if (fav.prod_requires_shipping == 1) {
                  phy.push(fav);
                } else if (fav.prod_requires_shipping == 0) {
                  dig.push(fav);
                }
              });
              this.setState({
                others_fav_physical: phy,
                others_fav_digital: dig,
                loader: false,
              });
            } else {
              this.setState({
                loader: false,
              });
            }
          })
          .catch((err) => {
            this.setState({
              loader: false,
            });
            console.log(err);
          });
      });
    } else {
      await this.retrieveToken().then((_token) => {
        this.props.getFavourites(_token, list_id);
      });
    }
  };

  componentDidMount = async () => {
    this.getDetails();
  };

  goToProduct = (favourite) => {
    this.setState({
      showBox: false,
    });
    this.props.navigation.push('ProductDetail', {
      product_id: favourite.product_id,
      product_requires_shipping: favourite.prod_requires_shipping,
    });
  };

  toggleBoxHandler = (id) => {
    if (id == this.state.selectedItemMenu && this.state.showBox) {
      this.setState({
        showBox: false,
      });
    } else {
      this.setState({
        showBox: true,
        selectedItemMenu: id,
      });
    }
  };

  renderRightIcon = (id) => {
    return (
      <TouchableOpacity
        style={{
          height: hp('16%'),
          width: wp('8%'),
          position: 'absolute',
          right: -8,
          top: 10,
        }}
        onPress={() => this.toggleBoxHandler(id)}>
        <Image
          style={styles.rgtMenuIcon}
          resizeMode="contain"
          source={require('../images/rgtMenuIcon.png')}
        />
      </TouchableOpacity>
    );
  };

  removeFromListHandler = () => {
    const {list_id} = this.state;
    this.setState({
      loader: true,
    });
    var details = {
      _token: this.state._token,
      product_id: this.state.selectedItemMenu,
    };
    if (list_id) {
      details = {
        ...details,
        ...{
          list_id: list_id,
        },
      };
    }
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post(
        BASE_URL +
          `/${
            list_id ? 'removeItemFromFavList' : 'deleteItemFromDefaultFavList'
          }`,
        formBody,
      )
      .then((response) => {
        this.setState({
          loader: false,
        });
        if (response.data.status == 1) {
          this.setState({
            showBox: false,
          });
          ToastAndroid.showWithGravityAndOffset(
            'Dihapus!',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          );
          this.getDetails();
          this.props.route.params.list();
        } else {
          ToastAndroid.showWithGravityAndOffset(
            'Penghapusan gagal!',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
          );
        }
      });
  };

  addToListHandler = () => {
    this.changeModalStateHandler();
    this.loadingModalHandler();
    var details = {
      _token: this.state._token,
      product_id: this.state.selectedItemMenu,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post(BASE_URL + '/displayUserCreatedListOnRedLine', formBody)
      .then((response) => {
        console.log('add', response);
        this.loadingModalHandler();
        if (response.data.status == 1) {
          this.setState({
            showBox: false,
            favItemList: response.data.user_created_fav_list,
          });
        } else {
          this.setState({showModal: false});
          ToastAndroid.showWithGravityAndOffset(
            'Tidak ada daftar lain yang hadir!',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
            25,
            50,
          );
        }
      });
  };

  renderRightIconContent = (id) => {
    if (this.state.showBox && id == this.state.selectedItemMenu) {
      return (
        <FavouriteRightMenu
          addToList={this.addToListHandler}
          removeFromList={this.removeFromListHandler}
        />
      );
    }
  };

  renderFavouriteDigitalProducts = (val) => {
    if (val == 'same') {
      return this.props.digital_favourites.map((favourite) => (
        <TouchableOpacity
          onPress={() => this.goToProduct(favourite)}
          key={favourite.product_id}
          style={styles.viewStyle}>
          {this.renderRightIcon(favourite.product_id)}
          {this.renderRightIconContent(favourite.product_id)}
          <Image
            resizeMode={'contain'}
            style={styles.product_img}
            source={{uri: favourite.product_image}}
          />
          <Text style={styles.product_text}>{favourite.product_name}</Text>
          <Text style={styles.product_price}>
            {'Rp. '}
            {formatRupiah(favourite.product_price)}
          </Text>
        </TouchableOpacity>
      ));
    } else if (val == 'diff') {
      return this.state.others_fav_digital.map((favourite) => (
        <TouchableOpacity
          onPress={() => this.goToProduct(favourite)}
          key={favourite.product_id}
          style={styles.viewStyle}>
          {/* {this.renderRightIcon(favourite.product_id)}
                    {this.renderRightIconContent(favourite.product_id)} */}
          <Image
            resizeMode={'contain'}
            style={styles.product_img}
            source={{uri: favourite.product_image}}
          />
          <Text style={styles.product_text}>{favourite.product_name}</Text>
          <Text style={styles.product_price}>
            {'Rp. '}
            {formatRupiah(favourite.product_price)}
          </Text>
        </TouchableOpacity>
      ));
    }
  };

  renderFavouritePhysicalProducts = (val) => {
    if (val == 'same') {
      return this.props.physical_favourites.map((favourite) => (
        <TouchableOpacity
          onPress={() => this.goToProduct(favourite)}
          key={favourite.product_id}
          style={styles.viewStyle}>
          {this.renderRightIcon(favourite.product_id)}
          {this.renderRightIconContent(favourite.product_id)}
          <Image
            resizeMode={'contain'}
            style={styles.product_img}
            source={{uri: favourite.product_image}}
          />
          <Text style={styles.product_text}>{favourite.product_name}</Text>
          <Text style={styles.product_price}>
            {'Rp. '}
            {formatRupiah(favourite.product_price)}
          </Text>
        </TouchableOpacity>
      ));
    } else if (val == 'diff') {
      return this.state.others_fav_physical.map((favourite) => (
        <TouchableOpacity
          onPress={() => this.goToProduct(favourite)}
          key={favourite.product_id}
          style={styles.viewStyle}>
          {/* {this.renderRightIcon(favourite.product_id)}
                    {this.renderRightIconContent(favourite.product_id)} */}
          <Image
            resizeMode={'contain'}
            style={styles.product_img}
            source={{uri: favourite.product_image}}
          />
          <Text style={styles.product_text}>{favourite.product_name}</Text>
          <Text style={styles.product_price}>
            {'Rp. '}
            {formatRupiah(favourite.product_price)}
          </Text>
        </TouchableOpacity>
      ));
    }
  };

  changeModalStateHandler = () => {
    this.setState((prevState, prevProps) => {
      return {
        showModal: !prevState.showModal,
      };
    });
  };

  loadingModalHandler = () => {
    this.setState((prevState, prevProps) => {
      return {
        loadingModal: !prevState.loadingModal,
      };
    });
  };

  changeStateApi = async (val, list_id) => {
    this.loadingModalHandler();
    var details = {
      _token: this.state._token,
      list_id: list_id,
      prod: this.state.selectedItemMenu,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/markFavOrUnFavInCheckbox', formBody)
      .then((response) => {
        console.log('done', response);
        this.loadingModalHandler();
        if (response.data.status == 1) {
          this.changeCheckState(val, list_id);
          this.getDetails();
          this.props.route.params.list();
          if (val) {
            ToastAndroid.showWithGravityAndOffset(
              'Ditambahkan!',
              ToastAndroid.SHORT,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
          } else {
            ToastAndroid.showWithGravityAndOffset(
              'Dihapus!',
              ToastAndroid.SHORT,
              ToastAndroid.BOTTOM,
              25,
              50,
            );
          }
        } else {
          ToastAndroid.showWithGravityAndOffset(
            response.data.msg,
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
            25,
            50,
          );
        }
      });
  };

  changeCheckState = (val, list_id) => {
    const {favItemList} = this.state;
    let newFav = [...favItemList];
    newFav.every((item) => {
      if (item.list_id == list_id) {
        item.already_checked = val;
        return false;
      } else {
        return true;
      }
    });
    this.setState({
      favItemList: newFav,
    });
  };

  render() {
    const {containerStyle, viewStyle2, newArrival} = styles;
    if (!this.state.diff_user) {
      if (this.props.loading || this.state.loader) {
        return (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <StatusBar backgroundColor="#C90205" barStyle="light-content" />
            <Spinner color="red" size="large" />
          </View>
        );
      }

      if (
        this.props.digital_favourites.length < 1 &&
        this.props.physical_favourites.length < 1
      ) {
        return (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
              width: '100%',
            }}>
            <StatusBar backgroundColor="#C90205" barStyle="light-content" />
            <Text
              style={{
                textAlign: 'center',
                fontSize: hp('2%'),
                color: '#000',
              }}>
              Tidak ada item
            </Text>
          </View>
        );
      }

      return (
        <View
          onStartShouldSetResponder={(event) =>
            this.setState({
              showBox: false,
            })
          }
          style={containerStyle}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Modal
            loadingModal={this.state.loadingModal}
            changeModalState={this.changeModalStateHandler}
            showModalState={this.state.showModal}>
            <View>
              {this.state.favItemList.map((item, index) => (
                <View style={styles.abcModal} key={item.list_id + index}>
                  <Text style={styles.labelStyle2}>{item.list_name}</Text>
                  <CheckBoxComponent
                    ref={this.checkBoxRef}
                    checked={item.already_checked}
                    onChange={(val) => this.changeStateApi(val, item.list_id)}
                    checkedColor="#15b9ff"
                    uncheckedColor="rgba(0,0,0,0.5)"
                    labelposition="left"
                    iconSize={25}
                    iconName="iosFill"
                  />
                </View>
              ))}
            </View>
          </Modal>
          <ScrollView>
            <ProductCard>
              {this.props.listname && (
                <View
                  style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      color: '#ff3d5b',
                      fontFamily: Font.RobotoRegular,
                      fontSize: hp('2.3%'),
                      paddingTop: 10,
                    }}>
                    {this.props.listname}
                  </Text>
                  <Text
                    onPress={() =>
                      this.props.route.params.deleteList(this.state.list_id)
                    }
                    style={{
                      color: '#696969',
                      fontFamily: Font.RobotoLight,
                      fontSize: hp('2.3%'),
                      paddingTop: 10,
                    }}>
                    Hapus Daftar
                  </Text>
                </View>
              )}
              {this.props.digital_favourites.length > 0 ? (
                <View>
                  <View style={viewStyle2}>
                    <Text style={newArrival}>PRODUK DIGITAL</Text>
                  </View>
                  <View style={styles.wrapper_Style}>
                    {this.renderFavouriteDigitalProducts('same')}
                  </View>
                </View>
              ) : null}
              {this.props.physical_favourites.length > 0 ? (
                <View>
                  <View style={viewStyle2}>
                    <Text style={newArrival}>PRODUK FISIK</Text>
                  </View>
                  <View style={styles.wrapper_Style}>
                    {this.renderFavouritePhysicalProducts('same')}
                  </View>
                </View>
              ) : null}
            </ProductCard>
          </ScrollView>
        </View>
      );
    }
    if (this.state.diff_user) {
      if (this.state.loader) {
        return (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Spinner color="red" size="large" />
          </View>
        );
      }
      if (
        this.state.others_fav_digital.length < 1 &&
        this.state.others_fav_physical.length < 1
      ) {
        return (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
              width: '100%',
            }}>
            <Text
              style={{
                textAlign: 'center',
                fontSize: hp('2.3%'),
                color: '#000',
              }}>
              {this.state.user_name} tidak punya barang favorit!
            </Text>
          </View>
        );
      }
      return (
        <View
          onStartShouldSetResponder={(event) =>
            this.setState({
              showBox: false,
            })
          }
          style={containerStyle}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <ScrollView>
            <ProductCard>
              {this.state.user_name != '' && (
                <View
                  style={{
                    justifyContent: 'center',
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      color: '#ff3d5b',
                      fontFamily: Font.RobotoRegular,
                      fontSize: hp('2.3%'),
                      paddingTop: 10,
                    }}>
                    {this.state.user_name + 's favorit'}
                  </Text>
                </View>
              )}
              {this.state.others_fav_digital.length > 0 ? (
                <View>
                  <View style={viewStyle2}>
                    <Text style={newArrival}>PRODUK DIGITAL</Text>
                  </View>
                  <View style={styles.wrapper_Style}>
                    {this.renderFavouriteDigitalProducts('diff')}
                  </View>
                </View>
              ) : null}
              {this.state.others_fav_physical.length > 0 ? (
                <View>
                  <View style={viewStyle2}>
                    <Text style={newArrival}>PRODUK FISIK</Text>
                  </View>
                  <View style={styles.wrapper_Style}>
                    {this.renderFavouritePhysicalProducts('diff')}
                  </View>
                </View>
              ) : null}
            </ProductCard>
          </ScrollView>
        </View>
      );
    }
  }
}

const styles = {
  rgtMenuIcon: {
    height: hp('4%'),
    width: wp('2%'),
  },
  abc: {
    overflow: 'hidden',
  },
  labelStyle2: {
    flex: 1,
    fontSize: hp('2%'),
    fontFamily: Font.RobotoBold,
    marginLeft: 10,
  },
  abcModal: {
    padding: 10,
    flexDirection: 'row-reverse',
    textAlign: 'left',
    width: '100%',
    justifyContent: 'space-between',
    backgroundColor: '#F5F5F5',
    margin: 2,
    borderRadius: 10,
    elevation: 1,
  },
  containerStyle: {
    backgroundColor: '#fff',
    width: '100%',
    height: '100%',
  },
  banner_top: {
    Width: '100%',
  },
  heading: {
    borderWidth: 1,
    borderColor: '#f00',
    height: 80,
    width: '100%',
  },
  bannerImg: {
    width: '100%',
    // height:150,
    height: hp('25%'),
  },
  bottomSection: {
    borderColor: '#e7e7e7',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 30,
  },
  section1: {
    borderColor: '#f00',
    borderRightWidth: 1,
    textAlign: 'center',
    width: '33%',
  },
  buy_btn: {},
  wrapper_Style: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
  },

  viewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    width: '50%',
    bottom: 0,
    right: 0,
    color: 'ffffff',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
  },
  product_img: {
    // width:65,
    // height:95,
    width: wp('20%'),
    height: hp('15%'),
  },

  product_text: {
    color: '#545454',
    // fontSize:16  ,
    fontSize: wp('3%'),
    width: '100%',
    textAlign: 'center',
    marginTop: wp('2%'),
    // marginBottom:15,
    marginBottom: wp('2%'),
  },
  product_price: {
    color: '#00b3ff',
    // fontSize:18,
    fontWeight: 'bold',
    marginBottom: wp('2%'),
    fontSize: hp('1.25%'),
  },
  like_icon: {
    position: 'absolute',
    top: 12,
    right: 12,
    // width:26,
    // height:24,
    width: wp('5%'),
    height: hp('2.25%'),
  },
  buy_btn: {
    // width:90,
    // height:26.5,
    width: wp('22%'),
    height: hp('3.25%'),
    borderColor: '#c7c7c7',
    borderWidth: 1,
    borderRadius: 50,
    textAlign: 'center',
    color: '#c90305',
    fontSize: hp('1.5%'),
    lineHeight: wp('6%'),
    // width:wp('30%'),
    // height:wp('10%')
  },

  viewStyle2: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
  },
  newArrival: {
    color: '#696969',
    // fontSize:20,
    fontSize: hp('1.5%'),
    textTransform: 'uppercase',
  },
  viewAll: {
    color: '#c90305',
    // fontSize:16,
    fontSize: hp('1.5%'),
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.fav.loading,
    physical_favourites: state.fav.physical_favourites,
    digital_favourites: state.fav.digital_favourites,
    listname: state.fav.listname,
  };
};

export default connect(mapStateToProps, {getFavourites})(Favorites);
