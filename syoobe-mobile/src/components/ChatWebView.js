import React, {Component} from 'react';
import {AsyncStorage} from 'react-native';
import {WebView} from 'react-native-webview';
import base64 from 'react-native-base64';

class ChatWebView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      needToDecode: '',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('needToDecode').then((res) =>
      this.setState({needToDecode: res}),
    );

    AsyncStorage.getItem('emailStore').then((res) =>
      this.setState({email: res}),
    );

    AsyncStorage.getItem('passwordStore').then((res) =>
      this.setState({password: res}),
    );
  }

  render() {
    console.log(this.state.needToDecode);
    console.log('ini webview', this.state.email, this.state.password);
    console.log(
      'ini decode',
      base64.decode('ZGE3ZTk3OGIwYTM3ODI2ZjQwNTYxMGVkYzY0ZmI5Y2I='),
    );

    const passwordDecode = base64.decode(this.state.password);

    return (
      <>
        {this.state.needToDecode === 'true' ? (
          <WebView
            source={{
              uri: `https://syoobe.co.id/pesan/?email=${this.state.email}&password=${passwordDecode}`,
              // uri: 'https://syoobe.co.id/pesan/back-to-login'
            }}
          />
        ) : (
          <WebView
            source={{
              uri: `https://syoobe.co.id/pesan/?email=${this.state.email}&password=${this.state.password}`,
              // uri: 'https://syoobe.co.id/pesan/back-to-login'
            }}
          />
        )}
      </>
    );
  }
}

export default ChatWebView;
