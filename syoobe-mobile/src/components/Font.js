export const Font = {
  RobotoRegular: 'Roboto-Regular',
  RobotoMedium: 'Roboto-Medium',
  RobotoBold: 'Roboto-Bold',
  RobotoBlack: 'Roboto-Black',
  RobotoLight: 'Roboto-Light',
  RobotoThin: 'Roboto-Thin',
};
