import React, {Fragment, Component} from 'react';
import {
  TouchableWithoutFeedback,
  TextInput,
  Modal,
  LayoutAnimation,
  Platform,
  UIManager,
  StyleSheet,
  AsyncStorage,
  StatusBar,
  Text,
  Image,
  Button,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {Font} from './Font';
import axios from 'axios';
import PostageAccountListComponent from './PostageAccountListComponent';
import {Spinner} from 'native-base';
import FlashMessage, {showMessage} from 'react-native-flash-message';
import {showToast} from '../helpers/toastMessage';
import {ERROR_MESSAGE} from '../constants/constants';

class PostageAccountHistory extends Component {
  static navigationOptions = {
    drawerLabel: 'Saldo ongkir',
    drawerIcon: () => (
      <Image
        source={require('../images/postageAccount.png')}
        style={{width: wp('4%'), height: hp('2.5%')}}
      />
    ),
  };
  constructor() {
    super();
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }
  state = {
    loading: false,
    _token: null,
    current_page: 1,
    transaction_details_list: [],
    latest_invoice_balance: null,
    prepaid_balance: null,
    price_currency: null,
    expanded: false,
    clicked: null,
    willShowModal: false,
    inputError: false,
    amount: '',
    total_wallet_balance: null,
    messageIconType: null,
    pdfUrl: null,
    modalType: 'transfer',
    loadingModal: false,
    ShowLabelNotificationData: [],
    tn_id: null,
    pagesShownArray: [],
    total_pages: 1,
    first_page: null,
    last_page: null,
    current_page: 1,
  };

  changeLayout = (index) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState((prevState, prevProps) => {
      return {
        expanded: !prevState.expanded,
        clicked: index,
      };
    });
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = () => {
    this.retrieveToken().then((_token) => {
      this.fetchData();
    });
  };

  fetchData = () => {
    this.setState({
      loading: true,
    });
    details = {
      _token: this.state._token,
      page: this.state.current_page,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/myPostageAccountListing', formBody)
      .then((response) => {
        const {
          total_page,
          total_wallet_balance,
          price_currency,
          prepaid_balance,
          latest_invoice_balance,
          transaction_details_list,
        } = response.data;
        console.log('data', response);
        if (response.data.status == 1) {
          this.setState({
            transaction_details_list,
            latest_invoice_balance,
            prepaid_balance,
            price_currency,
            total_wallet_balance,
            total_pages: total_page,
          });
          this.callSetShownArray();
        }
        this.setState({
          latest_invoice_balance,
          prepaid_balance,
          price_currency,
          total_wallet_balance,
          total_pages: total_page,
        });
        this.setState({
          loading: false,
        });
      });
  };

  transferFromWallet = () => {
    if (this.state.total_wallet_balance) {
      this.setState({
        modalType: 'transfer',
      });
      this.changeModalState();
    } else {
      showToast({
        message: 'Saldo Dompet Tidak Cukup!',
      });
    }
  };

  comfirmTransfer = () => {
    const {total_wallet_balance, _token, amount} = this.state;
    if (
      amount.toString().trim() == '' ||
      parseFloat(amount) < 10 ||
      parseFloat(amount) > total_wallet_balance
    ) {
      this.setState({
        inputError: true,
      });
      showToast({
        message: 'Jumlah tidak valid!',
      });
    } else {
      this.changeModalState();
      this.setState({
        loading: true,
        inputError: false,
        amount: '',
      });
      details = {
        _token: _token,
        transfer_amount: amount,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      axios
        .post('https://syoobe.co.id/api/transferFundfromWallet', formBody)
        .then((response) => {
          this.setState({
            loading: false,
          });
          console.log('data1', response);
          if (response.data.status == 1) {
            this.setState({
              messageIconType: 'success',
            });
            showMessage({
              message: 'Keberhasilan',
              description: `Jumlah yang ditransfer!`,
              type: 'success',
            });
            this.fetchData();
          } else {
            this.setState({
              messageIconType: 'danger',
            });
            showMessage({
              message: 'Kesalahan',
              description: `Transfer gagal!`,
              type: 'danger',
            });
            this.changeModalState();
          }
        });
    }
  };

  addFundsToPostage = () => {
    this.props.navigation.navigate('AddFunds', {
      checked: 'postage',
      postageDetails: this.fetchData,
    });
  };

  reprint = async (id) => {
    this.setState({
      loading: true,
    });
    details = {
      _token: this.state._token,
      tnd_id: id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/reprintLableAPI', formBody)
      .then((response) => {
        console.log('data', response);
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          this.setState({
            pdfUrl: response.data.image_link,
          });
          this.props.navigation.navigate('PdfViewer', {
            pdf: this.state.pdfUrl,
            type: 'url',
          });
        }
      });
  };

  PrintReturnLabel = async (tnd_id, tnd_opr_id) => {
    this.setState({
      loading: true,
    });
    details = {
      _token: this.state._token,
      tnd_id: tnd_id,
      opr_id: tnd_opr_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/PrintReturnLabel', formBody)
      .then((response) => {
        console.log('data', response);
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          this.setState({
            pdfUrl: response.data.image_link,
          });
          this.props.navigation.navigate('PdfViewer', {
            pdf: this.state.pdfUrl,
            type: 'url',
          });
        }
      });
  };

  printReturnShippingCenterLabel = async (tnd_id, tmu_id) => {
    this.setState({
      loading: true,
    });
    details = {
      _token: this.state._token,
      tnd_id: tnd_id,
      tmu_id: tmu_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/printReturnShippingCenterLabel', formBody)
      .then((response) => {
        console.log('data', response);
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          this.setState({
            pdfUrl: response.data.image_link,
          });
          this.props.navigation.navigate('PdfViewer', {
            pdf: this.state.pdfUrl,
            type: 'url',
          });
        } else {
          showToast({
            message: response.data.msg,
          });
        }
      });
  };

  reprint_return_label = async (id) => {
    this.setState({
      loading: true,
    });
    details = {
      _token: this.state._token,
      tnd_id: id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/rePrintAReturnLabel', formBody)
      .then((response) => {
        console.log('data', response);
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          this.setState({
            pdfUrl: response.data.image_link,
          });
          this.props.navigation.navigate('PdfViewer', {
            pdf: this.state.pdfUrl,
            type: 'url',
          });
        }
      });
  };

  showLabeldata = async (id) => {
    this.setState({
      loadingModal: true,
      tn_id: id,
    });
    details = {
      _token: this.state._token,
      tnd_id: id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(
        'https://syoobe.co.id/api/sendLabelNotificationDisplayAPI',
        formBody,
      )
      .then((response) => {
        console.log('data', response);
        this.setState({
          loadingModal: false,
        });
        if (response.data.status == 1) {
          this.setState({
            ShowLabelNotificationData: response.data.sendData,
          });
        }
      });
  };

  sendLabelNotification = async (value) => {
    await this.setState({
      modalType: 'labelNot',
    });
    this.changeModalState();
    this.showLabeldata(value);
  };

  changeModalState = () => {
    this.setState((prevState, prevProps) => {
      return {
        willShowModal: !prevState.willShowModal,
        inputError: false,
        amount: '',
        tn_id: null,
      };
    });
  };

  sendMail = async () => {
    this.setState({
      loadingModal: true,
    });
    details = {
      _token: this.state._token,
      tnd_id: this.state.tn_id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(
        'https://syoobe.co.id/api/sentEmailForSendLabelNotification',
        formBody,
      )
      .then((response) => {
        this.setState({
          loadingModal: false,
        });
        if (response.data.status == 1) {
          this.changeModalState();
          this.fetchData();
          this.setState({
            messageIconType: 'success',
          });
          showMessage({
            message: 'Keberhasilan',
            description: `Pemberitahuan terkirim!`,
            type: 'success',
          });
        } else {
          this.setState({
            messageIconType: 'danger',
          });
          showMessage({
            message: 'Kesalahan',
            description: ERROR_MESSAGE,
            type: 'danger',
          });
        }
      });
  };

  goRight = () => {
    const {pagesShownArray} = this.state;
    this.setShownArray(
      pagesShownArray[0] + 1,
      pagesShownArray[pagesShownArray.length - 1] + 1,
    );
  };

  goLeft = () => {
    const {pagesShownArray} = this.state;
    this.setShownArray(
      pagesShownArray[0] - 1,
      pagesShownArray[pagesShownArray.length - 1] - 1,
    );
  };

  callSetShownArray = () => {
    const {total_pages, pagesShownArray} = this.state;
    if (pagesShownArray.length > 0) {
      this.setShownArray(
        pagesShownArray[0],
        pagesShownArray[pagesShownArray.length - 1],
      );
    } else {
      this.setShownArray(1, total_pages < 4 ? total_pages : 4);
    }
  };

  setShownArray = async (first, last) => {
    const {total_pages} = this.state;
    if (first > 0 && last <= total_pages) {
      let array = [];
      for (let i = first; i <= last; i++) {
        array.push(i);
      }
      await this.setState({
        pagesShownArray: [...array],
        first_page: first,
        last_page: last,
      });
    }
  };

  selectPage = async (val) => {
    await this.setState({
      current_page: val,
    });
    this.fetchData();
  };

  render() {
    //styles
    const {
      textStyle,
      selectedIndex,
      paginationViewNumber,
      paginationStyles,
      walletBalanceStyle,
      textColorRed,
      availableBalanceText,
      textInputStyle,
      textInputStyleError,
      bottonWrapperView,
      outerModal,
      innerModal,
      modalHeadingStyle,
      centerStyle,
    } = styles;

    //state
    const {
      total_pages,
      first_page,
      last_page,
      current_page,
      pagesShownArray,
      ShowLabelNotificationData,
      loadingModal,
      modalType,
      messageIconType,
      total_wallet_balance,
      inputError,
      loading,
      clicked,
      price_currency,
      prepaid_balance,
      transaction_details_list,
      latest_invoice_balance,
    } = this.state;

    //combine style
    const combineStyles = StyleSheet.flatten([
      paginationViewNumber,
      selectedIndex,
    ]);
    return (
      <Fragment>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          style={styles.scrollView}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.willShowModal}
            onRequestClose={this.changeModalState}>
            <TouchableWithoutFeedback onPress={this.changeModalState}>
              <View style={outerModal}>
                {modalType == 'transfer' && (
                  <View style={innerModal}>
                    <Text style={modalHeadingStyle}>
                      {' '}
                      Enter amount to transfer{' '}
                    </Text>
                    <View style={walletBalanceStyle}>
                      <Text style={availableBalanceText}>
                        {' '}
                        Available Wallet Balance:{' '}
                      </Text>
                      <Text style={textColorRed}>
                        {' '}
                        {price_currency} {total_wallet_balance}
                      </Text>
                    </View>
                    <View
                      style={{
                        margin: 20,
                      }}>
                      <TextInput
                        style={
                          !inputError ? textInputStyle : textInputStyleError
                        }
                        onChangeText={(amount) => this.setState({amount})}
                        value={this.state.amount}
                        placeholder={'Masukkan Nilai'}
                        keyboardType={'numeric'}
                      />
                    </View>
                    <View style={bottonWrapperView}>
                      <Button
                        color={'#C90205'}
                        title={'Membatalkan'}
                        onPress={this.changeModalState}
                      />
                      <Button
                        title={'Transfer'}
                        onPress={this.comfirmTransfer}
                      />
                    </View>
                  </View>
                )}
                {modalType == 'labelNot' && (
                  <View style={innerModal}>
                    {loadingModal ? (
                      <Spinner text={'Memuat...'} color={'red'} />
                    ) : (
                      <Fragment>
                        {ShowLabelNotificationData.map((data, index) => (
                          <Fragment key={index}>
                            <Text style={modalHeadingStyle}>
                              Detail Pengiriman
                            </Text>
                            <View
                              style={{
                                padding: 5,
                                alignItems: 'flex-start',
                              }}
                              key={index}>
                              <View style={walletBalanceStyle}>
                                <Text style={availableBalanceText}>
                                  {' '}
                                  Penyedia Pengiriman:{' '}
                                </Text>
                                <Text style={textColorRed}>
                                  {' '}
                                  {data.provider}
                                </Text>
                              </View>
                              <View style={walletBalanceStyle}>
                                <Text style={availableBalanceText}>
                                  {' '}
                                  Nama penerima:{' '}
                                </Text>
                                <Text style={textColorRed}> {data.name}</Text>
                              </View>
                              <View style={walletBalanceStyle}>
                                <Text style={availableBalanceText}>
                                  {' '}
                                  Penerima e-mail:{' '}
                                </Text>
                                <Text style={textColorRed}> {data.email}</Text>
                              </View>
                              <View style={walletBalanceStyle}>
                                <Text style={availableBalanceText}>
                                  {' '}
                                  Tanggal Permintaan:{' '}
                                </Text>
                                <Text style={textColorRed}> {data.date}</Text>
                              </View>
                              <View style={walletBalanceStyle}>
                                <Text style={availableBalanceText}>
                                  {' '}
                                  URL pelacakan:{' '}
                                </Text>
                                <Text style={textColorRed}> {data.url}</Text>
                              </View>
                            </View>
                            <View style={bottonWrapperView}>
                              <Button
                                color={'#C90205'}
                                title={'Membatalkan'}
                                onPress={this.changeModalState}
                              />
                              <Button
                                title={'Kirim Surat'}
                                onPress={this.sendMail}
                              />
                            </View>
                          </Fragment>
                        ))}
                      </Fragment>
                    )}
                  </View>
                )}
              </View>
            </TouchableWithoutFeedback>
          </Modal>
          {loading ? (
            <View style={centerStyle}>
              <Spinner text={'Loading...'} color={'red'} />
            </View>
          ) : (
            <Fragment>
              <Text style={styles.postageTxt}>Akun Ongkos Kirim</Text>
              <View style={styles.balancDivWrapper}>
                <View style={styles.balanceDiv}>
                  {prepaid_balance != null && (
                    <View style={[styles.balanceInner, styles.backgroundColor]}>
                      <Text style={styles.balanceTxt}>
                        Saldo dibayar dimuka
                      </Text>
                      <Text style={styles.balance}>
                        {price_currency}
                        {prepaid_balance}
                      </Text>
                    </View>
                  )}
                  {latest_invoice_balance != null && (
                    <View style={[styles.balanceInner, styles.backgroundColor]}>
                      <Text style={styles.balanceTxt}>
                        Saldo Faktur Terbaru
                      </Text>
                      <Text style={styles.balance}>
                        {price_currency}
                        {latest_invoice_balance}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={styles.balanceDiv}>
                  <Button
                    color={'#C90205'}
                    title={'Transfer dari dompet'}
                    onPress={() => this.transferFromWallet()}
                  />
                  <Button
                    color={'#C90205'}
                    title={'Tambahkan Dana'}
                    onPress={() => this.addFundsToPostage()}
                  />
                </View>
              </View>
              <View style={styles.touchStyle}>
                <Text style={styles.headerText}>Tanggal</Text>
                <Text style={styles.headerText}>Detail</Text>
              </View>
              {transaction_details_list.map((detail, index) => (
                <Fragment key={index}>
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => this.changeLayout(index)}
                    style={styles.touchStyle}>
                    <Text style={styles.rgtTxt}>{detail.date}</Text>
                    {this.state.expanded && index == clicked ? (
                      <Image
                        style={styles.imageStyle}
                        resizeMode={'contain'}
                        source={require('../images/arrowDown.png')}
                      />
                    ) : (
                      <Image
                        style={styles.imageStyle}
                        resizeMode={'contain'}
                        source={require('../images/down.png')}
                      />
                    )}
                  </TouchableOpacity>
                  <View
                    style={{
                      height:
                        this.state.expanded && index == clicked ? null : 0,
                      overflow: 'hidden',
                    }}>
                    <PostageAccountListComponent
                      sendLabelEmail={this.sendLabelNotification}
                      detail={{...detail}}
                      reprintLable={this.reprint}
                      reprintReturnLabel={this.reprint_return_label}
                      printReturnLabelHanlder={this.PrintReturnLabel}
                      printReturnShippingCenterLabelHandler={
                        this.printReturnShippingCenterLabel
                      }
                    />
                  </View>
                </Fragment>
              ))}
              {!transaction_details_list && (
                <View style={paginationStyles}>
                  {first_page > 1 && (
                    <TouchableOpacity onPress={this.goLeft}>
                      <Image
                        resizeMode={'contain'}
                        source={require('../images/left-arrow.png')}
                      />
                    </TouchableOpacity>
                  )}
                  {pagesShownArray.map((val, index) => (
                    <View
                      key={index}
                      style={
                        current_page == val
                          ? paginationViewNumber
                          : combineStyles
                      }>
                      <Text
                        onPress={() => this.selectPage(val)}
                        style={textStyle}>
                        {val}
                      </Text>
                    </View>
                  ))}
                  {last_page < total_pages && (
                    <TouchableOpacity onPress={this.goRight}>
                      <Image
                        resizeMode={'contain'}
                        source={require('../images/right-arrow.png')}
                      />
                    </TouchableOpacity>
                  )}
                </View>
              )}
            </Fragment>
          )}
        </ScrollView>
        <FlashMessage
          position="top"
          icon={messageIconType}
          floating={true}
          animated={true}
          animationDuration={500}
          duration={5000}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  textStyle: {
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
  },
  selectedIndex: {
    backgroundColor: '#f6f8f9',
  },
  paginationViewNumber: {
    backgroundColor: '#D3D3D3',
    borderRadius: 30,
    height: 30,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paginationStyles: {
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '70%',
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingBottom: 20,
  },
  walletBalanceStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    // paddingTop: 5
  },
  availableBalanceText: {
    color: '#545454',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
    textAlign: 'center',
  },
  textColorRed: {
    color: 'red',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoMedium,
    textAlign: 'center',
  },
  bottonWrapperView: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    margin: 20,
    width: '100%',
  },
  textInputStyle: {
    height: 40,
    borderRadius: 10,
    width: '30%',
    borderColor: 'gray',
    borderWidth: 1,
  },
  textInputStyleError: {
    height: 40,
    borderRadius: 10,
    width: '30%',
    borderColor: 'red',
    borderWidth: 2,
  },
  modalHeadingStyle: {
    color: '#545454',
    fontSize: hp('2.5%'),
    fontFamily: Font.RobotoRegular,
    textAlign: 'center',
  },
  innerModal: {
    width: wp('80%'),
    height: hp('35%'),
    backgroundColor: '#fff',
    padding: 20,
    zIndex: 5,
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  outerModal: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    borderRadius: 20,
  },
  centerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: hp('85%'),
  },
  imageStyle: {
    height: hp('10%'),
    width: wp('5%'),
  },
  touchStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    borderWidth: 1,
    borderColor: '#e8edef',
    elevation: 0.5,
    height: 50,
    alignItems: 'center',
    padding: 10,
    marginVertical: 5,
  },
  lftTxt: {
    textAlign: 'center',
    color: '#4e4c4c',
    fontSize: hp('1.8%'),
  },
  rgtTxt: {
    color: '#7d7d7d',
    fontSize: hp('1.8%'),
    textAlign: 'left',
  },
  headerText: {
    color: '#7d7d7d',
    fontSize: hp('2%'),
    textAlign: 'left',
    fontFamily: Font.RobotoMedium,
  },
  scrollView: {
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  topDiv: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 35,
  },
  postageTxt: {
    fontSize: hp('2.5%'),
    fontFamily: Font.RobotoMedium,
    paddingRight: 10,
    flex: 1,
    color: '#545454',
    // paddingVertical:hp('2.5%'),
    paddingTop: hp('1.8%'),
    paddingBottom: hp('2.5%'),
  },
  balancDivWrapper: {
    marginBottom: 25,
  },

  balanceDiv: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginBottom: 15,
  },
  balanceInner: {
    textAlign: 'center',
    width: '45%',
    elevation: 1,
  },
  backgroundColor: {
    backgroundColor: '#f6f6f6',
    borderWidth: 1,
    borderColor: '#efefef',
    paddingTop: hp('5%'),
    paddingBottom: hp('5%'),
  },
  balanceTxt: {
    textAlign: 'center',
    fontSize: hp('2%'),
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },
  balance: {
    textAlign: 'center',
    fontSize: hp('3%'),
    color: '#00b3ff',
    fontFamily: Font.RobotoBold,
  },
});

export default PostageAccountHistory;
