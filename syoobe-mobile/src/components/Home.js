import React, {Component} from 'react';
import {
  AsyncStorage,
  Image,
  LayoutAnimation,
  Platform,
  ScrollView,
  StatusBar,
  View,
  Linking,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as RNLocalize from 'react-native-localize';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Carousel from 'react-native-snap-carousel';
import {connect} from 'react-redux';
import {
  fetchRecentlyViewed,
  homeProducts,
  homeProductsWithToken,
  logOutUser,
  showSearchField,
} from '../actions';
import {setI18nConfig} from '../translations/translation';
import {Spinner} from './common';
import {ProductCard} from './common/product/ProductItem';
import ProductList from './common/product/ProductList';
import ChildrenProps from './HOC/ChildrenProps';
import {ProductNewArrival} from './product';
import PTRView from 'react-native-pull-to-refresh';
import {GoogleSignin} from '@react-native-community/google-signin';
import DeepLinking from 'react-native-deep-linking';
import Bugsnag from '@bugsnag/react-native';
import {notificationApi} from '../services/services';
import {storeUnreadNotifications} from '../actions';

class Home extends Component {
  constructor(props) {
    super(props);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    setI18nConfig();
    this.state = {
      _token: null,
      device_token: null,
      orientation: 'potrait',
      userInfo: null,
      fcmToken: null,
      // allProducts: {},
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      const fcmToken = await AsyncStorage.getItem('fcmToken');
      console.log('fcm token', fcmToken);

      this.setState({
        _token: userToken,
        device_token: DeviceInfo.getUniqueId(),
        fcmToken: fcmToken,
      });
      return userToken;
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
    return;
  };

  bindScreenDimensionsUpdate = (event) => {
    const {width, height} = event.nativeEvent.layout;

    this.setState({
      orientation: width < height ? 'potrait' : 'landscape',
    });
  };

  componentDidMount = () => {
    DeepLinking.addScheme('syoobe://');
    Linking.addEventListener('url', this.handleUrl);

    DeepLinking.addRoute('/home', (response) => {
      // example://test
      console.log('ini respon deeplink', response);
    });

    RNLocalize.addEventListener('change', this.handleLocalizationChange);
    this.retrieveToken()
      .then((_token) => {
        if (_token === null) {
          this.props.homeProducts();
          this.getRecentProducts();
        } else {
          this.callNotifications(_token);
          this.setState({_token});
          this.props.homeProductsWithToken({_token});
          setTimeout(() => this.getRecentProducts(), 3000);
        }
      })
      .catch((error) => {
        Bugsnag.notify(error);
        console.log('Promise is rejected with error: ' + error);
      });
  };

  callNotifications = (token) => {
    this.setState({
      loading: true,
    });
    const details = {
      _token: token,
    };
    notificationApi({...details})
      .then((response) => {
        // console.log('response', response.data);
        if (response.data.status === 1) {
          this.props.storeUnreadNotifications(
            response.data.total_unread_notification,
          );
        }
        this.setState({
          loading: false,
        });
      })
      .catch((error) => {
        console.log('ee', error);
        Bugsnag.notify(error);
      });
  };

  getRecentProducts = async () => {
    var details = {user_id: this.props.user_id};
    if (this.state.device_token != null) {
      details = {
        ...details,
        ...{device_token: this.state.device_token},
      };
    }
    if (this.state._token != null) {
      details = {
        ...details,
        ...{_token: this.state._token},
      };
    }
    // console.log(details);
    await this.props.fetchRecentlyViewed(details);
  };

  removeToken = async () => {
    try {
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('fcmToken');
      await AsyncStorage.removeItem('deviceInfo');
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
  };

  removeShopId = async () => {
    try {
      await AsyncStorage.removeItem('shop_id');
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
  };

  removeImageUrl = async () => {
    try {
      await AsyncStorage.removeItem('image');
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
  };

  removeUsername = async () => {
    try {
      await AsyncStorage.removeItem('name');
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
  };

  removeEmail = async () => {
    try {
      await AsyncStorage.removeItem('email');
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
  };

  removeStore = async () => {
    try {
      await AsyncStorage.removeItem('emailStore');
      await AsyncStorage.removeItem('passwordStore');
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
  };

  requestLogOut = async () => {
    this.removeStore();
    this.removeUsername();
    this.removeImageUrl();
    this.removeEmail();
    this.removeShopId();
    GoogleSignin.revokeAccess();
    GoogleSignin.signOut();
    GoogleSignin.clearCachedAccessToken(this.setState({userInfo: null}));
    await this.removeToken()
      .then(() => {
        this.props.logOutUser(this.props.navigation);
      })
      .catch((error) => {
        Bugsnag.notify(error);
        console.log('error', error);
      });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (
      prevProps.recentlyViewedArray.length !==
      this.props.recentlyViewedArray.length
    ) {
      this.getRecentProducts();
    }
    if (this.props.home === 'logout') {
      console.log('ini logout Home');
      this.requestLogOut();
    }
  };

  renderRecentlyViewed = () => {
    return this.props.recentlyViewedArray.map((product, index) => (
      <ProductCard key={index} {...this.props} row={product} />
    ));
  };

  renderSection = () => {
    const {containerStyle} = styles;
    return (
      <View style={[containerStyle, {backgroundColor: '#fff'}]}>
        <ProductList
          data={this.props.home}
          _token={this.state._token}
          tab={'nonRecent'}
          {...this.props}
        />
        {this.props.recentlyViewedArray &&
          this.props.recentlyViewedArray.length > 0 && (
            <View>
              <ProductNewArrival />
              <View
                style={{
                  flex: 1,
                  marginBottom: hp(4),
                }}>
                <View style={{flex: 1}}>
                  <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}>
                    {this.renderRecentlyViewed()}
                  </ScrollView>
                  <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}>
                    {/* {this.renderRecentlyViewed()} */}
                  </ScrollView>
                </View>
              </View>
            </View>
          )}
      </View>
    );
  };

  search = (event) => {
    this.props.navigation.navigate('ProductsByCategory', {
      category_id: event.category_id,
      keyword: event.keyword,
      search: true,
    });
  };

  viewAllFromProductType = (event) => {
    this.props.navigation.navigate('ProductsByCategory', {
      category_id: event.category_id,
      keyword: event.keyword,
      search: true,
    });
  };

  _renderItem = ({item, index}) => {
    return (
      <View
        key={index}
        style={{
          flex: 1,
          paddingHorizontal: wp(1),
          paddingBottom: hp(1),
          backgroundColor: '#fff',
          width: wp(105),
        }}>
        <View>
          {this.state.orientation === 'potrait' ? (
            <Image style={styles.imagePotrait} source={{uri: item.path}} />
          ) : (
            <Image style={styles.imageLandscape} source={{uri: item.path}} />
          )}
        </View>
      </View>
    );
  };

  onHomePageScroll = () => {
    if (this.props.showSearch) {
      this.props.showSearchField();
    }
  };

  render() {
    // console.log('ini state', JSON.stringify(this.state));
    // console.log('ini props', JSON.stringify(this.props));
    // console.log('ini props', JSON.stringify(this.props.home));
    // console.log('ini props', JSON.stringify(this.props.user_id));
    // console.log('ini state', JSON.stringify(this.state.fcmToken));

    if (this.props.loading || this.props.recentLoading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          {/* <Spinner color="red" size="large" secondText={'50'} /> */}
          <Spinner color="red" size="large" />
        </View>
      );
    }

    return (
      <ChildrenProps>
        <View style={{flex: 1}} onLayout={this.bindScreenDimensionsUpdate}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <PTRView
            onRefresh={() =>
              this.state._token
                ? this.props.homeProductsWithToken({_token: this.state._token})
                : this.props.homeProducts()
            }>
            <ScrollView onScroll={this.onHomePageScroll}>
              {/* <Spinner2 visible={this.props.addToCartLoading} /> */}
              <View style={{flex: 1}}>
                <View style={{alignSelf: 'center'}}>
                  <Carousel
                    data={this.props.bannerImages}
                    renderItem={this._renderItem}
                    sliderWidth={
                      this.state.orientation === 'potrait'
                        ? wp('105%')
                        : wp('250%')
                    }
                    itemWidth={
                      this.state.orientation === 'potrait'
                        ? wp('105%')
                        : wp('250%')
                    }
                    initialScrollIndex={1}
                    layout={'default'}
                    autoplay={true}
                    autoplayInterval={4000}
                    autoplayDelay={500}
                    loopClonesPerSide={2}
                    loop={true}
                    inactiveSlideOpacity={0.4}
                  />
                </View>
              </View>
              {this.renderSection()}
            </ScrollView>
          </PTRView>
        </View>
      </ChildrenProps>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user_id: state.profile.profile_details.user_id,
    error: state.home.error,
    loading: state.home.loading,
    recentLoading: state.home.recentLoading,
    home: state.home.homeProducts,
    bannerImages: state.home.bannerImages,
    recentlyViewedArray: state.home.recentlyViewed,
    showSearch: state.home.showSearch,
    categories: state.category.categories,
    addToCartLoading: state.cart.addToCartLoading,
  };
};

const styles = {
  shadow: {
    shadowColor: '#eaeaea',
    shadowOffset: {
      width: 15,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 12.22,
    ...Platform.select({
      android: {
        elevation: 4,
      },
      ios: {
        shadowColor: '#000',
        shadowOffset: {
          width: 15,
          height: 4,
        },
        shadowOpacity: 0.1,
        shadowRadius: 12.22,
      },
    }),
  },
  abc: {
    overflow: 'hidden',
  },
  containerStyle: {
    backgroundColor: '#f00',
    width: '100%',
  },
  banner_top: {
    Width: '100%',
  },
  heading: {
    borderWidth: 1,
    borderColor: '#f00',
    height: 80,
    width: '100%',
  },
  bannerImg: {
    width: wp('100%'),
    // height:150,
    height: hp('20%'),
  },
  bottomSection: {
    borderColor: '#e7e7e7',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 30,
  },
  section1: {
    borderColor: '#f00',
    borderRightWidth: 1,
    textAlign: 'center',
    width: '33%',
  },
  imageLandscape: {
    resizeMode: 'stretch',
    height: hp('30%'),
    width: wp('250%'),
  },
  imagePotrait: {
    resizeMode: 'stretch',
    height: hp('21%'),
    width: wp('100%'),
  },
};

export default connect(mapStateToProps, {
  storeUnreadNotifications,
  logOutUser,
  fetchRecentlyViewed,
  showSearchField,
  homeProducts,
  homeProductsWithToken,
})(Home);
