import React, {Component} from 'react';
import PDFView from 'react-native-view-pdf';
import {
  PermissionsAndroid,
  Button,
  StyleSheet,
  StatusBar,
  View,
} from 'react-native';
import {Spinner} from './common';
import RNFetchBlob from 'react-native-fetch-blob';
import {showToast} from '../helpers/toastMessage';

class PdfViewer extends Component {
  static navigationOptions = ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
  });

  state = {
    loading: false,
    showDownloadButton: true,
    fileUrl: null,
    type: null,
  };

  downloadFile = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Download to storage Permission',
          message:
            'Syoobe App needs access to your storage ' +
            'so that you can download files and save them.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        let dirs = RNFetchBlob.fs.dirs.DownloadDir + '/' + 'Label';
        RNFetchBlob.config({
          path: dirs,
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: false,
            path: dirs,
          },
        })
          .fetch('GET', this.state.fileUrl)
          .then((res) => {
            console.log(res);
            showToast({
              message: 'Downloaded',
            });
            this.setState({
              loading: false,
            });
          })
          .catch((error) => console.log('ee', error));
      } else {
        console.log('Storage permission denied');
      }
    } catch (err) {
      this.setState({
        loading: false,
      });
      console.warn(err);
    }
  };

  componentDidMount = () => {
    this.setState({
      fileUrl: this.props.route.params.pdf,
      type: this.props.route.params.type,
    });
  };

  render() {
    const {type, fileUrl, loading, showDownloadButton} = this.state;
    return (
      <View style={styles.viewStyle}>
        {showDownloadButton && (
          <View style={styles.buttonStyle}>
            <Button
              onPress={this.downloadFile}
              title={'Download File'}
              color={'#C90205'}
            />
          </View>
        )}
        {loading && <Spinner text={'Loading...'} color="red" size="large" />}
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        {/* {
                    fileUrl && */}
        <PDFView
          fadeInDuration={250.0}
          style={{flex: 1}}
          resource={this.props.route.params.pdf}
          resourceType={this.props.route.params.type}
          onLoad={() => {
            console.log(`PDF rendered`);
            this.setState({
              loading: false,
              showDownloadButton: true,
            });
          }}
          onError={(error) => console.log('Cannot render PDF', error)}
        />
        {/* } */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonStyle: {
    alignSelf: 'flex-end',
    margin: 20,
    width: '40%',
  },
  viewStyle: {
    flex: 1,
    backgroundColor: '#fff',
    height: '100%',
    width: '100%',
  },
});

export default PdfViewer;
