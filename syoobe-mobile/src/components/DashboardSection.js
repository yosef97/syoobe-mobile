import React, {Component} from 'react';
import {Text, Image, View, TouchableOpacity} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class DashboardSection extends Component {
  constructor(props) {
    super(props);

    this.state = {
      chatObj: [],
      unreadCount: 0,
    };
  }

  render() {
    console.log('render', JSON.stringify(this.props));
    const {
      boxStyle,
      total_sales,
      total_order,
      total_purchase,
      total_credit,
      totalPurches,
      unread_messages,
      price,
    } = styles;
    var obj = Object.assign({}, this.props.box);

    return (
      <View style={{alignContent: 'center'}}>
        {obj[0] === 'total_sales' && (
          <View style={boxStyle}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Sales')}>
              <Image
                style={total_sales}
                source={require('../images/icon1.png')}
              />
              <Text style={totalPurches}>{'TOTAL PENJUALAN'}</Text>
              <Text style={price}>{obj[1]}</Text>
            </TouchableOpacity>
          </View>
        )}

        {obj[0] === 'total_order' && this.props.route.name === 'Pembeli' && (
          <View style={boxStyle}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('MyOrderList')}>
              <Image
                style={total_order}
                source={require('../images/icon2.png')}
              />
              <Text style={totalPurches}>{'TOTAL PESANAN'}</Text>
              <Text style={price}>{obj[1]}</Text>
            </TouchableOpacity>
          </View>
        )}

        {obj[0] === 'total_order' && this.props.route.name === 'Penjual' && (
          <View style={boxStyle}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Sales')}>
              <Image
                style={total_order}
                source={require('../images/icon2.png')}
              />
              <Text style={totalPurches}>{'TOTAL PESANAN'}</Text>
              <Text style={price}>{obj[1]}</Text>
            </TouchableOpacity>
          </View>
        )}

        {obj[0] === 'total_purchase' && (
          <View style={boxStyle}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('MyOrderList')}>
              <Image
                style={total_purchase}
                source={require('../images/purchesImg.png')}
              />
              <Text style={totalPurches}>{'TOTAL PEMBELIAN'}</Text>
              <Text style={price}>{obj[1]}</Text>
            </TouchableOpacity>
          </View>
        )}

        {/* {obj[0] === 'total_credit' && (
          <View style={boxStyle}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('MyWallet')}>
              <Image
                style={total_credit}
                source={require('../images/icon3.png')}
              />
              <Text style={totalPurches}>{'SALDO'}</Text>
              <Text style={price}>{obj[1]}</Text>
            </TouchableOpacity>
          </View>
        )} */}

        {obj[0] === 'unread_messages' && (
          <View style={styles.accountBalance}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Messages')}>
              <Image
                style={unread_messages}
                source={require('../images/icon4.png')}
              />
              <Text style={totalPurches}>{'PESAN YANG BELUM DI BACA'}</Text>
              <Text style={price}>{this.props.buyerData.unread_messages}</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const styles = {
  boxStyle: {
    borderWidth: 1,
    borderColor: 'transparent',
    elevation: 1,
    borderRadious: 5,
    width: wp(45),
    height: hp(20),
    justifyContent: 'center',
    alignItem: 'center',
    padding: 10,
    marginBottom: 10,
    // backgroundColor: '#fff',
  },
  accountBalance: {
    elevation: 1,
    borderRadious: 5,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'transparent',
    height: hp(17),
    width: wp(94),
    paddingHorizontal: wp(2),
    // height: wp(40),
    paddingVertical: hp(5),
    alignItem: 'center',

    // padding: wp(10),
  },

  total_sales: {
    width: wp('8%'),
    height: wp('9%'),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  total_purchase: {
    width: wp('9.5%'),
    height: wp('9%'),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  total_credit: {
    width: wp('9%'),
    height: wp('9%'),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  unread_messages: {
    width: wp('9.3%'),
    height: wp('7%'),
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  total_order: {
    width: wp('9%'),
    height: wp('9%'),
    marginLeft: 'auto',
    marginRight: 'auto',
  },

  totalPurches: {
    textAlign: 'center',
    color: '#848484',
    marginVertical: 2,
    fontSize: wp('4%'),
    textTransform: 'capitalize',
  },
  price: {
    textAlign: 'center',
    color: '#00b3ff',
    fontSize: wp('6%'),
  },
};
export default DashboardSection;
