import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import AddToCart from './AddToCart';
import {connect} from 'react-redux';
import {translate} from '../translations/translation';
import {formatRupiah} from '../helpers/helper';

class ProductFeatureCom extends Component {
  renderLessText = () => {
    const {product_text} = styles;
    const {product_name} = this.props.details;
    if (String(product_name).length > 17) {
      return (
        <Text style={product_text}>{product_name.substring(0, 17)} ...</Text>
      );
    } else {
      return <Text style={product_text}>{product_name}</Text>;
    }
  };

  goToProduct = (product_id, product_name) => {
    this.props.navigation.push('ProductDetail', {
      product_id: product_id,
      product_requires_shipping: this.props.details.prod_requires_shipping,
      product_name: product_name,
    });
  };

  render() {
    const {product_img, viewStyle, product_price_Style, buy_btn} = styles;
    const {
      user_own_product,
      product_name,
      product_id,
      product_image,
      product_price,
      price_currency,
    } = this.props.details;
    // console.log(formatRupiah(product_price))
    return (
      <View style={viewStyle}>
        <TouchableOpacity
          onPress={() => this.goToProduct(product_id, product_name)}
          style={product_img}>
          <Image style={product_img} source={{uri: product_image}} />
        </TouchableOpacity>
        <TouchableWithoutFeedback
          onPress={() => this.goToProduct(product_id, product_name)}>
          {this.renderLessText()}
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => this.goToProduct(product_id, product_name)}>
          <Text style={product_price_Style}>
            {price_currency}
            {formatRupiah(product_price)}
          </Text>
        </TouchableWithoutFeedback>
        {this.props._token != null && !user_own_product ? (
          <AddToCart {...this.props}>
            {this.props.details.prod_requires_shipping == 1 ? (
              <Text style={buy_btn}>{translate('Add to Cart')}</Text>
            ) : (
              <Text style={buy_btn}>{translate('Buy Now')}</Text>
            )}
          </AddToCart>
        ) : (
          <AddToCart {...this.props}>
            {<Text style={buy_btn}>{translate('View')}</Text>}
          </AddToCart>
        )}
      </View>
    );
  }
}

const styles = {
  viewStyle: {
    // backgroundColor:'#f00',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    width: hp('25.5%'),
    // width:'50%',
    bottom: 0,
    right: 0,
    color: 'ffffff',
    // flex:1,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    borderRightWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
  },
  product_img: {
    // width:65,
    // height:95,
    width: wp('23%'),
    height: hp('17%'),
    resizeMode: 'cover',
  },

  product_text: {
    color: '#545454',
    // fontSize:16  ,
    fontSize: hp('2%'),
    width: '100%',
    textAlign: 'center',
    marginTop: hp('1%'),
    // marginBottom:15,
    marginBottom: hp('1%'),
    fontFamily: Font.RobotoRegular,
  },
  product_price_Style: {
    color: '#00b3ff',
    // fontSize:18,
    fontWeight: 'bold',
    marginBottom: hp('1%'),
    fontSize: hp('1.5%'),
    fontFamily: Font.RobotoBold,
  },
  buy_btn: {
    width: wp('21%'),
    height: hp('3.4%'),
    // borderColor:'#c7c7c7',
    // borderWidth:1,
    // borderRadius:50,
    textAlign: 'center',
    color: '#c90305',
    fontSize: hp('1.6%'),
    lineHeight: hp('3%'),
    fontFamily: Font.RobotoRegular,
    // width:wp('30%'),
    // height:wp('10%')
  },
};

export default connect(null, null)(ProductFeatureCom);

{
  /* <ProductButton {...props}> */
}
{
  /* <Text style={ buy_btn}>Add to Cart</Text> */
}
{
  /* <Image style={ buy_btn} source={require('../../images/buy_now.png')}/> */
}
{
  /* </ProductButton> */
}
