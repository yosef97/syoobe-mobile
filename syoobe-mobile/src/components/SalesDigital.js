import React, {Component} from 'react';
import {Text, Image, TouchableOpacity, View, ScrollView} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {ButtonTwo, ButtonSkip, CardSection, InputInner} from './common';
import {Icon, Accordion} from 'native-base';
const dataArray = [
  {
    title: 'Downloadable Product Permission ',
    content: (
      <View>
        <View
          style={{
            borderBottomColor: '#e7e7e7',
            borderBottomWidth: 1,
            paddingBottom: hp('2%'),
            marginBottom: hp('2%'),
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: hp('1%'),
            }}>
            <Text
              style={{
                color: '#858585',
                fontFamily: Font.RobotoRegular,
                fontSize: hp('2%'),
              }}>
              Downloads Remaining:{' '}
            </Text>
            <Text
              style={{
                color: '#00b3ff',
                fontFamily: Font.RobotoRegular,
                fontSize: hp('2%'),
              }}>
              0
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: hp('1%'),
            }}>
            <Text
              style={{
                color: '#858585',
                fontFamily: Font.RobotoRegular,
                fontSize: hp('2%'),
              }}>
              Download Expire:{' '}
            </Text>
            <Text
              style={{
                color: '#00b3ff',
                fontFamily: Font.RobotoRegular,
                fontSize: hp('2%'),
              }}>
              05-11-2018
            </Text>
          </View>
          <ButtonTwo
            pageBtn_style={{
              paddingTop: hp('1%'),
              paddingBottom: hp('1%'),
              backgroundColor: '#f07f7f',
            }}>
            <Text>Revoked</Text>
          </ButtonTwo>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: hp('1%'),
          }}>
          <Text
            style={{
              color: '#858585',
              fontFamily: Font.RobotoRegular,
              fontSize: hp('2%'),
            }}>
            One Time download Access:{' '}
          </Text>
          <TouchableOpacity>
            <Image
              style={{width: wp('40%'), height: wp('7%')}}
              resizeMode="contain"
              source={require('../images/pngBtn.png')}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            paddingTop: 15,
            paddingBottom: 15,
          }}>
          <ButtonSkip>Grant Access</ButtonSkip>
          <ButtonTwo>Send Message</ButtonTwo>
        </View>
      </View>
    ),
  },
  {
    title: 'Upload Sample File',
    content: (
      <View>
        <CardSection>
          <InputInner placeholder="" label="Download Name" />
        </CardSection>
        <View style={{marginBottom: hp('3%')}}>
          <Text
            style={{
              marginLeft: 4,
              marginBottom: 8,
            }}>
            File Name
          </Text>
          <View
            style={{
              color: '#000',
              padding: 5,
              fontSize: wp('4%'),
              width: '100%',
              marginTop: 0,
              backgroundColor: '#fff',
              borderRadius: 50,
              elevation: 6,
              height: hp('8%'),
              // marginLeft:2,
              // marginRight:2,
              fontFamily: Font.RobotoRegular,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{
                width: 'auto',
                backgroundColor: '#e4e4e4',
                borderRadius: 50,
                height: '100%',
                paddingLeft: 20,
                paddingRight: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{fontSize: hp('2%')}}>Choose File</Text>
            </TouchableOpacity>
            <Text style={{marginLeft: 10}}>No file chosen</Text>
          </View>
        </View>
        <CardSection>
          <InputInner placeholder="" label="Revision Time" />
        </CardSection>
        <Text
          style={{
            color: '#858585',
            fontSize: wp('4%'),
            marginRight: wp('10%'),
          }}>
          Status:
          <Text style={{color: '#00b3ff', fontSize: wp('4%')}}>
            In Progress
          </Text>
        </Text>
        <Image
          style={{width: wp('32%'), height: wp('9%'), marginTop: hp('1.5%')}}
          resizeMode="contain"
          source={require('../images/sendMsg.png')}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            paddingTop: 15,
            paddingBottom: 15,
          }}>
          <ButtonSkip>+ Add Download</ButtonSkip>
          <ButtonTwo>Update</ButtonTwo>
        </View>
      </View>
    ),
  },
  {title: 'Payment History', content: <Text>Lorem ipsum dolor sit amet</Text>},
];

export default class SalesDigital extends Component {
  _renderHeader(item, expanded) {
    return (
      <View
        style={expanded ? [styles.hea_der, styles.hea_der2] : styles.hea_der}>
        <Text
          style={{
            color: '#545454',
            fontFamily: Font.RobotoRegular,
            fontSize: hp('2.2%'),
            borderTopWidth: 0,
          }}>
          {' '}
          {item.title}
        </Text>
        {expanded ? (
          <Icon style={{fontSize: 18}} name="arrow-dropup" />
        ) : (
          <Icon style={{fontSize: 18}} name="arrow-dropdown" />
        )}
      </View>
    );
  }
  _renderContent(item) {
    return <View style={styles.AcccontentWrapper}>{item.content}</View>;
  }
  render() {
    const {} = styles;
    return (
      <View>
        <ScrollView style={styles.bgColor}>
          <View>
            <View style={styles.boxStyle}>
              <View style={styles.imgContentWrapper}>
                <View style={styles.imgWrapper}>
                  <Image
                    style={styles.boxImg}
                    resizeMode="contain"
                    source={require('../images/w1.png')}
                  />
                </View>
                <View style={{flex: 1}}>
                  <Text style={styles.productName}>
                    Reebok Original Blue Sporty Watch {this.props.heading}{' '}
                  </Text>
                  <Text style={styles.available}>
                    Code:
                    <Text style={styles.count}> 111</Text>
                  </Text>
                  <Text style={styles.available}>
                    Invoice #:
                    <Text style={styles.count}>180627-0004534-S0001</Text>
                  </Text>
                  <Text style={styles.available}>
                    Sale Date:
                    <Text style={styles.count}>06-27-2018</Text>
                  </Text>
                  <Text style={styles.available}>
                    Processing Time:
                    <Text style={styles.count}>1 to 3 Business Days</Text>
                  </Text>
                  <Text style={styles.available}>
                    Qty:
                    <Text style={styles.count}>1 </Text>
                  </Text>
                  <Text style={styles.available}>
                    Status:
                    <Text style={styles.stat_us}> Shipped </Text>
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.priceDetail}>
              <Text style={styles.priceHeading}>PRICE DETAILS</Text>
              <View style={styles.totalPriceWrapper}>
                <Text style={styles.totalItem}>Cart Total</Text>
                <Text style={styles.priceTxt}>$5.00</Text>
              </View>
              <View style={styles.totalPriceWrapper}>
                <Text style={styles.totalItem}>Shipping & Handling</Text>
                <Text style={styles.priceTxt}>+ $0.00</Text>
              </View>
              <View style={styles.totalPriceWrapper}>
                <Text style={styles.totalItem}>Sales Tax</Text>
                <Text style={styles.priceTxt}>+ $0.20</Text>
              </View>
              <View style={styles.totalPriceWrapper}>
                <Text style={[styles.totalItem, styles.boldClass]}>
                  Order Total
                </Text>
                <Text style={styles.priceTxt}>$5.20</Text>
              </View>
            </View>
            <View style={styles.addressWrapper}>
              <View>
                <View style={styles.addressDiv}>
                  <View style={styles.flexCss}>
                    <Text style={styles.billingAddress}>BILLING ADDRESS</Text>
                    <Text style={styles.productName2}>Colin Tierney</Text>
                    <View style={styles.iconBox}>
                      <Image
                        style={styles.iconAddress}
                        source={require('../images/addressIcon.png')}
                      />
                      <Text style={styles.iconTxt}>
                        3909 Witmer Rd, Niagara Falls, NY 14305, USA
                      </Text>
                    </View>
                    <View style={styles.iconBox}>
                      <Image
                        style={styles.icon}
                        source={require('../images/phIcon.png')}
                      />
                      <Text style={styles.iconTxt}>+1 716-298-1822</Text>
                    </View>
                    <View style={styles.iconBox}>
                      <Image
                        style={styles.iconEmail}
                        source={require('../images/emailIcon.png')}
                      />
                      <Text style={styles.iconTxt}>
                        colin.tierney@gmail.com
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={styles.addressDiv}>
                  <View style={styles.flexCss}>
                    <Text style={styles.billingAddress}>SHIPPING ADDDRESS</Text>
                    <Text style={styles.productName2}>Colin Tierney</Text>
                    <View style={styles.iconBox}>
                      <Image
                        style={styles.iconAddress}
                        resizeMode="contain"
                        source={require('../images/addressIcon.png')}
                      />
                      <Text style={styles.iconTxt}>
                        3909 Witmer Rd, Niagara Falls, NY 14305, USA
                      </Text>
                    </View>
                    <View style={styles.iconBox}>
                      <Image
                        style={styles.icon}
                        resizeMode="contain"
                        source={require('../images/phIcon.png')}
                      />
                      <Text style={styles.iconTxt}>+1 716-298-1822</Text>
                    </View>
                    <View style={styles.iconBox}>
                      <Image
                        style={styles.iconEmail}
                        resizeMode="contain"
                        source={require('../images/emailIcon.png')}
                      />
                      <Text style={styles.iconTxt}>
                        colin.tierney@gmail.com
                      </Text>
                    </View>
                  </View>
                  <Image
                    style={styles.editIcon}
                    source={require('../images/editIcon.png')}
                  />
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              borderColor: '#e7e7e7',
              borderTopWidth: 1,
              borderBottomWidth: 1,
            }}>
            <Accordion
              dataArray={dataArray}
              animation={true}
              expanded={true}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              // headerStyle={{ backgroundColor: "#b7daf8" }}
              contentStyle={{backgroundColor: '#f00'}}
            />
          </View>
        </ScrollView>
        <View>
          <Image
            style={styles.plusBtn}
            source={require('../images/chat.png')}
          />
        </View>
      </View>
    );
  }
}
const styles = {
  AcccontentWrapper: {
    padding: 10,
    borderBottomColor: '#e7e7e7',
    borderBottomWidth: 1,
  },
  // row_:{
  //     flexDirection:'row',
  //     justifyContent:'space-between',
  //     marginBottom:hp('1%')
  // },
  // lftTxt:{
  //     color:'#858585',
  //     fontFamily:Font.RobotoRegular,
  //     fontSize:hp('2.5%'),
  // },
  // rgtTxt:{
  //     color:'#00b3ff',
  //     fontFamily:Font.RobotoRegular,
  //     fontSize:hp('2.5%'),
  // },
  hea_der: {
    flexDirection: 'row',
    paddingVertical: hp('2%'),
    paddingHorizontal: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderBottomColor: '#e7e7e7',
    borderBottomWidth: 1,
    borderTopColor: '#f00',
    elevation: 0,
  },
  hea_der2: {
    backgroundColor: '#f1f1f1',
  },
  abc: {},
  flexCss: {
    width: '80%',
    flexWarp: 'warp',
  },
  editIcon: {
    width: 40,
    height: 40,
  },
  icon: {
    width: 14,
    height: 14,
  },
  iconAddress: {
    width: 13,
    height: 18,
    marginTop: 3,
  },
  addressWrapper: {
    marginTop: hp('1%'),
    // borderBottomWidth:1,
    // borderColor:'#e7e7e7',
    paddingBottom: hp('1%'),
    paddingHorizontal: 10,
  },
  addressDiv: {
    paddingBottom: hp('2%'),
    marginBottom: hp('2%'),
    borderBottomWidth: 1,
    borderBottomColor: '#e7e7e7',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  billingAddress: {
    fontSize: wp('4.3%'),
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    marginBottom: hp('1%'),
  },
  productName2: {
    fontSize: wp('4.2%'),
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },
  iconBox: {
    flexDirection: 'row',
    marginTop: 5,
    // alignItems:'center',
  },
  iconEmail: {
    width: 14,
    height: 11,
    marginTop: 4,
  },
  iconTxt: {
    fontSize: wp('4%'),
    marginLeft: 8,
  },
  btn_style: {
    paddingLeft: 10,
    paddingRight: 10,
    width: 'auto',
    marginTop: hp('3%'),
  },
  bgColor: {
    backgroundColor: '#fff',
  },
  boxStyle: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    paddingBottom: 0,
    flexDirection: 'row',
    flexWarp: 'warp',
    position: 'relative',
  },
  imgContentWrapper: {
    flexDirection: 'row',
    // flexBasis:'80%',
    flex: 1,
    paddingRight: 15,
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    paddingBottom: 10,
  },
  boxImg: {
    width: wp('20%'),
    height: wp('32%'),
  },
  imgWrapper: {
    width: wp('20%'),
    marginRight: wp('3%'),
  },
  available: {
    color: '#858585',
    fontSize: hp('2%'),
    marginRight: wp('10%'),
    // fontFamily:Font.RobotoMedium,
  },
  price: {
    color: '#00b3ff',
    fontSize: wp('5%'),
    // fontFamily:Font.RobotoBold,
  },
  availableWrapper: {
    flexDirection: 'row',
    paddingTop: hp('0.4%'),
    paddingBottom: hp('0.4%'),
  },
  rgtMenuIcon: {
    width: wp('1.6%'),
    height: wp('7.5%'),
  },
  productName: {
    fontSize: wp('4.5%'),
    color: '#545454',
    // fontFamily:Font.RobotoRegular,
  },
  count: {
    color: '#00b3ff',
    fontSize: hp('2%'),
  },
  stat_us: {
    color: '#c90305',
    fontSize: hp('2%'),
  },
  priceHeading: {
    color: '#696969',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoMedium,
  },
  totalPriceWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: hp('1.5%'),
    paddingTop: hp('1.5%'),
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  totalItem: {
    fontSize: wp('4.2%'),
    color: '#6f6f6f',
    fontFamily: Font.RobotoRegular,
  },
  boldClass: {
    fontFamily: Font.RobotoBold,
  },
  priceTxt: {
    color: '#00b3ff',
    // fontWeight:'bold',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoBold,
  },
  priceDetail: {
    padding: 10,
  },
  plusBtn: {
    position: 'absolute',
    width: 50,
    height: 50,
    right: 10,
    bottom: 30,
  },
};
