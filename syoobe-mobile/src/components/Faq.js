import React, {Component} from 'react';
import {
  AsyncStorage,
  StatusBar,
  Image,
  Text,
  View,
  ScrollView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import axios from 'axios';
import {ButtonTwo, Spinner} from './common';
import RNPickerSelect from 'react-native-picker-select';

export default class Faq extends Component {
  static navigationOptions = {
    drawerLabel: 'Pusat Bantuan',
    drawerIcon: () => (
      <Image
        source={require('../images/help-icon.png')}
        style={{width: wp('4.3%'), height: hp('2.4%')}}
      />
    ),
  };

  state = {
    _token: null,
    topics: [],
    selectedTopic: 0,
    topicName: null,
    contentBasedonTopic: [],
    loading: true,
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = async () => {
    this.retrieveToken()
      .then((_token) => {
        this.setState({
          loading: true,
        });
        let details = {
          _token: _token,
        };
        var formBody = [];
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        axios
          .post('https://syoobe.co.id/api/faqTopic', formBody)
          .then((response) => {
            if (response.data.status == 1) {
              let array1 = [];
              response.data.data.forEach((item) => {
                array1.push({
                  label: item.category_name,
                  value: item.category_id,
                });
              });
              this.setState({
                topics: array1,
              });
              this.loadContent(response.data.data[0].category_id);
            } else {
              this.setState({
                loading: false,
              });
            }
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  loadContent = async (value) => {
    await this.setState({
      selectedTopic: value,
      loading: true,
    });
    let details = {
      _token: this.state._token,
      topic_id: this.state.selectedTopic,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/faqListOnTopic', formBody)
      .then((response) => {
        if (response.data.status == 1) {
          this.setState({
            contentBasedonTopic: response.data.faqlist,
            topicName: response.data.category_name,
            loading: false,
          });
        } else {
          this.setState({
            loading: false,
          });
        }
      });
  };

  render() {
    // let topics;
    // if(this.state.topics){
    //     topics = this.state.topics.map((value) => {
    //         return <Picker.Item key={value.category_id}
    //         value={value.category_id}
    //         label={value.category_name} />
    //     });
    // }
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color="red" size="large" />
        </View>
      );
    }
    return (
      <ScrollView style={styles.bgColor}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <View style={styles.viewStyle}>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.dropheadingClass}>
                Jelajahi menurut Topik
              </Text>
            </View>
            <View style={styles.dropdownClass}>
              <RNPickerSelect
                style={styles.pickerClass}
                value={this.state.selectedTopic}
                onValueChange={(val) => this.loadContent(val)}
                items={this.state.topics}
              />
            </View>
          </View>
        </View>
        <View style={styles.contentDiv}>
          {this.state.topicName && (
            <Text style={styles.hea_ding}>
              {this.state.topicName.toUpperCase()}
            </Text>
          )}
          {this.state.contentBasedonTopic.map((topic, index) => (
            <View key={topic.faq_id} style={styles.row}>
              <Text style={styles.headingTxt}>{topic.question}</Text>
              <Text style={styles.descriptiontxt}>{topic.answer}</Text>
            </View>
          ))}
        </View>
        <View style={{paddingBottom: 15}}>
          <ButtonTwo
            onPress={() =>
              this.props.navigation.navigate('ContactSupport', {
                _token: this.state._token,
              })
            }>
            Hubungi kami
          </ButtonTwo>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  bgColor: {
    backgroundColor: '#fff',
  },
  viewStyle: {
    padding: 10,
  },
  dropdownClass: {
    justifyContent: 'center',
    alignItems: 'center',
    height: hp('6%'),
    elevation: 1,
    borderRadius: 50,
    // marginBottom:17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  pickerClass: {
    height: hp('6%'),
  },
  dropheadingClass: {
    fontSize: hp('2.2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  contentDiv: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },
  hea_ding: {
    color: '#696969',
    fontFamily: Font.RobotoMedium,
    fontSize: hp('2.2%'),
    marginBottom: hp('1.6%'),
    textTransform: 'uppercase',
  },
  headingTxt: {
    color: '#959595',
    fontFamily: Font.RobotoMedium,
    fontSize: hp('2'),
    marginBottom: hp('1%'),
  },
  descriptiontxt: {
    color: '#959595',
    fontFamily: Font.RobotoLight,
    fontSize: hp('1.8'),
  },
  row: {
    marginBottom: hp('2%'),
  },
};
