import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import {ProductButton} from './product';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {connect} from 'react-redux';
import {setI18nConfig, translate} from '../translations/translation';

class ProductFeatureShop extends Component {
  render() {
    // console.log(this.props);
    const {product_img, viewStyle, buy_btn, buy_btn_img} = styles;
    const {name, logo, id} = this.props.details;
    return (
      <View style={viewStyle}>
        <Image style={product_img} source={{uri: logo}} />
        <ProductButton onPress={() => this.goToShop(id)} style={buy_btn}>
          <Text style={buy_btn}>{translate('View Shop')}</Text>
          {/* <Image style={ buy_btn_img}  source={require('../../images/viewProduct.png')}/> */}
        </ProductButton>
      </View>
    );
  }

  goToShop = (id) => {
    this.props.navigation.navigate('ShopDetails', {shops_id: id});
  };
}


const styles = {
  viewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 23,
    paddingBottom: 23,
    width: wp('50%'),
    // width:'50%',
    bottom: 0,
    right: 0,
    color: 'ffffff',
    // flex:1,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    borderRightWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
  },
  product_img: {
    // width:85,
    // height:25,
    marginBottom: 5,
    width: wp('20%'),
    height: wp('7.5%'),
  },

  // buy_btn_img:{
  //     // width:93,
  //     // height:27,
  //     width:wp('22%'),
  //     height:wp('3.9%'),
  //     // width:60,
  //     // height:17,
  // },
  buy_btn: {
    // width:90,
    // height:26.5,
    width: wp('22%'),
    height: wp('6.5%'),
    // borderColor: "#c7c7c7",
    // borderWidth: 1,
    // borderRadius: 50,
    textAlign: 'center',
    color: '#c90305',
    fontSize: wp('3%'),
    lineHeight: wp('6%'),
    fontFamily: Font.RobotoRegular,
    // width:wp('30%'),
    // height:wp('10%')
  },
};

export default connect()(ProductFeatureShop);
// export {ProductFeatureShop};
