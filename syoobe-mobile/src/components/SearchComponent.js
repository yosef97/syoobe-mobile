import React, {Component} from 'react';
import {Text, TouchableOpacity, Image, View, TextInput} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {connect} from 'react-redux';
// import {Icon, Picker} from 'native-base';
import ActionSheet from 'react-native-action-sheet';

class SearchComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      PickerSelectedVal: '',
      selected: undefined,
      name: null,
      searchText: '',
      onFocusInput: true,
      categoryArray: [],
    };
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.categories.length != this.props.categories.length) {
      let array = [];
      this.props.categories.map((element) => {
        array.push(element.category_name);
      });
      this.setState({
        categoryArray: array,
      });
    }
  };

  showActonSheet = () => {
    ActionSheet.showActionSheetWithOptions(
      {
        options: this.state.categoryArray,
        tintColor: 'blue',
      },
      (buttonIndex) => {
        let category_id = this.props.categories[buttonIndex]?.category_id;
        let category_name = this.props.categories[buttonIndex]?.category_name;
        this.setState({
          selected: category_id,
          name: category_name,
        });
      },
    );
  };

  render() {
    // console.log('ini props seach comp', JSON.stringify(this.props));
    // console.log('ini state search comp', JSON.stringify(this.state));

    const {boxStyle, boxImg, totalPurches, price} = styles;
    return (
      <View style={boxStyle}>
        <View style={styles.wrapper}>
          <View style={styles.inputDiv}>
            <TextInput
              onChangeText={(text) =>
                this.setState({
                  searchText: text,
                })
              }
              defaultValue={this.state.searchText}
              placeholder="Apa yang kamu cari?"
              autoFocus={true}
              style={styles.inputStyle}
            />
          </View>
          <View style={styles.selectDiv}>
            <View style={styles.selectWrapper}>
              <TouchableOpacity
                onPress={this.showActonSheet}
                style={styles.pickerStyle}>
                <Text
                  style={{
                    fontFamily: Font.RobotoRegular,
                    color: 'gray',
                    fontSize: 16,
                  }}>
                  {this.state.selected ? this.state.name : 'Semua'}
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.props.onClickSearch({
                  keyword: this.state.searchText,
                  category_id: this.state.selected,
                })
              }
              style={styles.imgDiv}>
              <Image
                style={styles.searchIcon}
                resizeMode="contain"
                source={require('../images/search_field_icon.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  boxStyle: {
    borderWidth: 1,
    borderColor: 'transparent',
    elevation: 3,
    borderRadious: 5,
    width: '100%',
    justifyContent: 'center',
    alignItem: 'center',
    padding: 10,
    backgroundColor: '#c90305',
  },
  wrapper: {
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    backgroundColor: '#f9e3e3',
    alignItems: 'center',
  },
  inputStyle: {
    // backgroundColor:'#1800ff'
    paddingLeft: 10,
    fontSize: hp('2%'),
  },
  inputDiv: {
    // width:'50%',
    flexBasis: '60%',
  },
  selectDiv: {
    // width:'40%',
    flexBasis: '40%',
    // borderColor:'#fff',
    // borderWidth:1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imgDiv: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 20,
    height: 'auto',
    paddingRight: 3,
  },
  selectWrapper: {
    // paddingLeft:10,
    // width:'68%',
    flex: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: '#000',
    marginTop: hp('1%'),
    marginBottom: hp('1%'),
  },
  pickerStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: undefined,
    height: hp('6%'),
    color: '#7b7b7b',
    //  fontFamily:Fonts.RobotoRegular,
    //  fontSize:hp('1%')
  },
  imgDiv: {
    width: wp('9%'),
    height: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  searchIcon: {
    width: wp('5%'),
    // marginLeft:'auto',
    // marginRight:'auto'
  },
};

export default connect(null, null)(SearchComponent);
