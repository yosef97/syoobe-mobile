import axios from 'axios';
import React, {Component} from 'react';
import {
  AsyncStorage,
  Picker,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {
  ButtonTwo,
  CardSection,
  InputInner,
  Spinner,
  TextareaInner,
} from './common';
import {Font} from './Font';

class RequestNewCompatibility extends Component {
  state = {
    _token: null,
    fields: [{data: []}],
    selectedCategory: 0,
    comment: '',
    loading: false,
  };

  componentDidMount = () => {
    this.retrieveToken();
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
    } catch (error) {
      console.log(error);
    }
  };

  getCompatContent = async (itemValue) => {
    this.setState({
      fields: [{data: []}],
      selectedCategory: itemValue,
      loading: true,
    });
    var details = {
      _token: this.state._token,
      group_id: itemValue,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/individuallyGroupAttribute', formBody)
      .then((response) => {
        const array = [...this.state.fields];
        Object.assign(array[0].data, response.data.data);
        array[0].data.forEach((item) => {
          Object.assign(item, {value: ''});
        });
        this.setState({
          fields: array,
          loading: false,
        });
      });
  };

  increaseReqCompatArray = async () => {
    var details = {
      _token: this.state._token,
      group_id: this.state.selectedCategory,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/individuallyGroupAttribute', formBody)
      .then((response) => {
        const array = [...this.state.fields];
        array.push({data: response.data.data});
        Object.assign(array[array.length - 1].data, response.data.data);
        array[array.length - 1].data.forEach((item) => {
          Object.assign(item, {value: ''});
        });
        this.setState({
          fields: array,
        });
      });
  };

  decreaseReqCompatArray = async () => {
    var array = [...this.state.fields];
    array.splice(array.length - 1, 1);
    await this.setState({
      fields: array,
    });
  };

  changeText = (text, index, innerindex) => {
    var array = [...this.state.fields];
    array[index].data[innerindex].value = text;
    this.setState({
      fields: array,
    });
  };

  changeComment = async (text) => {
    this.setState({
      comment: text,
    });
    // var array = [...this.state.fields];
    // array.forEach((item) => {
    //     item.data.forEach((data) => {
    //         data.comment = text
    //     })
    // })
    // await this.setState({
    //     fields: array
    // })
  };

  requestNewCompatibility = async () => {
    this.setState({
      loading: true,
    });
    if (this.state.selectedCategory == 0) {
      Alert.alert('Silahkan pilih kategory');
    } else {
      var details = {};
      var hitApi = true;
      for (var i = 0; i < (await this.state.fields.length); i++) {
        for (var j = 0; j < this.state.fields[i].data.length; j++) {
          if (this.state.fields[i].data[j].value.trim() == '') {
            Alert.alert('Silahkan lengkapi data anda');
            hitApi = false;
            this.setState({
              loading: false,
            });
            break;
          }
        }
      }
      if (hitApi) {
        details = {
          ...details,
          ...{_token: this.state._token},
          ...{category: this.state.selectedCategory},
          ...{product_compatibilities: this.state.fields},
        };
        if (this.state.comment.trim() != '') {
          details = {
            ...details,
            ...{comment: this.state.comment},
          };
        }
        var formBody = [];
        for (var property in details) {
          if (property == 'product_compatibilities') {
            var array = [];
            for (var i = 0; i < details[property].length; i++) {
              array.push(details[property][i]);
            }
            formBody.push(property + '=' + JSON.stringify(array));
          } else {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + '=' + encodedValue);
          }
        }
        formBody = formBody.join('&');
        await axios
          .post('https://syoobe.co.id/api/requestCompatibility', formBody)
          .then((response) => {
            this.setState({
              loading: false,
            });
            if (response.data.status == 1) {
              Alert.alert(
                'Informasi',
                'Data berhasil dikirim, silahkan tunggu setelah Admin menyetujui permintaan Anda',
                [{text: 'OK', onPress: () => this.props.navigation.pop()}],
              );
            } else {
              Alert.alert('Informasi', response.data.msg, [
                {text: 'OK', onPress: () => this.props.navigation.pop()},
              ]);
            }
          })
          .catch((err) => {
            this.setState({
              loading: false,
            });
            console.log('er', err);
            Alert.alert('Maaf ada yang salah');
          });
      }
    }
  };

  render() {
    let compatibilities;
    if (this.props.compatHeaders) {
      compatibilities = this.props.compatHeaders.map((value) => {
        return (
          <Picker.Item
            key={value.group_id}
            value={value.group_id}
            label={value.group_name}
          />
        );
      });
    }
    if (this.state.loading) {
      return <Spinner color="red" size="large" />;
    }
    return (
      <View style={{backgroundColor: '#fff', height: '100%', padding: 15}}>
        <ScrollView keyboardShouldPersistTaps={'handled'}>
          <Text
            style={{
              color: '#545454',
              fontSize: hp('3%'),
              paddingBottom: 0,
              fontFamily: Font.RobotoRegular,
              textAlign: 'center',
            }}>
            {' '}
            AJUKAN PERMINTAAN KOMPATIBILITAS YANG BARU{' '}
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.dropheadingClass}>Kategori</Text>
            <Text style={{color: '#f00', fontSize: hp('2.2%'), marginTop: 5}}>
              {' '}
              *
            </Text>
          </View>
          <View style={styles.dropdownClass}>
            <Picker
              style={styles.pickerClass}
              selectedValue={this.state.selectedCategory}
              onValueChange={(itemValue) => this.getCompatContent(itemValue)}>
              <Picker.Item label="Kategori " value="0" key="0" />
              {compatibilities}
            </Picker>
          </View>
          {this.state.fields[0].data.length > 0 && (
            <View>
              {this.state.fields.map((data, index) => (
                <View key={index} style={styles.shippingWrapper}>
                  {this.state.fields[index].data.map((field, innerindex) => (
                    <View key={innerindex}>
                      <CardSection>
                        <InputInner
                          value={field.value}
                          onChangeText={(value) =>
                            this.changeText(value, index, innerindex)
                          }
                          message={'not-mandatory'}
                          label={field.attribute_name}
                        />
                      </CardSection>
                    </View>
                  ))}
                </View>
              ))}
              <TouchableOpacity
                disabled={this.state.fields.length <= 1}
                onPress={this.decreaseReqCompatArray}
                style={styles.addBtn}>
                <Text style={styles.addTxt}>-</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this.increaseReqCompatArray}
                style={styles.addBtn}>
                <Text style={styles.addTxt}>+</Text>
              </TouchableOpacity>
            </View>
          )}
          <CardSection>
            <TextareaInner
              value={this.state.comment}
              onChangeText={(value) => this.changeComment(value)}
              message={'not-mandatory'}
              label="Komentar"
            />
          </CardSection>
          {
            <View style={styles.btnWrapper}>
              <ButtonTwo
                onPress={() => {
                  this.requestNewCompatibility();
                }}>
                Kirim
              </ButtonTwo>
            </View>
          }
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  leftArrow: {
    position: 'absolute',
    left: -14,
  },
  rightArrow: {
    position: 'absolute',
    right: 0,
  },
  arrowCode: {
    fontSize: hp('8%'),
    color: '#fff',
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  Panel_Button_Text: {
    fontSize: hp('2.1%'),
    textAlign: 'left',
    color: '#545454',
    fontFamily: Font.RobotoBold,
  },
  Btn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: hp('2.2%'),
    paddingHorizontal: 10,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  iconImg: {
    width: 16,
    height: 9,
  },
  multi_SelectClass: {
    elevation: 1,
    borderRadius: 50,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
    height: hp('8%'),
    lineHeight: 0,
  },
  optionWrapper: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  cross_Icon: {
    backgroundColor: '#f00',
    width: '20%',
    padding: 10,
  },
  cross_Icon2: {
    backgroundColor: '#fff',
    width: '20%',
    padding: 10,
  },
  iconText: {
    backgroundColor: '#7892EB',
    width: '80%',
    padding: 10,
  },
  iconText2: {
    backgroundColor: '#fff',
    width: '80%',
    padding: 10,
  },

  whiteColor: {
    color: '#fff',
  },
  blackColor: {
    color: '#000',
  },
  redColor: {
    color: '#000',
  },
  required: {
    borderColor: '#e7e7e7',
    borderWidth: 1,
    padding: 10,
    marginBottom: 10,
  },
  crossColor: {
    textAlign: 'center',
    color: '#000',
  },
  closeBtn2: {
    position: 'absolute',
    right: 0,
    width: 30,
    height: 30,
    top: 0,
    zIndex: 1002,
    color: '#fff',
    borderRadius: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  shopBannerWrapper: {
    width: wp('50%'),
    height: hp('25%'),
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  shopBanner: {
    width: '100%',
    height: '100%',
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 10,
    resizeMode: 'contain',
    position: 'relative',
  },
  closeIcon: {
    position: 'absolute',
    right: 0,
    top: 0,
    width: 25,
    height: 25,
  },
  innerSegment: {
    backgroundColor: '#f00',
  },
  containerStyle: {
    padding: 0,
    // flex:1
  },
  topSection: {
    backgroundColor: '#e6e6e6',
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    flexWarp: 'warp',
    justifyContent: 'space-between',
  },
  inputStyle: {
    color: '#000',
    paddingLeft: 10,
    fontSize: wp('4%'),
    marginTop: 0,
    backgroundColor: '#fff',
    height: 40,
    fontFamily: Font.RobotoRegular,
    width: wp('30%'),
    borderRadius: 5,
  },
  datepicherStyle: {
    width: '100%',
    color: '#000',
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: wp('4%'),
    marginTop: 0,
    backgroundColor: '#fff',
    height: hp('8%'),
    fontFamily: Font.RobotoRegular,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  pickerClass: {
    height: 40,
    width: 'auto',
  },
  dropheadingClass: {
    display: 'none',
  },
  btnWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 15,
  },
  segmentStyle: {
    backgroundColor: '#b90002',
    textAlign: 'left',
    justifyContent: 'flex-start',
    paddingLeft: 30,
    height: 'auto',
    paddingTop: 10,
    paddingBottom: 0,
    paddingRight: 30,
    alignItems: 'center',
  },
  tabWrapper: {},
  segmentBtn: {
    borderWidth: 0,
    borderColor: 'transparent',
    bottom: 0,
    margin: 0,
    paddingTop: 10,
    paddingBottom: 10,
    height: 'auto',
    marginBottom: 0,
  },
  segmentBtnTxt: {
    color: '#e6e6e6',
  },
  segmentBtnTxtActive: {
    color: '#606060',
  },
  segmentActive: {
    backgroundColor: '#e6e6e6',
    borderWidth: 0,
    borderColor: 'transparent',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    paddingTop: 10,
    height: 'auto',
    paddingBottom: 10,
    color: '#e6e6e6',
  },
  boxImg: {
    position: 'absolute',
    // right:20,
    // top:13,
    // width:wp('7%'),
    // height:hp('3.8%')
  },
  viewStyle: {
    marginleft: 5,
    marginRight: 5,
    paddingLeft: 10,
    paddingRight: 10,
  },
  btnWrapper: {
    paddingBottom: 15,
  },
  abc: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingLeft: 5,
    textAlign: 'left',
    flexWrap: 'wrap',
    width: '100%',
    flexBasis: '100%',
  },
  checkBoxStyle: {
    width: wp('10%'),
    flexBasis: wp('10%'),
  },
  labelStyle2Wrapper: {
    width: '87%',
    flexBasis: '87%',
    // fontSize:hp('30%'),
  },
  labelstyle2: {
    // fontSize:wp('30%'),
    color: '#545454',
    marginLeft: 0,
    fontFamily: Font.RobotoRegular,
    // color:'#f00'
  },
  labelTxt: {
    color: '#545454',
    marginLeft: 0,
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.3%'),
  },
  billingAddress: {
    fontSize: hp('2.5%'),
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    marginBottom: hp('1%'),
    borderBottomWidth: 1,
    borderBottomColor: '#e7e7e7',
    paddingBottom: 10,
    marginTop: hp('1.5%'),
  },
  productReceive: {
    padding: 20,
  },
  headingTxt: {
    color: '#696969',
    fontFamily: Font.RobotoMedium,
    marginBottom: 10,
  },
  labelStyle: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  inputWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: hp('3%'),
  },
  inputStyle: {
    color: '#000',
    paddingLeft: 20,
    fontSize: wp('4%'),
    width: '30%',
    marginTop: 0,
    backgroundColor: '#fff',
    borderRadius: 50,
    elevation: 2,
    height: hp('8%'),
    fontFamily: Font.RobotoRegular,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  dropdownClass: {
    // elevation: 1,
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
    borderRadius: 50,
  },
  pickerClass: {
    height: hp('8%'),
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  shippingWrapper: {
    padding: 10,
    borderColor: '#e7e7e7',
    borderWidth: 1,
    marginTop: hp('2%'),
  },
  addBtn: {
    backgroundColor: '#DC0028',
    width: wp('20%'),
    height: wp('10%'),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    alignSelf: 'flex-end',
    marginBottom: 10,
  },
  addTxt: {
    color: '#fff',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: wp('7%'),
  },
  view_Style: {
    color: '#000',
    padding: 5,
    fontSize: wp('4%'),
    width: '100%',
    marginTop: 0,
    backgroundColor: '#fff',
    borderRadius: 50,
    elevation: 6,
    height: hp('8%'),
    fontFamily: Font.RobotoRegular,
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonStyle: {
    width: 'auto',
    backgroundColor: '#e4e4e4',
    borderRadius: 50,
    height: '100%',
    fontSize: wp('3%'),
    paddingLeft: 15,
    paddingRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noFileTxt: {
    marginLeft: 10,
  },
  fileType: {
    fontSize: wp('3.5%'),
    marginTop: 10,
    paddingLeft: 8,
    fontFamily: Font.RobotoLight,
  },
  fileTypeRed: {
    fontSize: wp('3.5%'),
    marginTop: 10,
    paddingLeft: 8,
    fontFamily: Font.RobotoLight,
    color: '#DC0028',
  },
  logoTxt: {
    fontSize: wp('4%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  fileInputWrapper: {
    // marginBottom:10
  },
  resultDiv: {
    width: '100%',
    borderColor: '#e3e3e3',
    borderWidth: 1,
    height: hp('15%'),
    padding: 10,
    backgroundColor: '#f5f5f5',
    marginBottom: hp('3%'),
  },
  topSectionBtn: {
    marginTop: hp('2%'),
    marginBottom: hp('1%'),
    padding: 10,
    textAlign: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  notFinding: {
    fontSize: hp('2.2%'),
    fontFamily: Font.RobotoRegular,
    marginBottom: hp('1%'),
  },
  reqBtn: {
    backgroundColor: '#2daebf',
    alignSelf: 'center',
    marginBottom: hp('1%'),
    borderRadius: 5,
  },
  reqBtnTxt: {
    color: '#fff',
    fontSize: hp('1.7%'),
    fontFamily: Font.RobotoRegular,
    textAlign: 'center',
    paddingHorizontal: hp('1%'),
    paddingVertical: hp('0.9%'),
    borderRadius: 5,
  },
  segmentStyle2: {
    backgroundColor: '#e6e6e6',
    textAlign: 'left',
    justifyContent: 'flex-start',
    paddingLeft: 10,
    height: 'auto',
    paddingTop: 5,
    paddingBottom: 0,
    borderColor: '#000cff',
    borderWidth: 1,
  },
  segmentBtn2: {
    borderWidth: 0,
    borderColor: 'transparent',
    bottom: 0,
    margin: 0,
    paddingTop: 10,
    paddingBottom: 10,
    height: 'auto',
    marginBottom: 0,
    marginBottom: 5,
    borderColor: '#c90305',
    borderWidth: 1,
    marginRight: 5,
    borderLeftWidth: 1,
    borderLeftColor: '#c90305',
  },
  segmentActive2: {
    backgroundColor: '#c90305',
    borderColor: '#c90305',
    borderWidth: 1,
    paddingTop: 10,
    height: 'auto',
    paddingBottom: 10,
    color: '#e6e6e6',
    marginBottom: 5,
    marginRight: 5,
  },
  segmentBtnTxt2: {
    color: '#606060',
    textAlign: 'center',
    fontSize: hp('2%'),
  },
  segmentBtnTxtActive2: {
    color: '#e6e6e6',
    textAlign: 'center',
    fontSize: hp('2%'),
  },
  discountTab: {
    justifyContent: 'space-between',
    paddingRight: 10,
  },
  segmentBtn3: {
    borderWidth: 0,
    borderColor: 'transparent',
    bottom: 0,
    margin: 0,
    paddingTop: 10,
    paddingBottom: 10,
    height: 'auto',
    marginBottom: 0,
    marginBottom: 5,
    borderColor: '#c90305',
    borderWidth: 1,
    borderLeftWidth: 1,
    borderLeftColor: '#c90305',
    width: '50%',
  },
  segmentActive3: {
    backgroundColor: '#c90305',
    borderColor: '#c90305',
    borderWidth: 1,
    paddingTop: 10,
    height: 'auto',
    paddingBottom: 10,
    color: '#e6e6e6',
    marginBottom: 5,
    width: '50%',
  },
  segmentBtnTxt3: {
    color: '#606060',
    textAlign: 'center',
    width: '100%',
    fontSize: hp('2%'),
  },
  segmentBtnTxtActive3: {
    color: '#e6e6e6',
    textAlign: 'center',
    width: '100%',
    fontSize: hp('2%'),
  },
  generalTabBtn: {
    borderWidth: 0,
    borderColor: 'transparent',
    bottom: 0,
    margin: 0,
    paddingTop: 10,
    paddingBottom: 10,
    height: 'auto',
    marginBottom: 0,
    marginBottom: 5,
    borderColor: '#c90305',
    borderWidth: 1,
    borderLeftWidth: 1,
    borderLeftColor: '#c90305',
    width: '50%',
  },

  generalTabBtnActive1: {
    backgroundColor: '#006400',
    borderColor: '#c90305',
    borderWidth: 1,
    paddingTop: 10,
    height: 'auto',
    paddingBottom: 10,
    color: '#e6e6e6',
    marginBottom: 5,
    width: '50%',
  },
  generalTabBtnActive2: {
    backgroundColor: '#4B0082',
    borderColor: '#c90305',
    borderWidth: 1,
    paddingTop: 10,
    height: 'auto',
    paddingBottom: 10,
    color: '#e6e6e6',
    marginBottom: 5,
    width: '50%',
  },
};

const mapStateToProps = (state) => {
  return {
    compatHeaders: state.country.compatData,
  };
};

export default connect(mapStateToProps, {})(RequestNewCompatibility);

// var array = [...this.state.fields];
// array.push({data: this.state.backendData});
// await this.setState({
//     fields: [...this.state.fields,{data:[...this.state.fields[this.state.fields.length-1].data]}]
// })
// await this.setState({
//     fields: array
// })

// this.setState(state => {
//     console.log('data',state.fields[index].data[innerindex].value);
//     const list = state.fields[index].data.map((item, i) => {
//         if (innerindex === i) {
//             item.value = text;
//             return item;
//         } else {
//             return item;
//         }
//     });
//     return {
//         list
//     };
// })
// array[index].data.forEach((item, i) => {
//     if(innerindex == i){
//         item.value = text
//     }
// })
