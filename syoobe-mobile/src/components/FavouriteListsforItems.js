import React, {Fragment, Component} from 'react';
import {
  Alert,
  TextInput,
  Button as Butt,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
  AsyncStorage,
  TouchableOpacity,
  StatusBar,
  Text,
  Image,
  View,
  ScrollView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {Spinner} from './common';
import axios from 'axios';
import {Button, Segment} from 'native-base';
import NoRecords from '../components/NoRecords';
import {showToast} from '../helpers/toastMessage';
import {substr} from '../helpers/helper';

class FavouriteListsforItems extends Component {
  static navigationOptions = {
    drawerLabel: 'Favorit',
    drawerIcon: () => (
      <Image
        source={require('../images/favourite-icon.png')}
        style={{width: wp('4.5%'), height: hp('2.2%')}}
      />
    ),
  };

  state = {
    seg: 1,
    loading: false,
    loadingModal: false,
    _token: null,
    mainFavArray: null,
    mainFavListName: null,
    subFavArray: null,
    mainFavTotalItems: null,
    willShowModal: false,
    inputError: false,
    newListName: '',
    showDelete: false,
    selectedIndex: null,
    shopDetails: null,
  };

  constructor(props) {
    super(props);
    this.scrollRef = React.createRef();
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = async () => {
    await this.retrieveToken();
    this.loadItems();
  };

  loadItems = (value) => {
    this.setState({
      loading: true,
    });
    var details = {
      _token: this.state._token,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/allFavouriteList', formBody)
      .then((response) => {
        console.log('data', response);
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          this.setState({
            mainFavArray: response.data.main_fav_product_details,
            mainFavListName: response.data.main_fav_list_name,
            mainFavTotalItems: response.data.total_item_in_main_fav_list,
            subFavArray: response.data.sub_fav_deatils,
          });
          if (value == 'down') {
            setTimeout(() => {
              this.scrollRef.current.scrollToEnd({animated: true});
            }, 50);
          }
        } else {
          this.setState({
            mainFavArray: null,
            mainFavListName: null,
            mainFavTotalItems: null,
            subFavArray: null,
          });
        }
      });
  };

  goToListContent = (id) => {
    this.setState({
      selectedIndex: null,
    });
    this.props.navigation.navigate('Favorites', {
      list_id: id,
      deleteList: this.showDeleteListAlert,
      list: this.loadItems,
    });
  };

  changeModalState = () => {
    this.setState((prevState, prevProps) => {
      return {
        willShowModal: !prevState.willShowModal,
      };
    });
  };

  changeShowDeleteState = (index) => {
    if (index == undefined) {
      this.setState({
        showDelete: false,
      });
      showToast({
        message: 'Tidak dapat menghapus daftar default!',
      });
    } else {
      if (this.state.selectedIndex == index) {
        this.setState((prevState, prevProps) => {
          return {
            showDelete: !prevState.showDelete,
            selectedIndex: index,
          };
        });
      } else {
        this.setState({
          showDelete: true,
          selectedIndex: index,
        });
      }
    }
  };

  createFavList = () => {
    if (this.state.newListName.toString().trim() != '') {
      this.setState({
        loadingModal: true,
      });
      var details = {
        _token: this.state._token,
        ulist_title: this.state.newListName,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      axios
        .post('https://syoobe.co.id/api/createListInFavouriteSection', formBody)
        .then((response) => {
          this.setState({
            loadingModal: false,
          });
          if (response.data.status == 1) {
            this.changeModalState();
            this.setState({
              inputError: false,
              newListName: '',
            });
            showToast({
              message: 'Daftar Dibuat!',
            });
            this.loadItems('down');
          } else {
            showToast({
              message: 'Tidak dapat membuat daftar!',
            });
          }
        });
    } else {
      this.setState({
        inputError: true,
      });
      showToast({
        message: 'Nama tidak boleh kosong!',
      });
    }
  };

  showDeleteListAlert = (id) => {
    Alert.alert('Konfirmasi', 'Anda yakin ingin menghapus daftar ini?', [
      {text: 'Membatalkan', style: 'cancel'},
      {text: 'Iya', onPress: () => this.deleteListonLongPress(id)},
    ]);
  };
  deleteListonLongPress = (id) => {
    this.setState({
      loading: true,
    });
    var details = {
      _token: this.state._token,
      user_list_id: id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/deleteFavouriteuserList', formBody)
      .then((response) => {
        console.log('data', response);
        if (response.data.status == 1) {
          this.loadItems();
          showToast({
            message: 'Daftar Dihapus!',
          });
          this.setState({
            showDelete: false,
            selectedIndex: null,
          });
          this.props.navigation.navigate('FavouriteListsforItems');
        } else {
          this.setState({
            loading: false,
          });
          showToast({
            message: 'Gagal menghapus daftar!',
          });
        }
      });
  };

  getShops = () => {
    this.setState({
      loading: true,
    });
    var details = {
      _token: this.state._token,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/favouriteShopList', formBody)
      .then((response) => {
        console.log('data', response);
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          this.setState({
            shopDetails: response.data.new_fav_shop_details,
          });
        } else {
          this.setState({
            shopDetails: null,
          });
        }
      });
  };

  goToShop = (id) => {
    this.props.navigation.navigate('ShopDetails', {shops_id: id});
  };

  removeShop = (id) => {
    this.setState({
      loading: true,
    });
    var details = {
      _token: this.state._token,
      shop_id: id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/removeShopFromFavList', formBody)
      .then((response) => {
        console.log('rem', response);
        if (response.data.status == 1) {
          this.getShops();
          showToast({
            message: 'Dihapus',
          });
        } else {
          this.setState({
            loading: false,
          });
          showToast({
            message: 'Gagal Menghapus toko!',
          });
        }
      });
  };

  showDeleteShop = (id) => {
    Alert.alert(
      'Konfirmasi',
      'Anda yakin ingin menghapus toko ini dari daftar favorit Anda?',
      [
        {text: 'Membatalkan', style: 'cancel'},
        {text: 'Iya', onPress: () => this.removeShop(id)},
      ],
    );
  };

  render() {
    const {
      shopDetails,
      selectedIndex,
      showDelete,
      loadingModal,
      loading,
      inputError,
      seg,
      subFavArray,
      mainFavTotalItems,
      mainFavArray,
      mainFavListName,
    } = this.state;
    const {
      bottonWrapperView,
      textInputStyle,
      textInputStyleError,
      modalHeadingStyle,
      innerModal,
      outerModal,
    } = styles;
    const combinedStyle = StyleSheet.flatten([styles.lastBtn, styles.boxStyle]);
    const combinedStyle2 = StyleSheet.flatten([
      styles.lastBtn,
      styles.boxStyle2,
    ]);
    return (
      <Fragment>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.willShowModal}
          onRequestClose={() => {
            this.changeModalState();
          }}>
          <TouchableWithoutFeedback onPress={this.changeModalState}>
            <View style={outerModal}>
              <TouchableWithoutFeedback>
                <View style={innerModal}>
                  {loadingModal ? (
                    <Spinner color={'red'} />
                  ) : (
                    <Fragment>
                      <Text style={modalHeadingStyle}>Buat Daftar</Text>
                      <View
                        style={{
                          margin: 20,
                          width: '60%',
                        }}>
                        <TextInput
                          style={
                            inputError ? textInputStyleError : textInputStyle
                          }
                          onChangeText={(val) =>
                            this.setState({newListName: val})
                          }
                          value={this.state.newListName}
                          placeholder={'Masukkan nama'}
                        />
                      </View>
                      <View style={bottonWrapperView}>
                        <Butt
                          color={'#C90205'}
                          title={'Membatalkan'}
                          onPress={this.changeModalState}
                        />
                        <Butt title={'Membuat'} onPress={this.createFavList} />
                      </View>
                    </Fragment>
                  )}
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
        <Segment style={styles.segmentStyle}>
          <View style={{flexDirection: 'row'}}>
            <Button
              first
              active={this.state.seg === 1 ? true : false}
              onPress={() => this.setState({seg: 1, showDelete: false})}
              style={
                this.state.seg === 1 ? styles.segmentActive : styles.segmentBtn
              }>
              <Text
                style={
                  this.state.seg === 1
                    ? styles.segmentBtnTxtActive
                    : styles.segmentBtnTxt
                }>
                PRODUK
              </Text>
            </Button>
            <Button
              active={this.state.seg === 2 ? true : false}
              onPress={() => {
                this.setState({seg: 2, showDelete: false});
                this.getShops();
              }}
              style={
                this.state.seg === 2 ? styles.segmentActive : styles.segmentBtn
              }>
              <Text
                style={
                  this.state.seg === 2
                    ? styles.segmentBtnTxtActive
                    : styles.segmentBtnTxt
                }>
                TOKO
              </Text>
            </Button>
          </View>
        </Segment>
        <ScrollView
          ref={this.scrollRef}
          style={{backgroundColor: '#fff', padding: 10}}>
          <View
            style={{
              height: '100%',
              borderColor: '#D3D3D3',
              borderWidth: 0.1,
              paddingBottom: 20,
            }}
            onStartShouldSetResponder={(event) =>
              this.setState({showDelete: false})
            }>
            {loading && (
              <View style={styles.centerSpinner}>
                <Spinner color={'red'} size={'large'} />
              </View>
            )}
            {((seg == 1 && !mainFavArray && !subFavArray && !loading) ||
              (seg == 2 && !shopDetails && !loading)) && (
              // <View style={{
              //     width:'100%',
              //     height:hp('80%'),
              //     justifyContent: 'center',
              //     alignItems: 'center',
              //     marginLeft:'auto',
              //     marginRight:'auto',
              //     alignSelf:'auto',
              // }}>
              //     <Image style={{
              //         width:wp('50%'),
              //         height:hp('50%'),
              //         resizeMode: 'contain',
              //     }} source={require('../images/norecord.png')}/>
              // </View>
              <NoRecords />
            )}
            {seg == 1 && (mainFavArray || subFavArray) && !loading && (
              <View style={styles.wrap}>
                <TouchableOpacity
                  onLongPress={() => this.changeShowDeleteState()}
                  onPress={() => this.goToListContent()}
                  style={styles.touch}>
                  <View style={styles.wrapperBoxStyle}>
                    {mainFavArray &&
                      mainFavArray.map((main, index) => (
                        <Fragment key={index + main.product_id}>
                          {index == 3 && mainFavTotalItems - 3 > 1 ? (
                            <View style={combinedStyle}>
                              <Text style={{color: '#fff'}}>
                                + {mainFavTotalItems - 3}
                              </Text>
                              <Text
                                style={{color: '#fff', fontSize: hp('1.5%')}}>
                                Favorit
                              </Text>
                            </View>
                          ) : (
                            <View style={styles.boxStyle}>
                              <Image
                                style={styles.product_img}
                                source={{uri: main.product_image}}
                              />
                            </View>
                          )}
                        </Fragment>
                      ))}
                  </View>
                  <View
                    style={{
                      ...styles.bottomSection,
                      justifyContent: 'space-between',
                    }}>
                    <Text style={styles.itemsStyle}>
                      {substr(mainFavListName, 15)}
                    </Text>
                    <Text style={styles.totalItems}> {mainFavTotalItems}</Text>
                  </View>
                </TouchableOpacity>
                {subFavArray &&
                  subFavArray.map((sub, index) => (
                    <Fragment key={index}>
                      {showDelete && index == selectedIndex ? (
                        <View style={styles.touchDelete}>
                          <TouchableWithoutFeedback
                            onPress={() =>
                              this.setState({
                                showDelete: false,
                              })
                            }>
                            <TouchableOpacity
                              onPress={() =>
                                this.showDeleteListAlert(sub.sub_list_id)
                              }>
                              <Image
                                style={styles.deleteIcon}
                                source={require('../images/del_icon.png')}
                              />
                              <Text>Hapus Daftar?</Text>
                            </TouchableOpacity>
                          </TouchableWithoutFeedback>
                        </View>
                      ) : (
                        <TouchableOpacity
                          onLongPress={() => this.changeShowDeleteState(index)}
                          style={styles.touch}
                          onPress={() => this.goToListContent(sub.sub_list_id)}>
                          {sub.sub_fav_product_details.length >= 1 ? (
                            <View style={styles.wrapperBoxStyle}>
                              {sub.sub_fav_product_details.map(
                                (subFavProduct, innerIndex) => (
                                  <Fragment
                                    key={innerIndex + subFavProduct.product_id}>
                                    {innerIndex == 3 &&
                                    sub.total_item - 3 > 1 ? (
                                      <View style={combinedStyle}>
                                        <Text style={{color: '#fff'}}>
                                          + {sub.total_item - 3}
                                        </Text>
                                        <Text
                                          style={{
                                            color: '#fff',
                                            fontSize: hp('1.5%'),
                                          }}>
                                          Favorit
                                        </Text>
                                      </View>
                                    ) : (
                                      <View style={styles.boxStyle}>
                                        <Image
                                          style={styles.product_img}
                                          source={{
                                            uri: subFavProduct.product_image,
                                          }}
                                        />
                                      </View>
                                    )}
                                  </Fragment>
                                ),
                              )}
                            </View>
                          ) : (
                            <View style={styles.center}>
                              <Text>Daftar Kosong</Text>
                            </View>
                          )}
                          <View style={styles.bottomSection}>
                            <Text style={styles.itemsStyle}>
                              {substr(sub.sub_fav_name, 15)}
                            </Text>
                            <Text style={styles.totalItems}>
                              {' '}
                              {sub.total_item}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                    </Fragment>
                  ))}
              </View>
            )}
            {seg == 2 && shopDetails && !loading && (
              <Fragment>
                {shopDetails.map((shop, index) => (
                  <TouchableWithoutFeedback
                    onPress={() => this.goToShop(shop.shop_id)}
                    key={index}>
                    <View style={styles.boxWrapper}>
                      <View style={styles.hesdingSection}>
                        <Text style={styles.newShop}>{shop.shop_name}</Text>
                        <Text
                          onPress={() => this.showDeleteShop(shop.shop_id)}
                          style={styles.closeIcon}>
                          X
                        </Text>
                      </View>
                      <View style={styles.userSection}>
                        <Image
                          style={styles.product_img2}
                          source={{uri: shop.shop_image}}
                        />
                        <View style={styles.content_section}>
                          <Text style={styles.shopOwner}>Pemilik toko</Text>
                          <Text style={styles.ownerDetail}>
                            {shop.owner_name}
                          </Text>
                        </View>
                      </View>
                      <View style={styles.wrapperBoxStyle}>
                        {shop.prod_details.map((product, innerIndex) => (
                          <Fragment key={innerIndex}>
                            {innerIndex == 3 &&
                            shop.total_prod_record - 3 > 1 ? (
                              <View style={combinedStyle2}>
                                <Text style={{color: '#fff'}}>
                                  + {shop.total_prod_record - 3}
                                </Text>
                                <Text
                                  style={{color: '#fff', fontSize: hp('1.5%')}}>
                                  Favorit
                                </Text>
                              </View>
                            ) : (
                              <View style={styles.boxStyle2}>
                                <Image
                                  style={styles.shopItemImg}
                                  resizeMode="contain"
                                  source={{uri: product.image}}
                                />
                              </View>
                            )}
                          </Fragment>
                        ))}
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                ))}
              </Fragment>
            )}
          </View>
        </ScrollView>
        {seg == 1 && (
          <TouchableWithoutFeedback onPress={this.changeModalState}>
            <Image
              resizeMode={'contain'}
              style={styles.plusBtn}
              source={require('../images/red_plus.png')}
            />
          </TouchableWithoutFeedback>
        )}
      </Fragment>
    );
  }
}
const styles = StyleSheet.create({
  deleteIcon: {
    height: hp('10%'),
    width: wp('20%'),
  },
  centerSpinner: {
    alignItems: 'center',
    justifyContent: 'center',
    height: hp('82%'),
  },
  bottonWrapperView: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    margin: 20,
    width: '100%',
  },
  textInputStyle: {
    height: 40,
    borderRadius: 10,
    // width:'100%',
    borderColor: 'gray',
    borderWidth: 1,
  },
  textInputStyleError: {
    height: 40,
    borderRadius: 10,
    // width:'100%',
    borderColor: 'red',
    borderWidth: 2,
  },
  modalHeadingStyle: {
    color: '#545454',
    fontSize: hp('2.5%'),
    fontFamily: Font.RobotoRegular,
    textAlign: 'center',
  },
  innerModal: {
    width: wp('80%'),
    height: hp('30%'),
    backgroundColor: '#fff',
    padding: 20,
    zIndex: 5,
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  outerModal: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  plusBtn: {
    position: 'absolute',
    width: 37,
    height: 37,
    right: 30,
    bottom: 30,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '90%',
    // width: '100%'
  },
  wrap: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    width: '100%',
    height: '100%',
    marginBottom: 10,
  },
  touch: {
    borderRadius: 2,
    width: '48%',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: '#dddede',
    padding: 10,
    marginBottom: 10,
    height: hp('30%'),
  },
  touchDelete: {
    borderRadius: 2,
    width: '48%',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#dddede',
    padding: 10,
    marginBottom: 10,
    height: hp('30%'),
  },
  segmentBtn: {
    borderWidth: 0,
    borderColor: 'transparent',
    bottom: 0,
    margin: 0,
    paddingTop: 10,
    paddingBottom: 10,
    height: 'auto',
    marginBottom: 0,
    width: wp('48%'),
  },
  segmentBtnTxt: {
    color: '#e6e6e6',
    textAlign: 'center',
    width: '100%',
  },
  segmentBtnTxtActive: {
    color: '#606060',
    textAlign: 'center',
    width: '100%',
  },
  segmentActive: {
    backgroundColor: '#e6e6e6',
    borderWidth: 0,
    borderColor: 'transparent',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    paddingTop: 10,
    height: 'auto',
    paddingBottom: 10,
    color: '#e6e6e6',
    width: wp('48%'),
  },
  segmentStyle: {
    backgroundColor: '#b90002',
    textAlign: 'center',
    justifyContent: 'center',
    paddingLeft: 30,
    height: 'auto',
    paddingTop: 10,
    paddingBottom: 0,
    paddingRight: 30,
    alignItems: 'center',
    width: '100%',
  },
  wrapperBoxStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    // margin: 2,
    justifyContent: 'space-between',
    width: '100%',
    // flex:1
  },
  lastBtn: {
    backgroundColor: '#727272',
    color: '#fff',
  },
  boxStyle: {
    borderRadius: 2,
    elevation: 1,
    borderWidth: 1,
    borderColor: '#dddede',
    width: '49%',
    marginBottom: 5,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  boxStyle2: {
    borderRadius: 2,
    elevation: 1,
    borderWidth: 1,
    borderColor: '#dddede',
    width: '24%',
    marginBottom: 5,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  product_img: {
    width: wp('14%'),
    height: hp('10%'),
    resizeMode: 'contain',
  },

  bottomSection: {
    width: '100%',
    // padding: 4,
    flexDirection: 'row',
  },
  itemsStyle: {
    color: '#DC0028',
    fontSize: hp('1.8%'),
    fontFamily: Font.RobotoMedium,
  },
  totalItems: {
    color: '#666',
    fontSize: hp('1.8%'),
    fontFamily: Font.RobotoRegular,
  },
  // second box
  hesdingSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  newShop: {
    color: '#727272',
    fontSize: hp('2.8%'),
    fontFamily: Font.RobotoBold,
  },
  closeIcon: {
    backgroundColor: '#DC0028',
    width: wp('5%'),
    height: hp('2.5%'),
    textAlign: 'center',
    color: '#fff',
  },
  boxWrapper: {
    padding: 5,
    margin: 5,
    borderWidth: 1,
    borderColor: '#dddede',
  },
  userSection: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },

  product_img2: {
    width: 70,
    height: 80,
    resizeMode: 'contain',
  },
  content_section: {
    flex: 1,
    paddingLeft: 15,
  },
  shopOwner: {
    color: '#999',
    fontSize: hp('1.5%'),
    fontFamily: Font.RobotoRegular,
  },
  ownerDetail: {
    color: '#999',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
  },

  shopItemImg: {
    width: wp('14%'),
    height: hp('10%'),
  },
});

export default FavouriteListsforItems;
