import React, {Component} from 'react';
import {AsyncStorage} from 'react-native';
import {WebView} from 'react-native-webview';
import base64 from 'react-native-base64';

class VideoProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product_video_url:
        props.route.params && props.route.params.product_video_url,
    };
  }

  render() {
    console.log('ini props', JSON.stringify(this.props));
    return (
      <>
        <WebView
          source={{
            uri: this.state.product_video_url,
          }}
        />
      </>
    );
  }
}

export default VideoProduct;
