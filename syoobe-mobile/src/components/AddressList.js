import React, {Component} from 'react';
import {Alert, ScrollView, TouchableOpacity, View, Text} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {Font} from './Font';
import {RadioButton} from 'react-native-paper';
import axios from 'axios';
import {Button, Spinner} from './common';
import {selectAddressPayment} from '../actions';

class AddressList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checked: props.route.params.ua_id,
      _token: props.route.params._token,
      type: props.route.params.type,
      loading: true,
      addressList: [],
    };
  }

  componentDidMount = () => {
    var details = {
      _token: this.state._token,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/changeAddressList', formBody)
      .then((response) => {
        if (response.data.status == 1) {
          this.setState({
            addressList: response.data.change_address_list,
            loading: false,
          });
        } else {
          this.setState({
            loading: false,
          });
        }
        console.log('r', response);
      });
  };

  deleteConfirm = async (id) => {
    this.setState({
      loading: true,
    });
    var details = {
      _token: this.state._token,
      ua_id: id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/deleteShippingAddressInCart', formBody)
      .then((response) => {
        if (response.data.status == 1) {
          let newAddressList = [...this.state.addressList];
          newAddressList.forEach((address, index) => {
            if (address.ua_id == id) {
              newAddressList.splice(index, 1);
            }
          });
          this.setState({
            loading: false,
            addressList: newAddressList,
          });
          Alert.alert('Address deleted!');
        }
      });
  };

  selectAddress = () => {
    if (this.props.route.params.ua_id == this.state.checked) {
      this.props.navigation.navigate('Payment');
    } else {
      var details = {
        _token: this.state._token,
        ua_id: this.state.checked,
      };
      console.log(details);
      this.props.selectAddressPayment(this.state.type, details);
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.billing != null && this.props.shipping != null) {
      if (
        prevProps.billing.ua_id != this.props.billing.ua_id ||
        prevProps.shipping.ua_id != this.props.shipping.ua_id
      ) {
        this.props.navigation.navigate('Payment');
        console.log('psuh payment');
      }
    }
  };

  deleteAddress = (id) => {
    Alert.alert('Confirm', 'Are you sure want to delete this addres?', [
      {text: 'Cancel', style: 'cancel'},
      {text: 'OK', onPress: () => this.deleteConfirm(id)},
    ]);
  };

  render() {
    const {checked} = this.state;
    if (this.state.loading || this.props.loading) {
      return <Spinner color="red" size="large" />;
    }
    const address_list_length = this.state.addressList.length > 0;
    return (
      <View
        style={{
          backgroundColor: '#fff',
          height: '100%',
          paddingBottom: hp('7%'),
        }}>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('AddOrEditAddress', {
              typeHeader: 'Tambah alamat baru',
              _token: this.state._token,
              type: this.state.type,
            })
          }>
          <Text style={styles.addHeader}>+ Tambah alamat baru</Text>
        </TouchableOpacity>
        <ScrollView>
          {this.state.addressList.map((address, index) => (
            <TouchableOpacity
              onPress={() => {
                this.setState({checked: address.ua_id});
              }}
              key={address.ua_id}
              style={styles.sectionRow}>
              <RadioButton
                value={address.ua_id}
                color={'#00b3ff'}
                uncheckedColor={'#29abe2'}
                status={checked === address.ua_id ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({checked: address.ua_id});
                }}
              />
              <View style={styles.contentDiv}>
                <Text style={styles.UserName}>{address.ua_name}</Text>
                {address.address2 != '' ? (
                  <Text style={styles.userAddress}>
                    {address.address1}, {address.address2}, {address.city},{' '}
                    {address.state_name} - {address.zip}, {address.country_name}
                  </Text>
                ) : (
                  <Text style={styles.userAddress}>
                    {address.address1}, {address.city}, {address.state_name} -{' '}
                    {address.zip}, {address.country_name}
                  </Text>
                )}
                <Text style={styles.userMobile}>{address.phn_no}</Text>
              </View>
              {checked == address.ua_id && (
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('AddOrEditAddress', {
                      typeHeader: 'Edit Alamat',
                      address_id: address.ua_id,
                      _token: this.state._token,
                      type: this.state.type,
                    })
                  }
                  style={styles.buttonStyle}>
                  <Text style={styles.textStyle}>Edit</Text>
                </TouchableOpacity>
              )}
              {checked != address.ua_id && (
                <TouchableOpacity
                  onPress={() => this.deleteAddress(address.ua_id)}
                  style={styles.buttonStyle}>
                  <Text style={styles.textStyle}>Delete</Text>
                </TouchableOpacity>
              )}
            </TouchableOpacity>
          ))}
        </ScrollView>
        {address_list_length && (
          <View
            style={{
              position: 'absolute',
              bottom: 5,
              width: '100%',
              paddingHorizontal: wp(3),
            }}>
            <Button onPress={() => this.selectAddress()}>Pilih</Button>
            {/* <TouchableOpacity
              onPress={() => this.selectAddress()}
              style={styles.bottom_buttonStyle}>
              <Text style={styles.bottom_textStyle}>SELECT ADDRESS</Text>
            </TouchableOpacity> */}
          </View>
        )}
      </View>
    );
  }
}

const styles = {
  addHeader: {
    fontSize: hp('2.5%'),
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    paddingRight: 15,
    color: '#c90305',
    backgroundColor: '#fff',
    marginBottom: 10,
    elevation: 5,
  },
  sectionRow: {
    flexDirection: 'row',
    overflow: 'hidden',
    backgroundColor: '#fff',
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    alignItems: 'flex-start',
  },
  buttonStyle: {
    // width: 35,
    marginRight: 10,
    borderWidth: 1,
    borderColor: '#c90305',
    padding: 5,
    borderRadius: 5,
  },

  textStyle: {
    color: '#c90305',
    fontSize: hp('2%'),
  },
  contentDiv: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
  },
  UserName: {
    fontSize: hp('2.4%'),
    marginBottom: 7,
    fontFamily: Font.RobotoMedium,
  },
  userAddress: {
    fontSize: hp('2.1%'),
    marginBottom: 5,
    fontFamily: Font.RobotoRegular,
  },
  userMobile: {
    fontSize: hp('2.1%'),
    fontFamily: Font.RobotoRegular,
  },
  bottom_buttonStyle: {
    width: '100%',
    // backgroundColor: '#00b3ff',
    backgroundColor: '#C90205',
    // C90205
    // position: 'absolute',
    // bottom: 0,
  },
  bottom_textStyle: {
    textAlign: 'center',
    fontSize: hp('2.7%'),
    color: '#fff',
    paddingTop: 10,
    paddingBottom: 10,
    fontFamily: Font.RobotoRegular,
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.payment.loading,
    billing: state.payment.billing_address,
    shipping: state.payment.shipping_address,
  };
};

export default connect(mapStateToProps, {selectAddressPayment})(AddressList);
