import axios from 'axios';
import React, {Component} from 'react';
import {Alert} from 'react-native';
import {KeyboardAvoidingView, ScrollView, Text, View} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect} from 'react-redux';
import {getOrderDetails} from '../actions';
import {substr} from '../helpers/helper';
import {Button, CardSection, Spinner} from './common';
import InputText from './common/InputText';
import {ProductFileUpload} from './common/product/ProductFileUpload';
import Bugsnag from '@bugsnag/react-native';

class SendRevision extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      _token: props.route.params._token,
      order_product_id: props.route.params.order_product_id,
      muf_id: props.route.params.muf_id,
      muf_opr_id: props.route.params.muf_opr_id,
      files: [],
      fileNames: '',
      subject: 'Revisi_' + props.route.params.muf_id,
      message: '',
    };
  }

  selectFile = async (fileType, item) => {
    try {
      const file = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      await this.setState({
        loading: true,
      });
      this.setState({
        files: file,
      });

      this.setState({
        fileNames: file.name,
        loading: false,
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        Bugsnag.notify(err);
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw Bugsnag.notify(err);
      }
    }
  };

  onSubmit = async () => {
    if (this.state.subject !== '' && this.state.message !== '') {
      this.setState({
        loading: true,
      });
      var formData = new FormData();
      const file = {
        name: this.state.files.name,
        uri: this.state.files.uri,
        type: this.state.files.type,
      };

      formData.append('_token', this.state._token);
      formData.append('muf_id', this.state.muf_id);
      formData.append('opr_id', this.state.muf_opr_id);
      formData.append('thread_subject', this.state.subject);
      formData.append('message_text', this.state.message);
      formData.append('requirement_file', file);
      await axios
        .post(
          'https://syoobe.co.id/api/revisionRequestForGraphicsProduct',
          formData,
        )
        .then((response) => {
          console.log(
            'response revisionRequestForGraphicsProduct',
            JSON.stringify(response),
          );
          if (response.data.status === 1) {
            this.setState({
              loading: false,
            });
            this.props.getOrderDetails(this.props.route.params.details);
            Alert.alert('Informasi', response.data.msg, [
              {
                text: 'OK',
                onPress: () =>
                  this.props.navigation.replace('HomeDrawer', {
                    screen: 'Sales',
                  }),
              },
            ]);
          } else {
            this.setState({
              loading: false,
            });
            Alert.alert('Informasi', response.data.msg, [
              {
                text: 'OK',
                onPress: () =>
                  this.props.navigation.replace('HomeDrawer', {
                    screen: 'Sales',
                  }),
              },
            ]);
          }
        })
        .catch((err) => {
          console.log('e', err);
          this.setState({
            loading: false,
          });
        });
    } else {
      Alert.alert('Internal Server Error');
    }
  };

  render() {
    console.log('ini props ooo', JSON.stringify(this.state));
    // console.log('ini subject', this.state.subject);
    // console.log('ini message', this.state.message);
    // console.log('ini filrname', this.state.fileNames);
    // console.log('ini file', this.state.files);

    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Spinner color="red" size="large" />
        </View>
      );
    }

    return (
      <View style={{flex: 1}}>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          style={styles.scrollClass}>
          {/* {this.state.loading && <Spinner2 visible={this.state.loading} />} */}
          <View
            style={{
              marginleft: 5,
              marginRight: 5,
              marginTop: 10,
              height: '100%',
              padding: 15,
              position: 'relative',
              flex: 1,
            }}>
            <CardSection>
              <View style={{flex: 1}}>
                <Text>Subjek</Text>

                <Text style={[styles.textInputContainer, styles.shadow]}>
                  {this.state.subject}
                </Text>
              </View>

              {/* <InputText
                value={this.state.subject}
                onChangeText={(value) => this.setState({subject: value})}
                label="Judul"
                card={true}
              /> */}
            </CardSection>
            <CardSection>
              <InputText
                value={this.state.message}
                onChangeText={(value) => this.setState({message: value})}
                label="Keterangan"
                multiline
                numberOfLines={6}
              />
            </CardSection>

            <ProductFileUpload
              // index={i}
              loading={this.state.loading}
              onPress={({type, index, ref, item}) => {
                ref.close();
                this.selectFile(type, item);
              }}
              fileName={this.state.fileNames}
            />
          </View>
        </ScrollView>
        <KeyboardAvoidingView behavior={'height'} enabledtest>
          <View
            style={{
              paddingHorizontal: wp(3),
              paddingVertical: hp(1),
              backgroundColor: '#fff',
              borderTopColor: '#dcdde1',
              borderTopWidth: 0.5,
            }}>
            <Button
              onPress={() => {
                this.onSubmit();
              }}>
              Kirim Persyaratan
            </Button>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export const FileUploadList = ({
  data,
  item,
  index,
  size,
  comments = null,
  sparator = true,
}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomColor: '#dcdde1',
        paddingVertical: hp(1.5),
        borderBottomWidth: sparator ? (data.length - 1 === index ? 0 : 0.5) : 0,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          // justifyContent: 'space-between',
        }}>
        <MaterialCommunityIcons
          name={item.item.icon}
          size={item.item.size ? item.item.size : 60}
          color={item.item.color}
        />
        <View style={{marginLeft: 10}}>
          <Text style={{fontSize: hp(2.2), fontWeight: 'bold'}}>
            {item.name.length < 25
              ? item.name
              : `            ${`${substr(item.name)}.${
                  item.name.split('.')[1]
                }`}`}
            {/* Nama File Nama File Nama File .png */}
          </Text>
          {size && (
            <Text style={{fontSize: hp(2.2)}}>{`${
              item.size >= 9000000000
                ? `${Math.round(item.size / 1000) / 1000} MB`
                : `${Math.round(item.size / 1000)} KB`
            }`}</Text>
          )}
          {comments && !size && (
            <Text style={{fontSize: hp(2.2)}}>{comments}</Text>
          )}
        </View>
      </View>
      {/* <FontAwesome5Icon size={hp(2)} color={'red'} name={'times'} /> */}
    </View>
  );
};

const styles = {
  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
  },
  textInputContainer: {
    borderRadius: 5,
    width: '100%',
    // height: '100%',
    marginVertical: hp(1),
    paddingVertical: hp(1),
    paddingHorizontal: wp(3),
  },
  shadow: {
    shadowColor: '#eaeaea',
    shadowOffset: {
      width: 15,
      height: 4,
    },
    shadowOpacity: 0.1,
    shadowRadius: 12.22,
    elevation: 2,
  },
  textInput: {
    paddingHorizontal: wp(3),
    width: '100%',
    position: 'relative',
    zIndex: -99,
  },
};

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {getOrderDetails})(SendRevision);
