import axios from 'axios';
import React, {Component} from 'react';
import {
  Alert,
  AsyncStorage,
  DatePickerAndroid,
  Image,
  Modal,
  ScrollView,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import DocumentPicker from 'react-native-document-picker';
import {Snackbar} from 'react-native-paper';
import {AirbnbRating} from 'react-native-ratings';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {connect} from 'react-redux';
import {addToCart, fetchRecentlyViewed, viewCart} from '../actions';
import {formatRupiah, substr} from '../helpers/helper';
import CarousalNew from './CarousalNew';
import {Spinner} from './common';
import {ProductDescription} from './common/product/ProductDescription';
import {ProductPortfolio} from './common/product/ProductPortfolio';

import {FooterActionButton} from './common/product/ProductDetailFooter';
import {ProductDetailInformation} from './common/product/ProductDetailInformation';
import {ProductCard} from './common/product/ProductItem';
import {Font} from './Font';
import {ProductCardSection} from './product';
import ProductFeatureCom from './ProductFeatureCom';
import Share from 'react-native-share';

// import FavouriteButton from './FavouriteButton';

class ProductDetail extends Component {
  copyItems = 0;
  product_requires_shipping = {};
  product_id = {};
  constructor(props) {
    super(props);

    this.state = {
      images: [],
      productDetails: {},
      totalItems: 1,
      loading: true,
      modalVisible: false,
      _token: null,
      device_token: null,
      user_product: false,
      isDateTimePickerVisible: false,
      option: [],
      snackbarVisible: false,
      productShareLink: null,
      shops_id: null,
      error: false,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  setHeaderName = (name) => {
    const {setParams} = this.props.navigation;
    setParams({product_name: name});
  };

  fetchData = async () => {
    console.log('fetchData');
    const deviceId = DeviceInfo.getUniqueId();
    this.setState({
      device_token: deviceId,
    });
    await this.retrieveToken();
    this.product_id = this.props.route.params.product_id;
    this.product_requires_shipping = this.props.route.params.product_requires_shipping;
    this.setState({
      user_product: this.props.route.params.user_product,
    });
    var details = {};
    if (this.state._token) {
      details = {
        ...details,
        ...{_token: this.state._token},
        ...{product_id: this.product_id},
      };
    } else {
      details = {
        ...details,
        ...{product_id: this.product_id},
      };
    }
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    console.log(formBody);
    await axios
      .post('https://syoobe.co.id/api/product_details', formBody)
      .then((response) => {
        // console.log('ini response', JSON.stringify(response));
        if (response.data.status == 1) {
          if (response.data.data.product_options.length > 0) {
            let array = [];
            response.data.data.product_options.map((item) => {
              if (item.type == 'select') {
                let options = {
                  product_option_id: item.product_option_id,
                  option_value:
                    item.product_option_value[0].product_option_value_id,
                };
                array.push(options);
              } else if (item.type == 'checkbox') {
                let opArray = [];
                item.product_option_value.map((innerOp) => {
                  opArray.push({
                    valueSelected: innerOp.product_option_value_id,
                    isBoolean: false,
                  });
                });
                let options = {
                  product_option_id: item.product_option_id,
                  option_value: opArray,
                };
                array.push(options);
              } else {
                let options = {
                  product_option_id: item.product_option_id,
                  option_value: item.value,
                };
                array.push(options);
              }
            });
            this.setState({
              option: array,
            });
          }

          this.setState({
            productShareLink:
              response.data.data.product_view_path_for_social_media,
            productDetails: response.data.data,
            loading: false,
          });
          this.setHeaderName(response.data.data.product_name);
        } else {
          this.setState({loading: false, error: true});
        }
      })
      .catch((err) => console.log(err.response));
  };

  componentDidMount = async () => {
    console.log('componentDidMount');
    await this.fetchData();
    AsyncStorage.getItem('shop_id').then((res) =>
      this.setState({shops_id: res}),
    );
  };

  componentDidUpdate = async (prevProps, prevState) => {
    if (
      prevState.productDetails.product_id !==
      this.state.productDetails.product_id
    ) {
      var details = {};
      details = {
        ...details,
        ...{product_id: this.state.productDetails.product_id},
      };
      if (this.state.device_token != null) {
        details = {
          ...details,
          ...{device_token: this.state.device_token},
        };
      }
      if (this.state._token != null) {
        details = {
          ...details,
          ...{_token: this.state._token},
        };
      }
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post(
          'https://syoobe.co.id/api/recentlyViewedProductStoreAPI',
          formBody,
        )
        .then((recent_response) => {
          this.props.fetchRecentlyViewed(details);
        });
    }
  };

  renderCarousal = (images) => {
    if (images) {
      return <CarousalNew {...this.props} carousalImages={images} />;
    }
  };

  renderRecommendedProducts = () => {
    if (this.state.productDetails.recommended_more_products != null) {
      // const recommendedProducts = this.state.productDetails.seller_more_products.slice(0, 2);
      return this.state.productDetails.recommended_more_products.map(
        (product, index) => (
          <ProductFeatureCom
            tab={'nonRecent'}
            key={index}
            {...this.props}
            details={product}
          />
        ),
      );
    }
  };

  renderRelatedProducts = () => {
    if (this.state.productDetails.related_products != null) {
      // const recommendedProducts = this.state.productDetails.seller_more_products.slice(0, 2);
      return this.state.productDetails.related_products.map(
        (product, index) => (
          <ProductCard key={index} {...this.props} row={product} />
        ),
      );
    }
  };

  increment = () => {
    this.copyItems++;
    this.setState({
      totalItems: this.copyItems,
    });
  };

  decrement = () => {
    this.copyItems--;
    this.setState({
      totalItems: this.copyItems,
    });
  };

  requestAddToCart = async (ref) => {
    // console.log(ref);
    if (this.state.totalItems < 1) {
      Alert.alert('Jumlah produk harus lebih besar dari 0');
    } else {
      await this.retrieveToken()
        .then((_token) => {
          if (_token) {
            const quantity = this.state.totalItems;
            this.props
              .addToCart({
                _token,
                product_id: this.product_id,
                quantity,
                option: this.state.option,
              })
              .then(() => {
                ref.close();
                this.setState((state) => ({
                  snackbarVisible: true,
                }));

                setTimeout(() => {
                  this.setState((state) => ({
                    snackbarVisible: false,
                  }));
                }, 2000);
              });
            setTimeout(() => this.props.viewCart({_token}), 2000);
          } else {
            this.props.navigation.navigate('AuthDrawer', {screen: 'Login'});
            Alert.alert('Silakan masuk untuk mulai membeli!');
          }
        })
        .catch((error) => {
          console.log('Promise is rejected with error: ' + error);
        });
    }
  };

  buyNow = async (ref) => {
    if (this.state.totalItems < 1) {
      Alert.alert('Jumlah produk harus lebih besar dari 0');
    } else {
      await this.retrieveToken()
        .then((_token) => {
          if (_token) {
            this.props.viewCart({_token});
            if (this.props.cart_count > 0) {
              Alert.alert(
                'Pesan konfirmasi',
                'Troli Anda tidak kosong.' +
                  ' Harap hapus konten keranjang Anda atau isi pesanan sebelum melanjutkan untuk membeli produk digital apa pun',
                [
                  {
                    text: 'Membatalkan',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {
                    text: 'Oke Bawa saya ke Troli',
                    onPress: () => {
                      ref.close();
                      this.props.navigation.navigate('MyCart');
                    },
                  },
                ],
                // { cancelable: false }
              );
            } else {
              const quantity = this.state.totalItems;
              const product_id = this.product_id;
              this.props.addToCart({
                _token,
                product_id,
                quantity,
                option: this.state.option,
              });
              setTimeout(() => {
                if (!this.props.cart_error) {
                  this.props.navigation.navigate('MyCart', {
                    product_description: 'digital',
                  });
                  ref.close();
                }
              }, 2000);
            }
          } else {
            ref.close();
            this.props.navigation.navigate('AuthDrawer', {screen: 'Login'});
            Alert.alert('Silakan masuk untuk mulai membeli!');
          }
        })
        .catch((error) => {
          console.log('Promise is rejected with error: ' + error);
        });
    }
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  GoToCompat = (header, values) => {
    this.props.navigation.navigate('ProductDetailsInfo', {
      pageHeader: 'Compatibility',
      header: header,
      values: values,
      price_currency: this.state.productDetails.price_currency,
      store_name: this.state.productDetails.store_name,
    });
  };

  GoToSpec = (specifications) => {
    this.props.navigation.navigate('ProductDetailsInfo', {
      pageHeader: 'Specifications',
      spec: specifications,
      price_currency: this.state.productDetails.price_currency,
    });
  };

  GoToPolicies = (policies) => {
    this.props.navigation.navigate('ProductDetailsInfo', {
      pageHeader: 'Policies',
      store_name: this.state.productDetails.store_name,
      policies: policies,
      price_currency: this.state.productDetails.price_currency,
    });
  };

  checkBox = (isBoolean, valueSelected, index, innerIndex) => {
    // console.log(isBoolean, valueSelected, index, innerIndex);
    this.setState((state) => {
      const list = state.option.map((item, i) => {
        if (index === i) {
          return (item.option_value[innerIndex].isBoolean = isBoolean);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  handleDatePicked = () => {
    this.setState({
      isDateTimePickerVisible: true,
    });
  };

  hideDateTimePicker = () => {
    this.setState({
      isDateTimePickerVisible: false,
    });
  };

  openDatePicker = async (value, index) => {
    let dataArray = String(value).split('-');
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        date: new Date(dataArray[0], --dataArray[1], dataArray[2]),
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        let createdDate = year + '-' + month + '-' + day;
        this.setState((state) => {
          const list = state.option.map((item, i) => {
            if (index === i) {
              return (item.option_value = createdDate);
            } else {
              return item;
            }
          });
          return {
            list,
          };
        });
      }
    } catch ({code, message}) {
      console.warn('Cannot open date picker', message);
    }
  };

  setTimeAndDate = async (value, index) => {
    this.setState((state) => {
      const list = state.option.map((item, i) => {
        if (index === i) {
          return (item.option_value = value);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  openDatePickerDateTime = async (value, time, index) => {
    console.log('time', new Date());
    let dataArray = String(value).split('-');
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        date: new Date(value),
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        let createdDate = year + '-' + (parseInt(month) + 1) + '-' + day;
        this.setState((state) => {
          const list = state.option.map((item, i) => {
            if (index === i) {
              return (item.option_value = createdDate + (' ' + time));
            } else {
              return item;
            }
          });
          return {
            list,
          };
        });
      }
    } catch ({code, message}) {
      console.warn('Cannot open date picker', message);
    }
  };

  openTimePickerDateTime = async (value, date, index) => {
    this.setState((state) => {
      const list = state.option.map((item, i) => {
        if (index === i) {
          return (item.option_value = date + ' ' + value);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  selectRadio = (valueToSet, value, index) => {
    // console.log(valueToSet, value, index);
    if (value != valueToSet) {
      this.setState((state) => {
        const list = state.option.map((item, i) => {
          if (index === i) {
            return (item.option_value = valueToSet);
          } else {
            return item;
          }
        });
        return {
          list,
        };
      });
    }
  };

  selectBox = (valueToSet, index) => {
    this.setState((state) => {
      const list = state.option.map((item, i) => {
        if (index === i) {
          return (item.option_value = valueToSet);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };
  changeText = (val, index) => {
    this.setState((state) => {
      const list = state.option.map((item, i) => {
        // console.log(index === i);
        if (index === i) {
          return (item.option_value = val);
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
  };

  uploadFile = async (index) => {
    var url = 'https://syoobe.co.id/api/file_upload_in_add_to_cart_app_api';
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      const formData = new FormData();
      formData.append('_token', this.state._token);
      formData.append('file', {
        name: res.name,
        uri: res.uri,
        type: res.type,
      });
      this.xmlRequest('POST', url, formData, index);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  xmlRequest = (method, url, data, index) => {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.upload.onprogress = (progressEvent) => {
      var total = progressEvent.total;
      var loaded = progressEvent.loaded;
      var percent = Math.trunc((loaded / total) * 100);
      this.setState((state) => {
        const list = state.option.map((item, i) => {
          if (index === i) {
            item.option_value = percent;
            return item;
          } else {
            return item;
          }
        });
        return {
          list,
        };
      });
    };
    xhr.setRequestHeader('Content-type', 'multipart/form-data');
    xhr.onload = this.onContentUploadSuccess_.bind(this, index);
    xhr.onerror = this.onContentUploadError_.bind(this, index);
    xhr.send(data);
  };

  onContentUploadSuccess_ = (index, value) => {
    if (JSON.parse(value.target._response).status === 1) {
      this.setState((state) => {
        const list = state.option.map((item, i) => {
          if (index === i) {
            item.option_value = JSON.parse(value.target._response).file_name;
            return item;
          } else {
            return item;
          }
        });
        return {
          list,
        };
      });
    } else {
      Alert.alert(JSON.parse(value.target._response).data);
    }
  };

  onContentUploadError_ = (index, error) => {
    this.setState((state) => {
      const list = state.option.map((item, i) => {
        if (index === i) {
          item.option_value = '';
          return item;
        } else {
          return item;
        }
      });
      return {
        list,
      };
    });
    Alert.alert('Upload failed. Please try again!');
  };

  goToShop = (id) => {
    if (id === this.state.shops_id) {
      this.props.navigation.navigate('ShopDetails');
    } else {
      this.props.navigation.navigate('ShopDetails', {shops_id: id});
    }
  };

  _toggleShow = () => {
    var str = this.state.productShareLink.toLowerCase();
    var res = str.replace(' ', '-');
    var shareLink = res.replace('com', 'co.id');
    let shareOptions = {
      title: 'Bagikan',
      url: shareLink,
      subject: 'Temukan ' + this.state.productDetails.product_name,
      message:
        'Temukan ' +
        this.state.productDetails.product_name +
        ' hanya di Syoobe.',
    };
    Share.open(shareOptions);
  };

  render() {
    console.log(
      'ini state',
      JSON.stringify(this.state.productDetails.product_brief),
    );
    // console.log('ini props', JSON.stringify(this.props));
    // console.log('ini state', this.state.productShareLink.replace(' ', '-'));

    // console.log(
    //   'ini shop id',
    //   JSON.stringify(this.state.productDetails.store_id),
    // );

    // console.log('ini state shop id', JSON.stringify(this.state.shops_id));

    const {
      store_state_name,
      images,
      product_brief,
      product_name,
      shipping_details,
      product_brand,
      store_name,
      product_price,
      price_currency,
      specification_details,
      return_request_details,
      product_review_details,
      product_review_count,
      product_rating,
      product_options,
      product_type,
      is_free_shipping,
      product_model,
      product_sku,
      comp_attribute_details,
      final_compatibility_result,
      compatibility_form_display,
      duration_time,
      store_id,
      product_length_and_height,
      product_weight_details,
      product_available_date,
      product_condition,
      product_portfolio,
      product_video_url,
    } = this.state.productDetails;

    // console.log(JSON.stringify(this.state.productDetails));

    const {recomendedTxt, carousalStyle, critiStyle, descriptionTxt} = styles;

    var ratingsArray = [];
    var rounded_off_rating = null;
    if (product_rating) {
      ratingsArray = String(product_rating).split('.');
      if (ratingsArray[1] >= 5) {
        rounded_off_rating = ratingsArray[0] + 1;
      } else {
        rounded_off_rating = ratingsArray[0];
      }
    }

    // console.log(product_brief);
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color="red" size="large" />
        </View>
      );
    }

    if (this.state.error) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <FontAwesome5Icon name="exclamation-triangle" size={hp('8%')} />
          {/* <Spinner color="red" size="large" /> */}
          <Text style={styles.labelStyle2}>
            Data Tidak Ada atau Dihapus oleh penjual
          </Text>
        </View>
      );
    }

    return (
      <View style={{flex: 1}}>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'rgba(0,0,0,0.5)',
            }}>
            <View
              style={{
                width: wp('80%'),
                maxHeight: hp('50%'),
                backgroundColor: '#fff',
                padding: 20,
                zIndex: 5,
                position: 'relative',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <ScrollView>
                <Text
                  style={{
                    color: '#545454',
                    fontSize: hp('3%'),
                    paddingBottom: 0,
                    fontFamily: Font.RobotoRegular,
                    textAlign: 'center',
                  }}>
                  {' '}
                  Keterangan Produk{' '}
                </Text>
                {String(product_brief).trim() != '' ? (
                  <Text
                    style={{
                      color: '#858585',
                      fontSize: hp('2.2%'),
                      fontFamily: Font.RobotoRegular,
                      textAlign: 'center',
                    }}>
                    {product_brief}
                  </Text>
                ) : (
                  <Text
                    style={{
                      color: '#858585',
                      fontSize: hp('2.2%'),
                      fontFamily: Font.RobotoRegular,
                      textAlign: 'center',
                    }}>
                    Produk ini tidak memiliki deskripsi apa pun.
                  </Text>
                )}
              </ScrollView>
              <TouchableOpacity
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
                style={styles.closeBtn2}>
                <Text style={styles.crossColor}>&#10005;</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <View
          style={{
            flex: 1,
            position: 'absolute',
            bottom: hp(10),
            left: 0,
            right: 0,
            zIndex: 999,
          }}>
          <Snackbar visible={this.state.snackbarVisible}>
            Produk ditambahkan ke keranjang
          </Snackbar>
        </View>
        <ScrollView style={{backgroundColor: '#f5f6fa'}}>
          <View style={carousalStyle}>{this.renderCarousal(images)}</View>
          <View
            style={{
              paddingHorizontal: wp(3),
              paddingVertical: hp(1),
              backgroundColor: '#fff',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontSize: hp(3),
                  fontWeight: 'bold',
                  textTransform: 'capitalize',
                  color: '#e74c3c',
                  paddingRight: wp(3),
                }}>
                {`Rp. ${formatRupiah(product_price)}`}
              </Text>

              {is_free_shipping == 'Y' && (
                <View style={{padding: 5}}>
                  <Image
                    resizeMode="stretch"
                    style={styles.freeShipImg}
                    source={require('../images/free_shipping.png')}
                  />
                </View>
              )}
              {product_type == 'I' && (
                <View style={{padding: 5}}>
                  <Image
                    resizeMode="cover"
                    style={styles.donwloadIcon}
                    source={require('../images/download.png')}
                  />
                </View>
              )}
              {product_type == 'G' && (
                <View style={{padding: 5}}>
                  <Image
                    resizeMode="cover"
                    style={styles.graphicdesignIcon}
                    source={require('../images/graphic_design.png')}
                  />
                </View>
              )}

              <View style={{padding: 7}}>
                <Image
                  style={styles.icon}
                  source={require('../images/sgLogo.png')}
                />
              </View>

              <View style={{padding: 7}}>
                <TouchableOpacity
                  disabled={!this.state.productShareLink}
                  onPress={() => this._toggleShow()}>
                  <Image
                    resizeMode="stretch"
                    style={styles.shareIcon}
                    source={require('../images/shareIcon.png')}
                  />
                </TouchableOpacity>
              </View>

              {/* {product_id && (
                <View
                  style={{
                    marginLeft: wp('10%'),
                    backgroundColor: 'blue',
                    marginTop: hp('-6%'),
                  }}>
                  <FavouriteButton {...this.props.productDetails} />
                </View>
              )} */}
            </View>

            <Text
              style={{
                fontSize: hp(2.4),
                fontWeight: 'bold',
                textTransform: 'capitalize',
                color: '#000',
              }}>
              {product_name}
            </Text>
            <View style={{alignItems: 'flex-start', paddingTop: hp(1)}}>
              {rounded_off_rating && (
                <AirbnbRating
                  count={5}
                  defaultRating={rounded_off_rating}
                  size={15}
                  isDisabled={true}
                  showRating={false}
                />
              )}
            </View>
          </View>
          <View
            style={{
              paddingHorizontal: wp(3),
              paddingVertical: hp(1),
              backgroundColor: '#fff',
              marginTop: 1,
            }}>
            <TouchableOpacity
              disabled={!this.state.shops_id}
              onPress={() => this.goToShop(store_id)}>
              <Text style={{fontSize: hp(2)}}>
                Dijual oleh <Text style={critiStyle}>{store_name}</Text>
                {store_name ? <Text> - {store_state_name}</Text> : null}
              </Text>
            </TouchableOpacity>
          </View>

          {product_type !== 'G' && product_type !== 'I' && (
            <ProductDetailInformation
              title={'Informasi Produk'}
              data={[
                {
                  name: 'Merek',
                  value: product_brand,
                  isAvailable: product_brand ? true : false,
                },
                {
                  name: 'Model',
                  value: product_model,
                  isAvailable: product_model ? true : false,
                },
                {
                  name: 'Kode Produk',
                  value: product_sku,
                  isAvailable: product_sku ? true : false,
                },
                {
                  name: 'Kondisi Barang',
                  value: product_condition,
                  isAvailable: product_condition ? true : false,
                },
                // {
                //   name: 'Tanggal Tersedia',
                //   value: product_available_date,
                //   isAvailable: product_available_date ? true : false,
                // },

                {
                  name: 'Waktu memproses',
                  value: duration_time,
                  isAvailable: duration_time ? true : false,
                },
                {
                  name: 'Dimensi (P x L x T)',
                  value:
                    product_length_and_height &&
                    product_length_and_height.length > 0
                      ? `${product_length_and_height[0].product_length} x ${product_length_and_height[0].prod_width} x ${product_length_and_height[0].prod_height} ${product_length_and_height[0].prod_length_class}`
                      : '',
                  isAvailable:
                    product_length_and_height &&
                    product_length_and_height.length > 0
                      ? true
                      : false,
                },
                {
                  name: 'Bobot',
                  value:
                    product_weight_details && product_weight_details.length > 0
                      ? `${product_weight_details[0].prod_weight} ${product_weight_details[0].prod_weight_class}`
                      : '',

                  isAvailable:
                    product_weight_details && product_weight_details.length > 0
                      ? true
                      : false,
                },
              ]}
            />
          )}

          {return_request_details && product_type !== 'G' ? (
            <ProductDetailInformation
              title={'KEBIJAKAN PENGEMBALIAN PRODUK'}
              data={[
                {
                  name: 'Terima Pengembalian',
                  value:
                    return_request_details[0].retrun_accepted == 'Y'
                      ? 'Ya'
                      : 'Tidak',
                  isAvailable: true,
                },
                {
                  name: 'Durasi Pengembalian',
                  value: `${return_request_details[0].return_time} Hari Kerja`,
                  isAvailable: return_request_details[0].return_time
                    ? true
                    : false,
                },
                {
                  name: 'Biaya Pengembalian Di tanggung Oleh?',
                  value:
                    return_request_details[0].return_paid_by == 'B'
                      ? 'Pembeli'
                      : 'Penjual',
                  isAvailable: return_request_details[0].return_paid_by
                    ? true
                    : false,
                },
                {
                  name: 'Biaya Restocking?',
                  value: 'Tidak',
                  isAvailable:
                    return_request_details[0].restocking_fee != null &&
                    return_request_details[0].restocking_fee == 'N'
                      ? true
                      : false,
                },
                {
                  name: 'Biaya Penyetokan Ulang',
                  value: `${return_request_details[0].restocking_fee} %`,
                  isAvailable:
                    return_request_details[0].restocking_fee != null &&
                    return_request_details[0].restocking_fee != 'N'
                      ? true
                      : false,
                },
              ]}
            />
          ) : null}

          {(product_brief !== '' || product_brief != null) && (
            <View style={styles.listContainer}>
              <ProductDescription title={product_brief} style={styles} />
            </View>
          )}

          {product_video_url !== '' && (
            <View style={{...styles.listContainer, marginTop: hp(2)}}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('VideoProduct', {
                    product_video_url: this.state.productDetails
                      .product_video_url,
                  });
                }}>
                <View style={styles.listItem}>
                  <Text style={descriptionTxt}>Lihat Video Produk</Text>
                  <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
                </View>
              </TouchableOpacity>
            </View>
          )}

          {product_type === 'G' && product_portfolio !== null && (
            <View style={styles.listContainer}>
              <ProductPortfolio title={product_portfolio} style={styles} />
            </View>
          )}

          {specification_details != null && (
            <View style={{...styles.listContainer, marginTop: 1}}>
              <TouchableOpacity
                onPress={() => this.GoToSpec(specification_details)}>
                <View style={styles.listItem}>
                  <Text style={descriptionTxt}>Spesifikasi Produk</Text>
                  <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
                </View>
              </TouchableOpacity>
            </View>
          )}
          {compatibility_form_display == 'Y' && (
            <View style={{...styles.listContainer, marginTop: 1}}>
              <TouchableOpacity
                onPress={() =>
                  this.GoToCompat(
                    comp_attribute_details,
                    final_compatibility_result,
                  )
                }>
                <View style={styles.listItem}>
                  <Text style={descriptionTxt}>Kompatibilitas Produk</Text>
                  <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
                </View>
              </TouchableOpacity>
            </View>
          )}
          {shipping_details != null && (
            <View style={{...styles.listContainer, marginTop: 1}}>
              <TouchableOpacity
                onPress={() => this.GoToPolicies(shipping_details)}>
                <View style={styles.listItem}>
                  <Text style={descriptionTxt}>Pengiriman & Kebijakan</Text>
                  <FontAwesome5Icon name={'chevron-right'} size={hp(2)} />
                </View>
              </TouchableOpacity>
            </View>
          )}
          <View
            style={{
              backgroundColor: '#fff',
              paddingTop: hp(1),
              paddingBottom: hp(3),
              paddingHorizontal: wp(3),
              marginTop: hp(2),
            }}>
            <ScrollView>
              {product_review_details !== null ? (
                <View>
                  <Text style={recomendedTxt}>
                    Ulasan ({product_review_count})
                  </Text>
                  <View style={styles.reviewWrapper}>
                    {product_review_details.map((review, index) => (
                      <View
                        key={review.review_id}
                        style={{
                          flexDirection: 'row',
                          alignItems: 'flex-start',
                          marginBottom: wp(2),
                        }}>
                        <View style={{textAlign: 'center'}}>
                          <View style={styles.reviewImg}>
                            <Image
                              style={styles.boxImg}
                              resize="contain"
                              source={{uri: review.user_image}}
                            />
                          </View>
                          <Text style={styles.reviewedBy}>
                            {substr(review.given_by || '', 20)}
                          </Text>
                        </View>
                        <View style={styles.rightBox}>
                          <View>
                            <AirbnbRating
                              count={5}
                              defaultRating={review.review_rating}
                              size={15}
                              isDisabled={true}
                              showRating={false}
                            />
                            {/* <Image style={styles.rightRating} source={require('../images/ratingImg.png')} /> */}
                            <Text style={styles.productComment}>
                              {review.review_text}
                            </Text>
                          </View>
                          <Text style={styles.ratingDate}>
                            {review.reviewed_on}
                          </Text>
                        </View>
                      </View>
                    ))}
                  </View>
                </View>
              ) : (
                <View style={{marginTop: -10}}>
                  <Text style={recomendedTxt}>Tidak Ada Ulasan</Text>
                </View>
              )}
            </ScrollView>
          </View>
          <View>
            {this.state.productDetails.related_products != null && (
              <View style={{backgroundColor: '#fff', marginTop: hp(2)}}>
                <Text style={[recomendedTxt, {paddingHorizontal: wp(3)}]}>
                  PRODUK-PRODUK TERKAIT
                </Text>
                <View
                  style={{
                    flex: 1,
                    marginBottom: hp(10),
                  }}>
                  <View style={{flex: 1}}>
                    <ScrollView
                      showsHorizontalScrollIndicator={false}
                      horizontal={true}>
                      {this.renderRelatedProducts()}
                    </ScrollView>
                  </View>
                </View>
              </View>
            )}
            {this.state.productDetails.recommended_more_products != null && (
              <View
                style={{
                  backgroundColor: '#fff',
                  marginHorizontal: wp(3),
                  marginTop: hp(2),
                  flex: 1,
                  height: hp(45),
                }}>
                <Text style={recomendedTxt}>PRODUK YANG DIREKOMENDASIKAN</Text>
                <ProductCardSection>
                  <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}>
                    {this.renderRecommendedProducts()}
                  </ScrollView>
                </ProductCardSection>
              </View>
            )}
          </View>
        </ScrollView>

        {!this.state.productDetails.user_own_product && (
          <View style={{marginTop: hp(5)}}>
            <FooterActionButton
              {...this.props}
              {...this.state}
              onPressAddCart={(ref) =>
                product_type === 'P'
                  ? this.requestAddToCart(ref)
                  : this.buyNow(ref)
              }
              onPressBuyNow={this.buyNow}
              onPressIncrement={this.decrement}
              onPressButtonVariant={(
                {
                  value,
                  index,
                  childIndex = null,
                  isChecked = false,
                  optionValue = null,
                },
                type,
              ) => {
                switch (type) {
                  case 'select':
                    this.selectBox(value, index);
                    break;
                  case 'checkbox':
                    this.checkBox(isChecked, value, index, childIndex);
                    break;
                  case 'radio':
                    this.selectRadio(value, optionValue, index);
                    break;
                  case 'text':
                    this.changeText(value, childIndex);
                    break;
                  case 'textarea':
                    this.changeText(value, childIndex);
                    break;
                  case 'file':
                    this.uploadFile(index);
                    break;
                  case 'datetime':
                    this.openDatePickerDateTime(value, optionValue, index);
                    break;
                  case 'time-datetime':
                    this.openTimePickerDateTime(value, optionValue, index);
                    break;
                  case 'date':
                    this.setTimeAndDate(value, index);
                    break;
                  case 'time':
                    this.setTimeAndDate(value, index);
                    break;

                  default:
                    null;
                }
              }}
              onPressDecrement={this.increment}
            />
          </View>
        )}
      </View>
    );
  }
}

const styles = {
  listContainer: {
    backgroundColor: '#fff',
    paddingVertical: hp(1),
    paddingHorizontal: wp(3),
    marginTop: hp(2),
  },
  listItem: {
    paddingVertical: hp(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  optionMainView: {
    borderColor: '#e7e7e7',
    borderWidth: 2,
    borderRadius: 5,
    padding: 10,
    margin: 10,
  },
  boxImg1: {
    position: 'absolute',
    right: 5,
    top: 13,
    width: 16,
    height: 16,
  },
  checkBoxStyle: {
    // width:wp('10%'),
    // flexBasis:wp('10%'),
  },
  datepicherStyle: {
    borderColor: 1,
    color: '#000',
    paddingLeft: 0,
    fontSize: hp('8%'),
    marginTop: 0,
    backgroundColor: '#e7e7e7',
    height: 40,
    fontFamily: Font.RobotoRegular,
    width: '100%',
    borderRadius: 5,
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  datepicherStyle2: {
    borderColor: 1,
    color: '#000',
    paddingLeft: 0,
    fontSize: hp('8%'),
    marginTop: 0,
    backgroundColor: '#e7e7e7',
    height: 40,
    fontFamily: Font.RobotoRegular,
    width: '48.6%',
    borderRadius: 5,
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  radioWrappper: {
    flexDirection: 'row-reverse',
    paddingTop: 3,
    paddingBottom: 3,
    // justifyContent:'flex-start',
    alignItems: 'center',
    // borderBottomWidth:1,
    // borderColor:'#e7e7e7'
  },
  labelStyle: {
    // flex: 1,
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 10,
    marginBottom: 10,
  },
  labelStyle2: {
    fontSize: hp('2%'),
    fontFamily: Font.RobotoBold,
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 10,
    marginBottom: 10,
  },
  uploadLabelStyle: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  abc: {
    // flexDirection:'row',
    flexDirection: 'row-reverse',
    // justifyContent:'flex-start',
    paddingLeft: 5,
    textAlign: 'left',
    flexWrap: 'wrap',
    width: '100%',
    flexBasis: '100%',
  },
  dropdownClass: {
    elevation: 1,
    borderRadius: 50,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  pickerClass: {
    height: hp('6%'),
  },
  dropheadingClass: {
    fontSize: wp('4%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  titleStyle2: {
    color: '#545454',
    fontSize: wp('4.8%'),
    paddingBottom: 0,
    marginBottom: 15,
  },
  viewStyle: {
    marginleft: 5,
    marginRight: 5,
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 90,
  },
  critiStyle: {
    color: '#00b3ff',
  },
  freeShipImg: {
    width: 40,
    height: 25,
    marginRight: 10,
  },
  insdown: {
    width: hp('4%'),
    height: hp('4%'),
  },
  reviewWrapper: {
    padding: 16,
  },
  heading: {
    color: '#000',
    fontSize: hp('2%'),
    marginBottom: 15,
    fontFamily: Font.RobotoRegular,
  },
  reviewImg: {
    width: wp('15%'),
    height: wp('15%'),
    borderWidth: 1,
    borderColor: '#d2d8da',
    borderRadius: wp('15%'),
    marginRight: 10,
    padding: wp('1%'),
  },
  boxImg: {
    width: '100%',
    height: '100%',
    // borderWidth: 1,
    // borderColor: '#000',
    // borderRadius: '100%',
    borderRadius: wp('15%'),
  },
  uploadBoxImg: {
    height: hp('5.3%'),
    width: wp('10%'),
  },
  rightBox: {
    borderWidth: 1,
    borderColor: '#d2d8da',
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rightRating: {
    width: 85,
    height: 12,
  },
  productComment: {
    marginTop: 10,
    fontFamily: Font.RobotoItalc,
    fontSize: hp('2%'),
    color: '#000',
  },
  ratingDate: {
    fontSize: hp('1.5%'),
    color: '#000',
    fontFamily: Font.RobotoRegular,
  },
  reviewedBy: {
    color: '#000',
    fontSize: hp('1.5%'),
    marginTop: 10,
    lineHeight: hp('2%'),
    width: wp('15%'),
    marginRight: 10,
  },
  crossColor: {
    textAlign: 'center',
    color: '#000',
  },
  closeBtn2: {
    position: 'absolute',
    right: 0,
    width: 30,
    height: 30,
    top: 0,
    zIndex: 1002,
    color: '#fff',
    borderRadius: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },

  bgColor: {
    backgroundColor: '#fff',
    position: 'relative',
    height: '100%',
    // height:hp('30%')
  },
  ratingWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: hp('2%'),
    paddingBottom: hp('2%'),
    width: '100%',
    borderColor: '#e7e7e7',
    borderTopWidth: 1,
  },
  paddingLeftRight: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  priceTxt: {
    color: '#00b3ff',
    // fontSize:22,
    fontSize: wp('4.8%'),
    // fontWeight:'bold'
    fontFamily: Font.RobotoBold,
  },
  sfLogo: {
    // width:25,
    // height:27,
    marginLeft: 15,
    width: wp('6%'),
    height: hp('3%'),
  },
  lftPrice: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  descriptionTxt: {
    color: '#000',
    opacity: 0.7,
    // fontSize:20,
    fontSize: wp('4%'),
    fontFamily: Font.RobotoBold,
    // fontWeigth: 'bold',
  },
  rightArrow: {
    width: 5,
    height: 10,
  },
  warrentyWrapper: {
    // flexDirection:'row',
    // alignItems: 'center',
    width: '100%',
  },
  warrentyTxt: {
    // fontSize:18,
    color: '#858585',
    fontSize: hp('1.8%'),
  },
  recomendedTxt: {
    color: '#696969',
    // fontSize:22,
    fontSize: wp('4%'),
    paddingTop: hp('2%'),
    paddingBottom: hp('1.8%'),
    textTransform: 'uppercase',
    fontFamily: Font.RobotoMedium,
  },
  productSection: {
    paddingLeft: 15,
    paddingRight: 15,
    borderColor: '#ececec',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    marginBottom: 90,
  },
  footer: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    position: 'absolute',
    width: '100%',
    paddingTop: 10,
    paddingBottom: 10,
    bottom: 0,
    zIndex: 99,
    elevation: 10,
    borderTopWidth: 1,
    borderColor: '#dcdcdc',
  },
  inputStyle: {
    paddingTop: 10,
    color: '#000',
    // width:52,
    // height:52,
    width: wp('10%'),
    height: wp('10%'),
    // fontSize: 18,
    fontSize: hp('2%'),
    marginTop: 0,
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: wp('10%'),
    backgroundColor: '#fff',
    textAlign: 'center',
    elevation: 10,
  },
  footerInner: {
    width: '50%',
    textAlign: 'left',
    borderWidth: 1,
    borderColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  lftBtn: {
    flexDirection: 'row',
    backgroundColor: '#00b3ff',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  decressBtnStyle: {
    //  width:30,
    width: wp('9%'),
    backgroundColor: '#00b3ff',
    textAlign: 'center',
    alignItems: 'center',
    borderTopRightRadius: 0,
    borderTopLeftRadius: wp('9%'),
    borderBottomLeftRadius: wp('9%'),
    borderBottomRightRadius: 0,
    color: '#fff',
    // fontSize:20,
    fontSize: wp('6%'),
    fontFamily: Font.RobotoMedium,
  },
  incressBtnStyle: {
    // width:30,
    width: wp('9%'),
    backgroundColor: '#00b3ff',
    textAlign: 'center',
    alignItems: 'center',
    borderTopRightRadius: wp('9%'),
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: wp('9%'),
    color: '#fff',
    // fontSize:20,
    fontSize: wp('6%'),
    fontFamily: Font.RobotoMedium,
  },
  buyNowBtn: {
    // width:125,
    width: wp('27%'),
    // height:wp('100%'),
    borderColor: '#c90305',
    borderWidth: 1,
    backgroundColor: '#c90305',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    color: '#fff',
    // fontSize:18,
    textAlign: 'center',
    lineHeight: wp('8.5%'),
    fontSize: wp('3.5%'),
    fontFamily: Font.RobotoRegular,
  },
  sliderBottom: {
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  titleStyle: {
    color: '#545454',
    fontSize: hp('2.5%'),
    paddingBottom: 0,
    fontFamily: Font.RobotoRegular,
    // fontSize:22,
  },
  carousalStyle: {
    width: '100%',
    height: hp('45%'),
    backgroundColor: '#f6f6f6',
    // paddingBottom:35,
    // paddingTop:20,
  },
  soldStyle: {
    color: '#858585',
    fontSize: hp('1.7%'),
    marginBottom: 5,
    // marginLeft:8,
    fontFamily: Font.RobotoRegular,
  },
  critiStyle: {
    color: '#00b3ff',
    fontFamily: Font.RobotoRegular,
  },
  colorBarStyle: {
    width: hp('2.8%'),
    height: hp('2.8%'),
    borderRadius: 2,
    backgroundColor: '#e67904',
    marginLeft: 8,
  },
  colorBarWrapperStyle: {
    flexDirection: 'row',
    width: '100%',
    // fontSize:18,
    fontSize: hp('2.5%'),
    alignItems: 'center',
  },
  colorBrandWrapper: {
    flexDirection: 'row',
  },
  barTxtStyle: {
    color: '#858585',
    fontSize: hp('2.2%'),
    fontFamily: Font.RobotoRegular,
  },
  casinoTxtStyle: {
    color: '#c90305',
    fontFamily: Font.RobotoRegular,
  },
  downTxt: {
    color: '#4abc96',
    fontFamily: Font.RobotoRegular,
  },
  shareIcon: {
    height: 25,
    marginRight: 10,
  },
  icon: {
    width: 30,
    height: 25,
    marginRight: 10,
  },
  donwloadIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  graphicdesignIcon: {
    width: 45,
    height: 30,
    marginRight: 10,
  },
};

const mapStateToProps = (state) => {
  return {
    message: state.cart.message,
    // loading: state.cart.loading,
    cart_count: state.cart.cart_count,
    cart_error: state.cart.cart_error,
  };
};

export default connect(mapStateToProps, {
  fetchRecentlyViewed,
  addToCart,
  viewCart,
})(ProductDetail);
