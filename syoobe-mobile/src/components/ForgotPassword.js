import React, {Component} from 'react';
import {StatusBar, AsyncStorage, Text, ScrollView, Alert} from 'react-native';
import {Button, Card, CardSection, Spinner} from './common';
import {connect} from 'react-redux';
import {sendResetPassword, forgotPasswordChange} from '../actions';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import InputText from './common/InputText';

class ForgotPassword extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
    headerTitle: (
      <Text style={{fontSize: hp('2.5%'), color: '#fff'}}>
        Tidak ingat kata sandi
      </Text>
    ),
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  onForgotEmailChange = (text) => {
    var emailTrim = text.trim();
    this.props.forgotPasswordChange(emailTrim);
  };

  handleOnSendResetPassword = () => {
    if (this.props.username == '') {
      Alert.alert('Email tidak boleh kosong');
    } else {
      this.props.sendResetPassword(
        this.props.navigation,
        this.props.username.toLowerCase(),
      );
    }
  };

  render() {
    // console.log('ini state', JSON.stringify(this.state));
    console.log('ini props', JSON.stringify(this.props.username));

    if (this.props.loading) {
      return <Spinner color="red" size="large" />;
    }
    return (
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        style={styles.scrollClass}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <Card>
          <CardSection>
            <InputText
              placeholder="Username atau email"
              label="Email"
              onChangeText={this.onForgotEmailChange.bind(this)}
            />
          </CardSection>
          <Button onPress={() => this.handleOnSendResetPassword()}>
            <Text style={styles.errorTextStyle}>Kirim</Text>
          </Button>
        </Card>
      </ScrollView>
    );
  }
}
const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: '#FFFX',
  },
  bgImg: {
    height: '100%',
  },
  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
  },
};

const mapStateToProps = (state) => {
  return {
    username: state.forgotPassword.username,
    loading: state.forgotPassword.loading,
  };
};

export default connect(mapStateToProps, {
  sendResetPassword,
  forgotPasswordChange,
})(ForgotPassword);
