import React, { Component } from 'react'
import { Animated, View, StyleSheet, Image, Dimensions, ScrollView } from 'react-native'

const deviceWidth = Dimensions.get('window').width
const FIXED_BAR_WIDTH = 280
const BAR_SPACE = 10

const images = [
  'http://mydevfactory.com/~goutam/tarasis/syoobe/sliderImg1.png',
  'https://rukminim1.flixcart.com/image/300/300/jbzedu80/carpet-rug/p/q/9/marooncarpet03heavy-856412210-kihome-original-imaff5fjhrhbpjgt.jpeg?q=70',
  'https://rukminim1.flixcart.com/image/300/300/jcktzm80/glue-gun/k/z/h/hot-melt-glue-gun-kit-100-watt-on-off-with-40-glue-sticks-for-original-imaffnsnmh2xvkuy.jpeg?q=70',
  'https://s-media-cache-ak0.pinimg.com/originals/40/4f/83/404f83e93175630e77bc29b3fe727cbe.jpg',
  'https://s-media-cache-ak0.pinimg.com/originals/8d/1a/da/8d1adab145a2d606c85e339873b9bb0e.jpg',
  'https://s-media-cache-ak0.pinimg.com/originals/8d/1a/da/8d1adab145a2d606c85e339873b9bb0e.jpg',

]

export default class Carousal extends Component {

  numItems = images.length
  // itemWidth = (FIXED_BAR_WIDTH / this.numItems) - ((this.numItems - 1) * BAR_SPACE)
  animVal = new Animated.Value(0)
  itemWidth = 15
  itemHeight=15
  itemRadius=15

  render() {
    let imageArray = []
    let barArray = []
    images.forEach((image, i) => {
      // console.log(image, i)
      const thisImage = (
        // <Image
        //   key={`image${i}`}
        //   source={{uri: image}}
        //   style={{ width: deviceWidth, }}
        // />
        <View key={`image${i}`} style={{ width: deviceWidth,height:'100%',justifyContent:'center',alignItems:'center',paddingBottom:10  }}>
          <Image source={{uri: image}} style={{ width:150,height:'100%' }}/>
        </View>
      )
      imageArray.push(thisImage)

      const scrollBarVal = this.animVal.interpolate({
        inputRange: [deviceWidth * (i - 1), deviceWidth * (i + 1)],
        outputRange: [-this.itemWidth, this.itemWidth],
        extrapolate: 'clamp',
      })

      const thisBar = (
        <View
          key={`bar${i}`}
          style={[
            styles.track,
            {
              width: this.itemWidth,height:this.itemHeight,borderRadius:this.itemRadius,
              marginLeft: i === 0 ? 0 : BAR_SPACE,
            },
          ]}
        >
          <Animated.View

            style={[
              styles.bar,
              {
                width: this.itemWidth,
                height:this.itemHeight,
                borderRadius:this.itemRadius,
                transform: [
                  { translateX: scrollBarVal },
                ],
              },
            ]}
          />
        </View>
      )
      barArray.push(thisBar)
    })

    return (
      <View style={styles.container} flex={1}>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={10}
          pagingEnabled
          onScroll={
            Animated.event([{ nativeEvent: { contentOffset: { x: this.animVal } } }])}>
              {imageArray}
        </ScrollView>
        <View style={styles.barContainer}>
          {barArray}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  barContainer: {
    position: 'absolute',
    zIndex: 2,
    bottom:-20,
    flexDirection: 'row',
  },
  track: {
    backgroundColor: '#ccc',
    overflow: 'hidden',
    height: 2,
  },
  bar: {
    backgroundColor: '#5294d6',
    height: 2,
    position: 'absolute',
    left: 0,
    top: 0,
  },
})