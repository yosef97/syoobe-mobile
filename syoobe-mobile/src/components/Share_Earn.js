import React, {Component} from 'react';
import {
  AsyncStorage,
  StatusBar,
  Text,
  Image,
  TouchableOpacity,
  View,
  ScrollView,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {Spinner} from './common';
import axios from 'axios';
import {BASE_URL} from '../services/baseUrl';
import Clipboard from '@react-native-community/clipboard';
import Share from 'react-native-share';

export default class ShareEarn extends Component {
  static navigationOptions = {
    drawerLabel: 'Bagikan & Dapatkan Poinnya',
    drawerIcon: () => (
      <Image
        source={require('../images/share-icon.png')}
        style={{width: wp('4.2%'), height: hp('2.4%')}}
      />
    ),
  };

  constructor(props) {
    super(props);
    this.state = {
      _token: null,
      email: '',
      message: '',
      link: null,
      loading: true,
      share_earn_data: null,
      showMailContent: false,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  fetchData = () => {
    this.retrieveToken()
      .then((_token) => {
        var details = {
          _token: _token,
        };
        var formBody = [];
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        axios
          .post(BASE_URL + '/generateInvitationLink', formBody)
          .then((response) => {
            console.log('response:', response);
            if (response.data.status == 1) {
              this.setState({
                link: response.data.link,
                share_earn_data: response.data.share_earn_data,
                loading: false,
              });
            } else {
              Alert.alert('Kesalahan', 'Server tidak merespon', [{text: 'OK'}]);
            }
          });
      })
      .catch((err) => {
        Alert.alert('Kesalahan', 'Ada yang error', [{text: 'OK'}]);

        console.log(err);
      });
  };

  componentDidMount = () => {
    this.fetchData();
  };

  shareToTwitter = () => {
    Clipboard.setString(this.state.link);
    Alert.alert('Berhasil disalin ke clipboard');
  };

  handleCopy = (text) => {
    // alert(text);
    Clipboard.setString(text);
    Alert.alert('Berhasil disalin ke clipboard');
  };

  _toggleShow = () => {
    let shareOptions = {
      title: 'Bagikan',
      url: this.state.link,
      subject: 'Bagikan ke sahabatmu',
      message: this.state.share_earn_data,
    };
    Share.open(shareOptions);
  };

  render() {
    console.log('ini state', this.state);
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color="red" size="large" />
        </View>
      );
    }
    return (
      <ScrollView
        keyboardShouldPersistTaps="handled"
        style={styles.mainWrapper}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('TermsCondition')}>
            <Text style={styles.topTxt}>{this.state.share_earn_data}</Text>
            <Text style={styles.topTxt}>
              Pastikan untuk membaca semua{' '}
              <Text style={styles.termCondition}>Syarat dan ketentuan</Text>
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.bottomSection}>
          <View style={styles.linkBox}>
            <Text style={styles.linkBoxTxt}>
              Anda dapat menyalin tautan undangan di bawah ini & membagikannya
              kepada teman-teman Anda di saluran sosial Facebook, Twitter,
              LinkedIn, Blog, Email dll.
            </Text>
            <Text
              style={styles.linkTxt}
              onPress={() => this.handleCopy(this.state.link)}>
              {this.state.link}
            </Text>
          </View>

          <TouchableOpacity
            style={[styles.btnStyle, styles.emailColor]}
            onPress={() => this._toggleShow()}>
            <Text style={styles.btntxt}>Bagikan</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  mainWrapper: {
    backgroundColor: '#fff',
  },
  container: {
    padding: 10,
    flex: 1,
  },
  topTxt: {
    color: '#7d7d7d',
    fontSize: hp('1.9%'),
    fontFamily: Font.RobotoRegular,
    marginBottom: hp('1.4%'),
  },
  termCondition: {
    color: '#c90305',
  },
  bottomSection: {
    padding: 15,
    paddingTop: 0,
  },
  linkBox: {
    elevation: 1,
    borderRadius: 5,
    padding: 10,
    marginBottom: hp('1.5%'),
  },
  linkBoxTxt: {
    textAlign: 'center',
    color: '#7d7d7d',
    fontSize: hp('2.1%'),
    fontFamily: Font.RobotoLight,
    marginBottom: hp('1.4%'),
  },
  linkTxt: {
    textAlign: 'center',
    color: '#00b3ff',
    fontSize: hp('2.2%'),
    fontFamily: Font.RobotoMedium,
  },
  btnStyle: {
    paddingHorizontal: hp('2%'),
    paddingVertical: hp('2%'),
    borderRadius: 50,
    marginTop: hp('2.5%'),
  },
  btntxt: {
    color: '#ffffff',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.2%'),
    textAlign: 'center',
  },
  btnIcon: {
    width: hp('5.5%'),
    height: hp('5.5%'),
    position: 'absolute',
    left: hp('1%'),
    top: hp('1%'),
  },
  fbColor: {
    backgroundColor: '#3b5998',
  },
  twitColor: {
    backgroundColor: '#1da1f2',
  },
  emailColor: {
    backgroundColor: '#f86868',
  },
  inviteFrdWrapper: {
    paddingHorizontal: 20,
  },
  inviteFrd: {
    borderColor: '#f86868',
    borderWidth: 1,
    padding: 10,
  },
  inviteHeading: {
    fontSize: hp('2%'),
    textAlign: 'center',
    color: '#696969',
    marginBottom: hp('2%'),
  },
};
