import React from 'react';
import {Component} from 'react';
import {
  StatusBar,
  View,
  Image,
  AsyncStorage,
  Text,
  ScrollView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {
  getDashboardDetails,
  homeProducts,
  homeProductsWithToken,
} from '../actions';
import {connect} from 'react-redux';
import {Spinner} from './common';
import {DashboardTabNavigator} from './routes/DashboardTab';
import Carousel from 'react-native-snap-carousel';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orientation: 'potrait',
    };
  }

  static navigationOptions = {
    drawerLabel: 'Dasbor',
    drawerIcon: () => (
      <Image
        source={require('../images/dashboard.png')}
        style={{width: wp('4.5%'), height: hp('2%')}}
      />
    ),
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = () => {
    this.props.homeProducts();
    this.retrieveToken()
      .then((_token) => {
        this.props.getDashboardDetails({_token});
        this.props.homeProductsWithToken({_token});
      })
      .catch((error) => {
        console.log('Promise is rejected with error: ' + error);
      });
  };

  bindScreenDimensionsUpdate = (event) => {
    const {width, height} = event.nativeEvent.layout;

    this.setState({
      orientation: width < height ? 'potrait' : 'landscape',
    });
  };

  _renderItem = ({item, index}) => {
    console.log('ini item', item);
    return (
      <View
        key={index}
        style={{
          paddingHorizontal: wp(3),
          paddingBottom: hp(2),
          backgroundColor: '#fff',
          width: wp(105),
        }}>
        <View>
          {this.state.orientation === 'potrait' ? (
            <Image style={styles.imagePotrait} source={{uri: item.path}} />
          ) : (
            <Image style={styles.imageLandscape} source={{uri: item.path}} />
          )}
        </View>
      </View>
    );
  };

  render() {
    // console.log('ini state', JSON.stringify(this.state));
    console.log('ini props bananer', JSON.stringify(this.props));

    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color={'red'} />
        </View>
      );
    }
    return (
      <View
        style={{flex: 1, backgroundColor: '#fff'}}
        onLayout={this.bindScreenDimensionsUpdate}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <ScrollView>
          <View style={{alignSelf: 'center', flex: 1}}>
            <Carousel
              data={this.props.bannerImages}
              renderItem={this._renderItem}
              sliderWidth={
                this.state.orientation === 'potrait' ? wp('105%') : wp('250%')
              }
              itemWidth={
                this.state.orientation === 'potrait' ? wp('105%') : wp('250%')
              }
              initialScrollIndex={1}
              layout={'default'}
              autoplay={true}
              autoplayInterval={4000}
              autoplayDelay={500}
              loopClonesPerSide={2}
              loop={true}
              inactiveSlideOpacity={0.4}
            />
          </View>
          <Text style={styles.accHeading}>Jenis Akun</Text>
          <DashboardTabNavigator />
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  accHeading: {
    color: '#696969',
    fontFamily: Font.RobotoRegular,
    fontSize: wp(5),
    textAlign: 'center',
    paddingTop: 10,
    fontWeight: 'bold',
  },
  imageLandscape: {
    resizeMode: 'stretch',
    height: hp('30%'),
    width: wp('250%'),
  },
  imagePotrait: {
    resizeMode: 'stretch',
    height: hp('21%'),
    width: wp('100%'),
  },
};

const mapStateToProps = (state) => {
  return {
    buyerData: state.dashboard.buyerData,
    sellerData: state.dashboard.sellerData,
    loading: state.dashboard.loading,
    bannerImages: state.home.bannerImages,
  };
};

export default connect(mapStateToProps, {
  getDashboardDetails,
  homeProducts,
  homeProductsWithToken,
})(Dashboard);
