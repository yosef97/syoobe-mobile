import React, {Component} from 'react';
import {View, StyleSheet, Alert} from 'react-native';
import {TextInputCostum} from './common/TextInput';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ProductFileUpload} from './common/product/ProductFileUpload';
import Bugsnag from '@bugsnag/react-native';
import DocumentPicker from 'react-native-document-picker';
import {Content, Button, Text} from 'native-base';
import axios from 'axios';

class ReplyMessage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      _token: props.route.params?._token,
      thread_id: props.route.params?.thread_id,
      fileName: '',
      fileDownload: [],
      loadingUpload: false,
      textMessage: '',
      loading: false,
    };
  }

  selectAttachment = async (fileType, item) => {
    await this.setState({
      loadingUpload: true,
    });

    try {
      const file = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });

      this.setState({
        fileDownload: file,
        fileName: file.name,
      });

      this.setState({
        loadingUpload: false,
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        Bugsnag.notify(err);
      } else {
        throw Bugsnag.notify(err);
      }
    }
  };

  onSubmit = () => {
    if (this.state.textMessage !== '') {
      this.setState({
        loading: true,
      });

      const AttachmentFile = {
        name: this.state.fileDownload.name,
        uri: this.state.fileDownload.uri,
        type: this.state.fileDownload.type,
      };

      var formData = new FormData();

      formData.append('_token', this.state._token);

      formData.append('thread_id', this.state.thread_id);

      formData.append('message_text', this.state.textMessage);

      this.state.fileName && formData.append('msg_attachment', AttachmentFile);

      axios
        .post('https://syoobe.co.id/api/sentThreadMessage', formData)
        .then((response) => {
          console.log('update1', JSON.stringify(response));
          if (response.data.status === 1) {
            this.setState({
              loading: false,
            });
            Alert.alert('Informasi', response.data.msg, [
              {
                text: 'OK',
                onPress: () => this.props.navigation.replace('Messages'),
              },
            ]);
          } else {
            Alert.alert('Informasi', response.data.msg, [
              {
                text: 'OK',
                onPress: () => this.props.navigation.replace('Messages'),
              },
            ]);
            Alert.alert('Tindakan gagal');
          }
        })
        .catch((err) => {
          Bugsnag.notify(err);
          console.log('e', err);
          this.setState({
            loading: false,
          });
        });
    } else {
      Alert.alert('Mohon cek kembali pengajuan data anda.');
    }
  };

  render() {
    // console.log('ini props reply', JSON.stringify(this.props));
    // console.log('ini props reply', JSON.stringify(this.state._token));

    return (
      <View style={styles.container}>
        <Content>
          <TextInputCostum
            value={this.state.textMessage}
            onChangeText={(text) => this.setState({textMessage: text})}
            label="Masukan Pesan"
            multiline
            numberOfLines={5}
          />

          <ProductFileUpload
            value={this.state.fileDownload.filename}
            // index={i}
            loading={this.state.loadingOnUpload}
            onPress={({type, index, ref, item}) => {
              ref.close();
              this.selectAttachment(type, item);
            }}
            fileName={this.state.fileName}
          />

          <View style={{marginVertical: hp(2)}}>
            <Button
              block
              danger
              style={{backgroundColor: '#C90205'}}
              //   disabled={!this.state.fileNameSample}
              onPress={() => this.onSubmit()}>
              <Text>Kirim</Text>
            </Button>
          </View>
        </Content>
      </View>
    );
  }
}

export default ReplyMessage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 2,
    paddingVertical: hp(2),
    paddingHorizontal: wp(2.5),
    backgroundColor: '#fff',
  },
  content: {
    padding: 10,
  },
});
