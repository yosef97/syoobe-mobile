import React, {Component} from 'react';
import {AsyncStorage, ScrollView, Text} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {
  confirmNewEmail,
  currentPasswordChangeEmail,
  newEmail,
  saveChangeEmail,
} from '../actions';
import {Button, Card, CardSection, Spinner} from './common';
import InputText from './common/InputText';
import {Font} from './Font';

class ChangeEmail extends Component {
  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  changeEmail = async () => {
    await this.retrieveToken().then((_token) => {
      this.props.saveChangeEmail(
        this.props.navigation,
        _token,
        this.props.new_email,
        this.props.confirm_new_email,
        this.props.current_password_change_email,
      );
    });
  };

  changeNewEmail = (text) => {
    this.props.newEmail(text);
  };

  changeConfirmNewEmail = (text) => {
    this.props.confirmNewEmail(text);
  };

  changeCurrentPassword = (text) => {
    this.props.currentPasswordChangeEmail(text);
  };

  render() {
    if (this.props.loading) {
      return <Spinner color="red" size="large" />;
    }

    return (
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        style={styles.scrollClass}>
        <Card>
          <CardSection>
            <InputText
              placeholder=""
              label="Email baru"
              onChangeText={this.changeNewEmail.bind(this)}
            />
          </CardSection>
          <CardSection>
            <InputText
              placeholder=""
              label="Konfirmasi email baru"
              onChangeText={this.changeConfirmNewEmail.bind(this)}
            />
          </CardSection>
          <CardSection>
            <InputText
              placeholder=""
              label="Kata sandi saat ini"
              onChangeText={this.changeCurrentPassword.bind(this)}
            />
          </CardSection>
          <Text style={styles.errorTextStyle}></Text>
          <Button
            onPress={() => {
              this.changeEmail();
            }}>
            Ganti e-mail
          </Button>
          <Text style={styles.confirmMsg}>
            Alamat email Anda tidak akan berubah sampai Anda mengonfirmasi
            melalui email.
          </Text>
        </Card>
      </ScrollView>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: wp('4%'),
    alignSelf: 'center',
    // color: 'red'
  },

  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
  },
  confirmMsg: {
    color: '#8f8f8f',
    fontSize: wp('4%'),
    fontFamily: Font.RobotoLight,
    textAlign: 'center',
    paddingTop: 10,
  },
};

const mapStateToProps = (state) => {
  return {
    new_email: state.profile.new_email,
    confirm_new_email: state.profile.confirm_new_email,
    current_password_change_email: state.profile.current_password_change_email,
    loading: state.profile.loading,
  };
};

export default connect(mapStateToProps, {
  newEmail,
  confirmNewEmail,
  currentPasswordChangeEmail,
  saveChangeEmail,
})(ChangeEmail);
