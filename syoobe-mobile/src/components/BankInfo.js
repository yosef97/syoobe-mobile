import React, {Component} from 'react';
import {
  AsyncStorage,
  View,
  ScrollView,
  StatusBar,
  Text,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {CardSection, Spinner, FooterButton} from './common';
import {Font} from './Font';
import {
  saveBankDetails,
  changeBankAddress,
  changePaypalId,
  changeIfsc_code,
  changeAccountNumber,
  changeAccountHolderName,
  changeBankName,
  getBankDetails,
} from '../actions';
import {connect} from 'react-redux';
import InputText from './common/InputText';
import PTRView from 'react-native-pull-to-refresh';

class BankInfo extends Component {
  constructor(props) {
    super();
    this.state = {
      checked: false,
      _token: null,
      preApproveKey: null,
      loading: true,
      fromWallet: props.route.params && props.route.params.fromWallet,
      ub_bank_name: props.bank_info.ub_bank_name,
      ub_account_holder_name: props.bank_info.ub_account_holder_name,
      ub_account_number: props.bank_info.ub_account_number,
      ub_ifsc_swift_code: props.bank_info.ub_ifsc_swift_code,
      ub_bank_address: props.bank_info.ub_bank_address,
      loading2: props.loading,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount() {
    this.retrieveToken().then((_token) => {
      this.props.getBankDetails(_token);
      this.setState({
        _token: _token,
      });
      this.setState({
        loading: false,
      });
    });
  }

  handleChangeText = (text, type) => {
    console.log(text);
    this.setState({
      ...this.state,
      [type]: text,
    });
  };

  saveAll = async () => {
    const {_token, fromWallet} = this.state;
    this.props.saveBankDetails(fromWallet, this.props.navigation, {
      _token: _token,
      ub_bank_name: this.state.ub_bank_name,
      ub_account_holder_name: this.state.ub_account_holder_name,
      ub_account_number: this.state.ub_account_number,
      ub_ifsc_swift_code: this.state.ub_ifsc_swift_code,
      ub_bank_address: this.state.ub_bank_address,
    });
  };

  bankDetails = async () => {
    this.saveAll();
  };

  render() {
    // console.log('ini props info bank', JSON.stringify(this.props.loading));
    console.log('ini state info bank', JSON.stringify(this.state.loading2));
    console.log(
      'ini state info bank',
      JSON.stringify(this.state.ub_bank_address),
    );

    if (this.props.loading || this.state.loading || this.state.loading2) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color="red" size="large" />
        </View>
      );
    }

    return (
      <View style={{flex: 1}}>
        <PTRView
          onRefresh={() => {
            this.state._token !== null &&
              this.props.getBankDetails(this.state._token);
          }}>
          <ScrollView style={styles.scrollClass}>
            <View style={styles.viewStyle}>
              <CardSection>
                <InputText
                  onChangeText={(e) => this.handleChangeText(e, 'ub_bank_name')}
                  value={this.state.ub_bank_name}
                  placeholder="Masukkan Nama Bank"
                  label="Nama Bank *"
                />
              </CardSection>
              <CardSection>
                <InputText
                  onChangeText={(e) =>
                    this.handleChangeText(e, 'ub_account_holder_name')
                  }
                  value={this.state.ub_account_holder_name}
                  placeholder="Masukkan Nama Penerima / Pemegang Akun"
                  label="Nama Penerima / Pemegang Akun *"
                />
              </CardSection>
              <CardSection>
                <InputText
                  onChangeText={(e) =>
                    this.handleChangeText(e, 'ub_account_number')
                  }
                  value={this.state.ub_account_number}
                  keyboardType={'numeric'}
                  label="Nomor rekening bank *"
                  placeholder=" Masukkan Nomor rekening bank"
                />
              </CardSection>
              <CardSection>
                <InputText
                  onChangeText={(e) =>
                    this.handleChangeText(e, 'ub_ifsc_swift_code')
                  }
                  value={this.state.ub_ifsc_swift_code}
                  placeholder="Masukkan Kode BANK / Kode Swift"
                  label="Kode BANK / Kode Swift *"
                />
              </CardSection>
              <View style={{paddingTop: 0, paddingBottom: 20, flex: 1}}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('SwiftCode')}>
                  <Text style={{textAlign: 'justify'}}>
                    Untuk menerima atau mengirimkan uang dengan metoda Wire
                    Transfer (Telegraphic Transfer), Anda memerlukan kode SWIFT
                    (Society for World-wide Interbank Financial
                    Telecommunications) yaitu kode unik yang dimiliki setiap
                    bank untuk keperluan transfer uang dari dan ke luar negeri
                    Pelajari lebih lanjut tentang
                    <Text style={{color: 'red', textAlign: 'justify'}}>
                      {' '}
                      KODE SWIFT
                    </Text>
                  </Text>
                </TouchableOpacity>
              </View>

              <CardSection>
                <InputText
                  onChangeText={(e) =>
                    this.handleChangeText(e, 'ub_bank_address')
                  }
                  value={this.state.ub_bank_address}
                  placeholder="Masukkan Alamat bank"
                  label="Alamat bank *"
                  multiline={true}
                  numberOfLines={4}
                />
              </CardSection>
            </View>
          </ScrollView>
          <FooterButton onPress={() => this.bankDetails()}>
            Simpan Perubahan
          </FooterButton>
        </PTRView>
      </View>
    );
  }
}

const styles = {
  webViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop: 20,
  },
  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
    // padding:10
  },
  viewStyle: {
    marginleft: 5,
    marginRight: 5,
    marginTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  btnWrapper: {
    paddingBottom: 15,
  },

  // iconStyle:{
  //     // elevation: 2,
  //     // borderWidth:0.5,
  //     // borderColor:'#fff',

  // },
  abc: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent:'center'
    paddingLeft: 5,
  },
  labelstyle: {
    fontSize: hp('2%'),
    color: '#545454',
    marginRight: 10,
    marginLeft: 0,
    fontFamily: Font.RobotoRegular,
  },
  termConditionWrapper: {
    paddingLeft: 5,
    paddingRight: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    paddingTop: 5,
    paddingBottom: 8,
  },
  selectTxt: {
    color: '#8f8f8f',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoLight,
  },
  termTxt: {
    color: '#c90305',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.bank.loading,
    bank_info: state.bank.bank_info,
    bank_name: state.bank.bank_name,
    account_holder_name: state.bank.account_holder_name,
    account_number: state.bank.account_number,
    ifsc_code: state.bank.ifsc_code,
    paypal_id: state.bank.paypal_id,
    bank_address: state.bank.bank_address,
  };
};

export default connect(mapStateToProps, {
  saveBankDetails,
  changePaypalId,
  changeIfsc_code,
  changeAccountNumber,
  changeAccountHolderName,
  changeBankName,
  changeBankAddress,
  getBankDetails,
})(BankInfo);
