import React, {Component} from 'react';
import {
  AppState,
  AsyncStorage,
  ImageBackground,
  StatusBar,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {notificationApi} from '../services/services';
import {
  storeUnreadNotifications,
  storeNotifications,
  updateUserStatus,
} from '../actions';
import messaging from '@react-native-firebase/messaging';
import PushNotification, {Importance} from 'react-native-push-notification';
import DeviceInfo from 'react-native-device-info';

class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      _token: null,
    };
    this.createDefaultChannels();
    this._bootstrapAsync();
  }

  createDefaultChannels() {
    PushNotification.createChannel({
      channelId: 'com.syoobe', // (required)
      channelName: 'syoobe', // (required)
      channelDescription: 'A default channel', // (optional) default: undefined.
      soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
      importance: Importance.LOW, // (optional) default: Importance.HIGH. Int value of the Android notification importance
      vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
    });
  }

  async componentDidMount() {
    console.log('auth loading screen');
    AppState.addEventListener('change', this.handleAppStateChange);
    this.checkPermission();
    this.createNotificationListeners();
    this.getDeviceInfo();
  }

  handleAppStateChange = async (nextAppState) => {
    const userToken = await AsyncStorage.getItem('token');
    if (nextAppState === 'background') {
      this.props.updateUserStatus(0, userToken);
    }
    if (nextAppState === 'active') {
      this.props.updateUserStatus(1, userToken);
    }
  };

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  callNotifications = (token) => {
    this.setState({
      loading: true,
    });
    const details = {
      _token: token,
    };
    notificationApi({...details})
      .then((response) => {
        // console.log('response', response.data);
        if (response.data.status == 1) {
          this.props.storeUnreadNotifications(
            response.data.total_unread_notification,
          );
          this.props.storeNotifications(
            response.data.user_notification_details,
          );
        }
        this.setState({
          loading: false,
        });
      })
      .catch((error) => console.log('ee', error));
  };

  _bootstrapAsync = async () => {
    await AsyncStorage.getItem('token').then((res) =>
      this.setState({_token: res}),
    );
    if (this.state._token) {
      await this.callNotifications(this.state._token);
      this.props.navigation.replace('HomeDrawer');
    } else {
      this.props.navigation.replace('AuthDrawer');
    }
  };

  async checkPermission() {
    const enabled = await messaging().hasPermission();
    console.log('enabled', enabled);
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    var token = await messaging().getToken();
    console.log('token', token);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log('token', fcmToken);
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  async getDeviceInfo() {
    const deviceId = DeviceInfo.getUniqueId();
    if (deviceId) {
      await AsyncStorage.setItem('deviceInfo', deviceId);
    }
  }

  async requestPermission() {
    try {
      await messaging().requestPermission();
      this.getToken();
    } catch (error) {
      console.log('permission rejected');
    }
  }

  async createNotificationListeners() {
    // const notificationOpen = await messaging().getInitialNotification();
    // if (notificationOpen) {
    //   console.log('no33', notificationOpen);
    //   this.goToNotifications(notificationOpen.notification.data);
    // }

    // await messaging().onNotificationOpenedApp((notificationOpen) => {
    //   console.log('no2', notificationOpen);
    //   // const { title, body } = notificationOpen.notification;
    //   // this.showAlert(title, body);
    // });

    await messaging().onMessage((message) => {
      PushNotification.localNotification({
        channelId: 'com.syoobe',
        title: message.data?.title,
        message: message.data?.message,
        soundName: 'default',
      });
    });
  }

  goToNotifications = async (data) => {
    this.props.navigation.navigate('Notifications');
  };

  render() {
    return (
      <View>
        <StatusBar backgroundColor="transparent" transcluent />
        <ImageBackground
          style={styles.bgImg}
          source={require('../images/loginBg.png')}
        />
      </View>
    );
  }
}

const styles = {
  bgImg: {
    height: '100%',
  },
};

const mapStateToProps = (state) => {
  return {
    // notifications: state.notification.notifications,
    // unreadNotificationCount: state.notification.unreadNotificationCount,
  };
};

export default connect(mapStateToProps, {
  storeUnreadNotifications,
  storeNotifications,
  updateUserStatus,
})(AuthLoadingScreen);
