import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  AsyncStorage,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import axios from 'axios';
import Bugsnag from '@bugsnag/react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {Font} from './Font';
import {substr} from '../helpers/helper';

class Messages extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      _token: null,
      messagesData: null,
      page: null,
      total_page: null,
      total_record: null,
    };
  }

  componentDidMount() {
    this.retrieveToken().then((_token) => {
      this.getmessageList(_token);
    });
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
    return;
  };

  getmessageList = (_token) => {
    this.setState({
      loading: true,
    });

    var details = {
      _token: _token,
    };

    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/sellerBuyerMessageListing', formBody)
      .then((response) => {
        // console.log('respons message', JSON.stringify(response.data));
        this.setState({
          loading: false,
        });
        if (response.data.status === 1) {
          this.setState({
            messagesData: response.data.user_message_details,
            page: response.data.page,
            total_record: response.data.page.total_record,
            total_page: response.data.page.total_page,
          });
        }
      });
  };

  render() {
    return (
      <View style={{flex: 1, marginVertical: hp(1)}}>
        <View style={styles.headerMessage}>
          <View style={{width: wp(10)}}>
            <Text style={styles.fontBold}>Dari</Text>
          </View>
          <View style={{width: wp(17)}}>
            <Text style={styles.fontBold}>Subject</Text>
          </View>
          <View style={styles.headerWrapper}>
            <Text style={styles.fontBold}>Message</Text>
          </View>
          <View style={styles.headerWrapper}></View>
        </View>
        <ScrollView style={{marginHorizontal: wp(2), marginVertical: hp(2)}}>
          {this.state.messagesData &&
            this.state.messagesData?.map((messagesData) => (
              <View style={styles.wrapperContent}>
                <View>
                  <Image
                    style={styles.chatIcon}
                    // resize="contain"
                    source={{
                      uri: messagesData.message_sent_to_image,
                    }}
                  />

                  <Text style={styles.fontBold}>
                    {substr(messagesData.message_sent_to_username, 15)}
                  </Text>
                </View>
                <View style={styles.content}>
                  <View style={{width: wp(20)}}>
                    <Text style={styles.regularFont}>
                      {messagesData.message_subject}
                    </Text>
                  </View>
                  <View style={{width: wp(40)}}>
                    <Text style={styles.regularFont}>
                      {messagesData.message}
                    </Text>
                  </View>
                  {console.log(
                    'ini message data',
                    JSON.stringify(messagesData),
                  )}
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('ReplyMessage', {
                        thread_id: messagesData.thread_id,
                        _token: this.state._token,
                      })
                    }>
                    <FontAwesome5Icon
                      name={'reply'}
                      size={hp(2.5)}
                      color={'#C90205'}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerMessage: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  headerWrapper: {
    width: wp(20),

    // alignItems: 'center',
  },
  fontBold: {
    fontFamily: Font.RobotoMedium,
    fontWeight: 'bold',
    color: '#000',
    fontSize: hp(1.8),
  },
  regularFont: {
    fontFamily: Font.RobotoMedium,
    color: '#000',
    fontSize: hp(1.5),
  },
  wrapperContent: {
    flexDirection: 'row',
    borderWidth: 0.5,
    margin: 4,
    padding: 5,
    borderColor: 'red',
  },
  content: {
    flexDirection: 'row',
    width: wp(78),
    justifyContent: 'space-around',
    padding: wp(2),
    // backgroundColor: 'yellow'
  },

  chatIcon: {
    width: 50,
    height: 50,
    borderRadius: 50,
  },

  bodyRighChat: {
    fontSize: 16,
    color: '#fff',
  },

  rightChat: {
    backgroundColor: '#0078fe',
    padding: 10,
    marginLeft: '45%',
    borderRadius: 25,
    marginTop: 5,
    marginRight: '5%',
    maxWidth: '50%',
    alignSelf: 'flex-end',
  },

  rightArrow: {
    position: 'absolute',
    backgroundColor: '#0078fe',
    //backgroundColor:"red",
    width: 20,
    height: 25,
    bottom: 0,
    borderBottomLeftRadius: 25,
    right: -10,
  },

  rightArrowOverlap: {
    position: 'absolute',
    backgroundColor: '#fff',
    //backgroundColor:"green",
    width: 20,
    height: 35,
    bottom: -6,
    borderBottomLeftRadius: 18,
    right: -20,
  },
  leftArrowOverlap: {
    position: 'absolute',
    backgroundColor: '#fff',
    width: 20,
    height: 35,
    bottom: -6,
    borderBottomRightRadius: 18,
    left: -20,
  },
  leftChat: {
    backgroundColor: '#dedede',
    padding: 10,
    borderRadius: wp(10),
    marginTop: 5,
    marginLeft: '5%',
    maxWidth: '50%',
    alignSelf: 'flex-start',
  },
  bodyLeftChat: {
    fontSize: 16,
    color: '#000',
    justifyContent: 'center',
  },
  bodyRightDateChat: {
    fontSize: 10,
    color: '#fff',
  },
  bodyLeftDateChat: {
    fontSize: 10,
    color: '#000',
    justifyContent: 'center',
  },
  touchimgChangeIcon: {
    position: 'absolute',
    width: wp('10%'),
    height: wp('10%'),
    right: -20,
    top: 20,
  },
  imgChangeIcon: {
    width: wp('10%'),
    height: wp('10%'),
  },
});

export default Messages;
