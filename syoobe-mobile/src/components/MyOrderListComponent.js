import Clipboard from '@react-native-community/clipboard';
import React, {Component} from 'react';
import {
  Alert,
  AsyncStorage,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import PTRView from 'react-native-pull-to-refresh';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Fontisto from 'react-native-vector-icons/Fontisto';
import {connect} from 'react-redux';
import {
  emptyDownloadHistory,
  emptyOrderHistory,
  getBuyerDownloads,
  getBuyerOrders,
  getOrderDetails,
  getOrderHistoryStatusList,
  getSalesList,
} from '../actions';
import {formatRupiah} from '../helpers/helper';
import {ProductOrderFilter} from './common/product/ProductOrderFilter';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

class MyOrderListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _token: null,
      status_id: null,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({_token: userToken});
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount() {
    console.log('componentDidMount');
    this.fetchData();
  }

  fetchData = () => {
    this.retrieveToken().then((_token) => {
      let details = {
        _token,
        status: this.state.status_id,
      };
      if (this.props.type === 'sales') {
        this.props.getSalesList(details);
      } else {
        this.props.getBuyerOrders(details, 'new_search');
        this.props.getOrderHistoryStatusList(_token);
      }
    });
  };

  loadNewData = (id) => {
    this.fetchData();
  };

  handleCopy = (text) => {
    // alert(text);
    Clipboard.setString(text);
    Alert.alert('Berhasil disalin ke clipboard');
  };

  toOrderDetails = (item) => {
    let details = {
      _token: this.state._token,
      order_id: item.order_id,
      order_product_id: item.order_product_id,
      is_free_shipping: item.is_free_shipping,
      product_type: item.product_type,
    };

    if (this.props.type === 'sales') {
      this.props.navigation.navigate('SalesPhysical', {details});
      // if (item.product_type == 'P') {
      // } else {
      //   this.props.navigation.navigate('SalesDigital', {details});
      // }
    } else {
      if (item.product_type == 'P') {
        this.props.navigation.navigate('ViewOrderPhysical', {details});
      } else {
        this.props.navigation.navigate('ViewOrderDigital', {details});
      }
    }
  };

  setStatusId = (data) => {
    this.setState({status_id: data.val});
    this.loadNewData(data.val);
    data.ref.close();
  };

  render() {
    // console.log('ini status', this.state.status_id)
    // console.log('in state', JSON.stringify(this.state));
    // console.log('in props', JSON.stringify(this.props));
    // console.log(this.props);
    // this.fetchData();
    if (this.props.type === 'sales') {
      if (!this.props.salesList) {
        return (
          <View style={{flex: 1, backgroundColor: '#f5f6fa'}}>
            <View>
              <ProductOrderFilter
                data={this.props.status_list}
                value={this.state.status_id}
                onPress={(data) => this.setStatusId(data)}
                style={{paddingVertical: hp(2.4)}}
              />
            </View>
            <EmptyOrder
              {...this.props}
              onRefresh={this.fetchData}
              type={this.props.type}
            />
          </View>
        );
      }
    } else {
      if (!this.props.order_list.length) {
        return (
          <View style={{flex: 1, backgroundColor: '#f5f6fa'}}>
            <View>
              <ProductOrderFilter
                data={this.props.status_list}
                value={this.state.status_id}
                onPress={(data) => this.setStatusId(data)}
                style={{paddingVertical: hp(2.4)}}
              />
            </View>

            <EmptyOrder
              {...this.props}
              onRefresh={this.fetchData}
              type={this.props.type}
            />
          </View>
        );
      }
    }
    return (
      <View style={{flex: 1, backgroundColor: '#f5f6fa'}}>
        <PTRView onRefresh={() => this.fetchData()}>
          <ProductOrderFilter
            data={this.props.status_list}
            value={this.state.status_id}
            onPress={(data) => this.setStatusId(data)}
          />
          <FlatList
            data={
              this.props.type === 'sales'
                ? this.props.salesList
                : this.props.order_list
            }
            showsVerticalScrollIndicator={false}
            renderItem={({item, index}) => (
              <TouchableOpacity
                key={index}
                activeOpacity={0.5}
                onPress={() => this.toOrderDetails(item)}>
                <View style={styles.cardContainer}>
                  <View style={{flexDirection: 'row', paddingTop: hp(1)}}>
                    <View style={styles.image}>
                      <Image
                        style={{width: '100%', height: '100%'}}
                        source={{uri: item.image_path}}
                      />
                    </View>
                    <View style={{flex: 1, paddingLeft: wp(3)}}>
                      <Text style={styles.textTitle}>{item.product_name}</Text>
                      <View style={styles.textContainerPrice}>
                        <Text style={styles.textPrice}>
                          Rp. {formatRupiah(parseInt(item.product_price))} (
                          {item.product_quantity})
                        </Text>

                        <View style={{flexDirection: 'column-reverse'}}>
                          {/* <Text style={{fontSize: 30}}> > </Text> */}
                          <FontAwesome5Icon name="chevron-right" size={hp(2)} />
                        </View>

                        {/* <Text
                        style={
                          styles.textPriceQty
                        }>{`x${item.product_quantity}`}</Text> */}
                      </View>
                      {item.is_free_shipping && (
                        <View style={{padding: 5}}>
                          <Image
                            resizeMode="cover"
                            style={styles.freeShipImg}
                            source={require('../images/free_shipping.png')}
                          />
                        </View>
                      )}
                      {item.product_type === 'I' && (
                        <View style={{padding: 5}}>
                          <Image
                            resizeMode="cover"
                            style={styles.insdown}
                            source={require('../images/download.png')}
                          />
                        </View>
                      )}
                      {item.product_type === 'G' && (
                        <View style={{padding: 5}}>
                          <Image
                            resizeMode="cover"
                            style={styles.insdown}
                            source={require('../images/graphic_design.png')}
                          />
                        </View>
                      )}
                    </View>
                  </View>

                  <View style={styles.cardItems}>
                    <Text
                      style={{
                        ...styles.textPrice,
                        fontWeight: 'normal',
                        color: '#000',
                      }}>
                      Tanggal Pembelian
                    </Text>
                    <Text
                      style={{
                        ...styles.textPrice,
                        fontWeight: 'normal',
                        color: '#000',
                      }}>
                      {item.order_date}
                    </Text>
                  </View>
                  <View style={styles.cardItems}>
                    <Text
                      style={{
                        ...styles.textPrice,
                        color: '#000',
                        fontWeight: 'normal',
                      }}>
                      No Faktur
                    </Text>
                    <Text
                      onPress={() => this.handleCopy(item.invoice_number)}
                      style={{
                        ...styles.textPrice,
                        color: '#000',
                        fontWeight: 'normal',
                      }}>{`${item.invoice_number}`}</Text>
                  </View>
                  <View style={styles.cardItems}>
                    <Text
                      style={{
                        ...styles.textPrice,
                        fontWeight: 'normal',
                        color: '#000',
                      }}>
                      Status
                    </Text>

                    {this.props.type === 'sales' ? (
                      <Text
                        style={
                          item.status === 'Menunggu Konfirmasi Pembayaran'
                            ? styles.statusPending
                            : item.status === 'Pembayaran Sudah Diverifikasi'
                            ? styles.statusPaymentConfirm
                            : item.status === 'Pesanan Anda Sedang Diproses'
                            ? styles.statusProcces
                            : item.status === 'Pesanan Anda Telah Dikirim'
                            ? styles.statusShipped
                            : item.status === 'Pesanan Anda Telah Diterima '
                            ? styles.statusDelivered
                            : item.status === 'Permintaan Pengembalian Produk'
                            ? styles.statusRequestCancel
                            : item.status === 'Transaksi Ini Sudah Selesai'
                            ? styles.statusCompleted
                            : item.status === 'Pesanan Dibatalkan Oleh Pembeli'
                            ? styles.statusReturn
                            : item.status === 'Pesanan Dikembalikan'
                            ? styles.statusReturn
                            : styles.statusDef
                        }>
                        {item.status}
                      </Text>
                    ) : (
                      <Text
                        style={
                          item.order_status === 'Menunggu Konfirmasi Pembayaran'
                            ? styles.statusPending
                            : item.order_status ===
                              'Pembayaran Sudah Diverifikasi'
                            ? styles.statusPaymentConfirm
                            : item.order_status ===
                              'Pesanan Anda Sedang Diproses'
                            ? styles.statusProcces
                            : item.order_status === 'Pesanan Anda Telah Dikirim'
                            ? styles.statusShipped
                            : item.order_status ===
                              'Pesanan Anda Telah Diterima '
                            ? styles.statusDelivered
                            : item.order_status ===
                              'Permintaan Pengembalian Produk'
                            ? styles.statusRequestCancel
                            : item.order_status ===
                              'Transaksi Ini Sudah Selesai'
                            ? styles.statusCompleted
                            : item.order_status ===
                              'Pesanan Dibatalkan Oleh Pembeli'
                            ? styles.statusReturn
                            : item.order_status === 'Pesanan Dikembalikan'
                            ? styles.statusReturn
                            : styles.statusDef
                        }>
                        {item.order_status}
                      </Text>
                    )}
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </PTRView>
      </View>
    );
  }
}

const EmptyOrder = ({onRefresh, title, type}) => {
  const typeOrder = type === 'sales' ? 'Penjualan' : 'Pembelian';
  return (
    <View
      onRefresh={onRefresh}
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp(2),
      }}>
      <Fontisto name={'shopping-bag-1'} size={hp(15)} />
      <Text style={{paddingTop: hp(2), width: wp(50), textAlign: 'center'}}>
        {title ? title : `Kamu Belum Melakukan ${typeOrder} Apapun!`}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    display: 'flex',
    backgroundColor: '#fff',
    paddingVertical: hp(1),
    paddingHorizontal: wp(3),
    marginVertical: hp(1),
  },
  image: {
    width: wp(25),
    height: hp(14),
    maxHeight: hp(14),
    maxWidth: wp(25),
    backgroundColor: '#eaeaea',
  },
  textTitle: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#353b48',
    paddingBottom: hp(1),
  },
  textContainerPrice: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textPrice: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#e84118',
  },
  textPriceQty: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#353b48',
  },
  cardItems: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: hp(1),
    borderTopColor: '#f5f6fa',
    borderTopWidth: 1,
    marginTop: hp(1),
  },
  insdown: {
    width: 25,
    height: 25,
    marginTop: hp(2),
  },
  statusCompleted: {
    fontSize: hp(2),
    textAlign: 'center',
    color: '#6699ff',
  },
  statusDelivered: {
    fontSize: hp(2),
    textAlign: 'center',
    color: '#99cc00',
  },
  statusShipped: {
    fontSize: hp(2),
    textAlign: 'center',
    color: '#ff9966',
  },
  statusProcces: {
    fontSize: hp(2),
    textAlign: 'center',
    color: '#cc00ff',
  },
  statusPaymentConfirm: {
    fontSize: hp(2),
    textAlign: 'center',
    color: '#80ff00',
  },
  statusReturn: {
    fontSize: hp(2),
    textAlign: 'center',
    color: '#802000',
  },
  statusCanceled: {
    fontSize: hp(2),
    textAlign: 'center',
    color: '#cc3300',
  },
  statusPending: {
    fontSize: hp(2),
    textAlign: 'center',
    color: '#e63900',
  },
  statusRequestCancel: {
    fontSize: hp(2),
    textAlign: 'center',
    color: '#ff8000',
  },
  statusDef: {
    fontSize: hp(2),
    textAlign: 'center',
    color: 'blue',
  },
});

const mapStateToProps = (state) => {
  return {
    order_list: state.buyerOrders.order_list,
    no_orders: state.buyerOrders.no_orders,
    total_page: state.buyerOrders.total_page,
    loading: state.buyerOrders.loading,
    scrollLoading: state.buyerOrders.scrollLoading,
    endOfRecords: state.buyerOrders.endOfRecords,
    download_list: state.buyerOrders.download_list,
    total_page_downloads: state.buyerOrders.total_page_downloads,
    endOfDownloads: state.buyerOrders.endOfDownloads,
    no_downloads: state.buyerOrders.no_downloads,
    salesList: state.orderDetails.salesList,
    status_list: state.buyerOrders.status_list,
  };
};

export default connect(mapStateToProps, {
  getOrderDetails,
  getOrderHistoryStatusList,
  emptyDownloadHistory,
  getBuyerDownloads,
  emptyOrderHistory,
  getBuyerOrders,
  getSalesList,
})(MyOrderListComponent);
