import React, {Component} from 'react';
import {
  PermissionsAndroid,
  AsyncStorage,
  Alert,
  View,
  ScrollView,
  TouchableOpacity,
  Modal,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {
  Spinner,
  Button,
  CardSection,
  TextareaInner,
  ButtonTwo,
  InputInner,
} from './common';
import {BASE_URL} from '../services/baseUrl';
import {ERROR_MESSAGE} from '../constants/constants';
import {connect} from 'react-redux';
import {getOrderDetails} from '../actions';
import axios from 'axios';
import RNFetchBlob from 'react-native-fetch-blob';
import {randomColor, setIndexProgress} from '../helpers/helper';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import StepIndicator from 'react-native-step-indicator';
import {Provider} from 'react-native-paper';
import {ItemActionButton} from './common/view-product/ActionButton';
import {PopUpInfo} from './common/view-product/PopUpInfo';
import {
  ProductItemCard,
  Section,
} from './common/view-product/ProductDetailCard';
import {formatRupiah, substr} from '../helpers/helper';
import PTRView from 'react-native-pull-to-refresh';
import Clipboard from '@react-native-community/clipboard';
import {Text, Button as ButtonNative} from 'native-base';

class ViewOrderDigital extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showComponmentDownloadYourFiles: false,
      selectedIndexDownloadYourFiles: -1,
      showComponmentSampleFiles: false,
      selectedIndexSampleFiles: -1,
      showComponmentProductionFiles: false,
      selectedIndexProductionFiles: -1,
      showComponmentOneTimeAccess: false,
      selectedIndexOneTimeAccess: -1,
      currentPosition: 0,
      visible: false,
      _token: null,
      loading2: false,
      order_product_id: props.route.params.details.order_product_id,
      details: props.route.params.details,
      sendMessage: false,
      subject: '',
      message: '',
      user_id: null,
    };
  }

  handleCopy = (text) => {
    // alert(text);
    Clipboard.setString(text);
    Alert.alert('Berhasil disalin ke clipboard');
  };

  sendMessageToShop = () => {
    this.setState({
      sendMessage: true,
    });
  };

  closeMessageModal = () => {
    this.setState({
      sendMessage: false,
      subject: '',
      message: '',
    });
  };

  sendMessageContent = async () => {
    if (
      String(this.state.subject).trim() === '' ||
      String(this.state.message).trim() === ''
    ) {
      Alert.alert('Informasi', 'Kedua bidang wajib diisi', [{text: 'OK'}]);
    } else {
      var details = {};
      details = {
        ...details,
        ...{shop_id: this.props.data.shop_id},
        ...details,
        ...{_token: this.state._token},
        ...details,
        ...{thread_subject: this.state.subject},
        ...details,
        ...{message_text: this.state.message},
        ...details,
        ...{user_id: this.state.user_id},
      };

      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post(BASE_URL + '/sendMessageToShopOwner', formBody)
        .then((response) => {
          console.log('r', response);
          this.setState({
            sendMessage: false,
            subject: '',
            message: '',
          });
          if (response.data.status === 1) {
            Alert.alert('Informasi', 'Pesan terkirim!', [{text: 'OK'}]);
          }
        })
        .catch(() => {
          Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
        });
    }
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      const user_id = await AsyncStorage.getItem('user_id');
      this.setState({
        _token: userToken,
        user_id: user_id,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = () => {
    this.retrieveToken();
    this.props.getOrderDetails(this.props.route.params.details);
  };

  downloadAccess = async (data) => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Download to storage Permission',
          message:
            'Syoobe App needs access to your storage ' +
            'so that you can download files and save them.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        let dirs = RNFetchBlob.fs.dirs.DownloadDir + '/' + data.file_name;
        RNFetchBlob.config({
          path: dirs,
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: false,
            path: dirs,
          },
        })
          .fetch('GET', data.file_path)
          .then((res) => {
            console.log('ini res 2', JSON.stringify(res));
            this.setState({
              loading2: false,
            });

            Alert.alert('Informasi', 'Berhasil di download', [
              {
                text: 'OK',
                onPress: () =>
                  this.props.getOrderDetails(this.props.route.params.details),
              },
            ]);
          })
          .catch((error) => {
            Alert.alert('Informasi', 'Internal Server Error', [{text: 'OK'}]);
            console.log('error 123', error);
          });
      } else {
        console.log('Storage permission denied');
      }
    } catch (err) {
      this.setState({
        loading2: false,
      });
      console.warn(err);
    }
  };

  downloadFile = async (file, url, type) => {
    console.log('downloadFile');
    console.log('in', file);
    this.setState({
      loading2: true,
      showComponmentDownloadYourFiles: false,
      selectedIndexDownloadYourFiles: -1,
      showComponmentSampleFiles: false,
      selectedIndexSampleFiles: -1,
      showComponmentProductionFiles: false,
      selectedIndexProductionFiles: -1,
    });
    let details = {
      _token: this.state._token,
    };
    if (url == 'downloadFileForInstantDigitalProduct') {
      details = {
        ...details,
        ...{
          file_id: file.opf_id,
          opr_id: file.new_opr_id,
        },
      };
      if (type == 'one') {
        details = {
          ...details,
          ...{
            download_time: 'one',
          },
        };
      }
    } else if (url == 'downloadSampleFileForGraphics') {
      details = {
        ...details,
        ...{
          opr_id: file.muf_opr_id,
          muf_id: file.muf_id,
        },
      };
    } else if (url == 'downloadProductionFileForGraphics') {
      details = {
        ...details,
        ...{
          mpf_id: file.id,
        },
      };
    }

    let formBody = [];
    for (let property in details) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/' + url, formBody)
      .then((response) => {
        console.log('ini res 1', JSON.stringify(response));
        if (response.data.status === 1) {
          this.downloadAccess(response.data);
        } else {
          Alert.alert('informasi', response.data.msg, [{text: 'OK'}]);
          this.setState({
            loading2: false,
          });
        }
      })
      .catch((err) => {
        console.log('e', err);
        this.setState({
          loading2: false,
        });
      });
  };

  acceptRequest = async (file) => {
    this.setState({
      loading2: true,
      showComponmentDownloadYourFiles: false,
      selectedIndexDownloadYourFiles: -1,
      showComponmentSampleFiles: false,
      selectedIndexSampleFiles: -1,
      showComponmentProductionFiles: false,
      selectedIndexProductionFiles: -1,
    });
    let details = {
      _token: this.state._token,
      muf_id: file.muf_id,
      opr_id: file.muf_opr_id,
    };
    let formBody = [];
    for (let property in details) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/acceptRequestInGraphicsProduct', formBody)
      .then((response) => {
        console.log(response);
        if (response.data.status === 1) {
          this.setState({
            loading2: false,
          });
          this.props.getOrderDetails(this.props.route.params.details);
          Alert.alert('Permintaan disetujiu');
        } else {
          this.setState({
            loading2: false,
          });
          Alert.alert('Permintaan ditolak, silahkan coba lagi');
        }
      })
      .catch((err) => {
        console.log('e', err);
        this.setState({
          loading2: false,
        });
      });
  };

  goToRequirements = () => {
    this.props.navigation.navigate('SendYourRequirements', {
      _token: this.state._token,
      order_product_id: this.props.route.params.details.order_product_id,
      vendor_id: this.props.data.vendor_id,
      details: this.props.route.params.details,
    });
  };

  render() {
    // console.log('in state', JSON.stringify(this.state));
    // console.log(
    //   'in props download_file 13',
    //   JSON.stringify(this.props.data?.download_file),
    // );
    console.log(
      'in     this.props.data.requirement_sent',
      JSON.stringify(this.props.data),
    );

    if (this.props.loading || this.state.loading2) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Spinner color="red" size="large" />
        </View>
      );
    }

    const labels = [
      'Pembayaran',
      'Dalam Proses',
      'Contoh File',
      'File Produksi',
      'Lengkap',
    ];
    const customStyles = {
      stepIndicatorSize: 25,
      currentStepIndicatorSize: 30,
      separatorStrokeWidth: 2,
      currentStepStrokeWidth: 3,
      stepStrokeCurrentColor: '#fe7013',
      stepStrokeWidth: 3,
      stepStrokeFinishedColor: '#fe7013',
      stepStrokeUnFinishedColor: '#aaaaaa',
      separatorFinishedColor: '#fe7013',
      separatorUnFinishedColor: '#aaaaaa',
      stepIndicatorFinishedColor: '#fe7013',
      stepIndicatorUnFinishedColor: '#ffffff',
      stepIndicatorCurrentColor: '#ffffff',
      stepIndicatorLabelFontSize: 13,
      currentStepIndicatorLabelFontSize: 13,
      stepIndicatorLabelCurrentColor: '#fe7013',
      stepIndicatorLabelFinishedColor: '#ffffff',
      stepIndicatorLabelUnFinishedColor: '#aaaaaa',
      labelColor: '#999999',
      labelSize: 10,
      currentStepLabelColor: '#fe7013',
    };

    return (
      <Provider>
        <PTRView
          onRefresh={() =>
            this.props.getOrderDetails(this.props.route.params.details)
          }>
          <ScrollView style={{flex: 1, backgroundColor: '#f5f6fa'}}>
            {this.props.data != null && (
              <View>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('ProductDetail', {
                      product_id: this.props.data.detail.opr_product_id,
                    })
                  }>
                  {/* card product */}
                  <ProductItemCard
                    {...this.props.route.params.details}
                    data={this.props.data}
                  />
                </TouchableOpacity>
                {/* end card product */}
                {/* invoice */}
                <View
                  style={{
                    paddingVertical: hp(2),
                    paddingHorizontal: wp(3),
                    backgroundColor: '#fff',
                    marginBottom: hp(2),
                  }}>
                  {/* <Text
            style={{
              fontSize: hp(2),
              fontWeight: 'bold',
              color: '#000',
              paddingBottom: hp(2),
            }}>
            Invoice
          </Text> */}
                  <View style={{}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingBottom: hp(1.5),
                      }}>
                      <Text>Status Pembayaran</Text>
                      <Text style={{fontWeight: 'bold', color: '#000'}}>
                        {this.props.data.order_status}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 2,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Tanggal Pembelian</Text>
                      <Text style={{fontWeight: 'bold', color: '#000'}}>
                        {this.props.data.order_date}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingTop: hp(1.5),
                        borderTopWidth: 2,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Nomor Invoice</Text>
                      <Text>{this.props.data.order_invoice_number}</Text>
                    </View>
                  </View>
                </View>
                {/* end invoice */}
                {/* Price Detail */}
                <View
                  style={{
                    paddingVertical: hp(2),
                    paddingHorizontal: wp(3),
                    backgroundColor: '#fff',
                    marginBottom: hp(2),
                  }}>
                  <Text
                    style={{
                      fontSize: hp(2),
                      fontWeight: 'bold',
                      color: '#000',
                      paddingBottom: hp(2),
                    }}>
                    Detail Harga
                  </Text>
                  <View style={{}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 2,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Sub Total</Text>
                      <Text style={{fontWeight: 'bold', color: '#000'}}>
                        {`Rp. ${this.props.data.cart_total}`}
                      </Text>
                    </View>

                    {this.props.data.detail.opr_tax &&
                      this.props.data.detail.opr_tax > 0 && (
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: hp(1.5),
                            borderTopWidth: 2,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <Text>Pajak</Text>
                          <Text
                            style={{
                              fontWeight: 'bold',
                              color: '#000',
                            }}>
                            Rp.{' '}
                            {formatRupiah(
                              parseInt(this.props.data.detail.opr_tax),
                            )}
                          </Text>
                        </View>
                      )}

                    {this.props.data.order_discount && (
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          paddingVertical: hp(1.5),
                          borderTopWidth: 2,
                          borderTopColor: '#f5f6fa',
                        }}>
                        <Text>Promo</Text>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            color: '#000',
                          }}>{`-Rp. ${this.props.data.order_discount}`}</Text>
                      </View>
                    )}

                    {this.props.data.order_reward && (
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          paddingVertical: hp(1.5),
                          borderTopWidth: 2,
                          borderTopColor: '#f5f6fa',
                        }}>
                        <Text>Poin Hadiah</Text>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            color: '#000',
                          }}>{`-Rp. ${this.props.data.order_reward}`}</Text>
                      </View>
                    )}

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingTop: hp(1.5),
                        borderTopWidth: 2,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Total Keseluruhan
                      </Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>{`Rp. ${
                        this.props.data.grand_total
                          ? this.props.data.grand_total
                          : 0
                      }`}</Text>
                    </View>
                  </View>
                </View>
                {/* End Price Detail */}

                {/* Progress step */}
                {this.props.data.product_type == 'G' &&
                  this.props.data.graphics_product_status_bar && (
                    <View
                      style={{
                        paddingVertical: hp(2),
                        paddingHorizontal: wp(3),
                        backgroundColor: '#fff',
                        marginBottom: hp(2),
                      }}>
                      <View>
                        {this.props.data.download_file ? (
                          <Text
                            style={{textAlign: 'left', paddingVertical: hp(1)}}>
                            Silakan hubungi Penjual secara langsung jika Anda
                            memiliki pertanyaan mengenai pesanan produk Anda
                            atau mengalami unduhan apa pun masalah.
                          </Text>
                        ) : (
                          <View>
                            <Text
                              style={{
                                textAlign: 'left',
                                fontSize: hp(2),
                                fontWeight: 'bold',
                              }}>
                              Apakah Anda ingin tahu apa status pesanan Anda
                              dengan benar sekarang?
                            </Text>
                            <Text
                              style={{
                                textAlign: 'left',
                                paddingVertical: hp(1),
                              }}>
                              Pakar TI di Syoobe telah merekayasa secara khusus
                              Pelacak Syoobe untuk terus mengabari Anda tentang
                              status pesanan Anda.
                            </Text>
                          </View>
                        )}
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingTop: hp(1.5),
                            borderTopWidth: 2,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <Text
                            style={{
                              // fontWeight: 'bold',
                              color: '#000',
                            }}>
                            Anda akan menerima file dalam
                          </Text>

                          <Text
                            style={{
                              fontWeight: 'bold',
                              color: '#000',
                            }}>
                            {this.props.data.graphics_product_status_bar.date}
                          </Text>
                        </View>
                      </View>
                    </View>
                  )}
                {/* progress step */}
                {this.props.data.graphics_product_status_bar && (
                  <View
                    style={{
                      paddingVertical: hp(2),
                      paddingHorizontal: wp(3),
                      backgroundColor: '#fff',
                      marginBottom: hp(2),
                    }}>
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontWeight: 'bold',
                        color: '#000',
                        paddingBottom: hp(2),
                        textAlign: 'center',
                      }}>
                      {
                        labels[
                          setIndexProgress(
                            this.props.data.graphics_product_status_bar,
                          )
                        ]
                      }
                    </Text>
                    <StepIndicator
                      customStyles={customStyles}
                      currentPosition={setIndexProgress(
                        this.props.data.graphics_product_status_bar,
                      )}
                      labels={labels}
                      // direction={'vertical'}
                    />
                  </View>
                )}

                {/* Download Instant */}
                {this.props.data.download_file != null && (
                  <View
                    style={{
                      paddingVertical: hp(2),
                      paddingHorizontal: wp(3),
                      backgroundColor: '#fff',
                      marginBottom: hp(2),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingBottom: hp(2),
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontSize: hp(2),
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Unduh File Anda
                      </Text>
                      <PopUpInfo
                        // title={'Catatan'}
                        message={
                          'Harap hubungi Penjual secara langsung jika Anda memiliki pertanyaan tentang pesanan produk Anda atau mengalami masalah dengan pengunduhan.'
                        }
                      />
                    </View>

                    {this.props.data.download_file.map(
                      (download_file, index) => (
                        <View
                          style={{
                            flexDirection: 'row',
                            paddingVertical: hp(1),
                            borderTopWidth: 1,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <View
                            style={{
                              width: wp(10),
                              height: hp(5),
                              backgroundColor: randomColor(),
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderRadius: hp(2.5),
                            }}>
                            <FontAwesome5Icon
                              name={'file-alt'}
                              size={hp(2.5)}
                              color={'#fff'}
                            />
                          </View>

                          <View style={{flex: 1, paddingLeft: wp(3)}}>
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                              }}>
                              <Text
                                style={{
                                  fontSize: hp(2),
                                  fontWeight: 'bold',
                                  color: '#353b48',
                                  // textTransform: 'capitalize',s
                                }}>
                                {substr(download_file.file_name || '', 20)}
                              </Text>

                              {download_file.download_button_enable && (
                                <ItemActionButton
                                  onDownload={() =>
                                    this.downloadFile(
                                      download_file,
                                      'downloadFileForInstantDigitalProduct',
                                    )
                                  }
                                  onAcceptRequest={() => null}
                                  onSendRevisionRequest={() => null}
                                  disableAccept={true}
                                  disableRevision={true}
                                  productions={true}
                                  downloadDisabled={false}
                                />
                              )}
                            </View>

                            <Text style={{flex: 1, fontSize: hp(1.5)}}>
                              {download_file.download_details_text}
                            </Text>
                            <Text style={{fontSize: hp(1.5), color: '#e84118'}}>
                              {download_file.download_status}
                            </Text>
                          </View>
                        </View>
                      ),
                    )}
                  </View>
                )}
                {/* One time download */}
                {this.props.data.one_time_access != null && (
                  <View
                    style={{
                      paddingVertical: hp(2),
                      paddingHorizontal: wp(3),
                      backgroundColor: '#fff',
                      marginBottom: hp(2),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingBottom: hp(2),
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontSize: hp(2),
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Akses Unduhan Satu Kali
                      </Text>
                    </View>
                    {this.props.data.one_time_access.map(
                      (one_time_access, index) => (
                        <View
                          style={{
                            flexDirection: 'row',
                            paddingVertical: hp(1),
                            borderTopWidth: 1,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <View
                            style={{
                              width: wp(10),
                              height: hp(5),
                              backgroundColor: randomColor(),
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderRadius: hp(2.5),
                            }}>
                            <FontAwesome5Icon
                              name={'file-alt'}
                              size={hp(2.5)}
                              color={'#fff'}
                            />
                          </View>

                          <View style={{flex: 1, paddingLeft: wp(3)}}>
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                              }}>
                              <Text
                                style={{
                                  fontSize: hp(2),
                                  fontWeight: 'bold',
                                  color: '#353b48',
                                  // textTransform: 'capitalize',s
                                }}>
                                {one_time_access.file_name}
                              </Text>

                              {one_time_access.file_status === 'NA' && (
                                <ItemActionButton
                                  onDownload={() =>
                                    this.downloadFile(
                                      one_time_access,
                                      'downloadFileForInstantDigitalProduct',
                                      'one',
                                    )
                                  }
                                  onAcceptRequest={() => null}
                                  onSendRevisionRequest={() => null}
                                  disableAccept={true}
                                  disableRevision={true}
                                  productions={true}
                                  downloadDisabled={false}
                                />
                              )}
                            </View>
                            <Text style={{flex: 1, fontSize: hp(1.5)}}>
                              {one_time_access.download_details_text}
                            </Text>
                            <Text style={{fontSize: hp(1.5), color: '#e84118'}}>
                              {one_time_access.file_status}
                            </Text>
                          </View>
                        </View>
                      ),
                    )}
                  </View>
                )}

                {/* payment history */}
                <View
                  style={{
                    paddingVertical: hp(2),
                    paddingHorizontal: wp(3),
                    backgroundColor: '#fff',
                    marginBottom: hp(2),
                  }}>
                  {this.props.payment_history.map((items) => (
                    <Section>
                      <Text
                        style={{
                          fontSize: hp(2),
                          fontWeight: 'bold',
                          color: '#000',
                          paddingBottom: hp(2),
                        }}>
                        Rincian Pembayaran
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          paddingVertical: hp(1.5),
                          borderTopWidth: 1,
                          borderTopColor: '#f5f6fa',
                        }}>
                        <Text
                          style={{
                            color: '#000',
                          }}>
                          Tanggal
                        </Text>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            color: '#000',
                          }}>
                          {items.payment_history_date}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          paddingVertical: hp(1.5),
                          borderTopWidth: 1,
                          borderTopColor: '#f5f6fa',
                        }}>
                        <Text
                          style={{
                            color: '#000',
                          }}>
                          Kode Transaksi
                        </Text>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            color: '#000',
                          }}
                          onPress={() => this.handleCopy(items.transaction_id)}>
                          {substr(items.transaction_id || '', 25)}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          paddingVertical: hp(1.5),
                          borderTopWidth: 1,
                          borderTopColor: '#f5f6fa',
                        }}>
                        <Text
                          style={{
                            color: '#000',
                          }}>
                          Metode Pembayaran
                        </Text>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            color: '#000',
                          }}>
                          {items.payment_method}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          paddingVertical: hp(1.5),
                          borderTopWidth: 1,
                          borderTopColor: '#f5f6fa',
                        }}>
                        <Text
                          style={{
                            color: '#000',
                          }}>
                          Jumlah
                        </Text>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            color: '#000',
                          }}>
                          Rp. {formatRupiah(items.amount)}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          paddingTop: hp(1.5),
                          borderTopWidth: 1,
                          borderTopColor: '#f5f6fa',
                        }}>
                        <Text
                          style={{
                            color: '#000',
                          }}>
                          Komentar
                        </Text>
                        <Text
                          style={{
                            fontWeight: 'bold',
                            color: '#000',
                          }}>
                          {items.comments}
                        </Text>
                      </View>
                    </Section>
                  ))}
                </View>

                {/* Sample file */}
                {this.props.data.sample_file != null && (
                  <View
                    style={{
                      paddingVertical: hp(2),
                      paddingHorizontal: wp(3),
                      backgroundColor: '#fff',
                      marginBottom: hp(2),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingBottom: hp(2),
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontSize: hp(2),
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Contoh File
                      </Text>
                      <PopUpInfo
                        title={'Catatan'}
                        message={
                          'Harap unduh file sampel Anda untuk ditinjau, jika file tersebut sesuai harapan Anda, Silakan klik ikon Terima permintaan. Penjual akan mengunggah file baru untuk file Produksi dan Anda harus melakukannya unduh dari tab File Produksi untuk menghindari tanda air di file.'
                        }
                      />
                    </View>
                    {this.props.data.sample_file.map((sampleItem, index) => (
                      <View
                        style={{
                          flexDirection: 'row',
                          paddingVertical: hp(1),
                          borderTopWidth: 1,
                          borderTopColor: '#f5f6fa',
                        }}>
                        {console.log('file sample', JSON.stringify(sampleItem))}
                        <View
                          style={{
                            width: wp(10),
                            height: hp(5),
                            backgroundColor: randomColor(),
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: hp(2.5),
                          }}>
                          <FontAwesome5Icon
                            name={'file-alt'}
                            size={hp(2.5)}
                            color={'#fff'}
                          />
                        </View>

                        <View style={{flex: 1, paddingLeft: wp(3)}}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <Text
                              style={{
                                fontSize: hp(2),
                                fontWeight: 'bold',
                                color: '#353b48',
                                // textTransform: 'capitalize',s
                              }}>
                              {substr(sampleItem.file_name || '', 20)}
                            </Text>

                            <ItemActionButton
                              onDownload={() =>
                                this.downloadFile(
                                  sampleItem,
                                  'downloadSampleFileForGraphics',
                                )
                              }
                              onAcceptRequest={() =>
                                this.acceptRequest(sampleItem)
                              }
                              onSendRevisionRequest={() =>
                                this.props.navigation.navigate('SendRevision', {
                                  _token: this.state._token,
                                  order_product_id: this.props.route.params
                                    .details.order_product_id,
                                  muf_id: sampleItem.muf_id,
                                  muf_opr_id: sampleItem.muf_opr_id,
                                  // details: this.props.route.params.details,
                                })
                              }
                              disableAccept={sampleItem.accepted_btn_disable}
                              disableRevision={
                                sampleItem.revision_request_btn_disable
                              }
                            />
                          </View>
                          <Text style={{fontSize: hp(1.5)}}>
                            {sampleItem.revision_details}
                          </Text>
                          <Text style={{fontSize: hp(1.5), color: '#44bd32'}}>
                            {sampleItem.file_status}
                          </Text>
                        </View>
                      </View>
                    ))}
                  </View>
                )}

                {/* production files */}
                {this.props.data.production_file != null && (
                  <View
                    style={{
                      paddingVertical: hp(2),
                      paddingHorizontal: wp(3),
                      backgroundColor: '#fff',
                      marginBottom: hp(2),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingBottom: hp(2),
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontSize: hp(2),
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        File Produksi
                      </Text>
                    </View>
                    {this.props.data.production_file.map(
                      (production_file, index) => (
                        <View
                          style={{
                            flexDirection: 'row',
                            paddingVertical: hp(1),
                            borderTopWidth: 1,
                            borderTopColor: '#f5f6fa',
                          }}>
                          <View
                            style={{
                              width: wp(10),
                              height: hp(5),
                              backgroundColor: randomColor(),
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderRadius: hp(2.5),
                            }}>
                            <FontAwesome5Icon
                              name={'file-alt'}
                              size={hp(2.5)}
                              color={'#fff'}
                            />
                          </View>

                          <View style={{flex: 1, paddingLeft: wp(3)}}>
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                              }}>
                              <Text
                                style={{
                                  fontSize: hp(2),
                                  fontWeight: 'bold',
                                  color: '#353b48',
                                  // textTransform: 'capitalize',s
                                }}>
                                {substr(production_file.file_name || '', 25)}
                              </Text>

                              <ItemActionButton
                                onDownload={() =>
                                  this.downloadFile(
                                    production_file,
                                    'downloadProductionFileForGraphics',
                                  )
                                }
                                onAcceptRequest={() => null}
                                onSendRevisionRequest={() => null}
                                disableAccept={true}
                                disableRevision={true}
                                productions={true}
                              />
                            </View>
                            <Text style={{fontSize: hp(1.5)}}>
                              {production_file.comments}
                            </Text>
                            <Text style={{fontSize: hp(1.5), color: '#e84118'}}>
                              {production_file.status}
                            </Text>
                          </View>
                        </View>
                      ),
                    )}
                  </View>
                )}

                {this.props.data.my_download != null && (
                  <View
                    style={{
                      paddingVertical: hp(2),
                      paddingHorizontal: wp(3),
                      backgroundColor: '#fff',
                      marginBottom: hp(2),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingBottom: hp(2),
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          fontSize: hp(2),
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        Informasi Unduhan
                      </Text>
                    </View>
                    {this.props.data.my_download.map((my_download, index) => (
                      <View
                        style={{
                          flexDirection: 'row',
                          paddingVertical: hp(1),
                          borderTopWidth: 1,
                          borderTopColor: '#f5f6fa',
                        }}>
                        <View
                          style={{
                            width: wp(10),
                            height: hp(5),
                            backgroundColor: randomColor(),
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: hp(2.5),
                          }}>
                          <FontAwesome5Icon
                            name={'file-download'}
                            size={hp(2.5)}
                            color={'#fff'}
                          />
                        </View>

                        <View style={{flex: 1, paddingLeft: wp(3)}}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <Text
                              style={{
                                fontSize: hp(2),
                                fontWeight: 'bold',
                                color: '#353b48',
                                // textTransform: 'capitalize',s
                              }}>
                              {my_download.file_name}
                            </Text>
                          </View>
                          <Text style={{fontSize: hp(1.5)}}>
                            {my_download.date_time}
                          </Text>
                        </View>
                      </View>
                    ))}
                  </View>
                )}

                <View
                  style={{
                    paddingVertical: hp(2),
                    paddingHorizontal: wp(3),
                    backgroundColor: '#fff',
                    marginBottom: hp(2),
                  }}>
                  {this.props.data.product_type === 'G' && (
                    <View style={{paddingVertical: hp(1)}}>
                      <ButtonNative
                        block
                        danger
                        disabled={this.props.data.requirement_sent}
                        onPress={() => this.goToRequirements()}>
                        <Text> Kirim Persyaratan</Text>
                      </ButtonNative>
                    </View>
                  )}

                  {/* {this.props.data.progress &&
                    setIndexProgress(
                      this.props.data.graphics_product_status_bar || {},
                    ) < 3 && (
                      <View style={{paddingVertical: hp(1)}}>
                        <ButtonNative
                          block
                          danger
                          onPress={() =>
                            this.props.navigation.navigate('CancelForm', {
                              headerName: 'Batalkan Pesanan',
                              order_id: this.props.data.order_product_id,
                              type: 'cancel',
                            })
                          }>
                          <Text>Batalkan Pesanan</Text>
                        </ButtonNative>
                      </View>
                    )} */}

                  {this.props.data.order_status ==
                    'Pembayaran Sudah Diverifikasi' ||
                    this.props.data.order_status ==
                      'Pesanan Anda Sedang Diproses' ||
                    (this.props.data.order_status ==
                      'Menunggu Konfirmasi Pembayaran' && (
                      <View style={{paddingVertical: hp(1)}}>
                        <Button
                          onPress={() =>
                            this.props.navigation.navigate('CancelForm', {
                              headerName: 'Batalkan Pesanan',
                              order_id: this.props.data.order_product_id,
                              type: 'cancel',
                            })
                          }>
                          Batalkan Pesanan
                        </Button>
                      </View>
                    ))}

                  <View style={{paddingVertical: hp(1)}}>
                    <ButtonNative
                      bordered
                      block
                      disabled={!this.props.data.shop_id}
                      onPress={() => this.sendMessageToShop()}
                      danger>
                      <Text style={{color: '#CE3D3E'}}>Kirim Pesan</Text>
                    </ButtonNative>
                  </View>

                  {this.props.data.order_status ===
                    'Transaksi Ini Sudah Selesai' &&
                    this.props.data.isReview === 0 && (
                      <View style={{paddingVertical: hp(1)}}>
                        <ButtonNative
                          bordered
                          block
                          danger
                          disabled={!this.props.data.shop_id}
                          onPress={() =>
                            this.props.navigation.navigate('CancelForm', {
                              headerName: 'Penilaian',
                              order_id: this.props.data.order_product_id,
                              type: 'feedback',
                            })
                          }>
                          <Text style={{color: '#CE3D3E'}}>Penilaian</Text>
                        </ButtonNative>
                      </View>
                    )}
                </View>

                <View>
                  <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.sendMessage}
                    onRequestClose={() => {
                      this.closeMessageModal();
                    }}>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                      }}>
                      <View
                        style={{
                          width: wp('80%'),
                          maxHeight: hp('80%'),
                          backgroundColor: '#fff',
                          padding: 20,
                          zIndex: 5,
                          position: 'relative',
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderRadius: 10,
                        }}>
                        <ScrollView keyboardShouldPersistTaps={'handled'}>
                          <Text
                            style={{
                              color: '#545454',
                              fontSize: hp('3%'),
                              paddingBottom: 0,
                              fontFamily: Font.RobotoRegular,
                              textAlign: 'center',
                            }}>
                            Mengirim pesan
                          </Text>
                          <CardSection>
                            <InputInner
                              value={this.state.subject}
                              onChangeText={(value) =>
                                this.setState({subject: value})
                              }
                              label="Subyek"
                            />
                          </CardSection>
                          <CardSection>
                            <TextareaInner
                              value={this.state.message}
                              onChangeText={(value) =>
                                this.setState({message: value})
                              }
                              label="Pesan"
                            />
                          </CardSection>
                          <View style={styles.btnWrapper}>
                            <ButtonTwo
                              onPress={() => {
                                this.sendMessageContent();
                              }}>
                              Mengirim pesan
                            </ButtonTwo>
                          </View>
                        </ScrollView>
                        <TouchableOpacity
                          onPress={() => this.closeMessageModal()}
                          style={styles.closeBtn2}>
                          <Text style={styles.crossColor}>&#10005;</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </Modal>
                </View>
              </View>
            )}
          </ScrollView>
        </PTRView>
      </Provider>
    );
  }
}
const styles = {
  crossColor: {
    textAlign: 'center',
    color: '#000',
  },
  closeBtn2: {
    position: 'absolute',
    right: 0,
    width: 30,
    height: 30,
    top: 0,
    zIndex: 1002,
    color: '#fff',
    borderRadius: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnTxt: {
    fontSize: hp('1.6%'),
    fontFamily: Font.RobotoRegular,
  },
  bgColor: {
    backgroundColor: '#fff',
    position: 'relative',
    zIndex: 1,
  },
  z_index: {
    zIndex: 2,
  },

  boxStyle: {
    width: '100%',
    padding: 10,
    position: 'relative',
  },
  imgContentWrapper: {
    flexDirection: 'row',
    flex: 1,
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    paddingBottom: 10,
    justifyContent: 'space-between',
    marginBottom: hp('2%'),
  },
  imgWrapper: {
    marginRight: wp('3%'),
  },
  boxImg: {
    width: hp('10%'),
    height: hp('17%'),
  },
  productName: {
    fontSize: hp('2.9%'),
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },
  availableWrapper: {
    flexDirection: 'row',
    paddingTop: hp('0.4%'),
    paddingBottom: hp('0.4%'),
    justifyContent: 'space-between',
  },
  available: {
    color: '#858585',
    fontSize: hp('2%'),
    marginRight: wp('5%'),
    fontFamily: Font.RobotoMedium,
  },
  count: {
    color: '#00b3ff',
  },
  priceHeading: {
    color: '#696969',
    fontSize: hp('2.5%'),
    fontFamily: Font.RobotoMedium,
  },
  totalPriceWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: hp('1.5%'),
    paddingTop: hp('1.5%'),
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  totalItem: {
    fontSize: wp('4.2%'),
    color: '#6f6f6f',
    fontFamily: Font.RobotoRegular,
  },
  priceTxt: {
    color: '#00b3ff',
    // fontWeight:'bold',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoBold,
  },
  boldClass: {
    fontFamily: Font.RobotoBold,
  },
  doYouStyle: {
    padding: 10,
  },
  doYouHeading: {
    color: '#545454',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.5%'),
    marginBottom: hp('0.5%'),
  },
  doYouDescription: {
    color: '#7d7d7d',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
    marginBottom: hp('2%'),
  },
  blueColor: {
    color: '#00b3ff',
  },
  btn_style: {
    paddingLeft: 10,
    paddingRight: 10,
    width: '60%',
    marginTop: hp('2.5%'),
  },
  sendReq: {
    backgroundColor: '#f07f7f',
  },
  circleDivImg: {
    width: wp('60%'),
    height: wp('60%'),
  },
  circleDiv: {
    borderRadius: wp('80%'),
    marginRight: 'auto',
    marginLeft: 'auto',
    marginVertical: hp('3%'),
  },
  statusHeading: {
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    fontSize: hp('2.5%'),
    marginBottom: hp('1%'),
  },
  mt: {
    marginTop: hp('2%'),
  },
  row_: {
    flexDirection: 'row',
    borderColor: '#e7e7e7',
    borderBottomWidth: 1,
    paddingVertical: hp('2%'),
  },
  row_2: {
    flexDirection: 'row',
    paddingVertical: hp('1%'),
    position: 'relative',
  },

  dateDiv: {
    width: '25%',
    // paddingLeft:10
  },
  imgDiv: {
    width: '25%',
    paddingRight: 10,
  },
  lftImg: {
    width: '100%',
    height: hp('10%'),
  },
  date: {
    color: '#00b3ff',
    fontFamily: Font.RobotoBold,
    fontSize: hp('3.3%'),
  },
  monthYear: {
    color: '#7d7d7d',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
  },
  contentDiv: {
    width: '73%',
  },
  rgtMenuIcon: {
    width: wp('2%'),
    height: hp('3%'),
  },
  descriptionOne: {
    color: '#7d7d7d',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.5%'),
    marginBottom: hp('1%'),
  },
  descriptionTwo: {
    color: '#4a4a4a',
    fontFamily: Font.RobotoItalc,
    fontSize: hp('2%'),
  },
  wrapperStyle: {
    borderTopWidth: 1,
    borderTopColor: '#e7e7e7',
    padding: 10,
    position: 'relative',
    zIndex: 1,
  },
  wrapperStyle2: {
    padding: 10,
  },
  sampleHeading: {
    color: '#545454',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.3%'),
  },
  sampleTxt: {
    color: '#858585',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
  },
  count2: {
    color: '#c90305',
  },
  dwnBtn: {
    backgroundColor: '#00b3ff',
    alignSelf: 'flex-start',
    borderRadius: 50,
    paddingHorizontal: hp('3%'),
    paddingVertical: hp('1.1%'),
    marginTop: hp('1%'),
  },
  dwnTxt: {
    color: '#fff',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
  },
  sampleTxt2: {
    color: '#7d7d7d',
    fontFamily: Font.RobotoItalc,
    padding: 10,
    borderTopColor: '#e7e7e7',
    borderTopWidth: 1,
    fontSize: hp('2%'),
    position: 'relative',
    zIndex: 1,
  },
  comple_te: {
    borderRadius: 5,
    backgroundColor: '#00b3ff',
    alignSelf: 'flex-start',
    paddingHorizontal: hp('2.2%'),
    paddingVertical: hp('0.7%'),
    marginTop: hp('1%'),
    marginBottom: hp('1%'),
  },
  btnDiv: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: hp('2%'),
  },
  btnFont: {
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
  },
  btnBottomTxt: {
    color: '#7d7d7d',
    fontFamily: Font.RobotoItalc,
    padding: 10,
    borderBottomColor: '#e7e7e7',
    borderBottomWidth: 1,
    fontSize: hp('2%'),
    paddingTop: 0,
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.orderDetails.loading,
    data: state.orderDetails.data,
    payment_history: state.orderDetails.payment_history,
  };
};

export default connect(mapStateToProps, {getOrderDetails})(ViewOrderDigital);
