import React, {Component} from 'react';
import {Alert, Text, ScrollView} from 'react-native';
import {
  ButtonTwo,
  Card,
  CardSection,
  InputInner,
  Spinner,
  TextareaInner,
} from './common';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import axios from 'axios';

export default class ContactSupport extends Component {
  static navigationOptions = ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
    headerTitle: (
      <Text style={{fontSize: hp('2.5%'), color: '#fff'}}>Hubungi Kami Us</Text>
    ),
  });

  state = {
    _token: this.props.route.params._token,
    name: '',
    email: '',
    phone: '',
    message: '',
    loading: false,
  };

  validateEmail = (email) => {
    let val = new RegExp(
      '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@' +
        '[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$',
    );
    if (val.test(email)) {
      return true;
    } else {
      return false;
    }
  };

  contactUs = async () => {
    if (
      String(this.state.name).trim() == '' ||
      String(this.state.phone).trim() == '' ||
      String(this.state.message).trim() == ''
    ) {
      Alert.alert('Alert', 'Harap lengkapi semua data', [
        {text: 'OK', onPress: () => console.log('ok')},
      ]);
    } else if (!this.validateEmail(this.state.email)) {
      Alert.alert('Alert', 'Masukan email yang valid', [
        {text: 'OK', onPress: () => console.log('ok')},
      ]);
    } else {
      this.setState({
        loading: true,
      });
      let details = {
        _token: this.state._token,
        name: this.state.name,
        email: this.state.email,
        phone: this.state.phone,
        message: this.state.message,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post('https://syoobe.co.id/api/contactUs', formBody)
        .then((response) => {
          console.log(response);
          if (response.data.status == 1) {
            this.setState({
              loading: false,
            });

            Alert.alert(
              'Informasi',
              'Terima kasih telah menghubungi kami, pesan anda akan segera di proses',
              [{text: 'OK', onPress: () => this.props.navigation.pop()}],
            );
          } else {
            Alert.alert('Informasi', 'Data gagal dikirim', [
              {text: 'OK', onPress: () => this.props.navigation.pop()},
            ]);
            this.setState({
              loading: false,
            });
          }
        });
    }
  };

  render() {
    if (this.state.loading) {
      return <Spinner color="red" size="large" />;
    }
    return (
      <ScrollView style={styles.scrollClass}>
        <Card>
          <CardSection>
            <InputInner
              placeholder=""
              value={this.state.name}
              onChangeText={(val) =>
                this.setState({
                  name: val,
                })
              }
              label="Nama"
            />
          </CardSection>
          <CardSection>
            <InputInner
              placeholder=""
              value={this.state.email}
              onChangeText={(val) =>
                this.setState({
                  email: val,
                })
              }
              label="Email"
            />
          </CardSection>
          <CardSection>
            <InputInner
              placeholder=""
              keyboardType="numeric"
              value={this.state.phone}
              onChangeText={(val) =>
                this.setState({
                  phone: val,
                })
              }
              label="Nomor Handphone"
            />
          </CardSection>
          <CardSection>
            <TextareaInner
              value={this.state.message}
              onChangeText={(val) =>
                this.setState({
                  message: val,
                })
              }
              label="Pesan"
            />
          </CardSection>
          <ButtonTwo onPress={() => this.contactUs()}>Submit </ButtonTwo>
        </Card>
      </ScrollView>
    );
  }
}

const styles = {
  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
  },
};
