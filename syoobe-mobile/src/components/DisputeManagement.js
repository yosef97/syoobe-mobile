import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import {DisputeManagementTab} from './routes/DisputeManagementTab';

class DisputeManagement extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <DisputeManagementTab {...this.props} />
      </View>
    );
  }
}

export default connect()(DisputeManagement);
