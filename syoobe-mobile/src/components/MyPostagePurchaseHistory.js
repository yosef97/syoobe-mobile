import React, {Component} from 'react';
import {
  TouchableOpacity,
  Image,
  StyleSheet,
  AsyncStorage,
  Text,
  Button,
  StatusBar,
  View,
  ScrollView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ChildrenProps from './HOC/ChildrenProps';
import {Font} from './Font';
import axios from 'axios';
import PostagePurchaseTransaction from './PostagePurchaseTransaction';
// import RNHTMLtoPDF from "react-native-html-to-pdf";
import {Spinner} from 'native-base';
import {BASE_URL} from '../services/baseUrl';

export default class MyPostagePurchaseHistory extends Component {
  static navigationOptions = {
    drawerLabel: 'Riwayat Pembelian',
    drawerIcon: () => (
      <Image
        source={require('../images/purchaseHistory.png')}
        style={{width: wp('4.5%'), height: hp('2.5%')}}
      />
    ),
  };

  state = {
    _token: null,
    postage_details: [],
    viewOpen: false,
    activeId: null,
    total_pages: 1,
    current_page: 1,
    loading: false,
    pagesShownArray: [],
    first_page: null,
    last_page: null,
  };

  async createPDF() {
    const options = {
      html: '<h1>Postage_History</h1>',
      directory: 'Documents',
      fileName: 'Postage_History',
      height: 800,
      width: 1056,
      padding: 24,
    };
    // RNHTMLtoPDF.convert(options).then(filePath => {
    //   alert("File disimpan dalam dokumen");
    // });
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = () => {
    this.retrieveToken().then((_token) => {
      this.fetchData();
    });
  };

  setShownArray = async (first, last) => {
    const {total_pages} = this.state;
    if (first > 0 && last <= total_pages) {
      let array = [];
      for (let i = first; i <= last; i++) {
        array.push(i);
      }
      await this.setState({
        pagesShownArray: [...array],
        first_page: first,
        last_page: last,
      });
    }
  };

  goRight = () => {
    const {pagesShownArray} = this.state;
    this.setShownArray(
      pagesShownArray[0] + 1,
      pagesShownArray[pagesShownArray.length - 1] + 1,
    );
  };

  goLeft = () => {
    const {pagesShownArray} = this.state;
    this.setShownArray(
      pagesShownArray[0] - 1,
      pagesShownArray[pagesShownArray.length - 1] - 1,
    );
  };

  callSetShownArray = () => {
    const {total_pages, pagesShownArray} = this.state;
    if (pagesShownArray.length > 0) {
      this.setShownArray(
        pagesShownArray[0],
        pagesShownArray[pagesShownArray.length - 1],
      );
    } else {
      this.setShownArray(1, total_pages < 4 ? total_pages : 4);
    }
  };

  // shouldComponentUpdate = (nextProps, nextState) => {
  //     if(nextState.postage_details !== this.state.postage_details){
  //         console.log('in')
  //         return true;
  //     }
  //     else {
  //         console.log('else')
  //         return false;
  //     }
  // }

  fetchData = () => {
    this.setState({
      loading: true,
    });
    details = {
      _token: this.state._token,
      page: this.state.current_page,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post(BASE_URL + '/myPostagePurchaseHistory', formBody)
      .then((response) => {
        console.log('data', response);
        this.setState({
          loading: false,
        });
        if (response.data.status == 1) {
          let postage_array = [...response.data.postage_details];
          this.setState({
            postage_details: [...postage_array],
            total_pages: response.data.total_pages,
          });
          this.callSetShownArray();
        } else {
          this.setState({
            postage_details: [],
          });
        }
      });
  };

  toggleView = (id) => {
    this.setState({
      viewOpen: !this.state.viewOpen,
      activeId: id,
    });
  };

  selectPage = async (val) => {
    await this.setState({
      current_page: val,
    });
    this.fetchData();
  };

  render() {
    const {
      last_page,
      first_page,
      pagesShownArray,
      imageStyle,
      current_page,
      postage_details,
      viewOpen,
      activeId,
      total_pages,
      loading,
    } = this.state;
    const {
      selectedIndex,
      centerStyle,
      postageTxt,
      topDiv,
      paginationStyles,
      textStyle,
      paginationViewNumber,
    } = styles;
    const combineStyles = StyleSheet.flatten([
      paginationViewNumber,
      selectedIndex,
    ]);
    return (
      <ChildrenProps>
        <ScrollView style={styles.scrollView}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          {loading ? (
            <View style={centerStyle}>
              <Spinner color={'red'} />
            </View>
          ) : (
            <ChildrenProps>
              <View style={topDiv}>
                <Text style={postageTxt}>Riwayat Biaya Pengiriman</Text>
                {postage_details.length > 0 && (
                  <Button
                    color={'#C90205'}
                    title={'Print'}
                    onPress={() => this.createPDF()}
                  />
                )}
              </View>
              <View style={styles.touchStyle}>
                <Text style={styles.lftTxt}>Jumlah</Text>
                <Text style={styles.lftTxt}>Tanggal</Text>
                <Text style={styles.lftTxt}>Detail</Text>
              </View>
              {postage_details.map((detail, index) => (
                <PostagePurchaseTransaction
                  activeId={activeId}
                  viewOpen={viewOpen}
                  clicked={this.toggleView}
                  postageData={{...detail}}
                  key={index}
                />
              ))}
              {postage_details.length > 0 && (
                <View style={paginationStyles}>
                  {first_page > 1 && (
                    <TouchableOpacity onPress={this.goLeft}>
                      <Image
                        style={imageStyle}
                        resizeMode={'contain'}
                        source={require('../images/left-arrow.png')}
                      />
                    </TouchableOpacity>
                  )}
                  {pagesShownArray.map((val, index) => (
                    <View
                      key={index}
                      style={
                        current_page == val
                          ? paginationViewNumber
                          : combineStyles
                      }>
                      <Text
                        onPress={() => this.selectPage(val)}
                        style={textStyle}>
                        {val}
                      </Text>
                    </View>
                  ))}
                  {last_page < total_pages && (
                    <TouchableOpacity onPress={this.goRight}>
                      <Image
                        style={imageStyle}
                        resizeMode={'contain'}
                        source={require('../images/right-arrow.png')}
                      />
                    </TouchableOpacity>
                  )}
                </View>
              )}
            </ChildrenProps>
          )}
        </ScrollView>
      </ChildrenProps>
    );
  }
}

const styles = StyleSheet.create({
  touchStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    borderWidth: 1,
    borderColor: '#e8edef',
    elevation: 0.5,
    height: 50,
    alignItems: 'center',
    padding: 10,
    marginVertical: 5,
  },
  lftTxt: {
    textAlign: 'center',
    color: '#4e4c4c',
    fontSize: hp('1.8%'),
  },
  imageView: {
    marginHorizontal: 50,
    borderColor: '#000',
    borderWidth: 1,
    height: hp('5%'),
    width: wp('10%'),
  },
  imageStyle: {
    // height: hp('1%'),
    width: wp('2%'),
  },
  selectedIndex: {
    backgroundColor: '#f6f8f9',
  },
  centerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: hp('85%'),
  },
  paginationViewNumber: {
    backgroundColor: '#D3D3D3',
    // 808080
    borderRadius: 30,
    height: 30,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paginationStyles: {
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '70%',
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingBottom: 20,
  },
  scrollView: {
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  topDiv: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 35,
  },
  postageTxt: {
    fontSize: hp('2.5%'),
    fontFamily: Font.RobotoMedium,
    paddingRight: 10,
    flex: 1,
    color: '#545454',
  },
  buttonStyle: {
    alignSelf: 'stretch',
    backgroundColor: '#c90305',
    borderRadius: 50,
    // paddingHorizontal: hp('1.8%'),
    paddingVertical: hp('1.8%'),
    // marginBottom:10,
    width: '25%',
    // height: '%'
  },
  textStyle: {
    // alignSelf: 'center',
    // color: '#fff',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
  },
  // borderMarginBottom:{
  //     borderBottom:0,
  //     marginBottom:0,
  // },
  txtColor: {
    color: '#ed0030',
  },
});

// [...Array(total_pages)].map((val, index) =>
//     <View key={index} style={current_page == (index + 1) ?paginationViewNumber : combineStyles}>
//         <Text onPress={() => this.selectPage(index)} style={textStyle}>
//             {index + 1}
//         </Text>
//     </View>
// )
