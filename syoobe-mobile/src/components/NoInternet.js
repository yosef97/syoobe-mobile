import React from 'react';
import {View, StatusBar, Text} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const NoInternet = (props) => (
  <>
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <StatusBar backgroundColor="#C90205" barStyle="light-content" />
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#C90205',
          padding: 10,
          borderRadius: 5,
        }}>
        <Text
          style={{
            fontSize: hp('2%'),
            color: '#fff',
          }}>
          No Internet.
        </Text>
        <Text
          style={{
            fontSize: hp('2%'),
            color: '#fff',
          }}>
          Please check your Internet Connection.
        </Text>
      </View>
    </View>
  </>
);

export default NoInternet;
