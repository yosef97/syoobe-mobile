import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {addToCart, viewCart} from '../actions';
import {Alert, AsyncStorage} from 'react-native';

class AddToCart extends Component {
  constructor(props) {
    super(props);
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  requestAddToCart = async () => {
    await this.retrieveToken()
      .then((_token) => {
        const quantity = 1;
        const product_id = this.props.details.product_id;
        this.props.addToCart({_token, product_id, quantity});
        setTimeout(() => this.props.viewCart({_token}), 2000);
      })
      .catch((error) => {
        console.log('Promise is rejected with error: ' + error);
      });
  };

  buyNow = async () => {
    await this.retrieveToken()
      .then((_token) => {
        this.props.viewCart({_token});
        if (this.props.cart_count > 0) {
          Alert.alert(
            'Confirmation Message',
            'Your cart is not empty.' +
              ' Please clear your cart content or complete the order before proceeding to buy any digital product',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'Ok Take me to Cart',
                onPress: () => this.props.navigation.navigate('MyCart'),
              },
            ],
          );
        } else {
          const quantity = 1;
          const product_id = this.props.details.product_id;
          this.props.addToCart({_token, product_id, quantity});
          setTimeout(() => {
            if (!this.props.cart_error) {
              this.props.navigation.navigate('MyCart', {
                product_description: 'digital',
              });
            }
          }, 2000);
        }
      })
      .catch((error) => {
        console.log('Promise is rejected with error: ' + error);
      });
  };

  viewProduct = () => {
    this.props.navigation.push('ProductDetail', {
      product_id: this.props.details.product_id,
      product_requires_shipping: this.props.details.prod_requires_shipping,
      product_name: this.props.details.product_name,
    });
  };

  render() {
    if (this.props.children.props.children == 'Buy Now') {
      return (
        <TouchableOpacity
          onPress={() => this.buyNow()}
          style={styles.buttonStyle}>
          {this.props.children}
        </TouchableOpacity>
      );
    } else if (this.props.children.props.children == 'Add to Cart') {
      return (
        <TouchableOpacity
          onPress={() => this.requestAddToCart()}
          style={styles.buttonStyle}>
          {this.props.children}
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => this.viewProduct()}
          style={styles.buttonStyle}>
          {this.props.children}
        </TouchableOpacity>
      );
    }
  }
}

const styles = {
  buttonStyle: {
    backgroundColor: 'transparent',
    fontSize: 20,
    borderColor: '#c7c7c7',
    borderWidth: 1,
    borderRadius: 50,
  },
};

const mapStateToProps = (state) => {
  return {
    message: state.cart.message,
    loading: state.cart.loading,
    cart_count: state.cart.cart_count,
    cart_error: state.cart.cart_error,
  };
};

export default connect(mapStateToProps, {addToCart, viewCart})(AddToCart);
