import React, {Component} from 'react';
import {
  AsyncStorage,
  FlatList,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Feather from 'react-native-vector-icons/Feather';
import {connect} from 'react-redux';
import {getRewardPoint} from '../actions';
import {formatRupiah} from '../helpers/helper';
import {Spinner} from './common';

class MyReward extends Component {
  constructor(props) {
    super(props);
    this.state = {
      points: [],
      balance: 0,
      _token: null,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = () => {
    this.retrieveToken()
      .then((_token) => {
        this.getRewardPoint(_token);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getRewardPoint = (token) => {
    let details = {
      _token: token,
    };
    this.props
      .getRewardPoint(details)
      .then((res) => {
        this.setState({
          points: res.data,
          balance: res.point,
        });
      })
      .catch((err) => console.log('err', err));
  };

  render() {
    const {_token} = this.state;
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color="red" size="large" />
        </View>
      );
    }
    console.log(this.props.transactions);

    return (
      <View style={{flex: 1}}>
        <View
          style={{
            height: hp(15),
            width: '100%',
            backgroundColor: 'red',
            // borderBottomLeftRadius: hp(5),
            // borderBottomRightRadius: hp(5),
          }}>
          <View style={{padding: wp(3)}}>
            <Text style={{color: '#fff', fontSize: 16, paddingTop: hp(2)}}>
              Point Hadiah
            </Text>
            <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
              <Text style={{color: '#fff', fontSize: hp(2), paddingTop: hp(1)}}>
                Rp
              </Text>
              <Text style={{color: '#fff', fontSize: hp(5)}}>
                {' '}
                {formatRupiah(parseInt(this.state.balance))}
              </Text>
            </View>
          </View>
        </View>
        <View style={{flex: 1, paddingHorizontal: wp(3), marginTop: hp(5)}}>
          <Text
            style={{fontWeight: 'bold', fontSize: 14, paddingBottom: hp(2)}}>
            Riwayat
          </Text>
          <FlatList
            data={this.state.points}
            showsVerticalScrollIndicator={false}
            scrollEnabled
            renderItem={({item, index}) => (
              <View
                key={index}
                style={{
                  backgroundColor: '#fff',
                  padding: wp(3),
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  borderRadius: 5,
                  marginBottom: hp(2),
                }}>
                <View style={{width: wp(60)}}>
                  <Text
                    style={{fontWeight: 'bold', textTransform: 'capitalize'}}>
                    {item.description}
                  </Text>
                  <Text>{item.date}</Text>
                </View>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 16,
                    color: 'green',
                  }}>
                  Rp. {formatRupiah(parseInt(item.point))}
                </Text>
              </View>
            )}
          />
        </View>
      </View>
    );
  }
}

export default connect(null, {getRewardPoint})(MyReward);
