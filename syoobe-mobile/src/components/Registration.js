import React, {Component} from 'react';
import {
  Animated,
  Image,
  ImageBackground,
  Keyboard,
  KeyboardAvoidingView,
  ScrollView,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import ActionSheet from 'react-native-action-sheet';
import * as RNLocalize from 'react-native-localize';
import {Checkbox} from 'react-native-paper';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {
  nameChanged,
  regEmailChanged,
  registerUser,
  regPasswordChanged,
  usernameChanged,
} from '../actions';
import {setI18nConfig} from '../translations/translation';
import {
  Button,
  CardSection,
  CardSectionLogin,
  Footer,
  Input,
  Spinner,
} from './common';
import {Font} from './Font';

class Registration extends Component {
  state = {
    shift: new Animated.Value(0),
    isFooterVisible: true,
    user_phone: null,
    user_isd_phone_code: '+62',
    isd_array: [],
    actionSheetArray: [],
  };

  showActonSheet = () => {
    ActionSheet.showActionSheetWithOptions(
      {
        options: this.state.actionSheetArray,
        tintColor: 'blue',
        title: 'Pilih kode negara',
      },
      (buttonIndex) => {
        this.setState({
          user_isd_phone_code: this.state.isd_array[buttonIndex].code,
        });
        console.log('ss', this.state.user_isd_phone_code);
      },
    );
  };

  getCountries = async () => {
    fetch('https://syoobe.co.id/api/getCountryList', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((response) => this.storeCountries(response.data))
      .catch((error) => console.log('er', error));
  };

  storeCountries = (data) => {
    this.setState({
      isd_array: data,
    });

    const countryCodeLabels = [];
    data.forEach((value) => {
      countryCodeLabels.push(value.name + ' (' + value.code + ')');
    });
    this.setState({
      actionSheetArray: countryCodeLabels,
    });
  };

  componentDidMount = () => {
    this.getCountries();

    this.keyboardDidShowSub = Keyboard.addListener(
      'keyboardDidShow',
      this.handleKeyboardDidShow,
    );
    this.keyboardDidHideSub = Keyboard.addListener(
      'keyboardDidHide',
      this.handleKeyboardDidHide,
    );
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  };

  onNameChange = (text) => {
    this.props.nameChanged(text);
  };

  onUsernameChange = (text) => {
    this.props.usernameChanged(text);
  };

  onEmailChange = (text) => {
    var emailTrim = text.trim();
    this.props.regEmailChanged(emailTrim);
  };

  onPasswordChange = (text) => {
    this.props.regPasswordChanged(text);
  };

  onRegister = () => {
    let paternEmail = /\S+@\S+\.\S+/;

    // let paternEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let paternUsername = /^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/;
    const {navigation, name, username, password} = this.props;
    const email = this.props.email.toLowerCase();
    // const email = this.props.email.toLowerCase();

    console.log('ini email', email);
    const {user_phone, user_isd_phone_code} = this.state;
    if (name === '' || name === undefined) {
      Alert.alert('Mohon masukkan nama anda');
    } else if (!username.match(paternUsername)) {
      Alert.alert('Username yang kamu masukkan tidak valid');
    } else if (username === '' || username === undefined) {
      Alert.alert('Username tidak boleh kosong');
    } else if (!email.match(paternEmail)) {
      Alert.alert('Email yang kamu masukkan tidak valid');
    } else if (password === '' || password === undefined) {
      Alert.alert('Password tidak boleh kosong');
    } else if (user_phone === null) {
      Alert.alert('Nomor telepon tidak boleh kosong');
    } else {
      this.props.registerUser(navigation, {
        name,
        username,
        email,
        password,
        user_phone,
        user_isd_phone_code,
      });
    }
  };

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };

  handleKeyboardDidShow = (event) => {
    this.setState({
      isFooterVisible: false,
    });
  };

  handleKeyboardDidHide = () => {
    this.setState({
      isFooterVisible: true,
    });
  };

  renderError() {
    if (this.props.registerError) {
      return (
        <Text style={styles.errorTextStyle}>{this.props.registerError}</Text>
      );
    }
  }

  renderFooter = () => {
    if (this.state.isFooterVisible) {
      return (
        <Footer>
          <TouchableOpacity
            style={styles.footer_text2}
            onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={styles.footer_text}>Sudah terdaftar? Masuk</Text>
          </TouchableOpacity>
        </Footer>
      );
    }
  };

  onMessage = (event) => {
    if (event && event.nativeEvent.data) {
      if (['cancel', 'error', 'expired'].includes(event.nativeEvent.data)) {
        this.captchaForm.hide();
        return;
      } else {
        console.log('Verified code from Google', event.nativeEvent.data);
        setTimeout(() => {
          this.captchaForm.hide();
          this.onRegister();
        }, 1500);
      }
    }
  };

  verifyCaptcha = () => {
    this.captchaForm.show();
  };

  render() {
    // console.log('ini props', JSON.stringify(this.props));
    // console.log('ini state', JSON.stringify(this.state));

    // let countries;
    // if (this.state.isd_array) {
    //   countries = this.state.isd_array.map(value => {
    //     return (
    //       <Picker.Item
    //         key={value.code}
    //         value={value.code}
    //         label={value.name + " (" + value.code + ")"}
    //       />
    //     );
    //   });
    // }
    // console.log("isd array", this.state.isd_array);
    // console.log("countries", countries);

    if (this.props.loading) {
      return (
        <View>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <ImageBackground
            style={styles.bgImg}
            source={require('../images/loginBg.png')}>
            <Spinner size="large" />
          </ImageBackground>
        </View>
      );
    }
    return (
      <View>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <ImageBackground
          style={[styles.bgImg, {paddingHorizontal: wp(3)}]}
          source={require('../images/loginBg.png')}>
          <ScrollView keyboardShouldPersistTaps={'handled'}>
            <KeyboardAvoidingView behavior={'height'} enabled>
              <View
                style={{
                  flex: 1,
                  paddingHorizontal: wp(2),
                  backgroundColor: '#fff',
                  marginTop: hp(7),
                  borderRadius: 20,
                  paddingBottom: hp(3),
                }}>
                <Text style={styles.heading_txt}>{'Daftar'}</Text>
                {/* <Text style={styles.heading_txt}>{translate('registration')}</Text> */}
                <CardSectionLogin
                  style={styles.iconClass}
                  source={require('../images/logo.png')}>
                  <Image
                    style={styles.userIcon}
                    source={require('../images/userIcon.png')}
                  />
                  <Input
                    value={this.props.name}
                    placeholder="Nama"
                    label="Name"
                    onChangeText={this.onNameChange.bind(this)}
                  />
                </CardSectionLogin>
                <CardSectionLogin>
                  <Image
                    style={styles.userIcon}
                    source={require('../images/userIcon.png')}
                  />
                  <Input
                    value={this.props.username}
                    placeholder="Nama Pengguna"
                    label="Nama Pengguna"
                    onChangeText={this.onUsernameChange.bind(this)}
                  />
                </CardSectionLogin>
                <CardSectionLogin>
                  {/* < */}
                  <TouchableOpacity onPress={this.showActonSheet}>
                    <Text
                      style={{
                        fontFamily: Font.RobotoRegular,
                        color: '#000',
                        paddingHorizontal: wp(3),
                      }}>
                      {this.state.user_isd_phone_code}
                    </Text>
                  </TouchableOpacity>
                  <View style={{width: wp(80), backgroundColor: 'transparent'}}>
                    <Input
                      keyboardType="numeric"
                      placeholder="Nomor telepon"
                      value={this.state.user_phone}
                      label="Nomor telepon"
                      style={{paddingLeft: 0}}
                      onChangeText={(val) =>
                        this.setState({
                          user_phone: val,
                        })
                      }
                    />
                  </View>
                </CardSectionLogin>
                <CardSectionLogin>
                  <Image
                    style={styles.emailIcon}
                    source={require('../images/msg_icon.png')}
                  />
                  <Input
                    placeholder="Email"
                    value={this.props.email}
                    label="Email"
                    onChangeText={this.onEmailChange.bind(this)}
                  />
                </CardSectionLogin>
                <CardSectionLogin>
                  <Image
                    style={styles.passwordIcon}
                    source={require('../images/passwordIcon.png')}
                  />
                  <Input
                    value={this.props.password}
                    placeholder="********"
                    secureTextEntry
                    label="Password"
                    onChangeText={this.onPasswordChange.bind(this)}
                  />
                </CardSectionLogin>
                {this.renderError()}
                <View
                  style={[
                    styles.check_box,
                    {paddingHorizontal: wp(2), paddingBottom: hp(2)},
                  ]}>
                  <Checkbox status={'checked'} color={'red'} />
                  <Text style={[styles.checkbox_txt, {color: 'red'}]}>
                    Dengan Mengklik Mendaftar, Anda menyetujui{' '}
                    <Text style={styles.terms}>Syarat dan ketentuan </Text>
                    kami
                  </Text>
                </View>
                <Button
                  // onPress={this.onRegister.bind(this)}
                  onPress={this.onRegister}>
                  Daftar
                </Button>
                <CardSection />
              </View>
            </KeyboardAvoidingView>
          </ScrollView>
          {this.renderFooter()}
        </ImageBackground>
        {/* <ConfirmGoogleCaptcha
          ref={(_ref) => (this.captchaForm = _ref)}
          siteKey={"6LcxOssUAAAAAESU-qeAj12fkswavGmH1cLqj9SH"}
          baseUrl={"https://syoobe.co.id"}
          languageCode="en"
          onMessage={this.onMessage}
        /> */}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    name: state.reg.name,
    email: state.reg.email,
    username: state.reg.username,
    password: state.reg.password,
    registerError: state.reg.registerError,
    loading: state.reg.loading,
  };
};

const styles = {
  containerStyle: {
    flex: 1,
    height: 100,
    width: '100%',
    marginTop: 'auto',
    borderWidth: 1,
  },
  pickerClass: {
    height: hp('8%'),
  },
  dropdownClass: {
    justifyContent: 'center',
    // alignItems: "center",
    lineHeight: 16,
    height: hp('6%'),
    marginBottom: hp('2%'),
    paddingLeft: 15,
    backgroundColor: '#fff',
    borderRadius: 50,
    width: '100%',
  },
  scrollClass: {
    height: '100%',
  },
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'yellow',
  },
  bgImg: {
    height: '100%',
  },
  iconClass: {
    width: 20,
    height: 20,
  },
  userIcon: {
    width: wp('3.5%'),
    height: hp('2.2%'),
    marginLeft: 20,
  },
  passwordIcon: {
    height: hp('2.4%'),
    width: wp('3.3%'),
    // height:27,
    marginLeft: 20,
  },
  msgIcon: {
    height: hp('2.4%'),
    width: wp('3.3%'),
    marginLeft: 20,
  },
  emailIcon: {
    height: hp('1.5%'),
    width: wp('3.6%'),
    marginLeft: 20,
  },
  face_book: {
    width: 50,
    height: 50,
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 50,
    marginTop: 20,
    marginRight: 10,
    backgroundSize: 20,
  },
  google_plus: {
    width: 50,
    height: 50,
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 50,
    marginTop: 20,
  },
  footer_text: {
    width: '100%',
    color: '#ffffff',
    textAlign: 'center',
    fontFamily: Font.RobotoRegular,
  },
  heading_txt: {
    color: '#000',
    fontSize: hp('3%'),
    textAlign: 'center',
    // fontWeight:'bold',
    marginBottom: 30,
    paddingTop: hp(2),
    fontFamily: Font.RobotoMedium,
  },
  check_box: {
    borderWidth: 1,
    borderColor: '#000',
    width: 20,
    height: 20,
  },
  checkbox_Img: {
    width: wp('5%'),
    height: hp('2%'),
    backgroundColor: 'red',
  },
  checkbox_txt: {
    color: 'red',
    fontSize: hp('1.6%'),
    marginLeft: 5,
    fontFamily: Font.RobotoRegular,
  },
  terms: {
    color: 'red',
    fontSize: hp('1.8%'),
    marginLeft: 5,
    fontFamily: Font.RobotoBold,
  },
  check_box: {
    width: '100%',
    flexDirection: 'row',
    marginTop: hp('2%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer_text2: {
    width: '100%',
    textAlign: 'center',
  },
};

export default connect(mapStateToProps, {
  nameChanged,
  regPasswordChanged,
  regEmailChanged,
  registerUser,
  usernameChanged,
})(Registration);
