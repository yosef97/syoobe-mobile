import React, {Component} from 'react';
import {
  TouchableWithoutFeedback,
  FlatList,
  Animated,
  ScrollView,
  Keyboard,
  AsyncStorage,
  StatusBar,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
  Container,
  Content,
  Text,
  Button,
  Segment,
  Picker,
  DatePicker,
} from 'native-base';
import {ButtonSkip, ButtonTwo, Spinner} from './common';
import {Font} from './Font';
import OrderHistoryComponent from './OrderHistoryComponent';
import {connect} from 'react-redux';
import {
  getOrderDetails,
  emptyDownloadHistory,
  getBuyerDownloads,
  emptyOrderHistory,
  getOrderHistoryStatusList,
  getBuyerOrders,
} from '../actions';
import axios from 'axios';
import Spinner2 from 'react-native-loading-spinner-overlay';
import FlashMessage from 'react-native-flash-message';
import {showToast} from '../helpers/toastMessage';
import {formatRupiah} from '../helpers/helper';

class OrderHistory extends Component {
  static navigationOptions = {
    drawerLabel: 'Pesanan',
    drawerIcon: () => (
      <Image
        source={require('../images/myorder-icon.png')}
        style={{width: wp('3.7%'), height: hp('2.4%')}}
      />
    ),
  };
  constructor(props) {
    super(props);
    this.state = {
      seg: 1,
      fromDate: null,
      toDate: null,
      keyword: '',
      minprice: '',
      maxprice: '',
      PickerSelectedStatus: 'Select Status',
      PickerSelectedStatusId: 0,
      //   orderPageNo: 1,
      searchType: 'none',
      downloadFromDate: null,
      downloadToDate: null,
      donwloadPageNo: 1,
      productName: '',
      downloadSearchType: 'none',
      tokenFromState: null,
      showDropDown: false,
      selectedOrder: null,
    };
    this.setFromDate = this.setFromDate.bind(this);
    this.setToDate = this.setToDate.bind(this);
    this.setDownloadFromDate = this.setDownloadFromDate.bind(this);
    this.setDownloadToDate = this.setDownloadToDate.bind(this);
  }

  state = {
    loaderForDownloadDetails: false,
  };

  setFromDate(newDate) {
    this.setState({fromDate: newDate.toLocaleDateString()});
  }

  setToDate(newDate) {
    this.setState({toDate: newDate.toLocaleDateString()});
  }

  setDownloadFromDate(newDate) {
    this.setState({downloadFromDate: newDate.toLocaleDateString()});
  }

  setDownloadToDate(newDate) {
    this.setState({downloadToDate: newDate.toLocaleDateString()});
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  clearAll = () => {
    if (
      this.state.toDate == null &&
      this.state.fromDate == null &&
      this.state.keyword == '' &&
      this.state.maxprice == '' &&
      this.state.minprice == '' &&
      this.state.PickerSelectedStatusId == 0
    ) {
      showToast({
        message: 'Tidak ada yang jelas!',
      });
    } else {
      this.setState({
        // orderPageNo: 1,
        searchType: 'clear',
      });
      Keyboard.dismiss();
      this.setState({
        fromDate: null,
        toDate: null,
        keyword: '',
        maxprice: '',
        minprice: '',
        PickerSelectedStatus: 'Pilih Status',
        PickerSelectedStatusId: 0,
      });
      this.retrieveToken().then((_token) => {
        var details = {
          _token: _token,

          // 'page': this.state.orderPageNo
        };
        this.props.getBuyerOrders(details, this.state.searchType);
        // this.setState({
        //     orderPageNo: this.state.orderPageNo+1
        // })
      });
    }
  };

  clearInDownloads = () => {
    if (
      this.state.downloadFromDate == null &&
      this.state.downloadToDate == null &&
      this.state.productName == ''
    ) {
      showToast({
        message: 'Tidak ada yang jelas!',
      });
    } else {
      this.setState({
        donwloadPageNo: 1,
        downloadSearchType: 'clear',
      });
      Keyboard.dismiss();
      this.setState({
        downloadFromDate: null,
        downloadToDate: null,
        productName: '',
      });
      this.retrieveToken().then((_token) => {
        var details = {
          _token: _token,
          page: this.state.donwloadPageNo,
        };
        this.props.getBuyerDownloads(details, this.state.downloadSearchType);
        this.setState({
          donwloadPageNo: this.state.donwloadPageNo + 1,
        });
      });
    }
  };

  search = () => {
    if (
      this.state.toDate == null &&
      this.state.fromDate == null &&
      this.state.keyword == '' &&
      this.state.maxprice == '' &&
      this.state.minprice == '' &&
      this.state.PickerSelectedStatusId == 0
    ) {
      showToast({
        message: 'Tidak ada yang bisa dicari!',
      });
    } else {
      // this.setState({
      //     orderPageNo: 1
      // })
      Keyboard.dismiss();
      this.retrieveToken().then((_token) => {
        var details = {
          _token: _token,
          // 'page': this.state.orderPageNo
        };
        if (this.state.fromDate != null) {
          var add = {date_from: this.state.fromDate};
          details = {...details, ...add};
        }
        if (this.state.toDate != null) {
          var add = {date_to: this.state.toDate};
          details = {...details, ...add};
        }
        if (this.state.keyword != '') {
          var add = {keyword: this.state.keyword};
          details = {...details, ...add};
        }
        if (this.state.minprice != '') {
          var add = {minprice: this.state.minprice};
          details = {...details, ...add};
        }
        if (this.state.maxprice != '') {
          var add = {maxprice: this.state.maxprice};
          details = {...details, ...add};
        }
        if (this.state.PickerSelectedStatusId != 0) {
          var add = {status: this.state.PickerSelectedStatusId};
          details = {...details, ...add};
        }
        this.setState({
          searchType: 'new_search',
        });
        this.props.getBuyerOrders(details, this.state.searchType);
        // this.setState({
        //     orderPageNo: this.state.orderPageNo+1
        // })
      });
    }
  };

  searchInDownloads = () => {
    if (
      this.state.downloadFromDate == null &&
      this.state.downloadToDate == null &&
      this.state.productName == ''
    ) {
      showToast({
        message: 'Tidak ada yang bisa di cari!',
      });
    } else {
      this.setState({
        donwloadPageNo: 1,
      });
      Keyboard.dismiss();
      this.retrieveToken().then((_token) => {
        var details = {
          _token: _token,
          page: this.state.donwloadPageNo,
        };
        if (this.state.downloadFromDate != null) {
          var add = {date_from: this.state.downloadFromDate};
          details = {...details, ...add};
        }
        if (this.state.downloadToDate != null) {
          var add = {date_to: this.state.downloadToDate};
          details = {...details, ...add};
        }
        if (this.state.productName != '') {
          var add = {product_name: this.state.productName};
          details = {...details, ...add};
        }
        this.setState({
          downloadSearchType: 'new_search',
        });
        this.props.getBuyerDownloads(details, this.state.downloadSearchType);
        this.setState({
          donwloadPageNo: this.state.donwloadPageNo + 1,
        });
      });
    }
  };

  getOrders = () => {
    this.retrieveToken().then((_token) => {
      var details = {
        _token: _token,
        status: 1,
        // 'page': this.state.orderPageNo
      };
      if (this.state.fromDate != null) {
        var add = {date_from: this.state.fromDate};
        details = {...details, ...add};
      }
      if (this.state.toDate != null) {
        var add = {date_to: this.state.toDate};
        details = {...details, ...add};
      }
      if (this.state.keyword != '') {
        var add = {keyword: this.state.keyword};
        details = {...details, ...add};
      }
      if (this.state.minprice != '') {
        var add = {minprice: this.state.minprice};
        details = {...details, ...add};
      }
      if (this.state.maxprice != '') {
        var add = {maxprice: this.state.maxprice};
        details = {...details, ...add};
      }
      if (this.state.PickerSelectedStatusId != 0) {
        var add = {status: this.state.PickerSelectedStatusId};
        details = {...details, ...add};
      }
      this.setState({
        searchType: 'none',
      });
      this.props.getBuyerOrders(details, this.state.searchType);
      // this.setState({
      //     orderPageNo: this.state.orderPageNo+1
      // })
    });
  };

  getDownloads = () => {
    this.retrieveToken().then((_token) => {
      var details = {
        _token: _token,
        // 'page': this.state.donwloadPageNo
      };
      if (this.state.downloadFromDate != null) {
        var add = {date_from: this.state.downloadFromDate};
        details = {...details, ...add};
      }
      if (this.state.downloadToDate != null) {
        var add = {date_to: this.state.downloadToDate};
        details = {...details, ...add};
      }
      if (this.state.productName != '') {
        var add = {product_name: this.state.productName};
        details = {...details, ...add};
      }
      this.setState({
        downloadSearchType: 'none',
      });
      this.props.getBuyerDownloads(details, this.state.downloadSearchType);
      this.setState({
        donwloadPageNo: this.state.donwloadPageNo + 1,
      });
    });
  };

  componentDidMount = () => {
    this.retrieveToken().then((_token) => {
      this.setState({tokenFromState: _token});
      this.props.getOrderHistoryStatusList(_token);
    });
    this.getOrders();
    // this.getDownloads();
  };

  componentWillUnmount = () => {
    this.props.emptyOrderHistory();
    this.props.emptyDownloadHistory();
  };

  // toOrderDetails = (product) => {
  //     var details = {
  //         '_token': this.state.tokenFromState,
  //         'order_id': product.order_id,
  //         'order_product_id': product.order_product_id
  //     }
  //     if(product.product_type == 'P'){
  //         this.props.navigation.navigate('ViewOrderPhysical', {details});
  //     } else if(product.product_type == 'D'){
  //         this.props.navigation.navigate('ViewOrderDigital', {details});
  //     }
  // }

  showDropDownHandler = (id) => {
    if (id == this.state.selectedOrder) {
      this.setState((prevState, prevProps) => {
        return {
          selectedOrder: null,
          showDropDown: !prevState.showDropDown,
        };
      });
    } else {
      this.setState({
        selectedOrder: id,
        showDropDown: true,
      });
    }
  };

  renderOrders = () => {
    // console.log(this.props.order_list);
    if (this.props.loading) {
      return (
        <View
          style={{
            marginTop: hp('25%'),
          }}>
          <Spinner color="red" size="large" />
        </View>
      );
    }
    if (this.props.order_list.length > 0) {
      return this.props.order_list.map((product) => (
        <View key={product.invoice_number}>
          <OrderHistoryComponent
            selected={this.state.selectedOrder}
            dropDownState={this.state.showDropDown}
            showDropDownChange={this.showDropDownHandler}
            navigation={this.props.navigation}
            key={product.invoice_number}
            productDetail={product}
            _token={this.state.tokenFromState}
          />
        </View>
      ));
    }
    if (this.props.no_orders) {
      return (
        <View
          style={{
            marginTop: hp('25%'),
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: hp('2.5%'),
              color: '#000',
            }}>
            Tidak ada pesanan!
          </Text>
        </View>
      );
    }
  };

  changeKeyword = (event) => {
    this.setState({keyword: event});
  };

  changeMinPrice = (event) => {
    this.setState({minprice: event});
  };

  changeMaxPrice = (event) => {
    this.setState({maxprice: event});
  };

  changeProductName = (event) => {
    this.setState({productName: event});
  };

  selectStatus = (item, statusList) => {
    statusList.forEach((element) => {
      if (item == element.props.value) {
        this.setState({
          PickerSelectedStatusId: element.key,
          PickerSelectedStatus: element.props.value,
        });
      }
    });
  };

  isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 50
    );
  };

  toDownloadTab = () => {
    this.setState({
      seg: 2,
    });
    if (this.state.donwloadPageNo <= 1) {
      this.getDownloads();
    }
  };

  getDownloadDetails = async (id) => {
    this.setState({
      loaderForDownloadDetails: true,
    });
    let details = {
      _token: this.state.tokenFromState,
      order_product_id: id,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/mydownloadsorderview', formBody)
      .then((response) => {
        this.setState({
          loaderForDownloadDetails: false,
        });
        if (response.data.status == 1) {
          let downloadDetails = response.data.download_order_view;
          this.props.navigation.navigate('ProductDetailsInfo', {
            pageHeader: 'File Details',
            file_details: downloadDetails,
          });
        } else {
          showToast({
            message: 'Tidak ditemukan detail',
          });
        }
      })
      .catch((err) => {
        console.log(err);
        showToast({
          message: 'Some error occured, Please try again!',
        });
        this.setState({
          loaderForDownloadDetails: false,
        });
      });
  };

  renderDownloads = () => {
    if (this.props.loading) {
      return (
        <View
          style={{
            marginTop: hp('29%'),
          }}>
          <Spinner color="red" size="large" />
        </View>
      );
    }
    if (this.props.download_list.length > 0) {
      return this.props.download_list.map((product) => (
        <View key={product.order_invoice} style={styles.boxStyle}>
          <View style={styles.imgContentWrapper}>
            <View style={styles.imgWrapper}>
              <Image
                resizeMode={'contain'}
                style={styles.boxImg2}
                source={{uri: product.product_image}}
              />
            </View>
            <View style={{flex: 1}}>
              <Text style={styles.productName}>{product.product_name}</Text>
              <Text style={styles.available}>{product.date_time}</Text>
              <Text style={styles.available2}>
                Oleh
                <Text style={styles.count2}> {product.seller_name}</Text>
              </Text>
              <Text style={styles.available}>
                Faktur {product.order_invoice_sign}:
                <Text style={styles.count}> {product.order_invoice}</Text>
              </Text>
              <Text style={styles.available}>
                IP direkam:
                <Text style={styles.count}> {product.ip_recorded}</Text>
              </Text>
              <Text style={styles.price}>
                {product.price_currency} {formatRupiah(product.product_price)}{' '}
              </Text>
              <TouchableOpacity
                onPress={() =>
                  this.getDownloadDetails(product.order_product_id)
                }>
                <Text style={styles.stat_us}>
                  <Text style={{fontSize: hp('3.5%'), color: '#c90305'}}>
                    &#8594;
                  </Text>
                  Melihat rincian
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ));
    }

    if (this.props.no_downloads) {
      return (
        <View
          style={{
            marginTop: hp('25%'),
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: hp('2.5%'),
              color: '#000',
            }}>
            Tidak ada Unduhan!
          </Text>
        </View>
      );
    }
  };

  render() {
    console.log(this.props);
    let statusList;
    if (this.props.status_list.length > 0) {
      statusList = this.props.status_list.map((value) => {
        return (
          <Picker.Item
            key={value.status_id}
            value={value.status_name}
            label={value.status_name}
          />
        );
      });
    }
    return (
      <Container style={styles.containerStyle}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <Segment style={styles.segmentStyle}>
          <Button
            first
            active={this.state.seg === 1 ? true : false}
            onPress={() => this.setState({seg: 1})}
            style={
              this.state.seg === 1 ? styles.segmentActive : styles.segmentBtn
            }>
            <Text
              style={
                this.state.seg === 1
                  ? styles.segmentBtnTxtActive
                  : styles.segmentBtnTxt
              }>
              Pesanan Saya
            </Text>
          </Button>
          <Button
            active={this.state.seg === 2 ? true : false}
            onPress={() => this.toDownloadTab()}
            style={
              this.state.seg === 2 ? styles.segmentActive : styles.segmentBtn
            }>
            <Text
              style={
                this.state.seg === 2
                  ? styles.segmentBtnTxtActive
                  : styles.segmentBtnTxt
              }>
              Informasi Unduhan
            </Text>
          </Button>
        </Segment>
        <View>
          {this.state.loaderForDownloadDetails && (
            <Spinner2 visible={this.state.loaderForDownloadDetails} />
          )}
          {this.state.seg === 1 && (
            <ScrollView
              keyboardShouldPersistTaps={'handled'}
              // onScroll={({ nativeEvent }) => {
              //     if(this.isCloseToBottom(nativeEvent)){
              //         // if(this.state.orderPageNo <= this.props.total_page){
              //             this.getOrders();
              //         // }
              //     }}}
            >
              <View style={{paddingBottom: hp('20%')}}>
                <View style={styles.topSection}>
                  <View style={styles.row}>
                    <TextInput
                      style={styles.inputStyle}
                      placeholder="Kata kunci"
                      onChangeText={this.changeKeyword.bind(this)}
                    />
                    <View style={styles.dropdownClass}>
                      <Picker
                        style={styles.pickerClass}
                        selectedValue={this.state.PickerSelectedStatus}
                        onValueChange={(itemValue) =>
                          this.selectStatus(itemValue, statusList)
                        }>
                        {statusList}
                      </Picker>
                    </View>
                    <View style={styles.datepicherStyle}>
                      <DatePicker
                        locale={'en'}
                        timeZoneOffsetInMinutes={undefined}
                        modalTransparent={false}
                        animationType={'fade'}
                        androidMode={'default'}
                        placeHolderText="Tanggal Dari"
                        textStyle={{color: '#000', fontSize: wp('4%')}}
                        placeHolderTextStyle={{
                          color: '#d3d3d3',
                          fontSize: wp('4%'),
                        }}
                        onDateChange={this.setFromDate}
                      />
                      <Image
                        style={styles.boxImg}
                        source={require('../images/datepickerIcon.png')}
                      />
                    </View>
                  </View>
                  <View style={styles.row}>
                    <View style={styles.datepicherStyle}>
                      <DatePicker
                        // defaultDate={new Date()}
                        locale={'en'}
                        timeZoneOffsetInMinutes={undefined}
                        modalTransparent={false}
                        animationType={'fade'}
                        androidMode={'default'}
                        placeHolderText="Tanggal Ke"
                        textStyle={{color: '#000', fontSize: wp('4%')}}
                        placeHolderTextStyle={{
                          color: '#d3d3d3',
                          fontSize: wp('4%'),
                        }}
                        onDateChange={this.setToDate}
                        showIcon={true}
                      />
                      <Image
                        style={styles.boxImg}
                        source={require('../images/datepickerIcon.png')}
                      />
                    </View>
                    <TextInput
                      style={styles.inputStyle}
                      placeholder="Pesan dari(Rp)"
                      onChangeText={this.changeMinPrice.bind(this)}
                    />
                    <TextInput
                      style={styles.inputStyle}
                      placeholder="Pesan ke(Rp)"
                      onChangeText={this.changeMaxPrice.bind(this)}
                    />
                  </View>
                  {!this.state.hideHeader ? (
                    <View style={styles.btnWrapper}>
                      <ButtonSkip onPress={() => this.search()}>
                        <Text style={styles.btnFont}>Cari</Text>
                      </ButtonSkip>
                      <ButtonTwo onPress={() => this.clearAll()}>
                        <Text style={styles.btnFont}>Hapus Perubahan</Text>
                      </ButtonTwo>
                    </View>
                  ) : null}
                </View>
                {this.renderOrders()}
                {this.props.scrollLoading ? (
                  <View style={{marginTop: 20}}>
                    <Spinner color="red" size="large" />
                  </View>
                ) : null}
                {this.props.endOfRecords ? (
                  <View style={{marginTop: 20}}>
                    <Text style={{textAlign: 'center'}}>
                      Tidak ada lagi pesanan
                    </Text>
                  </View>
                ) : null}
              </View>
            </ScrollView>
          )}
          {this.state.seg === 2 && (
            <ScrollView
              keyboardShouldPersistTaps={'handled'}
              // onScroll={({ nativeEvent }) => {
              //     if(this.isCloseToBottom(nativeEvent)){
              //         if(this.state.donwloadPageNo <= this.props.total_page_downloads){
              //             this.getDownloads();
              //         }
              //     }}}
            >
              <View style={{paddingBottom: hp('20%')}}>
                <View style={styles.topSection}>
                  <View style={styles.row}>
                    <TextInput
                      style={styles.inputStyle}
                      placeholder="Nama Produk"
                      onChangeText={this.changeProductName.bind(this)}
                    />
                    <View style={styles.datepicherStyle}>
                      <DatePicker
                        locale={'en'}
                        timeZoneOffsetInMinutes={undefined}
                        modalTransparent={false}
                        animationType={'fade'}
                        androidMode={'default'}
                        placeHolderText="Tanggal Dari"
                        textStyle={{color: '#000', fontSize: wp('4%')}}
                        placeHolderTextStyle={{
                          color: '#d3d3d3',
                          fontSize: wp('4%'),
                        }}
                        onDateChange={this.setDownloadFromDate}
                        showIcon={true}
                      />
                      <Image
                        style={styles.boxImg}
                        source={require('../images/datepickerIcon.png')}
                      />
                    </View>
                    <View style={styles.datepicherStyle}>
                      <DatePicker
                        locale={'en'}
                        timeZoneOffsetInMinutes={undefined}
                        modalTransparent={false}
                        animationType={'fade'}
                        androidMode={'default'}
                        placeHolderText="Tanggal Ke"
                        textStyle={{color: '#000', fontSize: wp('4%')}}
                        placeHolderTextStyle={{
                          color: '#d3d3d3',
                          fontSize: wp('4%'),
                        }}
                        onDateChange={this.setDownloadToDate}
                      />
                      <Image
                        style={styles.boxImg}
                        source={require('../images/datepickerIcon.png')}
                      />
                    </View>
                  </View>
                  <View style={styles.btnWrapper}>
                    <ButtonSkip onPress={() => this.searchInDownloads()}>
                      <Text style={styles.btnFont}>Cari</Text>
                    </ButtonSkip>
                    <ButtonTwo onPress={() => this.clearInDownloads()}>
                      <Text style={styles.btnFont}>Hapus Perubahan</Text>
                    </ButtonTwo>
                  </View>
                </View>
                {this.renderDownloads()}
                {this.props.scrollLoading ? (
                  <View style={{marginTop: 20}}>
                    <Spinner color="red" size="large" />
                  </View>
                ) : null}
                {this.props.endOfDownloads ? (
                  <View style={{marginTop: 20}}>
                    <Text style={{textAlign: 'center'}}>
                      Tidak ada lagi unduhan
                    </Text>
                  </View>
                ) : null}
              </View>
            </ScrollView>
          )}
        </View>
        <FlashMessage
          position="top"
          icon="success"
          floating={true}
          animated={true}
          animationDuration={300}
          duration={4000}
        />
      </Container>
    );
  }
}

const styles = {
  dropdownClass: {
    // elevation: 1,
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
    borderRadius: 50,
  },
  pickerClass: {
    height: hp('6%'),
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  containerStyle: {
    padding: 0,
    // flex:1
  },
  topSection: {
    backgroundColor: '#e6e6e6',
    padding: 10,
  },

  row: {
    flexDirection: 'row',
    flexWarp: 'warp',
    justifyContent: 'space-between',
  },
  inputStyle: {
    color: '#000',
    paddingLeft: 10,
    fontSize: wp('4%'),
    marginTop: 0,
    backgroundColor: '#fff',
    height: 40,
    fontFamily: Font.RobotoRegular,
    width: wp('30%'),
    borderRadius: 5,
  },
  datepicherStyle: {
    color: '#000',
    paddingLeft: 0,
    fontSize: wp('4%'),
    marginTop: 0,
    backgroundColor: '#fff',
    height: 40,
    fontFamily: Font.RobotoRegular,
    width: wp('30%'),
    borderRadius: 5,
  },
  dropdownClass: {
    marginBottom: 10,
    paddingLeft: 10,
    backgroundColor: '#fff',
    borderRadius: 5,
    // fontSize: wp('4%'),
    width: wp('30%'),
  },
  pickerClass: {
    //    width:wp('20%'),
    height: 40,
    //    fontSize: wp('4%'),
    width: 'auto',
  },
  dropheadingClass: {
    display: 'none',
  },
  btnWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 15,
  },
  btnFont: {
    fontSize: hp('2%'),
    color: '#fff',
    fontFamily: Font.RobotoRegular,
  },
  segmentStyle: {
    backgroundColor: '#b90002',
    textAlign: 'left',
    justifyContent: 'flex-start',
    paddingLeft: 10,
    height: 'auto',
    paddingTop: 10,
    // alignItems:'baseline',
    paddingBottom: 0,
  },
  segmentBtn: {
    borderWidth: 0,
    borderColor: 'transparent',
    bottom: 0,
    margin: 0,
    paddingTop: 10,
    paddingBottom: 10,
    height: 'auto',
    marginBottom: 0,
  },
  segmentBtnTxt: {
    color: '#e6e6e6',
  },
  segmentBtnTxtActive: {
    color: '#606060',
    // color:'#f00'
  },
  segmentActive: {
    backgroundColor: '#e6e6e6',
    borderWidth: 0,
    borderColor: 'transparent',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    paddingTop: 10,
    height: 'auto',
    paddingBottom: 10,
    color: '#e6e6e6',
  },
  boxImg: {
    position: 'absolute',
    right: 5,
    top: 13,
    // width:wp('5%'),
    // height:wp('5%')
    width: 16,
    height: 16,
  },

  imgContentWrapper: {
    flexDirection: 'row',
    // flexBasis:'80%',
    flex: 1,
    paddingRight: 15,
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    paddingBottom: 10,
  },
  imgWrapper: {
    width: wp('20%'),
    marginRight: wp('3%'),
  },
  boxImg2: {
    width: wp('20%'),
    height: wp('32%'),
  },
  productName: {
    fontSize: wp('4.5%'),
    color: '#545454',
    fontFamily: Font.RobotoBold,
  },
  count: {
    color: '#00b3ff',
    fontSize: wp('4%'),
    fontFamily: Font.RobotoRegular,
  },
  count2: {
    color: '#00b3ff',
    fontSize: wp('4%'),
    fontFamily: Font.RobotoItalc,
  },
  stat_us: {
    color: '#c90305',
    fontSize: wp('4%'),
  },
  available: {
    color: '#858585',
    fontSize: wp('4%'),
    marginRight: wp('10%'),
    fontFamily: Font.RobotoMedium,
  },
  available2: {
    color: '#858585',
    fontSize: wp('4%'),
    marginRight: wp('10%'),
    fontFamily: Font.RobotoItalc,
  },
  boxStyle: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    paddingBottom: 0,
    flexDirection: 'row',
    flexWarp: 'warp',
    position: 'relative',
  },
  price: {
    color: '#00b3ff',
    fontSize: wp('5%'),
  },
};

const mapStateToProps = (state) => {
  return {
    order_list: state.buyerOrders.order_list,
    no_orders: state.buyerOrders.no_orders,
    status_list: state.buyerOrders.status_list,
    total_page: state.buyerOrders.total_page,
    loading: state.buyerOrders.loading,
    scrollLoading: state.buyerOrders.scrollLoading,
    endOfRecords: state.buyerOrders.endOfRecords,
    download_list: state.buyerOrders.download_list,
    total_page_downloads: state.buyerOrders.total_page_downloads,
    endOfDownloads: state.buyerOrders.endOfDownloads,
    no_downloads: state.buyerOrders.no_downloads,
  };
};

export default connect(mapStateToProps, {
  getOrderDetails,
  emptyDownloadHistory,
  getBuyerDownloads,
  emptyOrderHistory,
  getOrderHistoryStatusList,
  getBuyerOrders,
})(OrderHistory);
