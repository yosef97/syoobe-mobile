import React, {Component} from 'react';
import {AsyncStorage, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {
  emptyDownloadHistory,
  emptyOrderHistory,
  getBuyerDownloads,
  getBuyerOrders,
  getOrderDetails,
  getOrderHistoryStatusList,
} from '../actions';
import {Spinner} from './common';
// import MyOrderListComponent from './MyOrderListComponent';
import {MyOrderTabNavigator} from './routes/HistoryTab';

class MyOrderList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tokenFromState: null,
      loading: true,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = () => {
    // this.setState({loading: true});
    this.retrieveToken().then((_token) => {
      this.setState({tokenFromState: _token});
      this.props.getOrderHistoryStatusList(_token);
      this.setState({loading: false});
    });
    // this.getOrders();
    // this.getDownloads();
  };

  getOrders = (id) => {
    this.retrieveToken().then((_token) => {
      let details = {
        _token,
        status: 1 || id,
      };
      this.props.getBuyerOrders(details, 'new_search');
    });
  };

  render() {
    console.log('in state', JSON.stringify(this.state));
    // console.log('in props', JSON.stringify(this.props));

    // console.log(JSON.stringify(this.props.status_list));
    if (this.state.loading) {
      return (
        <View
          style={{
            marginTop: hp('29%'),
          }}>
          <Spinner color="red" size="large" />
        </View>
      );
    }
    return (
      <View style={{flex: 1, backgroundColor: '#f5f6fa'}}>
        {this.props.status_list && this.props.status_list.length > 0 && (
          <MyOrderTabNavigator
            loading={this.state.loading}
            lists={this.props.status_list}
            navigation={this.props.navigation}
            getOrder={(id) => this.getOrders(id)}
          />
        )}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    order_list: state.buyerOrders.order_list,
    no_orders: state.buyerOrders.no_orders,
    status_list: state.buyerOrders.status_list,
    total_page: state.buyerOrders.total_page,
    loading: state.buyerOrders.loading,
    scrollLoading: state.buyerOrders.scrollLoading,
    endOfRecords: state.buyerOrders.endOfRecords,
    download_list: state.buyerOrders.download_list,
    total_page_downloads: state.buyerOrders.total_page_downloads,
    endOfDownloads: state.buyerOrders.endOfDownloads,
    no_downloads: state.buyerOrders.no_downloads,
  };
};

export default connect(mapStateToProps, {
  getOrderDetails,
  emptyDownloadHistory,
  getBuyerDownloads,
  emptyOrderHistory,
  getOrderHistoryStatusList,
  getBuyerOrders,
})(MyOrderList);
