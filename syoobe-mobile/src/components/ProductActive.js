import React, {Component} from 'react';
import {AsyncStorage, FlatList, View, Alert, Text} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {
  getSellerActiveItems,
  deleteSellerProduct,
  changeProductStatus,
  getAddressInfo,
  getBankDetails,
} from '../actions';
import {Spinner} from './common';
import {EmptyProductSeller} from './common/state/EmptyProductSeller';
import {ProductCardSeller} from './product/ProductCardSeller';
import ActionButton from 'react-native-action-button';
import PTRView from 'react-native-pull-to-refresh';

class ProductActive extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newProductArray: [],
      shop_id: null,
      loading1: true,
      loading2: true,
      warning: false,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  handleWarning = () => {
    this.setState({warning: false});
  };

  componentDidMount() {
    console.log('componentDidMount');
    this.getBankInfo();
    this.getAddressReturn();
    this.fetchData();
    AsyncStorage.getItem('shop_id').then((res) =>
      this.setState({shop_id: res, loading1: false}),
    );
    this.handleWarning();
  }

  fetchData = async () => {
    console.log('fetchData');
    await this.retrieveToken().then((_token) => {
      console.log('ini token', _token);
      this.props.getSellerActiveItems(_token);
    });
    this.setState({
      loading2: false,
    }).catch((error) => {
      console.log('error', error);
    });
  };

  getBankInfo = async () => {
    console.log('getBankInfo');
    await this.retrieveToken()
      .then((_token) => {
        this.props.getBankDetails({_token});
      })
      .catch((error) => {
        console.log('Promise is rejected with error: ' + error);
      });
  };

  getAddressReturn = async () => {
    console.log('getAddressReturn');
    await this.retrieveToken()
      .then((_token) => {
        this.props.getAddressInfo(_token);
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  componentDidUpdate = async (prevProps, prevState) => {
    if (
      prevProps.active_products.length !== this.props.active_products.length
    ) {
      let array = [];
      await this.props.active_products.map((product, index) => {
        let obj = {sideMenu: false, ...product};
        array.push({...obj});
      });
      await this.setState({
        newProductArray: array,
      });
    }
  };

  toProductDetail = (product_id) => {
    this.props.navigation.push('ProductDetail', {
      product_id: product_id,
      user_product: 'true',
      // product_requires_shipping: this.props.details.prod_requires_shipping,
    });
  };

  onPauseItem = (product_id, ref) => {
    this.retrieveToken()
      .then((_token) => {
        var product_status = 1;
        var details = {
          _token: _token,
          product_id: product_id,
          product_status: product_status,
        };
        ref.close();
        this.props.changeProductStatus(details, 'active');
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  onRemoveItem = async (product_id, product_name, type, ref) => {
    Alert.alert(
      'Konfirmasi',
      'Apakah anda yakin akan hapus produk ' + product_name + ' ?',
      [
        {
          text: 'Batal',
          onPress: () => console.log('cancelled'),
          style: 'cancel',
        },
        {
          text: 'Ya, Hapus',
          onPress: () => {
            this.retrieveToken()
              .then((_token) => {
                var details = {
                  _token: _token,
                  product_id: product_id,
                };
                ref.close();
                this.props.deleteSellerProduct(details, type);
              })
              .catch((error) => {
                console.log('error', error);
              });
          },
        },
      ],
    );
  };

  toProductDetail = (product_id, product_name) => {
    this.props.navigation.navigate('ProductDetail', {
      product_id,
      // product_requires_shipping: this.props.details.prod_requires_shipping,
      product_name,
    });
  };

  onEditItem = (id, ref) => {
    ref.close();
    this.props.navigation.navigate('AddProduct', {prod_id: id});
  };

  render() {
    // console.log('ini true false shop id', this.state.shop_id == '');

    // console.log('ini true false shop id', this.state.shop_id);

    // console.log('ini state', JSON.stringify(this.state._));
    console.log('ini props product active', JSON.stringify(this.props));

    return (
      <>
        {this.state.loading1 || this.state.loading2 ? (
          <View
            style={{
              marginTop: hp('35%'),
            }}>
            <Spinner color="red" size="large" />
          </View>
        ) : this.state.shop_id == null ? (
          Alert.alert('Informasi', 'Tidak ada toko, Silahkan buat toko', [
            {
              text: 'OK',
              onPress: () => this.props.navigation.navigate('CreatShopStepOne'),
            },
          ])
        ) : this.props.bank_name == null ? (
          Alert.alert(
            'Informasi',
            'Tidak ada bank, Silahkan lengkapi rekening bank anda',
            [
              {
                text: 'OK',
                onPress: () =>
                  this.props.navigation.navigate('CreatShopStepOne'),
              },
            ],
          )
        ) : this.props.ura_name && this.props.ura_name == null ? (
          Alert.alert(
            'Informasi',
            'Silahkan lengkapi data alamat pengembailan',
            [
              {
                text: 'OK',
                onPress: () => this.props.navigation.navigate('EditProfile'),
              },
            ],
          )
        ) : (
          <View style={{flex: 1, backgroundColor: '#f5f6fa', padding: wp(3)}}>
            <PTRView
              onRefresh={() =>
                this.retrieveToken().then((_token) => {
                  this.props.getSellerActiveItems(_token);
                })
              }>
              {this.state.warning && (
                <View
                  style={{
                    backgroundColor: '##F3DEDD',
                  }}>
                  <Text style={{fontWeight: 'bold'}}>
                    Telah terjadi kesalahan berikut :{' '}
                  </Text>
                  <Text>
                    Anda telah melampaui produk maksimum yang dapat Anda jual.
                    <Text
                      style={{fontWeight: 'bold', color: '#FF3A59'}}
                      onPress={() =>
                        this.props.navigation.navigate('LearnSellingLimit')
                      }>
                      Pelajari lebih lagi bagaimana cara kerja batas penjualan
                    </Text>
                    Anda mungkin dapat mengajukan
                    <Text
                      style={{fontWeight: 'bold', color: '#FF3A59'}}
                      onPress={() =>
                        this.props.navigation.navigate('ApplySellingLimit', {
                          can_add_product: this.props.can_add_product,
                          can_upgrade_limit: this.props.can_upgrade_limit,
                          upgrade_limit_info: this.props.upgrade_limit_info,
                        })
                      }>
                      {' '}
                      Permintaan menaikan batas penjualan{' '}
                    </Text>{' '}
                    dengan mengkonfirmasi informasi Anda pada kami.
                  </Text>
                </View>
              )}

              <FlatList
                data={this.props.active_products}
                renderItem={({item, index}) => (
                  <ProductCardSeller
                    item={item}
                    key={index}
                    onPressPause={(id, ref) => this.onPauseItem(id, ref)}
                    onPressDelete={(id, name, ref) =>
                      this.onRemoveItem(id, name, 'active', ref)
                    }
                    onPressEdit={(id, ref) => this.onEditItem(id, ref)}
                    onPress={(id, name) => this.toProductDetail(id, name)}
                  />
                )}
                ListEmptyComponent={() => (
                  <EmptyProductSeller
                    addProduct
                    onPress={() => this.props.navigation.navigate('AddProduct')}
                  />
                )}
                showsVerticalScrollIndicator={false}
              />
            </PTRView>

            <ActionButton
              buttonColor="rgba(231,76,60,1)"
              onPress={
                this.props.can_add_product !== 0
                  ? () => this.props.navigation.navigate('AddProduct')
                  : () => {
                      this.setState({warning: true});
                    }
              }
            />
          </View>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    upgrade_limit_info: state.sellerProducts.upgrade_limit_info,
    can_add_product: state.sellerProducts.can_add_product,
    can_upgrade_limit: state.sellerProducts.can_upgrade_limit,
    no_paused: state.sellerProducts.no_paused,
    no_active: state.sellerProducts.no_active,
    active_products: state.sellerProducts.active_products,
    paused_products: state.sellerProducts.paused_products,
    sellerInfo: state.profile.sellerInfo,
    ura_name: state.profile.ura_name,
    ura_address_line_1: state.profile.ura_address_line_1,
    ura_address_line_2: state.profile.ura_address_line_2,
    ura_district_id: state.profile.ura_district_id,
    ura_district: state.profile.ura_district,
    ura_state: state.profile.ura_state,
    ura_state_name: state.profile.ura_state_name,
    ura_city: state.profile.ura_city,
    ura_city_id: state.profile.ura_city_id,
    ura_zip: state.profile.ura_zip,
    ura_phone: state.profile.ura_phone,
    bank_name: state.bank.bank_name,
    account_holder_name: state.bank.account_holder_name,
    account_number: state.bank.account_number,
    ifsc_code: state.bank.ifsc_code,
    paypal_id: state.bank.paypal_id,
    bank_address: state.bank.bank_address,
  };
};

export default connect(mapStateToProps, {
  getSellerActiveItems,
  deleteSellerProduct,
  changeProductStatus,
  getAddressInfo,
  getBankDetails,
})(ProductActive);
