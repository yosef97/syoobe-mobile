import React, {Component} from 'react';
import {Alert, Platform, View, Text} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import axios from 'axios';
import {Spinner} from './common';
import {StackActions, CommonActions} from '@react-navigation/native';
import FlashMessage, {showMessage} from 'react-native-flash-message';
import {WebView} from 'react-native-webview';
import {getWalletDetails, emptyWalletDetails} from '../actions';

class PaypalView extends Component {
  static navigationOptions = ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
    headerTitle: (
      <Text style={{fontSize: hp('2.5%'), color: '#fff'}}>Pay with Paypal</Text>
    ),
  });

  state = {
    _token: this.props.route.params._token,
    loading: false,
    loadingText: null,
    loadingText2: null,
    payKey: null,
    data: this.props.route.params.data,
    headersForPaypal: null,
    paymentDetailsGenerated: false,
    adaptiveFinalArray: [],
  };

  componentDidMount = () => {
    const {
      admin_paypal_id,
      price_currency,
      seller_amount,
      seller_bank_details,
      admin_commission_amount,
      admin_mail,
      pre_ApprovalKey,
      amount,
      payment_settings,
    } = this.state.data;
    let receiverDetails;
    if (
      this.state.data.postage_type == 'postage' ||
      this.state.data.postage_type == 'wallet'
    ) {
      receiverDetails = [
        {
          amount: amount,
          email: admin_mail,
        },
      ];
    } else {
      receiverDetails = [
        {
          amount: admin_commission_amount,
          email: admin_paypal_id,
        },
        {
          amount: seller_amount,
          email: seller_bank_details[0].ub_user_paypalid,
        },
      ];
    }
    let dataDetail = {
      actionType: 'PAY',
      currencyCode: price_currency ? price_currency : 'USD',
      receiverList: {
        receiver: [...receiverDetails],
      },
      returnUrl: 'https://syoobe.co.id/custom/payment_success',
      cancelUrl: 'https://syoobe.co.id/custom/payment_failed',
      requestEnvelope: {
        errorLanguage: 'en_US',
        detailLevel: 'ReturnAll',
      },
    };

    if (this.state.data.postage_type == 'postage' && pre_ApprovalKey) {
      dataDetail = {
        ...dataDetail,
        ...{
          preapprovalKey: pre_ApprovalKey,
        },
      };
    }

    const headersArg = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-PAYPAL-SECURITY-USERID': payment_settings.acct_username,
      'X-PAYPAL-SECURITY-PASSWORD': payment_settings.acct_password,
      'X-PAYPAL-SECURITY-SIGNATURE': payment_settings.acct_signature,
      'X-PAYPAL-REQUEST-DATA-FORMAT': 'JSON',
      'X-PAYPAL-RESPONSE-DATA-FORMAT': 'JSON',
      'X-PAYPAL-APPLICATION-ID': payment_settings.acct_appid,
    };
    this.setState({
      headersForPaypal: headersArg,
    });
    this.firstCallPay(dataDetail, headersArg);
  };

  goToHelpPage = () => {
    this.props.navigation.dispatch({
      type: 'Navigation/NAVIGATE',
      routeName: 'Help',
    });
  };

  paymentFailed = () => {
    const popAction = StackActions.pop({
      n: 1,
    });
    Alert.alert('Alert', 'Payment failed', [
      {text: 'Cancel', style: 'cancel'},
      {text: 'Contact us', onPress: this.goToHelpPage},
    ]);
    this.props.navigation.dispatch(popAction);
  };

  firstCallPay = async (dataDetail, headersArg) => {
    this.setState({
      loadingText: 'Please wait while we interact with Paypal...',
      loading: true,
    });
    await axios
      .post(
        'https://svcs.sandbox.paypal.com/AdaptivePayments/Pay',
        dataDetail,
        {
          headers: {
            ...headersArg,
          },
        },
      )
      .then((response) => {
        console.log('rr', response);
        this.setState({
          loadingText: null,
          loading: false,
        });
        if (response.data.responseEnvelope.ack == 'Success') {
          if (response.data.paymentExecStatus == 'COMPLETED') {
            if (response.data.paymentInfoList) {
              const {payKey, paymentInfoList} = response.data;
              this.savePayment(payKey, paymentInfoList);
            } else {
              this.getPaymentDetails(response.data.payKey, headersArg);
            }
          } else if (response.data.paymentExecStatus == 'CREATED') {
            this.setState({
              payKey: response.data.payKey,
            });
          }
        } else if (response.data.responseEnvelope.ack == 'Failure') {
          this.paymentFailed();
        }
      })
      .catch((err) => {
        this.setState({
          loadingText: null,
          loading: false,
        });
        this.paymentFailed();
        console.log({...err});
      });
  };

  _onNavigationStateChange = async (webViewState) => {
    if (
      webViewState.url.includes('https://syoobe.co.id/custom/payment_success')
    ) {
      if (!this.state.paymentDetailsGenerated) {
        this.getPaymentDetails();
        await this.setState({
          paymentDetailsGenerated: true,
        });
      }
    } else if (
      webViewState.url.includes('https://syoobe.co.id/custom/payment_failed')
    ) {
      // this.getPaymentDetails();
      this.paymentFailed();
    }
  };

  getPaymentDetails = async (payKey, headersArg) => {
    if (payKey) {
      this.setState({
        loadingText: 'Retreiving data from Paypal...',
        loading: true,
      });
    }
    const dataDetail = {
      payKey: payKey ? payKey : this.state.payKey,
      requestEnvelope: {
        errorLanguage: 'en_US',
      },
    };
    await axios
      .post(
        `https://svcs.sandbox.paypal.com/AdaptivePayments/PaymentDetails`,
        dataDetail,
        {
          headers: headersArg
            ? {
                ...headersArg,
              }
            : {
                ...this.state.headersForPaypal,
              },
        },
      )
      .then((response) => {
        console.log('final data', response);
        this.setState({
          loadingText: null,
          loading: false,
          // payKey: null,
        });
        const {payKey, paymentInfoList} = response.data;
        this.savePayment(payKey, paymentInfoList);
      })
      .catch((err) => {
        console.log({...err});
      });
  };

  getWallet = async () => {
    await this.props.emptyWalletDetails();
    var details = {
      _token: this.state._token,
      page: 1,
    };
    this.props.getWalletDetails(details);
  };

  clearCartApi = async () => {
    let details = {
      _token: this.state._token,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/clearCartAPI', formBody)
      .then((response) => {
        console.log('clear', response);
      });
  };

  redirectToPage = (data) => {
    if (
      this.state.data.postage_type == 'wallet' ||
      this.state.data.postage_type == 'postage'
    ) {
      this.setState({
        loading: true,
      });
      if (this.state.data.postage_type == 'wallet') {
        this.getWallet();
      } else if (
        this.state.data.postage_type == 'postage' &&
        this.props.route.params.postageDetails
      ) {
        this.props.route.params.postageDetails();
      }
      setTimeout(() => {
        this.setState({
          loading: false,
        });
        showMessage({
          message: 'Success',
          description: `$ ${this.state.data.amount} added to your ${
            this.state.data.postage_type == 'postage' ? 'Postage' : 'Wallet'
          } Account balance!`,
          type: 'success',
        });
        this.props.navigation.dispatch({
          type: 'Navigation/NAVIGATE',
          routeName: 'homeDrawer',
          action: {
            type: 'Navigation/NAVIGATE',
            routeName:
              this.state.data.postage_type == 'wallet'
                ? 'My Wallet'
                : 'PostageAccountHistory',
          },
        });
      }, 3000);
    } else {
      this.setState({
        loading: false,
        paykey: null,
      });
      this.clearCartApi();
      showMessage({
        message: 'Success',
        description: `Order Placed!`,
        type: 'success',
      });
      if (data.order_product_data) {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            CommonActions.navigate({
              routeName: 'ThankYou',
              params: {
                orders: data.order_product_data,
                _token: this.state._token,
                order_id: this.state.data.order_id,
              },
            }),
          ],
        });
        this.props.navigation.dispatch(resetAction);
      }
    }
  };

  savePayment = async (payKey, paymentInfoList) => {
    this.setState({
      loadingText: 'Completing transaction...',
      loading: true,
    });
    if (
      this.state.data.postage_type == 'postage' ||
      this.state.data.postage_type == 'wallet'
    ) {
      const {status, postage_type, amount, postage_notes} = this.state.data;
      details = {
        _token: this.state._token,
        transaction_id: paymentInfoList.paymentInfo[0].transactionId,
        sendortransactionId: paymentInfoList.paymentInfo[0].senderTransactionId,
        receiverPaypalId: paymentInfoList.paymentInfo[0].receiver.email,
        paykey: payKey,
        status: status,
        type: postage_type,
        amount: amount,
        postage_notes: postage_notes,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post('https://syoobe.co.id/api/storePaypalResponseInAddFund', formBody)
        .then((response) => {
          this.setState({
            loadingText: null,
            loading: false,
          });
          console.log('res', response);
          if (response.data.status == 1) {
            this.redirectToPage(response.data);
          }
        })
        .catch((err) => {
          console.log('err', err);
        });
    } else {
      let adaptiveArray = [];
      paymentInfoList.paymentInfo.forEach((eachPayment) => {
        console.log('each', eachPayment);
        if (eachPayment.receiver.email == 'paypal@syoobe.com') {
          let obj = {
            receiver_paypal_id: eachPayment.receiver.email,
            payment_status: '',
            sendortransactionId: eachPayment.senderTransactionId,
            transaction_id: '',
          };
          // adaptiveArray.push({...obj});
          adaptiveArray[1] = {...obj};
        } else {
          let obj = {
            receiver_paypal_id: eachPayment.receiver.email,
            payment_status: eachPayment.transactionStatus,
            sendortransactionId: eachPayment.senderTransactionId,
            transaction_id: eachPayment.transactionId,
          };
          adaptiveArray[0] = {...obj};
          // adaptiveArray.push({...obj});
        }
      });

      await this.setState({
        adaptiveFinalArray: [...adaptiveArray],
      });
      details = {
        _token: this.state._token,
        order_id: this.state.data.order_id,
        status: 1,
        pay_key: payKey,
        payment: [...this.state.adaptiveFinalArray],
      };
      let formBody = [];
      for (let property in details) {
        if (property == 'payment') {
          let array = [];
          for (let i = 0; i < details[property].length; i++) {
            array.push(details[property][i]);
          }
          formBody.push(property + '=' + JSON.stringify(array));
        } else {
          let encodedKey = encodeURIComponent(property);
          let encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
      }
      formBody = formBody.join('&');
      await axios
        .post('https://syoobe.co.id/api/paypalAdaptivePayment', formBody)
        .then((response) => {
          this.setState({
            loadingText: null,
            loading: false,
          });
          console.log('res', response);
          if (response.data.status == 1) {
            this.redirectToPage(response.data);
          }
        })
        .catch((err) => {
          console.log('err', err);
        });
    }
  };

  activityIndicatorLoadingView = () => {
    return (
      <Spinner
        text={'Please wait while we redirect you to Paypal...'}
        color="red"
        size="large"
      />
    );
  };

  render() {
    const {loadingText, loadingText2, payKey} = this.state;
    if (loading) {
      return (
        <View
          style={{
            height: hp('80%'),
            width: wp('100%'),
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Spinner
            secondText={loadingText2}
            text={loadingText}
            color="red"
            size="large"
          />
        </View>
      );
    }
    return (
      <View
        style={{
          backgroundColor: '#fff',
          height: '100%',
          width: '100%',
        }}>
        <FlashMessage position="top" />
        {payKey && (
          <WebView
            style={styles.webViewStyle}
            source={{
              uri: `https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey=${payKey}&expType=redirect`,
            }}
            onNavigationStateChange={this._onNavigationStateChange}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={true}
            renderLoading={this.activityIndicatorLoadingView}
            style={{marginTop: 20}}
          />
        )}
      </View>
    );
  }
}

const styles = {
  webViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
};

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {emptyWalletDetails, getWalletDetails})(
  PaypalView,
);
