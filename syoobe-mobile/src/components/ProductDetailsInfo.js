import React, {Component} from 'react';
import {Image, Text, View, ScrollView} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
export default class ProductDetailsInfo extends Component {
  state = {
    compatibilityHeader: null,
    compatibilityValues: null,
    specifications: null,
    policies: null,
    currency_symbol: null,
    store_name: null,
    download_details: null,
  };

  fetchData = () => {
    this.setState({
      specifications: this.props.route.params.spec,
      compatibilityHeader: this.props.route.params.header,
      compatibilityValues: this.props.route.params.values,
      policies: this.props.route.params.policies,
      currency_symbol: this.props.route.params.price_currency,
      store_name: this.props.route.params.store_name,
      download_details: this.props.route.params.file_details,
    });
  };

  componentDidMount = async () => {
    await this.fetchData();
  };

  render() {
    console.log('ini props', JSON.stringify(this.props));
    return (
      <ScrollView style={{backgroundColor: '#fff'}}>
        <View style={styles.shippingWrapper}>
          {this.state.download_details != null && (
            <View>
              <View style={styles.viewStyle}>
                <View horizontal={true} style={styles.tableWrapper}>
                  <View style={styles.tableinner2}>
                    <View
                      style={{
                        width: '25%',
                        padding: 10,
                        borderRightWidth: 1,
                        borderRightColor: '#d2d8da',
                      }}>
                      <Text style={styles.textStyle}>Gambar</Text>
                    </View>
                    <View
                      style={{
                        width: '25%',
                        padding: 10,
                        borderRightWidth: 1,
                        borderRightColor: '#d2d8da',
                      }}>
                      <Text style={styles.textStyle}>Alamat IP</Text>
                    </View>
                    <View
                      style={{
                        width: '25%',
                        padding: 10,
                        borderRightWidth: 1,
                        borderRightColor: '#d2d8da',
                      }}>
                      <Text style={styles.textStyle}>Nama file</Text>
                    </View>
                    <View
                      style={{
                        width: '25%',
                        padding: 10,
                        borderRightWidth: 1,
                        borderRightColor: '#d2d8da',
                      }}>
                      <Text style={styles.textStyle}>Tanggal & Waktu</Text>
                    </View>
                  </View>
                  {this.state.download_details.map((value, index) => (
                    <View key={index} style={styles.tableInner}>
                      <View
                        style={{
                          width: '25%',
                          padding: 3,
                          borderRightWidth: 1,
                          borderRightColor: '#d2d8da',
                        }}>
                        <Image
                          style={styles.imageBox}
                          resizeMode={'contain'}
                          source={{uri: value.file_path}}
                        />
                      </View>
                      <View
                        style={{
                          width: '25%',
                          padding: 10,
                          borderRightWidth: 1,
                          borderRightColor: '#d2d8da',
                        }}>
                        <Text style={styles.textStyle2}>
                          {value.ip_address}
                        </Text>
                      </View>
                      <View
                        style={{
                          width: '25%',
                          padding: 10,
                          borderRightWidth: 1,
                          borderRightColor: '#d2d8da',
                        }}>
                        <Text style={styles.textStyle2}>{value.file_name}</Text>
                      </View>
                      <View
                        style={{
                          width: '25%',
                          padding: 10,
                          borderRightWidth: 1,
                          borderRightColor: '#d2d8da',
                        }}>
                        <Text style={styles.textStyle2}>{value.date_time}</Text>
                      </View>
                    </View>
                  ))}
                </View>
              </View>
            </View>
          )}
          {this.state.policies != null && (
            <View style={styles.tableWrapper}>
              <View style={styles.tableinner2}>
                <View
                  style={{
                    width: '40%',
                    padding: 10,
                    borderRightWidth: 1,
                    borderRightColor: '#d2d8da',
                  }}>
                  <Text style={styles.textStyle}>Dikirim Ke</Text>
                </View>
                <View
                  style={{
                    width: '20%',
                    padding: 10,
                    borderRightWidth: 1,
                    borderRightColor: '#d2d8da',
                  }}>
                  <Text style={styles.textStyle}>Biaya</Text>
                </View>
                <View style={{width: '40%', padding: 10}}>
                  <Text style={styles.textStyle}>Barang Tambahan</Text>
                </View>
              </View>
              {this.state.policies.map((policy, index) => (
                <View key={index} style={styles.tableInner}>
                  <View
                    style={{
                      width: '40%',
                      padding: 10,
                      borderRightWidth: 1,
                      borderRightColor: '#d2d8da',
                    }}>
                    {policy.country_id == '-1' ? (
                      <Text style={styles.textStyle2}>
                        -- Ditempat Lain --{' '}
                        {policy.company_name != null ? (
                          <Text>oleh {policy.company_name}</Text>
                        ) : null}{' '}
                        Dalam {policy.duration}
                      </Text>
                    ) : (
                      <Text style={styles.textStyle2}>
                        -- {policy.country_name} --{' '}
                        {policy.company_name != null ? (
                          <Text>by {policy.company_name}</Text>
                        ) : null}{' '}
                        in {policy.duration}
                      </Text>
                    )}
                  </View>
                  <View
                    style={{
                      width: '20%',
                      padding: 10,
                      borderRightWidth: 1,
                      borderRightColor: '#d2d8da',
                    }}>
                    <Text style={styles.textStyle2}>
                      {this.state.currency_symbol} {policy.cost}
                    </Text>
                  </View>
                  <View style={{width: '40%', padding: 10}}>
                    <Text style={styles.textStyle2}>
                      {this.state.currency_symbol} {policy.additional_cost}
                    </Text>
                  </View>
                </View>
              ))}
            </View>
          )}
          {this.state.policies != null && (
            <View style={styles.shippingDescription}>
              {this.state.policies[0].shop_seller_info != null && (
                <Text style={styles.shopPolicies}>
                  {this.state.store_name}'s Kebijakan Toko
                </Text>
              )}
              {this.state.policies[0].shop_shipping_policy != null && (
                <View style={[styles.section1, styles.borderClass]}>
                  <Text style={styles.leftDescription}>Pengiriman</Text>
                  <Text style={styles.rightDescription}>
                    {this.state.policies[0].shop_shipping_policy}
                  </Text>
                </View>
              )}
              {this.state.policies[0].shop_additional_info != null && (
                <View style={styles.section1}>
                  <Text style={styles.leftDescription}>Kebijakan tambahan</Text>
                  <Text style={styles.rightDescription}>
                    {this.state.policies[0].shop_additional_info}
                  </Text>
                </View>
              )}
              {this.state.policies[0].shop_seller_info != null && (
                <View style={styles.section1}>
                  <Text style={styles.leftDescription}>Informasi Penjual</Text>
                  <Text style={styles.rightDescription}>
                    {this.state.policies[0].shop_seller_info}
                  </Text>
                </View>
              )}
            </View>
          )}
          {this.state.specifications != null && (
            <View>
              {this.state.specifications.map((spec, index) => (
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: '#d2d8da',
                    flexDirection: 'row',
                  }}
                  key={spec.attribute_id}>
                  <View
                    style={{
                      width: '30%',
                      backgroundColor: '#ccc',
                      borderRightWidth: 1,
                      borderRightColor: '#000',
                      padding: 10,
                    }}>
                    <Text>{spec.attribute_name}</Text>
                  </View>
                  <View style={{overflow: 'hidden', flex: 1}}>
                    {spec.sub_specification_details.map(
                      (spec_detail, innerIndex) => (
                        <View
                          style={{flexDirection: 'row'}}
                          key={spec_detail.sub_attribute_id}>
                          <View style={{width: '50%', padding: 10}}>
                            <Text>{spec_detail.sub_attribute_name}</Text>
                          </View>
                          <View style={{width: '50%', padding: 10}}>
                            <Text>{spec_detail.sub_attribute_value}</Text>
                          </View>
                        </View>
                      ),
                    )}
                  </View>
                </View>
              ))}
            </View>
          )}
          {this.state.compatibilityHeader != null &&
            this.state.compatibilityValues != null && (
              <View>
                <View style={styles.viewStyle}>
                  {this.state.compatibilityHeader &&
                    this.state.compatibilityValues && (
                      <View horizontal={true} style={styles.tableWrapper}>
                        <View style={styles.tableinner2}>
                          {this.state.compatibilityHeader.map(
                            (header, index) => (
                              <View
                                key={header.comp_attribute_id}
                                style={{
                                  width:
                                    '100' /
                                      this.state.compatibilityHeader.length +
                                    '%',
                                  padding: 10,
                                  borderRightWidth: 1,
                                  borderRightColor: '#d2d8da',
                                }}>
                                <Text style={styles.textStyle}>
                                  {header.comp_attribute_name}
                                </Text>
                              </View>
                            ),
                          )}
                        </View>
                        {this.state.compatibilityValues.map((value, index) => (
                          <View key={index} style={styles.tableInner}>
                            {value.existing_result_details.map(
                              (innerVal, inn) => (
                                <View
                                  key={inn}
                                  style={{
                                    width:
                                      '100' /
                                        value.existing_result_details.length +
                                      '%',
                                    padding: 10,
                                    borderRightWidth: 1,
                                    borderRightColor: '#d2d8da',
                                  }}>
                                  {innerVal.id ==
                                  this.state.compatibilityHeader[inn]
                                    .comp_attribute_id ? (
                                    <Text style={styles.textStyle2}>
                                      {innerVal.value}
                                    </Text>
                                  ) : (
                                    <Text>Tidak ada data</Text>
                                  )}
                                </View>
                              ),
                            )}
                          </View>
                        ))}
                      </View>
                    )}
                  <View style={styles.bottomDescription}>
                    <Text style={styles.test1}>
                      Produk ini didesain khusus hanya untuk 2 perangkat
                    </Text>
                    <Text style={styles.test2}>
                      Informasi kompatibilitas yang terdapat dalam tabel ini
                      telah disediakan oleh:{' '}
                      <Text style={styles.redColor}>
                        {this.state.store_name}
                      </Text>
                    </Text>
                    <Text style={styles.test3}>
                      Jika menurut Anda informasi apa pun untuk kompatibilitas
                      saat ini adalah data yang salah atau tidak lengkap, harap{' '}
                      <Text
                        onPress={() =>
                          this.props.navigation.navigate('Contact Us', {
                            _token: this.state._token,
                          })
                        }
                        style={styles.redColor}>
                        Beritahu kami
                      </Text>
                    </Text>
                  </View>
                </View>
              </View>
            )}
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  imageBox: {
    height: hp('15%'),
  },
  titleStyle: {
    color: '#545454',
    fontSize: wp('4.8%'),
    paddingBottom: 0,
  },
  viewStyle: {
    marginleft: 5,
    marginRight: 5,
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 90,
  },
  dropdownClass: {
    elevation: 1,
    borderRadius: 50,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  pickerClass: {
    height: hp('6%'),
  },
  dropheadingClass: {
    fontSize: wp('4%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  headTxt: {
    color: '#979797',
    fontSize: wp('4%'),
    marginBottom: 15,
  },
  bottomDescription: {
    paddingTop: 20,
  },
  test1: {
    color: '#979797',
    fontSize: wp('4.2%'),
    textAlign: 'center',
    marginBottom: 25,
    fontFamily: Font.RobotoRegular,
  },
  test2: {
    color: '#454444',
    fontSize: wp('4.2%'),
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: Font.RobotoRegular,
  },
  test3: {
    color: '#a1a1a1',
    fontSize: wp('4%'),
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: Font.RobotoRegular,
  },
  redColor: {
    color: '#c90305',
  },
  shippingWrapper: {
    padding: 16,
  },
  tableWrapper: {
    borderWidth: 1,
    borderColor: '#d2d8da',
  },
  textStyle: {
    color: '#545353',
    fontFamily: Font.RobotoRegular,
  },
  textStyle2: {
    color: '#545353',
    fontFamily: Font.RobotoRegular,
  },
  shopPolicies: {
    color: ' #000000',
    fontFamily: Font.RobotoRegular,
    marginTop: 20,
    marginBottom: 20,
    fontSize: hp('2%'),
  },
  leftDescription: {
    color: '#DC0028',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
    width: '30%',
  },
  rightDescription: {
    color: '#585757',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
    width: '70%',
    paddingLeft: 15,
  },
  section1: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#d2d8da',
    paddingBottom: 10,
    marginBottom: 10,
  },
  borderClass: {
    borderTopWidth: 1,
    borderTopColor: '#d2d8da',
    paddingTop: 10,
  },
  tableInner: {
    flexDirection: 'row',
    borderBottomColor: '#d2d8da',
    borderBottomWidth: 1,
  },
  tableinner2: {
    borderBottomColor: '#d2d8da',
    borderBottomWidth: 1,
    backgroundColor: '#f1f0f0',
    flexDirection: 'row',
  },
};
