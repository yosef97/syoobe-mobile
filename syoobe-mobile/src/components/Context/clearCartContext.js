import React from 'react';

const clearCartContext = React.createContext({
  clearCart: () => {},
});

export default clearCartContext;
