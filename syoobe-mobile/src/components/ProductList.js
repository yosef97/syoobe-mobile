import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {ProductFilterSort, ProductListFeature} from './product';
import HomePageLogo from './HomePageLogo';
import HomePageRightIcons from './HomePageRightIcons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Spinner} from './common';
import {getProductsByCategory} from '../actions';
import {connect} from 'react-redux';

class ProductList extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
    const category_id = this.props.route.params.category_id;
    this.props.getProductsByCategory({category_id});
  };

  static navigationOptions = ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
    headerTitle: (
      <TouchableOpacity
        style={{
          width: wp('33%'),
          height: wp('4.5%'),
          marginLeft: 'auto',
          marginRight: 'auto',
        }}>
        <HomePageLogo />
      </TouchableOpacity>
    ),

    headerRight: <HomePageRightIcons {...navigation} />,
  });

  renderProducts = () => {
    if (this.props.products) {
      return this.props.products.map((product, index) => (
        <ProductListFeature key={index} {...this.props} details={product} />
      ));
    }
  };

  render() {
    if (this.props.loading === true) {
      return <Spinner color={'red'} />;
    }
    return (
      <View>
        <ProductFilterSort />
        <View style={styles.abc}>{this.renderProducts()}</View>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    backgroundColor: '#f00',
    width: '100%',
  },
  banner_top: {
    Width: '100%',
  },
  heading: {
    borderWidth: 1,
    borderColor: '#f00',
    height: 80,
    width: '100%',
  },
  bannerImg: {
    width: '100%',
    height: 150,
  },
  bottomSection: {
    borderColor: '#e7e7e7',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 30,
  },

  abc: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    // borderWidth:1,
    marginLeft: 10,
    marginRight: 10,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderColor: '#e7e7e7',
    //    justifyContent:'center'
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.category.loading,
    products: state.category.products,
  };
};

export default connect(mapStateToProps, {getProductsByCategory})(ProductList);
