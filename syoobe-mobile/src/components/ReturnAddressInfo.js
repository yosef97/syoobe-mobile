import React, {Component} from 'react';
import {AsyncStorage, View, ScrollView} from 'react-native';
import {Card, CardSection, FooterButton} from './common';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {connect} from 'react-redux';
import {
  saveAddressInfo,
  changeSellerFullName,
  changeAddressLine1,
  changeAddressLine2,
  changeSellerCity,
  changeSellerZipCode,
  changeSellerPhoneNumber,
  getCountries,
  getStates,
  getAddressInfo,
} from '../actions';
import Loader from './Loading';
import {showToast} from '../helpers/toastMessage';
import {
  resetAdress,
  setSelectedProvince,
  setSelectedCity,
  setSelectedDistrict,
  setComponentRedirect,
} from '../actions/AdressAction';
import InputText from './common/InputText';

class ReturnAddressInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _token: null,
      ura_name: props.sellerInfo.ura_name,
      ura_address_line_1: props.sellerInfo.ura_address_line_1,
      ura_address_line_2: props.sellerInfo.ura_address_line_2,
      ura_zip: props.sellerInfo.ura_zip,
      ura_phone: props.sellerInfo.ura_phone,
    };
  }

  componentDidMount() {
    this.fetchData();
    setTimeout(() => {
      this.setSelectedAdress();
      this.setField();
    }, 2000);
  }

  setField = () => {
    this.setState({
      ura_name: this.props.sellerInfo.ura_name,
      ura_address_line_1: this.props.sellerInfo.ura_address_line_1,
      ura_address_line_2: this.props.sellerInfo.ura_address_line_2,
      ura_zip: this.props.sellerInfo.ura_zip,
      ura_phone: this.props.sellerInfo.ura_phone,
    });
  };

  // shouldComponentUpdate(nextProps, nextState) {
  //   if (nextProps.sellerInfo.ura_name !== nextState.ura_name) {
  //     if (this.state._token) {
  //       this.props.getAddressInfo(this.state._token);
  //     }
  //   }
  //   return false;
  // }

  fetchData = async () => {
    await this.retrieveToken()
      .then((_token) => {
        this.props.getAddressInfo(_token);
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  setSelectedAdress = () => {
    this.props.setSelectedProvince(
      {
        province: this.props.sellerInfo.ura_state_name,
        province_id: this.props.sellerInfo.ura_state,
      },
      this.props.navigation,
      true,
    );
    this.props.setSelectedCity(
      {
        city_name: this.props.sellerInfo.ura_city,
        city_id: this.props.sellerInfo.ura_city_id,
      },
      this.props.navigation,
      true,
    );
    this.props.setSelectedDistrict(
      {
        subdistrict_name: this.props.sellerInfo.ura_district,
        subdistrict_id: this.props.sellerInfo.ura_district_id,
      },
      this.props.navigation,
      true,
    );
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({_token: userToken});
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  handleChangeText = (text, type) => {
    console.log(text);
    this.setState({
      ...this.state,
      [type]: text,
    });
  };

  saveReturnAddressInfo = async () => {
    const {
      sellerInfo,
      selectedCity,
      selectedProvince,
      selectedDistrict,
      saveAddressInfo,
      navigation,
      resetAdress,
    } = this.props;
    if (!this.state.ura_name) {
      showToast({
        message: 'Silakan masukkan Nama Lengkap Anda!',
      });
    } else if (!this.state.ura_address_line_1) {
      showToast({
        message: 'Silakan masukkan alamat!',
      });
    } else if (!selectedProvince.province_id) {
      showToast({
        message: 'Provinsi tidak boleh kosong!',
      });
    } else if (!selectedCity.city_id) {
      showToast({
        message: 'Kota/Kabupaten tidak boleh kosong!',
      });
    } else if (!selectedDistrict.subdistrict_id) {
      showToast({
        message: 'Kecamatan tidak boleh kosong!',
      });
    } else if (!this.state.ura_zip) {
      showToast({
        message: 'Silakan masukkan Kode Pos!',
      });
    } else if (!this.state.ura_phone) {
      showToast({
        message: 'Silakan masukkan nomor telepon!',
      });
    } else {
      await this.retrieveToken()
        .then((_token) => {
          let details = {
            _token: _token,
            ura_name: this.state.ura_name,
            ura_address_line_1: this.state.ura_address_line_1,
            ura_address_line_2: this.state.ura_address_line_2,
            ura_city: selectedCity.city_name || sellerInfo.ura_city,
            ura_city_id: selectedCity.city_id || sellerInfo.ura_city_id,
            ura_state: selectedProvince.province_id || sellerInfo.ura_state,
            ura_state_name:
              selectedProvince.province || sellerInfo.ura_state_name,
            ura_district:
              selectedDistrict.subdistrict_name || sellerInfo.ura_district,
            ura_district_id:
              selectedDistrict.subdistrict_id || sellerInfo.ura_district_id,
            ura_zip: this.state.ura_zip,
            ura_phone: this.state.ura_phone,
          };
          setSelectedProvince(
            {
              province: sellerInfo.ura_state_name,
              province_id: sellerInfo.ura_state,
            },
            navigation,
            true,
          );
          setSelectedCity(
            {city_name: sellerInfo.ura_city, city_id: sellerInfo.ura_city_id},
            navigation,
            true,
          );
          setSelectedDistrict(
            {
              subdistrict_name: sellerInfo.ura_district,
              subdistrict_id: sellerInfo.ura_district_id,
            },
            navigation,
            true,
          );
          saveAddressInfo(navigation, details);
          resetAdress();
        })
        .catch(() => {
          resetAdress();
        });
    }
  };

  render() {
    console.log('ini state', JSON.stringify(this.state));
    console.log('ini props', JSON.stringify(this.props));

    const {
      selectedProvince,
      selectedCity,
      selectedDistrict,
      navigation,
      sellerInfo,
    } = this.props;

    if (this.props.loading) {
      return <Loader />;
    }
    // console.log(this.props);
    return (
      <View style={{flex: 1}}>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          style={styles.scrollClass}>
          <Card>
            <CardSection>
              <InputText
                placeholder="Masukkan Nama lengkap"
                label="Nama lengkap *"
                onChangeText={(e) => this.handleChangeText(e, 'ura_name')}
                value={this.state.ura_name}
              />
            </CardSection>
            <CardSection>
              <InputText
                label="Alamat Utama *"
                placeholder="Masukkan Alamat Utama"
                multiline={true}
                numberOfLines={4}
                onChangeText={(e) =>
                  this.handleChangeText(e, 'ura_address_line_1')
                }
                value={this.state.ura_address_line_1}
              />
            </CardSection>
            <CardSection>
              <InputText
                onChangeText={(e) =>
                  this.handleChangeText(e, 'ura_address_line_2')
                }
                label="Alamat Tambahan *"
                placeholder="Masukkan Alamat Tambahan"
                multiline={true}
                numberOfLines={4}
                value={
                  this.state.ura_address_line_2 === 'null'
                    ? ''
                    : this.state.ura_address_line_2
                }
              />
            </CardSection>

            <CardSection>
              <InputText
                placeholder="Masukkan Nomor Telepon"
                label="Nomor Telepon *"
                keyboardType={'phone-pad'}
                onChangeText={(e) => this.handleChangeText(e, 'ura_phone')}
                value={this.state.ura_phone}
              />
            </CardSection>
            <CardSection>
              <InputText
                value={selectedProvince.province || sellerInfo.ura_state_name}
                onFocus={() => {
                  this.props.setComponentRedirect('ReturnAddressInfo');
                  navigation.navigate('Province');
                }}
                placeholder={'Pilih Provinsi'}
                label="Provinsi *"
                icon={'chevron-down'}
              />
            </CardSection>
            <CardSection>
              <InputText
                value={selectedCity.city_name || sellerInfo.ura_city}
                onFocus={() =>
                  navigation.navigate(
                    selectedProvince.province ? 'City' : 'Province',
                    {
                      province_id:
                        selectedProvince.province_id || sellerInfo.ura_state,
                    },
                  )
                }
                label="Kota/Kabupaten *"
                placeholder="Pilih Kota/Kabupaten"
                icon={'chevron-down'}
              />
            </CardSection>
            <CardSection>
              <InputText
                value={
                  selectedDistrict.subdistrict_name || sellerInfo.ura_district
                }
                onFocus={() =>
                  navigation.navigate(
                    selectedCity.city_name ? 'District' : 'Province',
                    {
                      city_id:
                        selectedCity.city_id || sellerInfo.ura_district_id,
                    },
                  )
                }
                label="Kecamatan *"
                placeholder="Pilih Kecamatan"
                icon={'chevron-down'}
              />
            </CardSection>
            <CardSection>
              <InputText
                placeholder="Masukkan Kode Pos"
                label="Kode Pos *"
                keyboardType={'number-pad'}
                onChangeText={(e) => this.handleChangeText(e, 'ura_zip')}
                value={this.state.ura_zip}
              />
            </CardSection>
          </Card>
        </ScrollView>

        <FooterButton onPress={() => this.saveReturnAddressInfo()}>
          Simpan Perubahan
        </FooterButton>
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: wp('4%'),
    alignSelf: 'center',
    // color: 'red'
  },

  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
  },
  confirmMsg: {
    color: '#8f8f8f',
    fontSize: wp('4%'),
    fontFamily: Font.RobotoLight,
    textAlign: 'center',
    paddingTop: 10,
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  dropdownClass: {
    // elevation: 1,
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
    height: hp('6%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  pickerClass: {},
};

const mapStateToProps = (state) => {
  return {
    loading: state.profile.loading,
    sellerInfo: state.profile.sellerInfo,
    selectedProvince: state.country.province.selectedProvince,
    selectedCity: state.country.city.selectedCity,
    selectedDistrict: state.country.district.selectedDistrict,
  };
};

export default connect(mapStateToProps, {
  saveAddressInfo,
  changeSellerFullName,
  changeAddressLine1,
  changeAddressLine2,
  changeSellerCity,
  changeSellerZipCode,
  changeSellerPhoneNumber,
  getAddressInfo,
  getCountries,
  getStates,
  resetAdress,
  setSelectedProvince,
  setSelectedCity,
  setSelectedDistrict,
  setComponentRedirect,
})(ReturnAddressInfo);
