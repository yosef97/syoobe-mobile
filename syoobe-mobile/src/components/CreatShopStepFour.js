import React, {Component} from 'react';
import {
  StatusBar,
  AsyncStorage,
  Text,
  Image,
  View,
  ScrollView,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {CardSection, Spinner, Button} from './common';
import {Font} from './Font';
import {connect} from 'react-redux';
import {
  saveShopDetails,
  setMetaTagTitle,
  setMetaTagKeywords,
  setMetaTagDescription,
} from '../actions';
import InputText from './common/InputText';

class CreatShopStepFour extends Component {
  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  metaTagTitle = (text) => {
    this.props.setMetaTagTitle(text);
  };

  metaTagKeywords = (text) => {
    this.props.setMetaTagKeywords(text);
  };

  metaTagDescription = (text) => {
    this.props.setMetaTagDescription(text);
  };

  handleSaveShopDetails = async () => {
    const {
      navigation,
      shop_name,
      url_keywords,
      description,
      shop_city,
      country_id,
      state_id,
      logo,
      banner,
      announcement,
      message_to_buyer,
      is_prod_tax,
      shop_vendor_display_status,
      welcome_message,
      payment_policy,
      delivery_policy,
      refund_policy,
      additional_info,
      seller_info,
      shop_npwp,
      meta_tag_title,
      meta_tag_keywords,
      meta_tag_description,
      shop_id,
      shop_state_name,
      shop_district_id,
      shop_district_name,
      shop_city_id,
    } = this.props;
    if (meta_tag_title === '') {
      Alert.alert('Informasi', 'Tambahkan judul meta tag', [{text: 'OK'}]);
    } else if (meta_tag_keywords === '') {
      Alert.alert('Informasi', 'Tambahkan keyword', [{text: 'OK'}]);
    } else if (meta_tag_description === '') {
      Alert.alert('Informasi', 'Tambahkan deskripsi meta tag', [{text: 'OK'}]);
    } else {
      await this.retrieveToken().then((_token) => {
        this.props.saveShopDetails(
          navigation,
          _token,
          shop_name,
          url_keywords,
          description,
          shop_city,
          logo,
          banner,
          country_id,
          state_id,
          announcement,
          message_to_buyer,
          is_prod_tax,
          shop_vendor_display_status,
          welcome_message,
          payment_policy,
          delivery_policy,
          refund_policy,
          additional_info,
          seller_info,
          shop_npwp,
          meta_tag_title,
          meta_tag_keywords,
          meta_tag_description,
          shop_id,
          shop_state_name,
          shop_district_id,
          shop_district_name,
          shop_city_id,
        );
      });
    }
  };

  render() {
    console.log(
      'ini props shop_vendor_display_status',
      JSON.stringify(this.props.shop_vendor_display_status),
    );
    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color="red" size="large" />
        </View>
      );
    }

    return (
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        style={styles.scrollClass}>
        <Text style={styles.shopHeading}>Informasi Toko</Text>
        <View style={styles.processImgWrapper}>
          <Image
            style={styles.processImg}
            source={require('../images/processImg3.png')}
          />
        </View>

        <View style={styles.imgBtnWrapper}>
          <Text style={styles.doneTxt}>Info & Penampilan</Text>
          <Text style={styles.doneTxt}>Kebijakan Toko</Text>
          <Text style={[styles.doneTxt, styles.completeColor]}>
            Belanja Info SEO
          </Text>
        </View>

        <View style={styles.viewStyle}>
          <CardSection>
            <InputText
              value={this.props.meta_tag_title}
              onChangeText={this.metaTagTitle.bind(this)}
              placeholder=""
              label="Nama Marek"
            />
          </CardSection>
          <CardSection>
            <InputText
              value={this.props.meta_tag_keywords}
              onChangeText={this.metaTagKeywords.bind(this)}
              label="Kata Kunci Meta Tag"
              multiline
              numberOfLines={4}
            />
          </CardSection>
          {/* <Text style={ styles.errorTextStyle}></Text> */}

          <CardSection>
            <InputText
              value={this.props.meta_tag_description}
              onChangeText={this.metaTagDescription.bind(this)}
              label="Deskripsi Tag Meta"
              multiline
              numberOfLines={4}
            />
          </CardSection>

          <View style={styles.btnWrapper}>
            <Button onPress={() => this.handleSaveShopDetails()}>
              Simpan perubahan
            </Button>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: wp('4%'),
    alignSelf: 'center',
    // color: 'red',
    fontFamily: Font.RobotoRegular,
  },

  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
    // padding:10
  },
  shopHeading: {
    fontSize: wp('6%'),
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 15,
    color: '#696969',
    fontFamily: Font.RobotoRegular,
  },
  processImgWrapper: {
    paddingLeft: wp('7%'),
    marginRight: 5,
  },
  processImg: {
    // width:'100%',
    width: wp('86%'),
    height: wp('15.5%'),
  },
  imgBtnWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderWidth:1,
    // borderColor:'#f00',
    width: '100%',
    flexWrap: 'wrap',
    marginBottom: 15,
    marginTop: 8,
  },
  doneTxt: {
    width: wp('28%'),
    fontSize: wp('4%'),
    color: '#969696',
    fontFamily: Font.RobotoMedium,
    // justifyContent:'center',
    // alignItems:'center',
    textAlign: 'center',
  },
  completeColor: {
    color: '#424242',
  },
  viewStyle: {
    marginleft: 5,
    marginRight: 5,
    paddingLeft: 10,
    paddingRight: 10,
  },
  bottom_Text: {
    marginTop: 10,
    color: '#8f8f8f',
    fontFamily: Font.RobotoLight,
    fontSize: wp('4%'),
    paddingLeft: 10,
  },
  btnWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 0,
    paddingBottom: 15,
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.createShop.loading,
    shop_name: state.createShop.shop_name,
    url_keywords: state.createShop.url_keywords,
    description: state.createShop.description,
    shop_city: state.createShop.shop_city,
    country_id: state.createShop.country_id,
    state_id: state.createShop.state_id,
    logo: state.createShop.logo,
    banner: state.createShop.banner,
    announcement: state.createShop.announcement,
    message_to_buyer: state.createShop.message_to_buyer,
    is_prod_tax: state.createShop.is_prod_tax,
    shop_vendor_display_status: state.createShop.shop_vendor_display_status,
    welcome_message: state.createShop.welcome_message,
    payment_policy: state.createShop.payment_policy,
    delivery_policy: state.createShop.delivery_policy,
    refund_policy: state.createShop.refund_policy,
    additional_info: state.createShop.additional_info,
    seller_info: state.createShop.seller_info,
    shop_npwp: state.createShop.shop_npwp,
    meta_tag_title: state.createShop.meta_tag_title,
    meta_tag_keywords: state.createShop.meta_tag_keywords,
    meta_tag_description: state.createShop.meta_tag_description,
    message: state.createShop.message,
    shop_state_name: state.createShop.shop_state_name,
    shop_district_id: state.createShop.shop_district_id,
    shop_district_name: state.createShop.shop_district_name,
    shop_city_id: state.createShop.shop_city_id,
    shop_id: state.createShop.shop_id,
  };
};

export default connect(mapStateToProps, {
  saveShopDetails,
  setMetaTagTitle,
  setMetaTagKeywords,
  setMetaTagDescription,
})(CreatShopStepFour);
