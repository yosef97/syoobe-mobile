import React from 'react';
import {Component} from 'react';
import {View, ScrollView, Text, TouchableOpacity} from 'react-native';
import {Font} from './Font';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DashboardSection from './DashboardSection';
import {getDashboardDetails} from '../actions';
import {connect} from 'react-redux';
import ActionButton from 'react-native-action-button';

class DashboardSectionSeller extends Component {
  render() {
    console.log('ini state 313131', JSON.stringify(this.props));
    // console.log('ini props', JSON.stringify(this.props));

    if (this.props.sellerData) {
      var obj = this.props.sellerData;
      var initial = Object.entries(obj).map(([k, v]) => [k, v]);
      var first = initial.slice(0, 1);
      var second = initial.slice(2, 3);
      var res = [];
      res.push(first[0], second[0]);
      return (
        <View style={styles.scrollView}>
          <View style={styles.accountBalance}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('MyWallet')}>
              <Text style={styles.accountTxt}>
                {' '}
                {'Saldo rekening'.toUpperCase()}
              </Text>
              <Text style={styles.accountPrice}>
                {this.props.sellerData.account_balance}
              </Text>
            </TouchableOpacity>

            <View style={styles.accountBottom}>
              <View style={styles.totalPrice}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Sales')}>
                  <Text style={styles.priceHeading}>
                    {' '}
                    {'Total Produk'.toUpperCase()}
                  </Text>
                  <Text style={styles.productQnt}>
                    {this.props.sellerData.publishedItems}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.soldQnt}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Sales')}>
                  <Text style={styles.priceHeading}>
                    {' '}
                    {'Jumlah Terjual '.toUpperCase()}
                  </Text>
                  <Text style={styles.productQnt}>
                    {this.props.sellerData.tota_sold_quantity}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={styles.boxWrapper}>
            {res.map((box, index) => (
              <DashboardSection
                navigation={this.props.navigation}
                key={index}
                box={box}
                route={this.props.route}
              />
            ))}
          </View>
          <ActionButton
            buttonColor="rgba(231,76,60,1)"
            onPress={() => this.props.navigation.navigate('AddProduct')}
          />
        </View>
      );
    }
    return null;
  }
}

const styles = {
  boxWrapper: {
    // paddingHorizontal:15
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 5,
    paddingRight: 5,
    flexWrap: 'wrap',
  },
  scrollView: {
    paddingTop: 15,
    backgroundColor: '#fff',

    height: hp(60),
    paddingHorizontal: 10,
  },

  accountBalance: {
    elevation: 1,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'transparent',
    marginBottom: 15,
    height: wp(40),
    padding: wp(10),
  },
  accountTxt: {
    color: '#848484',
    textTransform: 'uppercase',
    fontSize: wp(3.5),
    fontFamily: Font.RobotoRegular,
  },
  accountPrice: {
    color: '#00b3ff',
    fontSize: wp(5),
  },
  accountBottom: {
    flexDirection: 'row',
    marginTop: 10,
  },
  totalPrice: {
    paddingRight: 15,
    borderRightWidth: 1,
    borderColor: '#d8d8d8',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: Font.RobotoMedium,
  },
  soldQnt: {
    paddingLeft: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  productQnt: {
    color: '#636363',
    fontSize: wp('6%'),
  },
};

const mapStateToProps = (state) => {
  return {
    sellerData: state.dashboard.sellerData,
  };
};

export default connect(mapStateToProps, {getDashboardDetails})(
  DashboardSectionSeller,
);
