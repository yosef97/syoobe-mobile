import axios from 'axios';
import React, {Component} from 'react';
import {
  Alert,
  AsyncStorage,
  Image,
  ScrollView,
  Text,
  TouchableHighlight,
  View,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {
  getAddressPayment,
  getCountries,
  setAmount,
  getWalletDetails,
  setValue,
} from '../actions';
import {Spinner} from './common';
import {Font} from './Font';
import {countNumber, getCompanyName, substr} from '../helpers/helper';
import {formatRupiah} from '../helpers/helper';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {ShippingOptions} from './common/payment/ShippingOptions';
import {PaymentOptions} from './common/payment/PaymentOptions';
import PTRView from 'react-native-pull-to-refresh';
import {
  DATA_TO_SEND,
  DELIVERY_CHARGE,
  SALES_TAX,
  SET_COST,
  SET_SELECTED_SHIPPING_OPTION_SERVICE,
  SET_SHIPPING_OPTIONS,
  SHIPPING_SUMMARY,
  TOTAL_PRICE,
} from '../actions/types';
import Bugsnag from '@bugsnag/react-native';

class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _token: null,
      checked: 'first',
      modalVisible: false,
      fullName: '',
      phoneNumber: '',
      addressLine1: '',
      addressLine2: '',
      city: '',
      zipCode: '',
      selectedCountry: 0,
      selectedState: 0,
      shippingSummary: [],
      shippingCompanies: [],
      shippingOptions: [],
      selectedShippingOptionService: [],
      price_currency: null,
      total_price: props.payable_amount,
      shippingCompanySelected: [],
      sales_tax: 0,
      total_tax: 0,
      total_cost: 0,
      deliveryCharge: 0,
      dataToSend: [],
      len: null,
      loading: false,
      isLoading: false,
      pickOngkir: null,
      totalShipping: 0,
      responseApiSkip: null,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
    return;
  };

  fetchShipping = async (details) => {
    console.log('fetchShipping');
    this.setState({
      loading: true,
      deliveryCharge: 0,
    });
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/shippingSummaryAPIForIndonesia', formBody)
      .then((response) => {
        if (response.data.status == 1) {
          let productData = [];
          let newPickerArray = [];
          let selectedOption = [];
          response.data.product_data.map((item) => {
            let mainObj = {...item};
            if (item.shipping_methods != null) {
              let product_total = (mainObj.product_total = countNumber(
                mainObj.product_total,
                item.selected_shipping?.pship_charges,
              ));
              console.log('product_total', product_total);

              newPickerArray.push({...mainObj});
            }
          });
          response.data.product_data.map((item, index) => {
            if (item.shipping_methods != null) {
              let obj = {
                product_id: item.id,
                pship_id: item.selected_shipping_option,
                toBeRemoved: false,
              };
              if (item.selected_shipping_option != 0) {
                let addDelCharge = parseFloat(this.state.deliveryCharge);
                item.shipping_methods.map((ship, indexin) => {
                  if (ship.charge_amount && ship.charge_amount != null) {
                    addDelCharge += parseFloat(ship.charge_amount);
                  }
                  // obj = {...obj, ...{"delivery_charge": ship.charge_amount}}
                });

                if (item.selected_shipping) {
                  let initialValue = {
                    product_id: item.id,
                    item: {
                      pship_duration: item.selected_shipping.pship_duration,
                      pship_id: item.selected_shipping.pship_id,
                      pship_service: item.selected_shipping.scompany_services,
                      pship_service_id: item.selected_shipping.pship_service,
                      pship_value: formatRupiah(
                        item.selected_shipping?.pship_charges,
                        false,
                      ),
                    },
                    value: item.selected_shipping.pship_id,
                    company_id: item.selected_shipping.scompany_id,
                  };

                  selectedOption.push(initialValue);
                }

                this.setState((state) => ({
                  deliveryCharge: addDelCharge,
                  // selectedShippingOptionService: [
                  //   ...state.selectedShippingOptionService,
                  //   initialValue,
                  // ],
                  total_cost: countNumber(
                    state.total_cost,
                    item.selected_shipping?.pship_charges,
                  ),
                }));
                let cost = countNumber(
                  this.state.total_cost,
                  item.selected_shipping?.pship_charges,
                );
                this.props.setValue(SET_COST, cost);
              } else {
                let opt = {
                  product_id: item.id,
                  item: null,
                  value: 0,
                  company_id: 0,
                };
                selectedOption.push(opt);
              }
              productData.push(obj);
            } else {
              let obj = {
                product_id: item.id,
                pship_id: null,
                toBeRemoved: true,
              };

              productData.push(obj);
            }
          });

          // item.duration ? item.company_name ? item.company_name+" - "+item.duration+"(+"+this.state.price_currency+" "+item.charge_amount+")": item.duration+"(+"+this.state.price_currency+" "+item.charge_amount+")" : item.company_name}
          this.setState((state) => ({
            shippingSummary: newPickerArray,
            // shippingSummary: response.data.product_data,
            selectedShippingOptionService: selectedOption,
            dataToSend: productData,
            price_currency: response.data.price_currency,
            total_price: response.data.new_cart_product_price_total,
            sales_tax: response.data.sales_tax_data,
            loading: false,
            responseApiSkip: response.data.status,
          }));
          this.props.setValue(SET_COST, 0);
          this.props.setValue(DATA_TO_SEND, productData);
          this.props.setValue(
            SET_SELECTED_SHIPPING_OPTION_SERVICE,
            selectedOption,
          );
          this.props.setValue(
            TOTAL_PRICE,
            response.data.new_cart_product_price_total,
          );
          this.props.setValue(SALES_TAX, response.data.sales_tax_data);
          this.props.setValue(SHIPPING_SUMMARY, newPickerArray);
        } else {
          this.setState({
            loading: false,
          });
        }
      });
  };

  refresh = () => {
    this.retrieveToken().then((_token) => {
      let details = {
        _token: _token,
      };
      this.props.getAddressPayment(details);
      this.props.getWalletDetails(details);
      this.fetchShipping(details);
    });
  };

  componentDidMount = async () => {
    console.log('componentDidMount');
    await this.retrieveToken().then((_token) => {
      let details = {
        _token: _token,
      };

      this.fetchShipping(details);
      this.props.getAddressPayment(details);
      this.props.getWalletDetails(details);
    });
  };

  saveShippingSummary = async ({ref}) => {
    let proceed = true;
    let remove = false;
    await this.state.dataToSend.filter((product) => {
      if (product.pship_id === 0) {
        proceed = false;
      } else if (product.toBeRemoved) {
        proceed = false;
        remove = true;
      }
    });
    if (proceed === true) {
      if (this.props.shipping === null || this.props.billing === null) {
        Alert.alert(
          'Informasi',
          'Silahkan pilih atau tambahkan alamat pengiriman.',
          [{text: 'OK'}],
        );
      } else if (
        this.props.shipping_address === null ||
        this.props.shipping_address.city_id === null ||
        this.props.shipping_address.district_id === null
      ) {
        Alert.alert(
          'Informasi',
          'Silahkan pilih atau tambahkan alamat pengiriman.',
          [{text: 'OK'}],
        );
      } else {
        // this.setState({
        //   loading: true,
        // });
        let details = {
          _token: this.state._token,
          shipping_locations: this.state.dataToSend,
        };
        var formBody = [];
        for (var property in details) {
          if (property == 'shipping_locations') {
            let array = [];
            for (var i = 0; i < details[property].length; i++) {
              array.push(details[property][i]);
            }
            formBody.push(property + '=' + JSON.stringify(array));
          } else {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + '=' + encodedValue);
          }
        }
        formBody = formBody.join('&');
        // console.log(formBody);
        axios
          .post(
            'https://syoobe.co.id/api/saveShippingAddressDetailsAPI',
            formBody,
          )
          .then((response) => {
            // console.log('res', response);
            this.setState({
              loading: false,
            });
            if (response.data.status == 1) {
              ref.open();
              // this.props.navigation.navigate(route, {
              //   data: response.data,
              //   _token: this.state._token,
              // });
            }
          });
      }
    } else if (remove) {
      Alert.alert(
        'Informasi',
        'Barang yang kamu pilih tidak bisa dikirim ke alamat yang kamu pilih, gunakan alamat lain atau hapus dari keranjang untuk melanjutkan',
        [{text: 'OK'}],
      );
    } else {
      Alert.alert('Informasi', 'Silahkan pilih metode pengiriman', [
        {text: 'OK'},
      ]);
    }
  };

  toPayment = (route, params) => {
    this.setState({
      loading: true,
    });
    let details = {
      _token: this.state._token,
    };

    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    axios
      .post('https://syoobe.co.id/api/finalPaymentSummary', formBody)
      .then((response) => {
        this.setState({
          loading: false,
        });
        console.log('response final pay', response.data);
        if (response.data.status === 1) {
          this.props.navigation.replace(route, {
            data: response.data,
            _token: this.state._token,
            ...params,
          });
        } else {
          Alert.alert('Informasi', response.data.msg, [{text: 'OK'}]);
        }
      });
  };

  onSetShipping = async (props) => {
    const {
      index,
      value,
      item,
      product_id,
      company_id,
      refParent,
      refChild,
    } = props;
    const {
      selectedShippingOptionService,
      total_price,
      shippingSummary,
      dataToSend,
    } = this.props.shipping;
    await this.props.setValue(SET_COST, 0);
    let dataProduct = [...shippingSummary];
    dataProduct[index].product_total = countNumber(
      dataProduct[index].product_total,
      item.pship_value,
    );
    let total_prices = countNumber(total_price, item.pship_value);

    this.props.setValue(DELIVERY_CHARGE, 0);
    this.props.setValue(TOTAL_PRICE, total_prices);
    this.props.setValue(SHIPPING_SUMMARY, dataProduct);
    this.props.setValue(
      SET_COST,
      countNumber(this.props.shipping.total_cost, item.pship_value),
    );

    this.props.setAmount(
      countNumber(this.props.payable_amount, item.pship_value),
    );
    let initialValue = {product_id, item, value, company_id};
    let data = [...dataToSend];
    let optionsService = [...selectedShippingOptionService];
    if (data[index].product_id === product_id) {
      data[index].pship_id = value;
      const selectedValue = selectedShippingOptionService.filter(
        (val) => val.product_id !== data[index].product_id,
      );
      let pship_value =
        selectedShippingOptionService[index].item?.pship_value || 0;
      let newDataProduct = [...shippingSummary];
      newDataProduct[index].product_total =
        newDataProduct[index].product_total - pship_value;

      optionsService[index].item = item;
      optionsService[index].company_id = company_id;
      optionsService[index].value = value;
      this.props.setValue(DATA_TO_SEND, data);
      this.props.setValue(SET_SELECTED_SHIPPING_OPTION_SERVICE, optionsService);
      this.props.setValue(
        TOTAL_PRICE,
        this.props.shipping.total_price - pship_value,
      );

      this.props.setValue(SHIPPING_SUMMARY, newDataProduct);
      this.props.setAmount(this.props.payable_amount - pship_value);
    } else {
      this.props.setValue(SET_SELECTED_SHIPPING_OPTION_SERVICE, [
        ...selectedShippingOptionService,
        initialValue,
      ]);
      // await this.setState({
      //   selectedShippingOptionService: [
      //     ...selectedShippingOptionService,
      //     initialValue,
      //   ],
      // });
    }

    // console.log('item', [...selectedShippingOptionService, initialValue]);
    refChild.close();
    refParent.close();
  };

  getShippingData = async (
    service_name,
    index,
    product_id,
    service,
    ref,
    refChild,
  ) => {
    const {
      shippingOptions,
      selectedShippingOptionService,
    } = this.props.shipping;
    this.setState({isLoading: true});

    const shipping = shippingOptions.filter(
      (val) => val.product_id === product_id,
    );

    const checkServiceAvailable = selectedShippingOptionService.filter(
      (val) => val.product_id === product_id,
    );

    if (shipping.length > 0) {
      if (checkServiceAvailable.length > 0) {
        this.setState({isLoading: false});
        refChild.open();
      }
    }

    let details = {
      _token: this.state._token,
      service_name: service_name,
      shipping_services_id: service,
      product_id: product_id,
      weight_class_id: this.props.shipping.shippingSummary[0]?.product
        .weight_class_id,
      weight: this.props.shipping.shippingSummary[0]?.product.weight,
      shop_id: this.props.shipping.shippingSummary[0]?.product.shop_id,
      ua_district_id: this.props.shipping.shippingSummary[0]?.product
        .shipping_address.ua_district_id,
    };

    // console.log('ini detail', details);
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/getShippingRatesNew', formBody)
      .then((response) => {
        // console.log('ini res get ship', JSON.stringify(response.data));
        if (response.data.status == 1) {
          const newShippingOptions = {
            product_id: response.data.product_id,
            options: response.data.options,
            company_id: response.data.company_id,
          };

          // console.log(newShippingOptions);
          let data = [...shippingOptions];
          if (data.length > 0) {
            const newData = data.filter((item) => {
              if (item.product_id === response.data.product_id) {
                item.options = response.data.options;
                item.company_id = response.data.company_id;
                const oldData = shippingOptions.filter(
                  (dat) => dat.product_id !== product_id,
                );
                // this.setState({
                //   shippingOptions: [...oldData, item],
                // });
                this.props.setValue(SET_SHIPPING_OPTIONS, [...oldData, item]);
              } else {
                const oldData = shippingOptions.filter(
                  (dat) => dat.product_id !== product_id,
                );
                // this.setState({
                //   shippingOptions: [...oldData, newShippingOptions],
                // });
                this.props.setValue(SET_SHIPPING_OPTIONS, [
                  ...oldData,
                  newShippingOptions,
                ]);
              }
            });
          } else {
            this.props.setValue(SET_SHIPPING_OPTIONS, [
              ...shippingOptions,
              newShippingOptions,
            ]);
          }
          this.setState({isLoading: false});
          refChild.open();
        }
      });
  };

  render() {
    if (this.state.selectedShippingOptionService) {
      var shippingTotal = this.state.selectedShippingOptionService.reduce(
        function (total, array) {
          // return the sum with previous value
          return total + array.item?.pship_value;
        },
        0,
      );
    }

    // console.log(
    //   'in state shippingSummary',
    //   JSON.stringify(this.props.shipping.shippingSummary),
    // );

    if (this.props.loading || this.state.loading) {
      return <Spinner color="red" size="large" />;
    }
    // if product type === 'p'
    const product_type_p = this.props.product_type == 'P' ? true : false;
    const shippingSummary =
      this.state.shippingSummary.length > 0 ? true : false;

    return (
      <View>
        <PTRView onRefresh={() => this.refresh()}>
          <ScrollView style={styles.wrapper}>
            <View style={{backgroundColor: '#fff', paddingHorizontal: wp(3)}}>
              {product_type_p && (
                <View
                  style={{
                    flex: 1,
                  }}>
                  <Text style={{...styles.billingAddress, paddingBottom: 5}}>
                    Alamat Pengiriman
                  </Text>
                  <View
                    style={{
                      borderTopColor: '#eaeaea',
                      borderTopWidth: 1,
                      paddingVertical: hp(1),
                    }}>
                    {this.props.shipping_address && (
                      <>
                        <Text style={{color: '#2f3640', fontSize: 16}}>
                          {this.props.shipping_address.user_name}
                        </Text>
                        <Text style={{color: '#2f3640', fontSize: 14}}>
                          {this.props.shipping_address.phone_number}
                        </Text>
                        {this.props.shipping_address.district === undefined ||
                        this.props.shipping_address.district === null ||
                        this.props.shipping_address.district === '' ? (
                          <Text>
                            {`${this.props.shipping_address.address1} ,${this.props.shipping_address.city},${this.props.shipping_address.state} ${this.props.shipping_address.zipcode}`}
                          </Text>
                        ) : (
                          <Text>
                            {`${this.props.shipping_address.address1} ,${this.props.shipping_address.district} ,${this.props.shipping_address.city},${this.props.shipping_address.state} ${this.props.shipping_address.zipcode}`}
                          </Text>
                        )}
                        {/* {address2 && <Text>{`${this.props.shipping_address?.address2}`}</Text>} */}
                      </>
                    )}

                    {!this.props.shipping_address && (
                      <>
                        <TouchableOpacity
                          underlayColor="#fff"
                          onPress={() =>
                            this.props.navigation.navigate('AddressList', {
                              _token: this.state._token,
                              type: 'shipping',
                              // ua_id: this.props.shipping_address.ua_id,
                            })
                          }>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: 'row',
                              // borderRadius: 5,
                              paddingVertical: hp(1),
                              justifyContent: 'space-between',
                              alignItems: 'center',
                            }}>
                            <Text style={{fontSize: 16}}>
                              Tambahkan Alamat Pengiriman
                            </Text>
                            <FontAwesome5Icon
                              name={'chevron-right'}
                              size={hp(2)}
                            />
                          </View>
                        </TouchableOpacity>
                      </>
                    )}

                    {this.props.shipping_address && (
                      <TouchableHighlight
                        underlayColor="#fff"
                        onPress={() =>
                          this.props.navigation.navigate('AddressList', {
                            _token: this.state._token,
                            type: 'shipping',
                            ua_id: this.props.shipping_address.ua_id,
                          })
                        }>
                        <View
                          style={{
                            flex: 1,
                            flexDirection: 'row',
                            borderTopColor: '#eaeaea',
                            borderTopWidth: 1,
                            // borderRadius: 5,
                            paddingVertical: hp(1),
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingTop: 10,
                            marginTop: 10,
                          }}>
                          <Text style={{fontSize: 16}}>
                            Gunakan alamat lain
                          </Text>
                          <FontAwesome5Icon
                            name={'chevron-right'}
                            size={hp(2)}
                          />
                        </View>
                      </TouchableHighlight>
                    )}
                  </View>
                </View>
              )}
            </View>
            <View
              style={{
                flex: 1,
                marginTop: hp(1),
                backgroundColor: '#fff',
                paddingHorizontal: wp(3),
                paddingTop: hp(1.5),
                borderBottomColor: '#eaeaea',
                borderBottomWidth: 1,
              }}>
              <Text
                style={{
                  ...styles.billingAddress,
                  paddingBottom: 5,
                  marginTop: 0,
                }}>
                Rincian Pengiriman
              </Text>
            </View>
            <View>
              {shippingSummary && (
                <View>
                  {this.state.shippingSummary.map((product, index) => (
                    <View key={index}>
                      <View
                        style={{
                          flex: 1,
                          marginBottom: hp(1),
                          backgroundColor: '#fff',
                          paddingHorizontal: wp(3),
                          paddingVertical: hp(1.5),
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            //   borderTopColor: '#eaeaea',
                            //   borderTopWidth: 1,
                            paddingVertical: hp(1.5),
                          }}>
                          <View
                            style={{
                              width: 70,
                              height: 70,
                              maxWidth: 70,
                              maxHeight: 70,
                              backgroundColor: '#eaeaea',
                              borderRadius: 5,
                            }}>
                            <Image
                              style={{
                                width: '100%',
                                height: '100%',
                                borderRadius: 5,
                              }}
                              source={{uri: product.product_image}}
                            />
                          </View>
                          <View style={{flex: 1, marginLeft: wp(3)}}>
                            <Text
                              style={{
                                fontSize: hp(2.2),
                                color: '#000',
                                fontWeight: '700',
                              }}>
                              {product.product_name}
                            </Text>
                            {/* <Text style={{fontSize: hp(2)}}>Jumlah barang 1</Text> */}
                            <View style={{flexDirection: 'row'}}>
                              <Text
                                style={{
                                  fontSize: hp(2.2),
                                  color: 'rgba(235, 47, 6,1.0)',
                                  fontWeight: 'bold',
                                }}>
                                {`${this.state.price_currency}. ${formatRupiah(
                                  product.unit_price,
                                )}`}
                              </Text>
                              <Text
                                style={{
                                  fontSize: hp(2.2),
                                  color: '#000',
                                  fontWeight: 'bold',
                                  marginLeft: wp(2),
                                }}>
                                {`(${product.qty_number})`}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                      <View
                        style={{
                          paddingHorizontal: wp(3),
                          paddingVertical: hp(1),
                          backgroundColor: '#fff',
                          marginBottom: 10,
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: 5,
                          }}>
                          <Text>Metode Pengiriman</Text>
                          {this.props.shipping_address === null ? (
                            <Text style={{color: 'red', fontWeight: 'bold'}}>
                              Alamat Kosong!
                            </Text>
                          ) : (
                            <ShippingOptions
                              options={product.shipping_methods}
                              selectedOption={
                                this.props.shipping.shippingOptions
                              }
                              companyName={substr(
                                getCompanyName(
                                  product.shipping_methods,
                                  this.props.shipping
                                    .selectedShippingOptionService,
                                  product.id,
                                ),
                                25,
                              )}
                              onSelectOption={(
                                service_name,
                                indexs,
                                productId,
                                service,
                                ref,
                                refChild,
                              ) =>
                                this.getShippingData(
                                  service_name,
                                  indexs,
                                  productId,
                                  service,
                                  ref,
                                  refChild,
                                )
                              }
                              service={product.selected_shipping_option}
                              parentIndex={index}
                              productId={product.id}
                              isLoading={this.state.isLoading}
                              optionsChild={
                                this.props.shipping.shippingOptions.filter(
                                  (ship) => ship.product_id === product.id,
                                )[0]
                              }
                              onSelectOptionService={(data) =>
                                this.onSetShipping(data)
                              }
                              selectedOptionService={
                                this.props.shipping
                                  .selectedShippingOptionService
                              }
                            />
                          )}
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: 5,
                          }}>
                          <Text>Ongkos Kirim</Text>
                          <Text>
                            Rp.{' '}
                            {this.props.shipping.selectedShippingOptionService[
                              index
                            ]?.product_id === product.id
                              ? formatRupiah(
                                  this.props.shipping
                                    .selectedShippingOptionService[index].item
                                    ?.pship_value,
                                )
                              : 0}
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingVertical: 5,
                          }}>
                          <Text>Estimasi</Text>

                          <Text>
                            {this.props.shipping.selectedShippingOptionService[
                              index
                            ]?.product_id === product.id
                              ? this.props.shipping
                                  .selectedShippingOptionService[index].item
                                  ?.pship_duration
                              : 0}{' '}
                            Hari
                          </Text>
                        </View>
                      </View>
                    </View>
                  ))}
                </View>
              )}
            </View>
            <View style={styles.cardHeader}>
              <Text
                style={{
                  ...styles.billingAddress,
                  paddingBottom: 5,
                  marginTop: 0,
                }}>
                Rincian Pembayaran
              </Text>
            </View>
            <View style={styles.cardBody}>
              <View style={styles.cardItem}>
                <Text>
                  Jumlah Barang (
                  {`${this.props.shipping.shippingSummary.length}`})
                </Text>
                <Text>
                  Rp.
                  {formatRupiah(this.props.rewardsCouldbeApplied)}
                </Text>
              </View>

              {this.props.tax_data && (
                <View style={styles.cardItem}>
                  <Text>Pajak</Text>
                  <Text>Rp. {this.props.tax_data?.value}</Text>
                </View>
              )}

              {/* value={!isNaN(feeTotal) ? formatRupiah(feeTotal) : 0} */}

              {shippingTotal != null ? (
                <View style={styles.cardItem}>
                  <Text>Ongkos Pengiriman</Text>
                  <Text>
                    {' '}
                    Rp.
                    {!isNaN(shippingTotal) ? formatRupiah(shippingTotal) : 0}
                  </Text>
                </View>
              ) : (
                <View style={styles.cardItem}>
                  <Text>Ongkos Pengiriman</Text>
                  <Text>Rp. 0</Text>
                </View>
              )}

              {this.props.shipping.shippingSummary[0]?.product.shipping_free >
                0 && (
                <View style={styles.cardItem}>
                  <Text>Bebas Ongkos Kirim</Text>
                  <Text>
                    Rp.{' '}
                    <Text style={{textDecorationLine: 'line-through'}}>
                      {formatRupiah(this.props.shipping.total_cost)}
                    </Text>
                  </Text>
                </View>
              )}

              {this.props.isCouponApplied && (
                <View style={styles.cardItem}>
                  <Text>Diskon</Text>
                  <Text>
                    Rp. -{formatRupiah(parseInt(this.props.coupon_value))}
                  </Text>
                </View>
              )}

              {this.props.reward_data_applied && (
                <View style={styles.cardItem}>
                  <Text>Point Hadiah</Text>
                  <Text>
                    {'Rp. -'}
                    {formatRupiah(this.props.reward_data_applied)}
                  </Text>
                </View>
              )}

              <View style={styles.cardItem}>
                <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                  Total Pembayaran
                </Text>
                <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                  Rp. {formatRupiah(this.props.payable_amount)}
                </Text>
              </View>
            </View>

            <View style={styles.cardButton}>
              {/* <Button onPress={this.handlePayment}>Bayar</Button> */}
              <PaymentOptions
                productData={this.props.shipping.shippingSummary}
                onPressPayment={(route, params) =>
                  this.toPayment(route, params)
                }
                onPress={(route) => this.saveShippingSummary(route)}
                {...this.props}
              />
            </View>
          </ScrollView>
        </PTRView>
      </View>
    );
  }
}

const styles = {
  cardBody: {
    flex: 1,
    marginBottom: hp(1),
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    paddingVertical: hp(1.5),
  },
  cardItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 5,
  },
  cardButton: {
    flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
    // marginBottom: hp(2),
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex: 1,
    marginTop: hp(1),
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    paddingTop: hp(1.5),
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
  },
  qtyClass: {
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.1%'),
    color: '#000',
  },
  priceTxt: {
    color: '#00b3ff',
    fontSize: hp('2.2%'),
    fontFamily: Font.RobotoBold,
  },
  section1: {
    flexDirection: 'row',
    width: '100%',
    flexShrink: 0,
    borderWidth: 1,
    borderColor: '#e7e7e7',
    padding: 10,
  },
  productImg: {
    flexBasis: wp('12%'),
    width: wp('12%'),
    height: hp('10%'),
    justifyContent: 'flex-start',
  },
  middleTxt: {
    flex: 1,
    alignItem: 'left',
    flexShrink: 0,
    paddingLeft: hp('2%'),
  },
  deleteIcon: {
    flexBasis: wp('13%'),
    height: hp('7%'),
    width: wp('13%'),
    justifyContent: 'flex-end',
    flexShrink: 0,
  },
  titleStyle: {
    color: '#c90305',
    fontSize: hp('1.8%'),
    paddingBottom: 5,
    fontFamily: Font.RobotoLight,
  },
  pickerClass: {
    height: hp('5%'),
  },
  selectLabel: {
    color: '#000',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
    paddingBottom: hp('1.5%'),
  },
  continueBtn: {
    padding: 20,
    // backgroundColor: '#f1f0f0',
    paddingBottom: 10,
    // marginTop: 15,
    marginBottom: 20,
    paddingLeft: 35,
    paddingRight: 35,
  },
  userMobile: {
    fontSize: hp('2.1%'),
    fontFamily: Font.RobotoRegular,
  },
  UserName: {
    fontSize: hp('2.4%'),
    marginBottom: 7,
    fontFamily: Font.RobotoMedium,
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  crossColor: {
    textAlign: 'center',
    color: '#000',
  },
  closeBtn2: {
    position: 'absolute',
    right: 0,
    width: 30,
    height: 30,
    top: 0,
    zIndex: 1002,
    color: '#fff',
    borderRadius: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
    // paddingHorizontal: 10,
    backgroundColor: '#f5f6fa',
  },
  payment_option: {
    color: '#696969',
    fontSize: hp('2.6%'),
    fontFamily: Font.RobotoRegular,
    marginBottom: hp('1.2%'),
  },
  rightIcon: {
    width: 20,
    height: 20,
  },
  transfar_bank: {
    fontSize: hp('2.2%'),
    color: '#545454',
    marginLeft: 10,
    fontFamily: Font.RobotoRegular,
  },
  totalPriceWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: hp('1.5%'),
    paddingTop: hp('1.5%'),
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },

  totalItem: {
    fontSize: wp('4.2%'),
    color: '#6f6f6f',
    fontFamily: Font.RobotoRegular,
  },
  fontClass: {
    fontSize: wp('4.4%'),
  },
  bankDetailTxt: {
    color: '#00b3ff',
    fontFamily: Font.RobotoMedium,
    fontSize: hp('2.1%'),
    marginTop: hp('1.5%'),
    marginBottom: hp('0.5%'),
  },
  accountStyle: {
    color: '#6f6f6f',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.1%'),
  },
  paymentBox: {
    elevation: 1,
    shadowColor: 'transparent',
    borderColor: 'transparent',
    shadowOpacity: 0.2,
    borderWidth: 1,
    padding: 10,
    marginTop: 15,
    marginBottom: hp('3.5%'),
    borderRadius: 2,
  },
  radioWrappper: {
    flexDirection: 'row',
    paddingTop: 4,
    paddingBottom: 4,
    alignItems: 'center',
  },
  heading_style: {
    color: '#545454',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.5%'),
  },

  billingAddress: {
    fontSize: wp('4.3%'),
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    marginBottom: hp('1%'),
    marginTop: hp('2%'),
  },
  iconBox: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
  },
  iconAddress: {
    width: 13,
    height: 18,
    marginTop: 3,
    position: 'absolute',
  },
  iconTxt: {
    fontSize: wp('4%'),
    marginLeft: 8,
  },
  changeAddBtn: {
    width: hp('19%'),
    height: hp('9%'),
    marginTop: hp('2.5%'),
    marginBottom: hp('2.5%'),
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.payment.loading,
    product_type: state.payment.product_type,
    billing: state.payment.billing_address,
    shipping_address: state.payment.shipping_address,
    changeDisplayShip: state.payment.changeDisplayShip,
    changeDisplayBill: state.payment.changeDisplayBill,
    isCouponApplied: state.cart.isCouponApplied,
    coupon_value: state.cart.coupon_value,
    available_reward_point: state.cart.available_reward_point,
    reward_data_applied: state.cart.reward_data,
    rewardsCouldbeApplied: state.cart.cart_max_rewards_points,
    reward_point_message: state.cart.reward_point_message,
    payable_amount: state.cart.net_payable_amount,
    balance: state.wallet.balance,
    shipping: state.shipping,
    tax_data: state.cart.tax_data,
  };
};

const dispatchToprops = {
  setAmount,
  getCountries,
  getAddressPayment,
  getWalletDetails,
  setValue,
};

export default connect(mapStateToProps, dispatchToprops)(Payment);
