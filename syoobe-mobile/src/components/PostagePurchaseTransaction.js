import React from 'react';
import {
  Image,
  TouchableOpacity,
  Animated,
  StyleSheet,
  View,
  Text,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ChildrenProps from './HOC/ChildrenProps';

const postagePurchaseTransaction = (props) => {
  return (
    <ChildrenProps>
      <TouchableOpacity
        onPress={() => props.clicked(props.postageData.id)}
        style={styles.touchStyle}>
        <Text style={styles.lftTxt}>$ {props.postageData.amount}</Text>
        <Text style={styles.rgtTxt}>{props.postageData.date}</Text>
        {props.viewOpen && props.activeId == props.postageData.id ? (
          <Image
            style={styles.imageStyle}
            resizeMode={'contain'}
            source={require('../images/arrowDown.png')}
          />
        ) : (
          <Image
            style={styles.imageStyle}
            resizeMode={'contain'}
            source={require('../images/down.png')}
          />
        )}
      </TouchableOpacity>
      {props.viewOpen && props.activeId == props.postageData.id && (
        <View style={styles.bdSection}>
          <View style={styles.rowView}>
            <View style={styles.lftTextView}>
              <Text style={styles.lftTxt}>Tanggal</Text>
            </View>
            <View style={styles.rgtTextView}>
              <Text style={styles.rgtTxt}>{props.postageData.date}</Text>
            </View>
          </View>
          <View style={styles.rowView}>
            <View style={styles.lftTextView}>
              <Text style={styles.lftTxt}>Nomor faktur</Text>
            </View>
            <View style={styles.rgtTextView}>
              <Text style={[styles.rgtTxt, styles.txtColor]}>
                {props.postageData.invoice_number}
              </Text>
            </View>
          </View>
          <View style={styles.rowView}>
            <View style={styles.lftTextView}>
              <Text style={styles.lftTxt}>ID transaksi</Text>
            </View>
            <View style={styles.rgtTextView}>
              <Text style={styles.rgtTxt}>
                {props.postageData.transaction_id}
              </Text>
            </View>
          </View>
          <View style={styles.rowView}>
            <View style={styles.lftTextView}>
              <Text style={styles.lftTxt}>Jumlah</Text>
            </View>
            <View style={styles.rgtTextView}>
              <Text style={styles.rgtTxt}>$ {props.postageData.amount}</Text>
            </View>
          </View>
          <View style={styles.rowView}>
            <View style={styles.lftTextView}>
              <Text style={styles.lftTxt}>Catatan</Text>
            </View>
            <View style={styles.rgtTextView}>
              <Text style={styles.rgtTxt}>{props.postageData.notes}</Text>
            </View>
          </View>
          <View style={styles.rowView}>
            <View style={styles.lftTextView}>
              <Text style={styles.lftTxt}>Status</Text>
            </View>
            <View style={styles.rgtTextView}>
              <Text style={styles.rgtTxt}>{props.postageData.statue}</Text>
            </View>
          </View>
        </View>
      )}
    </ChildrenProps>
  );
};

const styles = StyleSheet.create({
  imageStyle: {
    height: hp('10%'),
    width: wp('5%'),
  },
  touchStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    borderWidth: 1,
    borderColor: '#e8edef',
    elevation: 0.5,
    height: 50,
    alignItems: 'center',
    padding: 10,
    marginVertical: 5,
    // backgroundColor:'#f6f8f9',
    // justifyContent: 'center'
  },
  txtColor: {
    color: '#ed0030',
  },
  bdSection: {
    marginBottom: 20,
  },
  rowView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // alignItems:'center',
    marginBottom: 8,
  },
  lftTextView: {
    width: '38%',
    backgroundColor: '#f6f8f9',
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: hp('1.8%'),
    borderWidth: 1,
    borderColor: '#e8edef',
    elevation: 0.5,
  },
  rgtTextView: {
    paddingLeft: 20,
    paddingVertical: hp('1.8%'),
    flex: 1,
    marginLeft: 20,
    // borderWidth:1,
    // borderColor:'#f4f4f4',
    borderRadius: 2,
    justifyContent: 'center',
    elevation: 0.5,
  },
  lftTxt: {
    textAlign: 'center',
    color: '#4e4c4c',
    fontSize: hp('1.8%'),
  },
  rgtTxt: {
    color: '#7d7d7d',
    fontSize: hp('1.8%'),
    textAlign: 'left',
  },
});

export default postagePurchaseTransaction;
