import React, {Component} from 'react';
import SearchComponent from './SearchComponent';
import {View} from 'react-native';
import {connect} from 'react-redux';

class SearchParent extends Component {
  search = (event) => {
    // console.log('event', event);
    this.props.navigation.navigate('ProductsByCategory', {
      category_id: event.category_id,
      keyword: event.keyword,
      search: true,
    });
  };

  render() {
    return (
      <View>
        <SearchComponent
          categories={this.props.categories}
          onClickSearch={this.search.bind(this)}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    showSearch: state.home.showSearch,
    categories: state.category.categories,
  };
};

export default connect(mapStateToProps, {})(SearchParent);
