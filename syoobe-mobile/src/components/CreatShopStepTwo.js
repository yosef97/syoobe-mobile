import React, {Component} from 'react';
import {
  Text,
  Alert,
  TouchableOpacity,
  Switch,
  Image,
  View,
  ScrollView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {CardSection, Button} from './common';
import {Font} from './Font';
import FileComponent from './FileComponent';
import {connect} from 'react-redux';
import {
  skip_step_2,
  setCountryId,
  setStateId,
  display_status,
  close_banner,
  close_logo,
  update_banner,
  update_logo,
  messageToBuyer,
  shop_announcement,
  shopCity,
  shopDescription,
  getCountries,
  getStates,
  shop_district_id,
  shop_district_name,
  shop_state_name,
  shop_city_id,
} from '../actions';

import {
  resetAdress,
  setSelectedProvince,
  setSelectedCity,
  setSelectedDistrict,
  setComponentRedirect,
} from '../actions/AdressAction';
import InputText from './common/InputText';

class CreatShopStepTwo extends Component {
  state = {
    selectedStateId: 0,
    selectedCountryId: 0,
    countryLoaded: false,
    stateLoaded: false,
    countries: [],
    states: [],
  };

  showCalculation = () => {
    if (this.state.isSwitchOn) {
      // do something
    } else {
      // do something else
    }
  };

  componentDidMount() {
    this.fetchCountries();
    this.props.getStates({country_id: this.props.country_id});
    if (this.props.countries.length > 0) {
      const array = [];
      this.props.countries.map((element) => {
        array.push({
          value: element.id,
          label: element.name,
        });
      });
      this.setState({
        countries: array,
      });
    }
    if (this.props.states.length > 0) {
      const array = [];
      this.props.states.forEach((element) => {
        array.push({
          value: element.id,
          label: element.name,
        });
      });
      this.setState({
        states: array,
      });
    }
  }

  fetchCountries = async () => {
    await this.props.getCountries();
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.countries.length !== this.props.countries.length) {
      const array = [];
      this.props.countries.forEach((element) => {
        array.push({
          value: element.id,
          label: element.name,
        });
      });
      this.setState({
        countries: array,
      });
    }
    if (prevProps.states.length !== this.props.states.length) {
      const array = [];
      this.props.states.forEach((element) => {
        array.push({
          value: element.id,
          label: element.name,
        });
      });
      this.setState({
        states: array,
      });
    }
    if (!this.state.stateLoaded) {
      this.setState({
        selectedStateId: parseInt(this.props.state_id),
        stateLoaded: true,
      });
    }
  };

  // componentWillReceiveProps = () => {
  //   console.log('log', this.props.country_id);

  //   if (!this.state.countryLoaded) {
  //     this.setState({
  //       selectedCountryId: parseInt(this.props.country_id),
  //       countryLoaded: true,
  //     });
  //   }
  // };

  static getDerivedStateFromProps(props, state) {
    console.log('log', props.country_id);

    if (!state.countryLoaded) {
      return {
        selectedCountryId: parseInt(props.country_id),
        countryLoaded: true,
      };
    }
  }

  selectState = (itemValue) => {
    this.setState({selectedStateId: parseInt(itemValue)});
  };

  getStates = (itemValue) => {
    this.setState({selectedCountryId: parseInt(itemValue)});
    this.props.getStates({country_id: itemValue});
  };

  shopDescription = (text) => {
    this.props.shopDescription(text);
  };

  shopCity = (text) => {
    this.props.shopCity(text);
  };

  toAnnouncement = (text) => {
    this.props.shop_announcement(text);
  };

  messageToBuyer = (text) => {
    this.props.messageToBuyer(text);
  };

  update_logo = (shop_logo) => {
    this.props.update_logo(shop_logo);
  };

  update_banner = (shop_banner) => {
    this.props.update_banner(shop_banner);
  };

  closeLogo = () => {
    this.props.close_logo();
  };

  closeBanner = () => {
    this.props.close_banner();
  };
  districtId = (district_id) => {
    this.props.shop_district_id(district_id);
  };
  districtName = (district_name) => {
    this.props.shop_district_name(district_name);
  };
  cityId = (city_id) => {
    this.props.shop_city_id(city_id);
  };
  stateName = (state_name) => {
    this.props.shop_state_name(state_name);
  };

  goToStep3 = () => {
    const {
      shop,
      selectedProvince,
      selectedCity,
      selectedDistrict,
      navigation,
    } = this.props;
    if (this.state.PickerSelectedCountry === 'Pilih Negara') {
      Alert.alert('Informasi', 'Mohon Pilih Negara', [{text: 'OK'}]);
    } else if (this.state.PickerSelectedState === 'Select State') {
      Alert.alert('Informasi', 'Mohon Pilih Provinsi', [{text: 'OK'}]);
    } else if (this.props.description === '') {
      Alert.alert('Informasi', 'Masukkan deskripsi toko kamu', [{text: 'OK'}]);
    } else if (this.props.announcement === '') {
      Alert.alert('Informasi', 'Masukkan Pengumuman Yang Kamu Punya', [
        {text: 'OK'},
      ]);
    } else if (this.props.message_to_buyer === '') {
      Alert.alert('Informasi', 'Tambahkan pesan toko kamu', [{text: 'OK'}]);
    } else {
      navigation.navigate('CreatShopStepThree');
      this.stateName(selectedProvince.province || shop?.shop_state_name);
      this.cityId(selectedCity.city_id || shop?.shop_city_id);
      this.shopCity(selectedCity.city_name || shop?.shop_city);
      this.districtId(
        selectedDistrict.subdistrict_id || shop?.shop_district_id,
      );
      this.districtName(
        selectedDistrict.subdistrict_name || shop?.shop_district_name,
      );
    }
  };

  toggleSwitch = () => {
    this.props.display_status();
  };

  render() {
    // console.log('in state', JSON.stringify(this.state));

    // console.log(
    //   'in props display status',
    //   JSON.stringify(this.props.shop_vendor_display_status),
    // );

    const {
      shop,
      selectedProvince,
      selectedCity,
      selectedDistrict,
      navigation,
    } = this.props;
    return (
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        style={styles.scrollClass}>
        <Text style={styles.shopHeading}>Informasi Toko</Text>
        <View style={styles.processImgWrapper}>
          <Image
            style={styles.processImg}
            source={require('../images/processImg.png')}
          />
        </View>

        <View style={styles.imgBtnWrapper}>
          <Text style={[styles.doneTxt, styles.completeColor]}>
            Info dan Tampilan
          </Text>
          <Text style={styles.doneTxt}>Kebijakan Toko</Text>
          <Text style={styles.doneTxt}>Informasi Toko SEO</Text>
        </View>
        <View style={styles.viewStyle}>
          <CardSection>
            <InputText
              value={this.props.description}
              onChangeText={this.shopDescription.bind(this)}
              label="Keterangan"
              multiline
              numberOfLines={4}
            />
          </CardSection>
          <CardSection>
            <InputText
              value={selectedProvince.province || shop?.shop_state_name}
              onFocus={() => {
                this.props.setComponentRedirect('CreatShopStepTwo');
                this.props.navigation.navigate('Province');
              }}
              placeholder={'Pilih Provinsi'}
              label="Provinsi"
              icon={'chevron-down'}
            />
          </CardSection>
          <CardSection>
            <InputText
              value={selectedCity.city_name || shop?.shop_city}
              onFocus={() => {
                navigation.navigate(
                  selectedProvince.province ? 'City' : 'Province',
                  {
                    province_id:
                      selectedProvince.province_id || shop?.shop_state_name,
                  },
                );
              }}
              label="Kota/Kabupaten"
              placeholder="Pilih Kota/Kabupaten"
              icon={'chevron-down'}
            />
          </CardSection>
          <CardSection>
            <InputText
              value={
                selectedDistrict.subdistrict_name || shop?.shop_district_name
              }
              onFocus={() => {
                navigation.navigate(
                  selectedCity.city_name ? 'District' : 'Province',
                  {
                    city_id: selectedCity.city_id || shop?.shop_district_id,
                  },
                );
              }}
              label="Kecamatan"
              placeholder="Pilih Kecamatan"
              icon={'chevron-down'}
            />
          </CardSection>
          {this.props.logo != null ? (
            <View style={styles.shopLogo}>
              <Image style={styles.shopLogo} source={{uri: this.props.logo}} />
              <TouchableOpacity
                onPress={() => this.closeLogo()}
                style={styles.closeIcon}>
                <Image
                  style={{width: 25, height: 25}}
                  source={require('../images/close.png')}
                />
              </TouchableOpacity>
            </View>
          ) : null}
          <FileComponent
            onClickShowImage={this.update_logo.bind(this)}
            label={'Logo Toko'}></FileComponent>
          {this.props.banner != null ? (
            <View style={styles.shopBannerWrapper}>
              <Image
                style={styles.shopBanner}
                source={{uri: this.props.banner}}
              />
              <TouchableOpacity
                onPress={() => this.closeBanner()}
                style={styles.closeIcon}>
                <Image
                  style={{width: 25, height: 25}}
                  source={require('../images/close.png')}
                />
              </TouchableOpacity>
            </View>
          ) : null}
          <FileComponent
            onClickShowImage={this.update_banner.bind(this)}
            label={'Spanduk Toko'}></FileComponent>

          <CardSection>
            <InputText
              value={this.props.announcement}
              onChangeText={this.toAnnouncement.bind(this)}
              label="Pengumuman"
              multiline
              numberOfLines={4}
            />
          </CardSection>
          <CardSection>
            <InputText
              value={this.props.message_to_buyer}
              onChangeText={this.messageToBuyer.bind(this)}
              label="Pesan ke Pembeli"
              multiline
              numberOfLines={4}
            />
          </CardSection>

          <View style={styles.toggleContainer}>
            <Text style={styles.toggleTitle}>Status Tampilan</Text>
            <View style={styles.switchContainer}>
              <Text style={styles.errorTextStyle}>Liburan</Text>
              <Switch
                onValueChange={() => this.toggleSwitch()}
                value={this.props.shop_vendor_display_status}
                trackColor={'rgba(0, 179, 255, 0.5)'}
                thumbColor={'#00b3ff'}
              />
              <Text style={styles.errorTextStyle}>Aktif</Text>
            </View>
            <Text style={styles.switchBottomTxt}>
              Tekan tombol "LIBURAN" untuk menonaktifkan sementara semua produk
              Anda dari tampilan situs.
            </Text>
          </View>

          <View style={styles.btnWrapper}>
            <Button onPress={() => this.goToStep3()}>Lanjut</Button>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  cardButton: {
    // flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
    // marginBottom: hp(2),
    backgroundColor: '#fff',
  },
  dropdownClass: {
    // elevation: 1,
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
    // borderRadius: 50,
    height: hp('7%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  pickerClass: {
    height: hp('7%'),
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  bgImg: {
    height: '100%',
  },
  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
    // padding:10
  },
  shopHeading: {
    fontSize: wp('6%'),
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 15,
    color: '#696969',
    fontFamily: Font.RobotoRegular,
  },
  viewStyle: {
    marginleft: 5,
    marginRight: 5,
    paddingLeft: 10,
    paddingRight: 10,
  },
  toggleContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  toggleTitle: {
    width: '33%',
    fontSize: wp('4%'),
    fontFamily: Font.RobotoRegular,
    marginTop: 13,
  },
  switchContainer: {
    flexDirection: 'row',
    width: wp('94%'),
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginBottom: 10,
  },
  switchBottomTxt: {
    fontFamily: Font.RobotoLight,
    color: '#8f8f8f',
    fontSize: wp('3.5%'),
  },
  errorTextStyle: {
    fontSize: wp('3.5%'),
    alignSelf: 'center',
    fontFamily: Font.RobotoLight,
  },
  btnWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 15,
    paddingBottom: 15,
  },
  processImgWrapper: {
    paddingLeft: wp('7%'),
    marginRight: 5,
  },
  processImg: {
    // width:'100%',
    width: wp('86%'),
    height: wp('15.5%'),
  },

  imgBtnWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderWidth:1,
    // borderColor:'#f00',
    width: '100%',
    flexWrap: 'wrap',
    marginBottom: 15,
    marginTop: 8,
  },
  doneTxt: {
    width: wp('28%'),
    fontSize: wp('4%'),
    color: '#969696',
    fontFamily: Font.RobotoMedium,
    // justifyContent:'center',
    // alignItems:'center',
    textAlign: 'center',
  },
  completeColor: {
    color: '#424242',
  },
  shopLogo: {
    width: wp('20%'),
    height: wp('20%'),
    marginBottom: 10,
    position: 'relative',
  },
  shopBannerWrapper: {
    width: '50%',
    height: hp('15%'),
  },
  shopBanner: {
    width: '100%',
    height: '100%',
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 10,
  },
  closeIcon: {
    position: 'absolute',
    right: -30,
    top: 0,
    width: 25,
    height: 25,
  },
};

const mapStateToProps = (state) => {
  return {
    countries: state.country.countries,
    states: state.country.states,
    description: state.createShop.description,
    shop_city: state.createShop.shop_city,
    announcement: state.createShop.announcement,
    message_to_buyer: state.createShop.message_to_buyer,
    logo: state.createShop.logo,
    banner: state.createShop.banner,
    shop_vendor_display_status: state.createShop.shop_vendor_display_status,
    country_id: state.createShop.country_id,
    state_id: state.createShop.state_id,
    shop: state.createShop.shop,
    selectedProvince: state.country.province.selectedProvince,
    selectedCity: state.country.city.selectedCity,
    selectedDistrict: state.country.district.selectedDistrict,
  };
};

export default connect(mapStateToProps, {
  skip_step_2,
  setCountryId,
  setStateId,
  display_status,
  close_banner,
  close_logo,
  update_banner,
  update_logo,
  messageToBuyer,
  shop_announcement,
  shopCity,
  shopDescription,
  getCountries,
  getStates,
  resetAdress,
  setSelectedProvince,
  setSelectedCity,
  setSelectedDistrict,
  setComponentRedirect,
  shop_district_id,
  shop_district_name,
  shop_state_name,
  shop_city_id,
})(CreatShopStepTwo);
