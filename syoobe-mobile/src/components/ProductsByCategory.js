import React, {Component} from 'react';
import {Image, View, TouchableOpacity, Text, ScrollView} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Modal from 'react-native-modal';
import {RadioButton} from 'react-native-paper';
import {Font} from './Font';
import {connect} from 'react-redux';
import {getSearchResults, getProductsByCategory} from '../actions';
import {Spinner} from './common';
import CheckBoxComponent from './CheckBoxComponent';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {formatRupiah, substr} from '../helpers/helper';

class ProductsByCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      isModalSortVisible: false,
      isConfirmModalVisible: false,
      innerModalContentVisible: false,
      sub_cat_checked: null,
      checkedItems: new Map(),
      brands: '',
      checkedConditions: new Map(),
      conditions: '',
      free_shipping: false,
      exclude_out_of_Stock: false,
      price_low: 0,
      price_high: 0,
      checked: '',
      sortchecked: '',
      category_id: null,
      keyword: '',
      priceSet: false,
    };
  }

  componentDidMount = async () => {
    if (this.props.route.params.search) {
      await this.setState({
        category_id: this.props.route.params.category_id,
        keyword: this.props.route.params.keyword,
      });
      var details = {
        category_id: this.state.category_id,
        keyword: this.state.keyword,
      };
      this.props.getSearchResults(details);
    } else {
      await this.setState({
        category_id: this.props.route.params.category_id,
      });
      var details = {
        category_id: this.state.category_id,
      };
      this.props.getProductsByCategory(details);
    }
  };

  _toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
    });
  };

  _toggleInnerModalContent = () => {
    this.setState({
      innerModalContentVisible: !this.state.innerModalContentVisible,
    });
  };

  _toggleSortModal = () => {
    this.setState({
      isModalSortVisible: !this.state.isModalSortVisible,
    });
  };

  renderProducts = () => {
    if (this.props.loading == true) {
      return (
        <View
          style={{
            marginTop: hp('38%'),
          }}>
          <Spinner color="red" size="large" />
        </View>
      );
    }

    if (this.props.products.length < 1) {
      return (
        <View
          style={{
            marginTop: hp('38%'),
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: hp('2.5%'),
              color: '#000',
            }}>
            Tidak Ada Produk!
          </Text>
        </View>
      );
    }
    if (this.props.products) {
      return this.props.products.map((product) => (
        <View key={product.product_id} style={styles.listWrapper}>
          {product.product_type == 'P' && (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.push('ProductDetail', {
                  product_id: product.product_id,
                  product_requires_shipping: product.prod_requires_shipping,
                })
              }>
              <View style={styles.imgContentWrapper}>
                <View style={styles.imgWrapper}>
                  <Image
                    resizeMode="contain"
                    style={styles.boxImg}
                    source={{uri: product.product_image}}
                  />
                  <View>
                    <Text style={styles.titleStyle}>
                      {product.product_name}
                    </Text>
                    <Text style={styles.soldStyle}>
                      Dijual oleh: {product.store_name}
                    </Text>
                    {product.free_shipping == 'Y' && (
                      <View style={styles.ratingWrapper}>
                        <Image
                          resizeMode='stretch'
                          style={styles.sfLogo}
                          source={require('../images/free_shipping.png')}
                        />
                      </View>
                    )}

                    <Text style={styles.priceTxt}>
                      {product.price_currency}{' '}
                      {formatRupiah(product.product_price)}
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}

          {product.product_type == 'I' && (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.push('ProductDetail', {
                  product_id: product.product_id,
                  product_requires_shipping: product.prod_requires_shipping,
                })
              }>
              <View style={styles.imgContentWrapper}>
                <View style={styles.imgWrapper}>
                  <Image
                    resizeMode="contain"
                    style={styles.boxImg}
                    source={{uri: product.product_image}}
                  />
                  <View>
                    <Text style={styles.titleStyle}>
                      {product.product_name}
                    </Text>
                    <Text style={styles.soldStyle}>
                      Dijual oleh: {product.store_name}
                    </Text>
                    <View style={styles.ratingWrapper}>
                      <Image
                        resizeMode="contain"
                        style={styles.sfLogo}
                        source={require('../images/download.png')}
                      />
                    </View>
                    <Text style={styles.priceTxt}>
                      {product.price_currency}{' '}
                      {formatRupiah(product.product_price)}
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}
          {product.product_type == 'G' && (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.push('ProductDetail', {
                  product_id: product.product_id,
                  product_requires_shipping: product.prod_requires_shipping,
                })
              }>
              <View style={styles.imgContentWrapper}>
                <View style={styles.imgWrapper}>
                  <Image
                    resizeMode="contain"
                    style={styles.boxImg}
                    source={{uri: product.product_image}}
                  />
                  <View>
                    <Text style={styles.titleStyle}>
                      {product.product_name}
                    </Text>
                    <Text style={styles.soldStyle}>
                      Dijual oleh: {product.store_name}
                    </Text>
                    <View style={styles.ratingWrapper}>
                      <Image
                        resizeMode="contain"
                        style={styles.sfLogo}
                        source={require('../images/graphic_design.png')}
                      />
                    </View>
                    <Text style={styles.priceTxt}>
                      {product.price_currency}{' '}
                      {formatRupiah(product.product_price)}
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}
        </View>
      ));
    }
  };

  sortProducts = async (value) => {
    await this.setState({
      sortchecked: value,
    });
    var details = {
      category_id: this.state.category_id,
    };
    if (this.state.sortchecked == 'most_recent') {
      var add = {most_recent: 'desc'};
      details = {...details, ...add};
    } else if (this.state.sortchecked == 'featured') {
      var add = {featured: 'desc'};
      details = {...details, ...add};
    } else if (this.state.sortchecked == 'best_selling') {
      var add = {best_selling: 'desc'};
      details = {...details, ...add};
    } else if (this.state.sortchecked == 'price_order_asc') {
      var add = {price_order: 'asc'};
      details = {...details, ...add};
    } else if (this.state.sortchecked == 'price_order_desc') {
      var add = {price_order: 'desc'};
      details = {...details, ...add};
    }
    this.props.getProductsByCategory(details);
    this._toggleSortModal();
  };

  filterProducts = async (value) => {
    this.setState({
      checked: value,
      innerModalContentVisible: true,
    });
  };

  selectSubCategory = async (id) => {
    await this.setState({
      sub_cat_checked: id,
    });
  };

  shippingSelected = (event) => {
    this.setState({free_shipping: event});
  };

  availabilitySelected = (event) => {
    this.setState({exclude_out_of_Stock: event});
  };

  handleBrandsSelected(e, id) {
    const item = id;
    const isChecked = e;
    this.setState((prevState) => ({
      checkedItems: prevState.checkedItems.set(item, isChecked),
    }));
  }

  handleConditionsSelected(e, value) {
    const item = value;
    const isChecked = e;
    this.setState((prevState) => ({
      checkedConditions: prevState.checkedConditions.set(item, isChecked),
    }));
  }

  filtersApplied = async () => {
    var details = {};
    for (let [k, v] of await this.state.checkedItems) {
      if (v == true) {
        if (this.state.brands == '') {
          this.setState({
            brands: k,
          });
        } else {
          var array = [];
          array = this.state.brands.split(',');
          if (!array.includes(k)) {
            this.setState({
              brands: this.state.brands + ',' + k,
            });
          }
        }
      } else if (v == false) {
        if (this.state.brands != '') {
          if (this.state.brands.includes(',')) {
            var array = [];
            array = this.state.brands.split(',');
            if (array.includes(k)) {
              this.setState({
                brands: this.state.brands.replace(k + ',', ''),
              });
            }
          } else {
            this.setState({
              brands: '',
            });
          }
        }
      }
    }
    for (let [k, v] of await this.state.checkedConditions) {
      if (v == true) {
        if (this.state.conditions == '') {
          this.setState({
            conditions: k,
          });
        } else {
          var array = [];
          array = this.state.conditions.split(',');
          if (!array.includes(k)) {
            this.setState({
              conditions: this.state.conditions + ',' + k,
            });
          }
        }
      } else if (v == false) {
        if (this.state.conditions != '') {
          if (this.state.conditions.includes(',')) {
            var array = [];
            array = this.state.conditions.split(',');
            if (array.includes(k)) {
              this.setState({
                conditions: this.state.conditions.replace(k + ',', ''),
              });
            }
          } else {
            this.setState({
              conditions: '',
            });
          }
        }
      }
    }

    if (this.props.price_array != null) {
      if (
        this.state.price_low != this.props.price_array.minimum_price ||
        this.state.price_high != this.props.price_array.maximum_price
      ) {
        var add = {
          price_range: this.state.price_low + '-' + this.state.price_high,
        };
        details = {...details, ...add};
      }
    }

    if (this.state.sub_cat_checked != null) {
      var add = {category_id: this.state.sub_cat_checked};
      details = {...details, ...add};
    }
    if (this.state.sub_cat_checked == null) {
      var add = {category_id: this.state.category_id};
      details = {...details, ...add};
    }
    if (this.state.free_shipping) {
      var add = {free_shipping: 1};
      details = {...details, ...add};
    }
    if (this.state.exclude_out_of_Stock === true) {
      var add = {stock: 1};
      details = {...details, ...add};
    }
    if (this.state.brands != '') {
      var add = {brand: this.state.brands};
      details = {...details, ...add};
    }
    if (this.state.conditions != '') {
      var add = {condition: this.state.conditions};
      details = {...details, ...add};
    }
    this.props.getProductsByCategory(details);
    this.setState({
      priceSet: false,
    });
    this._toggleModal();
  };

  resetModal = async () => {
    this.setState({
      checkedItems: new Map(),
      checkedConditions: new Map(),
      free_shipping: false,
      exclude_out_of_Stock: false,
      priceSet: false,
      sub_cat_checked: null,
      checked: '',
      brands: '',
      conditions: '',
    });
    var details = {
      category_id: this.state.category_id,
    };
    this.props.getProductsByCategory(details);
    this._toggleModal();
  };

  multiSliderValuesChange = (event) => {
    this.setState({
      price_low: event[0],
      price_high: event[1],
    });
  };

  componentDidUpdate = async () => {
    if (this.props.price_array != null && this.state.priceSet != true) {
      await this.setState({
        price_low: this.props.price_array.minimum_price,
        price_high: this.props.price_array.maximum_price,
        priceSet: true,
      });
    }
  };

  render() {
    console.log('ini state', JSON.stringify(this.state));
    console.log('ini props', JSON.stringify(this.props));

    const {filter_icon, sort_icon, textStyle, wrapperStyle, viewStyle} = styles;
    const {sub_cat_checked, checked, sortchecked} = this.state;
    return (
      <ScrollView style={styles.bgColor}>
        <View style={viewStyle}>
          <View style={wrapperStyle}>
            <TouchableOpacity
              onPress={this._toggleModal}
              style={styles.touchBtn}>
              <Image
                style={filter_icon}
                source={require('../images/filterIcon.png')}
              />
              <Text style={textStyle}> Filter </Text>
            </TouchableOpacity>
          </View>
          <View style={wrapperStyle}>
            <TouchableOpacity
              onPress={this._toggleSortModal}
              style={styles.touchBtn}>
              <Image
                style={sort_icon}
                source={require('../images/sortIcon.png')}
              />
              <Text style={textStyle}>Sortir dengan</Text>
            </TouchableOpacity>
          </View>
        </View>
        {this.renderProducts()}
        <Modal
          onBackdropPress={() => this.setState({isModalSortVisible: false})}
          isVisible={this.state.isModalSortVisible}
          animationIn="slideInRight"
          animationOut="slideOutRight"
          animationInTiming={1000}
          animationOutTiming={1000}
          style={styles.bottomModal2}
          backdropTransitionInTiming={1000}
          backdropTransitionOutTiming={1000}>
          <View style={{flex: 1}} style={styles.modalWrapper}>
            <TouchableOpacity
              onPress={this._toggleSortModal}
              style={styles.closeBtn2}>
              <Text style={styles.crossColor}>&#10005;</Text>
            </TouchableOpacity>
            <View>
              <Text style={styles.filterText}>SORTIR DENGAN</Text>
            </View>
            <View style={styles.radioWrappper}>
              <Text style={styles.heading_style}>Terbaru</Text>
              <RadioButton
                value="first"
                color={'#00b3ff'}
                uncheckedColor={'#a1a1a1'}
                status={sortchecked === 'most_recent' ? 'checked' : 'unchecked'}
                onPress={() => this.sortProducts('most_recent')}
              />
            </View>
            <View style={styles.radioWrappper}>
              <Text style={styles.heading_style}>Unggulan</Text>
              <RadioButton
                value="second"
                color={'#00b3ff'}
                uncheckedColor={'#a1a1a1'}
                status={sortchecked === 'featured' ? 'checked' : 'unchecked'}
                onPress={() => this.sortProducts('featured')}
              />
            </View>
            <View style={styles.radioWrappper}>
              <Text style={styles.heading_style}>Penjualan terbaik</Text>
              <RadioButton
                value="third"
                color={'#00b3ff'}
                uncheckedColor={'#a1a1a1'}
                status={
                  sortchecked === 'best_selling' ? 'checked' : 'unchecked'
                }
                onPress={() => this.sortProducts('best_selling')}
              />
            </View>
            <View style={styles.radioWrappper}>
              <Text style={styles.heading_style}>Harga (Rendah ke Tinggi)</Text>
              <RadioButton
                value="fourth"
                color={'#00b3ff'}
                uncheckedColor={'#a1a1a1'}
                status={
                  sortchecked === 'price_order_asc' ? 'checked' : 'unchecked'
                }
                onPress={() => this.sortProducts('price_order_asc')}
              />
            </View>
            <View style={styles.radioWrappper}>
              <Text style={styles.heading_style}>Harga (Tinggi ke Rendah)</Text>
              <RadioButton
                value="fifth"
                color={'#00b3ff'}
                uncheckedColor={'#a1a1a1'}
                status={
                  sortchecked === 'price_order_desc' ? 'checked' : 'unchecked'
                }
                onPress={() => this.sortProducts('price_order_desc')}
              />
            </View>
          </View>
        </Modal>
        <Modal
          onBackdropPress={() => this.setState({isModalVisible: false})}
          isVisible={this.state.isModalVisible}
          animationIn="slideInLeft"
          animationOut="slideOutLeft"
          animationInTiming={1000}
          animationOutTiming={1000}
          style={styles.bottomModal}
          backdropTransitionInTiming={1000}
          backdropTransitionOutTiming={1000}>
          <View style={{flex: 1}} style={styles.modalWrapper}>
            <TouchableOpacity
              onPress={this._toggleModal}
              style={styles.closeBtn}>
              <Text style={styles.crossColor}>&#10005;</Text>
            </TouchableOpacity>
            {this.state.innerModalContentVisible && (
              <ScrollView>
                <View style={styles.subModal}>
                  <TouchableOpacity onPress={this._toggleInnerModalContent}>
                    <Text style={{fontSize: hp('7%'), marginRight: wp('6.5%')}}>
                      &#8249;
                    </Text>
                  </TouchableOpacity>
                  <Text style={styles.filterText}>{this.state.checked}</Text>
                </View>
                {this.state.checked === 'Browse by Categories' &&
                  this.props.category_details.map((category_detail) => (
                    <View
                      key={category_detail.category_id}
                      style={styles.radioWrappper}>
                      <TouchableOpacity
                        onPress={() =>
                          this.selectSubCategory(category_detail.category_id)
                        }>
                        <Text style={styles.heading_style}>
                          {category_detail.category_name}
                        </Text>
                      </TouchableOpacity>
                      <RadioButton
                        value={category_detail.category_id}
                        color={'#00b3ff'}
                        uncheckedColor={'#a1a1a1'}
                        status={
                          sub_cat_checked === category_detail.category_id
                            ? 'checked'
                            : 'unchecked'
                        }
                        onPress={() =>
                          this.selectSubCategory(category_detail.category_id)
                        }
                      />
                    </View>
                  ))}
                {this.state.checked === 'Brand' &&
                  this.props.brand_details.map((brand_detail) => (
                    <View
                      key={brand_detail.brand_id}
                      style={styles.radioWrappper}>
                      <Text style={styles.heading_style}>
                        {brand_detail.brand_name}
                      </Text>
                      <CheckBoxComponent
                        onChange={(event) =>
                          this.handleBrandsSelected(
                            event,
                            brand_detail.brand_id,
                          )
                        }
                        checked={this.state.checkedItems.get(
                          brand_detail.brand_id,
                        )}
                        checkedColor="#15b9ff"
                        uncheckedColor="rgba(0,0,0,0.5)"
                        checkBoxStyle={styles.checkBoxStyle}
                        iconStyle={styles.iconStyle}
                        labelposition="left"
                        iconSize={25}
                        labelStyle={styles.labelstyle}
                        iconName="iosFill"
                      />
                    </View>
                  ))}
                {this.state.checked === 'Price' &&
                  this.props.price_array != null && (
                    <View>
                      <View style={styles.minMaxWrapper}>
                        <View style={styles.minWrapper}>
                          <Text style={styles.price_Text}>Harga Min</Text>
                          <Text style={styles.price_Value}>
                            {this.props.price_array.minimum_price}
                          </Text>
                        </View>
                        <View style={styles.maxWrapper}>
                          <Text style={styles.price_Text}>Harga Maks</Text>
                          <Text style={styles.price_Value}>
                            {this.props.price_array.maximum_price}
                          </Text>
                        </View>
                      </View>
                      <View style={{paddingLeft: wp('2%')}}>
                        <MultiSlider
                          sliderLength={wp('70%')}
                          min={this.props.price_array.minimum_price}
                          max={this.props.price_array.maximum_price}
                          onValuesChange={this.multiSliderValuesChange}
                          values={[this.state.price_low, this.state.price_high]}
                        />
                      </View>
                      <View style={styles.minMaxWrapper}>
                        <View style={styles.minWrapper}>
                          <Text style={styles.price_Text}>
                            {this.state.price_low}
                          </Text>
                        </View>
                        <View style={styles.maxWrapper}>
                          <Text style={styles.price_Text}>
                            {this.state.price_high}
                          </Text>
                        </View>
                      </View>
                    </View>
                  )}
                {this.state.checked === 'Item Condition' &&
                  this.props.product_condition.map((condition, index) => (
                    <View key={index} style={styles.radioWrappper}>
                      <Text style={styles.heading_style}>
                        {condition.condition_name}
                      </Text>
                      <CheckBoxComponent
                        onChange={(event) =>
                          this.handleConditionsSelected(
                            event,
                            condition.condition_value,
                          )
                        }
                        checked={this.state.checkedConditions.get(
                          condition.condition_value,
                        )}
                        checkedColor="#15b9ff"
                        uncheckedColor="rgba(0,0,0,0.5)"
                        checkBoxStyle={styles.checkBoxStyle}
                        iconStyle={styles.iconStyle}
                        labelposition="left"
                        iconSize={25}
                        labelStyle={styles.labelstyle}
                        iconName="iosFill"
                      />
                    </View>
                  ))}
                {this.state.checked === 'Shipping' && (
                  <View style={styles.radioWrappper}>
                    <Text style={styles.heading_style}>Bebas biaya kirim</Text>
                    <CheckBoxComponent
                      onChange={(event) => this.shippingSelected(event)}
                      checked={this.state.free_shipping}
                      checkedColor="#15b9ff"
                      uncheckedColor="rgba(0,0,0,0.5)"
                      checkBoxStyle={styles.checkBoxStyle}
                      iconStyle={styles.iconStyle}
                      labelposition="left"
                      iconSize={25}
                      labelStyle={styles.labelstyle}
                      iconName="iosFill"
                    />
                  </View>
                )}
                {this.state.checked === 'Availability' && (
                  <View style={styles.radioWrappper}>
                    <Text style={styles.heading_style}>
                      Kecualikan kehabisan stok
                    </Text>
                    <CheckBoxComponent
                      onChange={(event) => this.availabilitySelected(event)}
                      checked={this.state.exclude_out_of_Stock}
                      checkedColor="#15b9ff"
                      uncheckedColor="rgba(0,0,0,0.5)"
                      checkBoxStyle={styles.checkBoxStyle}
                      iconStyle={styles.iconStyle}
                      labelposition="left"
                      iconSize={25}
                      labelStyle={styles.labelstyle}
                      iconName="iosFill"
                    />
                  </View>
                )}
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    paddingTop: 20,
                  }}>
                  <TouchableOpacity
                    onPress={this._toggleInnerModalContent}
                    style={styles.donebtn}>
                    <Text style={styles.btnText}>Selesai</Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            )}
            {!this.state.innerModalContentVisible && (
              <View>
                <View>
                  <Text style={styles.filterText}>FILTER</Text>
                </View>
                {this.props.category_details != null &&
                  this.props.products.length > 0 && (
                    <View style={styles.radioWrappper}>
                      <TouchableOpacity
                        onPress={() =>
                          this.filterProducts('Browse by Categories')
                        }>
                        <Text style={styles.heading_style}>
                          Jelajahi berdasarkan Kategori
                        </Text>
                      </TouchableOpacity>
                      <RadioButton
                        value="sub_categories"
                        color={'#00b3ff'}
                        uncheckedColor={'#a1a1a1'}
                        status={
                          checked === 'Browse by Categories'
                            ? 'checked'
                            : 'unchecked'
                        }
                        onPress={() =>
                          this.filterProducts('Browse by Categories')
                        }
                      />
                    </View>
                  )}
                {this.props.brand_details != null &&
                  this.props.products.length > 0 && (
                    <View style={styles.radioWrappper}>
                      <TouchableOpacity
                        onPress={() => this.filterProducts('Brand')}>
                        <Text style={styles.heading_style}>Merek</Text>
                      </TouchableOpacity>
                      <RadioButton
                        value="brand"
                        color={'#00b3ff'}
                        uncheckedColor={'#a1a1a1'}
                        status={checked === 'Brand' ? 'checked' : 'unchecked'}
                        onPress={() => this.filterProducts('Brand')}
                      />
                    </View>
                  )}
                {this.props.price_array != null &&
                  this.props.products.length > 0 && (
                    <View style={styles.radioWrappper}>
                      <TouchableOpacity
                        onPress={() => this.filterProducts('Price')}>
                        <Text style={styles.heading_style}>Harga</Text>
                      </TouchableOpacity>
                      <RadioButton
                        value="price"
                        color={'#00b3ff'}
                        uncheckedColor={'#a1a1a1'}
                        status={checked === 'Price' ? 'checked' : 'unchecked'}
                        onPress={() => this.filterProducts('Price')}
                      />
                    </View>
                  )}
                {this.props.product_condition != null &&
                  this.props.products.length > 0 && (
                    <View style={styles.radioWrappper}>
                      <TouchableOpacity
                        onPress={() => this.filterProducts('Item Condition')}>
                        <Text style={styles.heading_style}>Kondisi barang</Text>
                      </TouchableOpacity>
                      <RadioButton
                        value="item_condition"
                        color={'#00b3ff'}
                        uncheckedColor={'#a1a1a1'}
                        status={
                          checked === 'Item Condition' ? 'checked' : 'unchecked'
                        }
                        onPress={() => this.filterProducts('Item Condition')}
                      />
                    </View>
                  )}
                {this.props.products.length > 0 && (
                  <View style={styles.radioWrappper}>
                    <TouchableOpacity
                      onPress={() => this.filterProducts('Shipping')}>
                      <Text style={styles.heading_style}>pengiriman</Text>
                    </TouchableOpacity>
                    <RadioButton
                      value="shipping"
                      color={'#00b3ff'}
                      uncheckedColor={'#a1a1a1'}
                      status={checked === 'Shipping' ? 'checked' : 'unchecked'}
                      onPress={() => this.filterProducts('Shipping')}
                    />
                  </View>
                )}
                {this.props.products.length > 0 && (
                  <View style={styles.radioWrappper}>
                    <TouchableOpacity
                      onPress={() => this.filterProducts('Availability')}>
                      <Text style={styles.heading_style}>Ketersediaan</Text>
                    </TouchableOpacity>
                    <RadioButton
                      value="availability"
                      color={'#00b3ff'}
                      uncheckedColor={'#a1a1a1'}
                      status={
                        checked === 'Availability' ? 'checked' : 'unchecked'
                      }
                      onPress={() => this.filterProducts('Availability')}
                    />
                  </View>
                )}
                <View style={styles.bottomBtn}>
                  <TouchableOpacity
                    onPress={this.resetModal}
                    style={styles.resetBtn}>
                    <Text style={styles.btnText}>Setel ulang</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={this.filtersApplied}
                    style={styles.donebtn}>
                    <Text style={styles.btnText}>Cari</Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
          </View>
        </Modal>
      </ScrollView>
    );
  }
}
const styles = {
  minMaxWrapper: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  price_Text: {
    color: '#929292',
    fontSize: hp('2.2%'),
    fontFamily: Font.RobotoRegular,
  },
  price_Value: {
    color: '#545454',
    fontSize: hp('2.7%'),
    fontFamily: Font.RobotoMedium,
  },
  labelstyle: {
    fontSize: hp('2.5%'),
    color: '#545454',
    marginRight: 10,
    marginLeft: 0,
    fontFamily: Font.RobotoRegular,
  },
  subModal: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bgColor: {
    backgroundColor: '#fff',
  },
  listWrapper: {
    backgroundColor: '#fff',
  },
  abc: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    // borderWidth:1,
    marginLeft: 10,
    marginRight: 10,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderColor: '#e7e7e7',
  },
  touchBtn: {
    flexDirection: 'row',
  },
  containerStyle: {
    backgroundColor: '#f00',
    width: '100%',
  },
  banner_top: {
    Width: '100%',
  },
  heading: {
    borderWidth: 1,
    borderColor: '#f00',
    height: 80,
    width: '100%',
  },
  bannerImg: {
    width: '100%',
    height: hp('20%'),
    // height:150
  },
  bottomSection: {
    borderColor: '#e7e7e7',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 30,
  },
  section1: {
    borderColor: '#f00',
    borderRightWidth: 1,
    textAlign: 'center',
    width: '33%',
  },
  buy_btn: {},
  viewStyle: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#e6e6e6',
  },
  textStyle: {
    color: '#6b6b6b',
    // fontSize:hp('2.5%'),
    // fontSize:20,
    fontSize: hp('2.25%'),
  },
  sort_icon: {
    marginRight: 4,
    width: 23,
    height: 11.5,
    marginTop: 7,
  },
  filter_icon: {
    marginRight: 0,
    width: 15,
    height: 15,
    marginTop: 5,
  },
  wrapperStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  // modal
  bottomModal: {
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    margin: 0,
    width: '80%',
    marginTop: 20,
    padding: 10,
  },
  bottomModal2: {
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    margin: 0,
    width: '80%',
    marginTop: 20,
    padding: 10,
    // right:0
    position: 'absolute',
    right: 0,
    height: '100%',
  },

  closeBtn2: {
    position: 'absolute',
    left: -20,
    width: 30,
    height: 30,
    backgroundColor: '#c90305',
    top: -20,
    zIndex: 9999,
    color: '#fff',
    borderRadius: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },

  //   radio btn
  radioWrappper: {
    flexDirection: 'row',
    paddingTop: 8,
    paddingBottom: 8,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  heading_style: {
    color: '#545454',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.35%'),
  },
  bottomBtn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 20,
  },
  resetBtn: {
    width: '47%',
    borderRadius: 30,
    textAlign: 'center',
    backgroundColor: '#00b3ff',
  },
  donebtn: {
    width: '45%',
    borderRadius: 30,
    backgroundColor: '#c90305',
  },
  btnText: {
    textAlign: 'center',
    paddingTop: hp('0.75%'),
    paddingBottom: hp('0.75%'),
    fontFamily: Font.RobotoRegular,
    color: '#fff',
  },
  modalWrapper: {},
  closeBtn: {
    position: 'absolute',
    right: -20,
    width: 30,
    height: 30,
    backgroundColor: '#c90305',
    top: -20,
    zIndex: 9999,
    color: '#fff',
    borderRadius: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  crossColor: {
    textAlign: 'center',
    color: '#fff',
  },
  filterText: {
    paddingTop: 10,
    fontSize: hp('2.7%'),
    color: '#696969',
    fontFamily: Font.RobotoMedium,
    borderColor: '#e7e7e7',
    borderBottomWidth: 2,
    paddingBottom: 5,
  },
  sliderBottom: {
    // paddingBottom:15,
    paddingLeft: 28,
    paddingRight: 15,
    flex: 1,
  },
  titleStyle: {
    color: '#000',
    fontSize: hp(2.4),
    paddingTop: hp(1.5),
    paddingBottom: hp(1),
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),

  },
  soldStyle: {
    paddingHorizontal: wp(3),
    color: '#000',
    fontSize: wp('3.2%'),
    // fontSize:18,
  },
  critiStyle: {
    color: '#00b3ff',
  },
  colorBarStyle: {
    width: wp('4.4%'),
    height: hp('2.2%'),
    borderRadius: 2,
    backgroundColor: '#e67904',
    marginLeft: 8,
  },
  colorBarWrapperStyle: {
    flexDirection: 'row',
    width: '50%',
    flexBasis: '50%',
    // fontSize:18,
    fontSize: hp('2.5%'),
    alignItems: 'center',
  },
  colorBrandWrapper: {
    flexDirection: 'row',
  },
  barTxtStyle: {
    color: '#858585',
    // fontSize:18,
    fontSize: wp('4%'),
  },
  casinoTxtStyle: {
    color: '#c90305',
  },
  imgContentWrapper: {
    flexDirection: 'column',
    // flexBasis:'80%',
    flex: 1,
    padding: 10,
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#e7e7e7',
  },
  imgWrapper: {
    width: wp(70),
    flexDirection: 'row',
    // marginRight: wp(),
  },
  boxImg: {
    width: wp(30),
    height: hp(20),
  },
  shipWrapper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  freeShipImg: {
    width: wp('9%'),
    height: hp('3%'),
  },
  insdown: {
    width: wp('7.7%'),
    height: hp('4%'),
  },
  rightDiv: {
    // paddingLeft:20,
    flexBasis: '50%',
  },
  leftDiv: {
    flexBasis: '50%',
  },
  ratingWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: hp('1%'),
    // paddingTop:hp('1%'),
    // paddingBottom:hp('1%'),
    width: '100%',
    // borderColor:'#e7e7e7',
    // borderTopWidth:1
  },
  lftPrice: {
    flexDirection: 'row',
    alignItems: 'center',
    flexBasis: '50%',
  },
  priceTxt: {
    color: '#00b3ff',
    // fontSize:22,
    fontSize: hp('2.3%'),
    fontWeight: 'bold',
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
  },
  rightRating: {
    width: 85,
    height: 12,
  },
  sfLogo: {
    height: 27,
    marginLeft: 10,
  },

  downTxt: {
    color: '#4abc96',
    fontFamily: Font.RobotoRegular,
  },
  rtPolicy: {
    color: '#ff3d5b',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('1.6%'),
    marginBottom: 2,
  },
  rtAccept: {
    color: '#858585',
    fontSize: hp('1.6%'),
    fontFamily: Font.RobotoRegular,
  },
  address: {
    fontFamily: Font.RobotoRegular,
    color: '#454546',
    fontSize: hp('1.6%'),
    marginTop: hp('0.5%'),
  },
  location: {
    fontFamily: Font.RobotoRegular,
    color: '#000000',
    fontSize: hp('1.6%'),
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.category.loading,
    products: state.category.products,
    no_products: state.category.no_products,
    category_details: state.category.category_details,
    brand_details: state.category.brand_details,
    price_array: state.category.price_array,
    product_condition: state.category.product_condition,
  };
};

export default connect(mapStateToProps, {
  getSearchResults,
  getProductsByCategory,
})(ProductsByCategory);
