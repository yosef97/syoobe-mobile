import React, {Component} from 'react';
import Clipboard from '@react-native-community/clipboard';
import {
  Picker,
  Modal,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {
  CardSection,
  ButtonTwo,
  Spinner,
  Button,
  TextareaInner,
  InputInner,
} from './common';
import {BASE_URL} from '../services/baseUrl';
import {ERROR_MESSAGE} from '../constants/constants';
import {connect} from 'react-redux';
import {getOrderDetails} from '../actions';
import axios from 'axios';
import {
  ProductItemCard,
  Section,
} from './common/view-product/ProductDetailCard';
import {TrackingProduct} from './common/view-product/TrackingProduct';
import {formatRupiah, substr} from '../helpers/helper';
import PTRView from 'react-native-pull-to-refresh';

class ViewOrderPhysical extends Component {
  state = {
    sendMessage: false,
    loading2: false,
    order_id: this.props.route.params.details.order_id,
    order_product_id: this.props.route.params.details.order_product_id,
    _token: this.props.route.params.details._token,
    completeModalDetails: null,
    selectedOrderCompleteStatus: 0,
    sendMessage2: false,
    subject: '',
    message: '',
  };

  sendMessageToShop = () => {
    this.setState({
      sendMessage2: true,
    });
  };

  closeMessageModal = () => {
    this.setState({
      sendMessage2: false,
      subject: '',
      message: '',
    });
  };

  sendMessageContent = async () => {
    if (
      String(this.state.subject).trim() === '' ||
      String(this.state.message).trim() === ''
    ) {
      Alert.alert('Informasi', 'Kedua bidang wajib diisi', [{text: 'OK'}]);
    } else {
      var details = {};
      details = {
        ...details,
        ...{shop_id: this.props.data.shop_id},
        ...details,
        ...{_token: this.state._token},
        ...details,
        ...{thread_subject: this.state.subject},
        ...details,
        ...{message_text: this.state.message},
        ...details,
        ...{user_id: this.props.data.detail.user_id},
      };

      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post(BASE_URL + '/sendMessageToShopOwner', formBody)
        .then((response) => {
          console.log('r', response);
          this.setState({
            sendMessage2: false,
            subject: '',
            message: '',
          });
          if (response.data.status === 1) {
            Alert.alert('Informasi', 'Pesan terkirim!', [{text: 'OK'}]);
          }
        })
        .catch(() => {
          Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
        });
    }
  };

  componentDidMount = () => {
    console.log('componentDidMount');
    this.loadData();
  };

  loadData = () => {
    console.log(
      'this.props.route.params.details',
      this.props.route.params.details,
    );
    this.props.getOrderDetails(this.props.route.params.details);
  };

  dateForStatusHistory = (status, index) => {
    let date;
    let month_no;
    let year;
    let month_name;
    let array = [];
    array = String(status.date).split('-');
    date = array[0];
    month_no = array[1];
    year = array[2];

    switch (month_no) {
      case '01':
        month_name = 'Jan';
        break;
      case '02':
        month_name = 'Feb';
        break;
      case '03':
        month_name = 'Mar';
        break;
      case '04':
        month_name = 'Apr';
        break;
      case '05':
        month_name = 'May';
        break;
      case '06':
        month_name = 'Jun';
        break;
      case '07':
        month_name = 'Jul';
        break;
      case '08':
        month_name = 'Aug';
        break;
      case '09':
        month_name = 'Sept';
        break;
      case '10':
        month_name = 'Oct';
        break;
      case '11':
        month_name = 'Nov';
        break;
      case '12':
        month_name = 'Dec';
        break;
      default:
        month_name = month_no;
        break;
    }
    return (
      <View key={index} style={styles.row_}>
        <View style={styles.dateDiv}>
          <Text style={styles.date}>{date}</Text>
          <Text style={styles.monthYear}>
            {month_name}, {year}
          </Text>
        </View>
        <View style={styles.contentDiv}>
          {String(status.order_status).toLowerCase().includes('delivered') && (
            <View style={styles.btnClass}>
              <Text style={styles.btn_txt}>{status.order_status}</Text>
            </View>
          )}
          {String(status.order_status).toLowerCase().includes('shipped') && (
            <View style={[styles.btnClass, styles.shipped]}>
              <Text style={styles.btn_txt}>{status.order_status}</Text>
            </View>
          )}
          {String(status.order_status).toLowerCase().includes('in process') && (
            <View style={[styles.btnClass, styles.progress]}>
              <Text style={styles.btn_txt}>{status.order_status}</Text>
            </View>
          )}
          {String(status.order_status)
            .toLowerCase()
            .includes('payment confirmed') && (
            <View style={[styles.btnClass, styles.confirmed]}>
              <Text style={styles.btn_txt}>{status.order_status}</Text>
            </View>
          )}
          {String(status.order_status).toLowerCase().includes('completed') && (
            <View style={[styles.btnClass, styles.completed]}>
              <Text style={styles.btn_txt}>{status.order_status}</Text>
            </View>
          )}

          <Text style={styles.descriptionOne}>
            {String(status.comments).replace(/<\/?[^>]+(>|$)/g, '')}
          </Text>
          {status.customer_notified == 'Yes' && (
            <Text style={styles.descriptionTwo}>Kirim Pemberitahuan</Text>
          )}
        </View>
      </View>
    );
  };

  renderStatusHistory = () => {
    if (
      typeof this.props.data.status_history !== 'string' &&
      this.props.data.status_history != null
    ) {
      return (
        <View>
          <Text style={styles.statusHeading}>STATUS HISTORY</Text>
          {this.props.data.status_history.map((status, index) => {
            return this.dateForStatusHistory(status, index);
          })}
        </View>
      );
    }
  };

  completedThisOrder = async () => {
    this.setState({
      loading2: true,
    });
    let details = {
      _token: this.state._token,
      order_id: this.state.order_id,
    };
    let formBody = [];
    for (let property in details) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(
        'https://syoobe.co.id/api/orderCompleteDisplayForPhysical',
        formBody,
      )
      .then((response) => {
        // console.log(response);
        if (response.data.status === 1) {
          this.setState({
            loading2: false,
            completeModalDetails: response.data.data,
            selectedOrderCompleteStatus:
              response.data.data.order_new_status[0].status_id,
          });
          this.setState({
            sendMessage: true,
          });
        }
      })
      .catch((err) => {
        console.log('e', err);
        this.setState({
          loading2: false,
        });
      });
  };

  endOrder = () => {
    this.setState({
      sendMessage: false,
    });
  };

  updateCompleteOrderStatus = async () => {
    this.setState({
      loading2: true,
    });
    let details = {
      _token: this.state._token,
      order_id: this.state.order_id,
      opr_id: this.state.order_product_id,
      status_id: this.state.selectedOrderCompleteStatus,
      vendor_id: this.props.data.vendor_id,
    };
    let formBody = [];
    for (let property in details) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/submitOrderStatusForPhysical', formBody)
      .then((response) => {
        // console.log(response);
        if (response.data.status == 1) {
          this.setState({
            loading2: false,
            sendMessage: false,
          });
          this.loadData();
        } else {
          Alert.alert('Silahkan coba lagi');
          this.setState({
            loading2: false,
          });
        }
      })
      .catch((err) => {
        console.log('e', err);
        this.setState({
          loading2: false,
        });
      });
  };

  handleCopy = (text) => {
    // alert(text);
    Clipboard.setString(text);
    Alert.alert('Berhasil disalin ke clipboard');
  };

  render() {
    // console.log('in state', JSON.stringify(this.state));

    console.log('in props', JSON.stringify(this.props.data));

    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Spinner color="red" size="large" />
        </View>
      );
    }

    return (
      <PTRView onRefresh={() => this.loadData()}>
        <ScrollView style={{flex: 1, backgroundColor: '#f5f6fa'}}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.sendMessage}
            onRequestClose={() => {
              this.endOrder();
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'rgba(0,0,0,0.5)',
              }}>
              <View
                style={{
                  width: wp('80%'),
                  maxHeight: hp('80%'),
                  backgroundColor: '#fff',
                  padding: 20,
                  zIndex: 5,
                  position: 'relative',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 10,
                }}>
                <ScrollView keyboardShouldPersistTaps={'handled'}>
                  <Text
                    style={{
                      color: '#545454',
                      fontSize: hp('3%'),
                      paddingBottom: 0,
                      fontFamily: Font.RobotoRegular,
                      textAlign: 'center',
                    }}>
                    {' '}
                    Detail Pesanan{' '}
                  </Text>
                  <CardSection>
                    <View
                      style={{
                        flexDirection: 'column',
                      }}>
                      <Text style={styles.dropheadingClass}>Nama Produk :</Text>
                      {this.state.completeModalDetails && (
                        <Text style={[styles.totalItem, styles.boldClass]}>
                          {this.state.completeModalDetails.product_name}
                        </Text>
                      )}
                    </View>
                  </CardSection>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.dropheadingClass}>Status</Text>
                  </View>
                  <View style={styles.dropdownClass}>
                    <Picker
                      style={styles.pickerClass}
                      selectedValue={this.state.selectedOrderCompleteStatus}
                      onValueChange={(val) =>
                        this.setState({selectedOrderCompleteStatus: val})
                      }>
                      {this.state.completeModalDetails &&
                        this.state.completeModalDetails.order_new_status.map(
                          (item, index) => {
                            return (
                              <Picker.Item
                                label={item.status_name}
                                value={item.status_id}
                                key={item.status_id}
                              />
                            );
                          },
                        )}
                    </Picker>
                  </View>
                  {this.state.selectedOrderCompleteStatus !== 6 &&
                    this.state.selectedOrderCompleteStatus !== 7 && (
                      <Text
                        style={{
                          color: '#c90305',
                          fontSize: hp('1.8%'),
                        }}>
                        Pesanan ini belum tersedia untuk menyelesaikan pesanan
                        ini, mohon tunggu pesanan ini sampai.
                      </Text>
                    )}
                  {(this.state.selectedOrderCompleteStatus === 6 ||
                    this.state.selectedOrderCompleteStatus === 7) && (
                    <ButtonTwo
                      onPress={() => this.updateCompleteOrderStatus()}
                      style={styles.btn_style}>
                      <Text style={{fontSize: hp('2%')}}>Update</Text>
                    </ButtonTwo>
                  )}
                </ScrollView>
                <TouchableOpacity
                  onPress={() => this.endOrder()}
                  style={styles.closeBtn2}>
                  <Text style={styles.crossColor}>&#10005;</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          {this.props.data != null && (
            <View>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('ProductDetail', {
                    product_id: this.props.data.detail.opr_product_id,
                  })
                }>
                <ProductItemCard
                  {...this.props.route.params.details}
                  data={this.props.data}
                />
              </TouchableOpacity>

              <Section>
                <View style={{}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingBottom: hp(1.5),
                    }}>
                    <Text>Status</Text>
                    <Text style={{fontWeight: 'bold', color: '#000'}}>
                      {this.props.data.order_status}
                    </Text>
                  </View>
                  {this.props.data.tracking_number && (
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Nomor Resi</Text>
                      <Text style={{fontWeight: 'bold', color: '#000'}}>
                        {this.props.data.tracking_number}
                      </Text>
                    </View>
                  )}
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingVertical: hp(1.5),
                      borderTopWidth: 1,
                      borderTopColor: '#f5f6fa',
                    }}>
                    <Text>Tanggal Pembelian</Text>
                    <Text style={{fontWeight: 'bold', color: '#000'}}>
                      {this.props.data.order_date}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingTop: hp(1.5),
                      borderTopWidth: 1,
                      borderTopColor: '#f5f6fa',
                    }}>
                    <Text> Nomor Invoice</Text>
                    <Text
                      onPress={() =>
                        this.handleCopy(this.props.data.order_invoice_number)
                      }>
                      {this.props.data.order_invoice_number}
                    </Text>
                  </View>
                </View>
              </Section>
              <Section>
                <Text
                  style={{
                    fontSize: hp(2),
                    fontWeight: 'bold',
                    color: '#000',
                    paddingBottom: hp(2),
                  }}>
                  Detail Harga
                </Text>
                <View style={{}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingVertical: hp(1.5),
                      borderTopWidth: 1,
                      borderTopColor: '#f5f6fa',
                    }}>
                    <Text>Sub Total</Text>
                    <Text style={{fontWeight: 'bold', color: '#000'}}>
                      {`Rp. ${this.props.data.cart_total}`}
                    </Text>
                  </View>

                  {this.props.data.tax > 0 && (
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Pajak</Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        + Rp. {formatRupiah(this.props.data.tax)}
                      </Text>
                    </View>
                  )}

                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingVertical: hp(1.5),
                      borderTopWidth: 1,
                      borderTopColor: '#f5f6fa',
                    }}>
                    <Text>Biaya pengiriman</Text>
                    <Text style={{fontWeight: 'bold', color: '#000'}}>
                      Rp. {this.props.data.shipping_handling}
                    </Text>
                  </View>
                  {this.props.data.order_discount && (
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>
                        Promo ({this.props.data.detail.order_discount_coupon})
                      </Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>
                        - Rp.{' '}
                        {this.props.data.order_discount
                          ? this.props.data.order_discount
                          : 0}
                      </Text>
                    </View>
                  )}
                  {this.props.data.order_reward && (
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: hp(1.5),
                        borderTopWidth: 1,
                        borderTopColor: '#f5f6fa',
                      }}>
                      <Text>Poin Hadiah</Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          color: '#000',
                        }}>{`-Rp. ${
                        this.props.data.order_reward
                          ? this.props.data.order_reward
                          : 0
                      }`}</Text>
                    </View>
                  )}
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingTop: hp(1.5),
                      borderTopWidth: 1,
                      borderTopColor: '#f5f6fa',
                    }}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: '#000',
                      }}>
                      Total Keseluruhan
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: '#000',
                      }}>
                      Rp. {formatRupiah(this.props.data.grand_total)}
                    </Text>
                  </View>
                </View>
              </Section>
              <Section>
                <Text
                  style={{
                    fontSize: hp(2),
                    fontWeight: 'bold',
                    color: '#000',
                    paddingBottom: hp(1),
                  }}>
                  Alamat Pengiriman
                </Text>
                <Text style={{fontSize: hp(1.7)}}>
                  {this.props.data.shipping_address.name}
                </Text>
                <Text style={{fontSize: hp(1.7)}}>
                  {this.props.data.shipping_address.phone}
                </Text>
                <Text
                  style={{
                    fontSize: hp(1.7),
                  }}>{`${this.props.data.shipping_address.address1}, ${this.props.data.shipping_address.state}, ${this.props.data.shipping_address.city}, ${this.props.data.shipping_address.country} - ${this.props.data.shipping_address.zip}`}</Text>
                {/* {this.props.data.shipping_address.address2.length > 0 && (
                <Text>{this.props.data.shipping_address.address2}</Text>
              )} */}
              </Section>
              {this.props.data.payment_history.map((items) => (
                <Section>
                  <Text
                    style={{
                      fontSize: hp(2),
                      fontWeight: 'bold',
                      color: '#000',
                      paddingBottom: hp(2),
                    }}>
                    Riwayat Pembayaran
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingVertical: hp(1.5),
                      borderTopWidth: 1,
                      borderTopColor: '#f5f6fa',
                    }}>
                    <Text
                      style={{
                        color: '#000',
                      }}>
                      Kode Transaksi
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: '#000',
                      }}
                      onPress={() => this.handleCopy(items.transaction_id)}>
                      {substr(items.transaction_id || '', 25)}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingVertical: hp(1.5),
                      borderTopWidth: 1,
                      borderTopColor: '#f5f6fa',
                    }}>
                    <Text
                      style={{
                        color: '#000',
                      }}>
                      Metode Pembayaran
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: '#000',
                      }}>
                      {`${items.payment_method}`}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingVertical: hp(1.5),
                      borderTopWidth: 1,
                      borderTopColor: '#f5f6fa',
                    }}>
                    <Text
                      style={{
                        color: '#000',
                      }}>
                      Jumlah
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: '#000',
                      }}>
                      Rp. {formatRupiah(parseInt(items.amount))}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingTop: hp(1.5),
                      borderTopWidth: 1,
                      borderTopColor: '#f5f6fa',
                    }}>
                    <Text
                      style={{
                        color: '#000',
                      }}>
                      Komentar
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: '#000',
                      }}>
                      {items.comments}
                    </Text>
                  </View>
                </Section>
              ))}
              <Section>
                {this.props.data.order_status.toLowerCase() ===
                  'pesanan anda telah dikirim' && (
                  <View style={{paddingBottom: hp(1)}}>
                    <Button onPress={this.completedThisOrder}>Selesai</Button>
                  </View>
                )}
                {this.props.data.order_status.toLowerCase() ==
                  'pesanan anda telah diterima ' && (
                  <View style={{paddingBottom: hp(1)}}>
                    <Button onPress={this.completedThisOrder}>Selesai</Button>
                  </View>
                )}
                {this.props.data.order_status ===
                  'Transaksi Ini Sudah Selesai' &&
                  this.props.data.isReview === 0 && (
                    <View style={{paddingBottom: hp(1)}}>
                      <Button
                        onPress={() =>
                          this.props.navigation.navigate('CancelForm', {
                            headerName: 'Penilaian',
                            order_id: this.props.data.order_product_id,
                            type: 'feedback',
                          })
                        }>
                        Penilaian
                      </Button>
                    </View>
                  )}
                {this.props.data.order_status ===
                  'Pesanan Anda Telah Diterima ' &&
                  this.props.data.isReturned === 0 && (
                    <View style={{paddingBottom: hp(1)}}>
                      <Button
                        onPress={() =>
                          this.props.navigation.navigate('CancelForm', {
                            headerName: 'Permintaan Pengembalian',
                            order_id: this.props.data.order_product_id,
                            type: 'return',
                          })
                        }>
                        Pengembalian
                      </Button>
                    </View>
                  )}

                {this.props.data.order_status ===
                  'Pembayaran Sudah Diverifikasi' &&
                  this.props.data.isCancel === 0 && (
                    <View style={{paddingBottom: hp(1)}}>
                      <Button
                        onPress={() =>
                          this.props.navigation.navigate('CancelForm', {
                            headerName: 'Batalkan Pesanan',
                            order_id: this.props.data.order_product_id,
                            type: 'cancel',
                          })
                        }>
                        Batalkan Pesanan
                      </Button>
                    </View>
                  )}
                <View style={{paddingBottom: hp(1)}}>
                  <Button
                    color={'#273c75'}
                    outline
                    onPress={() => this.sendMessageToShop()}
                    disabled={
                      !this.props.data.shop_id &&
                      !this.props.data.detail.user_id
                    }>
                    Kirim Pesan
                  </Button>

                  {/* <Button
                  outline
                  onPress={() =>
                    this.props.navigation.navigate('ChatWebViewNavigate', {
                      userSelected: this.props.data.detail.opr_vendor_username,
                    })
                  }
                  disabled={!this.props.data.detail.opr_vendor_username}>
                  Hubungi Penjual
                </Button> */}
                </View>
                <View>
                  <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.sendMessage2}
                    onRequestClose={() => {
                      this.closeMessageModal();
                    }}>
                    <View
                      style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                      }}>
                      <View
                        style={{
                          width: wp('80%'),
                          maxHeight: hp('80%'),
                          backgroundColor: '#fff',
                          padding: 20,
                          zIndex: 5,
                          position: 'relative',
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderRadius: 10,
                        }}>
                        <ScrollView keyboardShouldPersistTaps={'handled'}>
                          <Text
                            style={{
                              color: '#545454',
                              fontSize: hp('3%'),
                              paddingBottom: 0,
                              fontFamily: Font.RobotoRegular,
                              textAlign: 'center',
                            }}>
                            {' '}
                            Mengirim pesan{' '}
                          </Text>
                          <CardSection>
                            <InputInner
                              value={this.state.subject}
                              onChangeText={(value) =>
                                this.setState({subject: value})
                              }
                              label="Subyek"
                            />
                          </CardSection>
                          <CardSection>
                            <TextareaInner
                              value={this.state.message}
                              onChangeText={(value) =>
                                this.setState({message: value})
                              }
                              label="Pesan"
                            />
                          </CardSection>
                          <View style={styles.btnWrapper}>
                            <ButtonTwo
                              onPress={() => {
                                this.sendMessageContent();
                              }}>
                              Mengirim pesan
                            </ButtonTwo>
                          </View>
                        </ScrollView>
                        <TouchableOpacity
                          onPress={() => this.closeMessageModal()}
                          style={styles.closeBtn2}>
                          <Text style={styles.crossColor}>&#10005;</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </Modal>
                </View>
                {this.props.data.order_status.toLowerCase() ===
                  'pesanan anda telah dikirim' && (
                  <View style={{paddingBottom: hp(1)}}>
                    <TrackingProduct
                      courier={this.props.data.coourier}
                      waybill={this.props.data.tracking_number}
                    />
                  </View>
                )}
              </Section>
            </View>
          )}
        </ScrollView>
      </PTRView>
    );
  }
}
const styles = {
  dropdownClass: {
    // elevation: 1,
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  pickerClass: {
    height: hp('6%'),
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  crossColor: {
    textAlign: 'center',
    color: '#000',
  },
  closeBtn2: {
    position: 'absolute',
    right: 0,
    width: 30,
    height: 30,
    top: 0,
    zIndex: 1002,
    color: '#fff',
    borderRadius: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn_style: {
    paddingLeft: 10,
    paddingRight: 10,
    width: 'auto',
    marginTop: hp('3%'),
  },
  bgColor: {
    backgroundColor: '#fff',
  },
  boxStyle: {
    width: '100%',
    padding: 10,
    position: 'relative',
  },
  imgContentWrapper: {
    flexDirection: 'row',
    flex: 1,
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    paddingBottom: 10,
    justifyContent: 'space-between',
    marginBottom: hp('2%'),
  },
  imgWrapper: {
    marginRight: wp('3%'),
  },
  boxImg: {
    width: hp('10%'),
    height: hp('17%'),
  },
  productName: {
    fontSize: hp('2.9%'),
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },
  availableWrapper: {
    flexDirection: 'row',
    paddingTop: hp('0.4%'),
    paddingBottom: hp('0.4%'),
    justifyContent: 'space-between',
  },
  available: {
    color: '#858585',
    fontSize: hp('2.2%'),
    marginRight: wp('5%'),
    fontFamily: Font.RobotoMedium,
  },
  price: {
    color: '#00b3ff',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoBold,
  },
  count: {
    color: '#00b3ff',
  },
  priceHeading: {
    color: '#696969',
    fontSize: hp('2.5%'),
    fontFamily: Font.RobotoMedium,
  },
  totalPriceWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: hp('1.5%'),
    paddingTop: hp('1.5%'),
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  totalItem: {
    fontSize: wp('4.2%'),
    color: '#6f6f6f',
    fontFamily: Font.RobotoRegular,
  },
  priceTxt: {
    color: '#00b3ff',
    // fontWeight:'bold',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoBold,
  },
  boldClass: {
    fontFamily: Font.RobotoBold,
  },
  productName2: {
    fontSize: wp('4.2%'),
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },
  billingAddress: {
    fontSize: wp('4.3%'),
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
  },
  iconBox: {
    flexDirection: 'row',
    marginTop: 5,
    // alignItems:'center',
  },
  icon: {
    width: 14,
    height: 14,
  },
  iconAddress: {
    width: 13,
    height: 18,
    marginTop: 3,
  },
  iconEmail: {
    width: 14,
    height: 11,
    marginTop: 4,
  },
  iconTxt: {
    fontSize: wp('4%'),
    marginLeft: 8,
  },
  addressWrapper: {
    marginTop: hp('4%'),
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    paddingBottom: hp('3.5%'),
    paddingHorizontal: 10,
  },
  addressDiv: {
    width: '100%',
    paddingRight: 10,
    paddingBottom: hp('2%'),
  },
  statusHeading: {
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    fontSize: hp('2.5%'),
    marginBottom: hp('1%'),
  },
  mt: {
    marginTop: hp('2%'),
  },
  row_: {
    flexDirection: 'row',
    borderColor: '#e7e7e7',
    borderBottomWidth: 1,
    paddingVertical: hp('2%'),
  },
  row_2: {
    flexDirection: 'row',
    paddingVertical: hp('1%'),
  },
  dateDiv: {
    width: '25%',
    // paddingLeft:10
  },
  date: {
    color: '#00b3ff',
    fontFamily: Font.RobotoBold,
    fontSize: hp('4%'),
  },
  monthYear: {
    color: '#7d7d7d',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
  },
  contentDiv: {
    width: '75%',
  },
  btnClass: {
    backgroundColor: '#c90305',
    alignSelf: 'flex-start',
    borderRadius: 5,
    paddingHorizontal: hp('3%'),
    paddingVertical: hp('1.1%'),
    marginBottom: hp('1%'),
    width: wp('50%'),
  },
  shipped: {
    backgroundColor: '#ff7f27',
  },
  progress: {
    backgroundColor: '#a349a4',
  },
  confirmed: {
    backgroundColor: '#308f16',
  },
  completed: {
    backgroundColor: '#00b3ff',
  },
  btn_txt: {
    color: '#ffffff',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
    textAlign: 'center',
  },
  descriptionOne: {
    color: '#7d7d7d',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.5%'),
    marginBottom: hp('1%'),
  },
  descriptionTwo: {
    color: '#4a4a4a',
    fontFamily: Font.RobotoItalc,
    fontSize: hp('2.5%'),
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.orderDetails.loading,
    data: state.orderDetails.data,
    payment_history: state.orderDetails.payment_history,
  };
};

export default connect(mapStateToProps, {getOrderDetails})(ViewOrderPhysical);
