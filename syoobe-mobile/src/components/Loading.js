import React from 'react';
import {View, StatusBar} from 'react-native';
import {Spinner} from './common';

const loader = () => (
  <View
    style={{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    <StatusBar backgroundColor="#C90205" barStyle="light-content" />
    <Spinner color="red" size="large" />
  </View>
);

export default loader;
