import React, {Component} from 'react';
import {
  Image,
  View,
  TouchableOpacity,
  Text,
  Alert,
  AsyncStorage,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {getCategoryList, viewCart, showSearchField} from '../actions';
import PushNotification, {Importance} from 'react-native-push-notification';

class HomePageRightIcons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _token: null,
    };
    this.createDefaultChannels();
    this.retrieveToken()
      .then((_token) => {
        this.props.viewCart({_token});
      })
      .catch((error) => {
        console.log('error', error);
      });
  }

  createDefaultChannels() {
    PushNotification.createChannel({
      channelId: 'com.syoobe', // (required)
      channelName: 'syoobe', // (required)
      channelDescription: 'A default channel', // (optional) default: undefined.
      soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
      importance: Importance.LOW, // (optional) default: Importance.HIGH. Int value of the Android notification importance
      vibrate: false, // (optional) default: true. Creates the default vibration patten if true.
    });
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({_token: userToken});
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  getScreen = async (value) => {
    await this.retrieveToken()
      .then((_token) => {
        if (_token) {
          if (value === 'noti') {
            this.props.navigate('Notifications');
          } else {
            this.props.navigate('MyCart');
          }
        } else {
          Alert.alert('Error', 'Silahkan Login kembali', [
            {
              text: 'OK',
              onPress: () => {
                this.props.navigate('Login');
              },
            },
          ]);
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  renderCount = () => {
    if (this.state._token) {
      if (this.props.cart_count !== null && this.props.cart_count > 0) {
        return <Text style={styles.counter}>{this.props.cart_count}</Text>;
      }
    }
  };

  renderSearchComponent = () => {
    if (this.props.categories.length < 1) {
      this.props.getCategoryList();
    }
    this.props.navigate('SearchScreen');
  };

  showNotification = () => {
    console.log('showNotification');
    // this.props.notifications.map(
    //   (notification) =>
    //     notification.tnd_read_unread_status !== '1' &&
    //     PushNotification.localNotification({
    //       channelId: 'com.syoobe',
    //       title: notification.tnd_notification_title,
    //       message: notification.tnd_notification_msg,
    //       soundName: 'default',
    //     }),
    // );

    this.props.notifications.map(
      (notification) =>
        notification.tnd_read_unread_status !== '1' &&
        PushNotification.localNotificationSchedule({
          channelId: 'com.syoobe',
          title: notification.tnd_notification_title,
          message: notification.tnd_notification_msg,
          // date: new Date(Date.now() + 1000 * 3), // in 24h
          date: new Date(Date.now() + 1000 * 60 * 60 * 24), // in 24h
          soundName: 'default',
        }),
    );
  };

  componentDidMount = () => {
    this.showNotification();
  };

  render() {
    return (
      <View
        style={{
          // marginRight:5,
          flexDirection: 'row',
        }}>
        {this.props.onlyCart != true && (
          <TouchableOpacity onPress={() => this.renderSearchComponent()}>
            <Image
              source={require('../images/search_Icon.png')}
              style={{width: wp('5%'), height: wp('5%'), marginRight: wp('3%')}}
            />
          </TouchableOpacity>
        )}
        {this.props.onlyCart != true && (
          <TouchableOpacity onPress={() => this.getScreen('noti')}>
            <Image
              source={require('../images/notificationIcon.png')}
              style={{width: wp('5%'), height: wp('5%'), marginRight: wp('3%')}}
            />
            {this.props.unreadNotificationCount !== null &&
              this.props.unreadNotificationCount > 0 && (
                <Text style={styles.counter}>
                  {this.props.unreadNotificationCount}
                </Text>
              )}
          </TouchableOpacity>
        )}
        <TouchableOpacity onPress={() => this.getScreen('cart')}>
          <Image
            source={require('../images/cartIcon.png')}
            style={{
              width: wp('5.4%'),
              height: wp('5%'),
              marginRight: wp('3.3%'),
            }}
          />
          {this.renderCount()}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  counter: {
    position: 'absolute',
    backgroundColor: '#fff',
    width: 23,
    height: 15,
    borderRadius: 20,
    textAlign: 'center',
    fontSize: 10,
    color: '#C90205',
    right: 2,
    top: -4,
  },
};

const mapStateToProps = (state) => {
  return {
    cart_count: state.cart.cart_count,
    categories: state.category.categories,
    showSearch: state.home.showSearch,
    unreadNotificationCount: state.notification.unreadNotificationCount,
    notifications: state.notification.notifications,
  };
};

export default connect(mapStateToProps, {
  getCategoryList,
  viewCart,
  showSearchField,
})(HomePageRightIcons);
