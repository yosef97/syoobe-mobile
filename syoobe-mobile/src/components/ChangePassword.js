import React, {Component} from 'react';
import {
  AsyncStorage,
  TouchableOpacity,
  Text,
  ScrollView,
  Alert,
} from 'react-native';
import {Button, Card, CardSection, Spinner} from './common';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {
  currentPassword,
  newPassword,
  confirmNewPassword,
  saveChangePassword,
} from '../actions';
import InputText from './common/InputText';

class ChangePassword extends Component {
  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  changeCurrentPassword = (text) => {
    this.props.currentPassword(text);
  };

  changeNewPassword = (text) => {
    this.props.newPassword(text);
  };

  changeConfirmNewPassword = (text) => {
    this.props.confirmNewPassword(text);
  };

  handleChangePassword = async () => {
    if (
      this.props.currentPassword !== '' &&
      this.props.newPassword !== '' &&
      this.props.confirmNewPassword !== ''
    ) {
      await this.retrieveToken().then((_token) => {
        this.props.saveChangePassword(
          this.props.navigation,
          _token,
          this.props.current_password,
          this.props.new_password,
          this.props.confirm_new_password,
        );
      });
    } else {
      Alert.alert('Mohon lengkapi data anda');
    }
  };

  render() {
    if (this.props.loading) {
      return <Spinner color="red" size="large" />;
    }

    return (
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        style={styles.scrollClass}>
        <Card>
          <CardSection>
            <InputText
              value={this.props.currentPassword}
              secureTextEntry={true}
              onChangeText={this.changeCurrentPassword.bind(this)}
              placeholder=""
              label="Kata sandi saat ini"
            />
          </CardSection>
          <CardSection>
            <InputText
              value={this.props.newPassword}
              secureTextEntry={true}
              onChangeText={this.changeNewPassword.bind(this)}
              placeholder=""
              label="Kata sandi baru"
            />
          </CardSection>
          <CardSection>
            <InputText
              value={this.props.confirmNewPassword}
              secureTextEntry={true}
              onChangeText={this.changeConfirmNewPassword.bind(this)}
              placeholder=""
              label="Konfirmasi password baru"
            />
          </CardSection>
          <Button
            onPress={() => {
              this.handleChangePassword();
            }}>
            Simpan perubahan
          </Button>
        </Card>
      </ScrollView>
    );
  }
}
const styles = {
  errorTextStyle: {
    fontSize: wp('4%'),
    alignSelf: 'center',
    // color: 'red'
  },

  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
  },
};

const mapStateToProps = (state) => {
  return {
    current_password: state.profile.current_password,
    new_password: state.profile.new_password,
    confirm_new_password: state.profile.confirm_new_password,
    loading: state.profile.loading,
  };
};

export default connect(mapStateToProps, {
  saveChangePassword,
  currentPassword,
  newPassword,
  confirmNewPassword,
})(ChangePassword);
