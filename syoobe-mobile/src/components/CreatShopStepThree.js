import React, {Component} from 'react';
import {Alert, Text, Image, View, ScrollView, Switch} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Button} from './common';
import {connect} from 'react-redux';
import {Font} from './Font';
import {
  skip_step_3,
  change_welcome_message,
  change_payment_policy,
  change_delivery_policy,
  change_refund_policy,
  additional_information,
  seller_information,
  shoping_npwp,
  shop_is_prod_tax,
} from '../actions';
import InputText from './common/InputText';

class CreatShopStepThree extends Component {
  static navigationOptions = ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
    headerTitle: (
      <Text style={{fontSize: hp('2.5%'), color: '#fff'}}>Buat Toko</Text>
    ),
  });

  toggleSwitch = () => {
    this.props.shop_is_prod_tax();
  };

  welcomeMessage = (text) => {
    this.props.change_welcome_message(text);
  };

  paymentPolicy = (text) => {
    this.props.change_payment_policy(text);
  };

  deliveryPolicy = (text) => {
    this.props.change_delivery_policy(text);
  };

  refundPolicy = (text) => {
    this.props.change_refund_policy(text);
  };

  additionalInfo = (text) => {
    this.props.additional_information(text);
  };

  sellerInfo = (text) => {
    this.props.seller_information(text);
  };

  shopNpwp = (text) => {
    this.props.shoping_npwp(text);
  };

  goToSetp4 = () => {
    let paternNpwp = /^[a-zA-Z0-9]{15}$/;
    if (this.props.welcome_message === '') {
      Alert.alert('Informasi', 'Tambahkan Pesan Selamat Datang', [
        {text: 'OK'},
      ]);
    } else if (this.props.payment_policy === '') {
      Alert.alert('Informasi', 'Tambahkan kebijakan pembayaran', [
        {text: 'OK'},
      ]);
    } else if (this.props.delivery_policy === '') {
      Alert.alert('Informasi', 'Tambahkan Kebijakan Pengiriman', [
        {text: 'OK'},
      ]);
    } else if (this.props.refund_policy === '') {
      Alert.alert('Informasi', 'Tambahkan Kebijakan Pengembalian', [
        {text: 'OK'},
      ]);
    } else if (this.props.additional_info === '') {
      Alert.alert('Informasi', 'Masukkan info tambahan', [{text: 'OK'}]);
    } else if (this.props.seller_info === '') {
      Alert.alert('Informasi', 'Tambahkan info penjual', [{text: 'OK'}]);
    } else if (
      this.props.shop_npwp !== '' &&
      this.props.shop_npwp !== undefined
    ) {
      if (!this.props.shop_npwp.match(paternNpwp)) {
        Alert.alert('Informasi', 'NPWP hanya 15 karakter', [{text: 'OK'}]);
      } else {
        this.props.navigation.navigate('CreatShopStepFour');
      }
    } else {
      this.props.navigation.navigate('CreatShopStepFour');
    }
  };

  render() {
    // console.log('ini true false', JSON.stringify(this.props.shop_npwp !== ''));
    // console.log('ini shoping_npwp npwp', JSON.stringify(this.props.shop_npwp));
    // console.log('ini shop npwp', JSON.stringify(!this.props.shop_npwp));
    // console.log(
    //   'ini tru false asli',
    //   this.props.shop_npwp !== '' && this.props.shop_npwp !== undefined,
    // );


    return (
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        style={styles.scrollClass}>
        <Text style={styles.shopHeading}>Informasi Toko</Text>

        <View style={styles.processImgWrapper}>
          <Image
            style={styles.processImg}
            source={require('../images/processImg2.png')}
          />
        </View>

        <View style={styles.imgBtnWrapper}>
          <Text style={styles.doneTxt}>Info & Penampilan</Text>
          <Text style={[styles.doneTxt, styles.completeColor]}>
            Kebijakan Toko
          </Text>
          <Text style={styles.doneTxt}>Informasi Toko SEO</Text>
        </View>

        <View style={styles.viewStyle}>
          <View style={styles.textAreaWrapper}>
            <InputText
              value={this.props.welcome_message}
              onChangeText={this.welcomeMessage.bind(this)}
              label="Pesan Selamat Datang"
              multiline
              numberOfLines={4}
            />
            <Text style={styles.bottom_Text}>
              Informasi umum, filosofi, dll.
            </Text>
          </View>
          {/* <Text style={ styles.errorTextStyle}></Text> */}

          <View style={styles.textAreaWrapper}>
            <InputText
              value={this.props.payment_policy}
              onChangeText={this.paymentPolicy.bind(this)}
              label="Kebijakan Pembayaran"
              multiline
              numberOfLines={4}
            />
            <Text style={styles.bottom_Text}>
              Metode pembayaran, ketentuan, tenggat waktu, pajak, kebijakan
              pembatalan, dll.
            </Text>
          </View>
          <View style={styles.textAreaWrapper}>
            <InputText
              value={this.props.delivery_policy}
              onChangeText={this.deliveryPolicy.bind(this)}
              label="Kebijakan Pengiriman"
              multiline
              numberOfLines={4}
            />
            <Text style={styles.bottom_Text}>
              Metode pengiriman, peningkatan, tenggat waktu, asuransi,
              konfirmasi, bea cukai internasional, dll.
            </Text>
          </View>
          <View style={styles.textAreaWrapper}>
            <InputText
              value={this.props.refund_policy}
              onChangeText={this.refundPolicy.bind(this)}
              label="Kebijakan Pengembalian Produk"
              multiline
              numberOfLines={4}
            />
            <Text style={styles.bottom_Text}>
              Ketentuan, barang yang memenuhi syarat, kerusakan, kerugian, dll.
            </Text>
          </View>
          <View style={styles.textAreaWrapper}>
            <InputText
              value={this.props.additional_info}
              onChangeText={this.additionalInfo.bind(this)}
              label="Informasi Tambahan"
              multiline
              numberOfLines={4}
            />
            <Text style={styles.bottom_Text}>
              Kebijakan tambahan, FAQ, pesanan khusus, grosir & pengiriman,
              jaminan, dll.
            </Text>
          </View>
          <View style={styles.textAreaWrapper}>
            <InputText
              value={this.props.seller_info}
              onChangeText={this.sellerInfo.bind(this)}
              label="Informasi Penjual"
              multiline
              numberOfLines={4}
            />
            <Text style={styles.bottom_Text}>
              Beberapa negara memerlukan informasi penjual seperti nama Anda,
              alamat fisik, alamat email kontak dan, jika berlaku, nomor
              identifikasi pajak.
            </Text>
          </View>
          <View style={styles.toggleContainer}>
            <Text style={styles.toggleTitle}>Pengusaha Kena Pajak</Text>
            <View style={styles.switchContainer}>
              <Text style={styles.errorTextStyle}>Tidak Aktif</Text>
              <Switch
                onValueChange={() => this.toggleSwitch()}
                value={this.props.is_prod_tax}
                trackColor={'rgba(0, 179, 255, 0.5)'}
                thumbColor={'#00b3ff'}
              />
              <Text style={styles.errorTextStyle}>Aktif</Text>
            </View>
          </View>
          <View style={styles.textAreaWrapper}>
            <InputText
              value={this.props.shop_npwp}
              onChangeText={this.shopNpwp.bind(this)}
              label="Informasi NPWP"
              multiline
              numberOfLines={4}
            />
            <Text style={styles.bottom_Text}>
              Pengisian NPWP digunakan untuk lapor pajak PPh 23 sebesar 2%. Jika
              NPWP tidak diisi atau tidak mempunyai NPWP maka untuk produk
              jualan kategori LAYANAN JASA akan dikenakan PPh sebesar 4%
            </Text>
          </View>
          <View style={styles.btnWrapper}>
            <Button onPress={() => this.goToSetp4()}>Lanjut</Button>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  switchContainer: {
    flexDirection: 'row',
    width: wp('94%'),
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 10,
  },
  switchBottomTxt: {
    fontFamily: Font.RobotoLight,
    color: '#8f8f8f',
    fontSize: wp('4%'),
  },
  toggleTitle: {
    fontSize: wp('3.5%'),
    fontFamily: Font.RobotoRegular,
    marginTop: 13,
  },
  toggleContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: hp('3%'),
  },
  errorTextStyle: {
    fontSize: wp('3.5%'),
    alignSelf: 'center',
    // color: 'red',
    fontFamily: Font.RobotoLight,
  },

  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
    // padding:10
  },
  shopHeading: {
    fontSize: wp('6%'),
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 15,
    color: '#696969',
    fontFamily: Font.RobotoRegular,
  },
  processImgWrapper: {
    paddingLeft: wp('7%'),
    marginRight: 5,
  },
  processImg: {
    // width:'100%',
    width: wp('86%'),
    height: wp('15.5%'),
  },
  imgBtnWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderWidth:1,
    // borderColor:'#f00',
    width: '100%',
    flexWrap: 'wrap',
    marginBottom: 15,
    marginTop: 8,
  },
  doneTxt: {
    width: wp('28%'),
    fontSize: wp('4%'),
    color: '#969696',
    fontFamily: Font.RobotoMedium,
    // justifyContent:'center',
    // alignItems:'center',
    textAlign: 'center',
  },
  completeColor: {
    color: '#424242',
  },
  viewStyle: {
    marginleft: 5,
    marginRight: 5,
    paddingLeft: 10,
    paddingRight: 10,
  },
  bottom_Text: {
    marginTop: 5,
    color: '#8f8f8f',
    fontFamily: Font.RobotoLight,
    fontSize: wp(3),
    // paddingLeft: 10,
  },
  textAreaWrapper: {
    marginBottom: hp('2%'),
  },
  btnWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 0,
    paddingBottom: 15,
  },
};

const mapStateToProps = (state) => {
  return {
    welcome_message: state.createShop.welcome_message,
    payment_policy: state.createShop.payment_policy,
    delivery_policy: state.createShop.delivery_policy,
    refund_policy: state.createShop.refund_policy,
    additional_info: state.createShop.additional_info,
    seller_info: state.createShop.seller_info,
    shop_npwp: state.createShop.shop_npwp,
    is_prod_tax: state.createShop.is_prod_tax,
  };
};

export default connect(mapStateToProps, {
  skip_step_3,
  shop_is_prod_tax,
  change_welcome_message,
  change_payment_policy,
  change_delivery_policy,
  change_refund_policy,
  additional_information,
  seller_information,
  shoping_npwp,
})(CreatShopStepThree);
