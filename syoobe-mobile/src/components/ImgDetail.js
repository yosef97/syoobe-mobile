import React, {Component} from 'react';
import ImageViewer from 'react-native-image-zoom-viewer';

export default class ImgDetail extends Component {
  images = [];
  index;
  constructor(props) {
    super(props);

    this.props.route.params.images.forEach((source) => {
      this.images.push({url: source});
    });
    this.index = this.props.route.params.index;
  }

  render() {
    // console.log('ini state', this.state);
    // console.log('ini props', this.props);

    return <ImageViewer index={this.index} imageUrls={this.images} />;
  }
}
