import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Picker} from 'react-native';
import {connect} from 'react-redux';
import {getStates} from '../actions';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';

class SelectDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      PickerSelectedVal: props.selected,
    };
  }

  getStates = (itemValue, items) => {
    items.forEach((item) => {
      if (item.props.value == itemValue) {
        this.props.getStates({country_id: item.key});
      }
    });
    this.setState({PickerSelectedVal: itemValue});
  };

  render() {
    let items;
    if (this.props.items) {
      items = this.props.items.map((value) => {
        return (
          <Picker.Item key={value.id} value={value.name} label={value.name} />
        );
      });
    }

    return (
      <View>
        <Text style={styles.dropHeading}>{this.props.heading}</Text>
        <View style={styles.selectWrapper}>
          <Picker
            style={styles.pickerStyle}
            selectedValue={this.state.PickerSelectedVal}
            onValueChange={(itemValue) => this.getStates(itemValue, items)}>
            {items}
          </Picker>
        </View>
      </View>
    );
  }
}

const styles = {
  selectWrapper: {
    elevation: 3,
    borderRadius: 50,
    marginBottom: 17,
    paddingLeft: 10,
  },
  dropHeading: {
    fontSize: wp('4%'),
    marginBottom: 5,
    marginLeft: 4,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  pickerStyle: {
    height: hp('8%'),
  },
};

const mapStateToProps = (state) => {
  return {};
};

// export default connect(mapStateToProps, {changeCountry})(SelectDropdown);
export default connect(mapStateToProps, {getStates})(SelectDropdown);
