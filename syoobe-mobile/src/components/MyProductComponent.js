import React, {Component} from 'react';
import {
  Alert,
  AsyncStorage,
  Text,
  Image,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {ProductRightMenu} from './product';
import {connect} from 'react-redux';
import {deleteSellerProduct, changeProductStatus} from '../actions';
import {formatRupiah} from '../helpers/helper';

class MyProductComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showComponmentB: false,
    };
  }
  _toggleShow = async () => {
    // await this.setState({showComponmentB: !this.state.showComponmentB});
    // if(this.props)
    const {product_id, sideMenu} = this.props.details;
    this.props.sideMenuToggle(sideMenu, product_id);
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  toProductDetail = (product_id) => {
    this.props.navigation.push('ProductDetail', {
      product_id: product_id,
      user_product: 'true',
      product_requires_shipping: this.props.details.prod_requires_shipping,
    });
  };

  onPublishItem = (product_id) => {
    this.retrieveToken()
      .then((_token) => {
        var product_status = 0;
        var details = {
          _token: _token,
          product_id: product_id,
          product_status: product_status,
        };
        this.props.changeProductStatus(details, 'pause');
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  onPauseItem = (product_id) => {
    this.retrieveToken()
      .then((_token) => {
        var product_status = 1;
        var details = {
          _token: _token,
          product_id: product_id,
          product_status: product_status,
        };
        this.props.changeProductStatus(details, 'active');
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  onRemoveItemFromActive = async (product_id, product_name) => {
    Alert.alert(
      'Confirmation Message',
      'Are you sure want to delete the product ' + product_name + ' ?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('cancelled'),
          style: 'cancel',
        },
        {
          text: 'Yes Delete',
          onPress: () => {
            this.retrieveToken()
              .then((_token) => {
                var details = {
                  _token: _token,
                  product_id: product_id,
                };
                this.props.deleteSellerProduct(details, 'active');
              })
              .catch((error) => {
                console.log('error', error);
              });
          },
        },
      ],
    );
  };

  onRemoveItemFromPaused = async (product_id, product_name) => {
    Alert.alert(
      'Confirmation Message',
      'Are you sure want to delete the product ' + product_name + ' ?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('cancelled'),
          style: 'cancel',
        },
        {
          text: 'Yes Delete',
          onPress: () => {
            this.retrieveToken()
              .then((_token) => {
                var details = {
                  _token: _token,
                  product_id: product_id,
                };
                this.props.deleteSellerProduct(details, 'pause');
              })
              .catch((error) => {
                console.log('error', error);
              });
          },
        },
      ],
    );
  };

  onEditItem = (id) => {
    this._toggleShow();
    this.props.navigation.navigate('AddProduct', {prod_id: id});
  };

  render() {
    const {
      product_id,
      product_image,
      product_name,
      product_price,
      price_currency,
      product_available,
    } = this.props.details;
    const {
      boxImg,
      available,
      price,
      availableWrapper,
      productName,
      rgtMenuIcon,
      imgContentWrapper,
      count,
      imgWrapper,
    } = styles;
    return this.props.isPaused != true ? (
      <TouchableOpacity
        disabled={this.props.outerTap}
        onPress={() => this.toProductDetail(product_id)}>
        <View style={styles.boxStyle}>
          {this.props.toggleInnerTap && (
            <ProductRightMenu
              remove={() =>
                this.onRemoveItemFromActive(product_id, product_name)
              }
              onPause={() => this.onPauseItem(product_id)}
              isPaused={false}
              onEdit={() => this.onEditItem(product_id)}
            />
          )}
          <View style={imgContentWrapper}>
            <View style={imgWrapper}>
              <Image style={boxImg} source={{uri: product_image}} />
            </View>
            <View style={{flex: 1}}>
              <Text style={productName}>{product_name}</Text>
              <View style={availableWrapper}>
                <Text style={available}>
                  Tersedia:
                  <Text style={count}>{product_available}</Text>
                </Text>
                <Text style={available}>
                  Terjual: <Text style={count}>0</Text>
                </Text>
              </View>
              <Text style={price}>
                {price_currency}
                {formatRupiah(product_price)}
              </Text>
            </View>
          </View>
          {this.props.fromSeller ? (
            <TouchableOpacity
              style={{
                height: hp('10%'),
                width: wp('5%'),
                alignItems: 'center',
              }}
              onPress={this._toggleShow}>
              <Image
                style={rgtMenuIcon}
                source={require('../images/rgtMenuIcon.png')}
              />
            </TouchableOpacity>
          ) : null}
        </View>
      </TouchableOpacity>
    ) : (
      <View
        style={this.props.toggleInnerTap ? styles.boxStyle2 : styles.boxStyle}>
        {this.props.toggleInnerTap && (
          <ProductRightMenu
            onEdit={() => this.onEditItem(product_id)}
            remove={() => this.onRemoveItemFromPaused(product_id, product_name)}
            onPublish={() => this.onPublishItem(product_id)}
            isPaused={true}
          />
        )}
        <View style={imgContentWrapper}>
          <View style={imgWrapper}>
            <Image style={boxImg} source={{uri: product_image}} />
          </View>
          <View style={{flex: 1}}>
            <Text style={productName}>{product_name}</Text>
            <View style={availableWrapper}>
              <Text style={available}>
                Available:
                <Text style={count}>{product_available}</Text>
              </Text>
              <Text style={available}>
                Sold: <Text style={count}>0</Text>
              </Text>
            </View>
            <Text style={price}>
              {price_currency}
              {product_price}
            </Text>
          </View>
        </View>
        {this.props.fromSeller ? (
          <TouchableOpacity
            style={{
              height: hp('10%'),
              width: wp('5%'),
              alignItems: 'center',
            }}
            onPress={this._toggleShow}>
            <Image
              style={rgtMenuIcon}
              source={require('../images/rgtMenuIcon.png')}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }
}

const styles = {
  boxStyle: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    flexDirection: 'row',
    flexWarp: 'warp',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
  },
  boxStyle2: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
    zIndex: 1,
  },
  boxImg: {
    width: hp('15%'),
    height: hp('13%'),
    resizeMode: 'contain',
  },
  imgWrapper: {
    width: hp('15%'),
    marginRight: wp('3%'),
  },
  available: {
    color: '#858585',
    fontSize: wp('4.2%'),
    marginRight: wp('10%'),
    fontFamily: Font.RobotoMedium,
  },
  price: {
    color: '#00b3ff',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoBold,
  },
  availableWrapper: {
    flexDirection: 'row',
    paddingTop: hp('0.4%'),
    paddingBottom: hp('0.4%'),
  },
  rgtMenuIcon: {
    width: wp('1.6%'),
    height: wp('7.5%'),
  },
  imgContentWrapper: {
    flexDirection: 'row',
    flex: 1,
    paddingRight: 15,
  },
  productName: {
    fontSize: wp('4.5%'),
    color: '#545454',
    fontFamily: Font.RobotoBold,
  },
  count: {
    color: '#00b3ff',
  },
};

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {
  deleteSellerProduct,
  changeProductStatus,
})(MyProductComponent);
