import React, {Component} from 'react';
import {Text, Image, View, ScrollView} from 'react-native';
import {ProductCardSection} from './product';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {ButtonTwo} from './common';
import axios from 'axios';
import FlashMessage from 'react-native-flash-message';
import {StackActions, CommonActions} from '@react-navigation/native';
import {showMessage} from 'react-native-flash-message';

export default class ThankYou extends Component {
  // state = {
  //   _token: this.props.route.params?._token,
  //   orders: this.props.route.params.orders,
  //   order_id: this.props.route.params.order_id,
  // };
  yarn;
  goToOrders = () => {
    this.props.navigation.navigate('My Orders');
  };

  toOrders = () => {
    showMessage({
      message: 'Success',
      description: `Order Placed!`,
      type: 'success',
    });
    this.props.navigation.replace('HomeDrawer', {screen: 'MyOrderList'});
  };

  componentDidMount = async () => {
    setTimeout(() => {
      this.toOrders();
    }, 2000);
  };

  render() {
    const {
      product_img,
      viewStyle,
      product_text,
      product_price,
      buy_btn,
      like_icon,
      watchIcon,
    } = styles;
    return (
      <ScrollView style={styles.bgColor}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: wp(5),
          }}>
          <Image
            style={styles.thankYouImg}
            source={require('../images/success.png')}
          />
          <View style={{marginTop: hp(5)}}>
            <Text style={styles.thankYouTxt}>Terima Kasih</Text>
            <Text style={styles.successfullyTxt}>
              Pesanan Kamu telah kami teruskan ke penjual. Kamu akan menerima
              pemberitahuan untuk proses selanjutnya.
            </Text>
          </View>
        </View>
        {/* <View style={{padding: 5}}>
                    <ButtonTwo onPress={() => this.goToOrders()}>View your Orders</ButtonTwo>
                </View> */}
        {/* <View style={{paddingLeft:10,paddingRight:10,backgroundColor:'#fff'}}>
                    <Text style={styles.txtStyle}>CUSTOMERS WHO BOUGHT THIS ALSO BOUGHT</Text>
                    <ProductCardSection>
                    <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={styles.abc}>
                    <View style= {viewStyle}> 
                    <Image style={ product_img} source={require('../images/w1.png')}/>
                        <Text style={ product_text}>Reebok Original Blue Sporty Watch</Text>
                        <Text style={ product_price}>$500.000</Text>
                        <Image style={ like_icon} source={require('../images/like_icon.png')}/>
                    </View>
                    <View style= {viewStyle}> 
                        <Image style={ product_img} source={require('../images/w1.png')}/>
                        <Text style={ product_text}>Reebok Original Blue Sporty Watch</Text>
                        <Text style={ product_price}>$500.000</Text>
                        <Image style={ like_icon} source={require('../images/like_icon.png')}/>
                    </View>
                    <View style= {viewStyle}> 
                        <Image style={ product_img} source={require('../images/w1.png')}/>
                        <Text style={ product_text}>Reebok Original Blue Sporty Watch</Text>
                        <Text style={ product_price}>$500.000</Text>
                        <Image style={ like_icon} source={require('../images/like_icon.png')}/>
                    </View>
                    <View style= {viewStyle}> 
                        <Image style={ product_img} source={require('../images/w1.png')}/>
                        <Text style={ product_text}>Reebok Original Blue Sporty Watch</Text>
                        <Text style={ product_price}>$500.000</Text>
                        <Image style={ like_icon} source={require('../images/like_icon.png')}/>
                    </View>
                    </ScrollView>
                    </ProductCardSection>
                </View> */}
      </ScrollView>
    );
  }
}
const styles = {
  bgColor: {
    flex: 1,
    backgroundColor: '#C90205',
  },
  abc: {
    overflow: 'hidden',
    backgroundColor: '#fff',
  },
  viewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    width: wp('50%'),
    bottom: 0,
    right: 0,
    color: 'ffffff',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    paddingLeft: 15,
    paddingRight: 15,
    borderRightWidth: 1,
    borderColor: '#e7e7e7',
    position: 'relative',
  },
  product_img: {
    width: wp('20%'),
    height: wp('30%'),
  },
  product_text: {
    color: '#545454',
    fontSize: wp('3%'),
    width: '100%',
    textAlign: 'center',
    marginTop: wp('2%'),
    marginBottom: wp('2%'),
  },
  product_price: {
    color: '#00b3ff',
    fontWeight: 'bold',
    marginBottom: wp('2%'),
    fontSize: wp('2.5%'),
  },
  like_icon: {
    position: 'absolute',
    top: 12,
    right: 12,
    width: wp('5%'),
    height: wp('4.5%'),
  },
  buy_btn: {
    width: wp('22%'),
    height: wp('6.5%'),
    borderColor: '#c7c7c7',
    borderWidth: 1,
    borderRadius: 50,
    textAlign: 'center',
    color: '#c90305',
    fontSize: wp('3%'),
    lineHeight: wp('6%'),
  },
  txtStyle: {
    fontSize: hp('2.2%'),
    color: '#696969',
    marginBottom: hp('2%'),
  },
  thankYouImg: {
    width: hp('16%'),
    height: hp('16%'),
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: hp('5%'),
    marginBottom: hp('2%'),
  },
  thankYouTxt: {
    textAlign: 'center',
    fontSize: hp('5%'),
    color: '#fff',
    fontFamily: Font.RobotoBold,
  },
  successfullyTxt: {
    textAlign: 'center',
    color: '#fff',
    fontSize: hp(2),
    paddingTop: hp(1.5),
    fontFamily: Font.RobotoRegular,
  },
  topSection: {
    marginBottom: hp('4%'),
    // borderWidth: 1,sss
    flex: 1,
    // borderColor: '#f00',
    alignItems: 'center',
    justifyContent: 'center',
  },
};
