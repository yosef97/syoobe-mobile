import React, {Component} from 'react';
import {Text, TouchableHighlight, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

class PaymentFailed extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
        }}>
        <AntDesign name={'frowno'} size={hp(30)} color={'#ee5253'} />
        <Text style={{paddingTop: hp(5), fontSize: hp(3)}}>
          Pembayaran Gagal Dilakukan!
        </Text>
        <TouchableHighlight
          onPress={() =>
            this.props.navigation.navigate('HomeDrawer', {screen: 'Home'})
          }>
          <View
            style={{
              marginTop: hp(2),
              paddingVertical: hp(1.5),
              paddingHorizontal: wp(5),
              borderColor: '#0abde3',
              borderWidth: 2,
              borderRadius: 10,
              // width: wp(90),
            }}>
            <Text style={{fontSize: hp(2)}}>Kembali Ke Home</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

export default PaymentFailed;
