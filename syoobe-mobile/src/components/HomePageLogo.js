import React from 'react';
import {Image} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default class HomePageLogo extends React.Component {
  render() {
    return (
      <Image
        style={{
          width: wp('33%'),
          height: wp('4.5%'),
          marginLeft: 'auto',
          marginRight: 'auto',
        }}
        source={require('../images/headerLogo.png')}
      />
    );
  }
}
