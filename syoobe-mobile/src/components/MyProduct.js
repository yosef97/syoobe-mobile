import React, {Component} from 'react';
import {
  Text,
  Image,
  Button,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import {ProductFilterSort, ProductListFeature} from './product';
import HomePageLogo from './HomePageLogo';
import HomePageRightIcons from './HomePageRightIcons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Spinner} from './common';
import {getProductsByCategory} from '../actions';
import {connect} from 'react-redux';
import MyProductComponent from './MyProductComponent';

class MyProduct extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
    const category_id = this.props.route.params.category_id;
    this.props.getProductsByCategory({category_id});
  };

  static navigationOptions = ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
    headerTitle: (
      <TouchableOpacity
        style={{
          width: wp('33%'),
          height: wp('4.5%'),
          marginLeft: 'auto',
          marginRight: 'auto',
        }}>
        <HomePageLogo />
      </TouchableOpacity>
    ),

    headerRight: <HomePageRightIcons {...navigation} />,
  });

  renderProducts = () => {
    if (this.props.products) {
      return this.props.products.map((product, index) => (
        // <ProductListFeature key={index} {...this.props} details={product} />
        <MyProductComponent key={index} {...this.props} details={product} />
      ));
    }
  };

  render() {
    if (this.props.loading == true) {
      return <Spinner color={'red'} />;
    }
    return (
      <ScrollView style={styles.container}>
        <View style={styles.container}>{this.renderProducts()}</View>
      </ScrollView>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    position: 'relative',
    padding: 10,
    backgroundColor: '#fff',
    zIndex: 1,
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.category.loading,
    products: state.category.products,
  };
};

export default connect(mapStateToProps, {getProductsByCategory})(MyProduct);
