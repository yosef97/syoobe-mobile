import React, {Component} from 'react';
import {ScrollView, View, Alert, AsyncStorage, StyleSheet} from 'react-native';
import {TextInputCostum} from './common/TextInput';
import {ProductFileUpload} from './common/product/ProductFileUpload';
import Bugsnag from '@bugsnag/react-native';
import axios from 'axios';
import DocumentPicker from 'react-native-document-picker';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Content, Button as ButtonNative, Text} from 'native-base';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {Font} from './Font';

class ApplySellingLimit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _token: '',
      business_name: '',
      full_name: '',
      phone: '',
      business_type: '',
      business_category: '',
      address: '',
      npwp: '',
      comment: '',
      fileNameIdCard: '',
      fileDownloadIdCard: [],
      fileNameBank: '',
      fileDownloadBank: [],
      fileNameNpwp: '',
      fileDownloadNpwp: [],
      loading: false,
      loadingUpload: false,
      can_add_product: props.route.params.can_add_product,
      can_upgrade_limit: props.route.params.can_upgrade_limit,
      upgrade_limit_info: props.route.params.upgrade_limit_info,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
    return;
  };

  componentDidMount() {
    this.retrieveToken();
  }

  selectFileForIdCard = async (fileType, item) => {
    await this.setState({
      loadingUpload: true,
    });

    try {
      const file = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });

      this.setState({
        fileDownloadIdCard: file,
        fileNameIdCard: file.name,
      });

      this.setState({
        loadingUpload: false,
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        Bugsnag.notify(err);
      } else {
        throw Bugsnag.notify(err);
      }
    }
  };

  selectFileBank = async (fileType, item) => {
    await this.setState({
      loadingUpload: true,
    });

    try {
      const file = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });

      this.setState({
        fileDownloadBank: file,
        fileNameBank: file.name,
      });

      this.setState({
        loadingUpload: false,
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        Bugsnag.notify(err);
      } else {
        throw Bugsnag.notify(err);
      }
    }
  };

  selectFileNpwp = async (fileType, item) => {
    await this.setState({
      loadingUpload: true,
    });

    try {
      const file = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });

      this.setState({
        fileDownloadNpwp: file,
        fileNameNpwp: file.name,
      });

      this.setState({
        loadingUpload: false,
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        Bugsnag.notify(err);
      } else {
        throw Bugsnag.notify(err);
      }
    }
  };

  onSubmit = () => {
    if (
      this.state.business_name !== '' &&
      this.state.full_name !== '' &&
      this.state.phone !== '' &&
      this.state.business_type !== '' &&
      this.state.business_category !== '' &&
      this.state.address !== '' &&
      this.state.comment !== '' &&
      this.state.fileNameIdCard !== '' &&
      this.state.fileNameBank !== '' &&
      this.state.fileNameNpwp !== ''
    ) {
      this.setState({
        loading: true,
      });

      const fileDownloadIdCard = {
        name: this.state.fileDownloadIdCard.name,
        uri: this.state.fileDownloadIdCard.uri,
        type: this.state.fileDownloadIdCard.type,
      };

      const fileDownloadBank = {
        name: this.state.fileDownloadBank.name,
        uri: this.state.fileDownloadBank.uri,
        type: this.state.fileDownloadBank.type,
      };

      const fileDownloadNpwp = {
        name: this.state.fileDownloadNpwp.name,
        uri: this.state.fileDownloadNpwp.uri,
        type: this.state.fileDownloadNpwp.type,
      };

      var formData = new FormData();

      formData.append('_token', this.state._token);

      formData.append('business_name', this.state.business_name);

      formData.append('full_name', this.state.full_name);

      formData.append('phone', this.state.phone);

      formData.append('business_type', this.state.business_type);

      formData.append('business_category', this.state.business_category);

      formData.append('address', this.state.address);

      formData.append('npwp', this.state.npwp);

      formData.append('id_card_info', fileDownloadIdCard);

      formData.append('bank_acc_info', fileDownloadBank);

      formData.append('npwp_info', fileDownloadNpwp);

      formData.append('comment', this.state.comment);

      axios
        .post('https://syoobe.co.id/api/requestUpgradeLimitProduct', formData)
        .then((response) => {
          console.log('update1', JSON.stringify(response));
          if (response.data.status == 1) {
            this.setState({
              loading: false,
            });
            Alert.alert('Informasi', response.data.msg, [
              {
                text: 'OK',
                onPress: () =>
                  this.props.navigation.replace('HomeDrawer', {
                    screen: 'SellerMyProductList',
                  }),
              },
            ]);
          } else {
            Alert.alert('Informasi', response.data.msg, [
              {
                text: 'OK',
                onPress: () =>
                  this.props.navigation.replace('HomeDrawer', {
                    screen: 'SellerMyProductList',
                  }),
              },
            ]);
            Alert.alert('Tindakan gagal');
          }
        })
        .catch((err) => {
          Bugsnag.notify(err);
          console.log('e', err);
          this.setState({
            loading: false,
          });
        });
    } else {
      Alert.alert('Mohon cek kembali pengajuan data anda.');
    }
  };

  render() {
    console.log('apply selling', JSON.stringify(this.props));
    return (
      <View>
        <ScrollView>
          {this.state.can_upgrade_limit === 1 ? (
            <View
              style={{
                flex: 1,
                margin: 2,
                paddingVertical: hp(2),
                paddingHorizontal: wp(2.5),
                backgroundColor: '#fff',
              }}>
              <Content>
                <TextInputCostum
                  value={this.state.business_name}
                  onChangeText={(text) => this.setState({business_name: text})}
                  label="Nama Usaha *"
                />
                <TextInputCostum
                  value={this.state.full_name}
                  onChangeText={(text) => this.setState({full_name: text})}
                  label="Nama Lengkap *"
                />
                <TextInputCostum
                  value={this.state.phone}
                  keyboardType={'numeric'}
                  onChangeText={(text) => this.setState({phone: text})}
                  label="Nomor Telepon *"
                />
                <TextInputCostum
                  value={this.state.business_type}
                  onChangeText={(text) => this.setState({business_type: text})}
                  label="Jenis Usaha *"
                />
                <TextInputCostum
                  value={this.state.business_category}
                  onChangeText={(text) =>
                    this.setState({business_category: text})
                  }
                  label="Kategori Usaha *"
                />

                <TextInputCostum
                  value={this.state.address}
                  onChangeText={(text) => this.setState({address: text})}
                  label="Alamat Lengkap *"
                  multiline
                  numberOfLines={5}
                />
                <TextInputCostum
                  value={this.state.npwp}
                  onChangeText={(text) => this.setState({npwp: text})}
                  label="Nomor NPWP"
                />

                <ProductFileUpload
                  value={this.state.fileDownloadIdCard.filename}
                  // index={i}
                  loading={this.state.loadingUpload}
                  onPress={({type, index, ref, item}) => {
                    ref.close();
                    this.selectFileForIdCard(type, item);
                  }}
                  label={'Foto / Scan KTP pemilik usaha *'}
                  fileName={this.state.fileNameIdCard}
                />

                <ProductFileUpload
                  value={this.state.fileDownloadBank.filename}
                  // index={i}
                  loading={this.state.loadingUpload}
                  onPress={({type, index, ref, item}) => {
                    ref.close();
                    this.selectFileBank(type, item);
                  }}
                  label={'Data rekening bank pemilik usaha / Perusahaan *'}
                  fileName={this.state.fileNameBank}
                />

                <ProductFileUpload
                  value={this.state.fileDownloadNpwp.filename}
                  // index={i}
                  loading={this.state.loadingUpload}
                  onPress={({type, index, ref, item}) => {
                    ref.close();
                    this.selectFileNpwp(type, item);
                  }}
                  label={'Foto / Scan NPWP pemilik usaha / Perusahaan *'}
                  fileName={this.state.fileNameNpwp}
                />

                <TextInputCostum
                  value={this.state.comment}
                  onChangeText={(text) => this.setState({comment: text})}
                  label="Komentar Anda *"
                  multiline
                  numberOfLines={5}
                />
                <View style={{marginVertical: hp(2)}}>
                  <ButtonNative
                    block
                    danger
                    //   disabled={!this.state.fileNameSample}
                    onPress={() => this.onSubmit()}>
                    <Text>Update</Text>
                  </ButtonNative>
                </View>
              </Content>
            </View>
          ) : (
            <View style={styles.container}>
              {this.state.upgrade_limit_info && this.state.upgrade_limit_info && (
                <View style={styles.content}>
                  <FontAwesome5Icon name={'clock'} size={50} />
                  <Text style={styles.textBold}>
                    {this.state.upgrade_limit_info?.status}
                  </Text>
                  <Text style={styles.textBold}>
                    HI {this.state.upgrade_limit_info?.applicant}
                  </Text>
                  <Text style={styles.textLight}>
                    {this.state.upgrade_limit_info?.message}
                  </Text>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontFamily: Font.RobotoRegular,
                    }}>
                    NOMOR REFERENSI
                  </Text>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontFamily: Font.RobotoRegular,
                    }}>
                    {this.state.upgrade_limit_info?.reference_number}
                  </Text>

                  <View style={{marginVertical: hp(2)}}>
                    <ButtonNative
                      block
                      danger
                      //   disabled={!this.state.fileNameSample}
                      onPress={() =>
                        this.props.navigation.navigate('HomeDrawer', {
                          screen: 'Home',
                        })
                      }>
                      <Text>Kembali ke Home</Text>
                    </ButtonNative>
                  </View>
                </View>
              )}
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

export default ApplySellingLimit;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 2,
    paddingVertical: hp(20),
    paddingHorizontal: wp(2.5),
    backgroundColor: '#fff',
    height: hp(100),
  },
  content: {
    alignItems: 'center',
    flexDirection: 'column',
    flex: 1,
  },
  textBold: {
    fontWeight: 'bold',
    marginBottom: hp(2),
    fontFamily: Font.RobotoRegular,
  },
  textLight: {
    textAlign: 'center',
    marginBottom: hp(2),
    fontFamily: Font.RobotoLight,
  },
});
