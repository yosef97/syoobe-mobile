import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import ImagePicker from 'react-native-image-picker';

export default class FileComponent extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //     image: null,
    // }
  }

  pickImage = async () => {
    const options = {
      title: this.props.title,
      storageOptions: {
        skipBackup: true,
        path: 'images',
        //   maxWidth: 200,
        //   maxHeight: 170
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('image picker', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        //   this.setState({image: response.uri})
        const image = 'data:image/png;base64,' + response.data;
        this.props.onClickShowImage(image);
      }
    });
  };
  render() {
    const {
      viewStyle,
      fileInputWrapper,
      logoTxt,
      fileType,
      buttonStyle,
      noFileTxt,
      textStyle,
    } = styles;
    return (
      <View style={fileInputWrapper}>
        <Text style={logoTxt}>{this.props.label}</Text>
        <View style={viewStyle}>
          <TouchableOpacity
            onPress={() => this.pickImage()}
            style={buttonStyle}>
            <Text style={textStyle}>Pilih File</Text>
          </TouchableOpacity>
          <Text style={noFileTxt}>Tidak ada file yang dipilih</Text>
        </View>
        <Text style={fileType}>
          Unggah .jpg, .gif atau .png. Dimensi yang disukai 100 X 100
        </Text>
      </View>
    );
  }
}

const styles = {
  viewStyle: {
    color: '#000',
    padding: 5,
    fontSize: wp('4%'),
    width: '100%',
    marginTop: 0,
    backgroundColor: '#fff',
    borderRadius: 50,
    elevation: 6,
    height: hp('6%'),
    // marginLeft:2,
    // marginRight:2,
    fontFamily: Font.RobotoRegular,
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonStyle: {
    width: 'auto',
    backgroundColor: '#e4e4e4',
    borderRadius: 50,
    height: '100%',
    fontSize: wp('4%'),
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noFileTxt: {
    marginLeft: 10,
  },
  fileType: {
    fontSize: wp('3.5%'),
    marginTop: 10,
    paddingLeft: 8,
    fontFamily: Font.RobotoLight,
  },
  logoTxt: {
    marginLeft: 4,
    marginBottom: 8,
  },
  fileInputWrapper: {
    marginBottom: 10,
  },
};
