import React, {Component} from 'react';
import {Image, View, Text, ScrollView} from 'react-native';
import {ButtonTwo, InputInner, Spinner} from './common';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import axios from 'axios';
import {Font} from './Font';
import Spinner2 from 'react-native-loading-spinner-overlay';
import {showMessage} from 'react-native-flash-message';
import {getWalletDetails, emptyWalletDetails} from '../actions';
import {connect} from 'react-redux';
import {showToast} from '../helpers/toastMessage';
import RNPickerSelect from 'react-native-picker-select';

class EnterCardDetails extends Component {
  state = {
    monthArray: [
      {label: 'Jan', value: '1'},
      {label: 'Feb', value: '2'},
      {label: 'Mar', value: '3'},
      {label: 'Apr', value: '4'},
      {label: 'May', value: '5'},
      {label: 'Jun', value: '6'},
      {label: 'Jul', value: '7'},
      {label: 'Aug', value: '8'},
      {label: 'Sept', value: '9'},
      {label: 'Oct', value: '10'},
      {label: 'Nov', value: '11'},
      {label: 'Dec', value: '12'},
    ],
    yearArray: [
      {label: '2020', value: '2020'},
      {label: '2021', value: '2021'},
      {label: '2022', value: '2022'},
      {label: '2023', value: '2023'},
      {label: '2024', value: '2024'},
      {label: '2025', value: '2025'},
      {label: '2026', value: '2026'},
      {label: '2027', value: '2027'},
      {label: '2028', value: '2028'},
      {label: '2029', value: '2029'},
      {label: '2030', value: '2030'},
      {label: '2031', value: '2031'},
      {label: '2032', value: '2032'},
    ],
    loading: false,
    cardNumber: '',
    cardHolderName: '',
    expiryMonth: 0,
    expiryYear: 0,
    cvv: '',
    postage_notes: this.props.route.params.postage_notes,
    order_type: this.props.route.params.order_type,
    cart_amount: this.props.route.params.amount,
    cart_amount_currency: this.props.route.params.currency,
    _token: this.props.route.params._token,
    order_id: this.props.route.params.order_id,
    finalResponse: null,
    cardType: null,
    cardLoading: false,
  };

  clearCartApi = async () => {
    let details = {
      _token: this.state._token,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/clearCartAPI', formBody)
      .then((response) => {
        console.log('clear', response);
      });
  };

  getWallet = async () => {
    await this.props.emptyWalletDetails();
    var details = {
      _token: this.state._token,
      page: 1,
    };
    this.props.getWalletDetails(details);
  };

  redirectTo = (data) => {
    if (
      this.state.order_type == 'postage' ||
      this.state.order_type == 'wallet'
    ) {
      if (this.state.order_type == 'wallet') {
        this.getWallet();
      } else if (this.state.order_type == 'postage') {
        if (this.props.route.params.postageDetails != undefined) {
          this.props.route.params.postageDetails();
        }
      }
      setTimeout(() => {
        this.setState({
          loading: false,
        });
        showMessage({
          message: 'Success',
          description: `$ ${this.state.cart_amount} added to your ${
            this.state.order_type == 'postage' ? 'Postage' : 'Wallet'
          } Account balance!`,
          type: 'success',
        });
        this.props.navigation.dispatch({
          type: 'Navigation/NAVIGATE',
          routeName: 'homeDrawer',
          action: {
            type: 'Navigation/NAVIGATE',
            routeName:
              this.state.order_type == 'postage'
                ? 'PostageAccountHistory'
                : 'My Wallet',
          },
        });
      }, 3000);
    } else {
      this.setState({
        loading: false,
      });
      this.clearCartApi();
      showMessage({
        message: 'Success',
        description: `Order Placed!`,
        type: 'success',
      });
      if (data.order_product_data) {
        this.props.navigation.navigate({
          routeName: 'ThankYou',
          params: {
            orders: data.order_product_data,
            _token: this.state._token,
            order_id: this.state.order_id,
          },
        });
      }
    }
    // this.props.navigation.dispatch(StackActions.popToTop());
  };

  payUsingCreditCard = async () => {
    if (
      String(this.state.cardNumber).trim() == '' ||
      String(this.state.cardHolderName).trim() == '' ||
      String(this.state.cvv).trim() == '' ||
      this.state.expiryMonth == 0 ||
      this.state.expiryYear == 0
    ) {
      showToast({
        message: 'Enter valid card Details',
      });
    } else {
      if (
        this.state.order_type == 'postage' ||
        this.state.order_type == 'wallet'
      ) {
        this.setState({
          loading: true,
        });
        var details = {
          _token: this.state._token,
          'card-number': this.state.cardNumber,
          month: this.state.expiryMonth,
          year: this.state.expiryYear,
          cvv: this.state.cvv,
          card_holder_name: this.state.cardHolderName,
          paidAmt: this.state.cart_amount,
          postage_notes: this.state.postage_notes,
          postage_type: this.state.order_type,
        };
        var formBody = [];
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        await axios
          .post('https://syoobe.co.id/api/creditCardPaymentInAddFund', formBody)
          .then((response) => {
            console.log('creditCardPaymentInAddFund', response);
            if (response.data.status == 1) {
              this.redirectTo();
            } else {
              this.setState({
                loading: false,
              });
              showToast({
                message: 'Your card details are not valid!',
              });
            }
          });
      } else {
        this.setState({
          loading: true,
        });
        var details = {
          _token: this.state._token,
          'card-number': this.state.cardNumber,
          month: this.state.expiryMonth,
          year: this.state.expiryYear,
          cvv: this.state.cvv,
          card_holder_name: this.state.cardHolderName,
          order_id: this.state.order_id,
        };
        var formBody = [];
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        await axios
          .post('https://syoobe.co.id/api/creditCardPaymentAPI', formBody)
          .then((response) => {
            console.log('r', response);
            if (response.data.status == 1) {
              this.redirectTo(response.data);
            } else {
              this.setState({
                loading: false,
              });
              showToast({
                message: 'Your card details are not valid!',
              });
            }
          });
      }
    }
  };

  onChangeCardNumber = async (val) => {
    await this.setState({
      cardLoading: true,
      cardNumber: val,
    });
    var details = {
      _token: this.state._token,
      card_number: this.state.cardNumber,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/creditCardType', formBody)
      .then((response) => {
        console.log(response.data);
        this.setState({
          cardLoading: false,
        });
        if (response.data.result.card_type != '') {
          this.setState({
            cardType: response.data.result.card_type,
          });
        } else {
          this.setState({
            cardType: null,
          });
        }
      });
  };

  render() {
    const {cardType} = this.state;
    return (
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        style={styles.scrollClass}>
        <View
          style={{
            marginleft: 5,
            marginRight: 5,
            // marginTop: 10,
            height: '100%',
            padding: 15,
            paddingTop: 0,
            position: 'relative',
            flex: 1,
          }}>
          {this.state.loading && <Spinner2 visible={this.state.loading} />}
          <View style={styles.viewCard}>
            <View style={styles.viewCardImage}>
              {(cardType == 'VISA' && (
                <Image
                  style={styles.cardImage}
                  source={require('../images/VISA.png')}
                />
              )) ||
                (cardType == 'MASTERCARD' && (
                  <Image
                    style={styles.cardImage}
                    source={require('../images/MASTERCARD.png')}
                  />
                )) ||
                (cardType == 'AMEX' && (
                  <Image
                    style={styles.cardImage}
                    source={require('../images/AMEX.png')}
                  />
                )) ||
                (cardType == 'DINERS_CLUB' && (
                  <Image
                    style={styles.cardImage}
                    source={require('../images/DINERS_CLUB.png')}
                  />
                )) ||
                (cardType == 'DISCOVER' && (
                  <Image
                    style={styles.cardImage}
                    source={require('../images/DISCOVER.png')}
                  />
                )) ||
                (cardType == 'JCB' && (
                  <Image
                    style={styles.cardImage}
                    source={require('../images/JCB.png')}
                  />
                )) ||
                (this.state.cardLoading && (
                  <Spinner color="red" size="small" />
                ))}
            </View>
            <InputInner
              placeholder=""
              keyboardType={'numeric'}
              value={this.state.cardNumber}
              onChangeText={(val) => this.onChangeCardNumber(val)}
              label="Nomor kartu"
              card={true}
            />
          </View>
          <View style={styles.viewCard}>
            <InputInner
              placeholder=""
              value={this.state.cardHolderName}
              onChangeText={(val) =>
                this.setState({
                  cardHolderName: val,
                })
              }
              label="Nama pemegang kartu"
              card={true}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View style={{width: '100%'}}>
              <View style={{marginBottom: 16}}>
                <Text style={styles.dropheadingClass}>
                  Tanggal kadaluarsa(MM/YY)
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View style={styles.dropdownClass}>
                  <RNPickerSelect
                    value={this.state.expiryMonth}
                    onValueChange={(val) =>
                      this.setState({
                        expiryMonth: val,
                      })
                    }
                    items={this.state.monthArray}
                  />
                  {/* <Picker
                                        style={styles.pickerClass}
                                        selectedValue={this.state.expiryMonth}
                                        onValueChange={(val) => this.setState({
                                            expiryMonth: val
                                        })}
                                    >
                                        <Picker.Item label="MM" value="0" />
                                        <Picker.Item label="Jan" value="1" />
                                        <Picker.Item label="Feb" value="2" />
                                        <Picker.Item label="Mar" value="3" />
                                        <Picker.Item label="Apr" value="4" />
                                        <Picker.Item label="May" value="5" />
                                        <Picker.Item label="Jun" value="6" />
                                        <Picker.Item label="Jul" value="7" />
                                        <Picker.Item label="Aug" value="8" />
                                        <Picker.Item label="Sept" value="9" />
                                        <Picker.Item label="Oct" value="10" />
                                        <Picker.Item label="Nov" value="11" />
                                        <Picker.Item label="Dec" value="12" />
                                    </Picker> */}
                </View>
                <View style={styles.dropdownClass}>
                  {/* <Picker
                    style={styles.pickerClass}
                    selectedValue={this.state.expiryYear}
                    onValueChange={val =>
                      this.setState({
                        expiryYear: val
                      })
                    }
                  >
                    <Picker.Item label="YY" value="0" />
                    <Picker.Item label="2019" value="2019" />
                    <Picker.Item label="2020" value="2020" />
                    <Picker.Item label="2021" value="2021" />
                    <Picker.Item label="2022" value="2022" />
                    <Picker.Item label="2023" value="2023" />
                    <Picker.Item label="2024" value="2024" />
                    <Picker.Item label="2025" value="2025" />
                    <Picker.Item label="2026" value="2026" />
                    <Picker.Item label="2027" value="2027" />
                    <Picker.Item label="2028" value="2028" />
                    <Picker.Item label="2029" value="2029" />
                  </Picker> */}
                  <RNPickerSelect
                    value={this.state.expiryYear}
                    onValueChange={(val) =>
                      this.setState({
                        expiryYear: val,
                      })
                    }
                    items={this.state.yearArray}
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={{width: '20%', marginTop: 15}}>
            <InputInner
              value={this.state.cvv}
              keyboardType={'numeric'}
              onChangeText={(val) =>
                this.setState({
                  cvv: val,
                })
              }
              label="CVV"
              card={true}
              secureTextEntry={true}
            />
          </View>
          <ButtonTwo onPress={() => this.payUsingCreditCard()}>
            Bayar {this.state.cart_amount_currency} {this.state.cart_amount}{' '}
          </ButtonTwo>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  viewCard: {
    position: 'relative',
    marginBottom: 12,
  },
  viewCardImage: {
    alignSelf: 'flex-end',
    // position: 'absolute',
    right: 0,
    top: hp('10.3%'),
    zIndex: 1001,
    width: wp('16%'),
    height: hp('5%'),
    // borderWidth: 2,
    // borderColor: '#000'
  },
  cardImage: {
    resizeMode: 'cover',
    width: wp('12%'),
    height: hp('5%'),
    // borderWidth: 2,
    // borderColor: '#000'
  },
  dropdownClass: {
    justifyContent: 'center',
    height: hp('6%'),
    alignItems: 'center',
    width: '49%',
    borderRadius: 1,
    // marginBottom:17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
    borderRadius: 50,
  },
  pickerClass: {
    height: hp('6%'),
    width: '100%',
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 14,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    // marginBottom:8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
  },
};

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {
  emptyWalletDetails,
  getWalletDetails,
})(EnterCardDetails);
