import React, {Component} from 'react';
import CheckBox from 'react-native-checkbox-heaven';
import {StyleSheet, View} from 'react-native';

export default class CheckBoxComponent extends Component {
  state = {
    checked: false,
  };

  componentDidMount() {
    if (this.props.checked === 1) {
      this.setState({checked: true});
    }
  }

  // handleOnChange = (val) => {
  //   this.setState({checked: val});
  // };

  render() {
    return (
      <View style={styles.container}>
        <CheckBox
          label={this.props.label}
          checked={this.props.checked}
          checkedColor={this.props.checkedColor}
          uncheckedColor={this.props.uncheckedColor}
          // onChange={this.handleOnChange.bind(this)}
          onChange={this.props.onChange}
          disabledColor="red"
          style={this.props.checkBoxStyle}
          iconStyle={this.props.iconStyle}
          labelPosition={this.props.labelposition}
          iconSize={this.props.iconSize}
          labelStyle={this.props.labelStyle}
          iconName={this.props.iconName}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  labelStyle: {
    marginLeft: 4,
    fontSize: 16,
    fontWeight: 'bold',
    color: '#2f4f4f',
  },
});
