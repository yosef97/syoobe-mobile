import React, {Component} from 'react';
import {
  AsyncStorage,
  Alert,
  StatusBar,
  Keyboard,
  Text,
  KeyboardAvoidingView,
  Image,
  ImageBackground,
  View,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {Button, Input, Footer, CardSectionLogin, Spinner} from './common';
import {
  setVerifyFalse,
  googleLogin,
  fbsocialLogin,
  emailChanged,
  passwordChanged,
  sendOtpVerification,
} from '../actions';
import {connect} from 'react-redux';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import FBSDK from 'react-native-fbsdk';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import axios from 'axios';
import {setI18nConfig, translate} from '../translations/translation';
import * as RNLocalize from 'react-native-localize';

const {LoginManager, AccessToken, GraphRequest, GraphRequestManager} = FBSDK;

class Login extends Component {
  constructor(props) {
    super(props);
    setI18nConfig();
    this.state = {
      isFooterVisible: true,
      loader: false,
    };
  }

  sendVerificationMail = async () => {
    // resendEmailVerificationLink
    //user_email_username
    let details = {
      user_email_username: this.props.username,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/resendEmailVerificationLink', formBody)
      .then((response) => {
        console.log('clear', response);
        if (response.data.status == 1) {
          Alert.alert('Berhasil', 'Email verifikasi telah dikirim!', [
            {text: 'OK'},
          ]);
        } else {
          Alert.alert('Gagal', 'Silahkan coba lagi!', [{text: 'OK'}]);
        }
      });
  };

  componentDidUpdate = () => {
    if (this.props.reVerify) {
      Alert.alert('Login Gagal!', this.props.reVerifyMsg, [
        {text: 'Cancel', style: 'cancel'},
        {
          text: 'Kirim email verifikasi',
          onPress: () => this.sendVerificationMail(),
        },
      ]);
      this.props.setVerifyFalse();
    }
  };

  async componentDidMount() {
    await this._configureGoogleSignIn();

    this.keyboardDidShowSub = Keyboard.addListener(
      'keyboardDidShow',
      this.handleKeyboardDidShow,
    );
    this.keyboardDidHideSub = Keyboard.addListener(
      'keyboardDidHide',
      this.handleKeyboardDidHide,
    );
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  _configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId:
        '981357932967-6t0giiqtgj8im17gg6ek7ui4r4iovkke.apps.googleusercontent.com',
    });
  }

  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  handleLocalizationChange = () => {
    setI18nConfig();
    this.forceUpdate();
  };

  handleKeyboardDidShow = (event) => {
    this.setState({
      isFooterVisible: false,
    });
  };

  handleKeyboardDidHide = () => {
    this.setState({
      isFooterVisible: true,
    });
  };

  onEmailChange = (initial) => {
    var text = initial;
    var emailtrim = text.trim();
    this.props.emailChanged(emailtrim);
  };

  onPasswordChange = (text) => {
    this.props.passwordChanged(text);
  };

  onButtonPress = async () => {
    const {username, password} = this.props;
    if (username === '') {
      Alert.alert('Username atau email yang anda masukkan tidak valid');
    } else if (password === '') {
      Alert.alert('Password tidak boleh kosong');
    } else {
      if (String(username).includes('@')) {
        var email = String(username).toLowerCase();
        this.props.sendOtpVerification(this.props.navigation, email, password);
        await AsyncStorage.setItem('emailStore', email);
        await AsyncStorage.setItem('passwordStore', password);
      } else {
        this.props.sendOtpVerification(
          this.props.navigation,
          username,
          password,
        );
        await AsyncStorage.setItem('password', this.props.password);
      }
    }
  };

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    return fcmToken;
  }

  async getDeviceInfo() {
    let deviceInfo = await AsyncStorage.getItem('deviceInfo');
    return deviceInfo;
  }

  renderError() {
    if (this.props.error) {
      return <Text style={styles.errorTextStyle}>{this.props.error}</Text>;
    }
  }

  async FBLoginCallback(error, result) {
    console.log('final res', result);
    if (error) {
      console.log('final err', error);
    } else {
      let device_token = await this.getToken();
      var details = {
        email: result.email,
        name: result.name,
        userid: result.id,
        device_token: device_token,
      };
      this.props.fbsocialLogin(this.props.navigation, details);
    }
  }

  FBGraphRequest = async (fields, callback) => {
    const accessData = await AccessToken.getCurrentAccessToken();
    console.log('access tokne', accessData);
    const infoRequest = new GraphRequest(
      '/me',
      {
        accessToken: accessData.accessToken,
        parameters: {
          fields: {
            string: fields,
          },
        },
      },
      callback.bind(this),
    );
    new GraphRequestManager().addRequest(infoRequest).start();
  };

  facebookLogin = async () => {
    let result;
    try {
      LoginManager.setLoginBehavior(
        Platform.OS === 'ios' ? 'native' : 'NATIVE_ONLY',
      );
      result = await LoginManager.logInWithPermissions([
        'public_profile',
        'email',
      ]);
    } catch (nativeError) {
      try {
        LoginManager.setLoginBehavior('WEB_ONLY');
        // result = await LoginManager.logOut();
        result = await LoginManager.logInWithPermissions([
          'public_profile',
          'email',
        ]);
      } catch (webError) {
        console.log('error', webError);
      }
    }
    if (result.isCancelled) {
      console.log('cancelled');
    } else {
      this.FBGraphRequest('email, id, name', this.FBLoginCallback);
    }
  };

  googleLogin = async () => {
    try {
      await GoogleSignin.hasPlayServices({
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      // let device_token = await this.getDeviceInfo();
      let device_token = await this.getToken();

      // console.log('device token', device_token);

      var details = {
        email: userInfo.user.email,
        name: userInfo.user.name,
        userid: userInfo.user.id,
        device_token: device_token,
      };
      this.props.googleLogin(this.props.navigation, details);
    } catch (error) {
      console.log('Message1', error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };

  renderForm() {
    return (
      <View style={{flex: 1, paddingHorizontal: wp(2)}}>
        <KeyboardAvoidingView behavior="position" enabled>
          <Image style={styles.logo} source={require('../images/logo.png')} />
          <View
            style={{
              backgroundColor: '#fff',
              paddingVertical: hp(2),
              paddingHorizontal: wp(2),
              borderRadius: 10,
            }}>
            <Text
              style={{
                paddingBottom: hp(2),
                fontSize: hp(3),
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              Masuk
            </Text>
            <CardSectionLogin
              style={styles.iconClass}
              source={require('../images/logo.png')}>
              <Image
                style={styles.userIcon}
                source={require('../images/userIcon.png')}
              />
              <Input
                placeholder={`Nama pengguna/email`}
                value={this.props.username}
                label="Email"
                onChangeText={this.onEmailChange.bind(this)}
              />
            </CardSectionLogin>
            <CardSectionLogin>
              <Image
                style={styles.passwordIcon}
                source={require('../images/passwordIcon.png')}
              />
              <Input
                placeholder={`${translate('password')}`}
                value={this.props.password}
                secureTextEntry
                label="Password"
                onChangeText={this.onPasswordChange.bind(this)}
              />
            </CardSectionLogin>
            {this.renderError()}
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('ForgotPassword')}>
              <View style={{paddingBottom: hp(2), alignItems: 'flex-end'}}>
                <Text style={{color: 'red', fontSize: hp(2)}}>
                  Lupa kata sandi?
                </Text>
              </View>
            </TouchableOpacity>
            <Button onPress={this.onButtonPress.bind(this)}>
              {translate('login')}
            </Button>
            <View
              style={{
                paddingTop: hp(2),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>Atau masuk dengan</Text>
            </View>
            <View style={styles.footerSocialIcon}>
              <View
                style={{
                  flex: 1,
                  // backgroundColor: 'red',
                  width: '100%',
                  maxWidth: '100%',
                  marginRight: wp(1),
                  paddingVertical: 10,
                }}>
                <TouchableOpacity onPress={() => this.facebookLogin()}>
                  <Image
                    style={{width: '100%', height: hp(5), borderRadius: 5}}
                    source={require('../images/facebook_logo.png')}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 1,
                  // backgroundColor: 'red',
                  width: '100%',
                  marginLeft: wp(1),
                  paddingVertical: 11,
                  paddingHorizontal: 2,
                }}>
                <TouchableOpacity onPress={() => this.googleLogin()}>
                  <Image
                    style={{
                      width: '100%',
                      height: hp(5),
                      borderWidth: 1,
                      borderColor: '#eaeaea',
                      borderRadius: 3,
                    }}
                    source={require('../images/google_logo.png')}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }

  renderFooter = () => {
    if (this.state.isFooterVisible) {
      return (
        <Footer>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Registration')}>
              <Text style={styles.footer_text}>
                {'Belum memiliki akun? Daftar'}
              </Text>
            </TouchableOpacity>
          </View>
        </Footer>
      );
    }
  };

  render() {
    if (this.props.loading) {
      return (
        <View>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <ImageBackground
            style={styles.bgImg}
            source={require('../images/loginBg.png')}>
            <Spinner size="large" />
          </ImageBackground>
        </View>
      );
    }

    return (
      <View>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <ImageBackground
          style={styles.bgImg}
          source={require('../images/loginBg.png')}>
          {this.renderForm()}
          {this.renderFooter()}
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    username: state.auth.username,
    password: state.auth.password,
    error: state.auth.error,
    loading: state.auth.loading,
    toast: state.auth.toast,
    reVerify: state.auth.reVerify,
    reVerifyMsg: state.auth.reVerifyMsg,
  };
};

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'yellow',
  },
  logo: {
    marginTop: 30,
    marginBottom: 40,
    marginLeft: 'auto',
    marginRight: 'auto',
    // width:170,
    // height:170,
    height: hp('16%'), // 70% of height device screen
    width: wp('32%'), // 80% of width device screen
  },
  bgImg: {
    height: '100%',
  },
  iconClass: {
    width: '100%',
    backgroundColor: '#fff',
  },
  face_book: {
    width: wp('10%'),
    height: hp('5%'),
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 17,
    marginTop: 20,
    marginRight: 10,
    marginBottom: 50,
  },
  google_plus: {
    width: wp('10%'),
    height: hp('5%'),
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 17,
    marginTop: 20,
    marginBottom: 50,
  },
  footer_text: {
    width: 'auto',
    color: '#ffffff',
    textAlign: 'center',
    fontSize: hp('2%'),
    fontFamily: Font.RobotoRegular,
  },
  userIcon: {
    height: hp('2.2%'),
    width: wp('4%'),
    marginLeft: 20,
  },
  passwordIcon: {
    height: hp('2.4%'),
    width: wp('3.3%'),
    marginLeft: 20,
  },
  footerSocialIcon: {
    flexDirection: 'row',
    marginTop: 10,
  },
};

export default connect(mapStateToProps, {
  setVerifyFalse,
  googleLogin,
  fbsocialLogin,
  emailChanged,
  passwordChanged,
  sendOtpVerification,
})(Login);
