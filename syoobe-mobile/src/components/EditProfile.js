import React, {Component} from 'react';
import {
  StatusBar,
  TouchableOpacity,
  AsyncStorage,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  ImageBackground,
  View,
  ScrollView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {connect} from 'react-redux';
import {Spinner} from './common';
import {getProfileDetails, update_profile_pic} from '../actions';
import ImagePicker from 'react-native-image-picker';

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      globalToken: null,
      screen: Dimensions.get('window'),
    };
  }

  static navigationOptions = {
    drawerLabel: 'Informasi Akun',
    drawerIcon: () => (
      <Image
        source={require('../images/myaccount-icon.png')}
        style={{width: wp('4.2%'), height: hp('2.2%')}}
      />
    ),
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  // storeImageUrl = async (image) => {
  //     try {
  //         await AsyncStorage.setItem('image', image);
  //     } catch (error) {
  //         console.log(error);
  //     }
  // }

  // removeImageUrl = async () => {
  //     try {
  //       await AsyncStorage.removeItem('image');
  //     } catch (error) {
  //       console.log(error);
  //     }
  // }

  componentDidMount = async () => {
    await this.retrieveToken()
      .then((_token) => {
        this.setState({
          globalToken: _token,
        });
        this.props.getProfileDetails(_token);
      })
      .catch((error) => {
        console.log('Promise is rejected with error: ' + error);
      });
  };

  getOrientation() {
    if (this.state.screen.width > this.state.screen.height) {
      return 'LANDSCAPE';
    } else {
      return 'PORTRAIT';
    }
  }

  getStyle() {
    if (this.getOrientation() === 'LANDSCAPE') {
      return landscapeStyles;
    } else {
      return portraitStyles;
    }
  }

  onLayout() {
    this.setState({screen: Dimensions.get('window')});
  }

  pickImage = async () => {
    const options = {
      title: 'Pilih gambar profil',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      // maxWidth: wp('100%'),
      maxHeight: hp('50%'),
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('image picker', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const user_profile_image = 'data:image/png;base64,' + response.data;
        // this.removeImageUrl();
        // this.storeImageUrl(user_profile_image)
        this.props.update_profile_pic(this.props.navigation, {
          _token: this.state.globalToken,
          user_profile_image,
        });
      }
    });
  };

  render() {
    console.log('ini state', JSON.stringify(this.state));
    console.log('ini props', JSON.stringify(this.props));

    if (this.props.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar backgroundColor="#C90205" barStyle="light-content" />
          <Spinner color="red" size="large" />
        </View>
      );
    }
    const {
      touchimgChangeIcon,
      ratingWrapper,
      bottomContainer,
      imgChangeIcon,
      container,
      profileImgDiv,
      profileImg,
      topWrapper,
      descriptionTxt,
      profilePicOuter,
      profileTitle,
      rightArrow,
    } = styles;
    return (
      <ScrollView style={container} onLayout={this.onLayout.bind(this)}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <View style={topWrapper}>
          <ImageBackground
            style={styles.bgImg}
            source={require('../images/bg1.png')}>
            <ImageBackground
              style={profilePicOuter}
              source={require('../images/profileBorder.png')}>
              <View style={profileImgDiv}>
                <Image
                  resizeMode="cover"
                  style={profileImg}
                  source={{uri: this.props.user_image_url}}
                />
                <TouchableOpacity
                  style={touchimgChangeIcon}
                  onPress={() => this.pickImage()}>
                  <Image
                    style={imgChangeIcon}
                    source={require('../images/imgChangeicon.png')}
                  />
                </TouchableOpacity>
              </View>
            </ImageBackground>
            <Text style={profileTitle}>{this.props.profileDetails.name}</Text>
          </ImageBackground>
          <View style={this.getStyle().bottomWrapper}>
            <ImageBackground
              style={styles.bottomBg}
              source={require('../images/bg2.png')}></ImageBackground>
          </View>
        </View>
        <View style={bottomContainer}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('PersonalInfo', {
                profileDetails: this.props.profileDetails,
              })
            }>
            <View style={ratingWrapper}>
              <Text style={descriptionTxt}>Info Pribadi</Text>
              <Image
                style={rightArrow}
                source={require('../images/arrow.png')}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('BankInfo')}>
            <View style={ratingWrapper}>
              <Text style={descriptionTxt}>Rekening Bank</Text>
              <Image
                style={rightArrow}
                source={require('../images/arrow.png')}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ReturnAddressInfo')}>
            <View style={ratingWrapper}>
              <Text style={descriptionTxt}>Alamat Pengembalian</Text>
              <Image
                style={rightArrow}
                source={require('../images/arrow.png')}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ChangePassword')}>
            <View style={ratingWrapper}>
              <Text style={descriptionTxt}>Ganti Kata Sandi</Text>
              <Image
                style={rightArrow}
                source={require('../images/arrow.png')}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ChangeEmail')}>
            <View style={ratingWrapper}>
              <Text style={descriptionTxt}>Ganti Email</Text>
              <Image
                style={rightArrow}
                source={require('../images/arrow.png')}
              />
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  bottomBg: {
    width: '100%',
    height: '100%',
    marginBottom: 20,
  },
  profilePicOuter: {
    padding: 5,
  },
  profileTitle: {
    color: '#ffffff',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoRegular,
  },
  bgImg: {
    height: hp('30%'),
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  topWrapper: {
    textAlign: 'center',
  },
  bottomContainer: {
    padding: 10,
    paddingTop: 0,
  },
  ratingWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: hp('2%'),
    paddingBottom: hp('2%'),
    width: '100%',
    borderColor: '#e7e7e7',
    borderBottomWidth: 1,
  },
  descriptionTxt: {
    color: '#545454',
    // fontSize:20,
    fontSize: wp('4%'),
    fontFamily: Font.RobotoRegular,
  },
  rightArrow: {
    width: 6,
    height: 10,
  },
  container: {
    backgroundColor: '#fff',
    height: '100%',
  },

  profileImg: {
    width: wp('27%'),
    height: wp('27%'),
    borderWidth: 3,
    borderColor: '#f00',
    borderRadius: wp('14%'),
    marginLeft: 'auto',
    marginRight: 'auto',
    // flex: 1,
    resizeMode: 'cover',
  },
  profileImgDiv: {
    position: 'relative',
  },
  imgChangeIcon: {
    width: wp('10%'),
    height: wp('10%'),
  },
  touchimgChangeIcon: {
    position: 'absolute',
    width: wp('10%'),
    height: wp('10%'),
    right: -20,
    top: 20,
  },
};

const portraitStyles = StyleSheet.create({
  bottomWrapper: {
    height: hp('5%'),
    backgroundColor: 'green',
  },
});

const landscapeStyles = StyleSheet.create({
  bottomWrapper: {
    height: hp('13%'),
    backgroundColor: 'blue',
  },
});

const mapStateToProps = (state) => {
  return {
    error: state.profile.error,
    loading: state.profile.loading,
    profileDetails: state.profile.profile_details,
    user_image_url: state.profile.user_image_url,
  };
};

export default connect(mapStateToProps, {
  getProfileDetails,
  update_profile_pic,
})(EditProfile);
