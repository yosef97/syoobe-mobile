import {formatRupiah} from '../helpers/helper';
import React, {Component} from 'react';
import {Text, Image, TouchableOpacity, View} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {MyOrdersRightMenu} from './product/MyOrdersRightMenu';

export default class OrderHistoryComponent extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //     showComponmentB: props.dropDownState,
    // }
  }

  _toggleShow = (id) => {
    // this.setState({showComponmentB: !this.state.showComponmentB})
    this.props.showDropDownChange(id);
  };

  sendFeedback = () => {
    // this.setState({showComponmentB: !this.state.showComponmentB})
    this.props.showDropDownChange();
    this.props.navigation.navigate('CancelForm', {
      headerName: 'Penilaian',
      order_id: this.props.productDetail.order_product_id,
      type: 'feedback',
    });
  };

  sendMessage = () => {
    // this.setState({showComponmentB: !this.state.showComponmentB})
    this.props.showDropDownChange();
    // this.props.navigation.navigate('CancelForm',{'headerName': 'Send Message', 'order_id': this.props.productDetail.order_product_id, 'type': 'sendMessage'});
    this.props.navigation.navigate('Chat', {
      sent_to_user_id: this.props.productDetail.sent_user_id_for_chat,
      sent_to_user_name: this.props.productDetail.sent_user_name_for_chat,
      image_url: this.props.productDetail.sent_user_pic_for_chat,
    });
  };

  onRefund = () => {
    // this.setState({showComponmentB: !this.state.showComponmentB})
    this.props.showDropDownChange();
    this.props.navigation.navigate('CancelForm', {
      headerName: 'Permintaan Pengembalian',
      order_id: this.props.productDetail.order_product_id,
      type: 'return',
    });
  };

  onCancel = () => {
    // this.setState({showComponmentB: !this.state.showComponmentB})
    this.props.showDropDownChange();
    this.props.navigation.navigate('CancelForm', {
      headerName: 'Batalkan Pesanan',
      order_id: this.props.productDetail.order_product_id,
      type: 'cancel',
    });
  };

  toOrderDetails = () => {
    var details = {
      _token: this.props._token,
      order_id: this.props.productDetail.order_id,
      order_product_id: this.props.productDetail.order_product_id,
    };
    if (this.props.productDetail.product_type == 'P') {
      this.props.navigation.navigate('ViewOrderPhysical', {details});
    } else if (this.props.productDetail.product_type == 'D') {
      this.props.navigation.navigate('ViewOrderDigital', {details});
    }
  };
  render() {
    console.log(this.props.productDetail);
    const {
      boxImg,
      available,
      price,
      productName,
      rgtMenuIcon,
      imgContentWrapper,
      count,
      stat_us,
      imgWrapper,
    } = styles;
    return (
      <View
        style={this.props.dropDownState ? styles.boxStyle2 : styles.boxStyle}>
        {this.props.dropDownState &&
          this.props.selected == this.props.productDetail.order_product_id && (
            <MyOrdersRightMenu
              onSendFeedback={() => this.sendFeedback()}
              onSendMessage={() => this.sendMessage()}
              onRefund={() => this.onRefund()}
              onCancel={() => this.onCancel()}
            />
          )}
        <View style={imgContentWrapper}>
          <TouchableOpacity onPress={this.toOrderDetails} style={imgWrapper}>
            <Image
              style={boxImg}
              source={{uri: this.props.productDetail.image_path}}
            />
          </TouchableOpacity>
          <View style={{flex: 1}}>
            <Text onPress={this.toOrderDetails} style={productName}>
              {this.props.productDetail.product_name}
              {this.props.heading}{' '}
            </Text>
            <Text onPress={this.toOrderDetails} style={available}>
              {this.props.productDetail.order_date}
            </Text>
            <Text onPress={this.toOrderDetails} style={available}>
              Faktur {this.props.productDetail.invoice_sign}:
              <Text onPress={this.toOrderDetails} style={count}>
                {' '}
                {this.props.productDetail.invoice_number}
              </Text>
            </Text>
            <Text onPress={this.toOrderDetails} style={price}>
              Rp.{formatRupiah(this.props.productDetail.product_price)}{' '}
            </Text>
            <Text onPress={this.toOrderDetails} style={available}>
              Status:
              <Text onPress={this.toOrderDetails} style={stat_us}>
                {' '}
                {this.props.productDetail.order_status}
              </Text>
            </Text>
          </View>
        </View>
        <TouchableOpacity
          style={{
            height: hp('6%'),
            width: wp('3.5%'),
            alignItems: 'center',
          }}
          onPress={() =>
            this._toggleShow(this.props.productDetail.order_product_id)
          }>
          <Image
            style={rgtMenuIcon}
            resizeMode="contain"
            source={require('../images/rgtMenuIcon.png')}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  boxStyle: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    paddingBottom: 0,
    flexDirection: 'row',
    flexWarp: 'warp',
    position: 'relative',
  },
  boxStyle2: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    paddingBottom: 0,
    flexDirection: 'row',
    // flexWarp:'warp',
    // borderBottomWidth:1,
    // borderColor:'#e7e7e7',
    position: 'relative',
    zIndex: 1,
  },
  imgContentWrapper: {
    flexDirection: 'row',
    // flexBasis:'80%',
    flex: 1,
    paddingRight: 15,
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
    paddingBottom: 10,
  },
  boxImg: {
    width: wp('20%'),
    height: hp('16%'),
    resizeMode: 'contain',
  },
  imgWrapper: {
    width: wp('20%'),
    marginRight: wp('3%'),
  },
  available: {
    color: '#858585',
    fontSize: hp('2%'),
    marginRight: wp('10%'),
    // fontFamily:Fonts.RobotoMedium,
  },
  price: {
    color: '#00b3ff',
    fontSize: wp('5%'),
    // fontFamily:Fonts.RobotoBold,
  },
  availableWrapper: {
    flexDirection: 'row',
    paddingTop: hp('0.4%'),
    paddingBottom: hp('0.4%'),
  },
  rgtMenuIcon: {
    height: hp('4%'),
    width: wp('2%'),
  },

  productName: {
    fontSize: wp('4.5%'),
    color: '#545454',
    fontFamily: Font.RobotoBold,
  },
  count: {
    color: '#00b3ff',
    fontSize: wp('4%'),
  },
  stat_us: {
    color: '#c90305',
    fontSize: wp('4%'),
  },
};
