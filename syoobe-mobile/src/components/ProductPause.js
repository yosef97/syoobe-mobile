import React, {Component} from 'react';
import {Alert, AsyncStorage, FlatList, View} from 'react-native';
import ActionButton from 'react-native-action-button';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {
  getSellerPausedItems,
  deleteSellerProduct,
  changeProductStatus,
} from '../actions';
import {Spinner} from './common';
import {EmptyProductSeller} from './common/state/EmptyProductSeller';
import {ProductCardSeller} from './product/ProductCardSeller';
import PTRView from 'react-native-pull-to-refresh';

class ProductPause extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newProductArray: [],
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  componentDidMount = () => {
    this.fetchData();
  };

  fetchData = () => {
    this.retrieveToken()
      .then((_token) => {
        this.props.getSellerPausedItems(_token);
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  componentDidUpdate = async (prevProps, prevState) => {
    // console.log(this.props);
    if (prevProps.active_products.length != this.props.active_products.length) {
      let array = [];
      await this.props.active_products.forEach((product, index) => {
        let obj = {sideMenu: false, ...product};
        array.push({...obj});
      });
      await this.setState({
        newProductArray: array,
      });
    }
  };

  toProductDetail = (product_id) => {
    this.props.navigation.push('ProductDetail', {
      product_id: product_id,
      user_product: 'true',
      // product_requires_shipping: this.props.details.prod_requires_shipping,
    });
  };

  onPublishItem = (product_id, ref) => {
    this.retrieveToken()
      .then((_token) => {
        var product_status = 0;
        var details = {
          _token: _token,
          product_id: product_id,
          product_status: product_status,
        };
        ref.close();
        this.props.changeProductStatus(details, 'pause');
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  onRemoveItem = async (product_id, product_name, type, ref) => {
    Alert.alert('Konfirmasi', 'Apakah anda yakin akan hapus produk ini ?', [
      {
        text: 'Batal',
        onPress: () => console.log('cancelled'),
        style: 'cancel',
      },
      {
        text: 'Ya, Hapus',
        onPress: () => {
          this.retrieveToken()
            .then((_token) => {
              var details = {
                _token: _token,
                product_id: product_id,
              };
              ref.close();
              this.props.deleteSellerProduct(details, type);
            })
            .catch((error) => {
              console.log('error', error);
            });
        },
      },
    ]);
  };

  onEditItem = (id, ref) => {
    ref.close();
    this.props.navigation.navigate('AddProduct', {prod_id: id});
  };

  render() {
    if (this.props.loading) {
      return (
        <View
          style={{
            marginTop: hp('35%'),
          }}>
          <Spinner color="red" size="large" />
        </View>
      );
    }
    return (
      <View style={{flex: 1, backgroundColor: '#f5f6fa', padding: wp(3)}}>
        <PTRView onRefresh={() => this.fetchData()}>
          <FlatList
            data={this.props.paused_products}
            renderItem={({item, index}) => (
              <ProductCardSeller
                item={item}
                key={index}
                onPressPublish={(id, ref) => this.onPublishItem(id, ref)}
                onPressDelete={(id, name, ref) =>
                  this.onRemoveItem(id, name, 'pause', ref)
                }
                onPressEdit={(id, ref) => this.onEditItem(id, ref)}
                isPause
              />
            )}
            ListEmptyComponent={() => <EmptyProductSeller />}
            showsVerticalScrollIndicator={false}
          />
        </PTRView>
        {/* <ActionButton
          buttonColor="rgba(231,76,60,1)"
          onPress={() => this.props.navigation.navigate('AddProduct')}
        /> */}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    no_paused: state.sellerProducts.no_paused,
    no_active: state.sellerProducts.no_active,
    loading: state.sellerProducts.loading,
    active_products: state.sellerProducts.active_products,
    paused_products: state.sellerProducts.paused_products,
  };
};

export default connect(mapStateToProps, {
  getSellerPausedItems,
  deleteSellerProduct,
  changeProductStatus,
})(ProductPause);
