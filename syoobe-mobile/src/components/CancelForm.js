import React, {Component} from 'react';
import {
  TouchableOpacity,
  AsyncStorage,
  Picker,
  View,
  Alert,
  Text,
  ScrollView,
} from 'react-native';
import {
  Button,
  ButtonTwo,
  Card,
  CardSection,
  Spinner,
  TextareaInner,
} from './common';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import axios from 'axios';
import {Font} from './Font';
import {AirbnbRating} from 'react-native-ratings';
import {RadioButton} from 'react-native-paper';
import {ProductDropdown} from './common/product/ProductDropdown';
import InputText from './common/InputText';

export default class CancelForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _token: null,
      selectedReason: 0,
      reasons: [],
      comment: '',
      review: '',
      rating: 0,
      loading: false,
      header: props.route.params.header,
      order_id: props.route.params.order_id,
      type: props.route.params.type,
      refundComment: '',
      returnQty: 0,
      returnReqReason: 0,
      returnReasonList: [],
      qtyArray: [],
      checked: 1,
    };
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  fetchData = () => {
    if (this.state.type === 'cancel') {
      this.setState({
        loading: true,
      });
      axios.post('https://syoobe.co.id/api/cancelReason').then((response) => {
        this.setState({
          reasons: response.data.reasonlist,
          loading: false,
        });
      });
    } else if (this.state.type === 'return') {
      this.setState({
        loading: true,
      });
      var details = {
        _token: this.state._token,
        orderproduct_id: this.state.order_id,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      axios
        .post('https://syoobe.co.id/api/returnProductQuantity', formBody)
        .then((response) => {
          console.log(response);
          if (response.data.status === 1) {
            this.setState({
              qtyArray: response.data.order_product_quantity,
            });
            axios
              .post('https://syoobe.co.id/api/returnRefundreason')
              .then((res) => {
                this.setState({
                  returnReasonList: res.data.return_request_reason_list,
                  loading: false,
                });
              });
          }
        });
    }
  };

  async componentDidMount() {
    await this.retrieveToken();
    this.fetchData();
  }

  sendReview = async () => {
    if (this.state.rating == 0 || String(this.state.review).trim() == '') {
      Alert.alert('Harap berikan peringkat dan ulasan Anda untuk produk!');
    } else {
      this.setState({
        loading: true,
      });
      var details = {
        _token: this.state._token,
        orderproduct_id: this.state.order_id,
        review_rating: this.state.rating,
        review_text: this.state.review,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post('https://syoobe.co.id/api/orderProductFeedback', formBody)
        .then((response) => {
          console.log('res', response);
          this.setState({
            loading: false,
          });
          if (response.data.status === 1) {
            Alert.alert('Ulasan Berhasil diinput');
            Alert.alert('Informasi', response.data.msg, [
              {text: 'OK', onPress: () => this.props.navigation.pop()},
            ]);
            this.props.navigation.pop();
          } else {
            Alert.alert('Informasi', response.data.msg, [
              {text: 'OK', onPress: () => this.props.navigation.pop()},
            ]);
          }
        });
    }
  };

  submitRequest = async () => {
    if (
      this.state.selectedReason === 0 ||
      String(this.state.comment).trim() === ''
    ) {
      Alert.alert('Inputan tidak boleh kosong');
    } else {
      this.setState({
        loading: true,
      });
      var details = {
        _token: this.state._token,
        order_id: this.state.order_id,
        reason: this.state.selectedReason,
        message: this.state.comment,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post('https://syoobe.co.id/api/orderDeleteInMyOrder', formBody)
        .then((response) => {
          console.log('response', response);
          this.setState({
            loading: false,
          });
          if (response.data.status == 1) {
            Alert.alert('Permintaan pembatalan berhasil dikirim');

            this.props.navigation.pop();
          } else {
            Alert.alert('Informasi', response.data.data, [{text: 'OK'}]);
          }
        });
    }
  };

  submitRequestfromSales = () => {
    if (String(this.state.comment).trim() === '') {
      Alert.alert('Inputan tidak boleh kosong');
    } else {
      this.props.route.params.submitCancel(this.state.comment);
      this.setState({
        loading: true,
      });
      setTimeout(() => {
        this.setState({
          loading: false,
        });
        // this.props.navigation.pop();
      }, 3000);
    }
  };

  submitReturnRequest = async () => {
    if (
      this.state.returnQty === 0 ||
      this.state.returnReqReason === 0 ||
      String(this.state.refundComment).trim() === ''
    ) {
      Alert.alert('Inputan tidak boleh kosong');
    } else {
      this.setState({
        loading: true,
      });
      let ret;
      if (this.state.checked === 1) {
        ret = 'RP';
      } else if (this.state.checked === 2) {
        ret = 'RE';
      }
      var details = {
        _token: this.state._token,
        orderproduct_id: this.state.order_id,
        refund_qty: this.state.returnQty,
        refund_or_replaceg: ret,
        refund_reason: this.state.returnReqReason,
        refmsg_text: this.state.refundComment,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post('https://syoobe.co.id/api/submitRefundReturnRequest', formBody)
        .then((response) => {
          console.log('response', response);
          this.setState({
            loading: false,
          });
          if (response.data.status === 1) {
            Alert.alert('Permintaan Pengembalian berhasil dikirim');

            this.props.navigation.pop();
          } else {
            Alert.alert('Informasi', response.data.msg, [{text: 'OK'}]);
          }
        });
    }
  };

  submitCancelRequestAccept = async () => {
    if (String(this.state.comment).trim() === '') {
      Alert.alert('Harap masukkan alasan untuk pembatalan');
    } else {
      this.setState({
        loading: true,
      });

      var details = {
        _token: this.state._token,
        oder_product_id: this.props.route.params.order_product_id,
        cancel_status: this.props.route.params.cancel_status,
        comments: this.state.comment,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post('https://syoobe.co.id/api/submitCancelOrderInSale', formBody)
        .then((response) => {
          console.log('response', response);
          this.setState({
            loading: false,
          });
          if (response.data.status === 1) {
            Alert.alert('Informasi', response.data.msg, [
              {
                text: 'OK',
                onPress: () =>
                  this.props.navigation.replace('HomeDrawer', {
                    screen: 'DisputeManagementList',
                  }),
              },
            ]);
          } else {
            Alert.alert('Informasi', response.data.msg, [{text: 'OK'}]);
          }
        });
    }
  };

  submitCancelRequestDecline = async () => {
    this.setState({
      loading: true,
    });

    var details = {
      _token: this.state._token,
      oder_product_id: this.props.route.params.order_product_id,
      cancel_status: this.props.route.params.cancel_status,
      comments: 'Decline by Seller via mobile apps',
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post('https://syoobe.co.id/api/submitCancelOrderInSale', formBody)
      .then((response) => {
        console.log('response', response);
        this.setState({
          loading: false,
        });
        if (response.data.status === 1) {
          Alert.alert('Informasi', response.data.msg, [
            {
              text: 'OK',
              onPress: () =>
                this.props.navigation.replace('HomeDrawer', {
                  screen: 'DisputeManagementList',
                }),
            },
          ]);
        } else {
          Alert.alert('Informasi', response.data.msg, [{text: 'OK'}]);
        }
      });
  };

  render() {
    // console.log('in state', JSON.stringify(this.state));
    console.log('in props', JSON.stringify(this.props));

    const {checked} = this.state;
    let reasons = [];
    if (this.state.reasons) {
      this.state.reasons.map((value) => {
        reasons.push({value: value.reason_id, name: value.reason});
      });
    }
    let returnReasonList;
    if (this.state.returnReasonList) {
      returnReasonList = this.state.returnReasonList.map((value) => {
        return (
          <Picker.Item
            key={value.reason_id}
            value={value.reason_id}
            label={value.reason_value}
          />
        );
      });
    }
    let qty;
    if (this.state.qtyArray) {
      qty = this.state.qtyArray.map((value) => {
        return (
          <Picker.Item
            key={value.product_quantity}
            value={value.product_quantity}
            label={String(value.product_quantity)}
          />
        );
      });
    }
    if (this.state.loading) {
      return <Spinner color="red" size="large" />;
    }
    return (
      <ScrollView style={styles.scrollClass}>
        {this.state.type == 'cancel' && (
          <Card>
            {/* </View> */}
            <View style={{marginBottom: hp(2)}}>
              <ProductDropdown
                items={reasons}
                onPress={({val, ref}) => {
                  this.setState({selectedReason: val});
                  ref.close();
                }}
                value={this.state.selectedReason}
                title={'Alasan'}
              />
            </View>
            <CardSection>
              <InputText
                value={this.state.message}
                onChangeText={(val) =>
                  this.setState({
                    comment: val,
                  })
                }
                multiline
                numberOfLines={6}
                label="Keterangan"
              />
            </CardSection>
            <Button onPress={() => this.submitRequest()}>Kirim </Button>
          </Card>
        )}
        {this.state.type === 'cancelFromSale' && (
          <Card>
            <CardSection>
              <InputText
                value={this.state.message}
                onChangeText={(val) =>
                  this.setState({
                    comment: val,
                  })
                }
                label="Alasan Pembatalan"
                multiline
                numberOfLines={6}
              />
            </CardSection>
            <Button onPress={() => this.submitRequestfromSales()}>
              Kirim{' '}
            </Button>
          </Card>
        )}
        {this.state.type === 'feedback' && (
          <Card>
            <View style={{paddingBottom: 10}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.dropheadingClass}>Penilaian Produk</Text>
                <Text
                  style={{color: '#f00', fontSize: hp('2.2%'), marginTop: 5}}>
                  {' '}
                  *
                </Text>
              </View>
              <View style={{justifyContent: 'flex-end'}}>
                <AirbnbRating
                  count={5}
                  reviews={[
                    'Sangat Buruk',
                    'Buruk',
                    'Baik',
                    'Bagus',
                    'Sangat Bagus',
                  ]}
                  defaultRating={this.state.rating}
                  size={20}
                  showRating={true}
                  onFinishRating={(value) =>
                    this.setState({
                      rating: value,
                    })
                  }
                />
              </View>
            </View>
            <CardSection>
              <InputText
                value={this.state.review}
                onChangeText={(val) =>
                  this.setState({
                    review: val,
                  })
                }
                multiline
                numberOfLines={6}
                label="Katakan sesuatu mengenai produk ini"
              />
            </CardSection>
            <Button onPress={() => this.sendReview()}>Kirim Penialian</Button>
          </Card>
        )}
        {this.state.type === 'return' && (
          <Card>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.dropheadingClass}>Jumlah Pengembalian</Text>
              <Text style={{color: '#f00', fontSize: hp('2.2%'), marginTop: 5}}>
                {' '}
                *
              </Text>
            </View>
            <View style={styles.dropdownClass}>
              <Picker
                style={styles.pickerClass}
                selectedValue={this.state.returnQty}
                onValueChange={(val) =>
                  this.setState({
                    returnQty: val,
                  })
                }>
                <Picker.Item label="Select" value="0" key="0" />
                {qty}
              </Picker>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.dropheadingClass}>Alasan Permintaan</Text>
              <Text style={{color: '#f00', fontSize: hp('2.2%'), marginTop: 5}}>
                {' '}
                *
              </Text>
            </View>
            <View style={styles.dropdownClass}>
              <Picker
                style={styles.pickerClass}
                selectedValue={this.state.returnReqReason}
                onValueChange={(val) =>
                  this.setState({
                    returnReqReason: val,
                  })
                }>
                <Picker.Item label="Select" value="0" key="0" />
                {returnReasonList}
              </Picker>
            </View>
            <Text style={styles.dropheadingClass}>
              Apa yang ingin kamu perbarui
            </Text>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    checked: 1,
                  })
                }
                style={styles.radioWrappper}>
                <RadioButton
                  value={1}
                  color={'#00b3ff'}
                  uncheckedColor={'#29abe2'}
                  status={checked == 1 ? 'checked' : 'unchecked'}
                  onPress={() =>
                    this.setState({
                      checked: 1,
                    })
                  }
                />
                <Text style={styles.heading_style}>Ganti Barang</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    checked: 2,
                  })
                }
                style={styles.radioWrappper}>
                <RadioButton
                  value={2}
                  color={'#00b3ff'}
                  uncheckedColor={'#29abe2'}
                  status={checked == 2 ? 'checked' : 'unchecked'}
                  onPress={() =>
                    this.setState({
                      checked: 2,
                    })
                  }
                />
                <Text style={styles.heading_style}>Pengembalian</Text>
              </TouchableOpacity>
            </View>
            <CardSection>
              <TextareaInner
                value={this.state.refundComment}
                onChangeText={(val) =>
                  this.setState({
                    refundComment: val,
                  })
                }
                label="Komentar"
              />
            </CardSection>
            <ButtonTwo onPress={() => this.submitReturnRequest()}>
              Kirim{' '}
            </ButtonTwo>
          </Card>
        )}
        {this.state.type === 'CancelRequestAccept' && (
          <Card>
            <CardSection>
              <InputText
                value={this.state.comment}
                onChangeText={(val) =>
                  this.setState({
                    comment: val,
                  })
                }
                multiline
                numberOfLines={6}
                label="Keterangan"
              />
            </CardSection>
            <Button onPress={() => this.submitCancelRequestAccept()}>
              Memperbarui
            </Button>
          </Card>
        )}

        {this.state.type === 'CancelRequestDecline' && (
          <Card>
            <CardSection>
              <Text style={styles.dropheadingClass}>
                Dengan mengklik tombol memperbaharui, kamu menyatakan tidak
                menerima atau menolak permintaan pembatalan untuk pesanan ini.
              </Text>
            </CardSection>

            <Button onPress={() => this.submitCancelRequestDecline()}>
              Memperbarui
            </Button>
          </Card>
        )}
      </ScrollView>
    );
  }
}

const styles = {
  heading_style: {
    color: '#545454',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
  },
  radioWrappper: {
    height: hp('7.2%'),
    flexDirection: 'row',
    paddingTop: 4,
    paddingBottom: 4,
    alignItems: 'center',
    // borderBottomWidth:1,
    // borderColor:'#e7e7e7',
  },
  dropdownClass: {
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  pickerClass: {
    height: hp('8%'),
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  scrollClass: {
    backgroundColor: '#fff',
    position: 'relative',
  },
};
