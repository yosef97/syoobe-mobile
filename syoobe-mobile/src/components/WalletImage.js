import React, {Component} from 'react';
import {
  TouchableWithoutFeedback,
  Button,
  TextInput,
  Modal,
  Text,
  Image,
  Animated,
  View,
  StyleSheet,
  PanResponder,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import axios from 'axios';
import {Font} from './Font';
import {formatRupiah, countNumber} from '../helpers/helper';

class WalletImage extends Component {
  constructor(props) {
    super(props);
    const position = new Animated.ValueXY();
    const panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gesture) => {
        position.setValue({x: gesture.dx, y: gesture.dy});
      },
      onPanResponderRelease: (event, gesture) => {
        if (
          gesture.dy >= 210 &&
          gesture.dy <= 310 &&
          gesture.dx <= 45 &&
          gesture.dx >= -55
        ) {
          position.setValue({x: 0, y: 0});
          this.showPopUpForAmount();
        } else {
          position.setValue({x: 0, y: 0});
        }
      },
    });
    this.state = {
      panResponder,
      position,
      accountNumberLast4Digit: String(
        this.props.data.user_details.ub_account_number,
      ).substring(this.props.data.user_details.ub_account_number.length - 4),
      willShowModal: false,
      arrowHide: 1,
      amount: '',
      inputError: false,
      start: false,
    };
  }

  showPopUpForAmount = () => {
    this.changeModalState();
  };

  changeModalState = () => {
    this.setState((prevState, prevProps) => {
      return {
        willShowModal: !prevState.willShowModal,
        inputError: false,
      };
    });
  };

  comfirmPayment = async () => {
    console.log('ini confirm payment');
    this.setState({
      inputError: false,
    });
    const {data, _token, updateLoading, onWithDrawal} = this.props;
    const withdrawal_amount = countNumber(this.state.amount);
    console.log('ini withdraw amount', withdrawal_amount);
    if (
      this.state.amount === '' ||
      this.state.amount === 0 ||
      !this.state.amount > 50
    ) {
      this.setState({
        inputError: true,
      });
      Alert.alert('Jumlah yang anda masukan tidak valid');
    } else if (this.state.amount > this.props.balance) {
      this.setState({
        inputError: true,
      });
      Alert.alert('Saldo anda tidak mencukupi');
    } else if (
      this.props.balance === 0 ||
      this.props.balance === '' ||
      this.props.balance === null ||
      this.props.balance === undefined
    ) {
      Alert.alert('Saldo anda tidak mencukupi');
    } else {
      this.changeModalState();
      updateLoading(true);
      let details = {
        _token: _token,
        withdrawal_amount: withdrawal_amount,
        ub_bank_name: data.user_details.ub_bank_name,
        ub_account_holder_name: data.user_details.ub_account_holder_name,
        ub_account_number: data.user_details.ub_account_number,
        ub_ifsc_swift_code: data.user_details.ub_ifsc_swift_code,
        ub_bank_address: data.user_details.ub_bank_address,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post('https://syoobe.co.id/api/submitWithdrawlRequest', formBody)
        .then((response) => {
          console.log('submitttt', JSON.stringify(response));
          if (response.data.status === 1) {
            updateLoading(false);
            this.setState({
              bankData: response.data,
            });
            onWithDrawal(this.state.bankData);
            Alert.alert('Sukses', 'Penarikan dana sedang di proses', [
              {
                text: 'OK',
                onPress: () => this.props.navigation.navigate('MyWallet'),
              },
            ]);
          } else {
            Alert.alert('Gagal', 'Penarikan maksimal 1 x 24 Jam', [
              {
                text: 'OK',
                onPress: () => this.props.navigation.navigate('MyWallet'),
              },
            ]);
            // console.log('submitttt', JSON.stringify(response));
          }
        });
    }
  };

  componentDidMount() {
    this.arrowChange();
  }

  arrowChange = () => {
    setTimeout(() => {
      this.setState((prevState, prevProps) => {
        return {
          arrowHide: prevState.arrowHide === 5 ? 1 : prevState.arrowHide + 1,
        };
      });
    }, 400);
  };

  render() {
    console.log(
      'ini true false balance',
      !this.state.amount > this.props.balance,
    );
    console.log('ini true false:', !this.state.amount > 50);
    // console.log('ini props', JSON.stringify(this.props));
    // console.log('ini state', JSON.stringify(this.state));
    console.log('ini state', JSON.stringify(this.state.amount));

    console.log('ini balance', this.props.balance);
    let handles = this.state.panResponder.panHandlers;
    const {
      arrowImageStyle,
      headingText,
      modalHeadingStyle,
      bottonWrapperView,
      innerModal,
      outerModal,
      textInputStyleError,
      textInputStyle,
      imageView,
      accountNumber,
      animatedStyle,
      image,
      bankAccountImage,
    } = styles;
    const {accountNumberLast4Digit, arrowHide, inputError} = this.state;
    return (
      <View style={{height: '100%', alignItems: 'center'}}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'space-evenly',
          }}>
          <Text style={headingText}>Geser untuk melakukan penarikan dana</Text>

          <Button
            color={'#C90205'}
            title={'Detail Bank'}
            onPress={this.props.toBankEdit}
          />
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.willShowModal}
            onRequestClose={() => {
              this.changeModalState();
            }}>
            <TouchableWithoutFeedback onPress={this.changeModalState}>
              <View style={outerModal}>
                <View style={innerModal}>
                  <Text style={modalHeadingStyle}>
                    {' '}
                    Masukkan jumlah yang akan ditarik{' '}
                  </Text>
                  <View
                    style={{
                      margin: 20,
                    }}>
                    <TextInput
                      style={inputError ? textInputStyleError : textInputStyle}
                      onChangeText={(amount) => this.setState({amount})}
                      value={formatRupiah(this.state.amount)}
                      placeholder={'Masukkan Jumlah'}
                      keyboardType={'numeric'}
                    />
                  </View>
                  <View style={bottonWrapperView}>
                    <Button
                      color={'#C90205'}
                      title={'Batal'}
                      onPress={this.changeModalState}
                    />
                    <Button title={'Penarikan'} onPress={this.comfirmPayment} />
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
          <Animated.View
            style={[animatedStyle, this.state.position.getLayout()]}
            {...handles}>
            <Image style={image} source={require('../images/wallet.png')} />
          </Animated.View>
          <View
            style={{
              height: hp('3.5%'),
            }}>
            {arrowHide === 1 ? null : (
              <Image
                style={arrowImageStyle}
                source={require('../images/down.png')}
              />
            )}
          </View>
          <View
            style={{
              height: hp('3.5%'),
            }}>
            {arrowHide === 2 ? null : (
              <Image
                style={arrowImageStyle}
                source={require('../images/down.png')}
              />
            )}
          </View>
          <View
            style={{
              height: hp('3.5%'),
            }}>
            {arrowHide === 3 ? null : (
              <Image
                style={arrowImageStyle}
                source={require('../images/down.png')}
              />
            )}
          </View>
          <View
            style={{
              height: hp('3.5%'),
            }}>
            {arrowHide == 4 ? null : (
              <Image
                style={arrowImageStyle}
                source={require('../images/down.png')}
              />
            )}
          </View>
          <View
            style={{
              height: hp('3.5%'),
            }}>
            {arrowHide == 5 ? null : (
              <Image
                style={arrowImageStyle}
                source={require('../images/down.png')}
              />
            )}
          </View>
          <View
            style={{
              marginTop: hp('4%'),
            }}>
            <View style={imageView}>
              <Image
                style={bankAccountImage}
                source={require('../images/bank-account.png')}
              />
            </View>
            <Text style={accountNumber}>XXXXXX{accountNumberLast4Digit}</Text>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  arrowImageStyle: {
    height: hp('3.5%'),
    width: wp('7%'),
  },
  modalHeadingStyle: {
    color: '#545454',
    fontSize: hp('2.5%'),
    fontFamily: Font.RobotoRegular,
    textAlign: 'center',
  },
  bottonWrapperView: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    margin: 20,
    width: '100%',
  },
  innerModal: {
    width: wp('80%'),
    height: hp('30%'),
    backgroundColor: '#fff',
    padding: 20,
    zIndex: 5,
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  outerModal: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  textInputStyle: {
    height: 40,
    borderRadius: 10,
    width: '30%',
    borderColor: 'gray',
    borderWidth: 1,
  },
  textInputStyleError: {
    height: 40,
    borderRadius: 10,
    width: '30%',
    borderColor: 'red',
    borderWidth: 2,
  },
  image: {
    height: hp('7%'),
    width: wp('14%'),
  },
  bankAccountImage: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
  },
  animatedStyle: {
    marginBottom: hp('4%'),
    zIndex: 1001,
    alignItems: 'center',
    justifyContent: 'center',
  },
  accountNumber: {
    textAlign: 'center',
    fontFamily: Font.RobotoRegular,
  },
  headingText: {
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.2%'),
    color: '#545454',
    padding: 5,
  },
  imageView: {
    borderColor: 'black',
    borderRadius: 40,
    borderWidth: 1,
    height: hp('10%'),
    width: wp('20%'),
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default WalletImage;
