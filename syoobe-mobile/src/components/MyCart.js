import React, {Component} from 'react';
import {
  Alert,
  Text,
  Image,
  TouchableOpacity,
  View,
  ScrollView,
  AsyncStorage,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Button, Spinner} from './common';
import {Font} from './Font';
import {
  applyReward,
  removeCoupon,
  applyCoupon,
  viewCart,
  deleteCartItem,
  editCartItemCount,
} from '../actions';
import {connect} from 'react-redux';
import Spinner2 from 'react-native-loading-spinner-overlay';
import axios from 'axios';
import {BASE_URL} from '../services/baseUrl';
import {formatRupiah} from '../helpers/helper';
import {ApplyRewardPoin} from './common/chart/ApplyRewardPoin';
import {ApplyCoupon} from './common/chart/ApplyCoupon';
import Bugsnag from '@bugsnag/react-native';

class MyCart extends Component {
  product_description = null;

  constructor(props) {
    super(props);
    this.state = {
      coupon: '',
      _token: null,
      showRewardInput: false,
      rewardText: 0,
      loading: false,
      product_type: null,
      applied: false,
    };
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
    if (this.product_description === 'digital' && this.props.cart_items) {
      this.handleDeleteCartItem(this.props.cart_items.products[0]);
    }
  }

  componentDidMount() {
    console.log('componentDidMount remove coupon');
    if (this.props.coupon_name) {
      setTimeout(() => {
        this.removeCoupon();
      }, 1000);
    }

    if (this.props.route.params) {
      this.product_description = this.props.route.params.product_description;
      this.setState({
        product_type: this.props.route.params.product_description,
      });
    }
    if (this.props.cart_items !== undefined) {
      var arr1 = [];
      arr1 = Object.values(this.props.cart_items.products);
      for (let i = 0; i < arr1.length; i++) {
        if (arr1[i].product_type === 'D') {
          this.setState({
            product_type: 'digital',
          });
        }
        break;
      }
    }
    this.requestViewCart();
  }

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      this.setState({
        _token: userToken,
      });
      return userToken;
    } catch (error) {
      Bugsnag.notify(error);
      console.log(error);
    }
    return;
  };

  /**
   * This method will call the action to view the cart items after retrieving the token.
   */
  requestViewCart = async () => {
    await this.retrieveToken()
      .then((_token) => {
        this.props.viewCart({_token});
      })
      .catch((error) => {
        Bugsnag.notify(error);
        console.log('Promise is rejected with error: ' + error);
      });
  };

  /**
   * This method will call the action to delete the cart item.
   */
  handleDeleteCartItem = async (item) => {
    const item_key = item.key;
    await this.retrieveToken()
      .then((_token) => this.props.deleteCartItem({_token, item_key}))
      .catch((error) => {
        Bugsnag.notify(error);
        console.log('Promise is rejected with error: ' + error);
      });
  };

  increaseProductCount = async (item) => {
    const product_key = item.key;
    const quantity = item.qty_number + 1;
    await this.retrieveToken()
      .then((_token) =>
        this.props.editCartItemCount({_token, product_key, quantity}),
      )
      .catch((error) => {
        Bugsnag.notify(error);
        console.log('Promise is rejected with error: ' + error);
      });
  };

  decreaseProductCount = async (item) => {
    const product_key = item.key;
    const quantity = item.qty_number - 1;
    await this.retrieveToken()
      .then((_token) =>
        this.props.editCartItemCount({_token, product_key, quantity}),
      )
      .catch((error) => {
        Bugsnag.notify(error);
        console.log('Promise is rejected with error: ' + error);
      });
  };

  toProduct = (id, name) => {
    this.props.navigation.push('ProductDetail', {
      product_id: id,
      product_name: name,
    });
  };

  renderEachCartItem = (cart_items) => {
    // console.log('ini cart item ', JSON.stringify(cart_items));
    const {lftBtn, decressBtnStyle, inputStyle, incressBtnStyle} = styles;

    if (cart_items.products !== undefined) {
      let arr = [];
      arr = Object.values(cart_items.products);
      return arr.map((item) => (
        <View key={item.id}>
          <View
            style={{
              flex: 1,
              marginBottom: hp(1),
              backgroundColor: '#fff',
              paddingHorizontal: wp(3),
              paddingVertical: hp(1.5),
            }}>
            <Text
              style={{
                fontSize: hp(2),
                fontWeight: 'bold',
                color: '#000',
                textTransform: 'capitalize',
              }}>
              {item.store_name}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                //   borderTopColor: '#eaeaea',
                //   borderTopWidth: 1,
                paddingVertical: hp(1.5),
              }}>
              <View
                style={{
                  width: 70,
                  height: 70,
                  maxWidth: 70,
                  maxHeight: 70,
                  backgroundColor: '#eaeaea',
                  borderRadius: 5,
                }}>
                <Image
                  style={{
                    width: '100%',
                    height: '100%',
                    borderRadius: 5,
                  }}
                  source={{uri: item.product_image}}
                />
              </View>
              <View style={{flex: 1, marginLeft: wp(3)}}>
                <Text
                  style={{
                    fontSize: hp(2.2),
                    color: '#000',
                    fontWeight: '700',
                  }}>
                  {item.product_name}
                </Text>
                {/* <Text style={{fontSize: hp(2)}}>Jumlah barang 1</Text> */}
                <View style={{flexDirection: 'row'}}>
                  <Text
                    style={{
                      fontSize: hp(2.2),
                      color: 'rgba(235, 47, 6,1.0)',
                      fontWeight: 'bold',
                    }}>
                    {`Rp. ${formatRupiah(item.unit_price)}`}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  }}>
                  <View style={lftBtn}>
                    <TouchableOpacity
                      disabled={
                        this.props.editCartLoading || item.qty_number < 2
                      }
                      onPress={() => this.decreaseProductCount(item)}>
                      <Text style={decressBtnStyle}>-</Text>
                    </TouchableOpacity>
                    <Text style={inputStyle}>{item.qty_number}</Text>
                    <TouchableOpacity
                      disabled={this.props.editCartLoading}
                      onPress={() => this.increaseProductCount(item)}>
                      <Text style={incressBtnStyle}>+</Text>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity
                    onPress={() => this.handleDeleteCartItem(item)}>
                    <Image
                      style={styles.deleteIcon}
                      source={require('../images/del_icon.png')}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      ));
    }
  };

  applyCoupon = async () => {
    if (String(this.state.coupon).trim() !== '') {
      var details = {
        _token: this.state._token,
        coupon: this.state.coupon,
      };
      this.props.applyCoupon(details);
    } else {
      Alert.alert('Mohon periksa kembali kupon anda');
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    console.log('componentDidUpdate');
    if (
      prevProps.coupon_message !== this.props.coupon_message &&
      this.props.coupon_message !== null
    ) {
      Alert.alert('Informasi', this.props.coupon_message, [{text: 'OK'}]);
    }

    if (
      prevProps.reward_point_message !== this.props.reward_point_message &&
      this.props.reward_point_message !== null
    ) {
      Alert.alert('Informasi', this.props.reward_point_message, [{text: 'OK'}]);
    }
  };

  removeCoupon = () => {
    var details = {
      _token: this.state._token,
    };
    this.props.removeCoupon(details);
  };

  showRewardText = () => {
    this.setState({
      showRewardInput: !this.state.showRewardInput,
    });
  };

  applyRewardPoints = () => {
    var details = {
      _token: this.state._token,
      reward_points: this.state.rewardText,
    };
    this.props.applyReward(details);
  };

  nextPage = async () => {
    if (
      this.product_description === 'digital' ||
      this.state.product_type === 'digital'
    ) {
      this.setState({
        loading: true,
      });
      let details = {
        _token: this.state._token,
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      axios
        .post(BASE_URL + '/finalPaymentSummary', formBody)
        .then((response) => {
          this.setState({
            loading: false,
          });
          console.log(
            'ini res final payment sum',
            JSON.stringify(response.data),
          );
          if (response.data.status === 1) {
            this.props.navigation.replace('FinalPayment', {
              data: response.data,
              _token: this.state._token,
              digital: true,
              props: this.props,
            });
          } else {
            // console.log('ini res', JSON.stringify(response));
            Alert.alert('Informasi', response.data.msg, [{text: 'OK'}]);
          }
        });

      // this.props.navigation.navigate('FinalPayment');
    } else {
      this.props.navigation.replace('Payment');
    }
  };

  render() {
    console.log('message', this.props.message);
    const {wrapper, wrapperView} = styles;
    if (this.props.loading || this.state.loading) {
      return <Spinner color="red" size="large" />;
    }

    if (this.props.cart_count === 0) {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontFamily: Font.RobotoRegular, fontSize: 25}}>
            Keranjang Kosong
          </Text>
        </View>
      );
    }

    // let arr = [];
    // arr = Object.values(this.props.cart_items.products);
    // arr.map((item) => console.log('ini item', ...item.qty_number));

    // console.log(JSON.stringify(this.props.cart_items.products));

    return (
      <View style={{flex: 1}}>
        <ScrollView keyboardShouldPersistTaps={'always'} style={wrapper}>
          <Spinner2 visible={this.props.editCartLoading} />
          <View style={wrapperView}>
            {this.renderEachCartItem(this.props.cart_items)}

            <View style={styles.cardHeader}>
              <Text
                style={{
                  ...styles.billingAddress,
                  paddingBottom: 5,
                  marginTop: 0,
                }}>
                Rincian Harga
              </Text>
            </View>
            <View style={styles.cardBody}>
              <View style={styles.cardItem}>
                {/* <Text>{`Jumlah Barang (${this.props.cart_items.products[0]?.qty_number})`}</Text> */}
                <Text>{`Jumlah Barang Berbeda (${this.props.cart_count})`}</Text>
                <Text>
                  {'Rp. '}
                  {formatRupiah(this.props.cart_total_price)}
                </Text>
              </View>
              {this.props.isCouponApplied && (
                <View style={styles.cardItem}>
                  <Text>{`Kupon (${this.props.coupon_name})`}</Text>
                  <Text>
                    Rp. -{formatRupiah(parseInt(this.props.coupon_value))}
                  </Text>
                </View>
              )}
              {this.props.tax_data && (
                <View style={styles.cardItem}>
                  <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                    Pajak
                  </Text>
                  <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                    Rp. {this.props.tax_data?.value}
                  </Text>
                </View>
              )}
              <View style={styles.cardItem}>
                <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                  Total Pembayaran
                </Text>
                <Text style={{fontSize: hp(2), fontWeight: 'bold'}}>
                  Rp.
                  {formatRupiah(this.props.net_payable_amount)}
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            paddingHorizontal: wp(3),
            paddingVertical: hp(2),
            backgroundColor: '#fff',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            borderBottomColor: '#f5f6fa',
            borderBottomWidth: 1,
          }}>
          <Text>Kupon dipakai</Text>
          <ApplyCoupon
            onChange={(value) => this.setState({coupon: value})}
            onPress={() => this.applyCoupon()}
            onReset={() => this.removeCoupon()}
            coupon={this.props.coupon_name}
            value={this.state.coupon}
          />
        </View>
        {this.props.available_reward_point && (
          <View
            style={{
              paddingHorizontal: wp(3),
              paddingVertical: hp(2),
              backgroundColor: '#fff',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              borderBottomColor: '#f5f6fa',
              borderBottomWidth: 1,
            }}>
            <Text>
              Poin Hadiah (Rp.
              {formatRupiah(this.props.available_reward_point)})
            </Text>
            <ApplyRewardPoin
              onChange={(value) =>
                this.setState({
                  rewardText: formatRupiah(value, false),
                })
              }
              onPress={() => this.applyRewardPoints()}
              value={
                this.state.rewardText ||
                formatRupiah(this.props.reward_data_applied, false)
              }
              availablePoint={formatRupiah(
                this.props.available_reward_point,
                false,
              )}
            />
          </View>
        )}
        <View style={styles.cardButton}>
          <Button onPress={() => this.nextPage()}>Lanjut</Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.cart.loading,
    cart_count: state.cart.cart_count,
    cart_total_price: state.cart.cart_total_price,
    net_payable_amount: state.cart.net_payable_amount,
    cart_items: state.cart.cart_items,
    message: state.cart.message,
    editCartLoading: state.cart.editCartLoading,
    coupon_name: state.cart.coupon_name,
    coupon_message: state.cart.coupon_message,
    price_currency: state.cart.price_currency,
    isCouponApplied: state.cart.isCouponApplied,
    coupon_value: state.cart.coupon_value,
    available_reward_point: state.cart.available_reward_point,
    reward_data_applied: state.cart.reward_data,
    rewardsCouldbeApplied: state.cart.cart_max_rewards_points,
    reward_point_message: state.cart.reward_point_message,
    tax_data: state.cart.tax_data,
  };
};

const styles = {
  cardBody: {
    flex: 1,
    marginBottom: hp(1),
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    paddingVertical: hp(1.5),
  },
  cardItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 5,
  },
  cardButton: {
    // flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
    // marginBottom: hp(2),
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex: 1,
    marginTop: hp(1),
    backgroundColor: '#fff',
    paddingHorizontal: wp(3),
    paddingTop: hp(1.5),
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
  },
  billingAddress: {
    fontSize: wp('4.3%'),
    color: '#4d4d4d',
    fontFamily: Font.RobotoMedium,
    marginBottom: hp('1%'),
    marginTop: hp('2%'),
  },
  wrapper: {
    // padding: 10,
    backgroundColor: '#f5f6fa',
    height: '100%',
  },
  wrapperView: {
    paddingBottom: 20,
  },
  section1: {
    flexDirection: 'row',

    paddingBottom: 12,
    marginBottom: 12,
    width: '100%',
    flexShrink: 0,
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },
  productImg: {
    flexBasis: wp('20%'),
    width: wp('20%'),
    height: wp('28%'),

    justifyContent: 'flex-start',
  },
  middleTxt: {
    flexBasis: wp('60%'),
    width: wp('60%'),

    alignItem: 'left',
    flexShrink: 0,
    paddingLeft: wp('2%'),
  },
  deleteIcon: {
    // flexBasis: wp('13%'),
    height: wp('14%'),
    width: wp('14%'),
    // backgroundColor: 'red',
    marginTop: 8,
    // justifyContent: 'flex-end',
    // flexShrink: 0,
  },
  titleStyle: {
    color: '#545454',
    fontSize: wp('4%'),
    paddingBottom: 0,
    // fontSize:22,
    fontFamily: Font.RobotoRegular,
  },
  soldStyle: {
    color: '#858585',
    fontSize: hp('2%'),
    // fontSize:18,
    marginBottom: 5,
    fontFamily: Font.RobotoRegular,
  },
  critiStyle: {
    color: '#00b3ff',
    fontFamily: Font.RobotoRegular,
  },

  priceTxt: {
    color: '#00b3ff',
    // fontWeight:'bold',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoBold,
  },
  footerInner: {
    width: '50%',
    textAlign: 'left',
    borderWidth: 1,
    borderColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  lftBtn: {
    width: wp('29%'),
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderTopRightRadius: wp('29%'),
    borderTopLeftRadius: wp('29%'),
    borderBottomLeftRadius: wp('29%'),
    borderBottomRightRadius: wp('29%'),
    elevation: 5,
    marginTop: 8,
  },
  decressBtnStyle: {
    //  width:30,
    width: wp('9%'),
    backgroundColor: '#fff',
    textAlign: 'center',
    alignItems: 'center',
    borderTopRightRadius: 0,
    borderTopLeftRadius: wp('9%'),
    borderBottomLeftRadius: wp('9%'),
    borderBottomRightRadius: 0,
    color: '#a9a9a9',
    // fontSize:20,
    fontSize: wp('6%'),
  },
  incressBtnStyle: {
    // width:30,
    width: wp('9%'),
    backgroundColor: '#fff',
    textAlign: 'center',
    alignItems: 'center',
    borderTopRightRadius: wp('9%'),
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: wp('9%'),
    color: '#a9a9a9',
    // fontSize:20,
    fontSize: wp('6%'),
  },
  inputStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
    color: '#000',
    // width:52,
    // height:52,
    width: wp('10%'),
    height: wp('10%'),
    // fontSize: 18,
    fontSize: hp('2%'),
    marginTop: 0,
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: wp('10%'),
    backgroundColor: '#fff',
    textAlign: 'center',
    elevation: 5,
    fontFamily: Font.RobotoLight,
  },
  priceHeading: {
    color: '#696969',
    fontSize: wp('5%'),
    fontFamily: Font.RobotoMedium,
  },
  totalPriceWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: hp('1.5%'),
    paddingTop: hp('1.5%'),
    borderBottomWidth: 1,
    borderColor: '#e7e7e7',
  },

  totalItem: {
    fontSize: wp('4.2%'),
    color: '#6f6f6f',
    fontFamily: Font.RobotoRegular,
  },
  applyCouponView: {
    elevation: 1,
    justifyContent: 'center',
    padding: 15,
    alignItems: 'center',
    marginTop: 15,
    borderRadius: 2,
    marginBottom: 15,
  },
  applyCoupon: {
    // textAlign:"center",
    color: '#848484',
    fontSize: wp('5%'),
    fontWeight: 'bold',
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
  },
  inputStyle2: {
    borderWidth: 2,
    // borderTopWidth:0,
    borderColor: '#e0e0e0',
    height: wp('11%'),
    fontSize: wp('4%'),
    width: '70%',
    textAlign: 'center',
    borderTopWidth: 0,
  },
  btnWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '75%',
    marginTop: 15,
  },
  btn_Apply: {
    color: '#00b3ff',
    fontFamily: Font.RobotoRegular,
  },
  btn_Remove: {
    color: '#c90305',
    fontFamily: Font.RobotoRegular,
  },
  cartEmpty: {
    width: wp('50%'),
    height: wp('50%'),
    marginLeft: 'auto',
    marginRight: 'auto',
    alignSelf: 'auto',
    transform: [{translateY: hp('25%')}],
  },
};

export default connect(mapStateToProps, {
  applyReward,
  removeCoupon,
  applyCoupon,
  viewCart,
  deleteCartItem,
  editCartItemCount,
})(MyCart);
