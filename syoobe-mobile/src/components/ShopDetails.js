import React, {Component} from 'react';
import {
  Modal,
  AsyncStorage,
  Picker,
  StatusBar,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  View,
  ScrollView,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Font} from './Font';
import {
  Spinner,
  CardSection,
  TextareaInner,
  ButtonTwo,
  InputInner,
} from './common';
import axios from 'axios';
import {AirbnbRating} from 'react-native-ratings';
import {connect} from 'react-redux';
import Share from 'react-native-share';
import {BASE_URL} from '../services/baseUrl';
import {formatRupiah} from '../helpers/helper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {ERROR_MESSAGE} from '../constants/constants';

class ShopDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seg: 1,
      chosenDate: new Date(),
      shops_id: null,
      shop_details: null,
      loading: true,
      showComponmentB: false,
      products: [],
      favourites: [],
      changeView: 1,
      shopData: null,
      reviews: [],
      _token: null,
      reasons: [],
      selected_reason: 0,
      comment: '',
      innerLoader: false,
      myShop: false,
      sendMessage: false,
      subject: '',
      message: '',
      user_id: null,
      shopShareLink: null,
      shopReported: false,
    };
    this.setDate = this.setDate.bind(this);
  }

  retrieveShop = async () => {
    try {
      const shop_id = await AsyncStorage.getItem('shop_id');
      this.setState({
        shops_id: shop_id,
        myShop: true,
      });
      return shop_id;
    } catch (error) {
      console.log(error);
    }
    return true;
  };

  setHeaderShopName = (name) => {
    console.log('inside header', this.props);
    const {setParams} = this.props.navigation;
    setParams({shop_name: name});
  };

  async componentDidMount() {
    console.log('ini did mount');
    await this.retrieveToken();
    if (!this.props.route.params && !this.props.route.params?.shops_id) {
      this.retrieveShop().then((shop_id) => {
        if (shop_id === null) {
          this.setState({
            loading: false,
          });
          Alert.alert('Informasi', 'Tidak ada toko, Silahkan buat toko', [
            {
              text: 'OK',
              onPress: () => this.props.navigation.navigate('CreatShopStepOne'),
            },
          ]);
        } else {
          var details = {};
          details = {
            ...details,
            ...{shops_id: shop_id},
          };
          var formBody = [];
          for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + '=' + encodedValue);
          }
          formBody = formBody.join('&');
          axios
            .post(BASE_URL + '/individuallyFeatureShopDispalyDetails', formBody)
            .then((response) => {
              console.log('log', response);
              this.setState({
                loading: false,
              });
              if (response.data.status === 1) {
                this.setState({
                  shop_details: response.data.shop_data[0],
                  products: response.data.product_details,
                  shopShareLink: String(
                    response.data.shop_data[0]
                      .shop_view_path_for_social_platform,
                  ).trim(),
                });
              } else {
                Alert.alert('Tidak ada Toko!', [{text: 'OK'}]);
              }
            })
            .catch(() => {
              Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
            });
        }
      });
    } else {
      var id = this.props.route.params.shops_id;
      await this.setState({
        shops_id: id,
      });
      var details = {};
      details = {
        ...details,
        ...{shops_id: this.state.shops_id},
      };
      if (this.state._token != null) {
        details = {
          ...details,
          ...{_token: this.state._token},
        };
      }
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post(BASE_URL + '/individuallyFeatureShopDispalyDetails', formBody)
        .then((response) => {
          // console.log(response);
          if ((response.data.status = 1)) {
            this.setState({
              shop_details: response.data.shop_data[0],
              products: response.data.product_details,
              shopShareLink: String(
                response.data.shop_data[0].shop_view_path_for_social_platform,
              ).trim(),
              loading: false,
            });
            this.setHeaderShopName(this.state.shop_details.shop_name);
          } else {
            Alert.alert('Tidak ada Toko!', [{text: 'OK'}]);
          }
        })
        .catch(() => {
          Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
        });
    }
  }

  setDate(newDate) {
    this.setState({chosenDate: newDate});
  }
  _toggleShow = () => {
    // this.setState({showComponmentB: !this.state.showComponmentB})
    let shareOptions = {
      title: 'Bagikan',
      // message: "Syoobe",
      url: this.state.shopShareLink,
      subject:
        'Selamat Datang di ' +
        this.state.shop_details.shop_name +
        ' di Syoobe.',
    };
    Share.open(shareOptions);
  };

  findWhoFavourited = async () => {
    this.setState({
      changeView: 2,
      innerLoader: true,
    });
    var details = {};
    details = {
      ...details,
      ...{shops_id: this.state.shops_id},
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/individuallyFeatureShopFavourite', formBody)
      .then((response) => {
        if (response.data.favdata instanceof Array) {
          this.setState({
            favourites: response.data.favdata,
            innerLoader: false,
          });
        } else {
          this.setState({
            innerLoader: false,
          });
        }
      })
      .catch(() => {
        Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
      });
  };

  retrieveToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('token');
      const user_id = await AsyncStorage.getItem('user_id');
      this.setState({
        _token: userToken,
        user_id: user_id,
      });
      return userToken;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  reportThisShop = () => {
    this.setState({
      changeView: 3,
      innerLoader: true,
    });
    this.retrieveToken()
      .then((_token) => {
        var details = {};
        details = {
          ...details,
          ...{_token: _token},
        };
        var formBody = [];
        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        formBody = formBody.join('&');
        axios
          .post(BASE_URL + '/reportReasonAPI', formBody)
          .then((response) => {
            this.setState({
              reasons: response.data.reasonlist,
              innerLoader: false,
            });
          })
          .catch(() => {
            Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
          });
      })
      .catch((err) => console.log(err));
  };

  showPolicies = async () => {
    this.setState({
      changeView: 4,
      innerLoader: true,
    });
    var details = {};
    details = {
      ...details,
      ...{shops_id: this.state.shops_id},
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/individuallyFeatureShopPolicy', formBody)
      .then((response) => {
        this.setState({
          shopData: response.data.shop_data[0],
          innerLoader: false,
        });
      })
      .catch(() => {
        Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
      });
  };

  showReviews = async () => {
    this.setState({
      changeView: 5,
      innerLoader: true,
    });
    var details = {};
    details = {
      ...details,
      ...{shops_id: this.state.shops_id},
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/individuallyFeatureShopReview', formBody)
      .then((response) => {
        this.setState({
          reviews: response.data.product_review_details,
          innerLoader: false,
        });
      })
      .catch(() => {
        Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
      });
  };

  favouriteShop = async () => {
    var details = {
      shop_id: this.state.shops_id,
      _token: this.state._token,
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');
    await axios
      .post(BASE_URL + '/makeFavouriteShopAPI', formBody)
      .then((response) => {
        // console.log('rr', response);
        if (response.data.status === 1) {
          let temp = this.state.shop_details;
          temp.is_shop_favourite = !temp.is_shop_favourite;
          this.setState({
            shop_details: temp,
          });
          Alert.alert('Informasi', response.data.msg, [{text: 'OK'}]);
        } else {
          Alert.alert('Informasi', response.data.msg, [{text: 'OK'}]);
        }
      })
      .catch(() => {
        Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
      });
  };

  reportShop = async () => {
    if (
      this.state.selected_reason !== 0 &&
      String(this.state.comment).trim() !== ''
    ) {
      this.setState({
        innerLoader: true,
      });
      var details = {};
      details = {
        ...details,
        ...{shops_id: this.state.shops_id},
        ...details,
        ...{_token: this.state._token},
        ...details,
        ...{sreport_reason: this.state.selected_reason},
        ...details,
        ...{sreport_message: this.state.comment},
      };
      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      axios
        .post(BASE_URL + '/sentReportAPI', formBody)
        .then((response) => {
          this.setState({
            innerLoader: false,
          });
          if (response.data.status === 1) {
            this.setState({
              shopReported: true,
            });
            Alert.alert(
              'Informasi',
              'Laporan Anda telah berhasil dikirim untuk ditinjau.',
              [{text: 'OK'}],
            );
          } else {
            Alert.alert('Informasi', response.data.msg, [{text: 'OK'}]);
          }
        })
        .catch(() => {
          Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
        });
    } else {
      Alert.alert('Informasi', 'Silakan isi semua kolom yang wajib diisi', [
        {text: 'OK'},
      ]);
    }
  };

  sendMessageToShop = () => {
    this.setState({
      sendMessage: true,
    });
  };

  closeMessageModal = () => {
    this.setState({
      sendMessage: false,
      subject: '',
      message: '',
    });
  };

  sendMessageContent = async () => {
    if (
      String(this.state.subject).trim() === '' ||
      String(this.state.message).trim() === ''
    ) {
      Alert.alert('Informasi', 'Kedua bidang wajib diisi', [{text: 'OK'}]);
    } else {
      var details = {};
      details = {
        ...details,
        ...{shop_id: this.state.shops_id},
        ...details,
        ...{_token: this.state._token},
        ...details,
        ...{thread_subject: this.state.subject},
        ...details,
        ...{message_text: this.state.message},
        ...details,
        ...{user_id: this.state.user_id},
      };

      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
      formBody = formBody.join('&');
      await axios
        .post(BASE_URL + '/sendMessageToShopOwner', formBody)
        .then((response) => {
          console.log('r', response);
          this.setState({
            sendMessage: false,
            subject: '',
            message: '',
          });
          if (response.data.status === 1) {
            Alert.alert('Informasi', 'Pesan terkirim!', [{text: 'OK'}]);
          }
        })
        .catch(() => {
          Alert.alert('Informasi', ERROR_MESSAGE, [{text: 'OK'}]);
        });
    }
  };

  render() {
    console.log('in state', JSON.stringify(this.state));
    console.log('in props', JSON.stringify(this.props));

    var ratingsArray = [];
    var rounded_off_rating = null;
    if (this.state.shop_details) {
      ratingsArray = String(this.state.shop_details.shop_rating).split('.');
      if (ratingsArray[1] >= 5) {
        rounded_off_rating = ratingsArray[0] + 1;
      } else {
        rounded_off_rating = ratingsArray[0];
      }
    }
    let reasons;
    if (this.state.reasons) {
      reasons = this.state.reasons.map((value) => {
        return (
          <Picker.Item
            key={value.reason_id}
            value={value.reason_id}
            label={value.reason}
          />
        );
      });
    }
    const {msgIcon, bannerImg, likeIcon, shareIcon, buy_btn} = styles;
    return (
      <ScrollView style={{flex: 1, backgroundColor: '#f5f6fa'}}>
        <StatusBar backgroundColor="#C90205" barStyle="light-content" />
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.sendMessage}
          onRequestClose={() => {
            this.closeMessageModal();
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'rgba(0,0,0,0.5)',
            }}>
            <View
              style={{
                width: wp('80%'),
                maxHeight: hp('80%'),
                backgroundColor: '#fff',
                padding: 20,
                zIndex: 5,
                position: 'relative',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <ScrollView keyboardShouldPersistTaps={'handled'}>
                <Text
                  style={{
                    color: '#545454',
                    fontSize: hp('3%'),
                    paddingBottom: 0,
                    fontFamily: Font.RobotoRegular,
                    textAlign: 'center',
                  }}>
                  {' '}
                  Mengirim pesan{' '}
                </Text>
                <CardSection>
                  <InputInner
                    value={this.state.subject}
                    onChangeText={(value) => this.setState({subject: value})}
                    label="Subyek"
                  />
                </CardSection>
                <CardSection>
                  <TextareaInner
                    value={this.state.message}
                    onChangeText={(value) => this.setState({message: value})}
                    label="Pesan"
                  />
                </CardSection>
                <View style={styles.btnWrapper}>
                  <ButtonTwo
                    onPress={() => {
                      this.sendMessageContent();
                    }}>
                    Mengirim pesan
                  </ButtonTwo>
                </View>
              </ScrollView>
              <TouchableOpacity
                onPress={() => this.closeMessageModal()}
                style={styles.closeBtn2}>
                <Text style={styles.crossColor}>&#10005;</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        {this.state.loading && (
          <View
            style={{
              marginTop: hp('41%'),
            }}>
            <Spinner color="red" size="large" />
          </View>
        )}
        {this.state.shop_details && (
          <View
            style={{
              backgroundColor: '#DCDCDC',
            }}>
            {/* blurRadius={3} */}
            <ImageBackground
              style={bannerImg}
              source={{uri: this.state.shop_details.shop_banner}}>
              <View style={styles.iconWrapper}>
                {this.state._token != null && !this.state.myShop && (
                  <TouchableOpacity onPress={() => this.sendMessageToShop()}>
                    <Image
                      style={msgIcon}
                      source={require('../images/msgIcon.png')}
                    />
                  </TouchableOpacity>
                )}
                {/* {console.log(this.state.shop_details.shop_owner_username)} */}
                {this.state._token != null && !this.state.myShop && (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('ChatWebViewNavigate', {
                        userSelected: this.state.shop_details
                          .shop_owner_username,
                      })
                    }
                    disabled={!this.state.shop_details.shop_owner_username}>
                    <Ionicons
                      name={'paper-plane-outline'}
                      size={20}
                      style={{marginRight: 20}}
                    />
                  </TouchableOpacity>
                )}

                {this.state._token != null && !this.state.myShop && (
                  <View>
                    {this.state.shop_details.is_shop_favourite && (
                      <TouchableOpacity onPress={() => this.favouriteShop()}>
                        <Image
                          style={likeIcon}
                          source={require('../images/like3.png')}
                        />
                      </TouchableOpacity>
                    )}
                    
                    {!this.state.shop_details.is_shop_favourite && (
                      <TouchableOpacity onPress={() => this.favouriteShop()}>
                        <Image
                          style={likeIcon}
                          source={require('../images/likeIcon.png')}
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                )}
                <TouchableOpacity onPress={this._toggleShow}>
                  <Image
                    style={shareIcon}
                    source={require('../images/shareIcon.png')}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.lftImgWrapper}>
                <View style={styles.lftImgDiv}>
                  <Image
                    style={styles.lftImg}
                    source={{uri: this.state.shop_details.shop_logo}}
                  />
                </View>
                <View>
                  <Text style={styles.headingTxt}>
                    {this.state.shop_details.shop_name}
                  </Text>
                  <Text style={styles.openTxt}>
                    (Dibuka pada {this.state.shop_details.shoped_opened_on})
                  </Text>
                  <AirbnbRating
                    count={this.state.shop_details.shop_max_rating}
                    defaultRating={rounded_off_rating}
                    size={20}
                    isDisabled={true}
                    showRating={false}
                  />
                  <Text style={styles.outTxt}>
                    {rounded_off_rating} dari{' '}
                    {this.state.shop_details.shop_max_rating}
                  </Text>
                </View>
              </View>
            </ImageBackground>
            {this.state.shop_details.shop_description !== '' && (
              <View style={styles.description}>
                {this.state.shop_details.shop_description !== '' ? (
                  <Text style={styles.descripTionTxt}>
                    {this.state.shop_details.shop_description}
                  </Text>
                ) : (
                  <Text style={styles.descripTionTxt}>
                    Tidak ada Deskripsi untuk toko ini
                  </Text>
                )}
              </View>
            )}
            <View style={styles.btn_Wrapper}>
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal={true}>
                <TouchableOpacity
                  onPress={this.findWhoFavourited}
                  style={styles.buttonStyle}>
                  <Image
                    style={styles.fav}
                    source={require('../images/favIcon.png')}
                  />
                  <Text style={styles.textStyle}>
                    Lihat siapa yang suka ini
                  </Text>
                </TouchableOpacity>
                {!this.state.myShop && (
                  <TouchableOpacity
                    onPress={this.reportThisShop}
                    style={styles.buttonStyle}>
                    <Image
                      style={styles.report}
                      source={require('../images/reportIcon.png')}
                    />
                    <Text style={styles.textStyle}>
                      Laporkan toko ini ke Syoobe
                    </Text>
                  </TouchableOpacity>
                )}
                <TouchableOpacity
                  onPress={this.showPolicies}
                  style={styles.buttonStyle}>
                  <Image
                    style={styles.policies}
                    source={require('../images/policies.png')}
                  />
                  <Text style={styles.textStyle}>Kebijakan</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.showReviews}
                  style={styles.buttonStyle}>
                  <Image
                    style={styles.reviews}
                    source={require('../images/review_icon.png')}
                  />
                  <Text style={styles.textStyle}>Ulasan</Text>
                </TouchableOpacity>
              </ScrollView>
            </View>
            <View style={styles.containerStyle}>
              {this.state.innerLoader ? (
                <View
                  style={{
                    marginTop: hp('10%'),
                  }}>
                  <Spinner color="red" size="large" />
                </View>
              ) : (
                <View style={{flex: 1, backgroundColor: '#f5f6fa'}}>
                  {this.state.changeView == 1 && (
                    <View>
                      {this.state.products ? (
                        this.state.products.map((product, index) => (
                          <TouchableOpacity
                            key={index}
                            onPress={() =>
                              this.props.navigation.push('ProductDetail', {
                                product_id: product.product_id,
                                product_requires_shipping:
                                  product.prod_requires_shipping,
                              })
                            }>
                            <View style={styles.cardContainer}>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  paddingTop: hp(1),
                                }}>
                                <View style={styles.image}>
                                  <Image
                                    style={{width: '100%', height: '100%'}}
                                    source={{uri: product.product_image}}
                                  />
                                </View>
                                <View style={{flex: 1, paddingLeft: wp(3)}}>
                                  <Text style={styles.textTitle}>
                                    {product.product_name}
                                  </Text>
                                  <View style={styles.textContainerPrice}>
                                    <Text
                                      style={
                                        styles.textPrice
                                      }>{`Rp. ${formatRupiah(
                                      product.product_price,
                                    )}`}</Text>
                                    {/* <Text
                                    style={
                                      styles.textPriceQty
                                    }>{`x${item.product_quantity}`}</Text> */}
                                  </View>
                                  {product.free_shipping === 'Y' && (
                                    <View style={{padding: 5}}>
                                      <Image
                                        resizeMode="cover"
                                        style={styles.freeShipImg}
                                        source={require('../images/free_shipping.png')}
                                      />
                                    </View>
                                  )}
                                  {product.product_type == 'I' && (
                                    <View style={{padding: 5}}>
                                      <Image
                                        resizeMode="cover"
                                        style={styles.insdown}
                                        source={require('../images/download.png')}
                                      />
                                    </View>
                                  )}
                                  {product.product_type == 'G' && (
                                    <View style={{padding: 5}}>
                                      <Image
                                        resizeMode="cover"
                                        style={styles.insdown}
                                        source={require('../images/graphic_design.png')}
                                      />
                                    </View>
                                  )}
                                </View>
                              </View>
                            </View>
                          </TouchableOpacity>
                        ))
                      ) : (
                        <View
                          style={{
                            marginTop: hp('20%'),
                            marginBottom: hp('25%'),
                          }}>
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: hp('2%'),
                              color: '#1d1f1d',
                            }}>
                            Belum ada produk!
                          </Text>
                        </View>
                      )}
                    </View>
                  )}
                  {this.state.changeView == 2 && (
                    <View>
                      {this.state.favourites.length > 0 ? (
                        this.state.favourites.map((fav, index) => (
                          <View key={fav.user_id} style={styles.favDiv}>
                            <View>
                              <Image
                                style={styles.leftImg}
                                source={{uri: fav.user_image}}
                              />
                            </View>
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                flex: 1,
                              }}>
                              <View style={{flex: 1, paddingRight: 8}}>
                                <Text style={styles.rightText}>
                                  {fav.user_name}
                                </Text>
                                {/* <Text style={styles.rightText2}>
                                                        Favorite Shop:
                                                        <Text style={{ color: '#DC0028' }}>1</Text>
                                                    </Text> */}
                              </View>
                              <TouchableOpacity
                                onPress={() =>
                                  this.props.navigation.navigate('Favorites', {
                                    user_id: fav.user_id,
                                    user_name: fav.user_name,
                                  })
                                }
                                style={styles.rightBtn}>
                                <Text style={{color: '#fff'}}>
                                  {fav.total_fav_item_per_person}
                                </Text>
                                <Text
                                  style={{color: '#fff', fontSize: hp('1.5%')}}>
                                  Favorit
                                </Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        ))
                      ) : (
                        <View
                          style={{
                            marginTop: hp('20%'),
                            marginBottom: hp('25%'),
                          }}>
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: hp('2%'),
                              color: '#1d1f1d',
                            }}>
                            Tidak ada favorit!
                          </Text>
                        </View>
                      )}
                    </View>
                  )}
                  {this.state.changeView === 3 && (
                    <View
                      style={{
                        backgroundColor: '#fff',
                        height: '100%',
                        padding: 15,
                      }}>
                      {!this.state.shopReported ? (
                        <ScrollView keyboardShouldPersistTaps={'handled'}>
                          <View style={{flexDirection: 'row'}}>
                            <Text style={styles.dropheadingClass}>Alasan</Text>
                            <Text
                              style={{
                                color: '#f00',
                                fontSize: hp('2.2%'),
                                marginTop: 5,
                              }}>
                              {' '}
                              *
                            </Text>
                          </View>
                          <View style={styles.dropdownClass}>
                            <Picker
                              style={styles.pickerClass}
                              selectedValue={this.state.selected_reason}
                              onValueChange={(value) =>
                                this.setState({
                                  selected_reason: value,
                                })
                              }>
                              <Picker.Item label="Pilih" value="0" key="0" />
                              {reasons}
                            </Picker>
                          </View>
                          <CardSection>
                            <TextareaInner
                              value={this.state.comment}
                              onChangeText={(value) =>
                                this.setState({comment: value})
                              }
                              label="Komentar"
                            />
                          </CardSection>
                          <View style={styles.btnWrapper}>
                            <ButtonTwo
                              onPress={() => {
                                this.reportShop();
                              }}>
                              Kirim Laporan
                            </ButtonTwo>
                          </View>
                        </ScrollView>
                      ) : (
                        <View
                          style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <Text style={styles.dropheadingClass}>
                            Terima kasih atas tanggapan Anda.
                          </Text>
                        </View>
                      )}
                    </View>
                  )}
                  {this.state.changeView === 4 && this.state.shopData != null && (
                    <View style={{padding: 16}}>
                      {this.state.shopData.shop_shipping_policy != null ||
                      this.state.shopData.shop_additional_faq_policy != null ||
                      this.state.shopData.shop_seller_information != null ? (
                        <View>
                          {this.state.shopData.shop_name != null && (
                            <Text style={styles.shopPolicies}>
                              {this.state.shopData.shop_name}'s Kebijakan Toko
                            </Text>
                          )}
                          {this.state.shopData.shop_shipping_policy != null && (
                            <View style={[styles.section1, styles.borderClass]}>
                              <Text style={styles.leftDescription}>
                                Pengiriman
                              </Text>
                              <Text style={styles.rightDescription}>
                                {this.state.shopData.shop_shipping_policy}
                              </Text>
                            </View>
                          )}
                          {this.state.shopData.shop_additional_faq_policy !=
                            null && (
                            <View style={styles.section1}>
                              <Text style={styles.leftDescription}>
                                Kebijakan & FAQ Tambahan
                              </Text>
                              <Text style={styles.rightDescription}>
                                {this.state.shopData.shop_additional_faq_policy}
                              </Text>
                            </View>
                          )}
                          {this.state.shopData.shop_seller_information !=
                            null && (
                            <View style={styles.section1}>
                              <Text style={styles.leftDescription}>
                                Informasi penjual
                              </Text>
                              <Text style={styles.rightDescription}>
                                {this.state.shopData.shop_seller_information}
                              </Text>
                            </View>
                          )}
                        </View>
                      ) : (
                        <View
                          style={{
                            marginTop: hp('20%'),
                            marginBottom: hp('25%'),
                          }}>
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: hp('2%'),
                              color: '#1d1f1d',
                            }}>
                            Kebijakan belum diperbarui untuk toko ini!
                          </Text>
                        </View>
                      )}
                    </View>
                  )}
                  {this.state.changeView == 5 && (
                    <View>
                      {this.state.reviews ? (
                        this.state.reviews.map((review, index) => (
                          <View key={review.review_id}>
                            <View style={styles.reviewWrapper}>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'flex-start',
                                }}>
                                <View style={{textAlign: 'center'}}>
                                  <View style={styles.reviewImg}>
                                    <Image
                                      style={styles.boxImg}
                                      resize="contain"
                                      source={{uri: review.user_image}}
                                    />
                                  </View>
                                  <Text style={styles.reviewedBy}>
                                    Diperiksa oleh {review.review_given_by}
                                  </Text>
                                </View>
                                <View style={styles.rightBox}>
                                  <View>
                                    <AirbnbRating
                                      count={5}
                                      defaultRating={review.review_rating}
                                      size={15}
                                      isDisabled={true}
                                      showRating={false}
                                    />
                                    <Text style={styles.productComment}>
                                      {review.review}
                                    </Text>
                                  </View>
                                  <View style={styles.reviewImg}>
                                    <Image
                                      style={styles.boxImg}
                                      resize="contain"
                                      source={{uri: review.product_image}}
                                    />
                                  </View>
                                  <Text style={styles.reviewedBy}>
                                    {review.product_name}
                                  </Text>
                                  <Text style={styles.ratingDate}>
                                    {review.reviewed_on}
                                  </Text>
                                </View>
                              </View>
                            </View>
                          </View>
                        ))
                      ) : (
                        <View
                          style={{
                            marginTop: hp('20%'),
                            marginBottom: hp('25%'),
                          }}>
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: hp('2%'),
                              color: '#1d1f1d',
                            }}>
                            Belum ada ulasan!
                          </Text>
                        </View>
                      )}
                    </View>
                  )}
                </View>
              )}
            </View>
          </View>
        )}
      </ScrollView>
    );
  }
}
const styles = {
  cardContainer: {
    backgroundColor: '#fff',
    paddingVertical: hp(1),
    paddingHorizontal: wp(3),
    marginVertical: hp(1),
  },
  image: {
    width: wp(25),
    height: hp(14),
    maxHeight: hp(14),
    maxWidth: wp(25),
    backgroundColor: '#eaeaea',
  },
  textTitle: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#353b48',
    paddingBottom: hp(1),
  },
  textContainerPrice: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textPrice: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#e84118',
  },
  textPriceQty: {
    fontSize: hp(2),
    fontWeight: 'bold',
    color: '#353b48',
  },
  cardItems: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: hp(1),
    borderTopColor: '#f5f6fa',
    borderTopWidth: 1,
    marginTop: hp(1),
  },
  insdown: {
    width: 25,
    height: 25,
    marginTop: hp(2),
  },
  crossColor: {
    textAlign: 'center',
    color: '#000',
  },
  closeBtn2: {
    position: 'absolute',
    right: 0,
    width: 30,
    height: 30,
    top: 0,
    zIndex: 1002,
    color: '#fff',
    borderRadius: 20,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  colorBarStyle: {
    width: wp('4.4%'),
    height: hp('2.2%'),
    borderRadius: 2,
    backgroundColor: '#e67904',
    marginLeft: 8,
  },
  casinoTxtStyle: {
    color: '#c90305',
  },
  colorBarWrapperStyle: {
    flexDirection: 'row',
    width: '50%',
    flexBasis: '50%',
    // fontSize:18,
    fontSize: hp('2.5%'),
    alignItems: 'center',
  },
  colorBrandWrapper: {
    flexDirection: 'row',
  },
  barTxtStyle: {
    color: '#858585',
    // fontSize:18,
    fontSize: hp('2%'),
  },
  address: {
    fontFamily: Font.RobotoRegular,
    color: '#454546',
    fontSize: hp('1.6%'),
    marginTop: hp('0.5%'),
  },
  location: {
    fontFamily: Font.RobotoRegular,
    color: '#000000',
    fontSize: hp('1.6%'),
  },

  freeShipImg: {
    width: wp('9%'),
    height: hp('3%'),
  },
  downTxt: {
    color: '#4abc96',
    fontFamily: Font.RobotoRegular,
  },
  sfLogo: {
    marginLeft: 10,
    width: wp('6%'),
    height: hp('3%'),
  },
  rightRating: {
    width: 85,
    height: 12,
  },
  ratingWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: hp('1%'),
    // paddingTop:hp('1%'),
    // paddingBottom:hp('1%'),
    width: '100%',
    // borderColor:'#e7e7e7',
    // borderTopWidth:1
  },
  lftPrice: {
    flexDirection: 'row',
    alignItems: 'center',
    flexBasis: '50%',
  },
  priceTxt: {
    color: '#00b3ff',
    // fontSize:22,
    fontSize: hp('2.3%'),
    fontWeight: 'bold',
  },
  rightDiv: {
    // paddingLeft:20,
    flexBasis: '50%',
  },
  critiStyle: {
    color: '#00b3ff',
  },
  soldStyle: {
    color: '#858585',
    fontSize: wp('3.2%'),
  },
  shipWrapper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  leftDiv: {
    flexBasis: '50%',
  },
  titleStyle: {
    color: '#545454',
    fontSize: wp('4.8%'),
    paddingBottom: hp('1%'),
  },
  rtAccept: {
    color: '#858585',
    fontSize: wp('3.2%'),
    fontFamily: Font.RobotoRegular,
  },
  rtPolicy: {
    color: '#ff3d5b',
    fontFamily: Font.RobotoRegular,
    fontSize: wp('3.2%'),
    marginBottom: 2,
  },
  sliderBottom: {
    paddingLeft: 28,
    paddingRight: 15,
    flex: 1,
  },
  reviewWrapper: {
    padding: 16,
  },
  recomendedTxt: {
    color: '#696969',
    // fontSize:22,
    fontSize: wp('4%'),
    paddingTop: hp('2%'),
    paddingBottom: hp('1.8%'),
    borderColor: '#e7e7e7',
    borderTopWidth: 1,
    textTransform: 'uppercase',
    fontFamily: Font.RobotoMedium,
  },
  ratingDate: {
    fontSize: hp('1.5%'),
    color: '#000',
    fontFamily: Font.RobotoRegular,
  },
  productComment: {
    marginTop: 10,
    fontFamily: Font.RobotoItalc,
    fontSize: hp('2%'),
    color: '#000',
  },
  rightBox: {
    borderWidth: 1,
    borderColor: '#d2d8da',
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  reviewImg: {
    width: wp('15%'),
    height: wp('15%'),
    borderWidth: 1,
    borderColor: '#d2d8da',
    borderRadius: wp('15%'),
    marginRight: 10,
    padding: wp('1%'),
  },
  shopPolicies: {
    fontFamily: Font.RobotoRegular,
    marginTop: 20,
    marginBottom: 20,
    fontSize: hp('2%'),
  },
  leftDescription: {
    color: '#DC0028',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
    width: '30%',
  },
  rightDescription: {
    color: '#585757',
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
    width: '70%',
    paddingLeft: 15,
  },
  section1: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#d2d8da',
    paddingBottom: 10,
    marginBottom: 10,
  },
  borderClass: {
    borderTopWidth: 1,
    borderTopColor: '#d2d8da',
    paddingTop: 10,
  },
  favDiv: {
    flexDirection: 'row',
    padding: 10,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#dbdbdb',
    marginTop: 10,
  },

  leftImg: {
    width: wp('20%'),
    height: hp('10%'),
    marginRight: wp('3%'),
    borderWidth: 1,
    borderColor: '#dbdbdb',
  },
  rightText: {
    color: '#727272',
    fontSize: hp('2.2%'),
    marginBottom: 10,
  },
  rightText2: {
    color: '#666',
    fontSize: hp('1.8%'),
  },
  rightBtn: {
    width: wp('15%'),
    backgroundColor: '#727272',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#fff',
  },
  fav: {
    width: 13,
    height: 11,
  },
  report: {
    width: 10,
    height: 12,
  },
  policies: {
    width: 10,
    height: 11,
  },
  reviews: {
    width: wp('3.5%'),
    height: hp('1.4%'),
  },
  buttonStyle: {
    borderColor: '#f00',
    borderWidth: 1,
    marginHorizontal: 10,
    borderRadius: 50,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 10,
  },
  textStyle: {
    color: '#b90002',
    paddingHorizontal: 10,
    paddingVertical: 5,

    borderRadius: 50,
  },
  btn_Wrapper: {
    flexDirection: 'row',
    paddingVertical: 15,
    // paddingTop:10,
    // backgroundColor:'#fff',
  },
  description: {},
  descripTionTxt: {
    paddingTop: 25,
    // paddingBottom:25,
    paddingLeft: 15,
    paddingRight: 15,
    color: '#292424',
    fontFamily: Font.RobotoThin,
    fontSize: hp('2.5%'),
  },
  bannerImg: {
    width: '100%',
    // height:150,
    height: hp('28%'),
  },
  iconWrapper: {
    flexDirection: 'row',
    paddingTop: 10,
    justifyContent: 'flex-end',
    paddingBottom: 10,
  },
  msgIcon: {
    width: 34,
    height: 16,
    marginRight: 20,
  },
  likeIcon: {
    width: 22,
    height: 18,
    marginRight: 20,
  },
  shareIcon: {
    width: 20,
    height: 21,
    marginRight: 10,
  },
  lftImgWrapper: {
    flexDirection: 'row',
  },
  lftImgDiv: {
    // width:80,
    // height:80,
    width: hp('15%'),
    height: hp('15%'),
    marginLeft: 10,
    // padding:3,
    borderWidth: 1,
    borderColor: '#fff',
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lftImg: {
    // width:72,
    // height:72
    width: hp('13%'),
    height: hp('13%'),
  },
  headingTxt: {
    color: '#ffffff',
    textShadowColor: '#000',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    fontFamily: Font.RobotoRegular,
    fontSize: hp('3.2%'),
  },
  openTxt: {
    color: '#ffffff',
    textShadowColor: '#000',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2.2%'),
    marginBottom: 5,
  },
  ratingIcon: {
    width: hp('20%'),
    height: hp('3%'),
    marginBottom: 5,
  },
  outTxt: {
    color: '#ffffff',
    textShadowColor: '#000',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 10,
    fontFamily: Font.RobotoRegular,
    fontSize: hp('2%'),
  },
  containerStyle: {
    padding: 0,
    backgroundColor: '#fff',
    height: '100%',
  },
  topSection: {
    backgroundColor: '#e6e6e6',
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    flexWarp: 'warp',
    justifyContent: 'center',
  },
  inputStyle: {
    color: '#000',
    paddingLeft: 10,
    fontSize: wp('4%'),
    marginTop: 0,
    backgroundColor: '#fff',
    height: 40,
    fontFamily: Font.RobotoRegular,
    width: wp('30%'),
    borderRadius: 5,
  },
  datepicherStyle: {
    color: '#000',
    paddingLeft: 0,
    fontSize: wp('4%'),
    marginTop: 0,
    backgroundColor: '#fff',
    height: 40,
    fontFamily: Font.RobotoRegular,
    width: wp('30%'),
    borderRadius: 5,
  },
  dropdownClass: {
    // elevation: 1,
    borderRadius: 1,
    marginBottom: 17,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: '#cfcdcd',
  },
  pickerClass: {
    height: hp('8%'),
  },
  dropheadingClass: {
    fontSize: hp('2%'),
    marginTop: 5,
    padding: 0,
    margin: 0,
    marginLeft: 4,
    marginBottom: 8,
    fontFamily: Font.RobotoRegular,
    color: '#545454',
  },
  btnWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 10,
    paddingBottom: 15,
  },
  segmentStyle: {
    backgroundColor: '#b90002',
    textAlign: 'left',
    justifyContent: 'flex-start',
    paddingLeft: 10,
    height: 'auto',
    paddingTop: 10,
    // alignItems:'baseline',
    paddingBottom: 0,
  },
  segmentBtn: {
    borderWidth: 0,
    borderColor: 'transparent',
    bottom: 0,
    margin: 0,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    height: 'auto',
    marginBottom: 0,
  },
  segmentBtnTxt: {
    color: '#e6e6e6',
  },
  segmentBtnTxtActive: {
    color: '#606060',
    // color:'#f00'
  },
  segmentActive: {
    backgroundColor: '#e6e6e6',
    borderWidth: 0,
    borderColor: 'transparent',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    paddingTop: 10,
    height: 'auto',
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    color: '#e6e6e6',
  },
  boxImgNew: {
    width: wp('24%'),
    height: hp('17%'),
  },
  boxImg: {
    width: '100%',
    height: '100%',
    borderRadius: wp('15%'),
  },
  reviewedBy: {
    color: '#000',
    fontSize: hp('1.5%'),
    marginTop: 10,
    lineHeight: hp('2%'),
    width: wp('15%'),
    marginRight: 10,
  },

  imgContentWrapper: {
    flexDirection: 'row',
    flex: 1,
    padding: 10,
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#e7e7e7',
  },
  imgWrapper: {
    width: wp('15%'),
    marginRight: wp('3%'),
  },
  boxImg2: {
    width: wp('20%'),
    height: wp('32%'),
  },
  productName: {
    fontSize: wp('4.5%'),
    color: '#545454',
    fontFamily: Font.RobotoRegular,
  },
  count: {
    color: '#00b3ff',
    fontSize: wp('4%'),
    fontFamily: Font.RobotoRegular,
  },
  count2: {
    color: '#00b3ff',
    fontSize: wp('4%'),
    fontFamily: Font.RobotoItalc,
  },
  stat_us: {
    color: '#c90305',
    fontSize: wp('4%'),
  },
  available: {
    color: '#858585',
    fontSize: wp('4%'),
    marginRight: wp('10%'),
    fontFamily: Font.RobotoMedium,
  },
  available2: {
    color: '#858585',
    fontSize: wp('4%'),
    marginRight: wp('10%'),
    fontFamily: Font.RobotoItalc,
  },
  boxStyle: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    padding: 10,
    paddingBottom: 0,
    flexDirection: 'row',
    flexWarp: 'warp',
    position: 'relative',
  },
  price: {
    color: '#00b3ff',
    fontSize: wp('5%'),
    // fontFamily:Fonts.RobotoBold,
  },
  abc: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // paddingLeft:5,
    flexWarp: 'warp',
    width: '100%',
    marginBottom: 10,
  },
  labelstyle: {
    fontSize: wp('4%'),
    color: '#545454',
    marginRight: 10,
    marginLeft: 0,
    fontFamily: Font.RobotoRegular,
    textAlign: 'left',
    left: 0,
  },
  checkBoxStyle: {
    flexBasis: 'auto',
    // minWidth:'30%',
    justifyContent: 'flex-start',
    textAlign: 'left',
    backgroundColor: '#ffffff',
    padding: 5,
    borderRadius: 5,
    minWidth: '30%',
    alignSelf: 'flex-start',
  },
};

export default connect(null, null)(ShopDetails);
