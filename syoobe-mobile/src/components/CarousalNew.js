import React from 'react';
import {
  TouchableOpacity,
  Animated,
  View,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
// import { Modal } from 'react-native';
// import ImageViewer from 'react-native-image-zoom-viewer';

const CarousalNew = (props) => {
  const deviceWidth = Dimensions.get('window').width;
  const deviceHeight = Dimensions.get('window').height;
  const FIXED_BAR_WIDTH = 280;
  const BAR_SPACE = 10;
  numItems = props.carousalImages.length;
  animVal = new Animated.Value(0);
  itemWidth = 8;
  itemHeight = 8;
  itemRadius = 8;

  let imageArray = [];
  let barArray = [];
  props.carousalImages.forEach((image, i) => {
    const thisImage = (
      <View
        key={i}
        style={{
          width: deviceWidth,
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() =>
            props.navigation.navigate('ImgDetail', {
              images: props.carousalImages,
              index: i,
            })
          }>
          <Image
            source={{uri: image}}
            resizeMode={'contain'}
            style={{width: wp('100%'), height: '100%'}}
          />
        </TouchableOpacity>
      </View>
    );
    imageArray.push(thisImage);

    const scrollBarVal = this.animVal.interpolate({
      inputRange: [deviceWidth * (i - 1), deviceWidth * (i + 1)],
      outputRange: [-this.itemWidth, this.itemWidth],
      extrapolate: 'clamp',
    });

    const thisBar = (
      <View
        key={`bar${i}`}
        style={[
          styles.track,
          {
            width: this.itemWidth,
            height: this.itemHeight,
            borderRadius: this.itemRadius,
            marginLeft: i === 0 ? 0 : BAR_SPACE,
          },
        ]}>
        <Animated.View
          style={[
            styles.bar,
            {
              width: this.itemWidth,
              height: this.itemHeight,
              borderRadius: this.itemRadius,
              transform: [{translateX: scrollBarVal}],
            },
          ]}
        />
      </View>
    );
    barArray.push(thisBar);
  });

  return (
    <View style={styles.container} flex={1}>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        scrollEventThrottle={10}
        pagingEnabled
        onScroll={Animated.event([
          {nativeEvent: {contentOffset: {x: this.animVal}}},
        ])}>
        {imageArray}
      </ScrollView>
      <View style={styles.barContainer}>{barArray}</View>
    </View>
  );
};

// const imageView = (images) => {
//   console.log('imagesPath',images);
//   return (
//     <Modal onRequestClose={()=>{}}  visible={true}>
//         <ImageViewer imageUrls={images}/>
//     </Modal>
//   );
// }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#000'
  },
  barContainer: {
    position: 'absolute',
    zIndex: 2,
    bottom: 20,
    flexDirection: 'row',
  },
  track: {
    backgroundColor: '#ccc',
    overflow: 'hidden',
    height: 2,
  },
  bar: {
    backgroundColor: '#5294d6',
    height: 2,
    position: 'absolute',
    left: 0,
    top: 0,
  },
});

export default CarousalNew;
