import axios from 'axios';
import {BASE_URL} from './baseUrl';

export const notificationApi = (data) => {
  var formBody = [];
  for (var property in data) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(data[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  console.log(formBody);
  return axios.post(`${BASE_URL}/notificationList`, formBody);
};

export const notificationViewAll = (data) => {
  var formBody = [];
  for (var property in data) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(data[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  console.log(formBody);
  return axios.post(`${BASE_URL}/notificationViewAll`, formBody);
};

export const notificationView = (data) => {
  var formBody = [];
  for (var property in data) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(data[property]);
    formBody.push(encodedKey + '=' + encodedValue);
  }
  formBody = formBody.join('&');
  return axios.post(`${BASE_URL}/notificationView`, formBody);
};

// export const chatNotificationSend = (data) => {
//   var formBody = [];
//   for (var property in data) {
//     var encodedKey = encodeURIComponent(property);
//     var encodedValue = encodeURIComponent(data[property]);
//     formBody.push(encodedKey + '=' + encodedValue);
//   }
//   formBody = formBody.join('&');
//   return axios.post(`${BASE_URL}/SendPushForChat`, formBody);
// };

// export const cancelSalesOrder = (data) => {
//   var formBody = [];
//   for (var property in data) {
//     var encodedKey = encodeURIComponent(property);
//     var encodedValue = encodeURIComponent(data[property]);
//     formBody.push(encodedKey + '=' + encodedValue);
//   }
//   formBody = formBody.join('&');
//   return axios.post(`${BASE_URL}/submitCancelOrderInSale`, formBody);
// };

export const otpVerification = (data) => {
  // var formBody = [];
  // for (var property in data) {
  //     var encodedKey = encodeURIComponent(property);
  //     var encodedValue = encodeURIComponent(data[property]);
  //     formBody.push(encodedKey + "=" + encodedValue);
  // }
  // formBody = formBody.join("&");
  return axios.post(`${BASE_URL}/otpVerification`, data);
};
