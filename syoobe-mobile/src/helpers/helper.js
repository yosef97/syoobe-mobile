export const formatRupiah = (rawNum, prefix = true) => {
  let number = rawNum?.toString().split('.');
  let num = number
    ?.filter((item) => {
      if (item !== '00' || item.length > 2) {
        return item;
      }
      return Math.round(item);
    })
    .join('');
  // console.log(num);
  let newNumber = num?.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');
  return prefix ? newNumber : parseInt(num);
};

export const countNumber = (number = 0, other_number = 0) => {
  let num1 = formatRupiah(number, false);
  let num2 = formatRupiah(other_number, false);

  let sum_number = num1 + num2;
  return sum_number;
};

export const getLabelPicker = (data) => data.filter((x) => x.label);

export const substr = (string, length = 25) => {
  return string.length > length ? `${string.substring(0, length)}...` : string;
};

export const getSlug = (string) => string.toLowerCase().split(' ').join('-');
export const getDate = (string) => {
  let date = string.split('-');
  let month = parseInt(date[1]);
  return `${date[0]}-${month}-${date[2]}`;
};

export const getCompanyName = (
  companies,
  services,
  product_id,
  title = 'Pilih Kurir',
) => {
  const service = services.filter((ser) => ser?.product_id === product_id)[0];
  const company = companies.filter(
    (comp) => comp?.company_id === service?.company_id,
  )[0];
  // console.log(service, company);
  return service && company
    ? `${company?.company_name} - ${service?.item.pship_service}`
    : title;
};

export const formatCc = (
  value,
  numberOfLength = 16,
  numberOfParts = 4,
  prefix = ' ',
) => {
  let v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
  let matches =
    numberOfLength === 16 ? v.match(/\d{4,16}/g) : v.match(/\d{2,5}/g);
  let match = (matches && matches[0]) || '';
  let parts = [];

  for (let i = 0, len = match.length; i < len; i += numberOfParts) {
    parts.push(match.substring(i, i + numberOfParts));
  }

  if (parts.length) {
    return parts.join(prefix);
  } else {
    return value;
  }
};

export const arrayChunk = (inputArray = [], perChunk = 3) => {
  let result = inputArray.reduce((resultArray, item, index) => {
    const chunkIndex = Math.floor(index / perChunk);

    if (!resultArray[chunkIndex]) {
      resultArray[chunkIndex] = []; // start a new chunk
    }

    resultArray[chunkIndex].push(item);

    return resultArray;
  }, []);

  return result;
};

export const setIndexProgress = (data = {}) => {
  let index = 0;

  if (
    data.payment_confirmed &&
    data.in_progress &&
    data.sample_files &&
    data.product_files &&
    data.completed
  ) {
    index = 4;
  } else if (
    data.payment_confirmed &&
    data.in_progress &&
    data.sample_files &&
    data.product_files
  ) {
    index = 3;
  } else if (data.payment_confirmed && data.in_progress && data.sample_files) {
    index = 2;
  } else if (data.payment_confirmed && data.in_progress) {
    index = 1;
  } else {
    index = 0;
  }

  return index;
};

export const randomColor = () => {
  const colors = [
    '#273c75',
    '#0097e6',
    '#dcdde1',
    '#353b48',
    '#e67e22',
    '#8e44ad',
    '#f1c40f',
    '#16a085',
    '#ee5253',
    '#0abde3',
  ];

  return colors[Math.floor(Math.random() * colors.length)];
};
