import { getStates } from '../../../actions';
import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { Checkbox } from 'react-native-paper';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { connect } from 'react-redux';
import { getProvince, setSelectedProvince } from '../../../actions/AdressAction';
import { Spinner } from '../../../components/common/Spinner';


class State extends Component {
  constructor(props) {
    super(props)
    this.state = {
      states: []
    }
  }

  componentDidMount() {
    this.fetchState()
  }

  fetchState = async () => {
    await this.props.getStates(this.props.route.params)
  }

  render() {
    const { component } = this.props
    if (!this.props.states.length > 0) {
      return (
        <Spinner color="red" size="large" />
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <View style={{ backgroundColor: '#fff', paddingHorizontal: wp(5) }}>
          <FlatList
            data={this.props.states}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <View style={style.containerList}>
                <Checkbox
                  status={this.props.selectedProvince.id === item.id ? 'checked' : 'unchecked'}
                  onPress={() => this.props.setSelectedProvince(item, this.props.navigation, false, component)}
                />
                <Text style={style.textList}>{item.name}</Text>
              </View>
            )}
            keyExtractor={(item) => item.id}
          />
        </View>
      </View>
    )
  }
}

const style = StyleSheet.create({
  containerList: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: hp(2),
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1
  },
  textList: { fontSize: hp(2.4), paddingLeft: wp(5) }
})

const mapStateToProps = state => {
  return {
    provinces: state.country.province.provinces,
    selectedProvince: state.country.province.selectedProvince,
    states: state.country.states,
    component: state.country.component
  };
};

export default connect(mapStateToProps, { getProvince, setSelectedProvince, getStates })(State)
