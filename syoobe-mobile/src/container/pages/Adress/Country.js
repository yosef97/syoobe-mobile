import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {Checkbox} from 'react-native-paper';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {Spinner} from '../../../components/common/Spinner';
// reducers
import {getCountries, getStates} from '../../../actions';
import {setSelectedCountry} from '../../../actions/AdressAction';
import InputText from '../../../components/common/InputText';

class Country extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countries: [],
    };
  }

  componentDidMount() {
    this.props.getCountries();
    this.fetchCountry();
  }

  fetchCountry = () => {
    this.setState({countries: this.props.countries});
  };

  searchCountry = (e) => {
    const updatedList = this.props.countries.filter((item) => {
      return item.name.toLowerCase().search(e.toLowerCase()) !== -1;
    });
    this.setState({countries: updatedList});
  };

  render() {
    if (!this.props.countries.length > 0) {
      return <Spinner color="red" size="large" />;
    }
    return (
      <View style={{flex: 1, paddingHorizontal: 10, backgroundColor: '#fff'}}>
        <View style={{flex: 1, paddingTop: hp(1), paddingBottom: 0}}>
          <InputText
            onChangeText={this.searchCountry}
            placeholder={'Cari'}
            icon={'search'}
          />
        </View>
        <View style={{flex: 8}}>
          <View style={{backgroundColor: '#fff'}}>
            <FlatList
              data={this.state.countries}
              showsVerticalScrollIndicator={false}
              renderItem={({item}) => (
                <View style={style.containerList}>
                  <Checkbox
                    status={
                      this.props.selectedCountry?.id == item.id
                        ? 'checked'
                        : 'unchecked'
                    }
                    onPress={() =>
                      this.props.setSelectedCountry(item, this.props.navigation)
                    }
                  />
                  <Text style={style.textList}>{item.name}</Text>
                </View>
              )}
              keyExtractor={(item) => item.id}
            />
          </View>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  containerList: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: hp(1),
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
  },
  textList: {fontSize: hp(2.4), paddingLeft: wp(5)},
});

const mapStateToProps = (state) => {
  return {
    countries: state.country.countries,
    selectedCountry: state.country.selectedCountry,
    country: state.country,
  };
};

export default connect(mapStateToProps, {
  getCountries,
  setSelectedCountry,
  getStates,
})(Country);
