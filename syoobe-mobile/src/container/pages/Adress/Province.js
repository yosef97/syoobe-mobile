import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {Checkbox} from 'react-native-paper';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';
import {getProvince, setSelectedProvince} from '../../../actions/AdressAction';
import {Spinner} from '../../../components/common/Spinner';

class Province extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedProvince: {},
    };
  }

  componentDidMount() {
    this.props.getProvince();
  }

  render() {
    if (!this.props.provinces.length > 0) {
      return <Spinner color="red" size="large" />;
    }
    return (
      <View style={{flex: 1}}>
        <View style={{backgroundColor: '#fff', paddingHorizontal: wp(5)}}>
          <FlatList
            data={this.props.provinces}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <View style={style.containerList}>
                <Checkbox
                  status={
                    this.props.selectedProvince.province_id === item.province_id
                      ? 'checked'
                      : 'unchecked'
                  }
                  onPress={() =>
                    this.props.setSelectedProvince(item, this.props.navigation)
                  }
                />
                <Text style={style.textList}>{item.province}</Text>
              </View>
            )}
            keyExtractor={(item) => item.province_id}
          />
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  containerList: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: hp(2),
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
  },
  textList: {fontSize: hp(2.4), paddingLeft: wp(5)},
});

const mapStateToProps = (state) => {
  return {
    provinces: state.country.province.provinces,
    selectedProvince: state.country.province.selectedProvince,
  };
};

export default connect(mapStateToProps, {getProvince, setSelectedProvince})(
  Province,
);
