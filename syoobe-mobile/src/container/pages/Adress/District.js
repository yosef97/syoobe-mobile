import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { Checkbox } from 'react-native-paper';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { connect } from 'react-redux';
import { getDistrict, setSelectedDistrict } from '../../../actions/AdressAction';
import { Spinner } from '../../../components/common/Spinner';


class District extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
    headerTitle:
      <Text style={{ fontSize: hp('2.5%'), color: '#fff' }}>
        Pilih Kecamatan
    </Text>
  })

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.getDistrict(this.props.route.params.city_id)
  }

  render() {
    if (!this.props.districts.length > 0) {
      return (
        <Spinner color="red" size="large" />
      );
    }
    const { component } = this.props
    // console.log(component)
    return (
      <View style={{ flex: 1 }}>
        <View style={{ backgroundColor: '#fff', paddingHorizontal: wp(5) }}>
          <FlatList
            data={this.props.districts}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <View style={style.containerList}>
                <Checkbox
                  status={this.props.selectedDistrict.subdistrict_id === item.subdistrict_id ? 'checked' : 'unchecked'}
                  onPress={() => this.props.setSelectedDistrict(item, this.props.navigation, false, component)}
                />
                <Text style={style.textList}>{item.subdistrict_name}</Text>
              </View>
            )}
            keyExtractor={(item) => item.subdistrict_id}
          />
        </View>
      </View>
    )
  }
}

const style = StyleSheet.create({
  containerList: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: hp(2),
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1
  },
  textList: { fontSize: hp(2.4), paddingLeft: wp(5) }
})

const mapStateToProps = state => {
  return {
    districts: state.country.district.districts,
    selectedDistrict: state.country.district.selectedDistrict,
    component: state.country.component
  };
};

export default connect(mapStateToProps, { getDistrict, setSelectedDistrict })(District)
