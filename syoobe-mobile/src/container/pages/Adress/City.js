import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  FlatList,
  StyleSheet,
} from 'react-native';
import {Checkbox} from 'react-native-paper';
import {connect} from 'react-redux';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {getCity, setSelectedCity} from '../../../actions/AdressAction';
import {Spinner} from '../../../components/common/Spinner';

class City extends Component {
  static navigationOptions = ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#C90205',
    },
    headerTintColor: 'white',
    headerTitle: (
      <Text style={{fontSize: hp('2.5%'), color: '#fff'}}>
        Pilih Kota/Kabupaten
      </Text>
    ),
  });

  constructor(props) {
    super(props);
    this.state = {
      selectedProvince: {},
    };
  }

  componentDidMount() {
    this.props.getCity(this.props.route.params.province_id);
  }

  render() {
    if (!this.props.cities.length > 0) {
      return <Spinner color="red" size="large" />;
    }
    const {
      component,
      navigation,
      setSelectedCity,
      cities,
      selectedCity,
    } = this.props;
    return (
      <View style={{flex: 1}}>
        <View style={{backgroundColor: '#fff', paddingHorizontal: wp(5)}}>
          <FlatList
            data={cities}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <View style={style.containerList}>
                <Checkbox
                  status={
                    selectedCity.city_id === item.city_id
                      ? 'checked'
                      : 'unchecked'
                  }
                  onPress={() =>
                    setSelectedCity(item, navigation, false, component)
                  }
                />
                <Text style={style.textList}>{item.city_name}</Text>
              </View>
            )}
            keyExtractor={(item) => item.city_id}
          />
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  containerList: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: hp(2),
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
  },
  textList: {fontSize: hp(2.4), paddingLeft: wp(5)},
});

const mapStateToProps = (state) => {
  return {
    cities: state.country.city.cities,
    selectedCity: state.country.city.selectedCity,
    component: state.country.component,
  };
};

export default connect(mapStateToProps, {getCity, setSelectedCity})(City);
