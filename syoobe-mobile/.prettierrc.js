module.exports = {
  bracketSpacing: false,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  endOfLine: 'auto',
  extends: ['airbnb-base', 'prettier'],
};
