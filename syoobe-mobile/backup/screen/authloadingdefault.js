import messaging from '@react-native-firebase/messaging';
import React from 'react';
import {
  AppState,
  AsyncStorage,
  ImageBackground,
  StatusBar,
  View,
} from 'react-native';
import PushNotification from 'react-native-push-notification';
import {connect} from 'react-redux';
import {updateUserStatus} from '../actions';
import * as actionTypes from '../actions/types';
import {notificationApi} from '../services/services';

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    // let config = {
    //   apiKey: "AIzaSyBqhdnwHIJP3wCjVtfdkSduHUxmNKAJ-B8",
    //   databaseURL: "https://syoobe-7f851.firebaseio.com",
    //   projectId: "syoobe-7f851",
    //   storageBucket: "syoobe-7f851.appspot.com",
    //   messagingSenderId: "471541084901",
    //   appId: "1:471541084901:ios:77b970b90b5911576f5a0c"
    // };
    // firebase.initializeApp(config);
    this._bootstrapAsync();
  }

  async componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners();
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange = async (nextAppState) => {
    const userToken = await AsyncStorage.getItem('token');
    if (nextAppState === 'background') {
      this.props.updateUserStatus(0, userToken);
    }
    if (nextAppState === 'active') {
      this.props.updateUserStatus(1, userToken);
    }
  };

  goToNotifications = async (data) => {
    this.props.navigation.navigate('Notifications');
  };

  async checkPermission() {
    const enabled = await messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    token = await messaging().getToken();
    console.log('token', token);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log('token', fcmToken);
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  async requestPermission() {
    try {
      await messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  componentWillUnmount() {
    this.notificationListener;
    this.notificationOpenedListener;
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  async createNotificationListeners() {
    this.notificationOpenedListener = messaging().onNotificationOpenedApp(
      (notificationOpen) => {
        console.log('no2', notificationOpen);
        // const { title, body } = notificationOpen.notification;
        // this.showAlert(title, body);
      },
    );
    const notificationOpen = await messaging().getInitialNotification();
    if (notificationOpen) {
      console.log('no33', notificationOpen);
      this.goToNotifications(notificationOpen.notification.data);
    }

    this.messageListener = messaging().onMessage((message) => {
      console.log('no4', message);
      //process data message
      this.showNotification(message.data);
      console.log(JSON.stringify(message));
    });
  }

  showNotification = (data) => {
    PushNotification.localNotification({
      title: data.title,
      message: data.body,
      soundName: 'good_notification.mp3',
    });
  };

  callNotifications = (token) => {
    console.log(token);
    const details = {
      _token: token,
    };
    notificationApi({...details})
      .then((response) => {
        console.log(response);
        if (response.data.status == 1) {
          this.props.storeUnreadNotifications(
            response.data.total_unread_notification,
          );
          this.props.storeNotifications(
            response.data.user_notification_details,
          );
        }
      })
      .catch((error) => console.log('ee', error));
  };

  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('token');
    if (userToken) {
      await this.callNotifications(userToken);
      // const resetAction = await StackActions.reset({
      //   index: 0,
      //   actions: [NavigationActions.navigate({ routeName: 'homeDrawer' })],
      // });
      // this.props.navigation.dispatch(resetAction)
      this.props.navigation.replace('HomeDrawer');
    } else {
      // const pushAction = StackActions.replace('AuthDrawer');
      this.props.navigation.replace('AuthDrawer');
    }
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <StatusBar backgroundColor="transparent" transcluent />
        <ImageBackground
          style={styles.bgImg}
          source={require('../images/loginBg.png')}
        />
      </View>
    );
  }
}

const styles = {
  bgImg: {
    height: '100%',
  },
};

const mapDispatchToProps = (dispatch) => {
  return {
    storeNotifications: (data) =>
      dispatch({type: actionTypes.NOTIFICATIONS, payload: data}),
    storeUnreadNotifications: (count) =>
      dispatch({type: actionTypes.UNREAD_NOTIFICATIONS, payload: count}),
    updateUserStatus,
  };
};

export default connect(null, mapDispatchToProps)(AuthLoadingScreen);
