import React, {Component} from 'react';
import {
  AppState,
  AsyncStorage,
  ImageBackground,
  StatusBar,
  View,
} from 'react-native';
import PushNotification, {Importance} from 'react-native-push-notification';
import {connect} from 'react-redux';
import {
  storeUnreadNotifications,
  storeNotifications,
  updateUserStatus,
} from '../actions';
import {notificationApi} from '../services/services';
import messaging from '@react-native-firebase/messaging';

class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);
    // this.createDefaultChannels();
    this._bootstrapAsync();
  }

  createDefaultChannels() {
    PushNotification.createChannel(
      {
        channelId: 'com.syoobe', // (required)
        channelName: 'syoobe', // (required)
        channelDescription: 'A default channel', // (optional) default: undefined.
        soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
        importance: Importance.LOW, // (optional) default: Importance.HIGH. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
      },
      (created) =>
        console.log(`createChannel 'default-channel-id' returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
    );
  }

  handleAppStateChange = async (nextAppState) => {
    const userToken = await AsyncStorage.getItem('token');
    if (nextAppState === 'background') {
      this.props.updateUserStatus(0, userToken);
    }
    if (nextAppState === 'active') {
      this.props.updateUserStatus(1, userToken);
    }
  };

  goToNotifications = async (data) => {
    this.props.navigation.navigate('Notifications');
  };

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    var token = await messaging().getToken();
    console.log('token', token);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log('token', fcmToken);
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  componentWillUnmount() {
    this.notificationListener;
    this.notificationOpenedListener;
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  showNotification = (data) => {
    console.log('ini data push notification', JSON.stringify(data));
    PushNotification.localNotification({
      title: data.title,
      message: data.body,
      soundName: 'good_notification.mp3',
    });
  };

  callNotifications = (token) => {
    const details = {
      _token: token,
    };
    notificationApi({...details})
      .then((response) => {
        if (response.data.status == 1) {
          this.props.storeUnreadNotifications(
            response.data.total_unread_notification,
          );
          this.props.storeNotifications(
            response.data.user_notification_details,
          );
        }
      })
      .catch((error) => console.log('ee', error));
  };

  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('token');
    if (userToken) {
      await this.callNotifications(userToken);
      this.props.navigation.replace('HomeDrawer');
    } else {
      this.props.navigation.replace('AuthDrawer');
    }
  };

  // handleShowNotification = () => {
  //   this.props.notifications.map(
  //     (notification) =>
  //       notification.tnd_read_unread_status !== '1' &&
  //       PushNotification.localNotification({
  //         title: notification.tnd_notification_title,
  //         message: notification.tnd_notification_msg,
  //         soundName: 'default',
  //       }),
  //   );

  //   this.props.notifications.map(
  //     (notification) =>
  //       notification.tnd_read_unread_status !== '1' &&
  //       PushNotification.localNotificationSchedule({
  //         title: notification.tnd_notification_title,
  //         message: notification.tnd_notification_msg,
  //         date: new Date(Date.now() + 1000), // in 24h
  //         soundName: 'default',
  //       }),
  //   );
  // };

  async componentDidMount() {
    console.log('componentDidMount');
    // this.handleShowNotification();
    this.checkPermission();
    this.createNotificationListeners();
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  async checkPermission() {
    const enabled = await messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async requestPermission() {
    try {
      await messaging().requestPermission();
      this.getToken();
    } catch (error) {
      console.log('permission rejected');
    }
  }

  async createNotificationListeners() {
    this.notificationOpenedListener = messaging().onNotificationOpenedApp(
      (notificationOpen) => {
        console.log('no2', notificationOpen);
      },
    );

    const notificationOpen = await messaging().getInitialNotification();
    if (notificationOpen) {
      console.log('no33', notificationOpen);
      this.goToNotifications(notificationOpen.notification.data);
    }

    this.messageListener = messaging().onMessage((message) => {
      console.log('no4', message);
      //process data message
      this.showNotification(message.data);
      console.log(JSON.stringify(message));
    });
  }

  render() {
    // console.log('ini props auth loading screen', JSON.stringify(this.props));
    // console.log('ini state', JSON.stringify(this.state));
    console.log('aaaaauthhhh');

    return (
      <View>
        <StatusBar backgroundColor="transparent" transcluent />
        <ImageBackground
          style={styles.bgImg}
          source={require('../images/loginBg.png')}
        />
      </View>
    );
  }
}

const styles = {
  bgImg: {
    height: '100%',
  },
};

const mapStateToProps = (state) => {
  return {
    // notifications: state.notification.notifications,
    // unreadNotificationCount: state.notification.unreadNotificationCount,
  };
};

export default connect(mapStateToProps, {
  storeUnreadNotifications,
  storeNotifications,
  updateUserStatus,
})(AuthLoadingScreen);
